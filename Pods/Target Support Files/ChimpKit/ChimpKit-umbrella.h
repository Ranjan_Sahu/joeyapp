#ifdef __OBJC__
#import <UIKit/UIKit.h>
#endif

#import "ChimpKit.h"
#import "CKAuthViewController.h"
#import "CKScanViewController.h"
#import "CKSubscribeAlertView.h"

FOUNDATION_EXPORT double ChimpKitVersionNumber;
FOUNDATION_EXPORT const unsigned char ChimpKitVersionString[];

