#ifdef __OBJC__
#import <UIKit/UIKit.h>
#endif

#import "ALPValidator.h"
#import "UIView+ALPValidator.h"

FOUNDATION_EXPORT double ALPValidatorVersionNumber;
FOUNDATION_EXPORT const unsigned char ALPValidatorVersionString[];

