//
//  MilestonesHelper.m
//  Joey
//
//  Created by werbwerks1 on 21/08/18.
//  Copyright © 2018 Auxilia. All rights reserved.
//

#import "MilestonesHelper.h"

@implementation MilestonesHelper

+ (MilestonesHelper *)sharedInstance {
    static dispatch_once_t pred;
    
    static MilestonesHelper *shared = nil;
    
    dispatch_once(&pred, ^{
        shared = [MilestonesHelper new];
    });
    return shared;
}

- (void)callServiceWithURL:(NSString *)URLString parameters:(id)parameters success:(void (^)(id responseObject))success failure:(void (^)(id responseObject))failure
{
    
    
    [CommonHelper checkInternetWithSuccess:^{
        [[WebServiceManager sharedManager] initWithBaseURL:URLString parameters:parameters success:^(id responseObject) {
            success(responseObject);
        } failure:^(id responseObject) {
            failure(responseObject);
        }];

    } failure:^{
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            //SQLite
            
//            [SVProgressHUD dismiss];
//`
//            NSDictionary * data = [WebServiceCache getDataForService:URLString];
//            if(data){
//                success(data);
//            }else{
//                NSError * error = [[NSError alloc] initWithDomain:@"WebserviceCache" code:404 userInfo:@{@"Error reason": @"Offline data not available"}];
//                failure(error);
//            }
            
        });
        
    }];
    
    
}

@end
