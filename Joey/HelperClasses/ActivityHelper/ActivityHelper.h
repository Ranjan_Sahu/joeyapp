//
//  ActivityHelper.h
//  Joey
//
//  Created by werbwerks1 on 22/08/18.
//  Copyright © 2018 Auxilia. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ActivityHelper : NSObject

+ (ActivityHelper *)sharedInstance;

- (void)callServiceWithURL:(NSString *)URLString parameters:(id)parameters success:(void (^)(id responseObject))success failure:(void (^)(id responseObject))failure;


@end
