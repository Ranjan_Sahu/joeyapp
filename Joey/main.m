  //
//  main.m
//  Joey
//
//  Created by csl on 21/12/2016.
//  Copyright © 2016 Auxilia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
