//
//  VideoPlayerManager.m
//  VideoPlayerWrapperDemo
//
//  Created by webwerks on 5/14/18.
//  Copyright © 2018 Neo. All rights reserved.
//

#import "VideoPlayerManager.h"

@implementation VideoPlayerManager

UIViewController *parentController;

+ (VideoPlayerManager *)sharedInstance {
    
    static VideoPlayerManager *_sharedInstance = nil;
    static dispatch_once_t oncePredicate;
    
    dispatch_once(&oncePredicate, ^{
        _sharedInstance = [VideoPlayerManager new];
    });
    return _sharedInstance;
}

- (void)initilizePlayerwithView:(UIView *)pView andController: (UIViewController *)vController
{
    
    NSURL *url = [NSURL URLWithString:_videoUrl];
    if (!_videoUrl || _videoUrl.length==0) {
        NSLog(@"Url issing !");
        return;
    }
    
 
//    AVAsset *asset;
//    if ([[url scheme] isEqualToString:@"file"])
//    {
//        asset = [AVAsset assetWithURL:[NSURL fileURLWithPath:_videoUrl]];
//    }
//    else
//    {
//        if (!_videoUrl || _videoUrl.length==0) {
//            NSLog(@"Url issing !");
//            return;
//        }
//        asset = [AVAsset assetWithURL:[NSURL URLWithString:_videoUrl]];
//    }
//
//    AVPlayerItem *item = [AVPlayerItem playerItemWithAsset:asset];
    
    [self removePlayerInstance];
    if (!_player) {
        _player = [AVPlayer new];
    }
    
    if (_showOnlyThumbnail) {
        if (_videoThumbnail) {
            [_videoThumbnail removeFromSuperview];
            _videoThumbnail = nil;
        }
        _videoThumbnail = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, pView.frame.size.width, pView.frame.size.height)];
        _videoThumbnail.clipsToBounds = YES;
        _videoThumbnail.image = [UIImage imageNamed:@"video_player_icon"];
        _videoThumbnail.backgroundColor = [UIColor darkTextColor];
        _videoThumbnail.contentMode = UIViewContentModeCenter;
        [pView addSubview: _videoThumbnail];
        [pView sizeToFit];
        [pView setNeedsLayout];
        [pView setNeedsDisplay];
    }
    else {
    
        [_player initWithURL:url];
        _player.allowsExternalPlayback = YES;
        _playerController = [[AVPlayerViewController alloc] init];
        _playerController.player = _player;
        _playerController.showsPlaybackControls = YES;
        _player.actionAtItemEnd = AVPlayerActionAtItemEndNone;
        
        _playerController.view.frame = pView.bounds;
        _playerController.videoGravity = AVLayerVideoGravityResizeAspect;
        _playerController.view.backgroundColor = [UIColor clearColor];
        
        
        if (_videoThumbnail) {
            [_videoThumbnail removeFromSuperview];
            _videoThumbnail = nil;
        }
        _videoThumbnail = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, _playerController.view.frame.size.width, _playerController.view.frame.size.height)];
        _videoThumbnail.clipsToBounds = YES;
        _videoThumbnail.image = [UIImage imageNamed:@"video_player_icon"];
        _videoThumbnail.backgroundColor = [UIColor darkTextColor];
        _videoThumbnail.contentMode = UIViewContentModeCenter;
        
        [_playerController.view addSubview:_videoThumbnail];
        [pView addSubview: _playerController.view];
        [pView sizeToFit];
        [pView setNeedsLayout];
        [pView setNeedsDisplay];
        [_player seekToTime:kCMTimeZero];
        [_player play];
        
        parentController = vController;
        
        if (!_isStatusObserverSet) {
            [_player.currentItem addObserver:self forKeyPath:@"status" options:(NSKeyValueObservingOptionNew | NSKeyValueObservingOptionInitial) context:nil];
            _isStatusObserverSet = YES;
        }
        
        // observer to handle when app is ended playing
        [[NSNotificationCenter defaultCenter] addObserver:parentController selector:@selector(itemDidFinishPlaying:) name:AVPlayerItemDidPlayToEndTimeNotification object:_player.currentItem];
    }
    
}

- (void)playPause
{
    if (_player && _player.rate > 0 ) {
        [_player pause];
    }else{
        [_player play];
    }
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    AVPlayerItem *objItem = (AVPlayerItem *)object;
    
    if ([keyPath isEqualToString:@"status"])
    {
        switch (objItem.status) {
            case AVPlayerItemStatusFailed:
                [self statusFailed];
                break;
            case AVPlayerItemStatusReadyToPlay:
                [self statusReadyToPlay];
                break;
            case AVPlayerItemStatusUnknown:
                [self statusUnknown];
                break;
            default:
                break;
        }
    }
}

- (void) removePlayerInstance
{
    [_player pause];
    if (_isStatusObserverSet) {
        [_player.currentItem removeObserver:self forKeyPath:@"status" ];
        _isStatusObserverSet = NO;
    }
    
    if (_player.currentItem != nil) {
        [_player replaceCurrentItemWithPlayerItem:nil];
    }
    [_playerController.view removeFromSuperview];
    _playerController.player = nil;
    _playerController = nil;
}

//-(void)itemDidFinishPlaying:(NSNotification *) notification {
//    NSLog(@"itemDidFinishPlaying");
//    [_player seekToTime:kCMTimeZero];
//    [_player pause];
//}
//
//-(void) applicationWillResignActive:(NSNotification *)notification
//{
//    [_player pause];
//    if (_isStatusObserverSet) {
//        [_player.currentItem removeObserver:self forKeyPath:@"status" ];
//        _isStatusObserverSet = NO;
//    }
//}

- (void)statusFailed
{
    NSLog(@"statusFailed");
    NSLog(@"------player item failed:%@",_player.currentItem.error);
}
- (void)statusReadyToPlay
{
    NSLog(@"statusReadyToPlay");
    NSLog(@"------player item failed:%@",_player.currentItem.error);
    if (_videoThumbnail) {
        [_videoThumbnail removeFromSuperview];
        _videoThumbnail = nil;
    }
}
- (void)statusUnknown
{
    NSLog(@"statusUnknown");
    NSLog(@"------player item failed:%@",_player.currentItem.error);
}

//-(void) showActivitySpinner
//{
//        CGFloat kScreenWidth = _playerController.view.bounds.size.width;
//        CGFloat kScreenHeight = _playerController.view.bounds.size.height;
//        self.videoActivitySpinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
//        [self.videoActivitySpinner setCenter:CGPointMake(kScreenWidth/2.0, kScreenHeight/2.0)];
//        self.videoActivitySpinner.center = _playerController.view.center;
//        [self.videoActivitySpinner setHidesWhenStopped:YES];
//        self.videoActivitySpinner.transform = CGAffineTransformMakeScale(1.0, 1.0);
//        self.videoActivitySpinner.color = UIColorFromRGB(0x96BFCE);
//        [_playerController.view addSubview:self.videoActivitySpinner];
//
//        [self.videoActivitySpinner startAnimating];
//}

//-(void) hideSpinner {
//    if (self.videoActivitySpinner) {
//        [self.videoActivitySpinner stopAnimating];
//        [self.videoActivitySpinner removeFromSuperview];
//        self.videoActivitySpinner = nil;
//    }
//}

@end
