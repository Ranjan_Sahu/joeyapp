//
//  NSString+Utility.h
//  Joey
//
//  Created by webwerks on 30/03/18.
//  Copyright © 2018 Auxilia. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Utility)

-(BOOL)isNull;

@end
