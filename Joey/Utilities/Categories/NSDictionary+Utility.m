//
//  NSDictionary+Utility.m
//  Joey
//
//  Created by webwerks on 30/03/18.
//  Copyright © 2018 Auxilia. All rights reserved.
//

#import "NSDictionary+Utility.h"
# define getValue [self valueForKey:key]
#import "NSString+Utility.h"

@implementation NSDictionary (Utility)

-(BOOL)hasValue:(NSString *)key{
    
    if([getValue isKindOfClass:[NSNull class ]]) return NO;
    if([getValue isKindOfClass:[NSString class]]){
        NSString * data = getValue;
        return ![data isNull];
    }
    return YES;
}

@end
