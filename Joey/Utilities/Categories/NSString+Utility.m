//
//  NSString+Utility.m
//  Joey
//
//  Created by webwerks on 30/03/18.
//  Copyright © 2018 Auxilia. All rights reserved.
//

#import "NSString+Utility.h"

@implementation NSString (Utility)

-(BOOL)isNull{
    
    return
    ([self isKindOfClass:[NSNull class]] || [self isEqualToString:@""] || [self isEqualToString:@"null"] || [self isEqualToString:@"(null)"]) || ([self isKindOfClass:[NSNull null]]);
    
}

@end
