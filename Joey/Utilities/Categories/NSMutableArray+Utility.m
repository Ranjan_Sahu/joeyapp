//
//  NSMutableArray+Utility.m
//  Joey
//
//  Created by webwerks on 30/03/18.
//  Copyright © 2018 Auxilia. All rights reserved.
//

#import "NSMutableArray+Utility.h"
#import "NSString+Utility.h"

@implementation NSMutableArray (Utility)

-(void)addIfNotNull:(id)object{
    if([object isKindOfClass:[NSNull class]]) return;
    if([object isKindOfClass:[NSString class]]){
        if(![(NSString *)object isNull]){
            [self addObject:object];
            return;
        }
        return;
    }else{
        [self addObject:object];
    }
}

@end
