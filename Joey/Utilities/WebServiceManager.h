//
//  WebServiceManager.h
//  Joey
//
//  Created by csl on 2/3/2017.
//  Copyright © 2017 Auxilia. All rights reserved.
//

#import <Foundation/Foundation.h>

//#define API_URL @"https://www.auxiliababy.com/joey/"
#define API_URL @"http://dev.auxiliababy.com/"


typedef void (^WebServiceManagerCallback)(NSString *raw, id parsed, NSError *error);

@interface WebServiceManager : AFHTTPSessionManager

+ (id)sharedManager;
- (void)initWithBaseURL:(NSString *)URLString parameters:(id)parameters success:(void (^)(id responseObject))success failure:(void (^)(id responseObject))failure;
- (void)initWithBaseURL:(NSString *)URLString parameters:(id)parameters formData:(id)formData success:(void (^)(id responseObject))success failure:(void (^)(id responseObject))failure;

@end
