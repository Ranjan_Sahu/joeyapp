//
//  WebServiceCache.h
//  Joey
//
//  Created by webwerks on 03/04/18.
//  Copyright © 2018 Auxilia. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WebServiceCache : NSObject

+(void)saveData:(NSString *)data forService:(NSString *)url;
+(NSDictionary *)getDataForService:(NSString *)url;
+(void)invalidateData;


@end
