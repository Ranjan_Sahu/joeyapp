//
//  WebServiceCache.m
//  Joey
//
//  Created by webwerks on 03/04/18.
//  Copyright © 2018 Auxilia. All rights reserved.
//

#import "WebServiceCache.h"
#import "CommonHelper.h"

@implementation WebServiceCache

+(void)saveData:(id)data forService:(NSString *)url{
    [CommonHelper saveToDefaults:data withKey:url];
}

+(id)getDataForService:(NSString *)url{
   return  [CommonHelper getFromDefaultsWithKey:url];
}

+(void)invalidateData{
    [CommonHelper clearDefaults];
}

@end
