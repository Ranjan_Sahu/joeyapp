//
//  CommonHelper.h
//  Joey
//
//  Created by webwerks on 12/11/17.
//  Copyright © 2017 Auxilia. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CommonHelper : NSObject

+(CommonHelper*) sharedInstance;

-(void) showSpinner;
-(void) hideSpinner;
-(void)vibrateDevice;
-(NSString *)checkForNullValue:(NSString *)string;

// For nsuserdefaults
+(void)saveToDefaults:(NSString *)data withKey:(NSString *)key;
+(NSDictionary *)getFromDefaultsWithKey:(NSString *)key;
+(void)clearDefaults;

// For Reachability
+(void)checkInternetWithSuccess:(void (^)())success failure:(void (^)())failure;

// Colors
+(NSMutableArray *)getAdultTeethColors;
+(NSMutableArray *)getChildTeethColors;

// generateThumbnailImage
-(UIImage *)generateThumbnailImageFromVideoWithUrl:(NSURL *)videoUrl;
-(void)generateThumbnailImageFromVideoWithUrl:(NSURL *)videoUrl inView:(UIButton *)btn;
// video Compression
-(void)compressVideo:(NSURL*)inputURL outputURL:(NSURL*)outputURL handler:(void (^)(AVAssetExportSession*))completion;
// Clear temp directory
- (void)clearTmpDirectory;
// Check for Wifi 
- (BOOL) checkforWifiNetwork;
@end
