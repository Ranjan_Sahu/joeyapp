//
//  ChartAxisValueFormatter.m
//  Joey
//
//  Created by csl on 20/2/2017.
//  Copyright © 2017 Auxilia. All rights reserved.
//

#import "ChartAxisValueFormatter.h"

@implementation ChartAxisValueFormatter
{
    NSArray *labels;
    __weak BarLineChartViewBase *_chart;
}

- (id)initForChart:(BarLineChartViewBase *)chart type:(enum ChartAxisType)type
{
    self = [super init];
    if (self)
    {
        self->_chart = chart;
        
        switch (type) {
            case ChartAxisTypeForDay:
                labels = @[
                           @"0",
                           @"1",
                           @"2",
                           @"3",
                           @"4",
                           @"5",
                           @"6",
                           @"7",
                           @"8",
                           @"9",
                           @"10",
                           @"11",
                           @"12",
                           @"13",
                           @"14",
                           @"15",
                           @"16",
                           @"17",
                           @"18",
                           @"19",
                           @"20",
                           @"21",
                           @"22",
                           @"23",
                           @"24",
                           ];
                break;
                
            case ChartAxisTypeForWeek:
                labels = @[
                           LocalizedString(@"mon", nil),
                           LocalizedString(@"tue", nil),
                           LocalizedString(@"wed", nil),
                           LocalizedString(@"thu", nil),
                           LocalizedString(@"fri", nil),
                           LocalizedString(@"sat", nil),
                           LocalizedString(@"sun", nil),
                           ];
                break;
                
            case ChartAxisTypeForMonth:
                labels = @[
                           @"1",
                           @"2",
                           @"3",
                           @"4",
                           @"5",
                           @"6",
                           @"7",
                           @"8",
                           @"9",
                           @"10",
                           @"11",
                           @"12",
                           @"13",
                           @"14",
                           @"15",
                           @"16",
                           @"17",
                           @"18",
                           @"19",
                           @"20",
                           @"21",
                           @"22",
                           @"23",
                           @"24",
                           @"25",
                           @"26",
                           @"27",
                           @"28",
                           @"29",
                           @"30",
                           @"31",
                           ];
                break;
                
            case ChartAxisTypeForPooDay:
                labels = @[
                           @"",
                           LocalizedString(@"hard", nil),
                           LocalizedString(@"soft", nil),
                           LocalizedString(@"runny", nil),
                           LocalizedString(@"mushy", nil),
                           LocalizedString(@"loose", nil),
                           LocalizedString(@"diarrhea", nil),
                           LocalizedString(@"mucus", nil),
                           @"",
                           ];
                break;
            case ChartAxisTypeForPooWeeWeek:
                labels = @[
                           @"",
                           LocalizedString(@"mon", nil),
                           LocalizedString(@"tue", nil),
                           LocalizedString(@"wed", nil),
                           LocalizedString(@"thu", nil),
                           LocalizedString(@"fri", nil),
                           LocalizedString(@"sat", nil),
                           LocalizedString(@"sun", nil),
                           @"",
                           ];
                break;
                
            case ChartAxisTypeForPooWeeMonth:
                labels = @[
                           @"",
                           @"1",
                           @"2",
                           @"3",
                           @"4",
                           @"5",
                           @"6",
                           @"7",
                           @"8",
                           @"9",
                           @"10",
                           @"11",
                           @"12",
                           @"13",
                           @"14",
                           @"15",
                           @"16",
                           @"17",
                           @"18",
                           @"19",
                           @"20",
                           @"21",
                           @"22",
                           @"23",
                           @"24",
                           @"25",
                           @"26",
                           @"27",
                           @"28",
                           @"29",
                           @"30",
                           @"31",
                           @"",
                           @"",
                           @"",
                           ];
                break;
                
            case ChartAxisTypeForMedical:
                labels = @[
                           @"",
                           @"S1",
                           @"S2",
                           @"S3",
                           @"M1",
                           @"M2",
                           @"M3",
                           @"M4",
                           @"M5",
                           @"M6",
                           @"M7",
                           @"",
                           ];
            default:
                break;
                
            case ChartAxisTypeForMedicalWeek:
                labels = @[
                           @"",
                           LocalizedString(@"mon", nil),
                           LocalizedString(@"tue", nil),
                           LocalizedString(@"wed", nil),
                           LocalizedString(@"thu", nil),
                           LocalizedString(@"fri", nil),
                           LocalizedString(@"sat", nil),
                           LocalizedString(@"sun", nil),
                           @"",
                           ];
                break;

        }
    }
    return self;
}

- (NSString *)stringForValue:(double)value
                        axis:(ChartAxisBase *)axis
{
    return labels[(int)value];
}

@end

