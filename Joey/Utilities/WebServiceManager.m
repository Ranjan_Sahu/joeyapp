//
//  WebServiceManager.m
//  Joey
//
//  Created by csl on 2/3/2017.
//  Copyright © 2017 Auxilia. All rights reserved.
//

#import "AppDelegate.h"
#import "WebServiceManager.h"
#import "Reachability.h"
#import "WebServiceCache.h"



@interface WebServiceManager ()

@end

@implementation WebServiceManager

static WebServiceManager *sharedInstance;

+ (WebServiceManager *) sharedManager
{
    if(sharedInstance == nil)
    {
        sharedInstance = [[WebServiceManager alloc] initWithBaseURL:[NSURL URLWithString:API_URL]];
        sharedInstance.responseSerializer = [AFHTTPResponseSerializer serializer];
        sharedInstance.responseSerializer.acceptableContentTypes = [NSSet setWithObjects: @"application/json", nil];
    }
    
    NSLog(@"sharedInstance=%@", sharedInstance);
    return sharedInstance;
}

#pragma mark - GET Service

- (void)initWithBaseURL:(NSString *)URLString parameters:(id)parameters success:(void (^)(id responseObject))success failure:(void (^)(id responseObject))failure
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    NSLog(@"URL is %@",URLString);
    NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
    NSString *currentAppVersion = infoDictionary[@"CFBundleShortVersionString"];
    NSString *accessToken = @"";
    if([[NSUserDefaults standardUserDefaults] objectForKey:@"access_token"]) accessToken = [[NSUserDefaults standardUserDefaults] objectForKey:@"access_token"];
    [parameters setValue:@"ios" forKey:@"mobile_platform"];
    [parameters setValue:currentAppVersion forKey:@"v"];
    [parameters setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"language"] forKey:@"language"];
    [parameters setValue:[NSTimeZone localTimeZone].name forKey:@"timezone"];
    [parameters setValue:accessToken forKey:@"access_token"];
    NSLog(@"parameters=%@", parameters);
    
    [CommonHelper checkInternetWithSuccess:^{
        
        [self callGetServiceWithURL:URLString withParameters:parameters success:^(id responseObject) {
            
            //[WebServiceCache saveData:responseObject forService:URLString];//to save data in cache
            
            success(responseObject);
        } failure:^(id responseObject) {
            failure(responseObject);
        }];
        
    } failure:^{
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [SVProgressHUD dismiss];
            
            NSDictionary * data = [WebServiceCache getDataForService:URLString];
            if(data){
                success(data);
            }else{
                NSError * error = [[NSError alloc] initWithDomain:@"WebserviceCache" code:404 userInfo:@{@"Error reason": @"Offline data not available"}];
                failure(error);
            }
            
        });
        
    }];
    
   
}

-(void) callGetServiceWithURL:(NSString *)URLString withParameters:(NSDictionary *)parameters success:(void (^)(id responseObject))success failure:(void (^)(id responseObject))failure{
    
    [self GET:URLString parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSString *responseString = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        [WebServiceCache saveData:responseString forService:URLString];
        id jsonData = [responseString dataUsingEncoding:NSUTF8StringEncoding];
        id result = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:nil];
        //NSLog(@"result=%@", result);
        
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        [SVProgressHUD dismiss];
        success(result);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"error=%@", error.localizedDescription);
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        [SVProgressHUD dismiss];
        if(failure) failure(error);
    }];
    
}


#pragma mark - POST Service

- (void)initWithBaseURL:(NSString *)URLString parameters:(id)parameters formData:(id)formData success:(void (^)(id responseObject))success failure:(void (^)(id responseObject))failure
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
    NSString *currentAppVersion = infoDictionary[@"CFBundleShortVersionString"];
    NSString *accessToken = @"";
    if([[NSUserDefaults standardUserDefaults] objectForKey:@"access_token"]) accessToken = [[NSUserDefaults standardUserDefaults] objectForKey:@"access_token"];
    
    [parameters setValue:currentAppVersion forKey:@"v"];
    [parameters setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"language"] forKey:@"language"];
    [parameters setValue:[NSTimeZone localTimeZone].name forKey:@"timezone"];
    [parameters setValue:accessToken forKey:@"access_token"];
    NSLog(@"parameters=%@", parameters);
    
    [CommonHelper checkInternetWithSuccess:^{
        
        [self callPostServiceWithURL:URLString withParameters:parameters formData:formData success:^(id responseObject) {
            //[WebServiceCache saveData:responseObject forService:URLString];
            success(responseObject);
        } failure:^(id responseObject) {
            failure(responseObject);
        }];
        
    } failure:^{
        
        id data = [WebServiceCache getDataForService:URLString];
        if(data){
            success(data);
        }else{
            NSError * error = [[NSError alloc] initWithDomain:@"WebserviceCache" code:404 userInfo:@{@"Error reason": @"Offline data not available"}];
            failure(error);
        }
        
    }];
    
    
}



-(void)callPostServiceWithURL:(NSString *)URLString withParameters:(NSDictionary *)parameters formData:(id)formData success:(void (^)(id responseObject))success failure:(void (^)(id responseObject))failure{
    
    [self POST:URLString parameters:parameters constructingBodyWithBlock:formData progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSString *responseString = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        [WebServiceCache saveData:responseString forService:URLString];
        id jsonData = [responseString dataUsingEncoding:NSUTF8StringEncoding];
        id result = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:nil];
        NSLog(@"result=%@", result);
        
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        [SVProgressHUD dismiss];
        success(result);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"error=%@", error);
        
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        [SVProgressHUD dismiss];
        if(failure) failure(error);
    }];
    
}

@end
