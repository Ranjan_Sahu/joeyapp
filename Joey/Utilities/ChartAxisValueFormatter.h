//
//  ChartAxisValueFormatter.h
//  Joey
//
//  Created by csl on 20/2/2017.
//  Copyright © 2017 Auxilia. All rights reserved.
//

#import <UIKit/UIKit.h>
@import Charts;

typedef enum ChartAxisType {
    ChartAxisTypeForDay = 0,
    ChartAxisTypeForWeek = 1,
    ChartAxisTypeForMonth = 2,
    ChartAxisTypeForPooDay = 3,
    ChartAxisTypeForPooWeeWeek = 4,
    ChartAxisTypeForPooWeeMonth = 5,
    ChartAxisTypeForMedical = 6,
    ChartAxisTypeForMedicalWeek = 7,
    
} ChartAxisType;

@interface ChartAxisValueFormatter : NSObject <IChartAxisValueFormatter>

- (id)initForChart:(BarLineChartViewBase *)chart type:(enum ChartAxisType)type;

@end
