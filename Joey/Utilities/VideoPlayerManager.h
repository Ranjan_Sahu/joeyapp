//
//  VideoPlayerManager.h
//  VideoPlayerWrapperDemo
//
//  Created by webwerks on 5/14/18.
//  Copyright © 2018 Neo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVKit/AVKit.h>
#import <AVFoundation/AVFoundation.h>
#import <MediaPlayer/MediaPlayer.h>

@interface VideoPlayerManager : NSObject

@property (nonatomic, weak) NSString *videoUrl;
@property (nonatomic) BOOL isStatusObserverSet;
@property (nonatomic) BOOL isPlabackObserverSet;
@property (nonatomic, strong) AVPlayerViewController *playerController;
@property (nonatomic, strong) AVPlayer *player;
@property (nonatomic, strong) UIImageView *videoThumbnail;
@property (nonatomic, assign) BOOL showOnlyThumbnail;
//@property (nonatomic, strong) UIActivityIndicatorView *videoActivitySpinner;

+ (VideoPlayerManager *)sharedInstance;
- (void)playPause;
- (void)initilizePlayerwithView:(UIView *)pView  andController: (UIViewController *)vController;
- (void)removePlayerInstance;

@end
