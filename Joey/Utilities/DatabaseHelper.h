//
//  DatabaseHelper.h
//  Joey
//
//  Created by webwerks on 3/12/18.
//  Copyright © 2018 Auxilia. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DatabaseHelper : NSObject

+ (DatabaseHelper *) sharedManager;

@end
