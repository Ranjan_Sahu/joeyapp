//
//  CommonHelper.m
//  Joey
//
//  Created by webwerks on 12/11/17.
//  Copyright © 2017 Auxilia. All rights reserved.
//

#import "CommonHelper.h"
#import <AudioToolbox/AudioToolbox.h>
#import "Reachability.h"
#import "WebServiceManager.h"

#define suiteName @"WebServiceCache"

@interface CommonHelper ()
@property(nonatomic, strong) UIActivityIndicatorView *spinner;
@end

@implementation CommonHelper



static CommonHelper *sharedInstance;

+(CommonHelper*) sharedInstance
{
    if (!sharedInstance)
    {
        sharedInstance = [[CommonHelper alloc] init];
    }
    return sharedInstance;
}

-(void) showSpinner {
  UIWindow *window =   [[UIApplication sharedApplication] keyWindow];
   CGFloat kScreenWidth = UIScreen.mainScreen.bounds.size.width;
    CGFloat kScreenHeight = UIScreen.mainScreen.bounds.size.height;
    self.spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [self.spinner setCenter:CGPointMake(kScreenWidth/2.0, kScreenHeight/2.0)];
    self.spinner.center = window.center;
    [self.spinner setHidesWhenStopped:YES];
    self.spinner.transform = CGAffineTransformMakeScale(1.0, 1.0);
    self.spinner.color = UIColorFromRGB(0x96BFCE);
    [window addSubview:self.spinner];
    
    [self.spinner startAnimating];
}

-(void) hideSpinner {
    if (self.spinner) {
        [self.spinner stopAnimating];
        [self.spinner removeFromSuperview];
    }
}

-(void)vibrateDevice {
    if([[UIDevice currentDevice].model isEqualToString:@"iPhone"]){
        AudioServicesPlaySystemSound (kSystemSoundID_Vibrate);
    }else{
        AudioServicesPlayAlertSound (kSystemSoundID_Vibrate);
    }
}

-(NSString *)checkForNullValue:(NSString *)string {
    
    if ([string isKindOfClass:[NSNull class]] || [string length]==0 || [string containsString:@"null"]) {
        return @"";
    }
    else if ([string length] > 0) {
        return string;
    }
    else
        return @"";
    
}

- (UIImage *)imageByScalingProportionallyToSize:(CGSize)targetSize withSourceImage:(UIImage *)sourceImg {
    
    UIImage *sourceImage = sourceImg;
    UIImage *newImage = nil;
    
    CGSize imageSize = sourceImage.size;
    CGFloat width = imageSize.width;
    CGFloat height = imageSize.height;
    
    CGFloat targetWidth = targetSize.width;
    CGFloat targetHeight = targetSize.height;
    
    CGFloat scaleFactor = 0.0;
    CGFloat scaledWidth = targetWidth;
    CGFloat scaledHeight = targetHeight;
    
    CGPoint thumbnailPoint = CGPointMake(0.0,0.0);
    
    if (CGSizeEqualToSize(imageSize, targetSize) == NO) {
        
        CGFloat widthFactor = targetWidth / width;
        CGFloat heightFactor = targetHeight / height;
        
        if (widthFactor < heightFactor)
            scaleFactor = widthFactor;
        else
            scaleFactor = heightFactor;
        
        scaledWidth  = width * scaleFactor;
        scaledHeight = height * scaleFactor;
        
        // center the image
        
        if (widthFactor < heightFactor) {
            thumbnailPoint.y = (targetHeight - scaledHeight) * 0.5;
        } else if (widthFactor > heightFactor) {
            thumbnailPoint.x = (targetWidth - scaledWidth) * 0.5;
        }
    }
    
    
    // this is actually the interesting part:
    
    UIGraphicsBeginImageContext(targetSize);
    
    CGRect thumbnailRect = CGRectZero;
    thumbnailRect.origin = thumbnailPoint;
    thumbnailRect.size.width  = scaledWidth;
    thumbnailRect.size.height = scaledHeight;
    
    [sourceImage drawInRect:thumbnailRect];
    
    newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    if(newImage == nil) NSLog(@"could not scale image");
    
    
    return newImage ;
}

#pragma mark - NSUserDefaults common methods

+(void)saveToDefaults:(NSString *)data withKey:(NSString *)key{
    
    NSUserDefaults * defaults = [self getDefault];
    [defaults setObject:data forKey:key];
    [defaults synchronize];
}

+(NSDictionary *)getFromDefaultsWithKey:(NSString *)key{
    
    NSUserDefaults * defaults = [self getDefault];
    NSString * responseString = [defaults valueForKey:key];
    id jsonData = [responseString dataUsingEncoding:NSUTF8StringEncoding];
    if(jsonData){
        id result = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:nil];
        return result;
    }
    
    return nil;
    
}

+(NSUserDefaults *)getDefault{
    
    return  [NSUserDefaults standardUserDefaults];//[[NSUserDefaults alloc] initWithSuiteName:suiteName];
    
}

+(void)clearDefaults{
    [[self getDefault] removeSuiteNamed:suiteName];
}

#pragma mark - Reachability

+(void)checkInternetWithSuccess:(void (^)())success failure:(void (^)())failure{
    
    Reachability* reach = [Reachability reachabilityWithHostname:@"www.google.com"];
    reach.reachableBlock = ^(Reachability*reach)
    {

        dispatch_async(dispatch_get_main_queue(), ^{
            success();
        });
    };
    
    reach.unreachableBlock = ^(Reachability*reach)
    {
        NSLog(@"Internet connection failed");
        dispatch_async(dispatch_get_main_queue(), ^{
        failure();
             });
    };
    
    // Start the notifier, which will cause the reachability object to retain itself!
    [reach startNotifier];
}


#pragma mark - Colors

+(NSMutableArray *)getAdultTeethColors{
    
   return [[NSMutableArray alloc] initWithObjects:UIColorFromRGB(0xDCCEDC), UIColorFromRGB(0xDCCEDC), UIColorFromRGB(0xDCCEDC), UIColorFromRGB(0xDCCEDC), UIColorFromRGB(0xDCCEDC), UIColorFromRGB(0xDCCEDC), UIColorFromRGB(0xDCCEDC), UIColorFromRGB(0xDCCEDC), UIColorFromRGB(0xF4EA9E), UIColorFromRGB(0xF4EA9E), UIColorFromRGB(0xF4EA9E), UIColorFromRGB(0xF4EA9E), UIColorFromRGB(0xF9C99B), UIColorFromRGB(0xF9C99B), UIColorFromRGB(0xF9C99B), UIColorFromRGB(0xF9C99B), UIColorFromRGB(0xF9C99B), UIColorFromRGB(0xF9C99B), UIColorFromRGB(0xF9C99B), UIColorFromRGB(0xF9C99B), UIColorFromRGB(0xF9C99B), UIColorFromRGB(0xF9C99B), UIColorFromRGB(0xF9C99B), UIColorFromRGB(0xF9C99B), UIColorFromRGB(0xBDD6D7), UIColorFromRGB(0xBDD6D7), UIColorFromRGB(0xBDD6D7), UIColorFromRGB(0xBDD6D7), UIColorFromRGB(0xCADAA9), UIColorFromRGB(0xCADAA9), UIColorFromRGB(0xCADAA9), UIColorFromRGB(0xCADAA9), nil];
    
}

+(NSMutableArray *)getChildTeethColors{
    
     return [[NSMutableArray alloc] initWithObjects:UIColorFromRGB(0xF4EA9E), UIColorFromRGB(0xF4EA9E), UIColorFromRGB(0xF9C99B), UIColorFromRGB(0xF9C99B), UIColorFromRGB(0xF9C99B), UIColorFromRGB(0xF9C99B), UIColorFromRGB(0xF4D2D5), UIColorFromRGB(0xF4D2D5), UIColorFromRGB(0xBDD6D7), UIColorFromRGB(0xBDD6D7), UIColorFromRGB(0xBDD6D7), UIColorFromRGB(0xBDD6D7), UIColorFromRGB(0xCADAA9), UIColorFromRGB(0xCADAA9), UIColorFromRGB(0xCADAA9), UIColorFromRGB(0xCADAA9), UIColorFromRGB(0xDCCEDC), UIColorFromRGB(0xDCCEDC), UIColorFromRGB(0xDCCEDC), UIColorFromRGB(0xDCCEDC), nil];
    
}

-(UIImage *)generateThumbnailImageFromVideoWithUrl:(NSURL *)videoUrl
{
    
    NSError *err;
    AVURLAsset *asset = [AVURLAsset assetWithURL:videoUrl];
    AVAssetImageGenerator *imgGenerator = [AVAssetImageGenerator assetImageGeneratorWithAsset:asset];
    CGImageRef thImg = [imgGenerator copyCGImageAtTime:CMTimeMake(0, 1) actualTime:nil error:&err];
    UIImage *tImg = [UIImage imageWithCGImage:thImg];
    return tImg;
    
}

-(void)generateThumbnailImageFromVideoWithUrl:(NSURL *)videoUrl inView:(UIButton *)btn
{
    [btn setImage:[UIImage imageNamed:@"video_player_icon"] forState:UIControlStateNormal];
    btn.backgroundColor = [UIColor darkTextColor];
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        NSError *err;
        AVURLAsset *asset = [AVURLAsset assetWithURL:videoUrl];
        AVAssetImageGenerator *imgGenerator = [AVAssetImageGenerator assetImageGeneratorWithAsset:asset];
        CGImageRef thImg = [imgGenerator copyCGImageAtTime:CMTimeMake(0, 1) actualTime:nil error:&err];
        UIImage *tImg = [UIImage imageWithCGImage:thImg];
        dispatch_async(dispatch_get_main_queue(), ^(void){
            [btn setImage:tImg forState:UIControlStateNormal];
        });
    });
}

- (void)compressVideo:(NSURL*)inputURL outputURL:(NSURL*)outputURL handler:(void (^)(AVAssetExportSession*))completion
{
    AVURLAsset *urlAsset = [AVURLAsset URLAssetWithURL:inputURL options:nil];
    AVAssetExportSession *exportSession = [[AVAssetExportSession alloc] initWithAsset:urlAsset presetName:AVAssetExportPresetLowQuality];
    exportSession.outputURL = outputURL;
    exportSession.outputFileType = AVFileTypeQuickTimeMovie;
    exportSession.shouldOptimizeForNetworkUse = YES;
    [exportSession exportAsynchronouslyWithCompletionHandler:^{
        completion(exportSession);
    }];
}

- (void)clearTmpDirectory
{
    NSArray* tmpDirectory = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:NSTemporaryDirectory() error:NULL];
    for (NSString *file in tmpDirectory) {
        [[NSFileManager defaultManager] removeItemAtPath:[NSString stringWithFormat:@"%@%@", NSTemporaryDirectory(), file] error:NULL];
    }
}

- (BOOL) checkforWifiNetwork
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    [reachability startNotifier];
    
    NetworkStatus status = [reachability currentReachabilityStatus];
    if(status == NotReachable){
        //No internet
        return NO;
    }else if (status == ReachableViaWiFi){
        //WiFi
        return YES;
    }else if (status == ReachableViaWWAN){
        //3G
        return NO;
    }
    return NO;
}
@end
