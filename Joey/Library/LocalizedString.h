//
//  LocalizedString.h
//  PowerWatch
//
//  Created by csl on 7/9/2017.
//  Copyright © 2017 Matrix Industries. All rights reserved.
//

#import <Foundation/Foundation.h>

#define LocalizedString(key, comment) [[LocalizedString sharedLocalSystem] localizedStringForKey:(key)]
#define LocalizationSetLanguage(language) [[LocalizedString sharedLocalSystem] setLanguage:(language)]

@interface LocalizedString : NSObject

+ (LocalizedString *)sharedLocalSystem;
- (NSString *)localizedStringForKey:(NSString *)key;
- (void)setLanguage:(NSString *)lang;

@end
