//
//  UITabBarController+hidable.m
//  Foodstep
//
//  Created by csl on 4/3/14.
//  Copyright (c) 2014 Foodstep. All rights reserved.
//

#import "AppDelegate.h"
#import "UITabBarController+hidable.h"

#define TABBAR_HEIGHT (49)

BOOL isAnimating;
CGRect originalFrame;
int currentIndex;
float scale;

@implementation UITabBarController (hidable)

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if(appDelegate.tabbar == nil) appDelegate.tabbar = self;
    self.delegate = self;
    self.selectedIndex = 1;
    
    UITabBarItem *tabBarItem1 = [self.tabBar.items objectAtIndex:0];
    UITabBarItem *tabBarItem2 = [self.tabBar.items objectAtIndex:1];
    UITabBarItem *tabBarItem3 = [self.tabBar.items objectAtIndex:2];
    UITabBarItem *tabBarItem4 = [self.tabBar.items objectAtIndex:3];
    UITabBarItem *tabBarItem5 = [self.tabBar.items objectAtIndex:4];
    
    [[UITabBarItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:UIColorFromRGB(0x8D8082), NSForegroundColorAttributeName, [UIFont fontWithName:@"SFUIDisplay-Medium" size:14.0f], NSFontAttributeName, nil] forState:UIControlStateSelected];
    
    tabBarItem1.title = LocalizedString(@"title_timeline", nil);
    tabBarItem2.title = LocalizedString(@"title_activities", nil);
    tabBarItem3.title = LocalizedString(@"title_milestones", nil);
    tabBarItem4.title = LocalizedString(@"title_gallary", nil);
    tabBarItem5.title = LocalizedString(@"title_message", nil);
    
    tabBarItem1.selectedImage = [[UIImage imageNamed:@"tabbar_icon_timeline_highlight"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    tabBarItem2.selectedImage = [[UIImage imageNamed:@"tabbar_icon_activities_highlight"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    tabBarItem3.selectedImage = [[UIImage imageNamed:@"tabbar_icon_milestones_highlight"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    tabBarItem4.selectedImage = [[UIImage imageNamed:@"tabbar_icon_gallary_highlight"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    tabBarItem5.selectedImage = [[UIImage imageNamed:@"tabbar_icon_message_highlight"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
}

/*- (void)setTabBarHidden:(BOOL)hidden
{
    if(isAnimating) return;
    originalFrame = self.tabBar.frame;
    
    if(hidden)
    {
        if(!self.tabBar.hidden)
        {
            isAnimating = true;
            [UIView animateWithDuration:0.3 animations:^{
                self.tabBar.frame = CGRectMake(self.tabBar.frame.origin.x, originalFrame.origin.y + TABBAR_HEIGHT, self.tabBar.frame.size.width, self.tabBar.frame.size.height);
            } completion:^(BOOL finished) {
                isAnimating = false;
                self.tabBar.hidden = YES;
            }];
        }
    }
    else
    {
        if(self.tabBar.hidden)
        {
            isAnimating = true;
            self.tabBar.hidden = NO;
            [UIView animateWithDuration:0.3 animations:^{
                self.tabBar.frame = CGRectMake(self.tabBar.frame.origin.x, originalFrame.origin.y - TABBAR_HEIGHT, self.tabBar.frame.size.width, self.tabBar.frame.size.height);
            } completion:^(BOOL finished) {
                isAnimating = false;
            }];
        }
    }
}*/

- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController
{
    currentIndex = (int)self.selectedIndex;
}

- (BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController
{
    return YES;
}

@end
