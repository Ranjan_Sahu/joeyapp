//
//  ContentViewController.m
//  KDViewPager
//
//  Created by kyle on 16/4/19.
//  Copyright © 2016年 kyleduo. All rights reserved.
//

#import "ContentViewController.h"
#import <AsyncImageView/AsyncImageView.h>
#import "NSString+Utility.h"
#import "VideoPlayerManager.h"

@interface ContentViewController ()
{
    CGRect vFrame;
}
@property (nonatomic, strong) NSString *photoURL;
@property (nonatomic,strong) AsyncImageView *imgPhoto;
@property (nonatomic,strong) VideoPlayerManager *vManager;
@end

@implementation ContentViewController
@synthesize imgPhoto;


-(instancetype)initWithImage:(NSString *)url withFrame:(CGRect )frame
{
    self = [super init];
    if (self) {
        
        vFrame = frame;
//        UIButton *btnPhoto = [[UIButton alloc] initWithFrame:CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, frame.size.height/2)];
//        [btnPhoto addTarget:self action:@selector(showPhotoAction:) forControlEvents:UIControlEventTouchUpInside];

        NSArray *imgExt = [NSArray arrayWithObjects:@"mp4",@"mov",@"m3u8", nil];
        NSURL *mediaUrl = [NSURL URLWithString:url];
        NSString *pathExt = [mediaUrl pathExtension];
        
        if ([imgExt containsObject:pathExt]) {
            [self setVideo:url];
        }else {
            [self setImage:url];
        }
        
//        [self.view addSubview:btnPhoto];
        
//        if ([imgExt containsObject:pathExt]) {
//            //video
//            [_vManager removePlayerInstance];
//            _vManager = [VideoPlayerManager new];
//            [self setVideo:url];
//            [self.view addSubview:btnPhoto];
//        }else {
//            //image
//            imgPhoto = [[AsyncImageView alloc] initWithFrame:CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, frame.size.height/2)];
//            imgPhoto.contentMode = UIViewContentModeScaleAspectFit;
//            imgPhoto.clipsToBounds = YES;
//            imgPhoto.image = [UIImage imageNamed:@"bg_photo"];
//            [self setImage:url];
//            [self.view addSubview:imgPhoto];
//            [self.view addSubview:btnPhoto];
//        }
//
//        [self.view addSubview:btnPhoto];
        
//        imgPhoto = [[AsyncImageView alloc] initWithFrame:CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, frame.size.height/2)];
//        imgPhoto.contentMode = UIViewContentModeScaleAspectFit;
//        //imgPhoto.backgroundColor = UIColorFromRGB(0xE0D3CB);
//        //imgPhoto.backgroundColor = [UIColor clearColor];
//        imgPhoto.clipsToBounds = YES;
//        imgPhoto.image = [UIImage imageNamed:@"bg_photo"];
//
//        [self setImage:url];
//        [self.view addSubview:imgPhoto];
//        [self.view addSubview:btnPhoto];
        
    }
    return self;
}

-(void)setImage:(NSString *) url{
    if(![url isNull]){
        
        for (UIView *vw in self.view.subviews) {
                [vw removeFromSuperview];
        }
        
        //dispatch_async(dispatch_get_main_queue(), ^{
            imgPhoto = [[AsyncImageView alloc] initWithFrame:CGRectMake(vFrame.origin.x, vFrame.origin.y, vFrame.size.width, vFrame.size.height/2)];
            imgPhoto.contentMode = UIViewContentModeScaleAspectFit;
            imgPhoto.clipsToBounds = YES;
            imgPhoto.image = [UIImage imageNamed:@"bg_photo"];
            imgPhoto.imageURL = [NSURL URLWithString:url];
            _photoURL = url;
            [self.view addSubview:imgPhoto];
        //});
        
        UIButton *btnPhoto = [[UIButton alloc] initWithFrame:CGRectMake(vFrame.origin.x, vFrame.origin.y, vFrame.size.width, vFrame.size.height/2)];
        [btnPhoto addTarget:self action:@selector(showPhotoAction:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:btnPhoto];
        
    }else{
        imgPhoto.image = [UIImage imageNamed:@"bg_photo"];
    }
}

-(void)setVideo:(NSString *) url{
    if(![url isNull]){
        
        for (UIView *vw in self.view.subviews) {
                [vw removeFromSuperview];
        }
        
        //dispatch_async(dispatch_get_main_queue(), ^{
        [_vManager removePlayerInstance];
        _vManager = [VideoPlayerManager new];
        _vManager.videoUrl = url;
        _vManager.showOnlyThumbnail = YES;
        [_vManager initilizePlayerwithView:self.view andController:self];
        [_vManager.player pause];
        //});
        
        UIButton *btnPhoto = [[UIButton alloc] initWithFrame:CGRectMake(vFrame.origin.x, vFrame.origin.y, vFrame.size.width, vFrame.size.height/2)];
        [btnPhoto addTarget:self action:@selector(showPhotoAction:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:btnPhoto];
        
    }
}

- (void)showPhotoAction:(UIButton *)btn{
    if(![_originalImage  isNull]){
        [self.delegate pagerSelectedWithUrl:_originalImage];
    }else{
        [self.delegate pagerSelectedWithUrl:_photoURL];
    }
}

#pragma mark - VideoManager Observer Handler
-(void)itemDidFinishPlaying:(NSNotification *) notification {
    NSLog(@"itemDidFinishPlaying");
    [_vManager.player pause];
    [_vManager.player seekToTime:kCMTimeZero];
}

-(void) applicationWillResignActive:(NSNotification *)notification {
    [_vManager.player pause];
    if (_vManager.isStatusObserverSet) {
        [_vManager.player.currentItem removeObserver:self forKeyPath:@"status" ];
        _vManager.isStatusObserverSet = NO;
    }
}

@end
