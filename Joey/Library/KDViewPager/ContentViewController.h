//
//  ContentViewController.h
//  KDViewPager
//
//  Created by kyle on 16/4/19.
//  Copyright © 2016年 kyleduo. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PagerSelectedDelegate

-(void)pagerSelectedWithUrl:(NSString *)url;

@end

@interface ContentViewController : UIViewController
@property (nonatomic, assign) NSUInteger index;
@property (nonatomic, assign) NSString * originalImage;
-(instancetype)initWithImage:(NSString *)url withFrame:(CGRect )frame;
-(void)setImage:(NSString *) url;
-(void)setVideo:(NSString *) url;
@property id<PagerSelectedDelegate> delegate;

@end
