//
//  UITabBarController+hidable.h
//  Foodstep
//
//  Created by csl on 4/3/14.
//  Copyright (c) 2014 Foodstep. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITabBarController (hidable) <UITabBarControllerDelegate>

//- (void)setTabBarHidden:(BOOL)hidden;

@end
