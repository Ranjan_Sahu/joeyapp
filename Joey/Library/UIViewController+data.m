//
//  UIViewController+data.m
//  Joey
//
//  Created by csl on 8/1/2017.
//  Copyright © 2017 Auxilia. All rights reserved.
//

#import <objc/runtime.h>
#import "UIViewController+data.h"

@implementation UIViewController (data)
static char DATA_PROPERTY_KEY;

@dynamic data;

- (void)setData:(NSMutableDictionary *)data
{
    objc_setAssociatedObject(self, &DATA_PROPERTY_KEY, data, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (id)data
{
    return (id)objc_getAssociatedObject(self, &DATA_PROPERTY_KEY);
}

@end
