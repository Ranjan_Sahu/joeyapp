//
//  TeethButton.h
//  RotateButton
//
//  Created by webwerks on 27/03/18.
//  Copyright © 2018 Manish Malviya. All rights reserved.
//

#import <UIKit/UIKit.h>

IB_DESIGNABLE

@interface TeethButton : UIButton

@property IBInspectable double angle;
@property IBInspectable long teethId;
 @property (strong,nonatomic) IBInspectable UIImage *unselectedImage;
 @property (strong,nonatomic) IBInspectable UIImage *selectedImage;
@property (strong,nonatomic) NSDictionary * data;
 @property BOOL isSelected;

 -(void)toggle;
-(void)showTip:(NSString *)text;

@end
