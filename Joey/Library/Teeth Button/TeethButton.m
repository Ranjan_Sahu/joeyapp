//
//  TeethButton.m
//  RotateButton
//
//  Created by webwerks on 27/03/18.
//  Copyright © 2018 Manish Malviya. All rights reserved.
//

#import "TeethButton.h"
#import <JDFTooltips/JDFTooltipView.h>

@implementation TeethButton



-(void)awakeFromNib{
    [super awakeFromNib];
    self.isSelected = NO;
    [self setUpView];
}

-(void)layoutSubviews{
    
    [super layoutSubviews];
    [self setUpView];
}


-(void)prepareForInterfaceBuilder{
    [self setUpView];
}

-(void)setUpView{
    
    self.transform = CGAffineTransformMakeRotation(self.angle * M_PI / 180.0);
}

-(void)toggle{

    if(_isSelected){
        [self setImage:_unselectedImage forState:UIControlStateNormal];
    }else{
        if(_selectedImage){
            dispatch_async(dispatch_get_main_queue(), ^{
                [self setImage:_selectedImage forState:UIControlStateNormal];
            });
        }else{
            if(self.tag <= 20){
              UIImage * image = [UIImage imageNamed:[NSString stringWithFormat:@"btn_teeth%d_selected",(int)self.tag]];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self setImage:image forState:UIControlStateNormal];
                });
                
            }
        }
    }
    _isSelected = !_isSelected;
}



-(void)showTip:(NSString *)text{
    
    
    if([self.imageView.image isEqual:self.selectedImage] || _isSelected){
        JDFTooltipView *tooltip = [[JDFTooltipView alloc] initWithTargetView:self hostView:self.superview tooltipText:text arrowDirection:JDFTooltipViewArrowDirectionUp width:200.0f];
        [tooltip show];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.9 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [tooltip hideAnimated:YES];
        });
    }
    
}


@end
