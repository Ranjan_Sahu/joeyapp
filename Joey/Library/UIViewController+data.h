//
//  UIViewController+data.h
//  Joey
//
//  Created by csl on 8/1/2017.
//  Copyright © 2017 Auxilia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (data)

@property (nonatomic, retain) NSMutableDictionary *data;

@end
