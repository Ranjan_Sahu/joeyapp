//
//  OCTBaseCollectionViewLayout_v2.m
//  OCTCustomCollectionViewLayout
//
//  Created by dmitry.brovkin on 4/3/17.
//  Copyright © 2017 dmitry.brovkin. All rights reserved.
//

#import "OCTGalleryLayout_v2.h"

static const CGFloat kSideItemWidthCoef = 0.3;
static const CGFloat kSideItemHeightAspect = 1;
static const NSInteger kNumberOfSideItems = 3;

@implementation OCTGalleryLayout_v2
{
    CGSize _mainItemSize;
    CGSize _sideItemSize;
    NSArray<NSNumber *> *_columnsXoffset;
}

#pragma mark Init

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    
    if (self) {
        self.totalColumns = 2;
    }
    
    return self;
}

#pragma mark Override Abstract methods

- (NSInteger)columnIndexForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    NSInteger totalItemsInRow = kNumberOfSideItems + 1;
    NSInteger columnIndex = indexPath.item % totalItemsInRow;
    NSInteger columnIndexLimit = self.totalColumns - 1;
    
    return columnIndex > columnIndexLimit  ? columnIndexLimit : columnIndex;
}

- (CGRect)calculateItemFrameAtIndexPath:(NSIndexPath *)indexPath columnIndex:(NSInteger)columnIndex columnYoffset:(CGFloat)columnYoffset {
    
   CGFloat collW = self.collectionView.bounds.size.width;
   CGFloat collH = self.collectionView.bounds.size.height;
   
    NSLog(@"items are %d",indexPath.item);
    
    NSInteger rowIndex;
    CGFloat x;
    CGFloat y;
    CGFloat width;
    
    if(indexPath.item == 0){
        self.totalColumns = 2;
        rowIndex = indexPath.item / self.totalColumns;
        x = collW * 0.02;
        y = collW * 0.02;
        width = collW * 0.30;
        return CGRectMake(x, y, width, width);
    
    }
    
    else if(indexPath.item == 1){
        self.totalColumns = 2;
        rowIndex = indexPath.item / self.totalColumns;
        x = collW * 0.02 + collW * 0.30 + collW * 0.02;
        y = collW * 0.02;
        width = collW * 0.62;
        return CGRectMake(x, y, width, width);
    }
    
    else if(indexPath.item == 2){
        self.totalColumns = 2;
        rowIndex = indexPath.item / self.totalColumns;
        x = collW * 0.02;
        y = collW * 0.02 + collW * 0.30 + collW * 0.02;
        width = collW * 0.30;
        return CGRectMake(x, y, width, width);
    }
    
    else if(indexPath.item == 3){
        self.totalColumns = 3;
        rowIndex = indexPath.item / self.totalColumns;
        x = collW * 0.02;
        y = collW * 0.02 + collW * 0.30 + collW * 0.02 + collW * 0.30 + collW * 0.02;
        width = collW * 0.30;
        return CGRectMake(x, y, width, width);
    }
    
    else if(indexPath.item == 4){
        self.totalColumns = 3;
        rowIndex = indexPath.item / self.totalColumns;
        x = collW * 0.02 + collW * 0.30 + collW * 0.02;
        y = collW * 0.02 + collW * 0.30 + collW * 0.02 + collW * 0.30 + collW * 0.02;
        width = collW * 0.30;
        return CGRectMake(x, y, width, width);
    }
    else if(indexPath.item == 5){
        self.totalColumns = 3;
        rowIndex = indexPath.item / self.totalColumns;
        x = collW * 0.02 + collW * 0.30 + collW * 0.02 + collW * 0.30 + collW * 0.02;
        y = collW * 0.02 + collW * 0.30 + collW * 0.02 + collW * 0.30 + collW * 0.02;
        width = collW * 0.30;
        return CGRectMake(x, y, width, width);
    }
    
    else{
        return CGRectMake(0.0, 0.0, 0.0, 0.0);
    }
    
//    NSInteger rowIndex = indexPath.item / self.totalColumns;
//    CGSize size = columnIndex == 0 ? _mainItemSize : _sideItemSize;
//
//
//
//    if(rowIndex == 0 && columnIndex == 0){
//        return CGRectMake(0.0, 0.0, size.width *0.33, size.width *0.33);
//    }
//    else{
//    return CGRectMake(_columnsXoffset[columnIndex].floatValue, columnYoffset, size.width, size.height);
//    }
}

- (void)calculateItemsSize {
    CGFloat contentWidthWithoutIndents = self.collectionView.bounds.size.width - self.contentInsets.left - self.contentInsets.right;
    CGFloat resolvedContentWidth = contentWidthWithoutIndents - self.interItemsSpacing;

    // We need to calculate side item size first, in order to calculate main item height
    CGFloat sideItemWidth = resolvedContentWidth * kSideItemWidthCoef;
    CGFloat sideItemHeight = sideItemWidth * kSideItemHeightAspect;

    _sideItemSize = CGSizeMake(sideItemWidth, sideItemHeight);

    // Now we can calculate main item height
    CGFloat mainItemWidth = resolvedContentWidth - sideItemWidth;
    CGFloat mainItemHeight = sideItemHeight * kNumberOfSideItems + ((kNumberOfSideItems - 1) * self.interItemsSpacing);

    _mainItemSize = CGSizeMake(mainItemWidth, mainItemHeight);
    
    // Calculating offsets by X for each column
    _columnsXoffset = @[@(0), @(_mainItemSize.width + self.interItemsSpacing)];
}

- (NSString *)description {
    return @"Layout v2";
}

@end
