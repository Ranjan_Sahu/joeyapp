//
//  SettingsFamilyBabyTableViewCell.m
//  Joey
//
//  Created by csl on 27/5/2017.
//  Copyright © 2017 Auxilia. All rights reserved.
//

#import "SettingsFamilyBabyTableViewCell.h"

@implementation SettingsFamilyBabyTableViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    [self setBackgroundColor:[UIColor clearColor]];
    self.backupPageNo = 1;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

- (void)setBounds:(CGRect)bounds
{
    [super setBounds:bounds];
    self.contentView.frame = self.bounds;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    [self.contentView updateConstraintsIfNeeded];
    [self.contentView layoutIfNeeded];
}

//#pragma mark -
//#pragma mark UIScrollViewDelegate Methods
//- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
//{
//    if([scrollView isEqual:self.familyScrollView])
//    {
//        NSUserDefaults *shareObject = [NSUserDefaults standardUserDefaults];
//        NSMutableArray *familyList = (NSMutableArray *)[NSKeyedUnarchiver unarchiveObjectWithData:[shareObject objectForKey:@"family_list"]];
//        NSLog(@"familyList0=%@", familyList);
//        
//        NSLog(@"scrollView.contentOffset.x=%f", scrollView.contentOffset.x );
//        CGFloat pageWidth = scrollView.bounds.size.width;
//        int pageNo = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
//        NSLog(@"pageNo=%d", pageNo);
//        NSLog(@"self.backupPageNo=%d", self.backupPageNo);
//        
//        if(pageNo == 0)
//        {
//            if(self.backupPageNo != pageNo)
//            {
//                int nextIndex = self.selectedFamilyIndex;
//                nextIndex = (nextIndex >= familyList.count)?0:nextIndex;
//                self.selectedFamilyIndex--;
//                self.selectedFamilyIndex = (self.selectedFamilyIndex < 0)?(int)(familyList.count-1):self.selectedFamilyIndex;
//                int prevIndex = self.selectedFamilyIndex-1;
//                prevIndex = (prevIndex < 0)?(int)(familyList.count-1):prevIndex;
//                
//                NSLog(@"selectedFamilyIndex0=%d", self.selectedFamilyIndex);
//                NSLog(@"prevIndex0=%d", prevIndex);
//                NSLog(@"nextIndex0=%d", nextIndex);
//                
//                self.familyScrollView.contentOffset = CGPointMake(scrollView.frame.size.width, self.familyScrollView.contentOffset.y);
//                self.backupPageNo = 1;
//                
//                [self.viewController updateBabyView:self.selectedFamilyIndex];
//            }
//        }
//        else if(pageNo == 2)
//        {
//            if(self.backupPageNo != pageNo)
//            {
//                int prevIndex = self.selectedFamilyIndex;
//                prevIndex = (prevIndex < 0)?(int)(familyList.count-1):prevIndex;
//                self.selectedFamilyIndex++;
//                self.selectedFamilyIndex = (self.selectedFamilyIndex >= familyList.count)?0:self.selectedFamilyIndex;
//                int nextIndex = self.selectedFamilyIndex+1;
//                nextIndex = (nextIndex >= familyList.count)?0:nextIndex;
//                
//                NSLog(@"selectedFamilyIndex1=%d", self.selectedFamilyIndex);
//                NSLog(@"prevIndex1=%d", prevIndex);
//                NSLog(@"nextIndex1=%d", nextIndex);
//                
//                self.familyScrollView.contentOffset = CGPointMake(scrollView.frame.size.width, self.familyScrollView.contentOffset.y);
//                self.backupPageNo = 1;
//                
//                [self.viewController updateBabyView:self.selectedFamilyIndex];
//            }
//        }
//        else self.backupPageNo = pageNo;
//    }
//}

@end
