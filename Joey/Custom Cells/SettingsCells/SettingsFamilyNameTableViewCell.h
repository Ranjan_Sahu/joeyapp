//
//  SettingsFamilyNameTableViewCell.h
//  Joey
//
//  Created by csl on 6/2/2017.
//  Copyright © 2017 Auxilia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SettingsFamilyViewController.h"

@interface SettingsFamilyNameTableViewCell : UITableViewCell <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *rightImage;
@property (weak, nonatomic) IBOutlet UIImageView *leftImage;
@property (weak, nonatomic) IBOutlet UILabel *lblFamilyName;

@end
