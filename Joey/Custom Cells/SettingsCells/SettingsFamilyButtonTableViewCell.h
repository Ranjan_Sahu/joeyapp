//
//  SettingsFamilyButtonTableViewCell.h
//  Joey
//
//  Created by csl on 20/6/2017.
//  Copyright © 2017 Auxilia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsFamilyButtonTableViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIButton *btnDelete;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *btnDeleteHeightConstraint;

@end
