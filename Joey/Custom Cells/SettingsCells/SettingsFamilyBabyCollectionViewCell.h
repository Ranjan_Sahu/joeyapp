//
//  SettingsFamilyBabyCollectionViewCell.h
//  Joey
//
//  Created by Aishwarya Rai on 12/12/17.
//  Copyright © 2017 Auxilia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsFamilyBabyCollectionViewCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UIImageView *imgBaby;
@property (weak, nonatomic) IBOutlet UILabel *txtBabyName;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imgBabyHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imgBabyWidthConstraint;

@end
