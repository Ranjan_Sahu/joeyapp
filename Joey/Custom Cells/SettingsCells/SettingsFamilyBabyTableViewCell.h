//
//  SettingsFamilyBabyTableViewCell.h
//  Joey
//
//  Created by csl on 27/5/2017.
//  Copyright © 2017 Auxilia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SettingsFamilyViewController.h"

@interface SettingsFamilyBabyTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UICollectionView *familyCollectionView;
@property (weak, nonatomic) IBOutlet UIImageView *rightArrow;

@property (nonatomic, strong) SettingsFamilyViewController *viewController;
@property (nonatomic) int selectedFamilyIndex;
@property (nonatomic) int backupPageNo;

@end
