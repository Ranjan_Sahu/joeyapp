//
//  SettingsFamilyButtonTableViewCell.m
//  Joey
//
//  Created by csl on 20/6/2017.
//  Copyright © 2017 Auxilia. All rights reserved.
//

#import "SettingsFamilyButtonTableViewCell.h"

@implementation SettingsFamilyButtonTableViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    [self setBackgroundColor:[UIColor clearColor]];
    
    self.btnDelete.layer.cornerRadius = 5.f;
    [self.btnDelete setTitle:LocalizedString(@"btn_delete_family", nil) forState:UIControlStateNormal];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

- (void)setBounds:(CGRect)bounds
{
    [super setBounds:bounds];
    self.contentView.frame = self.bounds;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    [self.contentView updateConstraintsIfNeeded];
    [self.contentView layoutIfNeeded];
}

@end
