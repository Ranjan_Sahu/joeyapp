//
//  SettingsFamilyNameTableViewCell.m
//  Joey
//
//  Created by csl on 6/2/2017.
//  Copyright © 2017 Auxilia. All rights reserved.
//

#import "SettingsFamilyNameTableViewCell.h"

@implementation SettingsFamilyNameTableViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    [self setBackgroundColor:[UIColor clearColor]];
    
    //self.lblName.text = LocalizedString(@"txt_name", nil);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

- (void)setBounds:(CGRect)bounds
{
    [super setBounds:bounds];
    self.contentView.frame = self.bounds;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    [self.contentView updateConstraintsIfNeeded];
    [self.contentView layoutIfNeeded];
    
    //self.lblName.preferredMaxLayoutWidth = CGRectGetWidth(self.lblName.frame);
    self.lblFamilyName.preferredMaxLayoutWidth = CGRectGetWidth(self.lblFamilyName.frame);
}

@end
