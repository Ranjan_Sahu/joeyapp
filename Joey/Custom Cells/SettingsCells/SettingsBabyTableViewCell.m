//
//  SettingsBabyTableViewCell.m
//  Joey
//
//  Created by csl on 24/1/2017.
//  Copyright © 2017 Auxilia. All rights reserved.
//

#import "SettingsBabyTableViewCell.h"

@implementation SettingsBabyTableViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    [self setBackgroundColor:[UIColor clearColor]];
    
    self.imgPhoto.clipsToBounds = YES;
    self.imgPhoto.layer.cornerRadius = self.imgPhoto.frame.size.width/2;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

- (void)setBounds:(CGRect)bounds
{
    [super setBounds:bounds];
    self.contentView.frame = self.bounds;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    [self.contentView updateConstraintsIfNeeded];
    [self.contentView layoutIfNeeded];
    
    self.txtName.preferredMaxLayoutWidth = CGRectGetWidth(self.txtName.frame);
    self.txtValue.preferredMaxLayoutWidth = CGRectGetWidth(self.txtValue.frame);
}

@end
