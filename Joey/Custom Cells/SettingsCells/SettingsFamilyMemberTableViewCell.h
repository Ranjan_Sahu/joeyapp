//
//  SettingsFamilyMemberTableViewCell.h
//  Joey
//
//  Created by csl on 6/2/2017.
//  Copyright © 2017 Auxilia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsFamilyMemberTableViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *txtName;
@property (nonatomic, weak) IBOutlet UILabel *txtValue;
@property (nonatomic, weak) IBOutlet UIButton *btnDelete;

@end
