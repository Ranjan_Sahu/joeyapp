//
//  SubscriptionCollectionViewCell.m
//  Joey
//
//  Created by webwerks on 2/22/18.
//  Copyright © 2018 Auxilia. All rights reserved.
//

#import "SubscriptionCollectionViewCell.h"

@implementation SubscriptionCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.contentView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    
//    self.bgView.layer.shadowOffset = CGSizeMake(4.0, 4.0);
//    self.bgView.layer.shadowOpacity = 1.0;
//    self.bgView.layer.shadowRadius = 0.0;
    
    self.bgView.layer.borderWidth = 1.0;
    self.bgView.layer.borderColor = UIColorFromRGB(0xB7ADA6).CGColor;
    
    if (IS_IPAD) {
        self.lblTitle.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.lblSubTitle.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.lblDescription1.font = [UIFont fontWithName:@"SFUIDisplay-Regular" size:23.0f];
        self.lblDescription2.font = [UIFont fontWithName:@"SFUIDisplay-Regular" size:23.0f];
        self.lblDescription3.font = [UIFont fontWithName:@"SFUIDisplay-Regular" size:23.0f];
        self.lblDescription4.font = [UIFont fontWithName:@"SFUIDisplay-Regular" size:23.0f];
        self.lblDescription5.font = [UIFont fontWithName:@"SFUIDisplay-Regular" size:23.0f];
    }

    [self layoutIfNeeded];
}

- (void)setBounds:(CGRect)bounds
{
    [super setBounds:bounds];
    self.contentView.frame = self.bounds;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    [self.contentView updateConstraintsIfNeeded];
    [self.contentView layoutIfNeeded];
    
    self.lblTitle.preferredMaxLayoutWidth = CGRectGetWidth(self.lblTitle.frame);
    self.lblSubTitle.preferredMaxLayoutWidth = CGRectGetWidth(self.lblSubTitle.frame);
    self.lblDescription1.preferredMaxLayoutWidth = CGRectGetWidth(self.lblDescription1.frame);
    self.lblDescription2.preferredMaxLayoutWidth = CGRectGetWidth(self.lblDescription2.frame);
    self.lblDescription3.preferredMaxLayoutWidth = CGRectGetWidth(self.lblDescription3.frame);
    self.lblDescription4.preferredMaxLayoutWidth = CGRectGetWidth(self.lblDescription4.frame);
    self.lblDescription5.preferredMaxLayoutWidth = CGRectGetWidth(self.lblDescription5.frame);

}

@end
