//
//  SettingsBabyTableViewCell.h
//  Joey
//
//  Created by csl on 24/1/2017.
//  Copyright © 2017 Auxilia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsBabyTableViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIImageView *imgPhoto;
@property (nonatomic, weak) IBOutlet UILabel *txtName;
@property (nonatomic, weak) IBOutlet UILabel *txtValue;

@end
