//
//  NotificationsTableViewCell.m
//  Joey
//
//  Created by webwerks on 2/14/18.
//  Copyright © 2018 Auxilia. All rights reserved.
//

#import "NotificationsTableViewCell.h"

@implementation NotificationsTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.toggleSwitch.tintColor = UIColorFromRGB(0xEDE6E1);
    self.toggleSwitch.onTintColor = UIColorFromRGB(0xD6E8C2);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
