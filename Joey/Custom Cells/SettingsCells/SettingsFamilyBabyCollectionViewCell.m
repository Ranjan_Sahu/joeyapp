//
//  SettingsFamilyBabyCollectionViewCell.m
//  Joey
//
//  Created by Aishwarya Rai on 12/12/17.
//  Copyright © 2017 Auxilia. All rights reserved.
//

#import "SettingsFamilyBabyCollectionViewCell.h"

@implementation SettingsFamilyBabyCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    
    self.imgBaby.backgroundColor = UIColorFromRGB(0xE0D3CB);
    self.imgBaby.image = [UIImage imageNamed:@"dummy"];
    self.imgBaby.clipsToBounds = YES;
    self.imgBaby.layer.cornerRadius = _imgBaby.frame.size.width/2;
    self.imgBaby.contentMode = UIViewContentModeScaleAspectFill;
    
    self.txtBabyName.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:13.0f];
    self.txtBabyName.textColor = UIColorFromRGB(0x8D8082);
    self.txtBabyName.textAlignment = NSTextAlignmentCenter;
    self.txtBabyName.numberOfLines = 0;
    [self.txtBabyName sizeToFit];
}

@end
