//
//  SubscriptionCollectionViewCell.h
//  Joey
//
//  Created by webwerks on 2/22/18.
//  Copyright © 2018 Auxilia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SubscriptionCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblSubTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblDescription1;
@property (weak, nonatomic) IBOutlet UILabel *lblDescription2;
@property (weak, nonatomic) IBOutlet UILabel *lblDescription3;
@property (weak, nonatomic) IBOutlet UILabel *lblDescription4;
@property (weak, nonatomic) IBOutlet UILabel *lblDescription5;

@property (weak, nonatomic) IBOutlet UIView *bgView;

@end
