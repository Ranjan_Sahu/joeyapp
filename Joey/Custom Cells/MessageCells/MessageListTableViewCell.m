//
//  MessageListTableViewCell.m
//  Joey
//
//  Created by webwerks on 2/8/18.
//  Copyright © 2018 Auxilia. All rights reserved.
//

#import "MessageListTableViewCell.h"

@implementation MessageListTableViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    [self setBackgroundColor:[UIColor clearColor]];
    
    self.imgIcon.contentMode = UIViewContentModeCenter;
    self.imgIconChevronRight.image = [self.imgIconChevronRight.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];

    if (IS_IPAD) {
        self.lblMessageTitle.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:18.0f];
        self.lblMessageSubText.font = [UIFont fontWithName:@"SFUIDisplay-Regular" size:20.0f];
    }
    [self layoutSubviews];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

- (void)setBounds:(CGRect)bounds
{
    [super setBounds:bounds];
    self.contentView.frame = self.bounds;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
//    [self.contentView updateConstraintsIfNeeded];
//    [self.contentView layoutIfNeeded];
//
//    self.lblMessageTitle.preferredMaxLayoutWidth = CGRectGetWidth(self.lblMessageTitle.frame);
//    self.lblMessageSubText.preferredMaxLayoutWidth = CGRectGetWidth(self.lblMessageSubText.frame);
}


@end
