//
//  MessageListTableViewCell.h
//  Joey
//
//  Created by webwerks on 2/8/18.
//  Copyright © 2018 Auxilia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MessageListTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *indicatorView;
@property (weak, nonatomic) IBOutlet UIImageView *imgIcon;
@property (weak, nonatomic) IBOutlet UIImageView *imgIconChevronRight;
@property (weak, nonatomic) IBOutlet UILabel *lblMessageTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblMessageSubText;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imgIconHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imgIconWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *indicatorHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *indicatorWidth;

@end
