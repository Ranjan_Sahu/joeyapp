//
//  MilestonesTableViewCell.h
//  Joey
//
//  Created by csl on 29/12/2016.
//  Copyright © 2016 Auxilia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MilestonesTableViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIImageView *imgIcon;
@property (nonatomic, weak) IBOutlet UIImageView *imgIconChevronRight;
@property (nonatomic, weak) IBOutlet UILabel *txtTime;
@property (nonatomic, weak) IBOutlet UILabel *txtDate;

@property (nonatomic, weak) IBOutlet UILabel *txtName;
@property (nonatomic, weak) IBOutlet UILabel *txtValue;
@property (nonatomic, weak) IBOutlet UILabel *txtNotes;

@property (nonatomic, weak) IBOutlet NSLayoutConstraint *imgIconWidthConstraint;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *imgIconHeightConstraint;


@end
