//
//  MilestoneSubfilterTableViewCell.h
//  Joey
//
//  Created by webwerks on 3/6/18.
//  Copyright © 2018 Auxilia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MilestoneSubfilterTableViewCell : UITableViewCell
@property (nonatomic, weak) IBOutlet UILabel *lblTitle;

@end
