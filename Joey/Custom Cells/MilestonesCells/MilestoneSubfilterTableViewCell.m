//
//  MilestoneSubfilterTableViewCell.m
//  Joey
//
//  Created by webwerks on 3/6/18.
//  Copyright © 2018 Auxilia. All rights reserved.
//

#import "MilestoneSubfilterTableViewCell.h"

@implementation MilestoneSubfilterTableViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    [self setBackgroundColor:UIColorFromRGB(0xEEE6E0)];
    self.lblTitle.textColor = UIColorFromRGB(0x827577);
    
    if (IS_IPAD) {
        self.lblTitle.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
    }
    [self layoutSubviews];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

- (void)setBounds:(CGRect)bounds
{
    [super setBounds:bounds];
    self.contentView.frame = self.bounds;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    [self.contentView updateConstraintsIfNeeded];
    [self.contentView layoutIfNeeded];
    self.lblTitle.preferredMaxLayoutWidth = CGRectGetWidth(self.lblTitle.frame);
}

@end
