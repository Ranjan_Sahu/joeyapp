//
//  MilestonesChecklistTableViewCell.h
//  Joey
//
//  Created by csl on 9/6/2017.
//  Copyright © 2017 Auxilia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MilestonesChecklistTableViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIView *checkBox;
@property (nonatomic, weak) IBOutlet UIView *checkBoxSelected;
@property (nonatomic, weak) IBOutlet UIImageView *imgIcon;
@property (nonatomic, weak) IBOutlet UILabel *lblName;
@property (nonatomic, weak) IBOutlet UILabel *txtName;

@property (nonatomic, weak) IBOutlet NSLayoutConstraint *imgIconWidthConstraint;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *imgIconHeightConstraint;

@end
