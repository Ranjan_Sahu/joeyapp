//
//  MilestonesChecklistTableViewCell.m
//  Joey
//
//  Created by csl on 9/6/2017.
//  Copyright © 2017 Auxilia. All rights reserved.
//

#import "MilestonesChecklistTableViewCell.h"

@implementation MilestonesChecklistTableViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    [self setBackgroundColor:[UIColor clearColor]];
    
    if (IS_IPAD) {
        self.lblName.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:18.0f];
        self.txtName.font = [UIFont fontWithName:@"SFUIDisplay-Regular" size:20.0f];
    }
    [self layoutSubviews];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

- (void)setBounds:(CGRect)bounds
{
    [super setBounds:bounds];
    self.contentView.frame = self.bounds;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
//    [self.contentView updateConstraintsIfNeeded];
//    [self.contentView layoutIfNeeded];
//    
//    self.lblName.preferredMaxLayoutWidth = CGRectGetWidth(self.lblName.frame);
//    self.txtName.preferredMaxLayoutWidth = CGRectGetWidth(self.txtName.frame);
}

@end
