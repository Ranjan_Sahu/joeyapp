//
//  MilestonesGrowTableViewCell.m
//  Joey
//
//  Created by csl on 29/12/2016.
//  Copyright © 2016 Auxilia. All rights reserved.
//

#import "MilestonesGrowTableViewCell.h"

@implementation MilestonesGrowTableViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    [self setBackgroundColor:[UIColor clearColor]];
    
    self.imgIcon.contentMode = UIViewContentModeCenter;
    self.imgIconChevronRight.image = [self.imgIconChevronRight.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    
    if (IS_IPAD) {
        self.txtTime.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:26.0f];
        self.txtDate.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:18.0f];
        self.txtName.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:18.0f];
        self.txtValue.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:22.0f];
        self.txtNotes.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:18.0f];
    }
    [self layoutSubviews];

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

- (void)setBounds:(CGRect)bounds
{
    [super setBounds:bounds];
    self.contentView.frame = self.bounds;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    [self.contentView updateConstraintsIfNeeded];
    [self.contentView layoutIfNeeded];
    
    self.txtTime.preferredMaxLayoutWidth = CGRectGetWidth(self.txtTime.frame);
    self.txtDate.preferredMaxLayoutWidth = CGRectGetWidth(self.txtDate.frame);
    self.txtValue.preferredMaxLayoutWidth = CGRectGetWidth(self.txtValue.frame);
    self.txtName.preferredMaxLayoutWidth = CGRectGetWidth(self.txtName.frame);
    self.txtNotes.preferredMaxLayoutWidth = CGRectGetWidth(self.txtNotes.frame);
    
}

@end
