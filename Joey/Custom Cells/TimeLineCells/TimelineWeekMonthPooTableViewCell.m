//
//  TimelineWeekMonthPooTableViewCell.m
//  Joey
//
//  Created by webwerks on 2/21/18.
//  Copyright © 2018 Auxilia. All rights reserved.
//

#import "TimelineWeekMonthPooTableViewCell.h"

@implementation TimelineWeekMonthPooTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    [self setBackgroundColor:[UIColor clearColor]];
    self.imgIcon.contentMode = UIViewContentModeCenter;
    self.imgIconChevronRight.image = [self.imgIconChevronRight.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];

    self.circularImg.layer.cornerRadius = self.circularImg.frame.size.width/2;
    self.circularImg.layer.borderWidth = 1.0;
    self.circularImg.layer.borderColor = [UIColor whiteColor].CGColor;
    
    if (IS_IPAD) {
        //self.txtValue1.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.txtName1.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:18.0f];
        self.txtValue2.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.txtName2.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:18.0f];
        self.txtValue3.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.txtName3.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:18.0f];
        self.txtValue4.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.txtName4.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:18.0f];
    }
    [self layoutSubviews];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

- (void)setBounds:(CGRect)bounds
{
    [super setBounds:bounds];
    self.contentView.frame = self.bounds;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    [self.contentView updateConstraintsIfNeeded];
    [self.contentView layoutIfNeeded];
    
    self.txtName1.preferredMaxLayoutWidth = CGRectGetWidth(self.txtName1.frame);
    self.txtValue2.preferredMaxLayoutWidth = CGRectGetWidth(self.txtValue2.frame);
    self.txtName2.preferredMaxLayoutWidth = CGRectGetWidth(self.txtName2.frame);
    self.txtValue3.preferredMaxLayoutWidth = CGRectGetWidth(self.txtValue3.frame);
    self.txtName3.preferredMaxLayoutWidth = CGRectGetWidth(self.txtName3.frame);
    self.txtValue4.preferredMaxLayoutWidth = CGRectGetWidth(self.txtValue4.frame);
    self.txtName4.preferredMaxLayoutWidth = CGRectGetWidth(self.txtName4.frame);
    
}
@end
