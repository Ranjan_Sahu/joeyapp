//
//  TimelineWeekMonthPooTableViewCell.h
//  Joey
//
//  Created by webwerks on 2/21/18.
//  Copyright © 2018 Auxilia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TimelineWeekMonthPooTableViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIImageView *imgIcon;
@property (nonatomic, weak) IBOutlet UIImageView *circularImg;
@property (nonatomic, weak) IBOutlet UIImageView *imgIconChevronRight;

//@property (nonatomic, weak) IBOutlet UILabel *txtValue1;
@property (nonatomic, weak) IBOutlet UILabel *txtName1;
@property (nonatomic, weak) IBOutlet UILabel *txtValue2;
@property (nonatomic, weak) IBOutlet UILabel *txtName2;
@property (nonatomic, weak) IBOutlet UILabel *txtValue3;
@property (nonatomic, weak) IBOutlet UILabel *txtName3;
@property (nonatomic, weak) IBOutlet UILabel *txtValue4;
@property (nonatomic, weak) IBOutlet UILabel *txtName4;

@property (nonatomic, weak) IBOutlet NSLayoutConstraint *imgIconWidthConstraint;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *imgIconHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *circleImgCentreX;

@end
