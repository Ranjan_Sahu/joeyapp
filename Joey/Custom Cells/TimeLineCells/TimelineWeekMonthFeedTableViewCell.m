//
//  TimelineWeekMonthFeedTableViewCell.m
//  Joey
//
//  Created by csl on 29/12/2016.
//  Copyright © 2016 Auxilia. All rights reserved.
//

#import "TimelineWeekMonthFeedTableViewCell.h"

@implementation TimelineWeekMonthFeedTableViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    [self setBackgroundColor:[UIColor clearColor]];
    
    //self.imgIcon.clipsToBounds = YES;
    //self.imgIcon.layer.cornerRadius = self.imgIcon.frame.size.width/2;
    self.imgIcon.contentMode = UIViewContentModeCenter;
    self.imgIconChevronRight.image = [self.imgIconChevronRight.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    
    if (IS_IPAD) {
        self.txtValue1.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.txtName1.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:18.0f];
        self.txtValue2.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.txtName2.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:18.0f];
        self.txtValue3.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.txtName3.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:18.0f];
        self.txtValue4.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.txtName4.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:18.0f];
    }
    [self layoutSubviews];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

- (void)setBounds:(CGRect)bounds
{
    [super setBounds:bounds];
    self.contentView.frame = self.bounds;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    [self.contentView updateConstraintsIfNeeded];
    [self.contentView layoutIfNeeded];
    
    self.txtValue1.preferredMaxLayoutWidth = CGRectGetWidth(self.txtValue1.frame);
    self.txtName1.preferredMaxLayoutWidth = CGRectGetWidth(self.txtName1.frame);
    self.txtValue2.preferredMaxLayoutWidth = CGRectGetWidth(self.txtValue2.frame);
    self.txtName2.preferredMaxLayoutWidth = CGRectGetWidth(self.txtName2.frame);
    self.txtValue3.preferredMaxLayoutWidth = CGRectGetWidth(self.txtValue3.frame);
    self.txtName3.preferredMaxLayoutWidth = CGRectGetWidth(self.txtName3.frame);
    self.txtValue4.preferredMaxLayoutWidth = CGRectGetWidth(self.txtValue4.frame);
    self.txtName4.preferredMaxLayoutWidth = CGRectGetWidth(self.txtName4.frame);
}

@end
