//
//  TimelineWeekMonthTableViewCell.h
//  Joey
//
//  Created by csl on 29/12/2016.
//  Copyright © 2016 Auxilia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TimelineWeekMonthTableViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIImageView *imgIcon;
@property (nonatomic, weak) IBOutlet UIImageView *imgIconChevronRight;
@property (nonatomic, weak) IBOutlet UILabel *txtValue1;
@property (nonatomic, weak) IBOutlet UILabel *txtName1;
@property (nonatomic, weak) IBOutlet UILabel *txtValue2;
@property (nonatomic, weak) IBOutlet UILabel *txtName2;

@property (nonatomic, weak) IBOutlet NSLayoutConstraint *imgIconWidthConstraint;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *imgIconHeightConstraint;

@end
