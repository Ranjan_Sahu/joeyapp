//
//  GallaryHeaderReusableView.h
//  Joey
//
//  Created by webwerks on 2/26/18.
//  Copyright © 2018 Auxilia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GallaryHeaderReusableView : UICollectionReusableView
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;

@end
