//
//  collectionCellGallaryLayout.h
//  Joey
//
//  Created by webwerks on 6/4/18.
//  Copyright © 2018 Auxilia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface collectionCellGallaryLayout : UICollectionViewCell <UIScrollViewDelegate>
//Anil
@property (weak, nonatomic) IBOutlet UIScrollView *scrlViewLayout;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewLayout;

@property (weak, nonatomic) IBOutlet UILabel *lblBlocksValue;
@property (weak, nonatomic) IBOutlet UIView *viewCustom;
@property (weak, nonatomic) IBOutlet UIView *viewBottom;

@property (weak, nonatomic) IBOutlet UIImageView *imgViewIcon;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewIconLabel;
@property (weak, nonatomic) IBOutlet UILabel *lblActivityName;



@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightCBottomView;


@end
