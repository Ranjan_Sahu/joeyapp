//
//  templateCollectionViewCell.h
//  Joey
//
//  Created by webwerks on 2/12/18.
//  Copyright © 2018 Auxilia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface templateCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (weak, nonatomic) IBOutlet UIImageView *imgView_inactive;


@end
