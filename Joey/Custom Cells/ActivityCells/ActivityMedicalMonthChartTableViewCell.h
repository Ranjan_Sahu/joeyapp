//
//  ActivityMedicalMonthChartTableViewCell.h
//  Joey
//
//  Created by csl on 9/1/2017.
//  Copyright © 2017 Auxilia. All rights reserved.
//

#import <UIKit/UIKit.h>
@import Charts;

@interface ActivityMedicalMonthChartTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet ScatterChartView *chartView;
@property (nonatomic, weak) IBOutlet CombinedChartView *temperatureChartView;
@property (nonatomic, weak) IBOutlet UILabel *txtLeftAxis;
@property (nonatomic, weak) IBOutlet UILabel *txtRightAxis;

//
@property (nonatomic, weak) IBOutlet UILabel *txtMedSummary;
@property (nonatomic, weak) IBOutlet UILabel *txtSympSummary;

// views
@property (nonatomic, weak) IBOutlet UIView *viewMed1;
@property (nonatomic, weak) IBOutlet UIView *viewMed2;
@property (nonatomic, weak) IBOutlet UIView *viewMed3;
@property (nonatomic, weak) IBOutlet UIView *viewMed4;
@property (nonatomic, weak) IBOutlet UIView *viewMed5;
@property (weak, nonatomic) IBOutlet UIView *viewMed6;
@property (weak, nonatomic) IBOutlet UIView *viewMed7;

@property (nonatomic, weak) IBOutlet UIView *viewSymp1;
@property (nonatomic, weak) IBOutlet UIView *viewSymp2;
@property (nonatomic, weak) IBOutlet UIView *viewSymp3;

// labels
@property (nonatomic, weak) IBOutlet UILabel *medName1;
@property (nonatomic, weak) IBOutlet UILabel *medName2;
@property (nonatomic, weak) IBOutlet UILabel *medName3;
@property (nonatomic, weak) IBOutlet UILabel *medName4;
@property (nonatomic, weak) IBOutlet UILabel *medName5;
@property (weak, nonatomic) IBOutlet UILabel *medName6;
@property (weak, nonatomic) IBOutlet UILabel *medName7;

@property (nonatomic, weak) IBOutlet UILabel *medValue1;
@property (nonatomic, weak) IBOutlet UILabel *medValue2;
@property (nonatomic, weak) IBOutlet UILabel *medValue3;
@property (nonatomic, weak) IBOutlet UILabel *medValue4;
@property (nonatomic, weak) IBOutlet UILabel *medValue5;
@property (weak, nonatomic) IBOutlet UILabel *medValue6;
@property (weak, nonatomic) IBOutlet UILabel *medValue7;

@property (nonatomic, weak) IBOutlet UILabel *sympName1;
@property (nonatomic, weak) IBOutlet UILabel *sympName2;
@property (nonatomic, weak) IBOutlet UILabel *sympName3;

@property (nonatomic, weak) IBOutlet UILabel *sympValue1;
@property (nonatomic, weak) IBOutlet UILabel *sympValue2;
@property (nonatomic, weak) IBOutlet UILabel *sympValue3;

// Height Constraints
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *hcViewMedSynp;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *chartViewHC;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewMed1HC;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewMed2HC;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewMed3HC;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewMed4HC;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewMed5HC;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewMed6HC;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewMed7HC;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewSymp1HC;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewSymp2HC;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewSymp3HC;


//@property (nonatomic, weak) IBOutlet UILabel *txtSummary;
//@property (nonatomic, weak) IBOutlet UIButton *btnMed1;
//@property (nonatomic, weak) IBOutlet UIView *viewMed1;
//@property (nonatomic, weak) IBOutlet UILabel *lblMed1;
//@property (nonatomic, weak) IBOutlet UILabel *txtMed1Name;
//@property (nonatomic, weak) IBOutlet UILabel *txtMed1Value;
//@property (nonatomic, weak) IBOutlet UIButton *btnMed2;
//@property (nonatomic, weak) IBOutlet UIView *viewMed2;
//@property (nonatomic, weak) IBOutlet UILabel *lblMed2;
//@property (nonatomic, weak) IBOutlet UILabel *txtMed2Name;
//@property (nonatomic, weak) IBOutlet UILabel *txtMed2Value;
//@property (nonatomic, weak) IBOutlet UIButton *btnMed3;
//@property (nonatomic, weak) IBOutlet UIView *viewMed3;
//@property (nonatomic, weak) IBOutlet UILabel *lblMed3;
//@property (nonatomic, weak) IBOutlet UILabel *txtMed3Name;
//@property (nonatomic, weak) IBOutlet UILabel *txtMed3Value;
//@property (nonatomic, weak) IBOutlet UIButton *btnMed4;
//@property (nonatomic, weak) IBOutlet UIView *viewMed4;
//@property (nonatomic, weak) IBOutlet UILabel *lblMed4;
//@property (nonatomic, weak) IBOutlet UILabel *txtMed4Name;
//@property (nonatomic, weak) IBOutlet UILabel *txtMed4Value;
//@property (nonatomic, weak) IBOutlet UIView *viewVomit;
//@property (nonatomic, weak) IBOutlet UILabel *lblVomit;
//@property (nonatomic, weak) IBOutlet UILabel *txtVomit;
//@property (nonatomic, weak) IBOutlet UIView *viewCough;
//@property (nonatomic, weak) IBOutlet UILabel *lblCough;
//@property (nonatomic, weak) IBOutlet UILabel *txtCough;

@end
