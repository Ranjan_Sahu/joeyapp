//
//  ActivityTableViewCell.m
//  Joey
//
//  Created by csl on 8/1/2017.
//  Copyright © 2017 Auxilia. All rights reserved.
//

#import "ActivityTableViewCell.h"

@implementation ActivityTableViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    [self setBackgroundColor:[UIColor clearColor]];
    self.btnPhoto.imageView.contentMode = UIViewContentModeScaleAspectFill;
    
    self.txtTime.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:15.0f];
    self.txtValue.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:15.0f];
    self.txtNotes.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:14.0f];

    self.txtTime.text = @"";
    self.txtValue.text = @"";
    self.txtNotes.text = @"";
    
    if (IS_IPAD) {
        self.txtTime.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f];
        self.txtValue.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f];
        self.txtNotes.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:20.0f];
    }

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

- (void)setBounds:(CGRect)bounds
{
    [super setBounds:bounds];
    self.contentView.frame = self.bounds;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    [self.contentView updateConstraintsIfNeeded];
    [self.contentView layoutIfNeeded];
    
    self.txtTime.preferredMaxLayoutWidth = CGRectGetWidth(self.txtTime.frame);
    self.txtValue.preferredMaxLayoutWidth = CGRectGetWidth(self.txtValue.frame);
    self.txtNotes.preferredMaxLayoutWidth = CGRectGetWidth(self.txtNotes.frame);

}

@end
