//
//  ActivityReminderRecurrenceTableViewCell.m
//  Joey
//
//  Created by csl on 28/5/2017.
//  Copyright © 2017 Auxilia. All rights reserved.
//

#import "ActivityReminderRecurrenceTableViewCell.h"

@implementation ActivityReminderRecurrenceTableViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    [self setBackgroundColor:[UIColor clearColor]];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

- (void)setBounds:(CGRect)bounds
{
    [super setBounds:bounds];
    self.contentView.frame = self.bounds;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    [self.contentView updateConstraintsIfNeeded];
    [self.contentView layoutIfNeeded];
    
    self.txtName.preferredMaxLayoutWidth = CGRectGetWidth(self.txtName.frame);
}

@end
