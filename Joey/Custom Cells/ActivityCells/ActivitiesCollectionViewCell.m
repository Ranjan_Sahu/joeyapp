//
//  ActivitiesCollectionViewCell.m
//  Joey
//
//  Created by csl on 17/2/2017.
//  Copyright © 2017 Auxilia. All rights reserved.
//

#import "ActivitiesCollectionViewCell.h"

@implementation ActivitiesCollectionViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.contentView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    
    self.viewBg.layer.shadowOffset = CGSizeMake(4.0, 4.0);
    self.viewBg.layer.shadowOpacity = 1.0;
    self.viewBg.layer.shadowRadius = 0.0;
    
    self.imgIcon.layer.cornerRadius = self.imgIcon.frame.size.width/2;
    self.imgIcon.clipsToBounds = YES;
    
    self.imgIconAlarm.image = [self.imgIconAlarm.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    self.imgIconAdd.image = [self.imgIconAdd.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    self.imgQuickAdd.image = [self.imgQuickAdd.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];

    self.imgIconAlarm.tintColor = UIColorFromRGB(0xEDE8E4);
    
    if (IS_IPAD) {
        self.quickAddImgWidth.constant = 44.0;
        self.quickAddImgHeight.constant = 44.0;
    }else {
        self.quickAddImgWidth.constant = 32.0;
        self.quickAddImgHeight.constant = 32.0;
    }

    self.imgIconAdd.transform = CGAffineTransformMakeScale(1.3, 1.3);
    self.imgIconAlarm.transform = CGAffineTransformMakeScale(1.3, 1.3);
    
    
    [self layoutIfNeeded];
    
}

- (void)setBounds:(CGRect)bounds
{
    [super setBounds:bounds];
    self.contentView.frame = self.bounds;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    [self.contentView updateConstraintsIfNeeded];
    [self.contentView layoutIfNeeded];
    [self.imgIcon layoutIfNeeded];
    
    self.imgIcon.layer.cornerRadius = self.imgIcon.frame.size.width/2;
    self.txtName.preferredMaxLayoutWidth = CGRectGetWidth(self.txtName.frame);
    self.txtTime.preferredMaxLayoutWidth = CGRectGetWidth(self.txtTime.frame);
    self.txtDiff.preferredMaxLayoutWidth = CGRectGetWidth(self.txtDiff.frame);
    self.txtValue.preferredMaxLayoutWidth = CGRectGetWidth(self.txtValue.frame);
    self.txtValue2.preferredMaxLayoutWidth = CGRectGetWidth(self.txtValue2.frame);
    
}

@end
