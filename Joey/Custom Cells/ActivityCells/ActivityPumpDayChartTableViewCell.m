//
//  ActivityPumpDayChartTableViewCell.m
//  Joey
//
//  Created by csl on 9/1/2017.
//  Copyright © 2017 Auxilia. All rights reserved.
//

#import "ActivityPumpDayChartTableViewCell.h"
#import "ChartAxisValueFormatter.h"

@implementation ActivityPumpDayChartTableViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    [self setBackgroundColor:[UIColor clearColor]];
    
    self.chartView.drawBarShadowEnabled = NO;
    self.chartView.drawValueAboveBarEnabled = NO;
    self.chartView.legend.enabled = NO;
    self.chartView.chartDescription.enabled = NO;
    self.chartView.pinchZoomEnabled = NO;
    self.chartView.highlightPerTapEnabled = NO;
    self.chartView.fitBars = YES;
    self.chartView.rightAxis.enabled = NO;
    
    ChartXAxis *xAxis = self.chartView.xAxis;
    xAxis.labelPosition = XAxisLabelPositionBottom;
    xAxis.labelFont = [UIFont fontWithName:@"SFUIDisplay-Medium" size:10.0f];
    if (IS_IPAD) {
        xAxis.labelFont = [UIFont fontWithName:@"SFUIDisplay-Medium" size:16.0f];
    }
    xAxis.labelTextColor = UIColorFromRGB(0xBFB5AE);
    xAxis.labelCount = 9;
    xAxis.axisLineColor = UIColorFromRGB(0xEEE8E4);
    xAxis.drawGridLinesEnabled = YES;
    xAxis.gridColor = UIColorFromRGB(0xEEE8E4);
    xAxis.valueFormatter = [[ChartAxisValueFormatter alloc] initForChart:self.chartView type:ChartAxisTypeForDay];
    
    ChartYAxis *leftAxis = self.chartView.leftAxis;
    leftAxis.labelPosition = YAxisLabelPositionOutsideChart;
    leftAxis.labelFont = [UIFont fontWithName:@"SFUIDisplay-Medium" size:10.0f];
    if (IS_IPAD) {
        leftAxis.labelFont = [UIFont fontWithName:@"SFUIDisplay-Medium" size:16.0f];
    }
    leftAxis.labelTextColor = UIColorFromRGB(0xBFB5AE);
    leftAxis.labelCount = 4;
    leftAxis.axisMinimum = 0;
    leftAxis.axisLineColor = UIColorFromRGB(0xEEE8E4);
    leftAxis.drawGridLinesEnabled = NO;
    leftAxis.gridColor = UIColorFromRGB(0xEEE8E4);
    
    self.pieChartView.legend.enabled = NO;
    self.pieChartView.chartDescription.enabled = NO;
    self.pieChartView.drawSliceTextEnabled = NO;
    self.pieChartView.holeRadiusPercent = 0.75f;
    self.pieChartView.userInteractionEnabled = NO;
    
    self.viewLeft.backgroundColor = UIColorFromRGB(0xFBDFC1);
    self.viewRight.backgroundColor = UIColorFromRGB(0xD1C6BE);
    
    self.txtNo.text = LocalizedString(@"txt_no_of_pumps", nil);
    self.txtSummary.text = LocalizedString(@"txt_summary", nil);
    self.txtLeft.text = LocalizedString(@"txt_left", nil);
    self.txtRight.text = LocalizedString(@"txt_right", nil);
    
    if (IS_IPAD) {
        self.txtAxis.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:17.0f];
        self.txtNo.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:20.0f];
        self.txtSummary.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:20.0f];
        self.txtLeft.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:22.0f];
        self.txtLeftValue.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:22.0f];
        self.txtRight.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:22.0f];
        self.txtRightValue.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:22.0f];
    }
    [self layoutSubviews];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

- (void)setBounds:(CGRect)bounds
{
    [super setBounds:bounds];
    self.contentView.frame = self.bounds;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    [self.contentView updateConstraintsIfNeeded];
    [self.contentView layoutIfNeeded];
    
    self.txtAxis.preferredMaxLayoutWidth = CGRectGetWidth(self.txtAxis.frame);
    self.txtNo.preferredMaxLayoutWidth = CGRectGetWidth(self.txtNo.frame);
    self.txtSummary.preferredMaxLayoutWidth = CGRectGetWidth(self.txtSummary.frame);
    self.txtLeft.preferredMaxLayoutWidth = CGRectGetWidth(self.txtLeft.frame);
    self.txtLeftValue.preferredMaxLayoutWidth = CGRectGetWidth(self.txtLeftValue.frame);
    self.txtRight.preferredMaxLayoutWidth = CGRectGetWidth(self.txtRight.frame);
    self.txtRightValue.preferredMaxLayoutWidth = CGRectGetWidth(self.txtRightValue.frame);
}

@end
