//
//  ActivityWeeMonthChartTableViewCell.m
//  Joey
//
//  Created by csl on 9/1/2017.
//  Copyright © 2017 Auxilia. All rights reserved.
//

#import "ActivityWeeMonthChartTableViewCell.h"
#import "ChartAxisValueFormatter.h"

@implementation ActivityWeeMonthChartTableViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    [self setBackgroundColor:[UIColor clearColor]];
    
    self.chartView.legend.enabled = NO;
    self.chartView.chartDescription.enabled = NO;
    self.chartView.pinchZoomEnabled = NO;
    self.chartView.highlightPerTapEnabled = NO;
    self.chartView.rightAxis.enabled = NO;
    
    ChartXAxis *xAxis = self.chartView.xAxis;
    xAxis.labelPosition = XAxisLabelPositionBottom;
    xAxis.labelFont = [UIFont fontWithName:@"SFUIDisplay-Medium" size:10.0f];
    if (IS_IPAD) {
        xAxis.labelFont = [UIFont fontWithName:@"SFUIDisplay-Medium" size:16.0f];
    }
    xAxis.labelTextColor = UIColorFromRGB(0xBFB5AE);
    xAxis.labelCount = 6;
    xAxis.axisMinimum = 0;
    xAxis.axisMaximum = 32;
    xAxis.axisLineColor = UIColorFromRGB(0xEEE8E4);
    xAxis.drawGridLinesEnabled = YES;
    xAxis.gridColor = UIColorFromRGB(0xEEE8E4);
    xAxis.valueFormatter = [[ChartAxisValueFormatter alloc] initForChart:self.chartView type:ChartAxisTypeForPooWeeMonth];
    
    ChartYAxis *leftAxis = self.chartView.leftAxis;
    leftAxis.labelPosition = YAxisLabelPositionOutsideChart;
    leftAxis.labelFont = [UIFont fontWithName:@"SFUIDisplay-Medium" size:10.0f];
    if (IS_IPAD) {
        leftAxis.labelFont = [UIFont fontWithName:@"SFUIDisplay-Medium" size:16.0f];
    }
    leftAxis.labelTextColor = UIColorFromRGB(0xBFB5AE);
    leftAxis.axisLineColor = UIColorFromRGB(0xEEE8E4);
    leftAxis.drawGridLinesEnabled = NO;
    leftAxis.gridColor = UIColorFromRGB(0xEEE8E4);
    leftAxis.drawTopYLabelEntryEnabled = false;
    
    /*self.chartView.legend.enabled = NO;
    self.chartView.chartDescription.enabled = NO;
    self.chartView.pinchZoomEnabled = NO;
    self.chartView.highlightPerTapEnabled = NO;
    self.chartView.rightAxis.enabled = NO;
    
    ChartXAxis *xAxis = self.chartView.xAxis;
    xAxis.labelPosition = XAxisLabelPositionBottom;
    xAxis.labelFont = [UIFont fontWithName:@"SFUIDisplay-Medium" size:10.0f];
    xAxis.labelTextColor = UIColorFromRGB(0xBFB5AE);
    xAxis.axisLineColor = UIColorFromRGB(0xEEE8E4);
    xAxis.drawGridLinesEnabled = YES;
    xAxis.drawLabelsEnabled = YES;
    xAxis.gridColor = UIColorFromRGB(0xEEE8E4);
    
    ChartYAxis *leftAxis = self.chartView.leftAxis;
    leftAxis.labelPosition = YAxisLabelPositionOutsideChart;
    leftAxis.labelFont = [UIFont fontWithName:@"SFUIDisplay-Medium" size:10.0f];
    leftAxis.labelTextColor = UIColorFromRGB(0xBFB5AE);
    leftAxis.axisLineColor = UIColorFromRGB(0xEEE8E4);
    leftAxis.drawGridLinesEnabled = NO;
    leftAxis.drawLabelsEnabled = NO;
    leftAxis.gridColor = UIColorFromRGB(0xEEE8E4);*/
    
    /*_chartView.chartDescription.enabled = NO;
    
    _chartView.drawGridBackgroundEnabled = NO;
    _chartView.dragEnabled = YES;
    [_chartView setScaleEnabled:YES];
    _chartView.maxVisibleCount = 200;
    _chartView.pinchZoomEnabled = YES;
    
    
    ChartYAxis *yl = _chartView.leftAxis;
    yl.labelFont = [UIFont fontWithName:@"HelveticaNeue-Light" size:10.f];
    //yl.spaceTop = 0.3;
    //yl.spaceBottom = 0.3;
    //yl.axisMinimum = 0.0; // this replaces startAtZero = YES
    
    _chartView.rightAxis.enabled = NO;
    
    ChartXAxis *xl = _chartView.xAxis;
    xl.labelPosition = XAxisLabelPositionBottom;
    xl.labelFont = [UIFont fontWithName:@"HelveticaNeue-Light" size:10.f];
    */
    //[self setDataCount:8 range:5];
    
    
    /*self.chartView.legend.enabled = NO;
    self.chartView.chartDescription.enabled = NO;
    self.chartView.rightAxis.enabled = NO;
    self.chartView.extraLeftOffset = 5;
    self.chartView.clipValuesToContentEnabled = NO;
    
    ChartYAxis *leftAxis = _chartView.leftAxis;
    leftAxis.drawLabelsEnabled = NO;
    leftAxis.axisLineColor = UIColorFromRGB(0xEEE8E4);
    leftAxis.drawGridLinesEnabled = NO;
    leftAxis.gridColor = UIColorFromRGB(0xEEE8E4);
    leftAxis.axisMinimum = 0.0; // this replaces startAtZero = YES
    
    ChartXAxis *xAxis = _chartView.xAxis;
    xAxis.labelPosition = XAxisLabelPositionBottom;
    xAxis.labelFont = [UIFont fontWithName:@"SFUIDisplay-Medium" size:10.0f];
    xAxis.labelTextColor = UIColorFromRGB(0xBFB5AE);
    xAxis.axisLineColor = UIColorFromRGB(0xEEE8E4);
    xAxis.drawGridLinesEnabled = YES;
    xAxis.gridColor = UIColorFromRGB(0xEEE8E4);
    xAxis.axisMinimum = 0.0; // this replaces startAtZero = YES
    */
    
    self.pieChartView.legend.enabled = NO;
    self.pieChartView.chartDescription.enabled = NO;
    self.pieChartView.drawSliceTextEnabled = NO;
    self.pieChartView.holeRadiusPercent = 0.75f;
    self.pieChartView.userInteractionEnabled = NO;
    
    self.viewLarge.layer.cornerRadius = self.viewLarge.frame.size.width/2;
    self.viewMedium.layer.cornerRadius = self.viewMedium.frame.size.width/2;
    self.viewSmall.layer.cornerRadius = self.viewSmall.frame.size.width/2;
    self.viewLarge.backgroundColor = UIColorFromRGB(0xF3FAF0);
    self.viewMedium.backgroundColor = UIColorFromRGB(0xE8F2DB);
    self.viewSmall.backgroundColor = UIColorFromRGB(0xCADAA9);
    
    self.txtAxis.text = LocalizedString(@"txt_freq", nil);
    self.txtNo.text = LocalizedString(@"txt_avg_wee", nil);
    self.txtSize.text = LocalizedString(@"txt_size", nil);
    
    if (IS_IPAD) {
        self.txtAxis.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:17.0f];
        self.txtNo.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:20.0f];
        self.txtSize.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:20.0f];
        self.txtLarge.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:22.0f];
        self.txtMedium.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:22.0f];
        self.txtSmall.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:22.0f];
    }
    [self layoutSubviews];

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

- (void)setBounds:(CGRect)bounds
{
    [super setBounds:bounds];
    self.contentView.frame = self.bounds;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    [self.contentView updateConstraintsIfNeeded];
    [self.contentView layoutIfNeeded];
    
    self.txtAxis.preferredMaxLayoutWidth = CGRectGetWidth(self.txtAxis.frame);
    self.txtNo.preferredMaxLayoutWidth = CGRectGetWidth(self.txtNo.frame);
    self.txtSize.preferredMaxLayoutWidth = CGRectGetWidth(self.txtSize.frame);
    self.txtLarge.preferredMaxLayoutWidth = CGRectGetWidth(self.txtLarge.frame);
    self.txtMedium.preferredMaxLayoutWidth = CGRectGetWidth(self.txtMedium.frame);
    self.txtSmall.preferredMaxLayoutWidth = CGRectGetWidth(self.txtSmall.frame);
}

@end
