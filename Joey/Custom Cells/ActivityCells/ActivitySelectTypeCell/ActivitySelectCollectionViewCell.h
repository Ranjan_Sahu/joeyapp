//
//  ActivitySelectCollectionViewCell.h
//  Joey
//
//  Created by Aishwarya Rai on 22/02/18.
//  Copyright © 2018 Auxilia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ActivitySelectCollectionViewCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UIImageView *imgActivityType;
@property (weak, nonatomic) IBOutlet UILabel *txtActivityType;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imgActivityHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imgActivityWidthConstraint;

@end
