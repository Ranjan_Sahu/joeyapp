//
//  ActivitySelectCollectionViewCell.m
//  Joey
//
//  Created by Aishwarya Rai on 22/02/18.
//  Copyright © 2018 Auxilia. All rights reserved.
//

#import "ActivitySelectCollectionViewCell.h"

@implementation ActivitySelectCollectionViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.contentView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    
    self.imgActivityType.clipsToBounds = YES;
    self.imgActivityType.contentMode = UIViewContentModeScaleAspectFit;
    self.imgActivityType.image = [self.imgActivityType.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    
    self.txtActivityType.textColor  = UIColorFromRGB(0x827577);
    
    if (IS_IPAD) {
        self.imgActivityHeightConstraint.constant = 44.0;
        self.imgActivityHeightConstraint.constant = 44.0;
        self.txtActivityType.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:22.0f];
    }else {
        self.imgActivityHeightConstraint.constant = 36.0;
        self.imgActivityHeightConstraint.constant = 36.0;
        self.txtActivityType.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:11.0f];
    }
    
    [self layoutIfNeeded];
    
}

- (void)setBounds:(CGRect)bounds
{
    [super setBounds:bounds];
    self.contentView.frame = self.bounds;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    [self.contentView updateConstraintsIfNeeded];
    [self.contentView layoutIfNeeded];
    [self.imgActivityType layoutIfNeeded];
    
    self.txtActivityType.preferredMaxLayoutWidth = CGRectGetWidth(self.txtActivityType.frame);
}

@end
