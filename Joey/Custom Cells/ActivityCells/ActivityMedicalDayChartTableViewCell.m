//
//  ActivityMedicalDayChartTableViewCell.m
//  Joey
//
//  Created by csl on 9/1/2017.
//  Copyright © 2017 Auxilia. All rights reserved.
//

#import "ActivityMedicalDayChartTableViewCell.h"
#import "ChartAxisValueFormatter.h"

@implementation ActivityMedicalDayChartTableViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    [self setBackgroundColor:[UIColor clearColor]];
    
    self.chartView.legend.enabled = NO;
    self.chartView.chartDescription.enabled = NO;
    self.chartView.pinchZoomEnabled = NO;
    self.chartView.highlightPerTapEnabled = NO;
    
    ChartXAxis *xAxis = self.chartView.xAxis;
    xAxis.labelPosition = XAxisLabelPositionBottom;
    xAxis.labelFont = [UIFont fontWithName:@"SFUIDisplay-Medium" size:10.0f];
    if (IS_IPAD) {
        xAxis.labelFont = [UIFont fontWithName:@"SFUIDisplay-Medium" size:16.0f];
    }
    xAxis.labelTextColor = UIColorFromRGB(0xBFB5AE);
    xAxis.labelCount = 9;
    xAxis.axisMinimum = -1;
    xAxis.axisMaximum = 24;
    xAxis.axisLineColor = UIColorFromRGB(0xEEE8E4);
    xAxis.drawGridLinesEnabled = YES;
    xAxis.gridColor = UIColorFromRGB(0xEEE8E4);
    
    ChartYAxis *leftAxis = self.chartView.leftAxis;
    leftAxis.labelPosition = YAxisLabelPositionOutsideChart;
    leftAxis.labelFont = [UIFont fontWithName:@"SFUIDisplay-Medium" size:10.0f];
    if (IS_IPAD) {
        leftAxis.labelFont = [UIFont fontWithName:@"SFUIDisplay-Medium" size:16.0f];
    }
    leftAxis.labelTextColor = UIColorFromRGB(0xBFB5AE);
    leftAxis.labelCount = 11;
    leftAxis.maxWidth = 44;
    leftAxis.minWidth = 18;
    leftAxis.axisLineColor = UIColorFromRGB(0xEEE8E4);
    leftAxis.drawGridLinesEnabled = NO;
    leftAxis.gridColor = UIColorFromRGB(0xEEE8E4);
    leftAxis.axisMinimum = 0;
    leftAxis.axisMaximum = 11;
    leftAxis.valueFormatter = [[ChartAxisValueFormatter alloc] initForChart:self.chartView type:ChartAxisTypeForMedical];
    
    ChartYAxis *rightAxis = self.chartView.rightAxis;
    rightAxis.labelPosition = YAxisLabelPositionOutsideChart;
    rightAxis.labelFont = [UIFont fontWithName:@"SFUIDisplay-Medium" size:10.0f];
    if (IS_IPAD) {
        rightAxis.labelFont = [UIFont fontWithName:@"SFUIDisplay-Medium" size:16.0f];
    }
    rightAxis.labelTextColor = [UIColor clearColor];
    rightAxis.maxWidth = 30;
    rightAxis.minWidth = 30;
    rightAxis.drawAxisLineEnabled = NO;
    rightAxis.drawGridLinesEnabled = NO;
    
    self.temperatureChartView.legend.enabled = NO;
    self.temperatureChartView.chartDescription.enabled = NO;
    self.temperatureChartView.pinchZoomEnabled = NO;
    self.temperatureChartView.highlightPerTapEnabled = YES;
    
    ChartXAxis *xAxis2 = self.temperatureChartView.xAxis;
    xAxis2.labelPosition = XAxisLabelPositionBottom;
    xAxis2.labelFont = [UIFont fontWithName:@"SFUIDisplay-Medium" size:10.0f];
    if (IS_IPAD) {
        xAxis2.labelFont = [UIFont fontWithName:@"SFUIDisplay-Medium" size:16.0f];
    }
    xAxis2.labelTextColor = [UIColor clearColor];
    xAxis2.labelCount = 9;
    xAxis2.axisMinimum = -1;
    xAxis2.axisMaximum = 24;
    xAxis2.drawAxisLineEnabled = NO;
    xAxis2.drawGridLinesEnabled = NO;
    
    ChartYAxis *leftAxis2 = self.temperatureChartView.leftAxis;
    leftAxis2.labelPosition = YAxisLabelPositionOutsideChart;
    leftAxis2.labelFont = [UIFont fontWithName:@"SFUIDisplay-Medium" size:10.0f];
    if (IS_IPAD) {
        leftAxis2.labelFont = [UIFont fontWithName:@"SFUIDisplay-Medium" size:16.0f];
    }
    leftAxis2.labelTextColor = [UIColor clearColor];
    leftAxis2.maxWidth = 20;
    leftAxis2.minWidth = 20;
    leftAxis2.drawAxisLineEnabled = NO;
    leftAxis2.drawGridLinesEnabled = NO;
    
    ChartYAxis *rightAxis2 = self.temperatureChartView.rightAxis;
    rightAxis2.labelPosition = YAxisLabelPositionOutsideChart;
    rightAxis2.labelFont = [UIFont fontWithName:@"SFUIDisplay-Medium" size:10.0f];
    if (IS_IPAD) {
        rightAxis2.labelFont = [UIFont fontWithName:@"SFUIDisplay-Medium" size:16.0f];
    }
    rightAxis2.labelTextColor = UIColorFromRGB(0xBFB5AE);
    rightAxis2.maxWidth = 30;
    rightAxis2.minWidth = 30;
    rightAxis2.drawAxisLineEnabled = NO;
    rightAxis2.drawGridLinesEnabled = NO;
    
    
    /*self.chartView.legend.enabled = NO;
    self.chartView.chartDescription.enabled = NO;
    self.chartView.pinchZoomEnabled = NO;
    self.chartView.highlightPerTapEnabled = NO;
    
    ChartXAxis *xAxis = self.chartView.xAxis;
    xAxis.labelPosition = XAxisLabelPositionBottom;
    xAxis.labelFont = [UIFont fontWithName:@"SFUIDisplay-Medium" size:10.0f];
    xAxis.labelTextColor = UIColorFromRGB(0xBFB5AE);
    xAxis.labelCount = 8;
    xAxis.axisLineColor = UIColorFromRGB(0xEEE8E4);
    xAxis.drawGridLinesEnabled = YES;
    xAxis.gridColor = UIColorFromRGB(0xEEE8E4);
    
    ChartYAxis *leftAxis = self.chartView.leftAxis;
    leftAxis.maxWidth = 20;
    leftAxis.minWidth = 20;
    leftAxis.labelPosition = YAxisLabelPositionOutsideChart;
    leftAxis.labelFont = [UIFont fontWithName:@"SFUIDisplay-Medium" size:10.0f];
    leftAxis.labelTextColor = UIColorFromRGB(0xBFB5AE);
    leftAxis.labelCount = 3;
    leftAxis.axisLineColor = UIColorFromRGB(0xEEE8E4);
    leftAxis.axisMinimum = 0;
    leftAxis.drawGridLinesEnabled = NO;
    leftAxis.gridColor = UIColorFromRGB(0xEEE8E4);
    
    ChartYAxis *rightAxis = self.chartView.rightAxis;
    rightAxis.enabled = YES;
    rightAxis.drawLabelsEnabled = YES;
    rightAxis.drawGridLinesEnabled = NO;
    rightAxis.labelFont = [UIFont fontWithName:@"SFUIDisplay-Medium" size:10.0f];
    rightAxis.labelTextColor = UIColorFromRGB(0xBFB5AE);
    rightAxis.axisLineColor = UIColorFromRGB(0xEEE8E4);
    
    self.temperatureChartView.legend.enabled = NO;
    self.temperatureChartView.chartDescription.enabled = NO;
    self.temperatureChartView.pinchZoomEnabled = NO;
    self.temperatureChartView.highlightPerTapEnabled = NO;

    ChartXAxis *xAxis2 = self.temperatureChartView.xAxis;
    xAxis2.enabled = NO;
    
    ChartYAxis *leftAxis2 = self.temperatureChartView.leftAxis;
    leftAxis2.maxWidth = 20;
    leftAxis2.minWidth = 20;
    leftAxis2.drawLabelsEnabled = YES;
    leftAxis2.labelFont = [UIFont fontWithName:@"SFUIDisplay-Medium" size:10.0f];
    leftAxis2.labelTextColor = [UIColor clearColor];
    leftAxis2.drawAxisLineEnabled = NO;
    leftAxis2.drawGridLinesEnabled = NO;
    
    ChartYAxis *rightAxis2 = self.temperatureChartView.rightAxis;
    rightAxis2.enabled = YES;
    rightAxis2.drawLabelsEnabled = YES;
    rightAxis2.labelFont = [UIFont fontWithName:@"SFUIDisplay-Medium" size:10.0f];
    rightAxis2.labelTextColor = [UIColor clearColor];
    rightAxis2.drawAxisLineEnabled = NO;
    rightAxis2.drawGridLinesEnabled = NO;*/
    
    self.viewMed1.backgroundColor = UIColorFromRGB(0xC5D5DF);
    self.viewMed2.backgroundColor = UIColorFromRGB(0xC5D5DF);
    self.viewMed3.backgroundColor = UIColorFromRGB(0xC5D5DF);
    self.viewMed4.backgroundColor = UIColorFromRGB(0xC5D5DF);
    self.viewMed5.backgroundColor = UIColorFromRGB(0xC5D5DF);
    self.viewMed6.backgroundColor = UIColorFromRGB(0xC5D5DF);
    self.viewMed7.backgroundColor = UIColorFromRGB(0xC5D5DF);

    self.viewSymp1.backgroundColor = UIColorFromRGB(0xC9DAAB);
    self.viewSymp2.backgroundColor = UIColorFromRGB(0xC9DAAB);
    self.viewSymp3.backgroundColor = UIColorFromRGB(0xC9DAAB);

    self.txtMedSummary.text = LocalizedString(@"txt_dailyMedicine", nil);
    self.txtSympSummary.text = LocalizedString(@"txt_dailySymptoms", nil);
    self.txtLeftAxis.text = LocalizedString(@"txt_medicineSymptoms", nil);

//    self.medName1.text = LocalizedString(@"med1", nil);
//    self.medName2.text = LocalizedString(@"med2", nil);
//    self.medName3.text = LocalizedString(@"med3", nil);
//    self.medName4.text = LocalizedString(@"med4", nil);
//    self.medName5.text = LocalizedString(@"med5", nil);
//    
//    self.sympName1.text = LocalizedString(@"symp1", nil);
//    self.sympName2.text = LocalizedString(@"symp2", nil);
//    self.sympName3.text = LocalizedString(@"symp3", nil);

    self.chartViewHC.constant = 280;
    if (IS_IPAD) {
        self.chartViewHC.constant = 310;
        self.txtLeftAxis.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:17.0f];
        self.txtRightAxis.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:17.0f];
        
        self.txtMedSummary.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:20.0f];
        self.txtSympSummary.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:20.0f];

        self.medName1.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:20.0f];
        self.medValue1.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:20.0f];
        self.medName2.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:20.0f];
        self.medValue2.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:20.0f];
        self.medName3.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:20.0f];
        self.medValue3.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:20.0f];
        self.medName4.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:20.0f];
        self.medValue4.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:20.0f];
        self.medName5.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:20.0f];
        self.medValue5.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:20.0f];
        self.medName6.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:20.0f];
        self.medValue6.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:20.0f];
        self.medName7.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:20.0f];
        self.medValue7.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:20.0f];

        self.sympName1.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:20.0f];
        self.sympValue1.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:20.0f];
        self.sympName2.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:20.0f];
        self.sympValue2.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:20.0f];
        self.sympName3.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:20.0f];
        self.sympValue3.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:20.0f];

    }
    
    [self layoutSubviews];

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    [self.contentView updateConstraintsIfNeeded];
    [self.contentView layoutIfNeeded];
    
    self.txtLeftAxis.preferredMaxLayoutWidth = CGRectGetWidth(self.txtLeftAxis.frame);
    self.txtRightAxis.preferredMaxLayoutWidth = CGRectGetWidth(self.txtRightAxis.frame);
    self.txtMedSummary.preferredMaxLayoutWidth = CGRectGetWidth(self.txtMedSummary.frame);
    self.txtSympSummary.preferredMaxLayoutWidth = CGRectGetWidth(self.txtSympSummary.frame);

    self.medName1.preferredMaxLayoutWidth = CGRectGetWidth(self.medName1.frame);
    self.medName2.preferredMaxLayoutWidth = CGRectGetWidth(self.medName2.frame);
    self.medName3.preferredMaxLayoutWidth = CGRectGetWidth(self.medName3.frame);
    self.medName4.preferredMaxLayoutWidth = CGRectGetWidth(self.medName4.frame);
    self.medName5.preferredMaxLayoutWidth = CGRectGetWidth(self.medName5.frame);
    self.medName6.preferredMaxLayoutWidth = CGRectGetWidth(self.medName6.frame);
    self.medName7.preferredMaxLayoutWidth = CGRectGetWidth(self.medName7.frame);

    self.sympName1.preferredMaxLayoutWidth = CGRectGetWidth(self.sympName1.frame);
    self.sympName2.preferredMaxLayoutWidth = CGRectGetWidth(self.sympName2.frame);
    self.sympName3.preferredMaxLayoutWidth = CGRectGetWidth(self.sympName3.frame);

}

@end
