//
//  ActivityReminderTableViewCell.h
//  Joey
//
//  Created by csl on 28/5/2017.
//  Copyright © 2017 Auxilia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ActivityReminderTableViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *txtName;
@property (nonatomic, weak) IBOutlet UILabel *txtValue;
@property (nonatomic, weak) IBOutlet UIButton *btnDelete;
@property (nonatomic, weak) IBOutlet UISwitch *switchReminder;

@end
