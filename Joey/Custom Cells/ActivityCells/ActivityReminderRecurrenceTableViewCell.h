//
//  ActivityReminderRecurrenceTableViewCell.h
//  Joey
//
//  Created by csl on 28/5/2017.
//  Copyright © 2017 Auxilia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ActivityReminderRecurrenceTableViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *txtName;
@property (nonatomic, weak) IBOutlet UIImageView *iconTick;

@end
