//
//  ActivityPooDayChartTableViewCell.h
//  Joey
//
//  Created by csl on 9/1/2017.
//  Copyright © 2017 Auxilia. All rights reserved.
//

#import <UIKit/UIKit.h>
@import Charts;

@interface ActivityPooDayChartTableViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet ScatterChartView *chartView;
@property (nonatomic, weak) IBOutlet PieChartView *pieChartView;
@property (nonatomic, weak) IBOutlet UILabel *txtAxis;
@property (nonatomic, weak) IBOutlet UILabel *txtNo;
@property (nonatomic, weak) IBOutlet UILabel *txtSize;
@property (nonatomic, weak) IBOutlet UIView *viewLarge;
@property (nonatomic, weak) IBOutlet UILabel *txtLarge;
@property (nonatomic, weak) IBOutlet UIView *viewMedium;
@property (nonatomic, weak) IBOutlet UILabel *txtMedium;
@property (nonatomic, weak) IBOutlet UIView *viewSmall;
@property (nonatomic, weak) IBOutlet UILabel *txtSmall;

@end
