//
//  ActivityFeedDayChartTableViewCell.h
//  Joey
//
//  Created by csl on 9/1/2017.
//  Copyright © 2017 Auxilia. All rights reserved.
//

#import <UIKit/UIKit.h>
@import Charts;

@interface ActivityFeedDayChartTableViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet BarChartView *chartView;
@property (nonatomic, weak) IBOutlet PieChartView *pieChartView;
@property (nonatomic, weak) IBOutlet UILabel *txtAxis;
@property (nonatomic, weak) IBOutlet UILabel *txtNo;
@property (nonatomic, weak) IBOutlet UILabel *txtSummary;
@property (nonatomic, weak) IBOutlet UIView *viewBottle;
@property (nonatomic, weak) IBOutlet UILabel *txtBottle;
@property (nonatomic, weak) IBOutlet UILabel *txtBottleValue;
@property (nonatomic, weak) IBOutlet UIView *viewBoob;
@property (nonatomic, weak) IBOutlet UILabel *txtBoob;
@property (nonatomic, weak) IBOutlet UILabel *txtBoobValue;
@property (nonatomic, weak) IBOutlet UIView *viewSolid;
@property (nonatomic, weak) IBOutlet UILabel *txtSolid;
@property (nonatomic, weak) IBOutlet UILabel *txtSolidValue;
@property (nonatomic, weak) IBOutlet UIButton *btnBottle;
@property (nonatomic, weak) IBOutlet UIButton *btnBoob;
@property (nonatomic, weak) IBOutlet UIButton *btnSolid;

@end
