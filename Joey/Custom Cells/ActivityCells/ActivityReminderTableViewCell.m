//
//  ActivityReminderTableViewCell.m
//  Joey
//
//  Created by csl on 28/5/2017.
//  Copyright © 2017 Auxilia. All rights reserved.
//

#import "ActivityReminderTableViewCell.h"

@implementation ActivityReminderTableViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self setBackgroundColor:[UIColor clearColor]];
    if (IS_IPAD) {
        self.txtName.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.txtValue.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:22.0f];
        self.btnDelete.transform = CGAffineTransformMakeScale(1.3, 1.3);
    }else {
        self.txtName.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:16.0f];
        self.txtValue.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:14.0f];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

- (void)setBounds:(CGRect)bounds
{
    [super setBounds:bounds];
    self.contentView.frame = self.bounds;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    [self.contentView updateConstraintsIfNeeded];
    [self.contentView layoutIfNeeded];
    
    self.txtName.preferredMaxLayoutWidth = CGRectGetWidth(self.txtName.frame);
    self.txtValue.preferredMaxLayoutWidth = CGRectGetWidth(self.txtValue.frame);
}

@end
