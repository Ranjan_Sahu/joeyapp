//
//  ActivitySleepMonthChartTableViewCell.m
//  Joey
//
//  Created by csl on 9/1/2017.
//  Copyright © 2017 Auxilia. All rights reserved.
//

#import "ActivitySleepMonthChartTableViewCell.h"
#import "ChartAxisValueFormatter.h"

@implementation ActivitySleepMonthChartTableViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    [self setBackgroundColor:[UIColor clearColor]];
    
    self.chartView.drawBarShadowEnabled = NO;
    self.chartView.drawValueAboveBarEnabled = NO;
    self.chartView.legend.enabled = NO;
    self.chartView.chartDescription.enabled = NO;
    self.chartView.pinchZoomEnabled = NO;
    self.chartView.highlightPerTapEnabled = NO;
    self.chartView.fitBars = YES;
    self.chartView.rightAxis.enabled = NO;
    
    ChartXAxis *xAxis = self.chartView.xAxis;
    xAxis.labelPosition = XAxisLabelPositionBottom;
    xAxis.labelFont = [UIFont fontWithName:@"SFUIDisplay-Medium" size:10.0f];
    if (IS_IPAD) {
        xAxis.labelFont = [UIFont fontWithName:@"SFUIDisplay-Medium" size:16.0f];
    }
    xAxis.labelTextColor = UIColorFromRGB(0xBFB5AE);
    xAxis.labelCount = 6;
    xAxis.axisLineColor = UIColorFromRGB(0xEEE8E4);
    xAxis.drawGridLinesEnabled = YES;
    xAxis.gridColor = UIColorFromRGB(0xEEE8E4);
    xAxis.valueFormatter = [[ChartAxisValueFormatter alloc] initForChart:self.chartView type:ChartAxisTypeForMonth];
    
    ChartYAxis *leftAxis = self.chartView.leftAxis;
    leftAxis.labelPosition = YAxisLabelPositionOutsideChart;
    leftAxis.labelFont = [UIFont fontWithName:@"SFUIDisplay-Medium" size:10.0f];
    if (IS_IPAD) {
        leftAxis.labelFont = [UIFont fontWithName:@"SFUIDisplay-Medium" size:16.0f];
    }
    leftAxis.labelTextColor = UIColorFromRGB(0xBFB5AE);
    leftAxis.labelCount = 4;
    leftAxis.axisMinimum = 0;
    leftAxis.axisLineColor = UIColorFromRGB(0xEEE8E4);
    leftAxis.drawGridLinesEnabled = NO;
    leftAxis.gridColor = UIColorFromRGB(0xEEE8E4);
    
    self.pieChartView.legend.enabled = NO;
    self.pieChartView.chartDescription.enabled = NO;
    self.pieChartView.drawSliceTextEnabled = NO;
    self.pieChartView.holeRadiusPercent = 0.75f;
    self.pieChartView.userInteractionEnabled = NO;
    
    self.viewDay.backgroundColor = UIColorFromRGB(0xF5EEB3);
    self.viewNight.backgroundColor = UIColorFromRGB(0xCAD7E0);
    
    self.txtAxisTop.text = LocalizedString(@"txt_hours", nil);
    self.txtNo.text = LocalizedString(@"txt_avg_daily_sleep", nil);
    self.txtSummary.text = LocalizedString(@"txt_average", nil);
    self.txtDay.text = LocalizedString(@"txt_day", nil);
    self.txtNight.text = LocalizedString(@"txt_night", nil);
    
    if (IS_IPAD) {
        self.txtAxisTop.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:17.0f];
        self.txtAxisBottom.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:17.0f];
        self.txtNo.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:20.0f];
        self.txtSummary.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:20.0f];
        self.txtDay.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:22.0f];
        self.txtDayValue.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:22.0f];
        self.txtNight.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:22.0f];
        self.txtNightValue.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:22.0f];
    }
    [self layoutSubviews];

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

- (void)setBounds:(CGRect)bounds
{
    [super setBounds:bounds];
    self.contentView.frame = self.bounds;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    [self.contentView updateConstraintsIfNeeded];
    [self.contentView layoutIfNeeded];
    
    self.txtAxisTop.preferredMaxLayoutWidth = CGRectGetWidth(self.txtAxisTop.frame);
    self.txtAxisBottom.preferredMaxLayoutWidth = CGRectGetWidth(self.txtAxisBottom.frame);
    self.txtNo.preferredMaxLayoutWidth = CGRectGetWidth(self.txtNo.frame);
    self.txtSummary.preferredMaxLayoutWidth = CGRectGetWidth(self.txtSummary.frame);
    self.txtDay.preferredMaxLayoutWidth = CGRectGetWidth(self.txtDay.frame);
    self.txtDayValue.preferredMaxLayoutWidth = CGRectGetWidth(self.txtDayValue.frame);
    self.txtNight.preferredMaxLayoutWidth = CGRectGetWidth(self.txtNight.frame);
    self.txtNightValue.preferredMaxLayoutWidth = CGRectGetWidth(self.txtNightValue.frame);
}

@end
