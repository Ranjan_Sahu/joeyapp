//
//  ActivityFeedGuideTableViewCell.h
//  Joey
//
//  Created by csl on 21/6/2017.
//  Copyright © 2017 Auxilia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ActivityFeedGuideTableViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIView *guideView;
@property (nonatomic, weak) IBOutlet UIView *highlightView;
@property (nonatomic, weak) IBOutlet UIView *starView;
@property (nonatomic, weak) IBOutlet UILabel *lblName;
@property (nonatomic, weak) IBOutlet UILabel *txtNo;
@property (nonatomic, weak) IBOutlet UILabel *lblNo1;
@property (nonatomic, weak) IBOutlet UILabel *lblNo2;
@property (nonatomic, weak) IBOutlet UILabel *lblNo3;
@property (nonatomic, weak) IBOutlet UILabel *lblNo4;

@property (nonatomic, weak) IBOutlet NSLayoutConstraint *starViewLeadingConstraint;

@end
