//
//  ActivityTableViewCell.h
//  Joey
//
//  Created by csl on 8/1/2017.
//  Copyright © 2017 Auxilia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ActivityTableViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *txtTime;
@property (nonatomic, weak) IBOutlet UILabel *txtValue;
@property (nonatomic, weak) IBOutlet UILabel *txtNotes;
@property (nonatomic, weak) IBOutlet UIButton *btnPhoto;

@property (nonatomic, weak) IBOutlet UIImageView *topSeperator;

@end
