//
//  ActivityPooDayChartTableViewCell.m
//  Joey
//
//  Created by csl on 9/1/2017.
//  Copyright © 2017 Auxilia. All rights reserved.
//

#import "ActivityPooDayChartTableViewCell.h"
#import "ChartAxisValueFormatter.h"

@implementation ActivityPooDayChartTableViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    [self setBackgroundColor:[UIColor clearColor]];
    self.chartView.legend.enabled = NO;
    self.chartView.chartDescription.enabled = NO;
    self.chartView.pinchZoomEnabled = NO;
    self.chartView.highlightPerTapEnabled = NO;
    self.chartView.rightAxis.enabled = NO;
    
    
    ChartXAxis *xAxis = self.chartView.xAxis;
    xAxis.labelPosition = XAxisLabelPositionBottom;
    xAxis.labelFont = [UIFont fontWithName:@"SFUIDisplay-Medium" size:10.0f];
    if (IS_IPAD) {
        xAxis.labelFont = [UIFont fontWithName:@"SFUIDisplay-Medium" size:16.0f];
    }
    xAxis.labelTextColor = UIColorFromRGB(0xBFB5AE);
    xAxis.labelCount = 9;
    xAxis.axisMinimum = -2.8;
    xAxis.axisMaximum = 25;
    xAxis.axisLineColor = UIColorFromRGB(0xEEE8E4);
    xAxis.drawGridLinesEnabled = YES;
    xAxis.gridColor = UIColorFromRGB(0xEEE8E4);
    
    ChartYAxis *leftAxis = self.chartView.leftAxis;
    leftAxis.labelPosition = YAxisLabelPositionInsideChart;
    leftAxis.labelFont = [UIFont fontWithName:@"SFUIDisplay-Medium" size:10.0f];
    if (IS_IPAD) {
        leftAxis.labelFont = [UIFont fontWithName:@"SFUIDisplay-Medium" size:16.0f];
    }
    leftAxis.labelTextColor = UIColorFromRGB(0xBFB5AE);
    leftAxis.axisMinimum = 0;
    leftAxis.axisMaximum = 8;
    leftAxis.maxWidth = 44;
    leftAxis.minWidth = 18;
    leftAxis.axisLineColor = UIColorFromRGB(0xEEE8E4);
    leftAxis.drawGridLinesEnabled = NO;
    leftAxis.gridColor = UIColorFromRGB(0xEEE8E4);
    leftAxis.valueFormatter = [[ChartAxisValueFormatter alloc] initForChart:self.chartView type:ChartAxisTypeForPooDay];
    leftAxis.axisLineColor = [UIColor clearColor];
    
    
    self.pieChartView.legend.enabled = NO;
    self.pieChartView.chartDescription.enabled = NO;
    self.pieChartView.drawSliceTextEnabled = NO;
    self.pieChartView.holeRadiusPercent = 0.75f;
    self.pieChartView.userInteractionEnabled = NO;
    
    self.viewLarge.layer.cornerRadius = self.viewLarge.frame.size.width/2;
    self.viewMedium.layer.cornerRadius = self.viewMedium.frame.size.width/2;
    self.viewSmall.layer.cornerRadius = self.viewSmall.frame.size.width/2;
    self.viewLarge.backgroundColor = UIColorFromRGB(0xF6F0EA);
    self.viewMedium.backgroundColor = UIColorFromRGB(0xF0E2D4);
    self.viewSmall.backgroundColor = UIColorFromRGB(0xE0C5B8);
    
    self.txtAxis.text = LocalizedString(@"txt_texture", nil);
    self.txtNo.text = LocalizedString(@"txt_daily_poo", nil);
    self.txtSize.text = LocalizedString(@"txt_size", nil);
    
    if (IS_IPAD) {
        self.txtAxis.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:17.0f];
        self.txtNo.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:20.0f];
        self.txtSize.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:20.0f];
        self.txtLarge.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:22.0f];
        self.txtMedium.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:22.0f];
        self.txtSmall.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:22.0f];
    }
    [self layoutSubviews];
}

- (void)setDataCount:(int)count range:(double)range
{
    NSMutableArray *yVals1 = [[NSMutableArray alloc] init];
    NSMutableArray *yVals2 = [[NSMutableArray alloc] init];
    NSMutableArray *yVals3 = [[NSMutableArray alloc] init];
    
    
    
    /*[yVals1 addObject:[[BubbleChartDataEntry alloc] initWithX:3.5 y:2 size:3]];
     [yVals1 addObject:[[BubbleChartDataEntry alloc] initWithX:9.1 y:2 size:2]];
     [yVals1 addObject:[[BubbleChartDataEntry alloc] initWithX:18.3 y:3 size:3]];
     */
    
    for (int i = 0; i < count; i++)
    {
        double val = (double) (arc4random_uniform(range));
        double size = (double) (arc4random_uniform(range));
        
        val = (double) (arc4random_uniform(range));
        size = (double) (arc4random_uniform(range));
        [yVals2 addObject:[[BubbleChartDataEntry alloc] initWithX:i y:val size:size]];
        
        val = (double) (arc4random_uniform(range));
        size = (double) (arc4random_uniform(range));
        [yVals3 addObject:[[BubbleChartDataEntry alloc] initWithX:i y:val size:size]];
    }
    
    BubbleChartDataSet *set1 = [[BubbleChartDataSet alloc] initWithValues:yVals1 label:@"DS 1"];
    [set1 setColor:ChartColorTemplates.colorful[0] alpha:0.50f];
    [set1 setDrawValuesEnabled:YES];
    BubbleChartDataSet *set2 = [[BubbleChartDataSet alloc] initWithValues:yVals2 label:@"DS 2"];
    [set2 setColor:ChartColorTemplates.colorful[1] alpha:0.50f];
    [set2 setDrawValuesEnabled:YES];
    BubbleChartDataSet *set3 = [[BubbleChartDataSet alloc] initWithValues:yVals3 label:@"DS 3"];
    [set3 setColor:ChartColorTemplates.colorful[2] alpha:0.50f];
    [set3 setDrawValuesEnabled:YES];
    
    NSMutableArray *dataSets = [[NSMutableArray alloc] init];
    [dataSets addObject:set1];
    //[dataSets addObject:set2];
    //[dataSets addObject:set3];
    
    BubbleChartData *data = [[BubbleChartData alloc] initWithDataSets:dataSets];
    [data setDrawValues:NO];
    [data setValueFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:7.f]];
    [data setHighlightCircleWidth: 1.5];
    [data setValueTextColor:UIColor.whiteColor];
    
    _chartView.data = data;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

- (void)setBounds:(CGRect)bounds
{
    [super setBounds:bounds];
    self.contentView.frame = self.bounds;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    [self.contentView updateConstraintsIfNeeded];
    [self.contentView layoutIfNeeded];
    
    self.txtAxis.preferredMaxLayoutWidth = CGRectGetWidth(self.txtAxis.frame);
    self.txtNo.preferredMaxLayoutWidth = CGRectGetWidth(self.txtNo.frame);
    self.txtSize.preferredMaxLayoutWidth = CGRectGetWidth(self.txtSize.frame);
    self.txtLarge.preferredMaxLayoutWidth = CGRectGetWidth(self.txtLarge.frame);
    self.txtMedium.preferredMaxLayoutWidth = CGRectGetWidth(self.txtMedium.frame);
    self.txtSmall.preferredMaxLayoutWidth = CGRectGetWidth(self.txtSmall.frame);
}

@end

