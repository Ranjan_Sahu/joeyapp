//
//  ActivitySleepGuideTableViewCell.m
//  Joey
//
//  Created by csl on 21/6/2017.
//  Copyright © 2017 Auxilia. All rights reserved.
//

#import "ActivitySleepGuideTableViewCell.h"

@implementation ActivitySleepGuideTableViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    [self setBackgroundColor:[UIColor clearColor]];
    
    self.highlightView.backgroundColor = UIColorFromRGB(0xCAD7E0);
    
    self.lblName.text = LocalizedString(@"txt_sleep_guide", nil);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

- (void)setBounds:(CGRect)bounds
{
    [super setBounds:bounds];
    self.contentView.frame = self.bounds;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    [self.contentView updateConstraintsIfNeeded];
    [self.contentView layoutIfNeeded];
    
    self.lblName.preferredMaxLayoutWidth = CGRectGetWidth(self.lblName.frame);
    self.txtNo.preferredMaxLayoutWidth = CGRectGetWidth(self.txtNo.frame);
    self.lblNo1.preferredMaxLayoutWidth = CGRectGetWidth(self.lblNo1.frame);
    self.lblNo2.preferredMaxLayoutWidth = CGRectGetWidth(self.lblNo2.frame);
    self.lblNo3.preferredMaxLayoutWidth = CGRectGetWidth(self.lblNo3.frame);
    self.lblNo4.preferredMaxLayoutWidth = CGRectGetWidth(self.lblNo4.frame);
}

@end
