//
//  ActivityFeedDayChartTableViewCell.m
//  Joey
//
//  Created by csl on 9/1/2017.
//  Copyright © 2017 Auxilia. All rights reserved.
//

#import "ActivityFeedDayChartTableViewCell.h"
#import "ChartAxisValueFormatter.h"

@implementation ActivityFeedDayChartTableViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    [self setBackgroundColor:[UIColor clearColor]];
    
    self.chartView.drawBarShadowEnabled = NO;
    self.chartView.drawValueAboveBarEnabled = NO;
    //self.chartView.legend.enabled = NO;
    self.chartView.chartDescription.enabled = NO;
    self.chartView.pinchZoomEnabled = NO;
    self.chartView.highlightPerTapEnabled = NO;
    self.chartView.fitBars = YES;
    self.chartView.rightAxis.enabled = NO;
    
    ChartLegend *legend = self.chartView.legend;
    legend.horizontalAlignment = ChartLegendHorizontalAlignmentCenter;
    legend.verticalAlignment = ChartLegendVerticalAlignmentBottom;
    legend.orientation = ChartLegendOrientationHorizontal;
    legend.drawInside = NO;
    legend.form = ChartLegendFormSquare;
    legend.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:12.f];
    if (IS_IPAD) {
        legend.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:18.f];
    }
    legend.textColor = UIColorFromRGB(0xBFB5AE);
    legend.xEntrySpace = 10.0;
    
    ChartXAxis *xAxis = self.chartView.xAxis;
    xAxis.labelPosition = XAxisLabelPositionBottom;
    xAxis.labelFont = [UIFont fontWithName:@"SFUIDisplay-Medium" size:10.0f];
    if (IS_IPAD) {
        xAxis.labelFont = [UIFont fontWithName:@"SFUIDisplay-Medium" size:16.0f];
    }
    xAxis.labelTextColor = UIColorFromRGB(0xBFB5AE);
    xAxis.labelCount = 9;
    xAxis.axisLineColor = UIColorFromRGB(0xEEE8E4);
    xAxis.drawGridLinesEnabled = YES;
    xAxis.gridColor = UIColorFromRGB(0xEEE8E4);
    xAxis.valueFormatter = [[ChartAxisValueFormatter alloc] initForChart:self.chartView type:ChartAxisTypeForDay];
    
    ChartYAxis *leftAxis = self.chartView.leftAxis;
    leftAxis.labelPosition = YAxisLabelPositionOutsideChart;
    leftAxis.labelFont = [UIFont fontWithName:@"SFUIDisplay-Medium" size:10.0f];
    if (IS_IPAD) {
        leftAxis.labelFont = [UIFont fontWithName:@"SFUIDisplay-Medium" size:16.0f];
    }
    leftAxis.labelTextColor = UIColorFromRGB(0xBFB5AE);
    leftAxis.labelCount = 4;
    leftAxis.axisMinimum = 0;
    leftAxis.axisLineColor = UIColorFromRGB(0xEEE8E4);
    leftAxis.drawGridLinesEnabled = NO;
    leftAxis.gridColor = UIColorFromRGB(0xEEE8E4);
    
    self.pieChartView.legend.enabled = NO;
    self.pieChartView.chartDescription.enabled = NO;
    self.pieChartView.drawSliceTextEnabled = NO;
    self.pieChartView.holeRadiusPercent = 0.75f;
    self.pieChartView.userInteractionEnabled = NO;
    
    self.viewBottle.backgroundColor = UIColorFromRGB(0xF5EEB3);
    self.viewBoob.backgroundColor = UIColorFromRGB(0xE7D7CD);
    self.viewSolid.backgroundColor = UIColorFromRGB(0xD2E2E2);
    
    self.txtNo.text = LocalizedString(@"txt_no_of_feeds", nil);
    self.txtSummary.text = LocalizedString(@"txt_summary", nil);
    self.txtBottle.text = LocalizedString(@"txt_bottle", nil);
    //self.txtBoob.text = LocalizedString(@"txt_boob", nil);
    self.txtBoob.text = LocalizedString(@"nursing", nil);
    self.txtSolid.text = LocalizedString(@"txt_solid", nil);
    
    if (IS_IPAD) {
        self.txtAxis.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:17.0f];
        self.txtNo.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:20.0f];
        self.txtSummary.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:20.0f];
        self.txtBoob.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:22.0f];
        self.txtBoobValue.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:22.0f];
        self.txtBottle.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:22.0f];
        self.txtBottleValue.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:22.0f];
        self.txtSolid.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:22.0f];
        self.txtSolidValue.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:22.0f];
    }
    [self layoutSubviews];
}

- (IBAction)selectBottleAction:(id)sender
{
    [self.btnBottle setImage:[UIImage imageNamed:@"icon_feed_bottle_highlight"] forState:UIControlStateNormal];
    [self.btnBoob setImage:[UIImage imageNamed:@"icon_feed_breastfeed"] forState:UIControlStateNormal];
    [self.btnSolid setImage:[UIImage imageNamed:@"icon_feed_solids"] forState:UIControlStateNormal];
}

- (IBAction)selectBoobAction:(id)sender
{
    [self.btnBottle setImage:[UIImage imageNamed:@"icon_feed_bottle"] forState:UIControlStateNormal];
    [self.btnBoob setImage:[UIImage imageNamed:@"icon_feed_breastfeed_highlight"] forState:UIControlStateNormal];
    [self.btnSolid setImage:[UIImage imageNamed:@"icon_feed_solids"] forState:UIControlStateNormal];
}

- (IBAction)selectSolidAction:(id)sender
{
    [self.btnBottle setImage:[UIImage imageNamed:@"icon_feed_bottle"] forState:UIControlStateNormal];
    [self.btnBoob setImage:[UIImage imageNamed:@"icon_feed_breastfeed"] forState:UIControlStateNormal];
    [self.btnSolid setImage:[UIImage imageNamed:@"icon_feed_solids_highlight"] forState:UIControlStateNormal];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

- (void)setBounds:(CGRect)bounds
{
    [super setBounds:bounds];
    self.contentView.frame = self.bounds;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    [self.contentView updateConstraintsIfNeeded];
    [self.contentView layoutIfNeeded];
    
    self.txtAxis.preferredMaxLayoutWidth = CGRectGetWidth(self.txtAxis.frame);
    self.txtNo.preferredMaxLayoutWidth = CGRectGetWidth(self.txtNo.frame);
    self.txtSummary.preferredMaxLayoutWidth = CGRectGetWidth(self.txtSummary.frame);
    self.txtBottle.preferredMaxLayoutWidth = CGRectGetWidth(self.txtBottle.frame);
    self.txtBottleValue.preferredMaxLayoutWidth = CGRectGetWidth(self.txtBottleValue.frame);
    self.txtBoob.preferredMaxLayoutWidth = CGRectGetWidth(self.txtBoob.frame);
    self.txtBoobValue.preferredMaxLayoutWidth = CGRectGetWidth(self.txtBoobValue.frame);
    self.txtSolid.preferredMaxLayoutWidth = CGRectGetWidth(self.txtSolid.frame);
    self.txtSolidValue.preferredMaxLayoutWidth = CGRectGetWidth(self.txtSolidValue.frame);
}

@end
