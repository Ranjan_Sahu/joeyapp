//
//  ActivitySleepDayChartTableViewCell.h
//  Joey
//
//  Created by csl on 9/1/2017.
//  Copyright © 2017 Auxilia. All rights reserved.
//

#import <UIKit/UIKit.h>
@import Charts;

@interface ActivitySleepDayChartTableViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet HorizontalBarChartView *chartView;
@property (nonatomic, weak) IBOutlet PieChartView *pieChartView;
@property (nonatomic, weak) IBOutlet UILabel *txtAxisTop;
@property (nonatomic, weak) IBOutlet UILabel *txtAxisBottom;
@property (nonatomic, weak) IBOutlet UILabel *txtNo;
@property (nonatomic, weak) IBOutlet UILabel *txtSummary;
@property (nonatomic, weak) IBOutlet UIView *viewDay;
@property (nonatomic, weak) IBOutlet UILabel *txtDay;
@property (nonatomic, weak) IBOutlet UILabel *txtDayValue;
@property (nonatomic, weak) IBOutlet UIView *viewNight;
@property (nonatomic, weak) IBOutlet UILabel *txtNight;
@property (nonatomic, weak) IBOutlet UILabel *txtNightValue;

@end
