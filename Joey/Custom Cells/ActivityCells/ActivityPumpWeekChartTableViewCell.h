//
//  ActivityPumpWeekChartTableViewCell.h
//  Joey
//
//  Created by csl on 9/1/2017.
//  Copyright © 2017 Auxilia. All rights reserved.
//

#import <UIKit/UIKit.h>
@import Charts;

@interface ActivityPumpWeekChartTableViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet BarChartView *chartView;
@property (nonatomic, weak) IBOutlet PieChartView *pieChartView;
@property (nonatomic, weak) IBOutlet UILabel *txtAxis;
@property (nonatomic, weak) IBOutlet UILabel *txtNo;
@property (nonatomic, weak) IBOutlet UILabel *txtSummary;
@property (nonatomic, weak) IBOutlet UIView *viewLeft;
@property (nonatomic, weak) IBOutlet UILabel *txtLeft;
@property (nonatomic, weak) IBOutlet UILabel *txtLeftValue;
@property (nonatomic, weak) IBOutlet UIView *viewRight;
@property (nonatomic, weak) IBOutlet UILabel *txtRight;
@property (nonatomic, weak) IBOutlet UILabel *txtRightValue;

@end
