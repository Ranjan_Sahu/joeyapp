//
//  ActivitiesCollectionViewCell.h
//  Joey
//
//  Created by csl on 17/2/2017.
//  Copyright © 2017 Auxilia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ActivitiesCollectionViewCell : UICollectionViewCell

@property (nonatomic, weak) IBOutlet UIView *viewBg;
@property (nonatomic, weak) IBOutlet UIImageView *imgIcon;
@property (nonatomic, weak) IBOutlet UIImageView *imgIconAlarm;
@property (nonatomic, weak) IBOutlet UIImageView *imgIconAdd;
@property (nonatomic, weak) IBOutlet UIButton *btnAlarm;
@property (nonatomic, weak) IBOutlet UIButton *btnAdd;
@property (nonatomic, weak) IBOutlet UILabel *txtTime;
@property (nonatomic, weak) IBOutlet UILabel *txtDiff;
@property (nonatomic, weak) IBOutlet UILabel *txtValue;
@property (nonatomic, weak) IBOutlet UILabel *txtValue2;
@property (nonatomic, weak) IBOutlet UILabel *txtName;

@property (weak, nonatomic) IBOutlet UIImageView *imgQuickAdd;
@property (weak, nonatomic) IBOutlet UIButton *btnQuickAdd;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *quickAddImgWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *quickAddImgHeight;

@end
