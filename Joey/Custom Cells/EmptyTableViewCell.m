//
//  EmptyTableViewCell.m
//  Foodstep
//
//  Created by csl on 21/8/15.
//  Copyright © 2015 Foodstep Limited. All rights reserved.
//

#import "EmptyTableViewCell.h"

@implementation EmptyTableViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    [self setBackgroundColor:[UIColor clearColor]];
    
    self.txtMessage.textColor = UIColorFromRGB(0x8D8082);
    self.txtMessage.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:14.0f];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

- (void)setBounds:(CGRect)bounds
{
    [super setBounds:bounds];
    self.contentView.frame = self.bounds;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    [self.contentView updateConstraintsIfNeeded];
    [self.contentView layoutIfNeeded];
    
    self.txtMessage.preferredMaxLayoutWidth = CGRectGetWidth(self.txtMessage.frame);
}

@end
