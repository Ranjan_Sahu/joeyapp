//
//  EmptyTableViewCell.h
//  Foodstep
//
//  Created by csl on 21/8/15.
//  Copyright © 2015 Foodstep Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EmptyTableViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *txtMessage;

@end
