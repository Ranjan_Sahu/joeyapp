//
//  AppDelegate.m
//  Joey
//
//  Created by csl on 21/12/2016.
//  Copyright © 2016 Auxilia. All rights reserved.
//

#import "AppDelegate.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import <ChimpKit/ChimpKit.h>
#import "iRate.h"
#import <UserNotifications/UNUserNotificationCenter.h>

@interface AppDelegate ()
{
    NSTimer *activityFeedTimer;
    NSTimer *activityPumpTimer;
    int activityFeedTimerSecounds;
    int activityPumpTimerSecounds;
}
@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Appearance
    [[UIBarButtonItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"SFUIDisplay-Medium" size:17.0f], NSFontAttributeName, nil] forState:UIControlStateNormal];
    [[UIBarButtonItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"SFUIDisplay-Medium" size:17.0f], NSFontAttributeName, nil] forState:UIControlStateHighlighted];
    [[UIBarButtonItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"SFUIDisplay-Medium" size:17.0f], NSFontAttributeName, nil] forState:UIControlStateSelected];
    [[UIBarButtonItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"SFUIDisplay-Medium" size:17.0f], NSFontAttributeName, nil] forState:UIControlStateFocused];
    [[UIBarButtonItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"SFUIDisplay-Medium" size:17.0f], NSFontAttributeName, nil] forState:UIControlStateFocused];

    // Notification
    UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeBadge|UIUserNotificationTypeSound|UIUserNotificationTypeAlert) categories:nil];
    [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
    
    NSDictionary *remoteNotification = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
    if(remoteNotification) [self handleNotification:remoteNotification forApplication:application];
    
    NSDictionary *localNotification = [launchOptions objectForKey:UIApplicationLaunchOptionsLocalNotificationKey];
    if(localNotification) [self handleLocalNotification:localNotification forApplication:application];
    
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    
    // IQKeyboardManager
    [[IQKeyboardManager sharedManager] setEnable:YES];
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:YES];
    [[IQKeyboardManager sharedManager] setToolbarManageBehaviour:IQAutoToolbarByTag];
    [[IQKeyboardManager sharedManager] setShouldShowToolbarPlaceholder:YES];
    [[IQKeyboardManager sharedManager] setShouldResignOnTouchOutside:YES];
    
    // PHP
    self.window.frame = [UIScreen mainScreen].bounds;
    self.isNetworkAvailable = YES;
    
    // Facebook
    [[FBSDKApplicationDelegate sharedInstance] application:application didFinishLaunchingWithOptions:launchOptions];
    
    //Chimp Kit
    //[[ChimpKit sharedKit] setApiKey:@"9d73bdcab72477d82937734459f9095f-us15"]; // client
    //[[ChimpKit sharedKit] setApiKey:@"0b6dc7e223f89813632a27705bd93380-us17"]; // demo Ranjan
    
    // Fabric
    [Fabric with:@[[Crashlytics class]]];
    
    // Google Analytics
    self.tracker = [[GAI sharedInstance] trackerWithTrackingId:@"UA-90747461-3"];
    
    // Bluetooth
    /*dispatch_queue_t centralQueue = dispatch_queue_create("com.auxiliababy.joey", DISPATCH_QUEUE_SERIAL);
    self.cbCentralManager = [[CBCentralManager alloc] initWithDelegate:self queue:centralQueue];
    NSLog(@"self.cbCentralManager=%@", self.cbCentralManager);*/
    
    // Timer
    //[self startSyncData];
    //[NSTimer scheduledTimerWithTimeInterval:30.f target:self selector:@selector(startSyncData) userInfo:nil repeats:YES];
    NSTimeZone *timeZone2 = [NSTimeZone localTimeZone];
    NSString *tzName2 = [timeZone2 name];
    NSLog(@"tzName2=%@", tzName2);
        
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
    formatter.dateFormat = @"yyyyMMddHHmmss";
    NSString *utc = [formatter stringFromDate:[NSDate date]];
    [formatter setTimeZone:[NSTimeZone localTimeZone]];
    formatter.dateFormat = @"ZZZ";
    NSString *region = [formatter stringFromDate:[NSDate date]];
//    NSLog(@"time2=%@", [NSString stringWithFormat:@"%@%@U", utc, region]);
    
    // SVProgressHUD Appearance
    [SVProgressHUD setBackgroundColor:[UIColor groupTableViewBackgroundColor]];
    [SVProgressHUD setRingThickness:2.0];
    [SVProgressHUD setForegroundColor:UIColorFromRGB(0x96BFCE)];
    
    // iRate
    [iRate sharedInstance].applicationBundleID = @"com.auxiliababy.joeyPlus";  //Change this with updated BundleID
    [iRate sharedInstance].onlyPromptIfLatestVersion = NO;
    [iRate sharedInstance].previewMode = NO;
    [iRate sharedInstance].eventCount = 0;
    [iRate sharedInstance].eventsUntilPrompt = 3;
    //    [iRate sharedInstance].appStoreID = 1296721871;
    
    return YES;
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    return [[FBSDKApplicationDelegate sharedInstance] application:application openURL:url sourceApplication:sourceApplication annotation:annotation];
}

- (void)applicationWillResignActive:(UIApplication *)application
{
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    //NSLog(@"applicationDidEnterBackground=%@", self.bluetoothManager);
    //[self.bluetoothManager disconnectDevice];
    
    BOOL chkFeedTimer = [[NSUserDefaults standardUserDefaults] boolForKey:@"isFeedTimerOn"];
    BOOL chkPumpTimer = [[NSUserDefaults standardUserDefaults] boolForKey:@"isPumpTimerOn"];
    if (chkFeedTimer || chkPumpTimer) {
        [self setTimerAlertForBackgroundState];
    }
    
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    /*NSLog(@"applicationWillEnterForeground=%@", self.bluetoothManager);
    if(self.bluetoothManager)
    {
        NSLog(@"self.peripheral=%@", self.peripheral.name);
        [self.bluetoothManager connectDevice:self.peripheral];
    }
    else
    {
        [self scanForPeripherals:YES];
    }*/
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    self.badgeCount = (int)application.applicationIconBadgeNumber;
    [FBSDKAppEvents activateApp];
    
    UILocalNotification *notification = nil;
    for(UILocalNotification *notify in [[UIApplication sharedApplication] scheduledLocalNotifications])
    {
        if([[notify.userInfo objectForKey:@"notification_identifier"] isEqualToString:@"Timer_Alert"])
        {
            notification = notify;
            [[UIApplication sharedApplication] cancelLocalNotification:notification];
        }
    }
    
}

- (void)applicationWillTerminate:(UIApplication *)application
{
}


// Push
- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings
{
    [application registerForRemoteNotifications];
}

- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    self.deviceToken = [[[deviceToken description] stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]] stringByReplacingOccurrencesOfString:@" " withString:@""];
//    NSLog(@"My token is: %@, %@", self.deviceToken, self.isNetworkAvailable?@"YES":@"NO");
    
    NSUserDefaults *shareObject = [NSUserDefaults standardUserDefaults];
    if([shareObject integerForKey:@"user_id"] != 0)
    {
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setValue:@([shareObject integerForKey:@"user_id"]) forKey:@"user_id"];
        [parameters setValue:self.deviceToken forKey:@"device_token"];
        
        [[WebServiceManager sharedManager] initWithBaseURL:@"api/updateUser" parameters:parameters success:^(id result) {
            NSLog(@"ghanta");
        } failure:^(NSError *error) {
            NSLog(@"ghanta Error%@",error.localizedDescription);
        }];
    }
}

- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{
    NSLog(@"Failed to get token, error: %@", error);
}

- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification
{
    NSLog(@"didReceiveLocalNotification=%@", notification);
    [self handleLocalNotification:notification forApplication:application];
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    NSLog(@"didReceiveRemoteNotification");
    [self handleNotification:userInfo forApplication:application];
}

- (void)handleNotification:(NSDictionary *)userInfo forApplication:(UIApplication *)application
{
    NSUserDefaults *shareObject = [NSUserDefaults standardUserDefaults];
    int userId = [[[userInfo objectForKey:@"aps"] objectForKey:@"user_id"] intValue];
    int babyId = [[[userInfo objectForKey:@"aps"] objectForKey:@"baby_id"] intValue];
    NSString *name = [[userInfo objectForKey:@"aps"] objectForKey:@"name"];
    NSLog(@"handleNotification name=%@", name);
    
    if(userId == [shareObject integerForKey:@"user_id"])
    {
        NSMutableArray *babyList = (NSMutableArray *)[NSKeyedUnarchiver unarchiveObjectWithData:[shareObject objectForKey:@"baby_list"]];
        
        for(int i=0; i<babyList.count; i++){
            if(babyId == [[[babyList objectAtIndex:i] objectForKey:@"id"] intValue]){
                [shareObject setInteger:babyId forKey:@"baby_id"];
                [shareObject setInteger:i forKey:@"baby_index"];
                [shareObject synchronize];
                break;
            }
        }
        
        if([application applicationState] != UIApplicationStateActive)
        {
            if([name isEqualToString:@"grow"] || [name isEqualToString:@"move"] || [name isEqualToString:@"iq"] || [name isEqualToString:@"health"] || [name isEqualToString:@"others"]){
                [self.tabbar setSelectedIndex:2];
            }else if ([name isEqualToString:@"feed"] || [name isEqualToString:@"pump"] || [name isEqualToString:@"sleep"] || [name isEqualToString:@"poo"] || [name isEqualToString:@"wee"] || [name isEqualToString:@"medical"]){
                [self.tabbar setSelectedIndex:1];
            }
        }
        else
        {
            /*
             if([name isEqualToString:@"grow"] || [name isEqualToString:@"move"] || [name isEqualToString:@"iq"] || [name isEqualToString:@"health"] || [name isEqualToString:@"others"]) [[self.tabbar.tabBar.items objectAtIndex:2] setBadgeValue:@"1"];
             else [[self.tabbar.tabBar.items objectAtIndex:1] setBadgeValue:@"1"];
             
             UINavigationController *navigationController = ((UITabBarController *)self.window.rootViewController).selectedViewController;
             
             if(navigationController.childViewControllers.count > 0)
             {
             UIViewController *viewController = navigationController.childViewControllers[0];
             //NSLog(@"viewController=%@", viewController);
             [viewController viewWillAppear:YES];
             }
             */
            NSString *title = @"";
            if([name isEqualToString:@"grow"] || [name isEqualToString:@"move"] || [name isEqualToString:@"iq"] || [name isEqualToString:@"health"] || [name isEqualToString:@"others"]){
                title = @"New Milestone";
            }else if ([name isEqualToString:@"feed"] || [name isEqualToString:@"pump"] || [name isEqualToString:@"sleep"] || [name isEqualToString:@"poo"] || [name isEqualToString:@"wee"] || [name isEqualToString:@"medical"]){
                title = @"New Activity";
            }
            
            NSString *infoString = [NSString stringWithFormat:@"%@", [[userInfo objectForKey:@"aps"] objectForKey:@"alert"]];
            FCAlertView *alert = [[FCAlertView alloc] init];
            [alert showAlertInWindow:self.window withTitle:title withSubtitle:infoString withCustomImage:nil withDoneButtonTitle:@"Ok" andButtons:nil];
        }
    }
}

- (void)handleLocalNotification:(UILocalNotification *)notification forApplication:(UIApplication *)application
{
    NSLog(@"handleLocalNotification=%@", notification);
    self.updateActivitiesCheck = YES;
    
    if ([notification.userInfo objectForKey:@"notification_identifier"] != nil)
    {
        if([[notification.userInfo objectForKey:@"notification_identifier"] isEqualToString:@"Timer_Alert"])
        {
            NSLog(@"Timer_Alert");
        }
    }
    else
    {
        FCAlertView *alert = [[FCAlertView alloc] init];
        [alert showAlertInWindow:self.window withTitle:[NSString stringWithFormat:@"[%@] - %@", [notification.userInfo objectForKey:@"baby_name"], LocalizedString(@"header_reminders", nil)] withSubtitle:[notification.userInfo objectForKey:@"title"] withCustomImage:nil withDoneButtonTitle:@"Ok" andButtons:nil];
        
        if(self.tabbar.selectedIndex == 1)
        {
            UINavigationController *navigationController = ((UITabBarController *)self.window.rootViewController).selectedViewController;
            
            if(navigationController.childViewControllers.count > 0)
            {
                UIViewController *viewController = navigationController.childViewControllers[0];
                //NSLog(@"viewController1=%@", viewController);
                [viewController viewWillAppear:YES];
            }
        }
    }
}


/*#pragma mark Central Manager delegate methods
- (void)centralManagerDidUpdateState:(CBCentralManager *)central
{
    if([central state] == CBCentralManagerStatePoweredOff)
    {
        NSLog(@"CoreBluetooth BLE hardware is powered off");
    }
    else if([central state] == CBCentralManagerStatePoweredOn)
    {
        NSLog(@"CoreBluetooth BLE hardware is powered on and ready");
        [self scanForPeripherals:YES];
    }
    else if([central state] == CBCentralManagerStateUnauthorized)
    {
        NSLog(@"CoreBluetooth BLE state is unauthorized");
    }
    else if([central state] == CBCentralManagerStateUnknown)
    {
        NSLog(@"CoreBluetooth BLE state is unknown");
    }
    else if([central state] == CBCentralManagerStateUnsupported)
    {
        NSLog(@"CoreBluetooth BLE hardware is unsupported on this platform");
    }
}*/

/*!
 * @brief Starts scanning for peripherals with rscServiceUUID
 * @param enable If YES, this method will enable scanning for bridge devices, if NO it will stop scanning
 * @return 0 if success, -1 if Bluetooth Manager is not in CBCentralManagerStatePoweredOn state.
 */
/*- (int)scanForPeripherals:(BOOL)enable
{
    NSLog(@"AppDelegate scanForPeripherals=%@", enable?@"YES":@"NO");
    if(self.cbCentralManager.state != CBCentralManagerStatePoweredOn)
    {
        return -1;
    }
    
    // Scanner uses other queue to send events. We must edit UI in the main queue
    dispatch_async(dispatch_get_main_queue(), ^{
        if(enable)
        {
            //NSLog(@"scanForPeripherals=%@", self.cbCentralManager);
            NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:YES], CBCentralManagerScanOptionAllowDuplicatesKey, nil];
            [self.cbCentralManager scanForPeripheralsWithServices:nil options:options];
        }
        else
        {
            [self.cbCentralManager stopScan];
        }
    });
    
    return 0;
}

- (void)centralManager:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)peripheral advertisementData:(NSDictionary *)advertisementData RSSI:(NSNumber *)RSSI
{
    // Scanner uses other queue to send events. We must edit UI in the main queue
    if ([[advertisementData objectForKey:CBAdvertisementDataIsConnectable] boolValue])
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            // Add the sensor to the list and reload deta set
            ScannedPeripheral *sensor = [ScannedPeripheral initWithPeripheral:peripheral rssi:RSSI.intValue isPeripheralConnected:NO];
            //NSLog(@"appDelegate sensor=%@", sensor.name);
            
            NSUserDefaults *shareObject = [NSUserDefaults standardUserDefaults];
            
            if([shareObject objectForKey:@"device_list"])
            {
                NSMutableArray *deviceList = (NSMutableArray *)[NSKeyedUnarchiver unarchiveObjectWithData:[shareObject objectForKey:@"device_list"]];
                //NSLog(@"deviceList=%@", deviceList);
                
                for(int i=0; i<deviceList.count; i++)
                {
                    if([[[deviceList objectAtIndex:i] objectForKey:@"name"] isEqualToString:sensor.name] && [[[deviceList objectAtIndex:i] objectForKey:@"wearable_check"] boolValue])
                    {
                        [shareObject setInteger:[[[deviceList objectAtIndex:i] objectForKey:@"baby_id"] intValue] forKey:@"wearable_baby_id"];
                        [shareObject synchronize];
                        
                        self.peripheral = [sensor peripheral];
                        [self scanForPeripherals:NO];
                        [self centralManager:self.cbCentralManager didPeripheralSelected:self.peripheral];
                        
                        NSLog(@"Connecting to %@...", self.peripheral.name);
                        [self.cbCentralManager connectPeripheral:self.peripheral options:nil];
                        break;
                    }
                }
            }
        });
    }
}


#pragma mark - Scanner Delegate methods
- (void)centralManager:(CBCentralManager *)manager didPeripheralSelected:(CBPeripheral *)peripheral
{
    NSLog(@"AppDelegate connectPeripheral");
    // We may not use more than one Central Manager instance. Let's just take the one returned from Scanner View Controller
    self.bluetoothManager = [[BluetoothManager alloc] initWithManager:manager];
    self.bluetoothManager.delegate = self;
    [self.bluetoothManager connectDevice:peripheral];
}

#pragma mark - BluetoothManager delegate methods
- (void)didDeviceConnected:(NSString *)peripheralName
{
    NSLog(@"didDeviceConnected %@", peripheralName);
}
- (void)didDeviceDisconnected
{
    NSLog(@"didDeviceDisconnected");
}
- (void)isDeviceReady
{
    NSLog(@"Device is ready");
}
- (void)deviceNotSupported
{
    NSLog(@"Device not supported");
}
- (void)didCharacteristicsFound
{
    NSLog(@"didCharacteristicsFound");
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
    formatter.dateFormat = @"yyyyMMddHHmmss";
    NSString *utc = [formatter stringFromDate:[NSDate date]];
    [formatter setTimeZone:[NSTimeZone localTimeZone]];
    formatter.dateFormat = @"ZZZ";
    NSString *region = [formatter stringFromDate:[NSDate date]];
    NSLog(@"time2=%@", [NSString stringWithFormat:@"%@%@U", utc, region]);
    
    [self.bluetoothManager send:[NSString stringWithFormat:@"%@%@U", utc, region]];
}
- (void)didUpdateValueForCharacteristic:(NSString *)message
{
    NSLog(@"didUpdateValueForCharacteristic %@", message);
    
    NSArray *hexData = [message componentsSeparatedByString:@"-"];
    if(hexData.count >= 20)
    {
        message = @"";
        for(int i=0; i<hexData.count; i++)
        {
            NSString * hexChar = hexData[i];
            int value = 0;
            sscanf([hexChar cStringUsingEncoding:NSASCIIStringEncoding], "%x", &value);
            message = [NSString stringWithFormat:@"%@%c", message, (char)value];
        }
        
        NSLog(@"message=%@", message);
        if(![message hasPrefix:@"U"])
        {
            [self addActivity:message];
        }
    }
}

- (void)addActivity:(NSString *)message
{
    NSLog(@"addActivity");
    NSLog(@"message=%@", message);
    
    NSString *name = [message substringWithRange:NSMakeRange(0, 2)];
    if([name isEqualToString:@"FE"]) {name = @"feed"; NSLog(@"here");}
    if([name isEqualToString:@"PO"]) name = @"poo";
    if([name isEqualToString:@"WE"]) name = @"wee";
    if([name isEqualToString:@"SL"]) name = @"sleep";
    if([name isEqualToString:@"PU"]) name = @"pump";
    if([name isEqualToString:@"ME"]) name = @"medical";
    NSLog(@"name=%@", name);
    
    NSLog(@"time=%@", [message substringWithRange:NSMakeRange(2, 12)]);
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
    formatter.dateFormat = @"yyyyMMddHHmmss";
    NSDate *datetime = [formatter dateFromString:[NSString stringWithFormat:@"20%@", [message substringWithRange:NSMakeRange(2, 12)]]];
    NSLog(@"datetime=%@", datetime);
    
    NSUserDefaults *shareObject = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setValue:@([shareObject integerForKey:@"user_id"]) forKey:@"user_id"];
    [parameters setValue:@([shareObject integerForKey:@"wearable_baby_id"]) forKey:@"baby_id"];
    [parameters setValue:name forKey:@"name"];
    [parameters setValue:[[NSString stringWithFormat:@"%@", datetime] substringWithRange:NSMakeRange(0, 19)] forKey:@"time"];
    
    if([name isEqualToString:@"feed"])
    {
        NSString *type = [message substringWithRange:NSMakeRange(15, 1)];
        if([type isEqualToString:@"0"]) type = @"bbottle";
        if([type isEqualToString:@"1"]) type = @"formula";
        if([type isEqualToString:@"2"]) type = @"lboob";
        if([type isEqualToString:@"3"]) type = @"rboob";
        if([type isEqualToString:@"4"]) type = @"solids";
        if([type isEqualToString:@"5"]) type = @"water";
        if([type isEqualToString:@"6"]) type = @"juice";
        
        int value = [[message substringWithRange:NSMakeRange(16, 3)] intValue];
        NSLog(@"value=%d", value);
        
        [parameters setValue:type forKey:@"type"];
        if([type isEqualToString:@"lboob"] || [type isEqualToString:@"rboob"])
        {
            [parameters setValue:@(value) forKey:@"value"];
            [parameters setValue:@"min" forKey:@"unit"];
            [parameters setValue:@(value) forKey:@"duration"];
        }
        else if([type isEqualToString:@"solids"])
        {
            [parameters setValue:@(value) forKey:@"value"];
            [parameters setValue:@"gm" forKey:@"unit"];
        }
        else
        {
            [parameters setValue:@(value) forKey:@"value"];
            [parameters setValue:@"ml" forKey:@"unit"];
        }
    }
    else if([name isEqualToString:@"poo"])
    {
        NSString *type = @"poo";
        NSString *color = [message substringWithRange:NSMakeRange(15, 1)];
        NSString *texture = [message substringWithRange:NSMakeRange(16, 1)];
        NSString *size = [message substringWithRange:NSMakeRange(17, 1)];
        
        if([color isEqualToString:@"3"]) color = @"brown";
        if([color isEqualToString:@"4"]) color = @"yellow";
        if([color isEqualToString:@"5"]) color = @"green";
        if([color isEqualToString:@"6"]) color = @"black";
        if([color isEqualToString:@"7"]) color = @"alert";
        
        if([texture isEqualToString:@"8"]) texture = @"hard";
        if([texture isEqualToString:@"9"]) texture = @"soft";
        if([texture isEqualToString:@"A"]) texture = @"runny";
        if([texture isEqualToString:@"B"]) texture = @"alert";
        
        if([size isEqualToString:@"0"]) size = @"large";
        if([size isEqualToString:@"1"]) size = @"medium";
        if([size isEqualToString:@"2"]) size = @"small";
        
        [parameters setValue:type forKey:@"type"];
        [parameters setValue:color forKey:@"color"];
        [parameters setValue:texture forKey:@"texture"];
        [parameters setValue:size forKey:@"volume"];
        [parameters setValue:@(1)forKey:@"value"];
        [parameters setValue:@"time" forKey:@"unit"];
    }
    else if([name isEqualToString:@"wee"])
    {
        NSString *type = @"wee";
        NSString *size = [message substringWithRange:NSMakeRange(16, 1)];
        NSString *color = [message substringWithRange:NSMakeRange(17, 1)];

        if([size isEqualToString:@"4"]) size = @"large";
        if([size isEqualToString:@"5"]) size = @"medium";
        if([size isEqualToString:@"6"]) size = @"small";
        
        if([color isEqualToString:@"0"]) color = @"yellow";
        if([color isEqualToString:@"1"]) color = @"orange";
        if([color isEqualToString:@"2"]) color = @"clear";
        if([color isEqualToString:@"3"]) color = @"alert";
 
        [parameters setValue:type forKey:@"type"];
        [parameters setValue:color forKey:@"color"];
        [parameters setValue:size forKey:@"volume"];
        [parameters setValue:@(1)forKey:@"value"];
        [parameters setValue:@"time" forKey:@"unit"];
    }
    else if([name isEqualToString:@"pump"])
    {
        NSString *type = [message substringWithRange:NSMakeRange(14, 1)];
        NSString *duration = [message substringWithRange:NSMakeRange(15, 2)];
        NSString *value = [message substringWithRange:NSMakeRange(17, 3)];
        
        if([type isEqualToString:@"0"]) type = @"left";
        if([type isEqualToString:@"1"]) type = @"right";
        
        [parameters setValue:type forKey:@"type"];
        [parameters setValue:value forKey:@"value"];
        [parameters setValue:@"ml" forKey:@"unit"];
        [parameters setValue:duration forKey:@"duration"];
    }
    else if([name isEqualToString:@"sleep"])
    {
        NSString *type = [message substringWithRange:NSMakeRange(15, 1)];
        NSLog(@"type=%@", type);
        int value = 0;
        
        if([type isEqualToString:@"0"]) type = @"sleeping";
        if([type isEqualToString:@"1"]) type = @"awake";
        if([type isEqualToString:@"2"]) type = @"manual";
        
        if([type isEqualToString:@"manual"])
        {
            type = @"awake";
            value = [[message substringWithRange:NSMakeRange(16, 4)] intValue];
            NSLog(@"value=%d", value);
        }
        
        [parameters setValue:type forKey:@"type"];
        [parameters setValue:@(value) forKey:@"value"];
        [parameters setValue:@(value) forKey:@"duration"];
        [parameters setValue:@"min" forKey:@"unit"];
    }
    else if([name isEqualToString:@"medical"])
    {
        NSString *type = [message substringWithRange:NSMakeRange(15, 1)];
        NSLog(@"type=%@", type);
        
        if([type isEqualToString:@"0"]) type = @"med1";
        if([type isEqualToString:@"1"]) type = @"med2";
        if([type isEqualToString:@"2"]) type = @"med3";
        if([type isEqualToString:@"3"]) type = @"med4";
        if([type isEqualToString:@"4"]) type = @"temperature";
        if([type isEqualToString:@"5"]) type = @"vomit";
        if([type isEqualToString:@"6"]) type = @"cough";
        
        [parameters setValue:type forKey:@"type"];
        if([type isEqualToString:@"med1"] || [type isEqualToString:@"med2"] || [type isEqualToString:@"med3"] || [type isEqualToString:@"med4"])
        {
            int value = [[message substringWithRange:NSMakeRange(16, 3)] intValue];
            NSLog(@"value=%d", value);
            
            [parameters setValue:@(value) forKey:@"value"];
            [parameters setValue:@"ml" forKey:@"unit"];
        }
        else if([type isEqualToString:@"temperature"])
        {
            int value1 = [[message substringWithRange:NSMakeRange(16, 2)] intValue];
            int value2 = [[message substringWithRange:NSMakeRange(18, 1)] intValue];
            float value = [[NSString stringWithFormat:@"%d.%d", value1, value2] floatValue];
            NSLog(@"value=%f", value);
            
            [parameters setValue:@(value) forKey:@"value"];
            [parameters setValue:@"°C" forKey:@"unit"];
        }
        else if([type isEqualToString:@"vomit"])
        {
            [parameters setValue:@(1) forKey:@"value"];
            [parameters setValue:@"time" forKey:@"unit"];
        }
        else if([type isEqualToString:@"cough"])
        {
            [parameters setValue:@(1) forKey:@"value"];
            [parameters setValue:@"time" forKey:@"unit"];
        }
    }
    
    [[WebServiceManager sharedManager] initWithBaseURL:@"api/addBabyActivity" parameters:parameters success:^(id result) {
        
        if([[result objectForKey:@"success"] boolValue])
        {
            self.updateTimelineCheck = self.updateActivitiesCheck = self.updateActivityCheck = YES;
        }
        
    } failure:^(NSError *error) {
        
    }];
}*/

#pragma mark- Activity Timer methods
-(void) setTimerAlertForBackgroundState {
    
    NSString *timerActivityName;
    BOOL chkFeedTimer = [[NSUserDefaults standardUserDefaults] boolForKey:@"isFeedTimerOn"];
    BOOL chkPumpTimer = [[NSUserDefaults standardUserDefaults] boolForKey:@"isPumpTimerOn"];
    if (chkFeedTimer) {
        timerActivityName = @"Feed";
    }else if (chkPumpTimer){
        timerActivityName = @"Pump";
    }
    
    for (int i=1; i<=24; i++) {
        UILocalNotification *notification = [[UILocalNotification alloc]init];
        notification.timeZone = [NSTimeZone defaultTimeZone];
        notification.fireDate = [NSDate dateWithTimeIntervalSinceNow:5*60*i];
        // Set the notification ID and type data.
        NSMutableDictionary *notificationInfo = [[NSMutableDictionary alloc] init];
        [notificationInfo setObject:@"Timer_Alert" forKey:@"notification_identifier"];
        notification.userInfo = notificationInfo;
        // Set the notification description.
        notification.alertTitle =@"Timer - Alert";
        notification.alertBody = [NSString stringWithFormat:@"Your %@ Timer is ON !!",timerActivityName ];
        notification.soundName = UILocalNotificationDefaultSoundName;
        [[UIApplication sharedApplication] scheduleLocalNotification:notification];
    }
    
}

-(void) setFeedTimerAlertForActiveState{
    if (activityFeedTimer == nil || ![activityFeedTimer isValid]) {
        activityFeedTimerSecounds = 0;
        activityFeedTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(activityFeedTimerAction) userInfo:nil repeats:YES];
    }
}

-(void)activityFeedTimerAction{
    activityFeedTimerSecounds++;
    if (activityFeedTimerSecounds%300 == 0) {
        [COMMON_HELPER vibrateDevice];
    }
}

-(void) removeFeedTimerAlertForActiveState{
    if ([activityFeedTimer isValid] || activityFeedTimer != nil) {
        [activityFeedTimer invalidate];
        activityFeedTimer = nil;
        activityFeedTimerSecounds = 0;
    }
}

-(void) setPumpTimerAlertForActiveState{
    if (activityPumpTimer == nil || ![activityPumpTimer isValid]) {
        activityPumpTimerSecounds = 0;
        activityPumpTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(activityPumpTimerAction) userInfo:nil repeats:YES];
    }
}

-(void)activityPumpTimerAction{
    activityPumpTimerSecounds++;
    if (activityPumpTimerSecounds%300 == 0) {
        [COMMON_HELPER vibrateDevice];
    }
}

-(void) removePumpTimerAlertForActiveState{
    if ([activityPumpTimer isValid] || activityPumpTimer != nil) {
        [activityPumpTimer invalidate];
        activityPumpTimer = nil;
        activityPumpTimerSecounds = 0;
    }
    
}

@end
