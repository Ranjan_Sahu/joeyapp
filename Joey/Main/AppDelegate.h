//
//  AppDelegate.h
//  Joey
//
//  Created by csl on 21/12/2016.
//  Copyright © 2016 Auxilia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FBSDKCoreKit.h"
#import "FBSDKLoginKit.h"


@interface AppDelegate : UIResponder <UIApplicationDelegate>//, CBCentralManagerDelegate, BluetoothManagerDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) NSString *deviceToken;
@property (strong, nonatomic) UITabBarController *tabbar;
//@property (strong, nonatomic) BluetoothManager *bluetoothManager;
//@property (strong, nonatomic) CBCentralManager *cbCentralManager;
//@property (strong, nonatomic) CBPeripheral *peripheral;
@property (strong, nonatomic) id<GAITracker> tracker;
@property (nonatomic) int badgeCount;
@property (nonatomic) BOOL isNetworkAvailable;
@property (nonatomic) BOOL updateAppCheck;
@property (nonatomic) BOOL updateTimelineCheck;
@property (nonatomic) BOOL updateActivitiesCheck;
@property (nonatomic) BOOL updateActivityCheck;
@property (nonatomic) BOOL updateMilestonesCheck;
@property (nonatomic) BOOL updateGalleryCheck;

//- (int)scanForPeripherals:(BOOL)enable;
//- (void)addActivity:(NSString *)message;

-(void) setFeedTimerAlertForActiveState;
-(void) removeFeedTimerAlertForActiveState;
-(void) setPumpTimerAlertForActiveState;
-(void) removePumpTimerAlertForActiveState;

@end

