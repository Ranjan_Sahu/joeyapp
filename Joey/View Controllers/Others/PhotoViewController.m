//
//  PhotoViewController.m
//  Joey
//
//  Created by csl on 10/1/2017.
//  Copyright © 2017 Auxilia. All rights reserved.
//

#import "UIViewController+data.h"
#import "PhotoViewController.h"
#import "PECropViewController.h"
#import "KDViewPager.h"
#import "ContentViewController.h"

@interface PhotoViewController ()<PECropViewControllerDelegate,KDViewPagerDelegate,KDViewPagerDatasource>
{
    UIImageView *largeImageView;
    UIImage *newCroppedImage;
}
@property (nonatomic) UIPopoverPresentationController *popover;
@property (strong,nonatomic) KDViewPager * pager;

@end

@implementation PhotoViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.imgPhoto.contentMode = UIViewContentModeScaleAspectFit;
   
    self.imgPhoto.image = (UIImage *)[self.data objectForKey:@"image"];
    self.btnDelete.hidden = [[self.data objectForKey:@"view_check"] boolValue];
    self.btnCrop.hidden = ![[self.data objectForKey:@"show_crop"] boolValue];
    [self.data removeObjectForKey:@"show_crop"];
    
    // Page Control
    self.pgControl.numberOfPages = self.originalImgArray.count;
    self.pgControl.currentPage = 0;
    self.pgControl.pageIndicatorTintColor = [UIColor groupTableViewBackgroundColor];
}

-(void)viewWillAppear:(BOOL)animated{
    
    if(_isMultiple){
        self.imgPhoto.hidden = YES;
        _pager = [[KDViewPager alloc] initWithController:self inView:self.view];
        _pager.delegate = self;
        _pager.datasource = self;
        _pager.bounces = YES;
        [self.view bringSubviewToFront:self.btnClose];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [_pager setCurrentPage:_index];
        });
        
    }
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (IBAction)closeAction:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)shareAction:(id)sender
{

}

- (IBAction)deleteAction:(id)sender
{
    /*UIViewController *viewController = [self.parentViewController.viewControllers objectAtIndex:self.navigationController.viewControllers.count-2];
    viewController.data = [[NSMutableDictionary alloc] initWithDictionary:@{@"save_check":@(YES)}];*/
    UIViewController *viewController = (UIViewController *)[self.data objectForKey:@"viewController"];
    
    if ([self.data objectForKey:@"imageTag"]) {
        viewController.data = [[NSMutableDictionary alloc] initWithDictionary:@{@"deleteCheck":@(YES),@"imageTag":@([[self.data objectForKey:@"imageTag"]integerValue])}];
    }else {
        viewController.data = [[NSMutableDictionary alloc] initWithDictionary:@{@"deleteCheck":@(YES)}];
    }
    
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(IBAction)didSelectCrop:(UIButton *)sender
{
    [self openEditor:self.imgPhoto.image];
}

#pragma mark - PECropViewControllerDelegate methods

- (void)openEditor:(UIImage*) image1
{
    PECropViewController *controller = [[PECropViewController alloc] init];
    controller.delegate = self;
    controller.toolbarHidden = YES;
    controller.image = image1;
    controller.cropAspectRatio = [[NSUserDefaults standardUserDefaults] floatForKey:@"Img_Aspect"];
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:controller];
    if (IS_IPAD) {
        navigationController.modalPresentationStyle = UIModalPresentationFullScreen;
    }
    [self presentViewController:navigationController animated:YES completion:NULL];
    
}

- (void)cropViewController:(PECropViewController *)controller didFinishCroppingImage:(UIImage *)croppedImage transform:(CGAffineTransform)transform cropRect:(CGRect)cropRect
{
    [controller dismissViewControllerAnimated:YES completion:NULL];
    
    float scale = self.view.bounds.size.width*2/croppedImage.size.width;
    CGSize newSize = CGSizeMake(self.view.bounds.size.width*2, croppedImage.size.height*scale);
    
    if(croppedImage.size.width > croppedImage.size.height)
    {
        scale = self.view.bounds.size.width*2/croppedImage.size.height;
        newSize = CGSizeMake(croppedImage.size.width*scale, self.view.bounds.size.width*2);
    }
    UIGraphicsBeginImageContext(newSize);
    [croppedImage drawInRect:(CGRect){0, 0, newSize}];
    croppedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    newCroppedImage  = croppedImage;
    UIViewController *viewController = (UIViewController *)[self.data objectForKey:@"viewController"];
    viewController.data = [[NSMutableDictionary alloc] initWithDictionary:@{@"cropCheck":@(YES), @"newScaledImage":newCroppedImage,@"imageTag":@([[self.data objectForKey:@"imageTag"] integerValue])}];
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

- (void)cropViewControllerDidCancel:(PECropViewController *)controller
{
    [controller dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark - datasource
-(UIViewController *)kdViewPager:(KDViewPager *)viewPager controllerAtIndex:(NSUInteger)index cachedController:(ContentViewController *)cachedController {
    

    NSString * ogImg = @"";

    if(_originalImgArray.count > 0){
        ogImg = [_originalImgArray objectAtIndex:index];
    }
    
    if (cachedController == nil) {
        CGRect frame = self.view.frame;
        frame.size.height *= 2;
//        frame.size.height -= 64;
//        frame.origin.y += 64;
        cachedController = [[ContentViewController alloc] initWithImage:ogImg withFrame:frame];
        
    }
    return cachedController;
}

-(NSUInteger)numberOfPages:(KDViewPager *)viewPager {
    
    if(_originalImgArray.count == 0) {
        return 1;
    }
    return _originalImgArray.count;
    
}

#pragma mark - delegate
-(void)kdViewpager:(KDViewPager *)viewPager didSelectPage:(NSUInteger)index direction:(UIPageViewControllerNavigationDirection)direction {
    NSLog(@"didSelectpage: %lu direction: %lu", index, direction);
    self.pgControl.currentPage = index;
    
}
-(void)kdViewpager:(KDViewPager *)viewPager willSelectPage:(NSUInteger)index direction:(UIPageViewControllerNavigationDirection)direction{
    
}

@end
