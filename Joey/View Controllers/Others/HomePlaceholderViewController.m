//
//  HomePlaceholderViewController.m
//  Joey
//
//  Created by csl on 8/10/2017.
//  Copyright © 2017 Auxilia. All rights reserved.
//

#import "AppDelegate.h"
#import "HomePlaceholderViewController.h"

@interface HomePlaceholderViewController ()
{
    NSUserDefaults *shareObject;
    AppDelegate *appDelegate;
}
@end

@implementation HomePlaceholderViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    shareObject = [NSUserDefaults standardUserDefaults];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];

    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:UIColorFromRGB(0x8D8082), NSForegroundColorAttributeName, [UIFont fontWithName:@"SFUIDisplay-Medium" size:18.0f], NSFontAttributeName, nil]];
    [self.navigationController.navigationBar setBarTintColor:UIColorFromRGB(0xEEE8E4)];
    [self.navigationController.navigationBar setTranslucent:NO];
    [self customSettingsButton];
    
    UIButton *btnLogo = [[UIButton alloc] initWithFrame:CGRectZero];
    [btnLogo setImage:[UIImage imageNamed:@"nav_logo.png"] forState:UIControlStateNormal];
    [btnLogo sizeToFit];
    
    self.navigationItem.titleView = btnLogo;
    
    self.txtMessage1.text = LocalizedString(@"txt_home_message1", nil);
    self.txtMessage2.text = LocalizedString(@"txt_home_message2", nil);
    self.txtMessage3.text = LocalizedString(@"txt_home_message3", nil);
    self.txtMessage4.text = LocalizedString(@"txt_home_message4", nil);
}

- (void)viewWillAppear:(BOOL)animated
{
    NSLog(@"HomeViewController viewWillAppear");
    [super viewWillAppear:(BOOL)animated];
    
    [appDelegate.tracker send:[[[GAIDictionaryBuilder createScreenView] set:@"@Home" forKey:kGAIScreenName] build]];
    
    [self.navigationController.navigationBar setBarTintColor:UIColorFromRGB(0xEEE8E4)];
}

- (void)customSettingsButton
{
    UIImage *imgSettings = [UIImage imageNamed:@"nav_btn_settings"];
    UIButton *btnSettings = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnSettings setImage:imgSettings forState:UIControlStateNormal];
    btnSettings.frame = CGRectMake(52, 0, imgSettings.size.width, imgSettings.size.height);
    [btnSettings addTarget:self action:@selector(getSettingsAction:) forControlEvents:UIControlEventTouchUpInside];
    
    UIView *buttonView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 88, 44)];
    [buttonView addSubview:btnSettings];
    
    UIBarButtonItem *backBarItem = [[UIBarButtonItem alloc] initWithCustomView:buttonView];
    self.navigationItem.rightBarButtonItem = backBarItem;
}

- (void)getSettingsAction:(id)sender
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Settings" bundle:nil];
    UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"SettingsViewController"];
    viewController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:viewController animated:YES];
}

- (IBAction)getAboutAction:(id)sender
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Settings" bundle:nil];
    UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"SettingsAboutWebViewController"];
    viewController.data = [[NSMutableDictionary alloc] initWithDictionary:@{@"title":LocalizedString(@"title_settings_about", nil), @"url":@"http://www.joeyforparents.com"}];
    viewController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:viewController animated:YES];
}


@end
