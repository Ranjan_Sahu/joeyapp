//
//  SplashViewController.m
//  Joey
//
//  Created by csl on 22/12/2016.
//  Copyright © 2016 Auxilia. All rights reserved.
//

#import "AppDelegate.h"
#import "SplashViewController.h"
#import <ChimpKit/ChimpKit.h>

@interface SplashViewController ()<ChimpKitRequestDelegate>
{
    NSUserDefaults *shareObject;
    AppDelegate *appDelegate;
    NSString *language;
    Boolean termsCheck;
}
@end

@implementation SplashViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSLog(@"locale=%@", [[NSLocale currentLocale] localeIdentifier]);
    NSLog(@"preferredLanguages=%@", [[NSLocale preferredLanguages] objectAtIndex:0]);
    self.lblTerms.text = @"";
    termsCheck = NO;
    
    shareObject = [NSUserDefaults standardUserDefaults];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    language = [[NSLocale preferredLanguages] objectAtIndex:0];
    
    if([language hasPrefix:@"zh-Hant-HK"])
    {
        language = @"zh-Hant-HK";
    }
    else if([language hasPrefix:@"zh-Hant"])
    {
        language = @"zh-Hant";
    }
    else if([language hasPrefix:@"zh-Hans"])
    {
        language = @"zh-Hans";
    }
    else
    {
        language = @"en";
    }
    [shareObject setObject:language forKey:@"language"];
    [shareObject synchronize];
    LocalizationSetLanguage(language);
    
    [self.navigationController setNavigationBarHidden:YES];
    self.lblTitle.text = LocalizedString(@"txt_splash_title", nil);
    self.btnSignin.hidden = YES;
    self.btnSignup.hidden = YES;
    self.btnFacebook.hidden = YES;
    
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTapOnCheckAgreement:)];
    tapRecognizer.numberOfTapsRequired = 1;
    self.imgCheck.userInteractionEnabled = YES;
    [self.imgCheck addGestureRecognizer:tapRecognizer];
    
    NSLog(@"user_id=%d", [shareObject integerForKey:@"user_id"]);
    if([shareObject integerForKey:@"user_id"] != 0)
    {
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setValue:@([shareObject integerForKey:@"user_id"]) forKey:@"user_id"];
        [parameters setValue:language forKey:@"language"];
        [parameters setValue:appDelegate.deviceToken forKey:@"device_token"];
        
        [[WebServiceManager sharedManager] initWithBaseURL:@"api/updateUserDeviceToken" parameters:parameters success:^(id result) {
            
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            //[self.view removeFromSuperview];
        
            if(![shareObject boolForKey:@"email_confirm_check"])
            {
                UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"AccountVerificationViewController"];
                [self.navigationController pushViewController:viewController animated:NO];
                //UIViewController *navigationController = [storyboard instantiateViewControllerWithIdentifier:@"AccountVerificationNavigationController"];
                //appDelegate.window.rootViewController = navigationController;
                
                [self setupLayout];
            }
            else
            {
                [self setupLayout];
                if([[result objectForKey:@"success"] boolValue])
                {
                    if([[result valueForKey:@"family_list"] count] > 0)
                    {
                        [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableArray *)[result valueForKey:@"family_list"]] forKey:@"family_list"];
                    }
                    if([[result valueForKey:@"baby_list"] count] > 0)
                    {
                        [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableArray *)[result valueForKey:@"baby_list"]] forKey:@"baby_list"];
                    }
                    if([[result valueForKey:@"device_list"] count] > 0)
                    {
                        [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableArray *)[result valueForKey:@"device_list"]] forKey:@"device_list"];
                    }
                    [shareObject synchronize];
                    
                    NSMutableArray *familyList = (NSMutableArray *)[NSKeyedUnarchiver unarchiveObjectWithData:[shareObject objectForKey:@"family_list"]];
                    if(familyList.count > 0)
                    {
                        NSMutableArray *babyList = (NSMutableArray *)[NSKeyedUnarchiver unarchiveObjectWithData:[shareObject objectForKey:@"baby_list"]];
                        if(babyList.count > 0)
                        {
                            [self.view removeFromSuperview];
                            
                            UIViewController *tabBarController = [storyboard instantiateViewControllerWithIdentifier:@"TabBarController"];
                            appDelegate.window.rootViewController = tabBarController;
                        }
                        else
                        {
                            UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"NewBabyViewController"];
                            [self.navigationController pushViewController:viewController animated:YES];
                            [self setupLayout];
                        }
                    }
                    else
                    {
                        UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"InvitationViewController"];
                        viewController.data = [[NSMutableDictionary alloc] initWithDictionary:@{@"splash_page_check":@(YES)}];
                        [self.navigationController pushViewController:viewController animated:NO];
                        [self setupLayout];
                    }
                }
            }
        } failure:^(NSError *error) {
            NSLog(@"Error %@", error.localizedDescription);
        }];
    }
    else [self setupLayout];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
}

- (void)setupLayout
{
    
    self.imgCheck.layer.borderWidth = 2;
    self.imgCheck.layer.borderColor = UIColorFromRGB(0xEDE8E4).CGColor;

    if (IS_IPAD) {
        self.lblTitle.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:22.0f];
        [self.btnSignin.titleLabel setFont:[UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f]];
        [self.btnSignup.titleLabel setFont:[UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f]];
        [self.btnFacebook.titleLabel setFont:[UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f]];
        self.fbHeight.constant = 54.0;
        self.signInHeight.constant = 54.0;
        self.signUpHeight.constant = 54.0;
        self.logoHeight.constant = 120.0;
        self.logoWidth.constant = 147.0;
        self.imgCheckWidth.constant = 30.0;
        self.imgCheckHeight.constant = 30.0;
    }
    
    self.btnSignin.hidden = NO;
    self.btnSignin.layer.cornerRadius = 5.f;
    [self.btnSignin setTitle:LocalizedString(@"btn_splash_signin", nil) forState:UIControlStateNormal];
    
    self.btnSignup.hidden = NO;
    self.btnSignup.layer.cornerRadius = 5.f;
    [self.btnSignup setTitle:LocalizedString(@"btn_splash_signup", nil) forState:UIControlStateNormal];
    
    self.btnFacebook.hidden = NO;
    self.btnFacebook.layer.cornerRadius = 5.f;
    [self.btnFacebook setTitle:LocalizedString(@"btn_facebook", nil) forState:UIControlStateNormal];
    [self.btnFacebook setImageEdgeInsets:UIEdgeInsetsMake(0, -10, 0, 10)];
    
    [self setAttributedTextWithLink];
}

- (IBAction)getSigninAction:(id)sender
{
    if (!termsCheck) {
        [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:LocalizedString(@"error_agree_terms", nil) input:nil];
        return;
    }else {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"SigninViewController"];
        [self.navigationController pushViewController:viewController animated:YES];
    }
}

- (IBAction)getSignupAction:(id)sender
{
    if (!termsCheck) {
        [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:LocalizedString(@"error_agree_terms", nil) input:nil];
        return;
    }else {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"SignupViewController"];
        [self.navigationController pushViewController:viewController animated:YES];
    }
}

- (IBAction)facebookConnectAction:(id)sender
{
    if (!termsCheck) {
        [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:LocalizedString(@"error_agree_terms", nil) input:nil];
        return;
    }
    
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logOut];
    [login logInWithReadPermissions:@[@"public_profile", @"email"] fromViewController:self handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
         if(error)
         {
             NSLog(@"Process error");
         }
         else if (result.isCancelled)
         {
             NSLog(@"Cancelled");
         }
         else
         {
            NSLog(@"success");
             if([result.grantedPermissions containsObject:@"email"])
             {
                 if([result.grantedPermissions containsObject:@"email"])
                 {
                     FBSDKGraphRequest *request = [[FBSDKGraphRequest alloc]
                                                   initWithGraphPath:[NSString stringWithFormat:@"/%@",result.token.userID]
                                                   parameters:@{@"fields": @"picture, email, name, gender, id"}
                                                   HTTPMethod:@"GET"];
                     
                     [request startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
                         NSLog(@"result=%@", result);
                         
                         NSString *facebookId = [result objectForKey:@"id"];
                         NSString *name = [result objectForKey:@"name"];
                         NSString *email = [result objectForKey:@"email"];
                         if(email.length == 0) email = @"";
                         NSString *gender = [result objectForKey:@"gender"];
                         if(gender.length == 0) gender = @"";
                         
                         NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
                         [parameters setValue:@"facebook" forKey:@"login_type"];
                         [parameters setValue:facebookId forKey:@"facebook_id"];
                         [parameters setValue:name forKey:@"name"];
                         [parameters setValue:email forKey:@"email"];
                         [parameters setValue:gender forKey:@"gender"];
                         [parameters setValue:appDelegate.deviceToken forKey:@"device_token"];
                         NSLog(@"parameters=%@", parameters);
                         
                         // MailChimp
                         NSString *listId = @"32964100cf";
                         NSDictionary *paramDict = @{@"id": listId, @"email": @{@"email": email}, @"merge_vars": @{@"FNAME": name, @"LName":@""}};
                         [[ChimpKit sharedKit] setShouldUseBackgroundThread:YES];
                         [[ChimpKit sharedKit] setTimeoutInterval:30.0f];
                         [[ChimpKit sharedKit] callApiMethod:@"lists/subscribe" withParams:paramDict andDelegate:self];

                         [SVProgressHUD show];
                         [[WebServiceManager sharedManager] initWithBaseURL:@"api/login" parameters:parameters success:^(id result) {
                             
                             if([[result objectForKey:@"success"] boolValue])
                             {
                                 [shareObject setObject:[result objectForKey:@"facebook_id"] forKey:@"facebook_id"];
                                 [shareObject setInteger:[[result objectForKey:@"id"] integerValue] forKey:@"user_id"];
                                 [shareObject setObject:[result objectForKey:@"name"] forKey:@"user_name"];
                                 [shareObject setObject:[result objectForKey:@"email"] forKey:@"user_email"];
                                 [shareObject setBool:[[result objectForKey:@"email_confirm_check"] boolValue] forKey:@"email_confirm_check"];
                                 [shareObject setObject:[result objectForKey:@"access_token"] forKey:@"access_token"];
                                 [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[result valueForKey:@"unit_info"]] forKey:@"unit_info"];
                                 if([[result valueForKey:@"family_list"] count] > 0)
                                 {
                                     [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableArray *)[result valueForKey:@"family_list"]] forKey:@"family_list"];
                                 }
                                 if([[result valueForKey:@"baby_list"] count] > 0)
                                 {
                                     [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableArray *)[result valueForKey:@"baby_list"]] forKey:@"baby_list"];
                                 }
                                 if([[result valueForKey:@"device_list"] count] > 0)
                                 {
                                     [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableArray *)[result valueForKey:@"device_list"]] forKey:@"device_list"];
                                 }
                                 [shareObject synchronize];
                                 
                                 if([[result valueForKey:@"family_list"] count] > 0)
                                 {
                                     if([[result valueForKey:@"baby_list"] count] > 0)
                                     {
                                         [self.navigationController setNavigationBarHidden:YES];
                                         [self.view removeFromSuperview];
                                         
                                         UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                                         UIViewController *tabBarController = [storyboard instantiateViewControllerWithIdentifier:@"TabBarController"];
                                         appDelegate.window.rootViewController = tabBarController;
                                     }
                                     else
                                     {
                                         UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                                         UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"NewBabyViewController"];
                                         [self.navigationController pushViewController:viewController animated:YES];
                                     }
                                 }
                                 else
                                 {
                                     UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                                     UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"InvitationViewController"];
                                     viewController.data = [[NSMutableDictionary alloc] initWithDictionary:@{@"splash_page_check":@(YES)}];
                                     [self.navigationController pushViewController:viewController animated:YES];
                                 }
                             }
                             else
                             {
                                 UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                                 UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"FacebookSignupViewController"];
                                 viewController.data = [[NSMutableDictionary alloc] initWithDictionary:@{@"facebook_id":facebookId, @"name":name, @"email":email, @"gender":gender, @"splash_page_check":@(YES)}];
                                 [self.navigationController pushViewController:viewController animated:YES];
                             }
                             
                         } failure:^(NSError *error) {
                             [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:LocalizedString(@"error_unknown", nil) input:nil];
                         }];
                     }];
                 }
             }
         }
     }];
}

-(void) setAttributedTextWithLink
{
    CGFloat termsFontSize = 14.0;
    if (IS_IPAD) {
        termsFontSize = 20.0;
    }

    NSDictionary *attributes = @{NSFontAttributeName:[UIFont fontWithName:@"SFUIDisplay-Medium" size:termsFontSize], NSForegroundColorAttributeName:UIColorFromRGB(0xBFB5AE)};
    self.lblTerms.attributedText = [[NSAttributedString alloc] initWithString:LocalizedString(@"txt_signup_terms", nil) attributes:attributes];
    
    [self.lblTerms setLinkAttributeDefault:@{NSFontAttributeName:[UIFont fontWithName:@"SFUIDisplay-Medium" size:termsFontSize], NSForegroundColorAttributeName:UIColorFromRGB(0x96BFCE), NSUnderlineStyleAttributeName:@(NSUnderlineStyleSingle)}];
    [self.lblTerms setLinkAttributeHighlight:@{NSFontAttributeName:[UIFont fontWithName:@"SFUIDisplay-Medium" size:termsFontSize], NSForegroundColorAttributeName:UIColorFromRGB(0x8D8082), NSUnderlineStyleAttributeName:@(NSUnderlineStyleSingle)}];
    
    [self.lblTerms setLinksForSubstrings:@[LocalizedString(@"txt_terms_of_use", nil), LocalizedString(@"txt_privacy_policy", nil)] withLinkHandler:^(FRHyperLabel *label, NSString *substring) {
        if([substring isEqualToString:LocalizedString(@"txt_terms_of_use", nil)]){
            NSLog(@"Terms");
            [self getTermsAction];
        }else{
            NSLog(@"PP");
            [self getPrivacyAction];
        }
    }];
    
}

- (void)getTermsAction
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Settings" bundle:nil];
    UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"SettingsAboutWebViewController"];
    viewController.data = [[NSMutableDictionary alloc] initWithDictionary:@{@"title":LocalizedString(@"title_settings_about", nil), @"url":@"http://www.joeyforparents.com/mobile-app-terms-of-use"}];
    [self.navigationController pushViewController:viewController animated:YES];
}

- (void)getPrivacyAction
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Settings" bundle:nil];
    UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"SettingsAboutWebViewController"];
    viewController.data = [[NSMutableDictionary alloc] initWithDictionary:@{@"title":LocalizedString(@"title_settings_about", nil), @"url":@"http://www.joeyforparents.com/privacy-policy"}];
    [self.navigationController pushViewController:viewController animated:YES];
    
}

#pragma mark - ChimpKit Delegate
-(void)ckRequestIdentifier:(NSUInteger)requestIdentifier didSucceedWithResponse:(NSURLResponse *)response andData:(NSData *)data
{
    //NSLog(@"HTTP Status Code: %d", response.statusCode);
    NSLog(@"Response String: %lu", (unsigned long)requestIdentifier);
    
    NSError *parseError = nil;
    id responseData = [NSJSONSerialization JSONObjectWithData:data options:0 error:&parseError];
    if ([responseData isKindOfClass:[NSDictionary class]]) {
        id email = [responseData objectForKey:@"email"];
        if ([email isKindOfClass:[NSString class]]) {
            //Successfully subscribed email address
            dispatch_async(dispatch_get_main_queue(), ^{
                //Update UI here
            });
        }
    }
}

-(void)ckRequestFailedWithIdentifier:(NSUInteger)requestIdentifier andError:(NSError *)anError
{
    //Handle connection error
    NSLog(@"Error, %@", anError);
    dispatch_async(dispatch_get_main_queue(), ^{
        //Update UI here
    });
}

#pragma mark - IBAction Methods
- (void)didTapOnCheckAgreement:(UITapGestureRecognizer *)sender {
    if(!termsCheck){
        termsCheck = YES;
        self.imgCheck.image = [UIImage imageNamed:@"icon_tick.png"];
    }else{
        termsCheck = NO;
        self.imgCheck.image = nil;
    }
}
@end
