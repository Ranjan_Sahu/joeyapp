//
//  SplashViewController.h
//  Joey
//
//  Created by csl on 22/12/2016.
//  Copyright © 2016 Auxilia. All rights reserved.
//

#import "BaseViewController.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FRHyperLabel/FRHyperLabel.h>

@interface SplashViewController : BaseViewController

@property (nonatomic, weak) IBOutlet UILabel *lblTitle;
@property (nonatomic, weak) IBOutlet UIButton *btnSignin;
@property (nonatomic, weak) IBOutlet UIButton *btnSignup;
@property (nonatomic, weak) IBOutlet UIButton *btnFacebook;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *fbHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *signInHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *signUpHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *logoHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *logoWidth;
@property (weak, nonatomic) IBOutlet FRHyperLabel *lblTerms;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imgCheckWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imgCheckHeight;
@property (weak, nonatomic) IBOutlet UIImageView *imgCheck;

@end
