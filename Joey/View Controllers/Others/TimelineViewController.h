//
//  TimelineViewController.h
//  Joey
//
//  Created by csl on 21/12/2016.
//  Copyright © 2016 Auxilia. All rights reserved.
//

#import "BaseViewController.h"

@interface TimelineViewController : BaseViewController <UIScrollViewDelegate, UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, weak) IBOutlet UIScrollView *babyScrollView;
@property (nonatomic, weak) IBOutlet UIScrollView *contentScrollView;
@property (nonatomic, weak) IBOutlet UITableView *dayTableView;
@property (nonatomic, weak) IBOutlet UITableView *weekTableView;
@property (nonatomic, weak) IBOutlet UITableView *monthTableView;
@property (nonatomic, weak) IBOutlet UIButton *btnDay;
@property (nonatomic, weak) IBOutlet UIButton *btnWeek;
@property (nonatomic, weak) IBOutlet UIButton *btnMonth;

@property (nonatomic, weak) IBOutlet UIImageView *imgBabyPic1;
@property (nonatomic, weak) IBOutlet UIImageView *imgBabyPic2;
@property (nonatomic, weak) IBOutlet UIImageView *imgBabyPic3;
@property (nonatomic, weak) IBOutlet UILabel *txtName1;
@property (nonatomic, weak) IBOutlet UILabel *txtInfo1;
@property (nonatomic, weak) IBOutlet UILabel *txtName2;
@property (nonatomic, weak) IBOutlet UILabel *txtInfo2;
@property (nonatomic, weak) IBOutlet UILabel *txtName3;
@property (nonatomic, weak) IBOutlet UILabel *txtInfo3;

- (void)viewWillAppear:(BOOL)animated;

@end

