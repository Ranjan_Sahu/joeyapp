//
//  BaseViewController.m
//  Joey
//
//  Created by csl on 23/1/2017.
//  Copyright © 2017 Auxilia. All rights reserved.
//

#import "BaseTableViewController.h"

@interface BaseTableViewController ()

@end

@implementation BaseTableViewController

- (void)showAlertWithTitle:(NSString *)title message:(NSString *)message input:(id)input
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *btnOk = [UIAlertAction actionWithTitle:LocalizedString(@"btn_ok", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        if(input) [input becomeFirstResponder];
        
        [alert dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [alert addAction:btnOk];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)showAlertWithTitleCallback:(NSString *)title message:(NSString *)message callback:(UIAlertControllerCallback)callback
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *btnOk = [UIAlertAction actionWithTitle:LocalizedString(@"btn_ok", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        callback(action);
        [alert dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [alert addAction:btnOk];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)showConfirmAlert:(NSString *)title message:(NSString *)message callback:(UIAlertControllerCallback)callback
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *btnOk = [UIAlertAction actionWithTitle:LocalizedString(@"btn_ok", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        callback(action);
        [alert dismissViewControllerAnimated:YES completion:nil];
    }];
    
    UIAlertAction *btnCancel = [UIAlertAction actionWithTitle:LocalizedString(@"btn_cancel", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        [alert dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [alert addAction:btnCancel];
    [alert addAction:btnOk];
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)showDeleteAlert:(NSString *)title message:(NSString *)message callback:(UIAlertControllerCallback)callback
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *btnOk = [UIAlertAction actionWithTitle:LocalizedString(@"btn_ok", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        if([alert.textFields[0].text isEqualToString:LocalizedString(@"txt_confirm_delete", nil)])
        {
            callback(action);
            [alert dismissViewControllerAnimated:YES completion:nil];
        }
        else
        {
            [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:LocalizedString(@"error_confirm_delete", nil) input:nil];
        }
    }];
    
    UIAlertAction *btnCancel = [UIAlertAction actionWithTitle:LocalizedString(@"btn_cancel", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        [alert dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.autocapitalizationType = UITextAutocapitalizationTypeWords;
    }];
    
    [alert addAction:btnCancel];
    [alert addAction:btnOk];
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (float)convertUnit:(float)value unit:(NSString *)oldUnit newUnit:(NSString *)newUnit
{
    //NSLog(@"value1=%f", value);
    if([oldUnit isEqualToString:@"lb"] && [newUnit isEqualToString:@"kg"])
    {
        value /= 2.2;
    }
    if([oldUnit isEqualToString:@"kg"] && [newUnit isEqualToString:@"lb"])
    {
        value *= 2.2;
    }
    if([oldUnit isEqualToString:@"cm"] && [newUnit isEqualToString:@"in"])
    {
        value /= 2.54;
    }
    if([oldUnit isEqualToString:@"in"] && [newUnit isEqualToString:@"cm"])
    {
        value *= 2.54;
    }
    if([oldUnit isEqualToString:@"oz"] && [newUnit isEqualToString:@"ml"])
    {
        value *= 30;
    }
    if([oldUnit isEqualToString:@"ml"] && [newUnit isEqualToString:@"oz"])
    {
        value /= 30;
    }
    if([oldUnit isEqualToString:@"oz"] && [newUnit isEqualToString:@"gm"])
    {
        value *= 28;
    }
    if([oldUnit isEqualToString:@"gm"] && [newUnit isEqualToString:@"oz"])
    {
        value /= 28;
    }
    if([oldUnit isEqualToString:@"°C"] && [newUnit isEqualToString:@"°F"])
    {
        value = (value*1.8)+32;
    }
    if([oldUnit isEqualToString:@"°F"] && [newUnit isEqualToString:@"°C"])
    {
        value = (value-32)/1.8;
    }
    if([oldUnit isEqualToString:@"ml"] && [newUnit isEqualToString:@"tsp"])
    {
        value /= 5;
    }
    if([oldUnit isEqualToString:@"tsp"] && [newUnit isEqualToString:@"ml"])
    {
        value *= 5;
    }
    //NSLog(@"value2=%f", value);
    
    if([oldUnit isEqualToString:@"tbsp"] && [newUnit isEqualToString:@"gm"])
    {
        value *= 15;
    }
    if([oldUnit isEqualToString:@"tspn"] && [newUnit isEqualToString:@"gm"])
    {
        value *= 5;
    }
    if([oldUnit isEqualToString:@"cups"] && [newUnit isEqualToString:@"gm"])
    {
        value *= 250;
    }
    
    if([oldUnit isEqualToString:@"tbsp"] && [newUnit isEqualToString:@"oz"])
    {
        value *= 15;
        value /= 30;
    }
    if([oldUnit isEqualToString:@"tspn"] && [newUnit isEqualToString:@"oz"])
    {
        value *= 5;
        value /= 30;
    }
    if([oldUnit isEqualToString:@"cups"] && [newUnit isEqualToString:@"oz"])
    {
        value *= 250;
        value /= 30;
    }

    return value;
}

- (NSString *)convertUnit:(NSString *)string unit:(NSString *)unit spacing:(BOOL)spacing
{
    if(string.length == 0) return string;
    if(![string containsString:@" "]) return string;
    
    NSMutableArray *array = (NSMutableArray *)[string componentsSeparatedByString:@" "];
    NSString *oldUnit = [array objectAtIndex:array.count-1];
    float newValue = 0;
    float oldValue = [[array objectAtIndex:array.count-2] floatValue];
    NSString *newString = [NSString stringWithFormat:@"%@", [array objectAtIndex:array.count-2]];
    NSString *newUnit = oldUnit;
    
    if([oldUnit isEqualToString:unit])
    {
        newValue = oldValue;
        if(newValue == (int)newValue)
        {
            newString = [NSString stringWithFormat:@"%d", (int)newValue];
        }
        else if([oldUnit isEqualToString:@"oz"])
        {
            newString = [NSString stringWithFormat:@"%.1f", newValue];
        }
        else if([oldUnit isEqualToString:@"gm"])
        {
            newString = [NSString stringWithFormat:@"%.1f", newValue];
        }
        else if([oldUnit isEqualToString:@"cm"] || [oldUnit isEqualToString:@"in"])
        {
            newString = [NSString stringWithFormat:@"%.1f", newValue];
        }
        else if([oldUnit isEqualToString:@"lb"])
        {
            newString = [NSString stringWithFormat:@"%.2f", newValue];
        }
        else if([oldUnit isEqualToString:@"kg"])
        {
            newString = [NSString stringWithFormat:@"%.2f", newValue];
        }
        else if([oldUnit isEqualToString:@"°F"] || [oldUnit isEqualToString:@"°C"])
        {
            newString = [NSString stringWithFormat:@"%.1f", newValue];
        }
        else if([oldUnit isEqualToString:@"ml"] || [oldUnit isEqualToString:@"tsp"])
        {
            newString = [NSString stringWithFormat:@"%.1f", newValue];
        }
        newUnit = unit;
    }
    else if([oldUnit isEqualToString:@"lb"] && [unit isEqualToString:@"kg"])
    {
        newValue = oldValue/2.2;
        newString = [NSString stringWithFormat:@"%.2f", newValue];
        newUnit = unit;
    }
    else if([oldUnit isEqualToString:@"kg"] && [unit isEqualToString:@"lb"])
    {
        newValue = oldValue*2.2;
        newString = [NSString stringWithFormat:@"%.2f", newValue];
        newUnit = unit;
    }
    else if([oldUnit isEqualToString:@"cm"] && [unit isEqualToString:@"in"])
    {
        newValue = oldValue/2.54;
        newString = [NSString stringWithFormat:@"%.1f", newValue];
        newUnit = unit;
    }
    else if([oldUnit isEqualToString:@"in"] && [unit isEqualToString:@"cm"])
    {
        newValue = oldValue*2.54;
        newString = [NSString stringWithFormat:@"%.2f", newValue];
        newUnit = unit;
    }
    else if([oldUnit isEqualToString:@"oz"] && [unit isEqualToString:@"ml"])
    {
        newValue = oldValue*30;
        newString = [NSString stringWithFormat:@"%d", (int)round(newValue)];
        newUnit = unit;
    }
    else if([oldUnit isEqualToString:@"ml"] && [unit isEqualToString:@"oz"])
    {
        newValue = oldValue/30;
        newString = [NSString stringWithFormat:@"%.1f", newValue];
        newUnit = unit;
    }
    else if([oldUnit isEqualToString:@"oz"] && [unit isEqualToString:@"gm"])
    {
        newValue = oldValue*28;
        newString = [NSString stringWithFormat:@"%d", (int)round(newValue)];
        newUnit = unit;
    }
    else if([oldUnit isEqualToString:@"gm"] && [unit isEqualToString:@"oz"])
    {
        newValue = oldValue/28;
        newString = [NSString stringWithFormat:@"%.1f", newValue];
        newUnit = unit;
    }
    else if([oldUnit isEqualToString:@"°C"] && [unit isEqualToString:@"°F"])
    {
        newValue = (oldValue*1.8)+32;
        newString = [NSString stringWithFormat:@"%.1f", newValue];
        newUnit = unit;
    }
    else if([oldUnit isEqualToString:@"°F"] && [unit isEqualToString:@"°C"])
    {
        newValue = (oldValue-32)/1.8;
        newString = [NSString stringWithFormat:@"%.1f", newValue];
        newUnit = unit;
    }
    else if([oldUnit isEqualToString:@"ml"] && [unit isEqualToString:@"tsp"])
    {
        newValue = oldValue/5;
        newString = [NSString stringWithFormat:@"%.2f", newValue];
        
        NSString *digit = [newString componentsSeparatedByString:@"."][0];
        NSString *fraction = [newString componentsSeparatedByString:@"."][1];
        if([digit intValue] == 0) digit = @"";
        
        if([fraction isEqualToString:@"00"])
        {
            newString = digit;
        }
        else if([fraction isEqualToString:@"50"])
        {
            newString = [NSString stringWithFormat:@"%@½", digit];
        }
        else if([fraction isEqualToString:@"33"])
        {
            newString = [NSString stringWithFormat:@"%@⅓", digit];
        }
        else if([fraction isEqualToString:@"67"])
        {
            newString = [NSString stringWithFormat:@"%@⅔", digit];
        }
        else if([fraction isEqualToString:@"25"])
        {
            newString = [NSString stringWithFormat:@"%@¼", digit];
        }
        else if([fraction isEqualToString:@"75"])
        {
            newString = [NSString stringWithFormat:@"%@¾", digit];
        }
        newUnit = unit;
    }
    else if([oldUnit isEqualToString:@"tsp"] && [unit isEqualToString:@"ml"])
    {
        newValue = oldValue*5;
        newString = [NSString stringWithFormat:@"%.1f", oldValue];
        newUnit = unit;
    }
    if(newValue == (int)newValue)
    {
        newString = [NSString stringWithFormat:@"%d", (int)newValue];
    }
    
    [array replaceObjectAtIndex:array.count-2 withObject:newString];
    [array replaceObjectAtIndex:array.count-1 withObject:newUnit];
    NSString *tempString = [array componentsJoinedByString:@" "];
    
    if(spacing)
    {
        return tempString;
    }
    else
    {
        return [tempString stringByReplacingCharactersInRange:NSMakeRange(tempString.length-newUnit.length-1, newUnit.length+1) withString:newUnit];
    }
}

- (NSString *)convertSeconds:(int)diff
{
    int seconds = diff % 60;
    int minutes = (diff / 60) % 60;
    int hours = diff / 3600;
    
    return [NSString stringWithFormat:@"%02d:%02d:%02d", hours, minutes, seconds];
}

- (NSString *)getMinuteAndSecound:(int)diff
{
    int seconds = diff % 60;
    int minutes = (diff / 60) % 60;
    //int hours = diff / 3600;
    
    return [NSString stringWithFormat:@"%02d:%02d", minutes, seconds];
}

- (NSString *)convertDuration:(int)diff fullFormatCheck:(BOOL)fullFormatCheck
{
    NSString *duration = @"";
    
    int min = diff % 60;
    int hour = floor(diff / 60);
    if(hour > 24) hour = floor(diff % (60*24) / 60);
    int day = floor(diff / (60*24));
    
    if(day > 0) duration = [NSString stringWithFormat:@"%@%d%@%@ ", duration, day, fullFormatCheck?@" ":@"", (day > 1)?LocalizedString(fullFormatCheck?@"days":@"d", nil):LocalizedString(fullFormatCheck?@"day":@"d", nil)];
    if(hour > 0) duration = [NSString stringWithFormat:@"%@%d%@%@ ", duration, hour, fullFormatCheck?@" ":@"", (hour > 1)?LocalizedString(fullFormatCheck?@"hours":@"h", nil):LocalizedString(fullFormatCheck?@"hour":@"h", nil)];
    if(day == 0 && min > 0) duration = [NSString stringWithFormat:@"%@%d%@%@ ", duration, min, fullFormatCheck?@" ":@"", (min > 1)?LocalizedString(fullFormatCheck?@"minutes":@"m", nil):LocalizedString(fullFormatCheck?@"minute":@"m", nil)];
    
    if(diff == 0) duration = [NSString stringWithFormat:@"0 %@ ", LocalizedString(fullFormatCheck?@"minutes":@"m", nil)];
    
    return duration;
}

- (NSArray *)convertDuration:(int)diff
{
    int min = diff % 60;
    int hour = floor(diff / 60);
    if(hour > 24) hour = floor(diff % (60*24) / 60);
    
    return [NSArray arrayWithObjects:@(hour), @(min), nil];
}

- (NSDate *)getFirstDayOfTheWeekFromDate:(NSDate *)givenDate
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    // Edge case where beginning of week starts in the prior month
    NSDateComponents *edgeCase = [[NSDateComponents alloc] init];
    [edgeCase setMonth:2];
    [edgeCase setDay:1];
    [edgeCase setYear:2013];
    NSDate *edgeCaseDate = [calendar dateFromComponents:edgeCase];
    
    NSDateComponents *components = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitWeekOfYear|NSCalendarUnitWeekday fromDate:edgeCaseDate];
    [components setWeekday:2]; // 1 == Sunday, 7 == Saturday
    [components setWeekOfYear:[components weekOfYear]];
    
    // Find Sunday for the given date
    components = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitWeekOfYear|NSCalendarUnitWeekday fromDate:givenDate];
    [components setWeekday:2]; // 1 == Sunday, 7 == Saturday
    [components setWeekOfYear:[components weekOfYear]];
    
    return [calendar dateFromComponents:components];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


@end
