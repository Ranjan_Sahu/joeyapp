//
//  InvitationViewController.h
//  Joey
//
//  Created by csl on 21/3/2017.
//  Copyright © 2017 Auxilia. All rights reserved.
//

#import "BaseViewController.h"

@interface InvitationViewController : BaseViewController

@property (nonatomic, weak) IBOutlet UILabel *lblTitle;
@property (nonatomic, weak) IBOutlet UILabel *lblJoin;
@property (nonatomic, weak) IBOutlet UILabel *lblCreate;
@property (nonatomic, weak) IBOutlet UILabel *lblOr;
@property (nonatomic, weak) IBOutlet UIView *viewJoin;
@property (nonatomic, weak) IBOutlet UIView *viewCreate;
@property (nonatomic, weak) IBOutlet UITextField *txtInvitationCode;
@property (nonatomic, weak) IBOutlet UIView *viewInvitationCode;
@property (nonatomic, weak) IBOutlet UIButton *btnJoin;
@property (nonatomic, weak) IBOutlet UIButton *btnCreate;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *logoWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *logInHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *btnJoinHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *invitationCodeHeight;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *iconJoinFamilyWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *iconJoinFamilyHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *iconNewFamilyWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *iconNewFamilyHeight;

@end
