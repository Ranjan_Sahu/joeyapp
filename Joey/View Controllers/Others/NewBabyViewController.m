//
//  NewBabyViewController.m
//  Joey
//
//  Created by csl on 3/2/2017.
//  Copyright © 2017 Auxilia. All rights reserved.
//

#import "AppDelegate.h"
#import "NewBabyViewController.h"

@interface NewBabyViewController ()
{
    NSUserDefaults *shareObject;
    AppDelegate *appDelegate;
    NSDateFormatter *formatter;
    NSMutableDictionary *unitInfo;
    NSMutableArray *no100List;
    NSMutableArray *digitList;
    NSMutableArray *digit2List;
    NSMutableArray *familyList;
    NSMutableArray *familyIdList;
    NSMutableArray *relationshipList;
    NSMutableArray *relationshipValueList;
    NSMutableArray *bloodType1List;
    NSMutableArray *bloodType2List;
    NSDate *selectedBirthday;
    NSDate *selectedBirthDate;
    NSDate *selectedBirthTime;
    NSArray *selectedWeightIndexes;
    NSArray *selectedHeightIndexes;
    NSArray *selectedHeadSizeIndexes;
    NSArray *selectedBloodTypeIndexes;
    NSString *selectedWeightUnit;
    NSString *selectedHeightUnit;
    NSString *selectedHeadSizeUnit;
    NSString *selectedGender;
    NSString *selectedRelationship;
    UIImage *imgUploadPhoto;
    UIImageView *imageView;
    UIView *baseView;
    
    int selectedFamilyId;
    int selectedFamilyIndex;
    int selectedRelationshipIndex;
    float selectedWeight;
    float selectedHeight;
    float selectedHeadSize;
    BOOL removePhotoCheck;
    BOOL saveCheck;
}
@end

@implementation NewBabyViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    shareObject = [NSUserDefaults standardUserDefaults];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    formatter = [[NSDateFormatter alloc] init];
    [formatter setTimeZone:[NSTimeZone localTimeZone]];
    [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
    
    unitInfo = (NSMutableDictionary *)[NSKeyedUnarchiver unarchiveObjectWithData:[shareObject objectForKey:@"unit_info"]];
    no100List = [[NSMutableArray alloc] init];
    for(int i=0; i<100; i++) [no100List addObject:[NSString stringWithFormat:@"%d", i]];
    digitList = [[NSMutableArray alloc] init];
    for(int i=0; i<10; i++) [digitList addObject:[NSString stringWithFormat:@"%d", i]];
    digit2List = [[NSMutableArray alloc] init];
    for(int i=0; i<100; i++) [digit2List addObject:[NSString stringWithFormat:@"%02d", i]];
    
    formatter.dateFormat = @"yyyy-MM-dd hh:mm a";
    NSString *birthdayStr = [formatter stringFromDate:[NSDate date]];
    selectedBirthday = [formatter dateFromString:birthdayStr];
    self.txtBirthDate.text = [NSString stringWithFormat:@"%@", [formatter stringFromDate:selectedBirthday]];
    
    //formatter.dateFormat = @"yyyy-MM-dd";
    //NSString *dateStr = [formatter stringFromDate:[NSDate date]];
    selectedBirthDate = [NSDate date];
    //formatter.dateFormat = @"hh:mm a";//
    //NSString *timeStr = [formatter stringFromDate:[NSDate date]];
    selectedBirthTime = [NSDate date];

    if([[shareObject objectForKey:@"language"] hasPrefix:@"zh"]){
        [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"zh_HK"]];
        [formatter setDateStyle:NSDateFormatterMediumStyle];
        [formatter setTimeStyle:NSDateFormatterShortStyle];
    }else{
        [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
        formatter.dateFormat = @"EE d MMM, yyyy hh:mm a";
    }
    
    selectedGender = @"girl";
    self.btnGender2.alpha = 0.25f;
    
    selectedWeight = 0.00;
    selectedHeight = 0.0;
    selectedHeadSize = 0.0;
    selectedWeightIndexes = [[NSString stringWithFormat:@"%.2f", selectedWeight] componentsSeparatedByString:@"."];
    selectedWeightIndexes = [selectedWeightIndexes arrayByAddingObject:@"kg"];
    selectedHeightIndexes = [[NSString stringWithFormat:@"%.1f", selectedHeight] componentsSeparatedByString:@"."];
    selectedHeightIndexes = [selectedHeightIndexes arrayByAddingObject:@"cm"];
    selectedHeadSizeIndexes = [[NSString stringWithFormat:@"%.1f", selectedHeadSize] componentsSeparatedByString:@"."];
    selectedHeadSizeIndexes = [selectedHeadSizeIndexes arrayByAddingObject:@"cm"];
    
    familyList = [[NSMutableArray alloc] init];
    familyIdList = [[NSMutableArray alloc] init];
    if([[self.data objectForKey:@"settings_check"] boolValue])
    {
        self.imgChevonFamilyName.hidden = NO;
        self.btnFamilyName.hidden = NO;
        NSMutableArray *familyTempList = (NSMutableArray *)[NSKeyedUnarchiver unarchiveObjectWithData:[shareObject objectForKey:@"family_list"]];
        
        for(int i=0; i<familyTempList.count; i++)
        {
            if([[self.data objectForKey:@"family_id"] intValue] == [[[familyTempList objectAtIndex:i] objectForKey:@"id"] intValue])
            {
                selectedFamilyIndex = i;
                selectedFamilyId = [[self.data objectForKey:@"family_id"] intValue];
                self.txtFamilyName.text = [[familyTempList objectAtIndex:i] objectForKey:@"name"];
            }
            [familyList addObject:[[familyTempList objectAtIndex:i] objectForKey:@"name"]];
            [familyIdList addObject:[[familyTempList objectAtIndex:i] objectForKey:@"id"]];
        }
    }
    NSLog(@"familyIdList=%@", familyIdList);
    NSLog(@"familyList=%@", familyList);
    
    relationshipValueList = [[NSMutableArray alloc] initWithObjects: @"mommy", @"daddy", @"grandpa", @"grandma", @"helper", @"relative", @"caregiver", nil];
    relationshipList = [[NSMutableArray alloc] init];
    for(int i=0; i<relationshipValueList.count; i++) [relationshipList addObject:LocalizedString([relationshipValueList objectAtIndex:i], nil)];
    
    bloodType1List = [[NSMutableArray alloc] initWithObjects:@"A", @"B", @"AB", @"O", nil];
    bloodType2List = [[NSMutableArray alloc] initWithObjects:@"+", @"-", nil];
    selectedBloodTypeIndexes = [NSArray arrayWithObjects:@(0), @(0), nil];
    
    [self.navigationController setNavigationBarHidden:NO];
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:UIColorFromRGB(0x8D8082), NSForegroundColorAttributeName, [UIFont fontWithName:@"SFUIDisplay-Medium" size:18.0f], NSFontAttributeName, nil]];
    [self.navigationController.navigationBar setBarTintColor:UIColorFromRGB(0xEEE8E4)];
    [self.navigationController.navigationBar setTranslucent:NO];
    [self.navigationItem setTitle:LocalizedString(@"title_new_baby", nil)];
    [self customBackButton];
    
    //self.cropView.clipsToBounds = YES;
    //self.cropView.layer.cornerRadius = self.cropView.frame.size.width/2;
    
    self.lblFamilyName.text = LocalizedString(@"txt_name", nil);
    self.lblRelationship.text = LocalizedString(@"txt_relation_to_baby", nil);
    self.lblName.text = LocalizedString(@"txt_name", nil);
    self.lblBirthDate.text = LocalizedString(@"txt_datetime", nil);
    self.lblGender.text = LocalizedString(@"txt_gender", nil);
    self.lblWeight.text = LocalizedString(@"txt_birth_weight", nil);
    self.lblHeight.text = LocalizedString(@"txt_birth_length", nil);
    self.lblHeadSize.text = LocalizedString(@"txt_head_size", nil);
    self.lblBloodType.text = LocalizedString(@"txt_blood_type", nil);
    self.btnSave.title = LocalizedString(@"btn_save", nil);
    
    if (IS_IPAD) {
        self.lblFamilyName.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.lblRelationship.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.lblName.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.lblBirthDate.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.lblGender.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.lblWeight.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.lblHeight.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.lblHeadSize.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.lblBloodType.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        
        self.txtFamilyName.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.txtRelationship.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.txtName.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.txtBirthDate.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.txtWeight.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.txtHeight.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.txtHeadSize.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.txtBloodType.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
    }
    if(![[self.data objectForKey:@"settings_check"] boolValue])
    {
        [self.txtFamilyName becomeFirstResponder];
    }
    
//    self.scrollView.minimumZoomScale = 0.2f;
//    self.scrollView.maximumZoomScale = 2.0f;
//    self.scrollView.zoomScale = 1.0f;
    
    self.scrollView.layer.anchorPoint = CGPointMake(0.5, 0.5);
    self.scrollView.alwaysBounceVertical = YES;
    self.scrollView.alwaysBounceHorizontal = YES;
    //self.scrollView.minimumZoomScale = [self zoomScaleToBound];
    self.scrollView.maximumZoomScale = 2.0f;
    self.scrollView.showsVerticalScrollIndicator = NO;
    self.scrollView.showsHorizontalScrollIndicator = NO;
    self.scrollView.clipsToBounds = NO;
    self.automaticallyAdjustsScrollViewInsets = NO;

}

- (void)customBackButton
{
    if([self.navigationController.viewControllers count] > 1)
    {
        UIImage *imgBack = [UIImage imageNamed:@"nav_btn_back"];
        UIButton *btnBack = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnBack setImage:imgBack forState:UIControlStateNormal];
        btnBack.frame = CGRectMake(0, 0, imgBack.size.width, imgBack.size.height);
        [btnBack addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
        [btnBack setImageEdgeInsets:UIEdgeInsetsMake(0, -16, 0, 16)];
        UIBarButtonItem *backBarItem = [[UIBarButtonItem alloc] initWithCustomView:btnBack];
        self.navigationItem.leftBarButtonItem = backBarItem;
    }
}

- (void)backAction
{
    [self.data removeAllObjects];
    
    UIViewController *viewController = [self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count-2];
    viewController.data = [[NSMutableDictionary alloc] initWithDictionary:@{@"save_check":@(saveCheck)}];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)showCameraAction
{
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    [self presentViewController:imagePicker animated:NO completion:nil];
}

- (void)showPhotoAlbumAction
{
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:imagePicker animated:NO completion:nil];
}

- (BOOL)validateInputs
{
    if(self.txtRelationship.text.length == 0)
    {
        [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:LocalizedString(@"error_relationship", nil) input:nil];
        return NO;
    }
    
    ALPValidator *validator = nil;
    
    validator = [ALPValidator validatorWithType:ALPValidatorTypeString];
    [validator addValidationToEnsurePresenceWithInvalidMessage:LocalizedString(@"error_baby_name", nil)];
    [validator validate:self.txtName.text];
    if(![validator isValid])
    {
        [self.txtName becomeFirstResponder];
        [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:[[validator errorMessages] objectAtIndex:0] input:self.txtName];
        return NO;
    }
    
    validator = [ALPValidator validatorWithType:ALPValidatorTypeString];
    [validator addValidationToEnsurePresenceWithInvalidMessage:LocalizedString(@"error_baby_birth_date", nil)];
    [validator validate:self.txtBirthDate.text];
    if(![validator isValid])
    {
        [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:[[validator errorMessages] objectAtIndex:0] input:nil];
        return NO;
    }
    
    validator = [ALPValidator validatorWithType:ALPValidatorTypeString];
    [validator addValidationToEnsurePresenceWithInvalidMessage:LocalizedString(@"error_baby_weight", nil)];
    [validator validate:self.txtWeight.text];
    if(![validator isValid])
    {
        [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:[[validator errorMessages] objectAtIndex:0] input:nil];
        return NO;
    }
    
    validator = [ALPValidator validatorWithType:ALPValidatorTypeString];
    [validator addValidationToEnsurePresenceWithInvalidMessage:LocalizedString(@"error_baby_height", nil)];
    [validator validate:self.txtHeight.text];
    if(![validator isValid])
    {
        [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:[[validator errorMessages] objectAtIndex:0] input:nil];
        return NO;
    }
    
    validator = [ALPValidator validatorWithType:ALPValidatorTypeString];
    [validator addValidationToEnsurePresenceWithInvalidMessage:LocalizedString(@"error_baby_head_size", nil)];
    [validator validate:self.txtHeadSize.text];
    if(![validator isValid])
    {
        [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:[[validator errorMessages] objectAtIndex:0] input:nil];
        return NO;
    }
    
    validator = [ALPValidator validatorWithType:ALPValidatorTypeString];
    [validator addValidationToEnsurePresenceWithInvalidMessage:LocalizedString(@"error_baby_blood_type", nil)];
    [validator validate:self.txtBloodType.text];
    if(![validator isValid])
    {
        [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:[[validator errorMessages] objectAtIndex:0] input:nil];
        return NO;
    }
    
    return YES;
}

- (IBAction)getFamilyName:(id)sender
{
    [ActionSheetStringPicker showPickerWithTitle:@"" rows:familyList initialSelection:selectedRelationshipIndex doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        
        selectedFamilyIndex = (int)selectedIndex;
        selectedFamilyId = [[familyIdList objectAtIndex:1] intValue];
        self.txtFamilyName.text = selectedValue;
        [self.tableView reloadData];
        
    } cancelBlock:^(ActionSheetStringPicker *picker) {
        
    } origin:self.view];
}

- (IBAction)getPhotoAction:(id)sender
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:LocalizedString(@"title_selectImage", nil) message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *btnTakePhoto = [UIAlertAction actionWithTitle:LocalizedString(@"title_camera", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        
        AVAuthorizationStatus status = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
        if(status == AVAuthorizationStatusDenied || status == AVAuthorizationStatusRestricted)
        {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:LocalizedString(@"error_camera", nil) message:@"" preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *btnOk = [UIAlertAction actionWithTitle:LocalizedString(@"btn_ok", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                
                [alert dismissViewControllerAnimated:YES completion:nil];
            }];
            
            [alert addAction:btnOk];
            
            [self presentViewController:alert animated:YES completion:nil];
            
        }
        else
        {
            [self showCameraAction];
        }
        
        [alertController dismissViewControllerAnimated:YES completion:nil];
    }];
    
    UIAlertAction *btnPhotoAlbum = [UIAlertAction actionWithTitle:LocalizedString(@"title_camera_roll", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        
        PHAuthorizationStatus status = [PHPhotoLibrary authorizationStatus];
        if(status == PHAuthorizationStatusDenied || status == PHAuthorizationStatusRestricted)
        {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:LocalizedString(@"alert_photo", nil) message:@"" preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *btnOk = [UIAlertAction actionWithTitle:@"btn_ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                
                [alert dismissViewControllerAnimated:YES completion:nil];
            }];
            
            [alert addAction:btnOk];
            
            [self presentViewController:alert animated:YES completion:nil];
            
        }
        else
        {
            [self showPhotoAlbumAction];
        }
        
        
        [alertController dismissViewControllerAnimated:YES completion:nil];
    }];
    
    UIAlertAction *btnCancel = [UIAlertAction actionWithTitle:LocalizedString(@"title_cancel", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction * action) {
        
        [alertController dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [alertController addAction:btnTakePhoto];
    [alertController addAction:btnPhotoAlbum];
    [alertController addAction:btnCancel];
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        UIButton *button = (UIButton *)sender;
        alertController.popoverPresentationController.sourceView = button;
        alertController.popoverPresentationController.sourceRect = CGRectMake(button.frame.size.width/2,button.frame.size.height/2,1.0,1.0);
    }

    [self presentViewController:alertController animated:YES completion:nil];
}

- (IBAction)removePhotoAction:(id)sender
{
    [self showConfirmAlert:LocalizedString(@"alert_are_you_sure", nil) message:@"" callback:^(UIAlertAction *action) {
        
        [imageView removeFromSuperview];
        [baseView removeFromSuperview];
        self.btnAddImage.hidden = NO;
        self.btnRemoveImage.hidden = YES;
        self.scrollView.scrollEnabled = YES;
    }];
}

- (IBAction)getGenderAction:(id)sender
{
    UIButton *btn = (UIButton *)sender;
    if(btn == self.btnGender1)
    {
        self.btnGender1.alpha = 1.0f;
        self.btnGender2.alpha = 0.25f;
        selectedGender = @"girl";
    }
    else
    {
        self.btnGender1.alpha = 0.25f;
        self.btnGender2.alpha = 1.0f;
        selectedGender = @"boy";
    }
}

- (IBAction)nextAction:(id)sender
{
    if([self validateInputs])
    {
        [SVProgressHUD show];
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setValue:@([shareObject integerForKey:@"user_id"]) forKey:@"user_id"];
        
        if([self.data objectForKey:@"settings_check"]){
            [parameters setValue:@(selectedFamilyId) forKey:@"family_id"];
        }
        else{
            [parameters setValue:self.txtFamilyName.text forKey:@"family_name"];
        }
        
        formatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
        [parameters setValue:selectedRelationship forKey:@"relationship"];
        [parameters setValue:self.txtName.text forKey:@"name"];
        [parameters setValue:[formatter stringFromDate:selectedBirthday] forKey:@"birthday"];
        
        NSMutableArray *babyList = (NSMutableArray *)[NSKeyedUnarchiver unarchiveObjectWithData:[shareObject objectForKey:@"baby_list"]];
        if(babyList.count == 0)
        {
            [parameters setValue:selectedWeightUnit forKey:@"weight_unit"];
            [parameters setValue:selectedHeightUnit forKey:@"height_unit"];
            [parameters setValue:selectedHeadSizeUnit forKey:@"head_size_unit"];
        }
        
        // To check if birth date is future date
        formatter.dateFormat = @"yyyy-MM-dd HH:mm:ss ZZZ";
        NSDate *time = [formatter dateFromString:[formatter stringFromDate:selectedBirthday]];
        
        if([time compare:[NSDate date]] == NSOrderedDescending)
        {
            [parameters setValue:@"" forKey:@"gender"];
            [parameters setValue:@"0" forKey:@"birth_weight"];
            [parameters setValue:@"0" forKey:@"birth_height"];
            [parameters setValue:@"0" forKey:@"birth_head_size"];
            [parameters setValue:@"0" forKey:@"blood_type"];
        }
        else
        {
            [parameters setValue:selectedGender forKey:@"gender"];
            [parameters setValue:[self convertUnit:self.txtWeight.text unit:@"kg" spacing:YES] forKey:@"weight"];
            [parameters setValue:[self convertUnit:self.txtHeight.text unit:@"cm" spacing:YES] forKey:@"height"];
            [parameters setValue:[self convertUnit:self.txtHeadSize.text unit:@"cm" spacing:YES] forKey:@"head_size"];
            [parameters setValue:self.txtBloodType.text forKey:@"blood_type"];
        }
        [parameters setValue:[self.data objectForKey:@"photo_url"] forKey:@"photo_url"];
        [parameters setValue:@(removePhotoCheck) forKey:@"remove_check"];

        if(imgUploadPhoto)
        {
            self.maskView.hidden = YES;
            self.btnRemoveImage.hidden = YES;
            
            CGSize newSize = CGSizeMake(self.contentView.frame.size.width, self.contentView.frame.size.height);
            UIGraphicsBeginImageContextWithOptions(newSize, NO, appDelegate.window.screen.scale);
            [self.contentView.layer renderInContext:UIGraphicsGetCurrentContext()];
            UIImage *screenshot = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            
            self.maskView.hidden = NO;
            self.btnRemoveImage.hidden = NO;
            
            [[WebServiceManager sharedManager] initWithBaseURL:@"api/addBaby" parameters:parameters formData:^(id<AFMultipartFormData> formData) {
                [formData appendPartWithFileData:UIImageJPEGRepresentation(screenshot, 1.0f) name:@"image" fileName:@"image.jpg" mimeType:@"image/jpeg"];
                [formData appendPartWithFileData:UIImageJPEGRepresentation(imgUploadPhoto, 1.0f) name:@"image_original" fileName:@"image_original.jpg" mimeType:@"image/jpeg"];
            } success:^(id result) {
                
                if([[result objectForKey:@"success"] boolValue])
                {
                    [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableArray *)[result valueForKey:@"family_list"]] forKey:@"family_list"];
                    [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableArray *)[result valueForKey:@"baby_list"]] forKey:@"baby_list"];
                    [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[result valueForKey:@"unit_info"]] forKey:@"unit_info"];
                    [shareObject synchronize];
                    
                    [self.navigationController setNavigationBarHidden:YES];
                    [self.view removeFromSuperview];
                    
                    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                    UIViewController *tabBarController = [storyboard instantiateViewControllerWithIdentifier:@"TabBarController"];
                    appDelegate.window.rootViewController = tabBarController;
                }
                else
                {
                    [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:[result objectForKey:@"message"] input:nil];
                }
                
            } failure:^(NSError *error) {
                [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:LocalizedString(@"error_unknown", nil) input:nil];
            }];
        }
        else
        {
            [[WebServiceManager sharedManager] initWithBaseURL:@"api/addBaby" parameters:parameters success:^(id result) {
                
                if([[result objectForKey:@"success"] boolValue])
                {
                    [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableArray *)[result valueForKey:@"family_list"]] forKey:@"family_list"];
                    [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableArray *)[result valueForKey:@"baby_list"]] forKey:@"baby_list"];
                    [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[result valueForKey:@"unit_info"]] forKey:@"unit_info"];
                    [shareObject synchronize];
                    
                    [self.navigationController setNavigationBarHidden:YES];
                    [self.view removeFromSuperview];
                    
                    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                    UIViewController *tabBarController = [storyboard instantiateViewControllerWithIdentifier:@"TabBarController"];
                    appDelegate.window.rootViewController = tabBarController;
                }
                else
                {
                    [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:[result objectForKey:@"message"] input:nil];
                }
                
            } failure:^(NSError *error) {
                [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:LocalizedString(@"error_unknown", nil) input:nil];
            }];
        }
    }
}


#pragma mark - Custom Methods
- (void)selectTime:(NSString *)birthDate
{
    [ActionSheetDatePicker showPickerWithTitle:@"" datePickerMode:UIDatePickerModeTime selectedDate:selectedBirthTime minimumDate:nil maximumDate:nil doneBlock:^(ActionSheetDatePicker *picker, id selectedDate, id origin) {
        
        selectedBirthTime = selectedDate;
        NSLog(@"selectedMilestoneTime=%@", selectedBirthTime);
        
        formatter.dateFormat = @"hh:mm a";
        NSString *birthTime = [formatter stringFromDate:selectedDate];
        formatter.dateFormat = @"yyyy-MM-dd hh:mm a";
        selectedBirthday = [formatter dateFromString:[NSString stringWithFormat:@"%@ %@", birthDate, birthTime]];
        NSLog(@"selectedMilestoneDateTime=%@", selectedBirthday);
        
        self.txtBirthDate.text = [NSString stringWithFormat:@"%@", [formatter stringFromDate:selectedBirthday]];
        [self.tableView reloadData];
        [self enableDisableInputRows];
        
    } cancelBlock:^(ActionSheetDatePicker *picker) {
        
    } origin:self.view];
}

-(void)enableDisableInputRows
{
    formatter.dateFormat = @"yyyy-MM-dd HH:mm:ss ZZZ";
    NSDate *time = [formatter dateFromString:[formatter stringFromDate:selectedBirthday]];
    
    if([time compare:[NSDate date]] == NSOrderedDescending)
    {
        self.btnGender1.hidden = YES;
        self.btnGender2.hidden = YES;
        self.txtWeight.hidden = YES;
        self.txtHeight.hidden = YES;
        self.txtHeadSize.hidden = YES;
        self.txtBloodType.hidden = YES;
    }
    else {
        self.btnGender1.hidden = NO;
        self.btnGender2.hidden = NO;
        self.txtWeight.hidden = NO;
        self.txtHeight.hidden = NO;
        self.txtHeadSize.hidden = NO;
        self.txtBloodType.hidden = NO;
    }
}

#pragma mark - Table view data source
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if(section == 0) return LocalizedString(@"txt_family", nil);
    else return LocalizedString(@"txt_baby", nil);
    return nil;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UILabel *txtHeader;
    UIView *headerView;
    
    if (IS_IPAD) {
        txtHeader = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, tableView.bounds.size.width-20, 30)];
        txtHeader.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f];
        headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 40)];
    }
    else{
        txtHeader = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, tableView.bounds.size.width-20, 20)];
        txtHeader.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:15.0f];
        headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 30)];
    }
    txtHeader.textColor = UIColorFromRGB(0xFFFFFF);
    txtHeader.text = [self tableView:tableView titleForHeaderInSection:section];

    headerView.backgroundColor = UIColorFromRGB(0xBEB5AF);
    [headerView addSubview:txtHeader];
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(IS_IPAD) return 40;
    return 30;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([[self.data objectForKey:@"settings_check"] boolValue])
    {
        if(indexPath.section == 0 && indexPath.row == 0) return 0;
    }
    //if(indexPath.section == 1 && indexPath.row == 0) return 300;
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([[self.data objectForKey:@"settings_check"] boolValue])
    {
        if(indexPath.section == 0 && indexPath.row == 0) return 0;
    }
    //if(indexPath.section == 1 && indexPath.row == 0) return 300;    
    return UITableViewAutomaticDimension;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 0)
    {
        if(indexPath.row == 1)
        {
            [ActionSheetStringPicker showPickerWithTitle:@"" rows:relationshipList initialSelection:selectedRelationshipIndex doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
                
                selectedRelationshipIndex = (int)selectedIndex;
                selectedRelationship = [relationshipValueList objectAtIndex:selectedRelationshipIndex];
                self.txtRelationship.text = selectedValue;
                
                [self.tableView reloadData];
                
            } cancelBlock:^(ActionSheetStringPicker *picker) {
                
            } origin:self.view];
        }
    }
    else
    {
        formatter.dateFormat = @"yyyy-MM-dd HH:mm:ss ZZZ";
        NSDate *time = [formatter dateFromString:[formatter stringFromDate:selectedBirthday]];
        if((indexPath.row != 2)&&([time compare:[NSDate date]] == NSOrderedDescending)){
            return;
        }
        
        if(indexPath.row == 2)
        {
            NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
            [dateComponents setWeekdayOrdinal:40];
            NSCalendar *calendar = [NSCalendar currentCalendar];
            NSDate *maximumDate = [calendar dateByAddingComponents:dateComponents toDate:[NSDate date] options:0];
            
            [ActionSheetDatePicker showPickerWithTitle:@"" datePickerMode:UIDatePickerModeDate selectedDate:selectedBirthDate minimumDate:nil maximumDate:maximumDate doneBlock:^(ActionSheetDatePicker *picker, id selectedDate, id origin) {
                
                formatter.dateFormat = @"yyyy-MM-dd";
                NSString *birthDate = [formatter stringFromDate:selectedDate];
                selectedBirthDate = [formatter dateFromString:birthDate];
                
                formatter.dateFormat = @"EE d MMM, yyyy";
                self.txtBirthDate.text = [formatter stringFromDate:selectedDate];
                
                formatter.dateFormat = @"yyyy-MM-dd hh:mm a";
                selectedBirthday = [formatter dateFromString:[NSString stringWithFormat:@"%@ ", birthDate]];
                
                [self performSelector:@selector(selectTime:) withObject:birthDate afterDelay:0.3];
                
            } cancelBlock:^(ActionSheetDatePicker *picker) {
                
            } origin:self.view];
        }
        if(indexPath.row == 4)
        {
            NSArray *stringList = [NSArray arrayWithObjects:no100List, digit2List, [NSArray arrayWithObjects:@"kg", @"lb", nil], nil];
            NSArray *initialList = [NSArray arrayWithObjects:@([selectedWeightIndexes[0] intValue]), @([selectedWeightIndexes[1] intValue]), @([selectedWeightIndexes[2] intValue]), nil];
            
            ActionSheetMultipleStringPicker *picker = [ActionSheetMultipleStringPicker showPickerWithTitle:@"" rows:stringList initialSelection:initialList doneBlock:^(ActionSheetMultipleStringPicker *picker, NSArray *selectedIndexes, id selectedValues) {
                
                selectedWeightIndexes = selectedIndexes;
                selectedWeight = [[selectedValues componentsJoinedByString:@"."] floatValue];
                NSLog(@"selectedWeight=%f", selectedWeight);
                //self.txtWeight.text = [self convertUnit:[NSString stringWithFormat:@"%.2f %@", selectedWeight, [unitInfo objectForKey:@"weight"]] unit:[unitInfo objectForKey:@"weight"]];
                self.txtWeight.text = [NSString stringWithFormat:@"%.2f %@", selectedWeight, selectedValues[2]];
                selectedWeightUnit = selectedValues[2];
                
                [self.tableView reloadData];
                
            } cancelBlock:^(ActionSheetMultipleStringPicker *picker) {
                
            } origin:self.view];
            
            UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(20, 0, (picker.viewSize.width-40)/3, 36)];
            label1.text = @".   ";
            label1.textAlignment = NSTextAlignmentRight;
            label1.font = [UIFont systemFontOfSize:20.0f];
            label1.backgroundColor = [[UIColor redColor] colorWithAlphaComponent:0.f];
            [[picker.pickerView.subviews objectAtIndex:1] addSubview:label1];
        }
        if(indexPath.row == 5)
        {
            NSArray *stringList = [NSArray arrayWithObjects:no100List, digitList, [NSArray arrayWithObjects:@"cm", @"in", nil], nil];
            NSArray *initialList = [NSArray arrayWithObjects:@([selectedHeightIndexes[0] intValue]), @([selectedHeightIndexes[1] intValue]), @([selectedHeightIndexes[2] intValue]), nil];
            
            ActionSheetMultipleStringPicker *picker = [ActionSheetMultipleStringPicker showPickerWithTitle:@"" rows:stringList initialSelection:initialList doneBlock:^(ActionSheetMultipleStringPicker *picker, NSArray *selectedIndexes, id selectedValues) {
                
                selectedHeightIndexes = selectedIndexes;
                selectedHeight = [[selectedValues componentsJoinedByString:@"."] floatValue];
                //self.txtHeight.text = [self convertUnit:[NSString stringWithFormat:@"%f %@", selectedHeight, [unitInfo objectForKey:@"height"]] unit:[unitInfo objectForKey:@"height"]];
                self.txtHeight.text = [NSString stringWithFormat:@"%.1f %@", selectedHeight, selectedValues[2]];
                selectedHeightUnit = selectedValues[2];
                
                [self.tableView reloadData];
                
            } cancelBlock:^(ActionSheetMultipleStringPicker *picker) {
                
            } origin:self.view];
            
            UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(20, 0, (picker.viewSize.width-40)/3, 36)];
            label1.text = @".   ";
            label1.textAlignment = NSTextAlignmentRight;
            label1.font = [UIFont systemFontOfSize:20.0f];
            label1.backgroundColor = [[UIColor redColor] colorWithAlphaComponent:0.f];
            [[picker.pickerView.subviews objectAtIndex:1] addSubview:label1];
        }
        if(indexPath.row == 6)
        {
            NSArray *stringList = [NSArray arrayWithObjects:no100List, digitList, [NSArray arrayWithObjects:@"cm", @"in", nil], nil];
            NSArray *initialList = [NSArray arrayWithObjects:@([selectedHeadSizeIndexes[0] intValue]), @([selectedHeadSizeIndexes[1] intValue]), @([selectedHeadSizeIndexes[2] intValue]), nil];
            
            ActionSheetMultipleStringPicker *picker = [ActionSheetMultipleStringPicker showPickerWithTitle:@"" rows:stringList initialSelection:initialList doneBlock:^(ActionSheetMultipleStringPicker *picker, NSArray *selectedIndexes, id selectedValues) {
                
                selectedHeadSizeIndexes = selectedIndexes;
                selectedHeadSize = [[selectedValues componentsJoinedByString:@"."] floatValue];
                //self.txtHeadSize.text = [self convertUnit:[NSString stringWithFormat:@"%f %@", selectedHeadSize, [unitInfo objectForKey:@"head_size"]] unit:[unitInfo objectForKey:@"head_size"]];
                self.txtHeadSize.text = [NSString stringWithFormat:@"%.1f %@", selectedHeadSize, selectedValues[2]];
                selectedHeadSizeUnit = selectedValues[2];
                
                [self.tableView reloadData];
                
            } cancelBlock:^(ActionSheetMultipleStringPicker *picker) {
                
            } origin:self.view];
            
            UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(20, 0, (picker.viewSize.width-40)/3, 36)];
            label1.text = @".   ";
            label1.textAlignment = NSTextAlignmentRight;
            label1.font = [UIFont systemFontOfSize:20.0f];
            label1.backgroundColor = [[UIColor redColor] colorWithAlphaComponent:0.f];
            [[picker.pickerView.subviews objectAtIndex:1] addSubview:label1];
            
        }
        if(indexPath.row == 7)
        {
            NSArray *stringList = [NSArray arrayWithObjects:bloodType1List, bloodType2List, nil];
            NSArray *initialList = [NSArray arrayWithObjects:selectedBloodTypeIndexes[0], selectedBloodTypeIndexes[1], nil];
            
            [ActionSheetMultipleStringPicker showPickerWithTitle:@"" rows:stringList initialSelection:initialList doneBlock:^(ActionSheetMultipleStringPicker *picker, NSArray *selectedIndexes, id selectedValues) {
                
                selectedBloodTypeIndexes = selectedIndexes;
                self.txtBloodType.text = [selectedValues componentsJoinedByString:@""];
                
                [self.tableView reloadData];
                
            } cancelBlock:^(ActionSheetMultipleStringPicker *picker) {
                
            } origin:self.view];
        }
    }
}


#pragma mark - UIImagePickerController Delegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    dispatch_async(dispatch_get_main_queue(), ^{
        
        self.btnAddImage.hidden = YES;
        self.btnRemoveImage.hidden = NO;
        imgUploadPhoto = [info objectForKey:UIImagePickerControllerOriginalImage];
        NSLog(@"image=%@", imgUploadPhoto);
        
        if (picker.sourceType == UIImagePickerControllerSourceTypeCamera) {
            UIImageWriteToSavedPhotosAlbum(imgUploadPhoto, self, @selector(savedPhotoImage:didFinishSavingWithError:contextInfo:), nil);
        }
        imageView = [[UIImageView alloc] initWithImage:imgUploadPhoto];
        imageView.frame = CGRectMake(0, 0, imgUploadPhoto.size.width, imgUploadPhoto.size.height);
        
        //    NSLog(@"Height/Width Ratio:%f",imgUploadPhoto.size.height/imgUploadPhoto.size.width);
        //    NSLog(@"Width/Height Ratio:%f",imgUploadPhoto.size.width/imgUploadPhoto.size.height);
        
        float height_Constant = 2.0;
        float width_Constant = 2.0;
        if (imgUploadPhoto.size.width > imgUploadPhoto.size.height) {
            height_Constant = width_Constant = imgUploadPhoto.size.width / imgUploadPhoto.size.height;
        } else {
            height_Constant = width_Constant = imgUploadPhoto.size.height / imgUploadPhoto.size.width;
        }
        
        baseView = [[UIView alloc] initWithFrame:imageView.frame];
        imageView.center = baseView.center;
        
        self.scrollView.contentSize = baseView.frame.size;
        [self.scrollView addSubview:baseView];
        [baseView addSubview:imageView];
        
        self.scrollView.contentOffset = CGPointMake(imgUploadPhoto.size.width, imgUploadPhoto.size.height);
        _scrollView.minimumZoomScale = [self zoomScaleToBound];
        
        [self dismissViewControllerAnimated:NO completion:nil];
    });
}

- (CGFloat)zoomScaleToBound
{
    CGFloat scaleW = self.scrollView.bounds.size.width / imageView.bounds.size.width;
    CGFloat scaleH = self.scrollView.bounds.size.height / imageView.bounds.size.height;
    CGFloat max = MAX(scaleW, scaleH);
    
    return max;
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return baseView;
}

- (void) savedPhotoImage:(UIImage *)image
didFinishSavingWithError:(NSError *)error
             contextInfo:(void *)contextInfo
{
    NSLog(@"This image has been saved to your camera roll");
}

@end
