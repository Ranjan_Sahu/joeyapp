//
//  VideoViewController.m
//  Joey
//
//  Created by webwerks on 5/14/18.
//  Copyright © 2018 Auxilia. All rights reserved.
//

#import "VideoViewController.h"
#import "VideoPlayerManager.h"

@interface VideoViewController ()
{
    VideoPlayerManager *vManager;
}

@end

@implementation VideoViewController

- (BOOL)prefersStatusBarHidden{
    return YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.btnDelete.hidden = [[self.data objectForKey:@"view_check"] boolValue];
    vManager = [VideoPlayerManager new];
    vManager.videoUrl = (NSString *)[self.data objectForKey:@"video"];
    _videoView.backgroundColor = [UIColor clearColor];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [self.view layoutIfNeeded];
    [vManager initilizePlayerwithView:_videoView andController:self];
}

- (IBAction)closeAction:(id)sender
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [vManager removePlayerInstance];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)deleteAction:(id)sender
{
    UIViewController *viewController = (UIViewController *)[self.data objectForKey:@"viewController"];

    if ([self.data objectForKey:@"imageTag"]) {
        viewController.data = [[NSMutableDictionary alloc] initWithDictionary:@{@"deleteCheck":@(YES),@"imageTag":@([[self.data objectForKey:@"imageTag"]integerValue])}];
    }else {
        viewController.data = [[NSMutableDictionary alloc] initWithDictionary:@{@"deleteCheck":@(YES)}];
    }
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [vManager removePlayerInstance];
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

#pragma mark - VideoManager Observer Handler
-(void)itemDidFinishPlaying:(NSNotification *) notification {
    NSLog(@"itemDidFinishPlaying");
    [vManager.player pause];
    [vManager.player seekToTime:kCMTimeZero];
}

-(void) applicationWillResignActive:(NSNotification *)notification {
    [vManager.player pause];
    if (vManager.isStatusObserverSet) {
        [vManager.player.currentItem removeObserver:self forKeyPath:@"status" ];
        vManager.isStatusObserverSet = NO;
    }
}

@end
