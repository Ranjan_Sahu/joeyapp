//
//  TimelineViewController.m
//  Joey
//
//  Created by csl on 21/12/2016.
//  Copyright © 2016 Auxilia. All rights reserved.
//

#import "AppDelegate.h"
#import "TimelineViewController.h"
#import "TimelineDayTableViewCell.h"
#import "TimelineWeekMonthTableViewCell.h"
#import "TimelineWeekMonthFeedTableViewCell.h"
#import "EmptyTableViewCell.h"
#import "TimelineWeekMonthPooTableViewCell.h"
#import "TimelineWeekMonthPumpTableViewCell.h"

@interface TimelineViewController ()
{
    NSUserDefaults *shareObject;
    AppDelegate *appDelegate;
    NSDateFormatter *formatter;
    NSMutableArray *babyList;
    NSMutableArray *dayList;
    NSMutableArray *weekList;
    NSMutableArray *monthList;
    NSMutableDictionary *unitInfo;
    NSTimer *timer;
    BOOL isDayListInit;
    BOOL isWeekListInit;
    BOOL isMonthListInit;
    BOOL isDayListLoading;
    BOOL isWeekListLoading;
    BOOL isMonthListLoading;
    int limit;
    int dayListOffset;
    int weekListOffset;
    int monthListOffset;
    int selectedIndex;
    int backupPageNo;
    int selectedBabyIndex;
}
@end

@implementation TimelineViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    shareObject = [NSUserDefaults standardUserDefaults];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    formatter = [[NSDateFormatter alloc] init];
    [formatter setTimeZone:[NSTimeZone localTimeZone]];
    [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
    
    unitInfo = (NSMutableDictionary *)[NSKeyedUnarchiver unarchiveObjectWithData:[shareObject objectForKey:@"unit_info"]];
    dayList = [[NSMutableArray alloc] init];
    weekList = [[NSMutableArray alloc] init];
    monthList = [[NSMutableArray alloc] init];
    backupPageNo = 1;
    limit = 10;
    
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:UIColorFromRGB(0x8D8082), NSForegroundColorAttributeName, [UIFont fontWithName:@"SFUIDisplay-Medium" size:18.0f], NSFontAttributeName, nil]];
    [self.navigationController.navigationBar setBarTintColor:UIColorFromRGB(0xEEE8E4)];
    [self.navigationController.navigationBar setTranslucent:NO];
    [self customSettingsButton];
    
    UIButton *btnLogo = [[UIButton alloc] initWithFrame:CGRectZero];
    [btnLogo setImage:[UIImage imageNamed:@"nav_logo.png"] forState:UIControlStateNormal];
    [btnLogo sizeToFit];
    self.navigationItem.titleView = btnLogo;
    
    self.btnDay.layer.cornerRadius = 3.f;
    self.btnWeek.layer.cornerRadius = 3.f;
    self.btnMonth.layer.cornerRadius = 3.f;
    
    __weak __typeof(self)weakSelf = self;
    [self.dayTableView addPullToRefreshWithActionHandler:^{
        [weakSelf refreshAction];
    }];
    [self.dayTableView addInfiniteScrollingWithActionHandler:^{
        [weakSelf getDayList];
    }];
    
    [self.weekTableView addPullToRefreshWithActionHandler:^{
        [weakSelf refreshAction];
    }];
    [self.weekTableView addInfiniteScrollingWithActionHandler:^{
        [weakSelf getWeekList];
    }];
    
    [self.monthTableView addPullToRefreshWithActionHandler:^{
        [weakSelf refreshAction];
    }];
    [self.monthTableView addInfiniteScrollingWithActionHandler:^{
        [weakSelf getMonthList];
    }];
    
    [SVProgressHUD show];
    [self setupBabyList];
    [self refreshAction];
    
    if (IS_IPAD) {
        self.txtName1.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.txtInfo1.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:22.0f];
        self.txtName2.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.txtInfo2.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:22.0f];
        self.txtName3.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.txtInfo3.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:22.0f];
        
        [_btnDay.titleLabel setFont:[UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f]];
        [_btnWeek.titleLabel setFont:[UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f]];
        [_btnMonth.titleLabel setFont:[UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f]];
    }

    if(timer == nil) timer = [NSTimer scheduledTimerWithTimeInterval:60 target:self selector:@selector(refreshAction) userInfo:nil repeats:YES];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:(BOOL)animated];
    
    [appDelegate.tracker send:[[[GAIDictionaryBuilder createScreenView] set:@"Timeline" forKey:kGAIScreenName] build]];
    
    [self.navigationController.navigationBar setBarTintColor:UIColorFromRGB(0xEEE8E4)];
    
    [self.btnDay setTitle:LocalizedString(@"btn_day", nil) forState:UIControlStateNormal];
    [self.btnWeek setTitle:LocalizedString(@"btn_week", nil) forState:UIControlStateNormal];
    [self.btnMonth setTitle:LocalizedString(@"btn_month", nil) forState:UIControlStateNormal];
    
    unitInfo = (NSMutableDictionary *)[NSKeyedUnarchiver unarchiveObjectWithData:[shareObject objectForKey:@"unit_info"]];
    NSLog(@"unitInfo=%@", unitInfo);
    NSLog(@"here=%@", self.data);
    NSLog(@"appDelegate.updateTimelineCheck=%@", appDelegate.updateTimelineCheck?@"YES":@"NO");
    if([[self.data objectForKey:@"save_check"] boolValue] || appDelegate.updateTimelineCheck || selectedBabyIndex != (int)[shareObject integerForKey:@"baby_index"])
    {
        appDelegate.updateTimelineCheck = NO;
        isDayListInit = isWeekListInit = isMonthListInit = NO;
        [self.data removeAllObjects];
        
        [SVProgressHUD show];
        [self setupBabyList];
        [self refreshAction];
    }
    
    /*if(!appDelegate.cbCentralManager.delegate)
    {
        appDelegate.cbCentralManager.delegate = appDelegate;
        [appDelegate scanForPeripherals:YES];
    }
    
    NSLog(@"ble1=%@", appDelegate.bluetoothManager);*/
}




- (void)viewDidDisappear:(BOOL)animated
{
    NSLog(@"TimelineViewController viewDidDisappear");
    [super viewDidDisappear:animated];
    [timer invalidate];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    if(self.babyScrollView.contentSize.width == 0)
    {
        self.babyScrollView.contentSize = CGSizeMake(self.view.frame.size.width*3, 1);
        self.babyScrollView.contentOffset = CGPointMake(self.view.frame.size.width, self.babyScrollView.contentOffset.y);
        self.contentScrollView.contentSize = CGSizeMake(self.view.frame.size.width*3, 1);
    }
    
    self.imgBabyPic1.clipsToBounds = self.imgBabyPic2.clipsToBounds = self.imgBabyPic3.clipsToBounds = YES;
    self.imgBabyPic1.layer.cornerRadius = self.imgBabyPic1.frame.size.width/2;
    self.imgBabyPic2.layer.cornerRadius = self.imgBabyPic2.frame.size.width/2;
    self.imgBabyPic3.layer.cornerRadius = self.imgBabyPic3.frame.size.width/2;
}


#pragma mark :- UI setup
- (void)customSettingsButton
{
    UIImage *imgSettings = [UIImage imageNamed:@"nav_btn_settings"];
    UIButton *btnSettings = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnSettings setImage:imgSettings forState:UIControlStateNormal];
    btnSettings.frame = CGRectMake(52, 0, imgSettings.size.width, imgSettings.size.height);
    [btnSettings addTarget:self action:@selector(getSettingsAction:) forControlEvents:UIControlEventTouchUpInside];
    
    UIView *buttonView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 88, 44)];
    [buttonView addSubview:btnSettings];
    
    UIBarButtonItem *backBarItem = [[UIBarButtonItem alloc] initWithCustomView:buttonView];
    self.navigationItem.rightBarButtonItem = backBarItem;
}

- (void)setupBabyList
{
    babyList = (NSMutableArray *)[NSKeyedUnarchiver unarchiveObjectWithData:[shareObject objectForKey:@"baby_list"]];
    
    selectedBabyIndex = (int)[shareObject integerForKey:@"baby_index"];
    selectedBabyIndex = (selectedBabyIndex >= babyList.count)?0:selectedBabyIndex;
    NSLog(@"selectedBabyIndex1=%d", selectedBabyIndex);
    
    /*if([shareObject integerForKey:@"wearable_baby_id"] != [shareObject integerForKey:@"baby_id"])
    {
        for(int i=0; i<babyList.count; i++)
        {
            if([[[babyList objectAtIndex:i] objectForKey:@"id"] intValue] == [shareObject integerForKey:@"wearable_baby_id"])
            {
                selectedBabyIndex = i;
                NSLog(@"selectedBabyIndex2=%d", selectedBabyIndex);
                break;
            }
        }
        
        [shareObject setInteger:selectedBabyIndex forKey:@"baby_index"];
        [shareObject setInteger:[shareObject integerForKey:@"wearable_baby_id"] forKey:@"baby_id"];
        [shareObject synchronize];
    }*/
    
    for(int i=0; i<babyList.count; i++)
    {
        NSLog(@"weight=%@", [[babyList objectAtIndex:i] objectForKey:@"weight"]);
        NSLog(@"unit=%@", [unitInfo objectForKey:@"weight"]);
        [[babyList objectAtIndex:i] setObject:[self convertUnit:[[babyList objectAtIndex:i] objectForKey:@"height"] unit:[unitInfo objectForKey:@"height"] spacing:YES] forKey:@"height"];
        [[babyList objectAtIndex:i] setObject:[self convertUnit:[[babyList objectAtIndex:i] objectForKey:@"weight"] unit:[unitInfo objectForKey:@"weight"] spacing:YES] forKey:@"weight"];
    }
    
    NSLog(@"babyList2=%@", babyList);
    if(babyList.count == 1)
    {
        self.babyScrollView.scrollEnabled = NO;
        [self.imgBabyPic2 sd_setImageWithURL:[NSURL URLWithString:[[babyList objectAtIndex:0] objectForKey:@"photo_url"]] placeholderImage:[UIImage imageNamed:@"dummy"]];
        self.txtName2.text = [[babyList objectAtIndex:0] objectForKey:@"name"];
        self.txtInfo2.text = [NSString stringWithFormat:LocalizedString(@"txt_baby_info", nil), [[babyList objectAtIndex:0] objectForKey:@"age"], [[babyList objectAtIndex:0] objectForKey:@"height"], [[babyList objectAtIndex:0] objectForKey:@"weight"], [[babyList objectAtIndex:0] objectForKey:@"blood_type"]];
    }
    else
    {
        int prevIndex = selectedBabyIndex-1;
        prevIndex = (prevIndex < 0)?(int)(babyList.count-1):prevIndex;
        int nextIndex = selectedBabyIndex+1;
        nextIndex = (nextIndex >= babyList.count)?0:nextIndex;
        
        NSLog(@"prevIndex=%d, selectedBabyIndex=%d, nextIndex=%d", prevIndex, selectedBabyIndex, nextIndex);
        self.babyScrollView.scrollEnabled = YES;
        [self.imgBabyPic1 sd_setImageWithURL:[NSURL URLWithString:[[babyList objectAtIndex:prevIndex] objectForKey:@"photo_url"]] placeholderImage:[UIImage imageNamed:@"dummy.png"]];
        self.txtName1.text = [[babyList objectAtIndex:prevIndex] objectForKey:@"name"];
        self.txtInfo1.text = [NSString stringWithFormat:LocalizedString(@"txt_baby_info", nil), [[babyList objectAtIndex:prevIndex] objectForKey:@"age"], [[babyList objectAtIndex:prevIndex] objectForKey:@"height"], [[babyList objectAtIndex:prevIndex] objectForKey:@"weight"], [[babyList objectAtIndex:prevIndex] objectForKey:@"blood_type"]];
        
        [self.imgBabyPic2 sd_setImageWithURL:[NSURL URLWithString:[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"photo_url"]] placeholderImage:[UIImage imageNamed:@"dummy.png"]];
        self.txtName2.text = [[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"name"];
        self.txtInfo2.text = [NSString stringWithFormat:LocalizedString(@"txt_baby_info", nil), [[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"age"], [[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"height"], [[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"weight"], [[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"blood_type"]];
        
        [self.imgBabyPic3 sd_setImageWithURL:[NSURL URLWithString:[[babyList objectAtIndex:nextIndex] objectForKey:@"photo_url"]] placeholderImage:[UIImage imageNamed:@"dummy.png"]];
        self.txtName3.text = [[babyList objectAtIndex:nextIndex] objectForKey:@"name"];
        self.txtInfo3.text = [NSString stringWithFormat:LocalizedString(@"txt_baby_info", nil), [[babyList objectAtIndex:nextIndex] objectForKey:@"age"], [[babyList objectAtIndex:nextIndex] objectForKey:@"height"], [[babyList objectAtIndex:nextIndex] objectForKey:@"weight"], [[babyList objectAtIndex:nextIndex] objectForKey:@"blood_type"]];
    }
    
    [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"feed_info"]] forKey:@"feed_info"];
    [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"pump_info"]] forKey:@"pump_info"];
    [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"sleep_info"]] forKey:@"sleep_info"];
    [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"poo_info"]] forKey:@"poo_info"];
    [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"wee_info"]] forKey:@"wee_info"];
    [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"medical_info"]] forKey:@"medical_info"];
    [shareObject synchronize];
}

- (void)refreshAction
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    [shareObject setInteger:[[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"id"] intValue] forKey:@"baby_id"];
    NSLog(@"here1");
    [shareObject setInteger:selectedBabyIndex forKey:@"baby_index"];
    NSLog(@"here2");
    [shareObject synchronize];
    
    if(selectedIndex == 0)
    {
        dayListOffset = 0;
        isDayListLoading = NO;
        self.dayTableView.showsInfiniteScrolling = NO;
        [self getDayList];
    }
    else if(selectedIndex == 1)
    {
        weekListOffset = 0;
        isWeekListLoading = NO;
        self.weekTableView.showsInfiniteScrolling = NO;
        [self getWeekList];
    }
    else
    {
        monthListOffset = 0;
        isMonthListLoading = NO;
        self.monthTableView.showsInfiniteScrolling = NO;
        [self getMonthList];
    }
}

#pragma mark : WS Calls
- (void)getDayList
{
    if(isDayListLoading) return;
    isDayListLoading = YES;
    isDayListInit = YES;
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setValue:@([shareObject integerForKey:@"user_id"]) forKey:@"user_id"];
    [parameters setValue:@([shareObject integerForKey:@"baby_id"]) forKey:@"baby_id"];
    [parameters setValue:@(dayListOffset) forKey:@"offset"];
    [parameters setValue:@(limit) forKey:@"limit"];
    [[WebServiceManager sharedManager] initWithBaseURL:@"api/getTimelineListByDay" parameters:parameters success:^(id result) {
        
        if(dayListOffset == 0) [dayList removeAllObjects];
        
        NSMutableArray *list = (NSMutableArray *)[result valueForKey:@"list"];
        if(list.count > 0)
        {
            if(dayListOffset == 0)
            {
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    NSIndexPath *top = [NSIndexPath indexPathForRow:0 inSection:0];
                    [self.dayTableView scrollToRowAtIndexPath:top atScrollPosition:UITableViewScrollPositionTop animated:YES];
                });
            }
            
            self.dayTableView.showsInfiniteScrolling = YES;
            [dayList addObjectsFromArray:list];
            dayListOffset += limit;
        }
        else self.dayTableView.showsInfiniteScrolling = NO;
        
        BOOL sleepCheck = NO;
        for(int i=0; i<list.count; i++)
        {
            for(int j=0; j<[[[list objectAtIndex:i] objectForKey:@"list"] count]; j++)
            {
                NSString *name = [[[[list objectAtIndex:i] objectForKey:@"list"] objectAtIndex:j] objectForKey:@"name"];
                NSString *type = [[[[list objectAtIndex:i] objectForKey:@"list"] objectAtIndex:j] objectForKey:@"type"];
                
                if([name isEqualToString:@"feed"])
                {
                    if([type isEqualToString:@"solids"])
                    {
                        [[[[list objectAtIndex:i] objectForKey:@"list"] objectAtIndex:j] setObject:[self convertUnit:[[[[list objectAtIndex:i] objectForKey:@"list"] objectAtIndex:j] objectForKey:@"attribute_value"] unit:[unitInfo objectForKey:@"serving"] spacing:YES] forKey:@"attribute_value"];
                    }
                    else if([type isEqualToString:@"bbottle"] || [type isEqualToString:@"formula"] || [type isEqualToString:@"water"] || [type isEqualToString:@"juice"])
                    {
                        [[[[list objectAtIndex:i] objectForKey:@"list"] objectAtIndex:j] setObject:[self convertUnit:[[[[list objectAtIndex:i] objectForKey:@"list"] objectAtIndex:j] objectForKey:@"attribute_value"] unit:[unitInfo objectForKey:@"volume"] spacing:YES] forKey:@"attribute_value"];
                    }
                }
                else if([name isEqualToString:@"pump"])
                {
                    [[[[list objectAtIndex:i] objectForKey:@"list"] objectAtIndex:j] setObject:[self convertUnit:[[[[list objectAtIndex:i] objectForKey:@"list"] objectAtIndex:j] objectForKey:@"attribute_value"] unit:[unitInfo objectForKey:@"volume"] spacing:YES] forKey:@"attribute_value"];
                }
                else if([name isEqualToString:@"medical"])
                {
                    if([type isEqualToString:@"temperature"])
                    {
                        [[[[list objectAtIndex:i] objectForKey:@"list"] objectAtIndex:j] setObject:[self convertUnit:[[[[list objectAtIndex:i] objectForKey:@"list"] objectAtIndex:j] objectForKey:@"attribute_value"] unit:[unitInfo objectForKey:@"temperature"] spacing:YES] forKey:@"attribute_value"];
                    }
                    else if([type containsString:@"med"])
                    {
                        //[[[[list objectAtIndex:i] objectForKey:@"list"] objectAtIndex:j] setObject:[self convertUnit:[[[[list objectAtIndex:i] objectForKey:@"list"] objectAtIndex:j] objectForKey:@"attribute_value"] unit:[unitInfo objectForKey:@"medicine"] spacing:YES] forKey:@"attribute_value"];
                    }
                }
                
                if([name isEqualToString:@"sleep"])
                {
                    if(!sleepCheck)
                    {
                        sleepCheck = YES;
                        NSLog(@"sleepCheck");
                        
                        if([type isEqualToString:@"sleep_cycle"])
                        {
                            [shareObject removeObjectForKey:[NSString stringWithFormat:@"sleep_id_%d", (int)[shareObject integerForKey:@"baby_id"]]];
                            [shareObject removeObjectForKey:[NSString stringWithFormat:@"sleep_time_%d", (int)[shareObject integerForKey:@"baby_id"]]];
                        }
                        else
                        {
                            [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
                            formatter.dateFormat = @"yyyy-MM-dd hh:mm a";
                            NSDate *time = [formatter dateFromString:[NSString stringWithFormat:@"%@ %@", [[list objectAtIndex:i] objectForKey:@"date"], [[list objectAtIndex:i] objectForKey:@"time"]]];
                            
                            [shareObject setObject:@([[[list objectAtIndex:i] objectForKey:@"id"] intValue]) forKey:[NSString stringWithFormat:@"sleep_id_%d", (int)[shareObject integerForKey:@"baby_id"]]];
                            [shareObject setObject:time forKey:[NSString stringWithFormat:@"sleep_time_%d", (int)[shareObject integerForKey:@"baby_id"]]];
                        }
                        
                        [shareObject synchronize];
                    }
                }
            }
        }
        
        isDayListLoading = NO;
        [self.dayTableView reloadData];
        [self.dayTableView.pullToRefreshView stopAnimating];
        [self.dayTableView.infiniteScrollingView stopAnimating];
        [list removeAllObjects];
        list = result = nil;
        
        
    } failure:^(NSError *error) {
        isDayListLoading = NO;
        [self.dayTableView.pullToRefreshView stopAnimating];
        [self.dayTableView.infiniteScrollingView stopAnimating];
    }];
}

- (void)getWeekList
{
    if(isWeekListLoading) return;
    isWeekListLoading = YES;
    isWeekListInit = YES;
    limit = 30;
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setValue:@([shareObject integerForKey:@"user_id"]) forKey:@"user_id"];
    [parameters setValue:@([shareObject integerForKey:@"baby_id"]) forKey:@"baby_id"];
    [parameters setValue:@(weekListOffset) forKey:@"offset"];
    [parameters setValue:@(limit) forKey:@"limit"];
    [[WebServiceManager sharedManager] initWithBaseURL:@"api/getTimelineListByWeek" parameters:parameters success:^(id result) {
        
        if(weekListOffset == 0) [weekList removeAllObjects];
        
        NSMutableArray *list = (NSMutableArray *)[result valueForKey:@"list"];
        if(list.count > 0)
        {
            if(weekListOffset == 0)
            {
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    NSIndexPath *top = [NSIndexPath indexPathForRow:0 inSection:0];
                    [self.weekTableView scrollToRowAtIndexPath:top atScrollPosition:UITableViewScrollPositionTop animated:NO];
                });
            }
            
            self.weekTableView.showsInfiniteScrolling = YES;
            [weekList addObjectsFromArray:list];
            weekListOffset += limit;
        }
        else self.weekTableView.showsInfiniteScrolling = NO;
        
        
        for(int i=0; i<list.count; i++)
        {
            for(int j=0; j<[[[list objectAtIndex:i] objectForKey:@"list"] count]; j++)
            {
                NSString *name = [[[[list objectAtIndex:i] objectForKey:@"list"] objectAtIndex:j] objectForKey:@"name"];
                NSString *type = [[[[list objectAtIndex:i] objectForKey:@"list"] objectAtIndex:j] objectForKey:@"type"];
        
                if([name isEqualToString:@"feed"])
                {
                    
                    [[[[list objectAtIndex:i] objectForKey:@"list"] objectAtIndex:j] setObject:[self convertUnit:[[[[list objectAtIndex:i] objectForKey:@"list"] objectAtIndex:j] objectForKey:@"attribute_value1"] unit:[unitInfo objectForKey:@"volume"] spacing:YES] forKey:@"attribute_value1"];
                    
                    //[[[[list objectAtIndex:i] objectForKey:@"list"] objectAtIndex:j] setObject:[self convertUnit:[[[[list objectAtIndex:i] objectForKey:@"list"] objectAtIndex:j] objectForKey:@"attribute_value2"] unit:[unitInfo objectForKey:@"volume"]] forKey:@"attribute_value2"];
                    
                    [[[[list objectAtIndex:i] objectForKey:@"list"] objectAtIndex:j] setObject:[self convertUnit:[[[[list objectAtIndex:i] objectForKey:@"list"] objectAtIndex:j] objectForKey:@"attribute_value3"] unit:[unitInfo objectForKey:@"serving"] spacing:YES] forKey:@"attribute_value3"];
                }
                else if([name isEqualToString:@"pump"])
                {
                    [[[[list objectAtIndex:i] objectForKey:@"list"] objectAtIndex:j] setObject:[self convertUnit:[[[[list objectAtIndex:i] objectForKey:@"list"] objectAtIndex:j] objectForKey:@"attribute_value2"] unit:[unitInfo objectForKey:@"volume"] spacing:YES] forKey:@"attribute_value2"];
                }
                else if([name isEqualToString:@"medical"])
                {
                    if([type isEqualToString:@"temperature"])
                    {
                        [[[[list objectAtIndex:i] objectForKey:@"list"] objectAtIndex:j] setObject:[self convertUnit:[[[[list objectAtIndex:i] objectForKey:@"list"] objectAtIndex:j] objectForKey:@"attribute_value1"] unit:[unitInfo objectForKey:@"temperature"] spacing:YES] forKey:@"attribute_value1"];
                        
                        [[[[list objectAtIndex:i] objectForKey:@"list"] objectAtIndex:j] setObject:[self convertUnit:[[[[list objectAtIndex:i] objectForKey:@"list"] objectAtIndex:j] objectForKey:@"attribute_value2"] unit:[unitInfo objectForKey:@"temperature"] spacing:YES] forKey:@"attribute_value2"];
                    }
                    else if([type containsString:@"med"])
                    {
                        //[[[[list objectAtIndex:i] objectForKey:@"list"] objectAtIndex:j] setObject:[self convertUnit:[[[[list objectAtIndex:i] objectForKey:@"list"] objectAtIndex:j] objectForKey:@"attribute_value1"] unit:[unitInfo objectForKey:@"medicine"] spacing:YES] forKey:@"attribute_value1"];
                        
                        //[[[[list objectAtIndex:i] objectForKey:@"list"] objectAtIndex:j] setObject:[self convertUnit:[[[[list objectAtIndex:i] objectForKey:@"list"] objectAtIndex:j] objectForKey:@"attribute_value2"] unit:[unitInfo objectForKey:@"medicine"] spacing:YES] forKey:@"attribute_value2"];
                    }
                }
            }
        }
        
        
        
        isWeekListLoading = NO;
        [self.weekTableView reloadData];
        [self.weekTableView.pullToRefreshView stopAnimating];
        [self.weekTableView.infiniteScrollingView stopAnimating];
        [list removeAllObjects];
        list = result = nil;
        
    } failure:^(NSError *error) {
        isWeekListLoading = NO;
        [self.weekTableView.pullToRefreshView stopAnimating];
        [self.weekTableView.infiniteScrollingView stopAnimating];
    }];

}

- (void)getMonthList
{
    if(isMonthListLoading) return;
    isMonthListLoading = YES;
    isMonthListInit = YES;
    limit = 30;
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setValue:@([shareObject integerForKey:@"user_id"]) forKey:@"user_id"];
    [parameters setValue:@([shareObject integerForKey:@"baby_id"]) forKey:@"baby_id"];
    [parameters setValue:@(monthListOffset) forKey:@"offset"];
    [parameters setValue:@(limit) forKey:@"limit"];
    [[WebServiceManager sharedManager] initWithBaseURL:@"api/getTimelineListByMonth" parameters:parameters success:^(id result) {
        
        if(monthListOffset == 0) [monthList removeAllObjects];
        
        NSMutableArray *list = (NSMutableArray *)[result valueForKey:@"list"];
        if(list.count > 0)
        {
            if(monthListOffset == 0)
            {
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    NSIndexPath *top = [NSIndexPath indexPathForRow:0 inSection:0];
                    [self.monthTableView scrollToRowAtIndexPath:top atScrollPosition:UITableViewScrollPositionTop animated:NO];
                });
            }
            
            self.monthTableView.showsInfiniteScrolling = YES;
            [monthList addObjectsFromArray:list];
            monthListOffset += limit;
        }
        else self.monthTableView.showsInfiniteScrolling = NO;
        
        for(int i=0; i<list.count; i++)
        {
            for(int j=0; j<[[[list objectAtIndex:i] objectForKey:@"list"] count]; j++)
            {
                NSString *name = [[[[list objectAtIndex:i] objectForKey:@"list"] objectAtIndex:j] objectForKey:@"name"];
                NSString *type = [[[[list objectAtIndex:i] objectForKey:@"list"] objectAtIndex:j] objectForKey:@"type"];
                
                if([name isEqualToString:@"feed"])
                {
                    [[[[list objectAtIndex:i] objectForKey:@"list"] objectAtIndex:j] setObject:[self convertUnit:[[[[list objectAtIndex:i] objectForKey:@"list"] objectAtIndex:j] objectForKey:@"attribute_value1"] unit:[unitInfo objectForKey:@"volume"] spacing:YES] forKey:@"attribute_value1"];
                    
                    //[[[[list objectAtIndex:i] objectForKey:@"list"] objectAtIndex:j] setObject:[self convertUnit:[[[[list objectAtIndex:i] objectForKey:@"list"] objectAtIndex:j] objectForKey:@"attribute_value2"] unit:[unitInfo objectForKey:@"volume"]] forKey:@"attribute_value2"];
                    
                    [[[[list objectAtIndex:i] objectForKey:@"list"] objectAtIndex:j] setObject:[self convertUnit:[[[[list objectAtIndex:i] objectForKey:@"list"] objectAtIndex:j] objectForKey:@"attribute_value3"] unit:[unitInfo objectForKey:@"serving"] spacing:YES] forKey:@"attribute_value3"];
                }
                else if([name isEqualToString:@"pump"])
                {
                    [[[[list objectAtIndex:i] objectForKey:@"list"] objectAtIndex:j] setObject:[self convertUnit:[[[[list objectAtIndex:i] objectForKey:@"list"] objectAtIndex:j] objectForKey:@"attribute_value2"] unit:[unitInfo objectForKey:@"volume"] spacing:YES] forKey:@"attribute_value2"];
                }
                else if([name isEqualToString:@"medical"])
                {
                    if([type isEqualToString:@"temperature"])
                    {
                        [[[[list objectAtIndex:i] objectForKey:@"list"] objectAtIndex:j] setObject:[self convertUnit:[[[[list objectAtIndex:i] objectForKey:@"list"] objectAtIndex:j] objectForKey:@"attribute_value1"] unit:[unitInfo objectForKey:@"temperature"] spacing:YES] forKey:@"attribute_value1"];
                        
                        [[[[list objectAtIndex:i] objectForKey:@"list"] objectAtIndex:j] setObject:[self convertUnit:[[[[list objectAtIndex:i] objectForKey:@"list"] objectAtIndex:j] objectForKey:@"attribute_value2"] unit:[unitInfo objectForKey:@"temperature"] spacing:YES] forKey:@"attribute_value2"];
                    }
                    else if([type containsString:@"med"])
                    {
                        //[[[[list objectAtIndex:i] objectForKey:@"list"] objectAtIndex:j] setObject:[self convertUnit:[[[[list objectAtIndex:i] objectForKey:@"list"] objectAtIndex:j] objectForKey:@"attribute_value1"] unit:[unitInfo objectForKey:@"medicine"] spacing:YES] forKey:@"attribute_value1"];
                        
                        //[[[[list objectAtIndex:i] objectForKey:@"list"] objectAtIndex:j] setObject:[self convertUnit:[[[[list objectAtIndex:i] objectForKey:@"list"] objectAtIndex:j] objectForKey:@"attribute_value2"] unit:[unitInfo objectForKey:@"medicine"] spacing:YES] forKey:@"attribute_value2"];
                    }
                }
            }
        }
        
        isMonthListLoading = NO;
        [self.monthTableView reloadData];
        [self.monthTableView.pullToRefreshView stopAnimating];
        [self.monthTableView.infiniteScrollingView stopAnimating];
        [list removeAllObjects];
        list = result = nil;
        
    } failure:^(NSError *error) {
        isMonthListLoading = NO;
        [self.monthTableView.pullToRefreshView stopAnimating];
        [self.monthTableView.infiniteScrollingView stopAnimating];
    }];
}

- (IBAction)getDayListAction:(id)sender
{
    selectedIndex = 0;
    
    if(!isDayListInit)
    {
        [self refreshAction];
    }
    
    [self.btnDay setBackgroundColor:UIColorFromRGB(0x8D8082)];
    [self.btnWeek setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    
    [UIView animateWithDuration:0.3 animations:^{
        self.contentScrollView.contentOffset = CGPointMake(selectedIndex*self.view.bounds.size.width, self.contentScrollView.contentOffset.y);
    }];
}

- (IBAction)getWeekListAction:(id)sender
{
    selectedIndex = 1;
    
    if(!isWeekListInit)
    {
        [self refreshAction];
    }
    
    [self.btnDay setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnWeek setBackgroundColor:UIColorFromRGB(0x8D8082)];
    [self.btnMonth setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    
    [UIView animateWithDuration:0.3 animations:^{
        self.contentScrollView.contentOffset = CGPointMake(selectedIndex*self.view.bounds.size.width, self.contentScrollView.contentOffset.y);
    }];
}

- (IBAction)getMonthListAction:(id)sender
{
    selectedIndex = 2;
    
    if(!isMonthListInit)
    {
        [self refreshAction];
    }
    
    [self.btnDay setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnWeek setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth setBackgroundColor:UIColorFromRGB(0x8D8082)];
    
    [UIView animateWithDuration:0.3 animations:^{
        self.contentScrollView.contentOffset = CGPointMake(selectedIndex*self.view.bounds.size.width, self.contentScrollView.contentOffset.y);
    }];
}

- (IBAction)getBabyInfoAction:(id)sender
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Settings" bundle:nil];
    UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"SettingsBabyViewController"];
    viewController.data = [[babyList objectAtIndex:selectedBabyIndex] mutableCopy];
    NSLog(@"babyList=%@", babyList);
    NSLog(@"baby data=%@", viewController.data);
    viewController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:viewController animated:YES];
}

- (IBAction)getSettingsAction:(id)sender
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Settings" bundle:nil];
    UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"SettingsViewController"];
    viewController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:viewController animated:YES];
}


#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if([tableView isEqual:self.dayTableView])
    {
        if(dayList.count == 0) return 1;
        else return dayList.count;
    }
    else if([tableView isEqual:self.weekTableView])
    {
        if(weekList.count == 0) return 1;
        else return weekList.count;
    }
    else
    {
        if(monthList.count == 0) return 1;
        else return monthList.count;
    }
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if([tableView isEqual:self.dayTableView])
    {
        NSDate *date = [NSDate date];
        if(dayList.count > 0)
        {
            NSString *dateString = [[dayList objectAtIndex:section] objectForKey:@"header"];
            [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
            formatter.dateFormat = @"yyyy-MM-dd";
            date = [formatter dateFromString:dateString];
        }
        
        if([[shareObject objectForKey:@"language"] hasPrefix:@"zh"])
        {
            [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"zh_HK"]];
            [formatter setDateStyle:NSDateFormatterMediumStyle];
            [formatter setTimeStyle:NSDateFormatterShortStyle];
            
            return [[NSString stringWithFormat:@"%@", [formatter stringFromDate:date]] componentsSeparatedByString:@" "][0];
        }
        else
        {
            [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
            formatter.dateFormat = @"EE d MMM, yyyy";
            return [NSString stringWithFormat:@"%@", [formatter stringFromDate:date]];
        }
    }
    else if([tableView isEqual:self.weekTableView])
    {
        NSDate *date = [NSDate date];
        if(weekList.count > 0)
        {
            NSString *dateString = [[weekList objectAtIndex:section] objectForKey:@"header"];
            [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
            formatter.dateFormat = @"yyyy-MM-dd";
            date = [formatter dateFromString:dateString];
        }
        
        NSDate *firstDayOfWeek = [self getFirstDayOfTheWeekFromDate:date];
        NSDate *endDayOfWeek = [firstDayOfWeek dateByAddingTimeInterval:6*24*60*60];
        
        NSDateComponents *components1 = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:firstDayOfWeek];
        NSDateComponents *components2 = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:endDayOfWeek];
        
        if([[shareObject objectForKey:@"language"] hasPrefix:@"zh"])
        {
            [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"zh_HK"]];
            [formatter setDateStyle:NSDateFormatterMediumStyle];
            [formatter setTimeStyle:NSDateFormatterShortStyle];
            
            NSString *start = [[formatter stringFromDate:firstDayOfWeek]  componentsSeparatedByString:@" "][0];
            NSString *end = [[formatter stringFromDate:endDayOfWeek] componentsSeparatedByString:@" "][0];
            return [NSString stringWithFormat:@"%@ - %@", start, end];
        }
        else
        {
            if([components1 year] == [components2 year])
            {
                if([components1 month] == [components2 month])
                {
                    [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
                    formatter.dateFormat = @"ww, dd";
                    NSString *start = [NSString stringWithFormat:LocalizedString(@"txt_week_no", nil), [formatter stringFromDate:firstDayOfWeek]];
                    formatter.dateFormat = @"-dd MMMM yyyy";
                    NSString *end = [formatter stringFromDate:endDayOfWeek];
                    return [NSString stringWithFormat:@"%@%@", start, end];
                }
                else
                {
                    [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
                    formatter.dateFormat = @"ww, dd MMMM";
                    NSString *start = [NSString stringWithFormat:LocalizedString(@"txt_week_no", nil), [formatter stringFromDate:firstDayOfWeek]];
                    formatter.dateFormat = @" - dd MMMM yyyy";
                    NSString *end = [formatter stringFromDate:endDayOfWeek];
                    return [NSString stringWithFormat:@"%@%@", start, end];
                }
            }
            else
            {
                [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
                formatter.dateFormat = @"ww, dd MMMM yyyy";
                NSString *start = [NSString stringWithFormat:LocalizedString(@"txt_week_no", nil), [formatter stringFromDate:firstDayOfWeek]];
                formatter.dateFormat = @" - dd MMMM yyyy";
                NSString *end = [formatter stringFromDate:endDayOfWeek];
                return [NSString stringWithFormat:@"%@%@", start, end];
            }
        }
    }
    else
    {
        NSDate *date = [NSDate date];
        if(monthList.count > 0)
        {
            NSString *dateString = [[monthList objectAtIndex:section] objectForKey:@"header"];
            [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
            formatter.dateFormat = @"yyyy-MM";
            date = [formatter dateFromString:dateString];
        }
        
        if([[shareObject objectForKey:@"language"] hasPrefix:@"zh"])
        {
            [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"zh_HK"]];
            [formatter setDateStyle:NSDateFormatterMediumStyle];
            [formatter setTimeStyle:NSDateFormatterShortStyle];
            
            return [NSString stringWithFormat:@"%@月", [[formatter stringFromDate:date] componentsSeparatedByString:@"月"][0]];
        }
        else
        {
            [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
            formatter.dateFormat = @"MMMM yyyy";
            return [NSString stringWithFormat:@"%@", [formatter stringFromDate:date]];
        }
    }
    return nil;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    CGFloat headerHeight = 30;
    if (IS_IPAD) {
        headerHeight = 40;
    }
    
    if ([tableView isEqual:self.dayTableView])
    {
        UILabel *txtHeader = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, tableView.bounds.size.width-20, headerHeight)];
        txtHeader.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:15.0f];
        if(IS_IPAD) {
            txtHeader.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f];
        }
        txtHeader.textColor = UIColorFromRGB(0x8D8082);
        txtHeader.text = [self tableView:tableView titleForHeaderInSection:section];
        
        UIView *pictorialDataView = [[UIView alloc] initWithFrame:CGRectMake(0, headerHeight, tableView.bounds.size.width, headerHeight-2)];
        pictorialDataView.backgroundColor = UIColorFromRGB(0xFFFFFF);
        
        UIFont *lblFont = [UIFont fontWithName:@"SFUIDisplay-Regular" size:15.0f];
        if (IS_IPAD) {
            lblFont = [UIFont fontWithName:@"SFUIDisplay-Regular" size:21.0f];
        }
        
        if (dayList.count>0) {
            
            NSMutableDictionary *dataDict = [[NSMutableDictionary alloc] initWithDictionary:[dayList objectAtIndex:section]];
            NSMutableArray *dataArray = [[NSMutableArray alloc] initWithArray:[dataDict objectForKey:@"list"]];
            CGFloat unitX = tableView.bounds.size.width/(24*60);
            
            // New
            NSMutableArray *dayWiseChartList = [[NSMutableArray alloc] initWithArray:[dataDict objectForKey:@"daywiseChart"]];
            
            for (int i=0; i<dayWiseChartList.count; i++) {
                
                NSMutableArray *dataList = [[NSMutableArray alloc] initWithArray:[dayWiseChartList objectAtIndex:i]];
                
                int hourNo = i;
                int noOfSegment = (int)dataList.count;
                CGFloat actualWidthOfHour = 60*unitX;
                CGFloat barWidth = (CGFloat)(actualWidthOfHour/noOfSegment);
                UIColor *vwColor = UIColorFromRGB(0xFFFFFF);
                
                int j = 0;
                for (NSString *type in dataList) {
                    
                    if ([type isEqualToString: @"feed"]) {
                        vwColor = UIColorFromRGB(0xF4E695);
                    }else if ([type isEqualToString: @"pump"]){
                        vwColor = UIColorFromRGB(0xFFC391);
                    }else if ([type isEqualToString: @"sleep"]){
                        vwColor = UIColorFromRGB(0xC1D0DB);
                    }else if ([type isEqualToString: @"poo"]){
                        vwColor = UIColorFromRGB(0xDABEB1);
                    }else if ([type isEqualToString: @"wee"]){
                        vwColor = UIColorFromRGB(0xC4D5A1);
                    }else if ([type isEqualToString: @"medical"]){
                        vwColor = UIColorFromRGB(0xF3CDCF);
                    }
                    
                    CGFloat sleepDuration = 0;
                    CGFloat sleepXValue = 0;
                    
                    CGFloat xValue = (hourNo*unitX*60) + (barWidth*j);
                    UIView *barView = [[UIView alloc] initWithFrame:CGRectMake(xValue, 0, barWidth, headerHeight-2)];
                    barView.backgroundColor = vwColor;
                    [pictorialDataView addSubview:barView];
                    j++;

                    
//                    if ([type isEqualToString:@"sleep"]) {
//
//                        for (int i=0; i<dataArray.count; i++) {
//                            NSDictionary *dict = [dataArray objectAtIndex:i];
//                            NSString *name = [dict valueForKey:@"name"];
//                            CGFloat duration = [[dict valueForKey:@"duration"] floatValue];
//                            NSString *createdTime = [dict valueForKey:@"created_at"];
//                            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//                            [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
//                            NSDate *date = [dateFormatter dateFromString:createdTime];
//                            NSCalendar *calendar = [NSCalendar currentCalendar];
//                            NSDateComponents *components = [calendar components:(NSCalendarUnitHour | NSCalendarUnitMinute) fromDate:date];
//                            NSInteger hour = [components hour];
//                            NSInteger minute = [components minute];
//
//                            NSInteger totlaMinutes = (hour*60)+minute;
//                            sleepXValue = (CGFloat)(totlaMinutes*unitX);
//                            if (duration<15) {
//                                duration = 15.0;
//                            }
//                            sleepDuration = duration*unitX;
//
//                            if ([name isEqualToString:@"sleep"]) {
//                                UIView *barView = [[UIView alloc] initWithFrame:CGRectMake(sleepXValue, 0, sleepDuration, headerHeight-2)];
//                                barView.backgroundColor = vwColor;
//                                [pictorialDataView insertSubview:barView atIndex:0];
//                            }
//                        }
//                    }
//                    else {
//                        CGFloat xValue = (hourNo*unitX*60) + (barWidth*j);
//                        UIView *barView = [[UIView alloc] initWithFrame:CGRectMake(xValue, 0, barWidth, headerHeight-2)];
//                        barView.backgroundColor = vwColor;
//                        [pictorialDataView addSubview:barView];
//                        j++;
//                    }
                }
            }
            
            
            // Hours Label
            UILabel *lblZero = [[UILabel alloc] initWithFrame:CGRectMake(2, 0, 50, headerHeight-2)];
            lblZero.text = @"0";
            lblZero.font = lblFont;
            lblZero.textColor = UIColorFromRGB(0x827577);
            lblZero.textAlignment = NSTextAlignmentLeft;
            
            UILabel *lblSix = [[UILabel alloc] initWithFrame:CGRectMake((tableView.bounds.size.width/4)*1, 0, 50, headerHeight-2)];
            lblSix.text = @"6";
            lblSix.font = lblFont;
            lblSix.textColor = UIColorFromRGB(0x827577);
            lblSix.textAlignment = NSTextAlignmentLeft;
            
            UILabel *lblTweleve = [[UILabel alloc] initWithFrame:CGRectMake((tableView.bounds.size.width/4)*2, 0, 50, headerHeight-2)];
            lblTweleve.text = @"12";
            lblTweleve.font = lblFont;
            lblTweleve.textColor = UIColorFromRGB(0x827577);
            lblTweleve.textAlignment = NSTextAlignmentLeft;
            
            UILabel *lblEighteen = [[UILabel alloc] initWithFrame:CGRectMake((tableView.bounds.size.width/4)*3, 0, 50, headerHeight-2)];
            lblEighteen.text = @"18";
            lblEighteen.font = lblFont;
            lblEighteen.textColor = UIColorFromRGB(0x827577);
            lblEighteen.textAlignment = NSTextAlignmentLeft;
            
            [pictorialDataView addSubview:lblZero];
            [pictorialDataView addSubview:lblSix];
            [pictorialDataView addSubview:lblTweleve];
            [pictorialDataView addSubview:lblEighteen];
            
        }
        
        UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, headerHeight*2)];
        headerView.backgroundColor = UIColorFromRGB(0xEEE8E4);
        [headerView addSubview:txtHeader];
        [headerView addSubview:pictorialDataView];
        return headerView;
    }
    else
    {
        
        UILabel *txtHeader = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, tableView.bounds.size.width-20, headerHeight)];
        txtHeader.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:15.0f];
        if(IS_IPAD) {
            txtHeader.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f];
        }
        txtHeader.textColor = UIColorFromRGB(0x8D8082);
        txtHeader.text = [self tableView:tableView titleForHeaderInSection:section];
        
        UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, headerHeight)];
        headerView.backgroundColor = UIColorFromRGB(0xEEE8E4);
        [headerView addSubview:txtHeader];
        return headerView;
        
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if ([tableView isEqual:self.dayTableView]) {
        if (IS_IPAD) {
            return 40*2;
        }
        else {
            return 30*2;
        }

    }
    else {
        if (IS_IPAD) {
            return 40;
        }
        else {
            return 30;
        }
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if([tableView isEqual:self.dayTableView])
    {
        if(dayList.count > 0)
        {
            if([[[dayList objectAtIndex:section] objectForKey:@"list"] count] == 0) return 1;
            else return [[[dayList objectAtIndex:section] objectForKey:@"list"] count];
        }
    }
    else if([tableView isEqual:self.weekTableView])
    {
        if(weekList.count > 0)
        {
            if([[[weekList objectAtIndex:section] objectForKey:@"list"] count] == 0) return 1;
            else return [[[weekList objectAtIndex:section] objectForKey:@"list"] count];
        }
    }
    else
    {
        if(monthList.count > 0)
        {
            if([[[monthList objectAtIndex:section] objectForKey:@"list"] count] == 0) return 1;
            else return [[[monthList objectAtIndex:section] objectForKey:@"list"] count];
        }
    }
    
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *name;
    NSMutableDictionary *data;
    
    if([tableView isEqual:self.dayTableView])
    {
        name = [[[[dayList objectAtIndex:indexPath.section] objectForKey:@"list"] objectAtIndex:indexPath.row] objectForKey:@"name"];
        data = [[NSMutableDictionary alloc] initWithDictionary:@{@"activity_info":[[[dayList objectAtIndex:indexPath.section] objectForKey:@"list"] objectAtIndex:indexPath.row]}];
        
        if([name isEqualToString:@"feed"])
        {
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Activities" bundle:nil];
            UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"ActivityFeedEditViewController"];
            viewController.data = data;
            viewController.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:viewController animated:YES];
        }
        if([name isEqualToString:@"wee"])
        {
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PooWeeMedStoryboard" bundle:nil];
            UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"ActivityWeeEditViewController"];
            viewController.data = data;
            viewController.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:viewController animated:YES];
        }
        if([name isEqualToString:@"poo"])
        {
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PooWeeMedStoryboard" bundle:nil];
            UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"ActivityPooEditViewController"];
            viewController.data = data;
            viewController.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:viewController animated:YES];
        }
        if([name isEqualToString:@"pump"])
        {
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Activities" bundle:nil];
            UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"ActivityPumpEditViewController"];
            viewController.data = data;
            viewController.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:viewController animated:YES];
        }
        if([name isEqualToString:@"sleep"])
        {
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Activities" bundle:nil];
            UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"ActivitySleepEditViewController"];
            viewController.data = data;
            viewController.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:viewController animated:YES];
        }
        if([name isEqualToString:@"medical"])
        {
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PooWeeMedStoryboard" bundle:nil];
            UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"ActivityMedicalEditViewController"];
            viewController.data = data;
            viewController.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:viewController animated:YES];
        }
    }
    if([tableView isEqual:self.weekTableView])
    {
        name = [[[[weekList objectAtIndex:indexPath.section] objectForKey:@"list"] objectAtIndex:indexPath.row] objectForKey:@"name"];
        data = [[NSMutableDictionary alloc] initWithDictionary:@{@"selected_week":[[weekList objectAtIndex:indexPath.section] objectForKey:@"week"], @"selected_index":@(1), @"monthsold":[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"monthsold"]}];
        
        if([name isEqualToString:@"feed"])
        {
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Activities" bundle:nil];
            UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"ActivityFeedViewController"];
            viewController.data = data;
            viewController.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:viewController animated:YES];
        }
        if([name isEqualToString:@"wee"])
        {
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PooWeeMedStoryboard" bundle:nil];
            UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"ActivityWeeViewController"];
            viewController.data = data;
            viewController.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:viewController animated:YES];
        }
        if([name isEqualToString:@"poo"])
        {
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PooWeeMedStoryboard" bundle:nil];
            UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"ActivityPooViewController"];
            viewController.data = data;
            viewController.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:viewController animated:YES];
        }
        if([name isEqualToString:@"pump"])
        {
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Activities" bundle:nil];
            UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"ActivityPumpViewController"];
            viewController.data = data;
            viewController.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:viewController animated:YES];
        }
        if([name isEqualToString:@"sleep"])
        {
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Activities" bundle:nil];
            UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"ActivitySleepViewController"];
            viewController.data = data;
            viewController.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:viewController animated:YES];
        }
        if([name isEqualToString:@"medical"])
        {
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PooWeeMedStoryboard" bundle:nil];
            UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"ActivityMedicalViewController"];
            viewController.data = data;
            viewController.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:viewController animated:YES];
        }
    }
    if([tableView isEqual:self.monthTableView])
    {
        name = [[[[monthList objectAtIndex:indexPath.section] objectForKey:@"list"] objectAtIndex:indexPath.row] objectForKey:@"name"];
        data = [[NSMutableDictionary alloc] initWithDictionary:@{@"selected_month":[[monthList objectAtIndex:indexPath.section] objectForKey:@"month"], @"selected_index":@(2), @"monthsold":[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"monthsold"]}];
        
        if([name isEqualToString:@"feed"])
        {
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Activities" bundle:nil];
            UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"ActivityFeedViewController"];
            viewController.data = data;
            viewController.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:viewController animated:YES];
        }
        if([name isEqualToString:@"wee"])
        {
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PooWeeMedStoryboard" bundle:nil];
            UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"ActivityWeeViewController"];
            viewController.data = data;
            viewController.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:viewController animated:YES];
        }
        if([name isEqualToString:@"poo"])
        {
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PooWeeMedStoryboard" bundle:nil];
            UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"ActivityPooViewController"];
            viewController.data = data;
            viewController.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:viewController animated:YES];
        }
        if([name isEqualToString:@"pump"])
        {
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Activities" bundle:nil];
            UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"ActivityPumpViewController"];
            viewController.data = data;
            viewController.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:viewController animated:YES];
        }
        if([name isEqualToString:@"sleep"])
        {
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Activities" bundle:nil];
            UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"ActivitySleepViewController"];
            viewController.data = data;
            viewController.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:viewController animated:YES];
        }
        if([name isEqualToString:@"medical"])
        {
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PooWeeMedStoryboard" bundle:nil];
            UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"ActivityMedicalViewController"];
            viewController.data = data;
            viewController.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:viewController animated:YES];
        }
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([tableView isEqual:self.dayTableView])
    {
        if(dayList.count == 0)
        {
            EmptyTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"EmptyTableViewCell"];
            if(!cell)
            {
                [tableView registerNib:[UINib nibWithNibName:@"EmptyTableViewCell" bundle:nil] forCellReuseIdentifier:@"EmptyTableViewCell"];
                cell = [tableView dequeueReusableCellWithIdentifier:@"EmptyTableViewCell"];
            }
            
            return cell;
        }
        else
        {
            TimelineDayTableViewCell *cell = [self.dayTableView dequeueReusableCellWithIdentifier:@"TimelineDayTableViewCell"];
            if(!cell)
            {
                [self.dayTableView registerNib:[UINib nibWithNibName:@"TimelineDayTableViewCell" bundle:nil] forCellReuseIdentifier:@"TimelineDayTableViewCell"];
                cell = [self.dayTableView dequeueReusableCellWithIdentifier:@"TimelineDayTableViewCell"];
            }
            
            NSString *name = [[[[dayList objectAtIndex:indexPath.section] objectForKey:@"list"] objectAtIndex:indexPath.row] objectForKey:@"name"];
            
            if([name isEqualToString:@"feed"])
            {
                cell.imgIcon.backgroundColor = UIColorFromRGB(0xF4EA9E);
                cell.imgIconChevronRight.tintColor = UIColorFromRGB(0xF4EA9E);
            }
            else if([name isEqualToString:@"wee"])
            {
                cell.imgIcon.backgroundColor = UIColorFromRGB(0xCADAA9);
                cell.imgIconChevronRight.tintColor = UIColorFromRGB(0xCADAA9);
            }
            else if([name isEqualToString:@"poo"])
            {
                cell.imgIcon.backgroundColor = UIColorFromRGB(0xE0C5B8);
                cell.imgIconChevronRight.tintColor = UIColorFromRGB(0xE0C5B8);
            }
            else if([name isEqualToString:@"pump"])
            {
                cell.imgIcon.backgroundColor = UIColorFromRGB(0xF9C99B);
                cell.imgIconChevronRight.tintColor = UIColorFromRGB(0xF9C99B);
            }
            else if([name isEqualToString:@"sleep"])
            {
                cell.imgIcon.backgroundColor = UIColorFromRGB(0xC5D7E1);
                cell.imgIconChevronRight.tintColor = UIColorFromRGB(0xC5D7E1);
            }
            else if([name isEqualToString:@"medical"])
            {
                cell.imgIcon.backgroundColor = UIColorFromRGB(0xF4D2D5);
                cell.imgIconChevronRight.tintColor = UIColorFromRGB(0xF4D2D5);
            }
            
            UIImage *imgIcon = [UIImage imageNamed:[NSString stringWithFormat:@"icon_%@.png", name]];
            cell.imgIconWidthConstraint.constant = imgIcon.size.width;
            cell.imgIconHeightConstraint.constant = imgIcon.size.height;
            if (IS_IPAD) {
                cell.imgIconWidthConstraint.constant = cell.imgIconWidthConstraint.constant + 12;
                cell.imgIconHeightConstraint.constant = cell.imgIconHeightConstraint.constant + 12;
            }
            cell.imgIcon.image = imgIcon;
            cell.imgIcon.layer.cornerRadius = cell.imgIconWidthConstraint.constant/2;
            
            cell.txtValue.text = @"";
            if (![[[[[dayList objectAtIndex:indexPath.section] objectForKey:@"list"] objectAtIndex:indexPath.row] objectForKey:@"attribute_value"] isKindOfClass:[NSNull class]]) {
                cell.txtValue.text = [[[[dayList objectAtIndex:indexPath.section] objectForKey:@"list"] objectAtIndex:indexPath.row] objectForKey:@"attribute_value"];
            }
            
            cell.txtName.text = @"";
            if (![[[[[dayList objectAtIndex:indexPath.section] objectForKey:@"list"] objectAtIndex:indexPath.row] objectForKey:@"attribute_name"] isKindOfClass:[NSNull class]]) {
                cell.txtName.text = [[[[dayList objectAtIndex:indexPath.section] objectForKey:@"list"] objectAtIndex:indexPath.row] objectForKey:@"attribute_name"];
            }
            
            cell.txtNotes.text = @"";
            if (![[[[[dayList objectAtIndex:indexPath.section] objectForKey:@"list"] objectAtIndex:indexPath.row] objectForKey:@"notes"] isKindOfClass:[NSNull class]]) {
                cell.txtNotes.text = [[[[dayList objectAtIndex:indexPath.section] objectForKey:@"list"] objectAtIndex:indexPath.row] objectForKey:@"notes"];
            }

            CGFloat timeFontSize = 20.0f;
            CGFloat timeSuffixFontSize = 12.0f;
            if (IS_IPAD) {
                timeFontSize = 26.0;
                timeSuffixFontSize = 18.0;
            }

            NSString *time = [[[[dayList objectAtIndex:indexPath.section] objectForKey:@"list"] objectAtIndex:indexPath.row] objectForKey:@"time"];
            NSMutableAttributedString *timeString = [[NSMutableAttributedString alloc] initWithString:time attributes:@{NSFontAttributeName:[UIFont fontWithName:@"SFUIDisplay-Medium" size:timeFontSize]}];
            
            if([[shareObject objectForKey:@"language"] hasPrefix:@"zh"])
            {
                NSArray *timeArray = [time componentsSeparatedByString:@" "];
                NSLog(@"timeArray=%@", timeArray);
                time = [NSString stringWithFormat:@"%@ %@", LocalizedString(timeArray[1], nil), timeArray[0]];
                timeString = [[NSMutableAttributedString alloc] initWithString:time attributes:@{NSFontAttributeName:[UIFont fontWithName:@"SFUIDisplay-Medium" size:timeFontSize]}];
                [timeString setAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"SFUIDisplay-Medium" size:timeSuffixFontSize], NSBaselineOffsetAttributeName:@(4)} range:NSMakeRange(0,2)];
            }
            else
            {
                [timeString setAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"SFUIDisplay-Medium" size:timeSuffixFontSize]} range:NSMakeRange(6,2)];
            }
            [cell.txtTime setAttributedText:timeString];
            
            return cell;
        }
    }
    else if([tableView isEqual:self.weekTableView])
    {
        if(weekList.count == 0)
        {
            EmptyTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"EmptyTableViewCell"];
            if(!cell)
            {
                [tableView registerNib:[UINib nibWithNibName:@"EmptyTableViewCell" bundle:nil] forCellReuseIdentifier:@"EmptyTableViewCell"];
                cell = [tableView dequeueReusableCellWithIdentifier:@"EmptyTableViewCell"];
            }
            
            return cell;
        }
        else
        {
            NSString *name = [[[[weekList objectAtIndex:indexPath.section] objectForKey:@"list"] objectAtIndex:indexPath.row] objectForKey:@"name"];
            
            if([name isEqualToString:@"feed"])
            {
                TimelineWeekMonthFeedTableViewCell *cell = [self.weekTableView dequeueReusableCellWithIdentifier:@"TimelineWeekMonthFeedTableViewCell"];
                if(!cell)
                {
                    [self.weekTableView registerNib:[UINib nibWithNibName:@"TimelineWeekMonthFeedTableViewCell" bundle:nil] forCellReuseIdentifier:@"TimelineWeekMonthFeedTableViewCell"];
                    cell = [self.weekTableView dequeueReusableCellWithIdentifier:@"TimelineWeekMonthFeedTableViewCell"];
                }
                
                cell.imgIcon.backgroundColor = UIColorFromRGB(0xF4EA9E);
                cell.imgIconChevronRight.tintColor = UIColorFromRGB(0xF4EA9E);
                
                UIImage *imgIcon = [UIImage imageNamed:[NSString stringWithFormat:@"icon_%@.png", name]];
                cell.imgIconWidthConstraint.constant = imgIcon.size.width;
                cell.imgIconHeightConstraint.constant = imgIcon.size.height;
                if (IS_IPAD) {
                    cell.imgIconWidthConstraint.constant = cell.imgIconWidthConstraint.constant + 12;
                    cell.imgIconHeightConstraint.constant = cell.imgIconHeightConstraint.constant + 12;
                }
                cell.imgIcon.image = imgIcon;
                cell.imgIcon.layer.cornerRadius = cell.imgIconWidthConstraint.constant/2;
                cell.imgIcon.clipsToBounds = YES;
                
                cell.txtValue1.text = [NSString stringWithFormat:@"%@", [[[[weekList objectAtIndex:indexPath.section] objectForKey:@"list"] objectAtIndex:indexPath.row] objectForKey:@"attribute_value1"]];
                cell.txtValue2.text = [NSString stringWithFormat:@"%@", [[[[weekList objectAtIndex:indexPath.section] objectForKey:@"list"] objectAtIndex:indexPath.row] objectForKey:@"attribute_value2"]];
                cell.txtValue3.text = [NSString stringWithFormat:@"%@", [[[[weekList objectAtIndex:indexPath.section] objectForKey:@"list"] objectAtIndex:indexPath.row] objectForKey:@"attribute_value3"]];
                cell.txtValue4.text = [NSString stringWithFormat:@"%@", [[[[weekList objectAtIndex:indexPath.section] objectForKey:@"list"] objectAtIndex:indexPath.row] objectForKey:@"attribute_value4"]];
                cell.txtName1.text = [NSString stringWithFormat:@"%@", [[[[weekList objectAtIndex:indexPath.section] objectForKey:@"list"] objectAtIndex:indexPath.row] objectForKey:@"attribute_name1"]];
                cell.txtName2.text = [NSString stringWithFormat:@"%@", [[[[weekList objectAtIndex:indexPath.section] objectForKey:@"list"] objectAtIndex:indexPath.row] objectForKey:@"attribute_name2"]];
                cell.txtName3.text = [NSString stringWithFormat:@"%@", [[[[weekList objectAtIndex:indexPath.section] objectForKey:@"list"] objectAtIndex:indexPath.row] objectForKey:@"attribute_name3"]];
                cell.txtName4.text = [NSString stringWithFormat:@"%@", [[[[weekList objectAtIndex:indexPath.section] objectForKey:@"list"] objectAtIndex:indexPath.row] objectForKey:@"attribute_name4"]];
                
                return cell;
            }
            else if ([name isEqualToString:@"poo"] || [name isEqualToString:@"wee"])
            {
                TimelineWeekMonthPooTableViewCell *cell = [self.weekTableView dequeueReusableCellWithIdentifier:@"TimelineWeekMonthPooTableViewCell"];
                if(!cell)
                {
                    [self.weekTableView registerNib:[UINib nibWithNibName:@"TimelineWeekMonthPooTableViewCell" bundle:nil] forCellReuseIdentifier:@"TimelineWeekMonthPooTableViewCell"];
                    cell = [self.weekTableView dequeueReusableCellWithIdentifier:@"TimelineWeekMonthPooTableViewCell"];
                }
                
                NSString *avgColor = @"white";
                
                if ([name isEqualToString:@"poo"])
                {
                    cell.imgIcon.backgroundColor = UIColorFromRGB(0xE0C5B8);
                    cell.imgIconChevronRight.tintColor = UIColorFromRGB(0xE0C5B8);
                    
                    UIImage *imgIcon = [UIImage imageNamed:[NSString stringWithFormat:@"icon_%@.png", name]];
                    cell.imgIconWidthConstraint.constant = imgIcon.size.width;
                    cell.imgIconHeightConstraint.constant = imgIcon.size.height;
                    if (IS_IPAD) {
                        cell.imgIconWidthConstraint.constant = cell.imgIconWidthConstraint.constant + 12;
                        cell.imgIconHeightConstraint.constant = cell.imgIconHeightConstraint.constant + 12;
                    }
                    cell.imgIcon.image = imgIcon;
                    cell.imgIcon.layer.cornerRadius = cell.imgIconWidthConstraint.constant/2;
                    cell.imgIcon.clipsToBounds = YES;
                    
                    avgColor = [NSString stringWithFormat:@"%@", [[[[weekList objectAtIndex:indexPath.section] objectForKey:@"list"] objectAtIndex:indexPath.row] objectForKey:@"attribute_value1"]];

                    cell.txtValue2.text = [[NSString stringWithFormat:@"%@", [[[[weekList objectAtIndex:indexPath.section] objectForKey:@"list"] objectAtIndex:indexPath.row] objectForKey:@"attribute_value2"]] capitalizedString];
                    cell.txtValue3.text = [[NSString stringWithFormat:@"%@", [[[[weekList objectAtIndex:indexPath.section] objectForKey:@"list"] objectAtIndex:indexPath.row] objectForKey:@"attribute_value3"]] capitalizedString];
                    cell.txtValue4.text = [[NSString stringWithFormat:@"%@", [[[[weekList objectAtIndex:indexPath.section] objectForKey:@"list"] objectAtIndex:indexPath.row] objectForKey:@"attribute_value4"]] capitalizedString];
                    
                    cell.txtName1.text = [NSString stringWithFormat:@"%@", [[[[weekList objectAtIndex:indexPath.section] objectForKey:@"list"] objectAtIndex:indexPath.row] objectForKey:@"attribute_name1"]];
                    cell.txtName2.text = [NSString stringWithFormat:@"%@", [[[[weekList objectAtIndex:indexPath.section] objectForKey:@"list"] objectAtIndex:indexPath.row] objectForKey:@"attribute_name2"]];
                    cell.txtName3.text = [NSString stringWithFormat:@"%@", [[[[weekList objectAtIndex:indexPath.section] objectForKey:@"list"] objectAtIndex:indexPath.row] objectForKey:@"attribute_name3"]];
                    cell.txtName4.text = [NSString stringWithFormat:@"%@", [[[[weekList objectAtIndex:indexPath.section] objectForKey:@"list"] objectAtIndex:indexPath.row] objectForKey:@"attribute_name4"]];
                    
                    cell.txtValue4.hidden = NO;
                    cell.txtName4.hidden = NO;
                    
                }
                else if ([name isEqualToString:@"wee"])
                {
                    cell.imgIcon.backgroundColor = UIColorFromRGB(0xCADAA9);
                    cell.imgIconChevronRight.tintColor = UIColorFromRGB(0xCADAA9);
                    
                    UIImage *imgIcon = [UIImage imageNamed:[NSString stringWithFormat:@"icon_%@.png", name]];
                    cell.imgIconWidthConstraint.constant = imgIcon.size.width;
                    cell.imgIconHeightConstraint.constant = imgIcon.size.height;
                    if (IS_IPAD) {
                        cell.imgIconWidthConstraint.constant = cell.imgIconWidthConstraint.constant + 12;
                        cell.imgIconHeightConstraint.constant = cell.imgIconHeightConstraint.constant + 12;
                    }
                    cell.imgIcon.image = imgIcon;
                    cell.imgIcon.layer.cornerRadius = cell.imgIconWidthConstraint.constant/2;
                    cell.imgIcon.clipsToBounds = YES;
                    
                    avgColor = [NSString stringWithFormat:@"%@", [[[[weekList objectAtIndex:indexPath.section] objectForKey:@"list"] objectAtIndex:indexPath.row] objectForKey:@"attribute_value1"]];

                    cell.txtValue2.text = [[NSString stringWithFormat:@"%@", [[[[weekList objectAtIndex:indexPath.section] objectForKey:@"list"] objectAtIndex:indexPath.row] objectForKey:@"attribute_value2"]] capitalizedString];
                    cell.txtValue3.text = [[NSString stringWithFormat:@"%@", [[[[weekList objectAtIndex:indexPath.section] objectForKey:@"list"] objectAtIndex:indexPath.row] objectForKey:@"attribute_value3"]] capitalizedString];
                    cell.txtValue4.hidden = YES;
                    
                    cell.txtName1.text = [NSString stringWithFormat:@"%@", [[[[weekList objectAtIndex:indexPath.section] objectForKey:@"list"] objectAtIndex:indexPath.row] objectForKey:@"attribute_name1"]];
                    cell.txtName2.text = [NSString stringWithFormat:@"%@", [[[[weekList objectAtIndex:indexPath.section] objectForKey:@"list"] objectAtIndex:indexPath.row] objectForKey:@"attribute_name2"]];
                    cell.txtName3.text = [NSString stringWithFormat:@"%@", [[[[weekList objectAtIndex:indexPath.section] objectForKey:@"list"] objectAtIndex:indexPath.row] objectForKey:@"attribute_name3"]];
                    cell.txtName4.hidden = YES;
                }
                
                if ([avgColor isEqualToString:@"yellow"]) {
                    cell.circularImg.backgroundColor = UIColorFromRGB(0xFDE086);
                    cell.circularImg.layer.borderColor = UIColorFromRGB(0xFDE086).CGColor;
                }else if ([avgColor isEqualToString:@"lightyellow"]) {
                    cell.circularImg.backgroundColor = UIColorFromRGB(0xF5EBB8);
                    cell.circularImg.layer.borderColor = UIColorFromRGB(0xF5EBB8).CGColor;
                }else if ([avgColor isEqualToString:@"brown"]) {
                    cell.circularImg.backgroundColor = UIColorFromRGB(0xBE822B);
                    cell.circularImg.layer.borderColor = UIColorFromRGB(0xBE822B).CGColor;
                }else if ([avgColor isEqualToString:@"darkbrown"]) {
                    cell.circularImg.backgroundColor = UIColorFromRGB(0x482C00);
                    cell.circularImg.layer.borderColor = UIColorFromRGB(0x482C00).CGColor;
                }else if ([avgColor isEqualToString:@"green"]) {
                    cell.circularImg.backgroundColor = UIColorFromRGB(0xAEAD76);
                    cell.circularImg.layer.borderColor = UIColorFromRGB(0xAEAD76).CGColor;
                }else if ([avgColor isEqualToString:@"black"]) {
                    cell.circularImg.backgroundColor = UIColorFromRGB(0x4A4B4C);
                    cell.circularImg.layer.borderColor = UIColorFromRGB(0x4A4B4C).CGColor;
                }else if ([avgColor isEqualToString:@"white"] || [avgColor isEqualToString:@"clear"]) {
                    cell.circularImg.backgroundColor = UIColorFromRGB(0xF7F6F5);
                    cell.circularImg.layer.borderColor = UIColorFromRGB(0xD3D3D3).CGColor;
                }else if ([avgColor isEqualToString:@"orange"]) {
                    cell.circularImg.backgroundColor = UIColorFromRGB(0xF49B5C);
                    cell.circularImg.layer.borderColor = UIColorFromRGB(0xF49B5C).CGColor;
                }else if ([avgColor isEqualToString:@"alert"]) {
                    cell.circularImg.backgroundColor = UIColorFromRGB(0xF07861);
                    cell.circularImg.layer.borderColor = UIColorFromRGB(0xF07861).CGColor;
                }
                
                return cell;
            }
            else if ([name isEqualToString:@"sleep"] || [name isEqualToString:@"medical"])
            {
                TimelineWeekMonthTableViewCell *cell = [self.weekTableView dequeueReusableCellWithIdentifier:@"TimelineWeekMonthTableViewCell"];
                if(!cell)
                {
                    [self.weekTableView registerNib:[UINib nibWithNibName:@"TimelineWeekMonthTableViewCell" bundle:nil] forCellReuseIdentifier:@"TimelineWeekMonthTableViewCell"];
                    cell = [self.weekTableView dequeueReusableCellWithIdentifier:@"TimelineWeekMonthTableViewCell"];
                }

                if([name isEqualToString:@"sleep"])
                {
                    cell.imgIcon.backgroundColor = UIColorFromRGB(0xC5D7E1);
                    cell.imgIconChevronRight.tintColor = UIColorFromRGB(0xC5D7E1);
                    cell.txtValue2.hidden = cell.txtName2.hidden = NO;
                }
                else if([name isEqualToString:@"medical"])
                {
                    cell.imgIcon.backgroundColor = UIColorFromRGB(0xF4D2D5);
                    cell.imgIconChevronRight.tintColor = UIColorFromRGB(0xF4D2D5);
                    
                    if([[[[[weekList objectAtIndex:indexPath.section] objectForKey:@"list"] objectAtIndex:indexPath.row] objectForKey:@"attribute_value2"] intValue] == 0)
                    {
                        cell.txtValue2.hidden = cell.txtName2.hidden = YES;
                    }
                    else
                    {
                        cell.txtValue2.hidden = cell.txtName2.hidden = NO;
                    }
                }

                UIImage *imgIcon = [UIImage imageNamed:[NSString stringWithFormat:@"icon_%@.png", name]];
                cell.imgIconWidthConstraint.constant = imgIcon.size.width;
                cell.imgIconHeightConstraint.constant = imgIcon.size.height;
                if (IS_IPAD) {
                    cell.imgIconWidthConstraint.constant = cell.imgIconWidthConstraint.constant + 12;
                    cell.imgIconHeightConstraint.constant = cell.imgIconHeightConstraint.constant + 12;
                }
                cell.imgIcon.image = imgIcon;
                cell.imgIcon.layer.cornerRadius = cell.imgIconWidthConstraint.constant/2;
                cell.imgIcon.clipsToBounds = YES;
                
                cell.txtValue1.text = [NSString stringWithFormat:@"%@", [[[[weekList objectAtIndex:indexPath.section] objectForKey:@"list"] objectAtIndex:indexPath.row] objectForKey:@"attribute_value1"]];
                cell.txtValue2.text = [NSString stringWithFormat:@"%@", [[[[weekList objectAtIndex:indexPath.section] objectForKey:@"list"] objectAtIndex:indexPath.row] objectForKey:@"attribute_value2"]];
                cell.txtName1.text = [NSString stringWithFormat:@"%@", [[[[weekList objectAtIndex:indexPath.section] objectForKey:@"list"] objectAtIndex:indexPath.row] objectForKey:@"attribute_name1"]];
                cell.txtName2.text = [NSString stringWithFormat:@"%@", [[[[weekList objectAtIndex:indexPath.section] objectForKey:@"list"] objectAtIndex:indexPath.row] objectForKey:@"attribute_name2"]];

                return cell;
            }
            else
            {
                // For Pump
                TimelineWeekMonthPumpTableViewCell *cell = [self.weekTableView dequeueReusableCellWithIdentifier:@"TimelineWeekMonthPumpTableViewCell"];
                if(!cell)
                {
                    [self.weekTableView registerNib:[UINib nibWithNibName:@"TimelineWeekMonthPumpTableViewCell" bundle:nil] forCellReuseIdentifier:@"TimelineWeekMonthPumpTableViewCell"];
                    cell = [self.weekTableView dequeueReusableCellWithIdentifier:@"TimelineWeekMonthPumpTableViewCell"];
                }
                
                cell.imgIcon.backgroundColor = UIColorFromRGB(0xF9C99B);
                cell.imgIconChevronRight.tintColor = UIColorFromRGB(0xF9C99B);
                cell.txtValue2.hidden = cell.txtName2.hidden = NO;
                
                UIImage *imgIcon = [UIImage imageNamed:[NSString stringWithFormat:@"icon_%@.png", name]];
                cell.imgIconWidthConstraint.constant = imgIcon.size.width;
                cell.imgIconHeightConstraint.constant = imgIcon.size.height;
                if (IS_IPAD) {
                    cell.imgIconWidthConstraint.constant = cell.imgIconWidthConstraint.constant + 12;
                    cell.imgIconHeightConstraint.constant = cell.imgIconHeightConstraint.constant + 12;
                }
                cell.imgIcon.image = imgIcon;
                cell.imgIcon.layer.cornerRadius = cell.imgIconWidthConstraint.constant/2;
                cell.imgIcon.clipsToBounds = YES;
                
                cell.txtValue1.text = [NSString stringWithFormat:@"%@", [[[[weekList objectAtIndex:indexPath.section] objectForKey:@"list"] objectAtIndex:indexPath.row] objectForKey:@"attribute_value1"]];
                cell.txtValue2.text = [NSString stringWithFormat:@"%@", [[[[weekList objectAtIndex:indexPath.section] objectForKey:@"list"] objectAtIndex:indexPath.row] objectForKey:@"attribute_value2"]];
                cell.txtValue3.text = [NSString stringWithFormat:@"%@", [[[[weekList objectAtIndex:indexPath.section] objectForKey:@"list"] objectAtIndex:indexPath.row] objectForKey:@"attribute_value3"]];

                cell.txtName1.text = [NSString stringWithFormat:@"%@", [[[[weekList objectAtIndex:indexPath.section] objectForKey:@"list"] objectAtIndex:indexPath.row] objectForKey:@"attribute_name1"]];
                cell.txtName2.text = [NSString stringWithFormat:@"%@", [[[[weekList objectAtIndex:indexPath.section] objectForKey:@"list"] objectAtIndex:indexPath.row] objectForKey:@"attribute_name2"]];
                cell.txtName3.text = [NSString stringWithFormat:@"%@", [[[[weekList objectAtIndex:indexPath.section] objectForKey:@"list"] objectAtIndex:indexPath.row] objectForKey:@"attribute_name3"]];

                return cell;
            }
        }
    }
    else
    {
        if(monthList.count == 0)
        {
            EmptyTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"EmptyTableViewCell"];
            if(!cell)
            {
                [tableView registerNib:[UINib nibWithNibName:@"EmptyTableViewCell" bundle:nil] forCellReuseIdentifier:@"EmptyTableViewCell"];
                cell = [tableView dequeueReusableCellWithIdentifier:@"EmptyTableViewCell"];
            }
            
            return cell;
        }
        else
        {
            NSString *name = [[[[monthList objectAtIndex:indexPath.section] objectForKey:@"list"] objectAtIndex:indexPath.row] objectForKey:@"name"];
            
            if([name isEqualToString:@"feed"])
            {
                TimelineWeekMonthFeedTableViewCell *cell = [self.monthTableView dequeueReusableCellWithIdentifier:@"TimelineWeekMonthFeedTableViewCell"];
                if(!cell)
                {
                    [self.monthTableView registerNib:[UINib nibWithNibName:@"TimelineWeekMonthFeedTableViewCell" bundle:nil] forCellReuseIdentifier:@"TimelineWeekMonthFeedTableViewCell"];
                    cell = [self.monthTableView dequeueReusableCellWithIdentifier:@"TimelineWeekMonthFeedTableViewCell"];
                }
                
                cell.imgIcon.backgroundColor = UIColorFromRGB(0xF4EA9E);
                cell.imgIconChevronRight.tintColor = UIColorFromRGB(0xF4EA9E);
                
                UIImage *imgIcon = [UIImage imageNamed:[NSString stringWithFormat:@"icon_%@.png", name]];
                cell.imgIconWidthConstraint.constant = imgIcon.size.width;
                cell.imgIconHeightConstraint.constant = imgIcon.size.height;
                if (IS_IPAD) {
                    cell.imgIconWidthConstraint.constant = cell.imgIconWidthConstraint.constant + 12;
                    cell.imgIconHeightConstraint.constant = cell.imgIconHeightConstraint.constant + 12;
                }
                cell.imgIcon.image = imgIcon;
                cell.imgIcon.layer.cornerRadius = cell.imgIconWidthConstraint.constant/2;
                cell.imgIcon.clipsToBounds = YES;
                
                cell.txtValue1.text = [NSString stringWithFormat:@"%@", [[[[monthList objectAtIndex:indexPath.section] objectForKey:@"list"] objectAtIndex:indexPath.row] objectForKey:@"attribute_value1"]];
                cell.txtValue2.text = [NSString stringWithFormat:@"%@", [[[[monthList objectAtIndex:indexPath.section] objectForKey:@"list"] objectAtIndex:indexPath.row] objectForKey:@"attribute_value2"]];
                cell.txtValue3.text = [NSString stringWithFormat:@"%@", [[[[monthList objectAtIndex:indexPath.section] objectForKey:@"list"] objectAtIndex:indexPath.row] objectForKey:@"attribute_value3"]];
                cell.txtValue4.text = [NSString stringWithFormat:@"%@", [[[[monthList objectAtIndex:indexPath.section] objectForKey:@"list"] objectAtIndex:indexPath.row] objectForKey:@"attribute_value4"]];
                
                cell.txtName1.text = [NSString stringWithFormat:@"%@", [[[[monthList objectAtIndex:indexPath.section] objectForKey:@"list"] objectAtIndex:indexPath.row] objectForKey:@"attribute_name1"]];
                cell.txtName2.text = [NSString stringWithFormat:@"%@", [[[[monthList objectAtIndex:indexPath.section] objectForKey:@"list"] objectAtIndex:indexPath.row] objectForKey:@"attribute_name2"]];
                cell.txtName3.text = [NSString stringWithFormat:@"%@", [[[[monthList objectAtIndex:indexPath.section] objectForKey:@"list"] objectAtIndex:indexPath.row] objectForKey:@"attribute_name3"]];
                cell.txtName4.text = [NSString stringWithFormat:@"%@", [[[[monthList objectAtIndex:indexPath.section] objectForKey:@"list"] objectAtIndex:indexPath.row] objectForKey:@"attribute_name4"]];
                
                return cell;
            }
            else if ([name isEqualToString:@"poo"] || [name isEqualToString:@"wee"])
            {
                TimelineWeekMonthPooTableViewCell *cell = [self.monthTableView dequeueReusableCellWithIdentifier:@"TimelineWeekMonthPooTableViewCell"];
                if(!cell)
                {
                    [self.monthTableView registerNib:[UINib nibWithNibName:@"TimelineWeekMonthPooTableViewCell" bundle:nil] forCellReuseIdentifier:@"TimelineWeekMonthPooTableViewCell"];
                    cell = [self.monthTableView dequeueReusableCellWithIdentifier:@"TimelineWeekMonthPooTableViewCell"];
                }
                
                NSString *avgColor = @"white";
                
                if([name isEqualToString:@"poo"])
                {
                    cell.imgIcon.backgroundColor = UIColorFromRGB(0xE0C5B8);
                    cell.imgIconChevronRight.tintColor = UIColorFromRGB(0xE0C5B8);
                    
                    UIImage *imgIcon = [UIImage imageNamed:[NSString stringWithFormat:@"icon_%@.png", name]];
                    cell.imgIconWidthConstraint.constant = imgIcon.size.width;
                    cell.imgIconHeightConstraint.constant = imgIcon.size.height;
                    if (IS_IPAD) {
                        cell.imgIconWidthConstraint.constant = cell.imgIconWidthConstraint.constant + 12;
                        cell.imgIconHeightConstraint.constant = cell.imgIconHeightConstraint.constant + 12;
                    }
                    cell.imgIcon.image = imgIcon;
                    cell.imgIcon.layer.cornerRadius = cell.imgIconWidthConstraint.constant/2;
                    cell.imgIcon.clipsToBounds = YES;
                    
                    avgColor = [NSString stringWithFormat:@"%@", [[[[monthList objectAtIndex:indexPath.section] objectForKey:@"list"] objectAtIndex:indexPath.row] objectForKey:@"attribute_value1"]];
                    
                    cell.txtValue2.text = [[NSString stringWithFormat:@"%@", [[[[monthList objectAtIndex:indexPath.section] objectForKey:@"list"] objectAtIndex:indexPath.row] objectForKey:@"attribute_value2"]] capitalizedString];
                    cell.txtValue3.text = [[NSString stringWithFormat:@"%@", [[[[monthList objectAtIndex:indexPath.section] objectForKey:@"list"] objectAtIndex:indexPath.row] objectForKey:@"attribute_value3"]] capitalizedString];
                    cell.txtValue4.text = [[NSString stringWithFormat:@"%@", [[[[monthList objectAtIndex:indexPath.section] objectForKey:@"list"] objectAtIndex:indexPath.row] objectForKey:@"attribute_value4"]] capitalizedString];

                    cell.txtName1.text = [NSString stringWithFormat:@"%@", [[[[monthList objectAtIndex:indexPath.section] objectForKey:@"list"] objectAtIndex:indexPath.row] objectForKey:@"attribute_name1"]];
                    cell.txtName2.text = [NSString stringWithFormat:@"%@", [[[[monthList objectAtIndex:indexPath.section] objectForKey:@"list"] objectAtIndex:indexPath.row] objectForKey:@"attribute_name2"]];
                    cell.txtName3.text = [NSString stringWithFormat:@"%@", [[[[monthList objectAtIndex:indexPath.section] objectForKey:@"list"] objectAtIndex:indexPath.row] objectForKey:@"attribute_name3"]];
                    cell.txtName4.text = [NSString stringWithFormat:@"%@", [[[[monthList objectAtIndex:indexPath.section] objectForKey:@"list"] objectAtIndex:indexPath.row] objectForKey:@"attribute_name4"]];

                    cell.txtValue4.hidden = NO;
                    cell.txtName4.hidden = NO;
                    
                }
                else if([name isEqualToString:@"wee"])
                {
                    cell.imgIcon.backgroundColor = UIColorFromRGB(0xCADAA9);
                    cell.imgIconChevronRight.tintColor = UIColorFromRGB(0xCADAA9);
                    
                    UIImage *imgIcon = [UIImage imageNamed:[NSString stringWithFormat:@"icon_%@.png", name]];
                    cell.imgIconWidthConstraint.constant = imgIcon.size.width;
                    cell.imgIconHeightConstraint.constant = imgIcon.size.height;
                    if (IS_IPAD) {
                        cell.imgIconWidthConstraint.constant = cell.imgIconWidthConstraint.constant + 12;
                        cell.imgIconHeightConstraint.constant = cell.imgIconHeightConstraint.constant + 12;
                    }
                    cell.imgIcon.image = imgIcon;
                    cell.imgIcon.layer.cornerRadius = cell.imgIconWidthConstraint.constant/2;
                    cell.imgIcon.clipsToBounds = YES;
                    
                    avgColor = [NSString stringWithFormat:@"%@", [[[[monthList objectAtIndex:indexPath.section] objectForKey:@"list"] objectAtIndex:indexPath.row] objectForKey:@"attribute_value1"]];

                    cell.txtValue2.text = [[NSString stringWithFormat:@"%@", [[[[monthList objectAtIndex:indexPath.section] objectForKey:@"list"] objectAtIndex:indexPath.row] objectForKey:@"attribute_value2"]] capitalizedString];
                    cell.txtValue3.text = [[NSString stringWithFormat:@"%@", [[[[monthList objectAtIndex:indexPath.section] objectForKey:@"list"] objectAtIndex:indexPath.row] objectForKey:@"attribute_value3"]] capitalizedString];
                    cell.txtValue4.hidden = YES;
                    
                    cell.txtName1.text = [NSString stringWithFormat:@"%@", [[[[monthList objectAtIndex:indexPath.section] objectForKey:@"list"] objectAtIndex:indexPath.row] objectForKey:@"attribute_name1"]];
                    cell.txtName2.text = [NSString stringWithFormat:@"%@", [[[[monthList objectAtIndex:indexPath.section] objectForKey:@"list"] objectAtIndex:indexPath.row] objectForKey:@"attribute_name2"]];
                    cell.txtName3.text = [NSString stringWithFormat:@"%@", [[[[monthList objectAtIndex:indexPath.section] objectForKey:@"list"] objectAtIndex:indexPath.row] objectForKey:@"attribute_name3"]];
                    cell.txtName4.hidden = YES;

                }
                
                if ([avgColor isEqualToString:@"yellow"]) {
                    cell.circularImg.backgroundColor = UIColorFromRGB(0xFDE086);
                    cell.circularImg.layer.borderColor = UIColorFromRGB(0xFDE086).CGColor;
                }else if ([avgColor isEqualToString:@"lightyellow"]) {
                    cell.circularImg.backgroundColor = UIColorFromRGB(0xF5EBB8);
                    cell.circularImg.layer.borderColor = UIColorFromRGB(0xF5EBB8).CGColor;
                }else if ([avgColor isEqualToString:@"brown"]) {
                    cell.circularImg.backgroundColor = UIColorFromRGB(0xBE822B);
                    cell.circularImg.layer.borderColor = UIColorFromRGB(0xBE822B).CGColor;
                }else if ([avgColor isEqualToString:@"darkbrown"]) {
                    cell.circularImg.backgroundColor = UIColorFromRGB(0x482C00);
                    cell.circularImg.layer.borderColor = UIColorFromRGB(0x482C00).CGColor;
                }else if ([avgColor isEqualToString:@"green"]) {
                    cell.circularImg.backgroundColor = UIColorFromRGB(0xAEAD76);
                    cell.circularImg.layer.borderColor = UIColorFromRGB(0xAEAD76).CGColor;
                }else if ([avgColor isEqualToString:@"black"]) {
                    cell.circularImg.backgroundColor = UIColorFromRGB(0x4A4B4C);
                    cell.circularImg.layer.borderColor = UIColorFromRGB(0x4A4B4C).CGColor;
                }else if ([avgColor isEqualToString:@"white"] || [avgColor isEqualToString:@"clear"]) {
                    cell.circularImg.backgroundColor = UIColorFromRGB(0xF7F6F5);
                    cell.circularImg.layer.borderColor = UIColorFromRGB(0xD3D3D3).CGColor;
                }else if ([avgColor isEqualToString:@"orange"]) {
                    cell.circularImg.backgroundColor = UIColorFromRGB(0xF49B5C);
                    cell.circularImg.layer.borderColor = UIColorFromRGB(0xF49B5C).CGColor;
                }else if ([avgColor isEqualToString:@"alert"]) {
                    cell.circularImg.backgroundColor = UIColorFromRGB(0xF07861);
                    cell.circularImg.layer.borderColor = UIColorFromRGB(0xF07861).CGColor;
                }
                
                return cell;
            }
            else if ([name isEqualToString:@"sleep"] || [name isEqualToString:@"medical"])
            {
                TimelineWeekMonthTableViewCell *cell = [self.monthTableView dequeueReusableCellWithIdentifier:@"TimelineWeekMonthTableViewCell"];
                if(!cell)
                {
                    [self.monthTableView registerNib:[UINib nibWithNibName:@"TimelineWeekMonthTableViewCell" bundle:nil] forCellReuseIdentifier:@"TimelineWeekMonthTableViewCell"];
                    cell = [self.monthTableView dequeueReusableCellWithIdentifier:@"TimelineWeekMonthTableViewCell"];
                }
                
                if([name isEqualToString:@"sleep"])
                {
                    cell.imgIcon.backgroundColor = UIColorFromRGB(0xC5D7E1);
                    cell.imgIconChevronRight.tintColor = UIColorFromRGB(0xC5D7E1);
                }
                else if([name isEqualToString:@"medical"])
                {
                    cell.imgIcon.backgroundColor = UIColorFromRGB(0xF4D2D5);
                    cell.imgIconChevronRight.tintColor = UIColorFromRGB(0xF4D2D5);
                    
                    if([[[[monthList objectAtIndex:indexPath.section] objectForKey:@"list"] objectAtIndex:indexPath.row] objectForKey:@"attribute_value2"] == 0)
                    {
                        cell.txtValue2.hidden = cell.txtName2.hidden = YES;
                    }
                    else
                    {
                        cell.txtValue2.hidden = cell.txtName2.hidden = NO;
                    }
                }
                
                UIImage *imgIcon = [UIImage imageNamed:[NSString stringWithFormat:@"icon_%@.png", name]];
                cell.imgIconWidthConstraint.constant = imgIcon.size.width;
                cell.imgIconHeightConstraint.constant = imgIcon.size.height;
                if (IS_IPAD) {
                    cell.imgIconWidthConstraint.constant = cell.imgIconWidthConstraint.constant + 12;
                    cell.imgIconHeightConstraint.constant = cell.imgIconHeightConstraint.constant + 12;
                }
                cell.imgIcon.image = imgIcon;
                cell.imgIcon.layer.cornerRadius = cell.imgIconWidthConstraint.constant/2;
                cell.imgIcon.clipsToBounds = YES;
                
                cell.txtValue1.text = [NSString stringWithFormat:@"%@", [[[[monthList objectAtIndex:indexPath.section] objectForKey:@"list"] objectAtIndex:indexPath.row] objectForKey:@"attribute_value1"]];
                cell.txtValue2.text = [NSString stringWithFormat:@"%@", [[[[monthList objectAtIndex:indexPath.section] objectForKey:@"list"] objectAtIndex:indexPath.row] objectForKey:@"attribute_value2"]];
                
                cell.txtName1.text = [NSString stringWithFormat:@"%@", [[[[monthList objectAtIndex:indexPath.section] objectForKey:@"list"] objectAtIndex:indexPath.row] objectForKey:@"attribute_name1"]];
                cell.txtName2.text = [NSString stringWithFormat:@"%@", [[[[monthList objectAtIndex:indexPath.section] objectForKey:@"list"] objectAtIndex:indexPath.row] objectForKey:@"attribute_name2"]];

                return cell;
            }
            else
            {
                //Pump
                TimelineWeekMonthPumpTableViewCell *cell = [self.monthTableView dequeueReusableCellWithIdentifier:@"TimelineWeekMonthPumpTableViewCell"];
                if(!cell)
                {
                    [self.monthTableView registerNib:[UINib nibWithNibName:@"TimelineWeekMonthPumpTableViewCell" bundle:nil] forCellReuseIdentifier:@"TimelineWeekMonthPumpTableViewCell"];
                    cell = [self.monthTableView dequeueReusableCellWithIdentifier:@"TimelineWeekMonthPumpTableViewCell"];
                }
                
                    cell.imgIcon.backgroundColor = UIColorFromRGB(0xF9C99B);
                    cell.imgIconChevronRight.tintColor = UIColorFromRGB(0xF9C99B);
                    cell.txtValue2.hidden = cell.txtName2.hidden = NO;
                
                UIImage *imgIcon = [UIImage imageNamed:[NSString stringWithFormat:@"icon_%@.png", name]];
                cell.imgIconWidthConstraint.constant = imgIcon.size.width;
                cell.imgIconHeightConstraint.constant = imgIcon.size.height;
                if (IS_IPAD) {
                    cell.imgIconWidthConstraint.constant = cell.imgIconWidthConstraint.constant + 12;
                    cell.imgIconHeightConstraint.constant = cell.imgIconHeightConstraint.constant + 12;
                }
                cell.imgIcon.image = imgIcon;
                cell.imgIcon.layer.cornerRadius = cell.imgIconWidthConstraint.constant/2;
                cell.imgIcon.clipsToBounds = YES;
                
                cell.txtValue1.text = [NSString stringWithFormat:@"%@", [[[[monthList objectAtIndex:indexPath.section] objectForKey:@"list"] objectAtIndex:indexPath.row] objectForKey:@"attribute_value1"]];
                cell.txtValue2.text = [NSString stringWithFormat:@"%@", [[[[monthList objectAtIndex:indexPath.section] objectForKey:@"list"] objectAtIndex:indexPath.row] objectForKey:@"attribute_value2"]];
                cell.txtValue3.text = [NSString stringWithFormat:@"%@", [[[[monthList objectAtIndex:indexPath.section] objectForKey:@"list"] objectAtIndex:indexPath.row] objectForKey:@"attribute_value3"]];

                cell.txtName1.text = [NSString stringWithFormat:@"%@", [[[[monthList objectAtIndex:indexPath.section] objectForKey:@"list"] objectAtIndex:indexPath.row] objectForKey:@"attribute_name1"]];
                cell.txtName2.text = [NSString stringWithFormat:@"%@", [[[[monthList objectAtIndex:indexPath.section] objectForKey:@"list"] objectAtIndex:indexPath.row] objectForKey:@"attribute_name2"]];
                cell.txtName3.text = [NSString stringWithFormat:@"%@", [[[[monthList objectAtIndex:indexPath.section] objectForKey:@"list"] objectAtIndex:indexPath.row] objectForKey:@"attribute_name3"]];

                return cell;
                
            }
        }
    }
}


#pragma mark -
#pragma mark UIScrollViewDelegate Methods
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if([scrollView isEqual:self.babyScrollView])
    {
        NSLog(@"scrollView.contentOffset.x=%f", scrollView.contentOffset.x );
        CGFloat pageWidth = self.view.bounds.size.width;
        int pageNo = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
        NSLog(@"pageNo=%d", pageNo);
        NSLog(@"backupPageNo=%d", backupPageNo);
        
        if(pageNo == 0)
        {
            NSLog(@"selectedBabyIndex2=%d", selectedBabyIndex);
            if(backupPageNo != pageNo)
            {
                int nextIndex = selectedBabyIndex;
                nextIndex = (nextIndex >= babyList.count)?0:nextIndex;
                selectedBabyIndex--;
                selectedBabyIndex = (selectedBabyIndex < 0)?(int)(babyList.count-1):selectedBabyIndex;
                int prevIndex = selectedBabyIndex-1;
                prevIndex = (prevIndex < 0)?(int)(babyList.count-1):prevIndex;
                
                NSLog(@"selectedBabyIndex=%d", selectedBabyIndex);
                NSLog(@"prevIndex=%d", prevIndex);
                NSLog(@"nextIndex=%d", nextIndex);
                
                
                [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"feed_info"]] forKey:@"feed_info"];
                [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"pump_info"]] forKey:@"pump_info"];
                [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"sleep_info"]] forKey:@"sleep_info"];
                [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"poo_info"]] forKey:@"poo_info"];
                [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"wee_info"]] forKey:@"wee_info"];
                [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"medical_info"]] forKey:@"medical_info"];
                [shareObject synchronize];
                
                [self.imgBabyPic1 sd_setImageWithURL:[NSURL URLWithString:[[babyList objectAtIndex:prevIndex] objectForKey:@"photo_url"]] placeholderImage:[UIImage imageNamed:@"dummy.png"]];
                self.txtName1.text = [[babyList objectAtIndex:prevIndex] objectForKey:@"name"];
                self.txtInfo1.text = [NSString stringWithFormat:LocalizedString(@"txt_baby_info", nil), [[babyList objectAtIndex:prevIndex] objectForKey:@"age"], [[babyList objectAtIndex:prevIndex] objectForKey:@"height"], [[babyList objectAtIndex:prevIndex] objectForKey:@"weight"], [[babyList objectAtIndex:prevIndex] objectForKey:@"blood_type"]];
                
                [self.imgBabyPic2 sd_setImageWithURL:[NSURL URLWithString:[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"photo_url"]] placeholderImage:[UIImage imageNamed:@"dummy.png"]];
                self.txtName2.text = [[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"name"];
                self.txtInfo2.text = [NSString stringWithFormat:LocalizedString(@"txt_baby_info", nil), [[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"age"], [[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"height"], [[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"weight"], [[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"blood_type"]];
                
                [self.imgBabyPic3 sd_setImageWithURL:[NSURL URLWithString:[[babyList objectAtIndex:nextIndex] objectForKey:@"photo_url"]] placeholderImage:[UIImage imageNamed:@"dummy.png"]];
                self.txtName3.text = [[babyList objectAtIndex:nextIndex] objectForKey:@"name"];
                self.txtInfo3.text = [NSString stringWithFormat:LocalizedString(@"txt_baby_info", nil), [[babyList objectAtIndex:nextIndex] objectForKey:@"age"], [[babyList objectAtIndex:nextIndex] objectForKey:@"height"], [[babyList objectAtIndex:nextIndex] objectForKey:@"weight"], [[babyList objectAtIndex:nextIndex] objectForKey:@"blood_type"]];
                
                self.babyScrollView.contentOffset = CGPointMake(self.view.frame.size.width, self.babyScrollView.contentOffset.y);
                backupPageNo = 1;
                
                isDayListInit = isWeekListInit = isMonthListInit = NO;
                [self refreshAction];
            }
        }
        else if(pageNo == 2)
        {
            if(backupPageNo != pageNo)
            {
                int prevIndex = selectedBabyIndex;
                prevIndex = (prevIndex < 0)?(int)(babyList.count-1):prevIndex;
                selectedBabyIndex++;
                selectedBabyIndex = (selectedBabyIndex >= babyList.count)?0:selectedBabyIndex;
                int nextIndex = selectedBabyIndex+1;
                nextIndex = (nextIndex >= babyList.count)?0:nextIndex;

                NSLog(@"selectedBabyIndex3=%d", selectedBabyIndex);
                NSLog(@"prevIndex=%d", prevIndex);
                NSLog(@"nextIndex=%d", nextIndex);
                
                
                
                [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"feed_info"]] forKey:@"feed_info"];
                [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"pump_info"]] forKey:@"pump_info"];
                [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"sleep_info"]] forKey:@"sleep_info"];
                [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"poo_info"]] forKey:@"poo_info"];
                [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"wee_info"]] forKey:@"wee_info"];
                [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"medical_info"]] forKey:@"medical_info"];
                [shareObject synchronize];
                
                [self.imgBabyPic1 sd_setImageWithURL:[NSURL URLWithString:[[babyList objectAtIndex:prevIndex] objectForKey:@"photo_url"]] placeholderImage:[UIImage imageNamed:@"dummy.png"]];
                self.txtName1.text = [[babyList objectAtIndex:prevIndex] objectForKey:@"name"];
                self.txtInfo1.text = [NSString stringWithFormat:LocalizedString(@"txt_baby_info", nil), [[babyList objectAtIndex:prevIndex] objectForKey:@"age"], [[babyList objectAtIndex:prevIndex] objectForKey:@"height"], [[babyList objectAtIndex:prevIndex] objectForKey:@"weight"], [[babyList objectAtIndex:prevIndex] objectForKey:@"blood_type"]];
                
                [self.imgBabyPic2 sd_setImageWithURL:[NSURL URLWithString:[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"photo_url"]] placeholderImage:[UIImage imageNamed:@"dummy.png"]];
                self.txtName2.text = [[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"name"];
                self.txtInfo2.text = [NSString stringWithFormat:LocalizedString(@"txt_baby_info", nil), [[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"age"], [[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"height"], [[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"weight"], [[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"blood_type"]];
                
                [self.imgBabyPic3 sd_setImageWithURL:[NSURL URLWithString:[[babyList objectAtIndex:nextIndex] objectForKey:@"photo_url"]] placeholderImage:[UIImage imageNamed:@"dummy.png"]];
                self.txtName3.text = [[babyList objectAtIndex:nextIndex] objectForKey:@"name"];
                self.txtInfo3.text = [NSString stringWithFormat:LocalizedString(@"txt_baby_info", nil), [[babyList objectAtIndex:nextIndex] objectForKey:@"age"], [[babyList objectAtIndex:nextIndex] objectForKey:@"height"], [[babyList objectAtIndex:nextIndex] objectForKey:@"weight"], [[babyList objectAtIndex:nextIndex] objectForKey:@"blood_type"]];
                
                self.babyScrollView.contentOffset = CGPointMake(self.view.frame.size.width, self.babyScrollView.contentOffset.y);
                backupPageNo = 1;
                
                isDayListInit = isWeekListInit = isMonthListInit = NO;
                [self refreshAction];
            }
        }
        else backupPageNo = pageNo;
    }
    if([scrollView isEqual:self.contentScrollView])
    {
        CGFloat pageWidth = self.view.bounds.size.width;
        int pageNo = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
        
        if(pageNo == 0)
        {
            [self.btnDay sendActionsForControlEvents:UIControlEventTouchUpInside];
        }
        else if(pageNo == 1)
        {
            [self.btnWeek sendActionsForControlEvents:UIControlEventTouchUpInside];
        }
        else
        {
            [self.btnMonth sendActionsForControlEvents:UIControlEventTouchUpInside];
        }
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{    
    if([scrollView isEqual:self.contentScrollView])
    {
        CGFloat pageWidth = self.view.bounds.size.width;
        int pageNo = floor((self.contentScrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
        
        if(pageNo == 0)
        {
            [self.btnDay setBackgroundColor:UIColorFromRGB(0x8D8082)];
            [self.btnWeek setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
            [self.btnMonth setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
        }
        else if(pageNo == 1)
        {
            [self.btnDay setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
            [self.btnWeek setBackgroundColor:UIColorFromRGB(0x8D8082)];
            [self.btnMonth setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
        }
        else
        {
            [self.btnDay setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
            [self.btnWeek setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
            [self.btnMonth setBackgroundColor:UIColorFromRGB(0x8D8082)];
        }
    }
}


/*#pragma mark Central Manager delegate methods
- (void)centralManagerDidUpdateState:(CBCentralManager *)central
{
    if([central state] == CBCentralManagerStatePoweredOff)
    {
        NSLog(@"CoreBluetooth BLE hardware is powered off");
    }
    else if([central state] == CBCentralManagerStatePoweredOn)
    {
        NSLog(@"CoreBluetooth BLE hardware is powered on and ready");
    }
    else if([central state] == CBCentralManagerStateUnauthorized)
    {
        NSLog(@"CoreBluetooth BLE state is unauthorized");
    }
    else if([central state] == CBCentralManagerStateUnknown)
    {
        NSLog(@"CoreBluetooth BLE state is unknown");
    }
    else if([central state] == CBCentralManagerStateUnsupported)
    {
        NSLog(@"CoreBluetooth BLE hardware is unsupported on this platform");
    }
}*/

/*!
 * @brief Starts scanning for peripherals with rscServiceUUID
 * @param enable If YES, this method will enable scanning for bridge devices, if NO it will stop scanning
 * @return 0 if success, -1 if Bluetooth Manager is not in CBCentralManagerStatePoweredOn state.
 */
/*- (int)scanForPeripherals:(BOOL)enable
{
    if(appDelegate.cbCentralManager.state != CBCentralManagerStatePoweredOn)
    {
        return -1;
    }
    
    // Scanner uses other queue to send events. We must edit UI in the main queue
    dispatch_async(dispatch_get_main_queue(), ^{
        if(enable)
        {
            NSLog(@"appDelegate here");
            NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:YES], CBCentralManagerScanOptionAllowDuplicatesKey, nil];
            [appDelegate.cbCentralManager scanForPeripheralsWithServices:nil options:options];
        }
        else
        {
            [appDelegate.cbCentralManager stopScan];
        }
    });
    
    return 0;
}

- (void)centralManager:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)peripheral advertisementData:(NSDictionary *)advertisementData RSSI:(NSNumber *)RSSI
{
    // Scanner uses other queue to send events. We must edit UI in the main queue
    if ([[advertisementData objectForKey:CBAdvertisementDataIsConnectable] boolValue])
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            // Add the sensor to the list and reload deta set
            ScannedPeripheral *sensor = [ScannedPeripheral initWithPeripheral:peripheral rssi:RSSI.intValue isPeripheralConnected:NO];
            NSLog(@"sensor=%@", sensor.name);
            
            NSMutableArray *deviceList = (NSMutableArray *)[NSKeyedUnarchiver unarchiveObjectWithData:[shareObject objectForKey:@"device_list"]];
            //NSLog(@"deviceList=%@", deviceList);
            
            for(int i=0; i<deviceList.count; i++)
            {
                if([[[deviceList objectAtIndex:i] objectForKey:@"name"] isEqualToString:sensor.name] && [[[deviceList objectAtIndex:i] objectForKey:@"wearable_check"] boolValue])
                {
                    appDelegate.peripheral = [sensor peripheral];
                    [self scanForPeripherals:NO];
                    [self centralManager:appDelegate.cbCentralManager didPeripheralSelected:appDelegate.peripheral];
                    
                    NSLog(@"Connecting to %@...", appDelegate.peripheral.name);
                    [appDelegate.cbCentralManager connectPeripheral:appDelegate.peripheral options:nil];
                    break;
                }
            }
        });
    }
    else
    {
        NSLog(@"here");
    }
}


#pragma mark - Scanner Delegate methods
- (void)centralManager:(CBCentralManager *)manager didPeripheralSelected:(CBPeripheral *)peripheral
{
    NSLog(@"connectPeripheral2");
    // We may not use more than one Central Manager instance. Let's just take the one returned from Scanner View Controller
    appDelegate.bluetoothManager = [[BluetoothManager alloc] initWithManager:manager];
    appDelegate.bluetoothManager.delegate = self;
    [appDelegate.bluetoothManager connectDevice:peripheral];
}


#pragma mark - BluetoothManager delegate methods
- (void)didDeviceConnected:(NSString *)peripheralName
{
    NSLog(@"didDeviceConnected %@", peripheralName);
}
- (void)didDeviceDisconnected
{
    NSLog(@"didDeviceDisconnected");
    appDelegate.bluetoothManager = nil;
}
- (void)isDeviceReady
{
    NSLog(@"Device is ready");
}
- (void)deviceNotSupported
{
    NSLog(@"Device not supported");
}
- (void)didCharacteristicsFound
{
    NSLog(@"didCharacteristicsFound");
}
- (void)didUpdateValueForCharacteristic:(NSString *)message
{
    NSLog(@"didUpdateValueForCharacteristic %@", message);
    
    NSArray *hexData = [message componentsSeparatedByString:@"-"];
    if(hexData.count >= 20)
    {
        [appDelegate addActivity:hexData];
    }
}*/

@end
