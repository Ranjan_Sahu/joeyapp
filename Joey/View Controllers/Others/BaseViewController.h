//
//  BaseViewController.h
//  Joey
//
//  Created by csl on 23/1/2017.
//  Copyright © 2017 Auxilia. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^UIAlertControllerCallback)(UIAlertAction *action);

@interface BaseViewController : UIViewController

- (void)showAlertWithTitle:(NSString *)title message:(NSString *)message input:(id)input;
- (void)showAlertWithTitleCallback:(NSString *)title message:(NSString *)message callback:(UIAlertControllerCallback)callback;
- (void)showConfirmAlert:(NSString *)title message:(NSString *)message callback:(UIAlertControllerCallback)callback;
- (void)showDeleteAlert:(NSString *)title message:(NSString *)message callback:(UIAlertControllerCallback)callback;
- (NSArray *)rangesOfString:(NSString *)searchString inString:(NSString *)string;
- (float)convertUnit:(float)value unit:(NSString *)oldUnit newUnit:(NSString *)newUnit;
- (NSString *)convertUnit:(NSString *)string unit:(NSString *)newUnit spacing:(BOOL)spacing;
- (NSString *)convertDuration:(int)diff fullFormatCheck:(BOOL)fullFormatCheck;
- (NSArray *)convertDuration:(int)diff;
- (NSDate *)getFirstDayOfTheWeekFromDate:(NSDate *)givenDate;

@end
