//
//  InvitationViewController.m
//  Joey
//
//  Created by csl on 21/3/2017.
//  Copyright © 2017 Auxilia. All rights reserved.
//

#import "AppDelegate.h"
#import "InvitationViewController.h"

@interface InvitationViewController ()
{
    NSUserDefaults *shareObject;
    AppDelegate *appDelegate;
}
@end

@implementation InvitationViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    shareObject = [NSUserDefaults standardUserDefaults];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    [self.navigationController setNavigationBarHidden:NO];
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:UIColorFromRGB(0x8D8082), NSForegroundColorAttributeName, [UIFont fontWithName:@"SFUIDisplay-Medium" size:18.0f], NSFontAttributeName, nil]];
    [self.navigationController.navigationBar setBarTintColor:UIColorFromRGB(0xEEE8E4)];
    [self.navigationController.navigationBar setTranslucent:NO];
    
    if([[self.data objectForKey:@"settings_check"] boolValue])
    {
        [self.navigationItem setTitle:LocalizedString(@"title_settings_family", nil)];
    }
    else
    {
        [self.navigationItem setTitle:LocalizedString(@"title_signup", nil)];
    }
    [self customBackButton];
    
    self.lblTitle.text = LocalizedString(@"txt_invitation_title", nil);
    self.lblJoin.text = LocalizedString(@"txt_join_existing_family", nil);
    self.lblCreate.text = LocalizedString(@"txt_create_new_family", nil);
    self.lblOr.text = LocalizedString(@"txt_or", nil);
    
    self.txtInvitationCode.placeholder = LocalizedString(@"txt_invitation_code", nil);
    
    self.viewInvitationCode.layer.cornerRadius = 5.f;
    self.viewInvitationCode.layer.borderWidth = 1.f;
    self.viewInvitationCode.layer.borderColor = UIColorFromRGB(0xF2EEEB).CGColor;
    
    self.btnJoin.layer.cornerRadius = 5.f;
    [self.btnJoin setTitle:LocalizedString(@"btn_join", nil) forState:UIControlStateNormal];
    
    self.viewJoin.layer.cornerRadius = 5.f;
    self.viewCreate.layer.cornerRadius = 5.f;
    self.btnCreate.layer.cornerRadius = 5.f;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (IS_IPAD) {
        self.lblTitle.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:22.0f];
        self.lblJoin.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:20.0f];
        self.txtInvitationCode.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:20.0f];
        [self.btnJoin.titleLabel setFont:[UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f]];
        [self.btnCreate.titleLabel setFont:[UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f]];
        self.lblOr.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:20.0f];
        self.lblCreate.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:20.0f];
        self.logInHeight.constant = 96.0;
        self.logoWidth.constant = 117.0;
        self.btnJoinHeight.constant = 54.0;
        self.invitationCodeHeight.constant = 54.0;
        self.iconJoinFamilyWidth.constant = 137.0;
        self.iconJoinFamilyHeight.constant = 56.0;
        self.iconNewFamilyWidth.constant = 79.0;
        self.iconNewFamilyHeight.constant = 65.0;
    }
    
}

- (void)customBackButton
{
    if([self.navigationController.viewControllers count] > 1)
    {
        UIImage *imgBack = [UIImage imageNamed:@"nav_btn_back"];
        UIButton *btnBack = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnBack setImage:imgBack forState:UIControlStateNormal];
        btnBack.frame = CGRectMake(0, 0, imgBack.size.width, imgBack.size.height);
        [btnBack addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
        [btnBack setImageEdgeInsets:UIEdgeInsetsMake(0, -16, 0, 16)];
        UIBarButtonItem *backBarItem = [[UIBarButtonItem alloc] initWithCustomView:btnBack];
        self.navigationItem.leftBarButtonItem = backBarItem;
    }
}

- (void)backAction
{
    if([[self.data objectForKey:@"splash_page_check"] boolValue])
    {
        [self.navigationController setNavigationBarHidden:YES];
    }
    
    if(![[self.data objectForKey:@"settings_check"] boolValue])
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
    else
    {
        NSMutableArray *familyList = (NSMutableArray *)[NSKeyedUnarchiver unarchiveObjectWithData:[shareObject objectForKey:@"family_list"]];
        if(familyList.count == 0)
        {
            UIViewController *viewController = [self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count-3];
            [self.navigationController popToViewController:viewController animated:YES];
        }
        else
        {
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
}

- (BOOL)validateInputs
{
    ALPValidator *validator = nil;
    
    validator = [ALPValidator validatorWithType:ALPValidatorTypeString];
    [validator addValidationToEnsurePresenceWithInvalidMessage:LocalizedString(@"error_invitation_code", nil)];
    [validator validate:self.txtInvitationCode.text];
    if(![validator isValid])
    {
        [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:[[validator errorMessages] objectAtIndex:0] input:self.txtInvitationCode];
        return NO;
    }
    
    return YES;
}

- (IBAction)createFamilyAction:(id)sender
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"NewBabyViewController"];
    [self.navigationController pushViewController:viewController animated:YES];
}

- (IBAction)nextAction:(id)sender
{
    if([self validateInputs])
    {
        [SVProgressHUD show];
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setValue:@([shareObject integerForKey:@"user_id"]) forKey:@"user_id"];
        [parameters setValue:self.txtInvitationCode.text forKey:@"code"];
        [[WebServiceManager sharedManager] initWithBaseURL:@"api/confirmInvitation" parameters:parameters success:^(id result) {
            
            if([[result objectForKey:@"success"] boolValue])
            {
                if([[result valueForKey:@"device_list"] count] > 0)
                {
                    [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableArray *)[result valueForKey:@"device_list"]] forKey:@"device_list"];
                }
                if([[result valueForKey:@"family_list"] count] > 0)
                {
                    [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableArray *)[result valueForKey:@"family_list"]] forKey:@"family_list"];
                    [shareObject synchronize];
                }
                if([[result valueForKey:@"baby_list"] count] > 0)
                {
                    [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableArray *)[result valueForKey:@"baby_list"]] forKey:@"baby_list"];
                    [shareObject synchronize];
                    
                    [self.navigationController setNavigationBarHidden:YES];
                    [self.view removeFromSuperview];
                    
                    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                    UIViewController *tabBarController = [storyboard instantiateViewControllerWithIdentifier:@"TabBarController"];
                    appDelegate.window.rootViewController = tabBarController;
                }
                else
                {
                    [self.navigationController setNavigationBarHidden:YES];
                    
                    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                    UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"NewBabyViewController"];
                    [self.navigationController pushViewController:viewController animated:YES];
                }
            }
            else
            {
                [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:[result objectForKey:@"message"] input:nil];
            }
            
        } failure:^(NSError *error) {
            [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:LocalizedString(@"error_unknown", nil) input:nil];
        }];
    }
}


#pragma mark UITextFieldDelegate Methods
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if([textField isEqual:self.txtInvitationCode])
    {
        [self.btnJoin sendActionsForControlEvents:UIControlEventTouchUpInside];
    }
    return YES;
}

@end
