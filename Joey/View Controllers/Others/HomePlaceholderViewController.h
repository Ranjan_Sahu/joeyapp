//
//  HomePlaceholderViewController.h
//  Joey
//
//  Created by csl on 8/10/2017.
//  Copyright © 2017 Auxilia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomePlaceholderViewController : UIViewController

@property (nonatomic, weak) IBOutlet UILabel *txtMessage1;
@property (nonatomic, weak) IBOutlet UILabel *txtMessage2;
@property (nonatomic, weak) IBOutlet UILabel *txtMessage3;
@property (nonatomic, weak) IBOutlet UILabel *txtMessage4;

@end
