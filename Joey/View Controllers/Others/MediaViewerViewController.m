//
//  MediaViewerViewController.m
//  Joey
//
//  Created by webwerks on 6/20/18.
//  Copyright © 2018 Auxilia. All rights reserved.
//

#import "MediaViewerViewController.h"
#import "VideoPlayerManager.h"
#import <AsyncImageView/AsyncImageView.h>

@interface MediaViewerViewController ()
{
    VideoPlayerManager *vManager;
    int prevPageIndex;
    UIPageControl *pgControl;
    int pIndex;
}
@end

@implementation MediaViewerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.scrollView.delegate = self;
    self.scrollView.backgroundColor = [UIColor clearColor];
    self.mediaUrlArray = [self.data objectForKey:@"urls"];
    prevPageIndex = 0;//pageIndex
    pIndex = 0;
    NSString *mediaStr = [self.data objectForKey:@"SelectedMediaUrl"];
    if (mediaStr && mediaStr.length>0) {
        pIndex = (int)[self.mediaUrlArray indexOfObject:mediaStr];
    }
    
    if (pIndex<0 || pIndex>self.mediaUrlArray.count-1) {
        pIndex = 0;
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    [self.view layoutIfNeeded];
    [self createScrollPages];
    [self configurePageControl];
}

#pragma mark - Custom Methods
-(void)createScrollPages
{
    CGRect frame = CGRectMake(0, 0, 0, 0);
    for (int i =0; i<self.mediaUrlArray.count; i++) {
        frame.origin.x = self.scrollView.frame.size.width*i;
        frame.size = self.scrollView.frame.size;
        UIView *subview = [[UIView alloc]initWithFrame:frame];
        subview.backgroundColor = [UIColor clearColor];
        
        NSArray *imgExt = [NSArray arrayWithObjects:@"mp4",@"mov",@"m3u8", nil];
        NSString *urlStr = [_mediaUrlArray objectAtIndex:i];
        NSURL *url = [NSURL URLWithString:urlStr];
        NSString *pathExt = [url pathExtension];
        
        if ([imgExt containsObject:pathExt]) {
            //video
            if (pIndex == 0 && i == 0) {
                [vManager removePlayerInstance];
                vManager = [VideoPlayerManager new];
                vManager.videoUrl = urlStr;
                [vManager initilizePlayerwithView:subview andController:self];
                [vManager.player pause];
            }
        }else {
            //image
            AsyncImageView *imgView = [[AsyncImageView alloc] initWithFrame:CGRectMake(0, 0, subview.frame.size.width, subview.frame.size.height)];
            imgView.imageURL = [NSURL URLWithString:urlStr];
            [subview addSubview:imgView];
        }
        
        [self.scrollView addSubview:subview];
        
    }
    
    self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width * _mediaUrlArray.count, self.scrollView.frame.size.height);
    //Move to current page
    CGRect nFrame = self.scrollView.frame;
    nFrame.origin.x = nFrame.size.width * pIndex;
    nFrame.origin.y = 0;
    [self.scrollView scrollRectToVisible:nFrame animated:YES];
    
}

-(void) configurePageControl
{
    pgControl = [[UIPageControl alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2 - 50, self.view.frame.size.height-30, 100, 30)];
    pgControl.numberOfPages = self.mediaUrlArray.count;
    pgControl.currentPage = pIndex;
    pgControl.hidesForSinglePage = YES;
    pgControl.pageIndicatorTintColor = [UIColor groupTableViewBackgroundColor];
    pgControl.currentPageIndicatorTintColor = UIColorFromRGB(0x007AFF);
    [self.view addSubview:pgControl];
}

#pragma mark - UIScrollView Delegates
//- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
//{
//    int indexOfPage = scrollView.contentOffset.x / scrollView.frame.size.width;
//    if (prevPageIndex == indexOfPage) {
//        return;
//    }
//
//    pgControl.currentPage = indexOfPage;
//    prevPageIndex = indexOfPage;
//    NSArray *arr = [self.scrollView subviews];
//    UIView *v = [arr objectAtIndex:indexOfPage];
//
//    NSArray *imgExt = [NSArray arrayWithObjects:@"mp4",@"mov",@"m3u8", nil];
//    NSString *urlStr = [_mediaUrlArray objectAtIndex:indexOfPage];
//    NSURL *url = [NSURL URLWithString:urlStr];
//    NSString *pathExt = [url pathExtension];
//
//    if ([imgExt containsObject:pathExt]) {
//    //video
//        [vManager removePlayerInstance];
//        vManager = [VideoPlayerManager new];
//        vManager.videoUrl = urlStr;
//        [vManager initilizePlayerwithView:v andController:self];
//        [vManager.player pause];
//    }else {
//        //image
//        AsyncImageView *imgView = [[AsyncImageView alloc] initWithFrame:CGRectMake(0, 0, v.frame.size.width, v.frame.size.height)];
//        imgView.imageURL = [NSURL URLWithString:urlStr];
//        [v addSubview:imgView];
//    }
//
//}

- (void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView
{
    NSLog(@"scrollViewWillBeginDecelerating");
    int indexOfPage = scrollView.contentOffset.x / scrollView.frame.size.width;
    if (prevPageIndex != indexOfPage) {
        [vManager removePlayerInstance];
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    int indexOfPage = scrollView.contentOffset.x / scrollView.frame.size.width;
    if (prevPageIndex == indexOfPage) {
        return;
    }
    
    pgControl.currentPage = indexOfPage;
    prevPageIndex = indexOfPage;
    NSArray *arr = [self.scrollView subviews];
    UIView *v = [arr objectAtIndex:indexOfPage];
    
    NSArray *imgExt = [NSArray arrayWithObjects:@"mp4",@"mov",@"m3u8", nil];
    NSString *urlStr = [_mediaUrlArray objectAtIndex:indexOfPage];
    NSURL *url = [NSURL URLWithString:urlStr];
    NSString *pathExt = [url pathExtension];
    
    if ([imgExt containsObject:pathExt]) {
        //video
        [vManager removePlayerInstance];
        vManager = [VideoPlayerManager new];
        vManager.videoUrl = urlStr;
        [vManager initilizePlayerwithView:v andController:self];
        [vManager.player pause];
    }else {
        //image
        AsyncImageView *imgView = [[AsyncImageView alloc] initWithFrame:CGRectMake(0, 0, v.frame.size.width, v.frame.size.height)];
        imgView.imageURL = [NSURL URLWithString:urlStr];
        [v addSubview:imgView];
    }
    
}

#pragma mark - IBActions
- (IBAction)closeAction:(id)sender
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [vManager removePlayerInstance];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)shareAction:(id)sender
{
    
}

#pragma mark - VideoManager Observer Handler
-(void)itemDidFinishPlaying:(NSNotification *) notification {
    NSLog(@"itemDidFinishPlaying");
    [vManager.player pause];
    [vManager.player seekToTime:kCMTimeZero];
}

-(void) applicationWillResignActive:(NSNotification *)notification {
    [vManager.player pause];
    if (vManager.isStatusObserverSet) {
        [vManager.player.currentItem removeObserver:self forKeyPath:@"status" ];
        vManager.isStatusObserverSet = NO;
    }
}

@end
