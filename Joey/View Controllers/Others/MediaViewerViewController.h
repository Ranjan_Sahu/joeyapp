//
//  MediaViewerViewController.h
//  Joey
//
//  Created by webwerks on 6/20/18.
//  Copyright © 2018 Auxilia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface MediaViewerViewController : BaseViewController<UIScrollViewDelegate>

@property (nonatomic, weak) IBOutlet UIButton *btnClose;
@property (nonatomic, weak) IBOutlet UIButton *btnShare;
@property (nonatomic, weak) IBOutlet UIScrollView *scrollView;
@property (nonatomic, weak) NSMutableArray *mediaUrlArray;

@end
