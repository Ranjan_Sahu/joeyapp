//
//  PhotoViewController.h
//  Joey
//
//  Created by csl on 10/1/2017.
//  Copyright © 2017 Auxilia. All rights reserved.
//

#import "BaseViewController.h"

@interface PhotoViewController : BaseViewController

@property (nonatomic, weak) IBOutlet UIButton *btnClose;
@property (nonatomic, weak) IBOutlet UIButton *btnShare;
@property (nonatomic, weak) IBOutlet UIButton *btnDelete;
@property (nonatomic, weak) IBOutlet UIButton *btnCrop;
@property (nonatomic, weak) IBOutlet UIImageView *imgPhoto;
@property (nonatomic, strong) NSMutableArray * originalImgArray;
@property (nonatomic, strong) IBOutlet UIPageControl * pgControl;

@property NSInteger index;
@property BOOL isMultiple;
@end
