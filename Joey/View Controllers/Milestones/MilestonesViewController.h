//
//  MilestonesViewController.h
//  Joey
//
//  Created by csl on 21/12/2016.
//  Copyright © 2016 Auxilia. All rights reserved.
//

#import "BaseViewController.h"
#import "NSString+Utility.h"
#import "NSMutableArray+Utility.h"
#import "NSDictionary+Utility.h"
#import "PhotoViewController.h"
#import "KDViewPager.h"
#import "ContentViewController.h"

@interface MilestonesViewController : BaseViewController <UITableViewDelegate, UITableViewDataSource,KDViewPagerDelegate,KDViewPagerDatasource,PagerSelectedDelegate>

@property (nonatomic, weak) IBOutlet UIScrollView *babyScrollView;
@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, weak) IBOutlet UIView *filterView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *filterViewHeight;

@property (nonatomic, weak) IBOutlet UITableView *subFilterTblView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *subFilterTblViewHeight;

@property (nonatomic, weak) IBOutlet UIImageView *imgBabyPic1;
@property (nonatomic, weak) IBOutlet UIImageView *imgBabyPic2;
@property (nonatomic, weak) IBOutlet UIImageView *imgBabyPic3;
@property (nonatomic, weak) IBOutlet UILabel *txtName1;
@property (nonatomic, weak) IBOutlet UILabel *txtInfo1;
@property (nonatomic, weak) IBOutlet UILabel *txtName2;
@property (nonatomic, weak) IBOutlet UILabel *txtInfo2;
@property (nonatomic, weak) IBOutlet UILabel *txtName3;
@property (nonatomic, weak) IBOutlet UILabel *txtInfo3;
@property (nonatomic, weak) IBOutlet UIButton *btnGrow;
@property (nonatomic, weak) IBOutlet UIButton *btnMove;
@property (nonatomic, weak) IBOutlet UIButton *btnIQ;
@property (nonatomic, weak) IBOutlet UIButton *btnHealth;
@property (nonatomic, weak) IBOutlet UIButton *btnOthers;
@property (nonatomic, strong) KDViewPager *pager;
@property (nonatomic, assign) NSUInteger count;

- (void)viewWillAppear:(BOOL)animated;

@end
