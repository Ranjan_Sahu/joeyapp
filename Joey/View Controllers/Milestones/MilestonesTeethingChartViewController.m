//
//  MilestonesTeethingChartViewController.m
//  Joey
//
//  Created by csl on 8/6/2017.
//  Copyright © 2017 Auxilia. All rights reserved.
//

#import "AppDelegate.h"
#import "MilestonesTeethingChartViewController.h"
#import "TeethButton.h"

@interface MilestonesTeethingChartViewController ()
{
    NSUserDefaults *shareObject;
    AppDelegate *appDelegate;
    NSArray *babyTeethChartList,*adultTeethChartList;
}
@end

@implementation MilestonesTeethingChartViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    shareObject = [NSUserDefaults standardUserDefaults];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    [self.navigationController setNavigationBarHidden:NO];
    [self.navigationController.navigationBar setBarTintColor:UIColorFromRGB(0xEEE8E4)];
    [self.navigationController.navigationBar setTranslucent:NO];
    [self.navigationItem setTitle:LocalizedString(@"title_growth_chart", nil)];
    [self customBackButton];
    self.lblNewTooth.hidden = YES;
    self.lblLostTooth.hidden = YES;
    
    self.txtChartName.text = LocalizedString(@"txt_teething_chart", nil);
    self.lblTeethingBabyUpper.text = LocalizedString(@"txt_teething_upper", nil);
    self.lblTeethingBabyLower.text = LocalizedString(@"txt_teething_lower", nil);
    
    self.lblTeethingBaby1.text = LocalizedString(@"txt_teething_6_10_months", nil);
    self.lblTeethingBaby2.text = LocalizedString(@"txt_teething_8_13_months", nil);
    self.lblTeethingBaby3.text = LocalizedString(@"txt_teething_10_16_months", nil);
    self.lblTeethingBaby4.text = LocalizedString(@"txt_teething_13_19_months", nil);
    self.lblTeethingBaby5.text = LocalizedString(@"txt_teething_16_23_months", nil);
    self.lblTeethingBaby6.text = LocalizedString(@"txt_teething_23_33_months", nil);
    
    self.lblTeethingAdult1.text = LocalizedString(@"txt_teething_6_8_years", nil);
    self.lblTeethingAdult2.text = LocalizedString(@"txt_teething_7_9_years", nil);
    self.lblTeethingAdult3.text = LocalizedString(@"txt_teething_9_12_years", nil);
    self.lblTeethingAdult4.text = LocalizedString(@"txt_teething_11_13_years", nil);
    self.lblTeethingAdult5.text = LocalizedString(@"txt_teething_17_21_years", nil);

    self.btnBaby.backgroundColor = UIColorFromRGB(0x8D8082);
    self.btnBaby.selected = YES;
    self.viewTeethingBaby.hidden = NO;
    self.btnAdult.backgroundColor = UIColorFromRGB(0xE0D3CB);
    self.btnAdult.selected = NO;
    self.viewTeethingAdult.hidden = YES;
    self.btnBaby.layer.cornerRadius = 3.f;
    self.btnBaby.layer.masksToBounds = YES;
    self.btnAdult.layer.cornerRadius = 3.f;
    self.btnAdult.layer.masksToBounds = YES;
    if(IS_IPAD) {
        [self.btnBaby.titleLabel setFont:[UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f]];
        [self.btnAdult.titleLabel setFont:[UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f]];
        self.txtChartName.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.lblNewTooth.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:23.0f];
        self.lblLostTooth.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:23.0f];
    }
    [self.btnBaby addTarget:self action:@selector(didSelectBaby:) forControlEvents:UIControlEventTouchUpInside];
    [self.btnAdult addTarget:self action:@selector(didSelectAdult:) forControlEvents:UIControlEventTouchUpInside];
    
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self getGrowthChartList];
}

-(void)didSelectBaby:(UIButton *)sender {
    self.btnBaby.backgroundColor = UIColorFromRGB(0x8D8082);
    self.btnBaby.selected = YES;
    self.btnAdult.backgroundColor = UIColorFromRGB(0xE0D3CB);
    self.btnAdult.selected = NO;
    self.viewTeethingBaby.hidden = NO;
    self.viewTeethingAdult.hidden = YES;
}

-(void)didSelectAdult:(UIButton *)sender {
    [self processTeeth:adultTeethChartList];
    self.btnAdult.backgroundColor = UIColorFromRGB(0x8D8082);
    self.btnAdult.selected = YES;
    self.btnBaby.backgroundColor = UIColorFromRGB(0xE0D3CB);
    self.btnBaby.selected = NO;
    self.viewTeethingAdult.hidden = NO;
    self.viewTeethingBaby.hidden = YES;

}

- (void)customBackButton
{
    if([self.navigationController.viewControllers count] > 1)
    {
        UIImage *imgBack = [UIImage imageNamed:@"nav_btn_back"];
        UIButton *btnBack = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnBack setImage:imgBack forState:UIControlStateNormal];
        btnBack.frame = CGRectMake(0, 0, imgBack.size.width, imgBack.size.height);
        [btnBack addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
        [btnBack setImageEdgeInsets:UIEdgeInsetsMake(0, -16, 0, 16)];
        UIBarButtonItem *backBarItem = [[UIBarButtonItem alloc] initWithCustomView:btnBack];
        self.navigationItem.leftBarButtonItem = backBarItem;
    }
}

- (void)backAction
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)getGrowthChartList
{
    self.viewTeethingBaby1.layer.cornerRadius = self.viewTeethingBaby1.frame.size.width/2;
    self.viewTeethingBaby2.layer.cornerRadius = self.viewTeethingBaby2.frame.size.width/2;
    self.viewTeethingBaby3.layer.cornerRadius = self.viewTeethingBaby3.frame.size.width/2;
    self.viewTeethingBaby4.layer.cornerRadius = self.viewTeethingBaby4.frame.size.width/2;
    self.viewTeethingBaby5.layer.cornerRadius = self.viewTeethingBaby5.frame.size.width/2;
    self.viewTeethingBaby6.layer.cornerRadius = self.viewTeethingBaby6.frame.size.width/2;
    
    self.viewTeethingAdult1.layer.cornerRadius = self.viewTeethingAdult1.frame.size.width/2;
    self.viewTeethingAdult2.layer.cornerRadius = self.viewTeethingAdult2.frame.size.width/2;
    self.viewTeethingAdult3.layer.cornerRadius = self.viewTeethingAdult3.frame.size.width/2;
    self.viewTeethingAdult4.layer.cornerRadius = self.viewTeethingAdult4.frame.size.width/2;
    self.viewTeethingAdult5.layer.cornerRadius = self.viewTeethingAdult5.frame.size.width/2;

    if (IS_IPAD) {
        self.viewTeethBaby.transform = CGAffineTransformMakeScale(2, 2);
        self.viewTeethGuideBaby.transform = CGAffineTransformMakeScale(1.5, 1.5);
        self.viewTeethAdult.transform = CGAffineTransformMakeScale(2, 2);
        self.viewTeethGuideAdult.transform = CGAffineTransformMakeScale(1.5, 1.5);
    }
    
//    UIImage *img = [[UIImage alloc] init];
//    img = self.btnTeethingAdult1.imageView.image;
//    img = [img imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
//    [self.btnTeethingAdult1 setImage:img forState:UIControlStateNormal];
//    self.btnTeethingAdult1.tintColor = [UIColor redColor];
    
    self.btnTeethingBaby1.tag = 1;
    self.btnTeethingBaby2.tag = 2;
    self.btnTeethingBaby3.tag = 3;
    self.btnTeethingBaby4.tag = 4;
    self.btnTeethingBaby5.tag = 5;
    self.btnTeethingBaby6.tag = 6;
    self.btnTeethingBaby7.tag = 7;
    self.btnTeethingBaby8.tag = 8;
    self.btnTeethingBaby9.tag = 9;
    self.btnTeethingBaby10.tag = 10;
    self.btnTeethingBaby11.tag = 11;
    self.btnTeethingBaby12.tag = 12;
    self.btnTeethingBaby13.tag = 13;
    self.btnTeethingBaby14.tag = 14;
    self.btnTeethingBaby15.tag = 15;
    self.btnTeethingBaby16.tag = 16;
    self.btnTeethingBaby17.tag = 17;
    self.btnTeethingBaby18.tag = 18;
    self.btnTeethingBaby19.tag = 19;
    self.btnTeethingBaby20.tag = 20;
    
    self.btnTeethingAdult1.tag = 21;
    self.btnTeethingAdult2.tag = 22;
    self.btnTeethingAdult3.tag = 23;
    self.btnTeethingAdult4.tag = 24;
    self.btnTeethingAdult5.tag = 25;
    self.btnTeethingAdult6.tag = 26;
    self.btnTeethingAdult7.tag = 27;
    self.btnTeethingAdult8.tag = 28;
    self.btnTeethingAdult9.tag = 29;
    self.btnTeethingAdult10.tag = 30;
    self.btnTeethingAdult11.tag = 31;
    self.btnTeethingAdult12.tag = 32;
    self.btnTeethingAdult13.tag = 33;
    self.btnTeethingAdult14.tag = 34;
    self.btnTeethingAdult15.tag = 35;
    self.btnTeethingAdult16.tag = 36;
    self.btnTeethingAdult17.tag = 37;
    self.btnTeethingAdult18.tag = 38;
    self.btnTeethingAdult19.tag = 39;
    self.btnTeethingAdult20.tag = 40;
    self.btnTeethingAdult21.tag = 41;
    self.btnTeethingAdult22.tag = 42;
    self.btnTeethingAdult23.tag = 43;
    self.btnTeethingAdult24.tag = 44;
    self.btnTeethingAdult25.tag = 45;
    self.btnTeethingAdult26.tag = 46;
    self.btnTeethingAdult27.tag = 47;
    self.btnTeethingAdult28.tag = 48;
    self.btnTeethingAdult29.tag = 49;
    self.btnTeethingAdult30.tag = 50;
    self.btnTeethingAdult31.tag = 51;
    self.btnTeethingAdult32.tag = 52;

    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setValue:@([shareObject integerForKey:@"user_id"]) forKey:@"user_id"];
    [parameters setValue:@([shareObject integerForKey:@"baby_id"]) forKey:@"baby_id"];
    [parameters setValue:@"teething" forKey:@"type"];
    [parameters setValue:[self.data objectForKey:@"created_at"] forKey:@"created_at"];
    [[WebServiceManager sharedManager] initWithBaseURL:@"api/getGrowthChartList" parameters:parameters success:^(id result) {
        
        babyTeethChartList = [[NSArray alloc] initWithArray:[result objectForKey:@"baby_chart_list"]];
        adultTeethChartList = [[NSArray alloc] initWithArray:[result objectForKey:@"adult_chart_list"]];
        
        [self processTeeth:babyTeethChartList];
        
    } failure:^(NSError *error) {
    }];
}


-(void)processTeeth:(NSArray *)teeths{
    
    int delayInSeconds = 1;
    
    for(int i=0; i<[teeths count]; i++)
    {
        int teethId = [[[teeths objectAtIndex:i ] objectForKey:@"teeth_id"] intValue];
        
        TeethButton * button = [self.view viewWithTag:teethId];
        button.data = [teeths objectAtIndex:i];
        [button toggle];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self showTip:button];
        });
        delayInSeconds+=2;
        
        if([[[teeths objectAtIndex:i] objectForKey:@"content"] isEqualToString:@"Lost Tooth"]){
            button.imageView.image = [button.imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            button.imageView.tintColor = UIColorFromRGB(0x8D8082);
        }
        
    }
    
}

#pragma mark - Teeth Action
- (IBAction)didSelectBabyTeeth:(TeethButton *)sender {
    [self showTip:sender];
}

- (IBAction)didSelectAdultTeeth:(TeethButton *)sender {
    [self showTip:sender];
}

-(void)showTip:(TeethButton *)sender{
    
    NSDictionary *teethObj = sender.data;
    if ([[teethObj valueForKey:@"teeth_id"] intValue]== [sender tag]) {
        NSString *teethContent = [teethObj valueForKey:@"content"];
        NSString *teethDate = [teethObj valueForKey:@"date"];
//        NSString *teethTime = [teethObj valueForKey:@"time"];
        if ([teethContent containsString:@"New"]) {
            [sender showTip:[NSString stringWithFormat:@"New: %@",teethDate]];
//            self.lblNewTooth.text = [NSString stringWithFormat:@"New: %@",teethDate];
//            self.lblNewTooth.hidden = NO;
        }else if ([teethContent containsString:@"Lost"]) {
            [sender showTip:[NSString stringWithFormat:@"Lost: %@",teethDate]];
//            self.lblLostTooth.text = [NSString stringWithFormat:@"Lost: %@",teethDate];
//            self.lblLostTooth.hidden = NO;
        }
        [NSTimer scheduledTimerWithTimeInterval:3 target:self selector:@selector(hideLabel) userInfo:nil repeats:NO];
    }
    
}

-(void)hideLabel {
    self.lblNewTooth.hidden = YES;
    self.lblLostTooth.hidden = YES;
}

@end
