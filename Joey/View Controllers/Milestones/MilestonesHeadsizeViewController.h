//
//  MilestonesHeadsizeViewController.h
//  Joey
//
//  Created by csl on 2/3/2017.
//  Copyright © 2017 Auxilia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MilestonesHeadsizeViewController : UIViewController <UIScrollViewDelegate, UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, weak) IBOutlet UIBarButtonItem *btnAdd;
@property (nonatomic, weak) IBOutlet UITableView *tableView;

@end
