//
//  MilestonesViewController.m
//  Joey
//
//  Created by csl on 21/12/2016.
//  Copyright © 2016 Auxilia. All rights reserved.
//

#import "AppDelegate.h"
#import "MilestonesViewController.h"
#import "MilestonesTableViewCell.h"
#import "MilestonesGrowTableViewCell.h"
#import "EmptyTableViewCell.h"
#import "AsyncImageView.h"
#import "CommonHelper.h"
#import "MilestoneSubfilterTableViewCell.h"
#import "VideoPlayerManager.h"

@interface MilestonesViewController ()
{
    NSUserDefaults *shareObject;
    AppDelegate *appDelegate;
    NSDateFormatter *formatter;
    NSMutableArray *babyList;
    NSMutableArray *milestoneList;
    NSMutableDictionary *unitInfo;
    UIButton *btnFilter;
    UIView *headerView;
    AsyncImageView *imgPhoto;
    NSString *photoUrl , *photoUrlOriginal;
    BOOL isInit;
    BOOL isLoading;
    int limit;
    int offset;
    int backupPageNo;
    int selectedBabyIndex;
    int selectedFilter;
    NSString *selectedSubFilter;
    NSMutableArray *subfilterFields;
    NSMutableArray * imgArray;
    NSMutableArray * ogImgArray;
    NSMutableArray * mediaUrlsArray;
    
//    UIScrollView *viewPortScroll;
//    UIButton *viewPortBtn;
//    UIPageControl *pgControl;
//    VideoPlayerManager *vManager;
//    int prevPageIndex;
}
@end

@implementation MilestonesViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    shareObject = [NSUserDefaults standardUserDefaults];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    formatter = [[NSDateFormatter alloc] init];
    milestoneList = [[NSMutableArray alloc] init];
    imgArray = [NSMutableArray new];
    ogImgArray = [NSMutableArray new];
    mediaUrlsArray = [NSMutableArray new];
    [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
    
    unitInfo = (NSMutableDictionary *)[NSKeyedUnarchiver unarchiveObjectWithData:[shareObject objectForKey:@"unit_info"]];
    backupPageNo = 1;
    
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:UIColorFromRGB(0x8D8082), NSForegroundColorAttributeName, [UIFont fontWithName:@"SFUIDisplay-Medium" size:18.0f], NSFontAttributeName, nil]];
    [self.navigationController.navigationBar setBarTintColor:UIColorFromRGB(0xEEE8E4)];
    [self.navigationController.navigationBar setTranslucent:NO];
    [self customSettingsButton];
    
    UIButton *btnLogo = [[UIButton alloc] initWithFrame:CGRectZero];
    [btnLogo setImage:[UIImage imageNamed:@"nav_logo.png"] forState:UIControlStateNormal];
    [btnLogo sizeToFit];
    self.navigationItem.titleView = btnLogo;
    
    self.tableView.alwaysBounceVertical = YES;
    self.subFilterTblView.alwaysBounceVertical = YES;
    self.subFilterTblView.dataSource = self;
    self.subFilterTblView.delegate = self;
    self.subFilterTblView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.subFilterTblView.hidden = YES;
    self.subFilterTblView.backgroundColor = UIColorFromRGB(0xEEE6E0);
    self.subFilterTblView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
    
    selectedSubFilter = @"";
    self.filterViewHeight.constant = 50.0f;
    
    __weak __typeof(self)weakSelf = self;
    [self.tableView addPullToRefreshWithActionHandler:^{
        [weakSelf refreshAction];
    }];
    [self.tableView addInfiniteScrollingWithActionHandler:^{
        [weakSelf getMilestoneList];
    }];
    
    [SVProgressHUD show];
    [self setupBabyList];
    [self refreshAction];
    
    if (IS_IPAD) {
        self.txtName1.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.txtInfo1.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:22.0f];
        self.txtName2.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.txtInfo2.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:22.0f];
        self.txtName3.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.txtInfo3.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:22.0f];
    }
    [self.view layoutSubviews];
    selectedSubFilter = 0;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:(BOOL)animated];
    
    [appDelegate.tracker send:[[[GAIDictionaryBuilder createScreenView] set:@"Milestones" forKey:kGAIScreenName] build]];
    
    [self.navigationController.navigationBar setBarTintColor:UIColorFromRGB(0xEEE8E4)];
    
    unitInfo = (NSMutableDictionary *)[NSKeyedUnarchiver unarchiveObjectWithData:[shareObject objectForKey:@"unit_info"]];
    NSLog(@"unitInfo=%@", unitInfo);
    
    NSLog(@"here=%@", self.data);
    NSLog(@"appDelegate.updateMilestonesCheck=%@", appDelegate.updateMilestonesCheck?@"YES":@"NO");
    if([[self.data objectForKey:@"save_check"] boolValue] || appDelegate.updateMilestonesCheck || selectedBabyIndex != (int)[shareObject integerForKey:@"baby_index"])
    {
        appDelegate.updateMilestonesCheck = NO;
        [self.data removeAllObjects];
        
        [SVProgressHUD show];
        [self setupBabyList];
        [self refreshAction];
    }
    
    UITabBarItem *tabbarItem = (UITabBarItem *)[appDelegate.tabbar.tabBar.items objectAtIndex:2];
    NSLog(@"tabbarItem=%d", [tabbarItem.badgeValue intValue]);
    
    if(selectedBabyIndex != (int)[shareObject integerForKey:@"baby_index"] || [tabbarItem.badgeValue intValue] > 0)
    {
        NSLog(@"hey");
        tabbarItem.badgeValue = nil;
        [self setupBabyList];
        [self refreshAction];
    }
    
    /*if(!appDelegate.cbCentralManager.delegate)
     {
     appDelegate.cbCentralManager.delegate = appDelegate;
     [appDelegate scanForPeripherals:YES];
     }*/
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:(BOOL)animated];
    [UIView animateWithDuration:0.1f delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        self.filterView.frame = CGRectMake(0, -self.filterView.frame.size.height, self.filterView.frame.size.width, self.filterView.frame.size.height);
    } completion:nil];
    selectedFilter =0 ;
}

- (void)viewDidLayoutSubviews
{
    if(self.babyScrollView.contentSize.width == 0)
    {
        self.babyScrollView.contentSize = CGSizeMake(self.view.frame.size.width*3, 1);
        self.babyScrollView.contentOffset = CGPointMake(self.view.frame.size.width, self.babyScrollView.contentOffset.y);
    }
    
    self.imgBabyPic1.clipsToBounds = self.imgBabyPic2.clipsToBounds = self.imgBabyPic3.clipsToBounds = YES;
    self.imgBabyPic1.layer.cornerRadius = self.imgBabyPic1.frame.size.width/2;
    self.imgBabyPic2.layer.cornerRadius = self.imgBabyPic2.frame.size.width/2;
    self.imgBabyPic3.layer.cornerRadius = self.imgBabyPic3.frame.size.width/2;
}

- (void)customSettingsButton
{
    UIImage *imgFilter = [UIImage imageNamed:@"nav_btn_filter"];
    btnFilter = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnFilter setImage:imgFilter forState:UIControlStateNormal];
    btnFilter.frame = CGRectMake(-10, 0, imgFilter.size.width, imgFilter.size.height);
    [btnFilter addTarget:self action:@selector(filterAction:) forControlEvents:UIControlEventTouchUpInside];
    
    CGFloat fWidth = self.view.frame.size.width;
    
    UIImage *imgChecklist = [UIImage imageNamed:@"nav_btn_checklist"];
    UIButton *btnChecklist = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnChecklist setImage:imgChecklist forState:UIControlStateNormal];
    //btnChecklist.frame = CGRectMake(36, 0, imgChecklist.size.width, imgChecklist.size.height);
    btnChecklist.frame = CGRectMake((fWidth/4 - imgChecklist.size.width/2 - 20), 0, imgChecklist.size.width, imgChecklist.size.height);
    [btnChecklist addTarget:self action:@selector(getChecklistAction:) forControlEvents:UIControlEventTouchUpInside];
    
    UIImage *imgAdd = [UIImage imageNamed:@"nav_btn_add"];
    UIButton *btnAdd = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnAdd setImage:imgAdd forState:UIControlStateNormal];
    btnAdd.frame = CGRectMake(0, 0, imgAdd.size.width, imgAdd.size.height);
    [btnAdd addTarget:self action:@selector(addAction:) forControlEvents:UIControlEventTouchUpInside];
    
    UIImage *imgSettings = [UIImage imageNamed:@"nav_btn_settings"];
    UIButton *btnSettings = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnSettings setImage:imgSettings forState:UIControlStateNormal];
    btnSettings.frame = CGRectMake(((fWidth/4)+15 - imgSettings.size.width), 0, imgSettings.size.width, imgSettings.size.height);
    [btnSettings addTarget:self action:@selector(getSettingsAction:) forControlEvents:UIControlEventTouchUpInside];
    
    //UIView *leftButtonView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 88, 44)];
    UIView *leftButtonView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, (fWidth/4 + imgChecklist.size.width/2 - 20), 44)];
    [leftButtonView addSubview:btnFilter];
    [leftButtonView addSubview:btnChecklist];
    
    UIView *rightButtonView = [[UIView alloc] initWithFrame:CGRectMake((fWidth*3/4)-15, 0, (fWidth/4)+15, 44)];
    [rightButtonView addSubview:btnAdd];
    [rightButtonView addSubview:btnSettings];
    
    UIBarButtonItem *leftBarItem = [[UIBarButtonItem alloc] initWithCustomView:leftButtonView];
    self.navigationItem.leftBarButtonItem = leftBarItem;
    
    UIBarButtonItem *rightBarItem = [[UIBarButtonItem alloc] initWithCustomView:rightButtonView];
    self.navigationItem.rightBarButtonItem = rightBarItem;
}

- (void)setupBabyList
{
    babyList = (NSMutableArray *)[NSKeyedUnarchiver unarchiveObjectWithData:[shareObject objectForKey:@"baby_list"]];
    
    selectedBabyIndex = (int)[shareObject integerForKey:@"baby_index"];
    selectedBabyIndex = (selectedBabyIndex >= babyList.count)?0:selectedBabyIndex;
    NSLog(@"selectedBabyIndex1=%d", selectedBabyIndex);
    
    /*if([shareObject integerForKey:@"wearable_baby_id"] != [shareObject integerForKey:@"baby_id"])
     {
     for(int i=0; i<babyList.count; i++)
     {
     if([[[babyList objectAtIndex:i] objectForKey:@"id"] intValue] == [shareObject integerForKey:@"wearable_baby_id"])
     {
     selectedBabyIndex = i;
     NSLog(@"selectedBabyIndex2=%d", selectedBabyIndex);
     break;
     }
     }
     
     [shareObject setInteger:selectedBabyIndex forKey:@"baby_index"];
     [shareObject setInteger:[shareObject integerForKey:@"wearable_baby_id"] forKey:@"baby_id"];
     [shareObject synchronize];
     }*/
    
    for(int i=0; i<babyList.count; i++)
    {
        NSLog(@"weight=%@", [[babyList objectAtIndex:i] objectForKey:@"weight"]);
        NSLog(@"unit=%@", [unitInfo objectForKey:@"weight"]);
        [[babyList objectAtIndex:i] setObject:[self convertUnit:[[babyList objectAtIndex:i] objectForKey:@"height"] unit:[unitInfo objectForKey:@"height"] spacing:YES] forKey:@"height"];
        [[babyList objectAtIndex:i] setObject:[self convertUnit:[[babyList objectAtIndex:i] objectForKey:@"weight"] unit:[unitInfo objectForKey:@"weight"] spacing:YES] forKey:@"weight"];
    }
    
    // Check for Subscription of selected baby
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"is_Subscription_Enabled"];
    if (![[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"subscription"] isKindOfClass:[NSNull class]]) {
        BOOL isSubscribed = [[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"subscription"] boolValue];
        [[NSUserDefaults standardUserDefaults]setBool:isSubscribed forKey:@"is_Subscription_Enabled"];
    }
    
    NSLog(@"babyList2=%@", babyList);
    if(babyList.count == 1)
    {
        self.babyScrollView.scrollEnabled = NO;
        [self.imgBabyPic2 sd_setImageWithURL:[NSURL URLWithString:[[babyList objectAtIndex:0] objectForKey:@"photo_url"]] placeholderImage:[UIImage imageNamed:@"dummy.png"]];
        self.txtName2.text = [[babyList objectAtIndex:0] objectForKey:@"name"];
        self.txtInfo2.text = [NSString stringWithFormat:LocalizedString(@"txt_baby_info", nil), [[babyList objectAtIndex:0] objectForKey:@"age"], [[babyList objectAtIndex:0] objectForKey:@"height"], [[babyList objectAtIndex:0] objectForKey:@"weight"], [[babyList objectAtIndex:0] objectForKey:@"blood_type"]];
    }
    else
    {
        int prevIndex = selectedBabyIndex-1;
        prevIndex = (prevIndex < 0)?(int)(babyList.count-1):prevIndex;
        int nextIndex = selectedBabyIndex+1;
        nextIndex = (nextIndex >= babyList.count)?0:nextIndex;
        
        self.babyScrollView.scrollEnabled = YES;
        [self.imgBabyPic1 sd_setImageWithURL:[NSURL URLWithString:[[babyList objectAtIndex:prevIndex] objectForKey:@"photo_url"]] placeholderImage:[UIImage imageNamed:@"dummy.png"]];
        self.txtName1.text = [[babyList objectAtIndex:prevIndex] objectForKey:@"name"];
        self.txtInfo1.text = [NSString stringWithFormat:LocalizedString(@"txt_baby_info", nil), [[babyList objectAtIndex:prevIndex] objectForKey:@"age"], [[babyList objectAtIndex:prevIndex] objectForKey:@"height"], [[babyList objectAtIndex:prevIndex] objectForKey:@"weight"], [[babyList objectAtIndex:prevIndex] objectForKey:@"blood_type"]];
        
        [self.imgBabyPic2 sd_setImageWithURL:[NSURL URLWithString:[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"photo_url"]] placeholderImage:[UIImage imageNamed:@"dummy.png"]];
        self.txtName2.text = [[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"name"];
        self.txtInfo2.text = [NSString stringWithFormat:LocalizedString(@"txt_baby_info", nil), [[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"age"], [[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"height"], [[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"weight"], [[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"blood_type"]];
        
        [self.imgBabyPic3 sd_setImageWithURL:[NSURL URLWithString:[[babyList objectAtIndex:nextIndex] objectForKey:@"photo_url"]] placeholderImage:[UIImage imageNamed:@"dummy.png"]];
        self.txtName3.text = [[babyList objectAtIndex:nextIndex] objectForKey:@"name"];
        self.txtInfo3.text = [NSString stringWithFormat:LocalizedString(@"txt_baby_info", nil), [[babyList objectAtIndex:nextIndex] objectForKey:@"age"], [[babyList objectAtIndex:nextIndex] objectForKey:@"height"], [[babyList objectAtIndex:nextIndex] objectForKey:@"weight"], [[babyList objectAtIndex:nextIndex] objectForKey:@"blood_type"]];
    }
    
    [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"feed_info"]] forKey:@"feed_info"];
    [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"pump_info"]] forKey:@"pump_info"];
    [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"sleep_info"]] forKey:@"sleep_info"];
    [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"poo_info"]] forKey:@"poo_info"];
    [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"wee_info"]] forKey:@"wee_info"];
    [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"medical_info"]] forKey:@"medical_info"];
    [shareObject synchronize];
}

- (void)refreshAction
{
    NSLog(@"selectedBabyIndex=%d", selectedBabyIndex);
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    [shareObject setInteger:[[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"id"] intValue] forKey:@"baby_id"];
    [shareObject setInteger:selectedBabyIndex forKey:@"baby_index"];
    [shareObject synchronize];
    
    offset = 0;
    isLoading = NO;
    photoUrl = nil;
    photoUrlOriginal = nil;
    imgPhoto.image = [UIImage imageNamed:@"bg_photo"];
    
    self.tableView.showsInfiniteScrolling = NO;
    [self getMilestoneList];
}

- (void)getMilestoneList
{
    if(isLoading) return;
    isLoading = YES;
    limit = 30;
    
    NSString *filter = @"";
    NSString *subFilter = @"";
    if (selectedSubFilter.length>0) {
        if ([selectedSubFilter isEqualToString:@"Height"]) {
            subFilter = @"height";
        }else if ([selectedSubFilter isEqualToString:@"Weight"]){
            subFilter = @"weight";
        }else if ([selectedSubFilter isEqualToString:@"Circumference"]){
            subFilter = @"head_size";
        }else if ([selectedSubFilter isEqualToString:@"Teeth"]){
            subFilter = @"teething";
        }else if ([selectedSubFilter isEqualToString:@"Moods"]){
            subFilter = @"moods";
        }else if ([selectedSubFilter isEqualToString:@"Verbal"]){
            subFilter = @"verbal";
        }else if ([selectedSubFilter isEqualToString:@"Learning"]){
            subFilter = @"learning";
        }else if ([selectedSubFilter isEqualToString:@"Vaccine"]){
            subFilter = @"vaccine";
        }else if ([selectedSubFilter isEqualToString:@"Allergies"]){
            subFilter = @"allergies";
        }else if ([selectedSubFilter isEqualToString:@"Symptom"]){
            subFilter = @"symptom";
        }else if ([selectedSubFilter isEqualToString:@"Diagnosis"]){
            subFilter = @"diagnosis";
        }else if ([selectedSubFilter isEqualToString:@"Medication"]){
            subFilter = @"medication";
        }else if ([selectedSubFilter isEqualToString:@"Doctor"]){
            subFilter = @"clinic";
        }
    }
    
    if(selectedFilter == 1) {
        filter = @"grow";
    }
    if(selectedFilter == 2) {
        filter = @"move";
    }
    if(selectedFilter == 3) {
        filter = @"iq";
    }
    if(selectedFilter == 4) {
        filter = @"health";
    }
    if(selectedFilter == 5) {
        filter = @"others";
    }
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setValue:@([shareObject integerForKey:@"user_id"]) forKey:@"user_id"];
    [parameters setValue:@([shareObject integerForKey:@"baby_id"]) forKey:@"baby_id"];
    [parameters setValue:filter forKey:@"name"];
    [parameters setValue:@(offset) forKey:@"offset"];
    [parameters setValue:@(limit) forKey:@"limit"];
    [parameters setValue:subFilter forKey:@"type"];
    
    [[WebServiceManager sharedManager] initWithBaseURL:@"api/getMilestoneList" parameters:parameters success:^(id result) {
        
        if(offset == 0) [milestoneList removeAllObjects];
        NSLog(@"%@",parameters);
        NSMutableArray *list = (NSMutableArray *)[result valueForKey:@"list"];
        
        NSArray * arr = [list valueForKey:@"photo_url"];
        for(int i = 0; i < arr.count ; i++){
            if(![(NSString *)[arr objectAtIndex:i] isNull]){
                NSDictionary * dict = [list objectAtIndex:i];
                
                [ogImgArray removeAllObjects];
                [imgArray removeAllObjects];
                [mediaUrlsArray removeAllObjects];
                
                if(imgArray.count == 0){
                    [imgArray addIfNotNull:[dict valueForKey:@"photo_url"]];
                    [imgArray addIfNotNull:[dict valueForKey:@"video_url"]];
                    [imgArray addIfNotNull:[dict valueForKey:@"photo_url2"]];
                    [imgArray addIfNotNull:[dict valueForKey:@"video_url2"]];
                    [imgArray addIfNotNull:[dict valueForKey:@"photo_url3"]];
                    [imgArray addIfNotNull:[dict valueForKey:@"video_url3"]];

                    [ogImgArray addIfNotNull:[dict valueForKey:@"photo_url_original"]];
                    [ogImgArray addIfNotNull:[dict valueForKey:@"video_url"]];
                    [ogImgArray addIfNotNull:[dict valueForKey:@"photo_url_original2"]];
                    [ogImgArray addIfNotNull:[dict valueForKey:@"video_url2"]];
                    [ogImgArray addIfNotNull:[dict valueForKey:@"photo_url_original3"]];
                    [ogImgArray addIfNotNull:[dict valueForKey:@"video_url3"]];

                    [mediaUrlsArray addIfNotNull:[dict valueForKey:@"photo_url_original"]];
                    [mediaUrlsArray addIfNotNull:[dict valueForKey:@"video_url"]];
                    [mediaUrlsArray addIfNotNull:[dict valueForKey:@"photo_url_original2"]];
                    [mediaUrlsArray addIfNotNull:[dict valueForKey:@"video_url2"]];
                    [mediaUrlsArray addIfNotNull:[dict valueForKey:@"photo_url_original3"]];
                    [mediaUrlsArray addIfNotNull:[dict valueForKey:@"video_url3"]];
                    
                    [_pager reload];
                    
                    break;
                }
            }
        }
        if(list.count > 0)
        {
            if(offset == 0)
            {
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    NSIndexPath *top = [NSIndexPath indexPathForRow:0 inSection:0];
                    [self.tableView scrollToRowAtIndexPath:top atScrollPosition:UITableViewScrollPositionTop animated:YES];
                });
                
                if (![[result valueForKey:@"photo_url"] isKindOfClass:[NSNull class]])  {
                    photoUrl = [result valueForKey:@"photo_url"];
                }
                
                //photoUrlOriginal = [result valueForKey:@"photo_url_original"];
                //photoUrl = [[list firstObject] valueForKey:@"photo_url"];
                //photoUrlOriginal = [[list firstObject] valueForKey:@"photo_url_original"];
                
                NSLog(@"AAphotoUrl=%@", photoUrl);
                if((photoUrl != nil)&&(![photoUrl isEqual:[NSNull null]]))
                {
                    NSLog(@"imgPhoto=%@", imgPhoto);
                    imgPhoto.imageURL = [NSURL URLWithString:photoUrl];
                    //[imgPhoto loadImageWithURL:[NSURL URLWithString:photoUrl]];
                    //btnPhoto.imageView.contentMode = UIViewContentModeScaleAspectFill;
                }else{
                    imgPhoto.image = [UIImage imageNamed:@"bg_photo"];
                    //btnPhoto.imageView.contentMode = UIViewContentModeCenter;
                    //btnPhoto.imageView.image = [UIImage imageNamed:@"dummy"];
                }
            }
            
            self.tableView.showsInfiniteScrolling = YES;
            [milestoneList addObjectsFromArray:list];
            offset += limit;
        }
        else self.tableView.showsInfiniteScrolling = NO;
        
        isLoading = NO;
        [self.tableView reloadData];
        [self.tableView.pullToRefreshView stopAnimating];
        [self.tableView.infiniteScrollingView stopAnimating];
        [list removeAllObjects];
        list = result = nil;
        
        
    } failure:^(NSError *error) {
        isLoading = NO;
        [self.tableView.pullToRefreshView stopAnimating];
        [self.tableView.infiniteScrollingView stopAnimating];
    }];
}

- (void)showPhotoAction:(UIButton *)btn
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Settings" bundle:nil];
    UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"MediaViewerViewController"];
    viewController.data = [[NSMutableDictionary alloc] initWithDictionary:@{@"viewController":self, @"urls":mediaUrlsArray,@"view_check":@(YES)}];
    [self presentViewController:viewController animated:YES completion:nil];

//    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"PhotoViewController"];
//
//    if ((photoUrlOriginal != nil)&&(![photoUrlOriginal isKindOfClass:[NSNull class]])) {
//        NSData * imageData = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:photoUrlOriginal]];
//        UIImage *image = [UIImage imageWithData: imageData];
//        viewController.data = [[NSMutableDictionary alloc] initWithDictionary:@{@"viewController":self, @"image":image, @"view_check":@(YES)}];
//    }
//    else {
//        viewController.data = [[NSMutableDictionary alloc] initWithDictionary:@{@"viewController":self, @"image":imgPhoto.image, @"view_check":@(YES)}];
//    }
//
//    viewController.hidesBottomBarWhenPushed = YES;
//    [self presentViewController:viewController animated:YES completion:nil];
}

- (void)filterAction:(id)sender
{
    if(selectedFilter == 0)
    {
        [UIView animateWithDuration:0.1f delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
            if(self.filterView.frame.origin.y == 0)
            {
                self.filterView.frame = CGRectMake(0, -self.filterView.frame.size.height, self.filterView.frame.size.width, self.filterView.frame.size.height);
                [self hideSubfilter];
            }
            else
            {
                self.filterView.frame = CGRectMake(0, 0, self.filterView.frame.size.width, self.filterView.frame.size.height);
                //[self hideSubfilter];
            }
        } completion:nil];
    }
    else
    {
        self.filterView.frame = CGRectMake(0, -self.filterView.frame.size.height, self.filterView.frame.size.width, self.filterView.frame.size.height);
        [self hideSubfilter];
        
        selectedFilter = 0;
        selectedSubFilter = 0;
        [btnFilter setImage:[UIImage imageNamed:@"nav_btn_filter"] forState:UIControlStateNormal];
        [self refreshAction];
    }
}

- (void)subFilterAction:(id)sender
{
    if(selectedFilter == 0)
    {
        [UIView animateWithDuration:0.1f delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
            if(self.filterView.frame.origin.y == 0)
            {
                self.filterView.frame = CGRectMake(0, -self.filterView.frame.size.height, self.filterView.frame.size.width, self.filterView.frame.size.height);
            }
            else
            {
                self.filterView.frame = CGRectMake(0, 0, self.filterView.frame.size.width, self.filterView.frame.size.height);
            }
        } completion:nil];
    }
    else
    {
        selectedFilter = 0;
        [btnFilter setImage:[UIImage imageNamed:@"nav_btn_filter"] forState:UIControlStateNormal];
        [self refreshAction];
    }
}
- (void)getChecklistAction:(id)sender
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    //Development Calender
    UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"MilestoneChecklistViewController"];
    viewController.data = [[NSMutableDictionary alloc] initWithDictionary:@{@"month":[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"monthsold"]}];
    viewController.hidesBottomBarWhenPushed = YES;
    
    // RS
    CGFloat aspectRatio = self.tableView.frame.size.width/(self.tableView.frame.size.height/2);
    [[NSUserDefaults standardUserDefaults]setFloat:aspectRatio forKey:@"Img_Aspect"];
    
    [self.navigationController pushViewController:viewController animated:YES];
}

- (void)addAction:(id)sender
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"MilestoneAddViewController"];
    viewController.data = [[NSMutableDictionary alloc] initWithDictionary:@{@"name":[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"name"]}];
    
    // RS
    CGFloat aspectRatio = self.tableView.frame.size.width/(self.tableView.frame.size.height/2);
    [[NSUserDefaults standardUserDefaults]setFloat:aspectRatio forKey:@"Img_Aspect"];
    
    viewController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:viewController animated:YES];
}

- (void)getSettingsAction:(id)sender
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Settings" bundle:nil];
    UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"SettingsViewController"];
    viewController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:viewController animated:YES];
}

- (void)getChartAction:(UIButton *)sender
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *viewController;
    
    if([[[milestoneList objectAtIndex:sender.tag] objectForKey:@"type"] isEqualToString:@"teething"] || [[[milestoneList objectAtIndex:sender.tag] objectForKey:@"type"] isEqualToString:@"adult_teething"])
    {
        viewController = [storyboard instantiateViewControllerWithIdentifier:@"MilestonesTeethingChartViewController"];
        viewController.data = [[NSMutableDictionary alloc] initWithDictionary:@{@"type":[[milestoneList objectAtIndex:sender.tag] objectForKey:@"type"], @"created_at":[[milestoneList objectAtIndex:sender.tag] objectForKey:@"created_at"]}];
    }
    else
    {
        viewController = [storyboard instantiateViewControllerWithIdentifier:@"MilestonesChartViewController"];
        viewController.data = [[NSMutableDictionary alloc] initWithDictionary:@{@"type":[[milestoneList objectAtIndex:sender.tag] objectForKey:@"type"], @"gender":[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"gender"], @"month":[[milestoneList objectAtIndex:sender.tag] objectForKey:@"month"]}];
    }
    
    viewController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:viewController animated:YES];
}

- (IBAction)getGrowAction:(UIButton *)sender {
    
    sender.selected = !sender.selected;

    subfilterFields = [[NSMutableArray alloc] initWithObjects:@"Height",@"Weight",@"Circumference",@"Teeth", nil];
    [btnFilter setImage:[UIImage imageNamed:@"icon_grow_filter"] forState:UIControlStateNormal];
    
    if(!sender.selected){
        [self hideSubfilter];
        self.filterView.frame = CGRectMake(0, -self.filterView.frame.size.height, self.filterView.frame.size.width, self.filterView.frame.size.height);
    }
    
    else{
        [self showSubfilter];
    }
    
    selectedFilter = 1;
    selectedSubFilter = 0;
    [self refreshAction];
    [self.subFilterTblView reloadData];
    
}

- (IBAction)getMoveAction:(UIButton *)sender {
    
    [self hideSubfilter];
    
    [btnFilter setImage:[UIImage imageNamed:@"icon_move_filter"] forState:UIControlStateNormal];
    self.filterView.frame = CGRectMake(0, -self.filterView.frame.size.height, self.filterView.frame.size.width, self.filterView.frame.size.height);
    
    selectedFilter = 2;
    selectedSubFilter = 0;
    [self refreshAction];
}


- (IBAction)getIQAction:(UIButton *)sender {
    
    sender.selected = !sender.selected;
    
    subfilterFields = [[NSMutableArray alloc] initWithObjects:@"Moods",@"Verbal",@"Learning", nil];
    [btnFilter setImage:[UIImage imageNamed:@"icon_iq_filter"] forState:UIControlStateNormal];
    
    if(!sender.selected){
        [self hideSubfilter];
        self.filterView.frame = CGRectMake(0, -self.filterView.frame.size.height, self.filterView.frame.size.width, self.filterView.frame.size.height);
    }
    
    else{
        [self showSubfilter];
    }
    
    selectedFilter = 3;
    selectedSubFilter = 0;
    [self refreshAction];
    [self.subFilterTblView reloadData];
}

- (IBAction)getHealthAction:(UIButton *)sender {
    
    sender.selected = !sender.selected;
    
    subfilterFields = [[NSMutableArray alloc] initWithObjects:@"Vaccine",@"Allergies",@"Symptom", @"Diagnosis",@"Medication",@"Doctor", nil];
    [btnFilter setImage:[UIImage imageNamed:@"icon_health_filter"] forState:UIControlStateNormal];
    
    if(!sender.selected){
        [self hideSubfilter];
        self.filterView.frame = CGRectMake(0, -self.filterView.frame.size.height, self.filterView.frame.size.width, self.filterView.frame.size.height);
    }
    
    else{
        [self showSubfilter];
    }
    
    selectedFilter = 4;
    selectedSubFilter = 0;
    [self refreshAction];
    [self.subFilterTblView reloadData];
}

- (IBAction)getOthersAction:(UIButton *)sender {
    
    [self hideSubfilter];
    
    [btnFilter setImage:[UIImage imageNamed:@"icon_others_filter"] forState:UIControlStateNormal];
    self.filterView.frame = CGRectMake(0, -self.filterView.frame.size.height, self.filterView.frame.size.width, self.filterView.frame.size.height);
    
    selectedFilter = 5;
    selectedSubFilter = 0;
    [self refreshAction];
}


- (IBAction)getBabyInfoAction:(id)sender
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Settings" bundle:nil];
    UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"SettingsBabyViewController"];
    viewController.data = [[babyList objectAtIndex:selectedBabyIndex] mutableCopy];
    viewController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:viewController animated:YES];
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (tableView == self.subFilterTblView) {
        if(subfilterFields.count == 0) return 0;
        return 1;
    }else {
        if(milestoneList.count == 0) return 0;
        return 1;
    }
    
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return nil;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (tableView == self.subFilterTblView) {
        return nil;
    }else {
        if(!headerView)
        {
            headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, self.tableView.frame.size.height/2)];
            
            _pager = [[KDViewPager alloc] initWithController:self inView:headerView];
            _pager.datasource = self;
            [_pager.pagerView setBackgroundColor:[UIColor whiteColor]];
            _pager.delegate = self;
            
//            [self createScrollPages];
        }
        return headerView;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (tableView == self.subFilterTblView) {
        return 0;
    }else {
        return self.tableView.frame.size.height/2;
    }
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == self.subFilterTblView) {
        if(subfilterFields.count == 0) return 0;
        return subfilterFields.count;
    }else {
        if(milestoneList.count == 0) return 0;
        return milestoneList.count;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}
- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == self.subFilterTblView) {
        MilestoneSubfilterTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        selectedSubFilter = cell.lblTitle.text;
        [self hideSubfilter];
        [self refreshAction];
        self.filterView.frame = CGRectMake(0, -self.filterView.frame.size.height, self.filterView.frame.size.width, self.filterView.frame.size.height);
        
    }else {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        NSString *type = [[milestoneList objectAtIndex:indexPath.row] objectForKey:@"type"];
        
        if([type isEqualToString:@"birthday"] || [type isEqualToString:@"birth_height"] || [type isEqualToString:@"birth_weight"] || [type isEqualToString:@"birth_head_size"])
        {
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Settings" bundle:nil];
            UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"SettingsBabyViewController"];
            viewController.data = [[babyList objectAtIndex:selectedBabyIndex] mutableCopy];
            viewController.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:viewController animated:YES];
        }
        else
        {
            if([[[milestoneList objectAtIndex:indexPath.row] objectForKey:@"reference_id"] intValue] != 0 && ![type isEqualToString:@"teething"] && ![type isEqualToString:@"reminder"] && ![type isEqualToString:@"adult_teething"])
            {
                UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"MilestoneChecklistEditViewController"];
                viewController.data = [[NSMutableDictionary alloc] initWithDictionary:@{@"milestone_info":[milestoneList objectAtIndex:indexPath.row]}];
                
                // RS
                CGFloat aspectRatio = self.tableView.frame.size.width/(self.tableView.frame.size.height/2);
                [[NSUserDefaults standardUserDefaults]setFloat:aspectRatio forKey:@"Img_Aspect"];
                
                viewController.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:viewController animated:YES];
            }
            else
            {
                UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"MilestoneEditViewController"];
                viewController.data = [[NSMutableDictionary alloc] initWithDictionary:@{@"name":[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"name"], @"milestone_info":[milestoneList objectAtIndex:indexPath.row]}];
                
                // RS
                CGFloat aspectRatio = self.tableView.frame.size.width/(self.tableView.frame.size.height/2);
                [[NSUserDefaults standardUserDefaults]setFloat:aspectRatio forKey:@"Img_Aspect"];
                viewController.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:viewController animated:YES];
            }
        }
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == self.subFilterTblView)
    {
        if(subfilterFields.count == 0)
        {
            EmptyTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"EmptyTableViewCell"];
            if(!cell)
            {
                [tableView registerNib:[UINib nibWithNibName:@"EmptyTableViewCell" bundle:nil] forCellReuseIdentifier:@"EmptyTableViewCell"];
                cell = [tableView dequeueReusableCellWithIdentifier:@"EmptyTableViewCell"];
            }
            return cell;
        }
        else
        {
            MilestoneSubfilterTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MilestoneSubfilterTableViewCell"];
            if(!cell)
            {
                [tableView registerNib:[UINib nibWithNibName:@"MilestoneSubfilterTableViewCell" bundle:nil] forCellReuseIdentifier:@"MilestoneSubfilterTableViewCell"];
                cell = [tableView dequeueReusableCellWithIdentifier:@"MilestoneSubfilterTableViewCell"];
            }
            
            cell.lblTitle.text = [subfilterFields objectAtIndex:indexPath.row];
            
            return cell;
        }
    }
    else
    {
        if(milestoneList.count == 0)
        {
            EmptyTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"EmptyTableViewCell"];
            if(!cell)
            {
                [tableView registerNib:[UINib nibWithNibName:@"EmptyTableViewCell" bundle:nil] forCellReuseIdentifier:@"EmptyTableViewCell"];
                cell = [tableView dequeueReusableCellWithIdentifier:@"EmptyTableViewCell"];
            }
            
            return cell;
        }
        else
        {
            NSString *name = [[milestoneList objectAtIndex:indexPath.row] objectForKey:@"name"];
            NSString *type = [[milestoneList objectAtIndex:indexPath.row] objectForKey:@"type"];
            
            if([name isEqualToString:@"grow"])
            {
                MilestonesGrowTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MilestonesGrowTableViewCell"];
                if(!cell)
                {
                    [tableView registerNib:[UINib nibWithNibName:@"MilestonesGrowTableViewCell" bundle:nil] forCellReuseIdentifier:@"MilestonesGrowTableViewCell"];
                    cell = [tableView dequeueReusableCellWithIdentifier:@"MilestonesGrowTableViewCell"];
                }
                
                if([type isEqualToString:@"weight"])
                {
                    [[milestoneList objectAtIndex:indexPath.row] setObject:[self convertUnit:[[milestoneList objectAtIndex:indexPath.row] objectForKey:@"attribute_value"] unit:[unitInfo objectForKey:@"weight"] spacing:YES] forKey:@"attribute_value"];
                    
                }
                else if([type isEqualToString:@"height"])
                {
                    [[milestoneList objectAtIndex:indexPath.row] setObject:[self convertUnit:[[milestoneList objectAtIndex:indexPath.row] objectForKey:@"attribute_value"] unit:[unitInfo objectForKey:@"height"] spacing:YES] forKey:@"attribute_value"];
                }
                else if([type isEqualToString:@"head_size"])
                {
                    [[milestoneList objectAtIndex:indexPath.row] setObject:[self convertUnit:[[milestoneList objectAtIndex:indexPath.row] objectForKey:@"attribute_value"] unit:[unitInfo objectForKey:@"head_size"] spacing:YES] forKey:@"attribute_value"];
                }
                
                if([type isEqualToString:@"birthday"])
                {
                    cell.btnChart.enabled = NO;
                    cell.btnChart.hidden = YES;
                    cell.imgIcon.backgroundColor = UIColorFromRGB(0xB9CDDA);
                    cell.imgIconChevronRight.tintColor = UIColorFromRGB(0xB9CDDA);
                }
                else
                {
                    cell.btnChart.enabled = YES;
                    cell.btnChart.hidden = NO;
                    cell.imgIcon.backgroundColor = UIColorFromRGB(0xF3E99F);
                    cell.imgIconChevronRight.tintColor = UIColorFromRGB(0xF3E99F);
                }
                
                UIImage *imgIcon = [UIImage imageNamed:[NSString stringWithFormat:@"icon_%@_Selected.png", name]];
                if([type isEqualToString:@"birthday"] || [type isEqualToString:@"birth_height"] || [type isEqualToString:@"birth_weight"] || [type isEqualToString:@"birth_head_size"])
                {
                    imgIcon = [UIImage imageNamed:@"icon_birthday"];
                }
                
                cell.imgIconWidthConstraint.constant = imgIcon.size.width;
                cell.imgIconHeightConstraint.constant = imgIcon.size.height;
                if (IS_IPAD) {
                    cell.imgIconWidthConstraint.constant = cell.imgIconWidthConstraint.constant + 12;
                    cell.imgIconHeightConstraint.constant = cell.imgIconHeightConstraint.constant + 12;
                }
                cell.imgIcon.image = imgIcon;
                cell.imgIcon.layer.cornerRadius = cell.imgIconWidthConstraint.constant/2;
                
                cell.txtName.text = [COMMON_HELPER checkForNullValue:[[milestoneList objectAtIndex:indexPath.row] objectForKey:@"attribute_name"]];
                cell.txtValue.text = [COMMON_HELPER checkForNullValue:[[milestoneList objectAtIndex:indexPath.row] objectForKey:@"attribute_value"]];
                
                cell.txtNotes.text = [COMMON_HELPER checkForNullValue:[[milestoneList objectAtIndex:indexPath.row] objectForKey:@"notes"]];
                if ([type isEqualToString:@"adult_teething"] || [type isEqualToString:@"teething"]) {
                    cell.txtNotes.text = [COMMON_HELPER checkForNullValue:[[milestoneList objectAtIndex:indexPath.row] objectForKey:@"tooth_name"]];
                }
                
                cell.txtDate.text = [COMMON_HELPER checkForNullValue:[[milestoneList objectAtIndex:indexPath.row] objectForKey:@"date"]];
                
                cell.tag = indexPath.row;
                
                if([type isEqualToString:@"birthday"])
                {
                    cell.txtValue.text = LocalizedString(@"txt_welcome", nil);
                }
                
                CGFloat timeFontSize = 20.0f;
                CGFloat timeSuffixFontSize = 12.0f;
                if (IS_IPAD) {
                    timeFontSize = 26.0;
                    timeSuffixFontSize = 18.0;
                }
                
                NSString *time = [NSString stringWithFormat:@"%@",[[milestoneList objectAtIndex:indexPath.row] objectForKey:@"time"]];
                NSMutableAttributedString *timeString = [[NSMutableAttributedString alloc] initWithString:time attributes:@{NSFontAttributeName:[UIFont fontWithName:@"SFUIDisplay-Medium" size:timeFontSize]}];
                
                if([[shareObject objectForKey:@"language"] hasPrefix:@"zh"])
                {
                    NSArray *timeArray = [time componentsSeparatedByString:@" "];
                    NSLog(@"timeArray=%@", timeArray);
                    time = [NSString stringWithFormat:@"%@ %@", LocalizedString(timeArray[1], nil), timeArray[0]];
                    timeString = [[NSMutableAttributedString alloc] initWithString:time attributes:@{NSFontAttributeName:[UIFont fontWithName:@"SFUIDisplay-Medium" size:timeFontSize]}];
                    [timeString setAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"SFUIDisplay-Medium" size:timeSuffixFontSize], NSBaselineOffsetAttributeName:@(4)} range:NSMakeRange(0,2)];
                }
                else
                {
                    [timeString setAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"SFUIDisplay-Medium" size:timeSuffixFontSize]} range:NSMakeRange(6,2)];
                }
                [cell.txtTime setAttributedText:timeString];
                
                cell.btnChart.tag = indexPath.row;
                [cell.btnChart addTarget:self action:@selector(getChartAction:) forControlEvents:UIControlEventTouchUpInside];
                
                if(indexPath.row == 0){
                    cell.imgIcon.transform = CGAffineTransformMakeScale(1.5, 1.5);
                }
                else {
                    cell.imgIcon.transform = CGAffineTransformMakeScale(1, 1);
                }
                [cell layoutSubviews];
                
                return cell;
            }
            else
            {
                MilestonesTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MilestonesTableViewCell"];
                if(!cell)
                {
                    [tableView registerNib:[UINib nibWithNibName:@"MilestonesTableViewCell" bundle:nil] forCellReuseIdentifier:@"MilestonesTableViewCell"];
                    cell = [tableView dequeueReusableCellWithIdentifier:@"MilestonesTableViewCell"];
                }
                
                if([name isEqualToString:@"move"])
                {
                    cell.imgIcon.backgroundColor = UIColorFromRGB(0xFFC896);
                    cell.imgIconChevronRight.tintColor = UIColorFromRGB(0xFFC896);
                }
                if([name isEqualToString:@"iq"])
                {
                    cell.imgIcon.backgroundColor = UIColorFromRGB(0xC5DEAA);
                    cell.imgIconChevronRight.tintColor = UIColorFromRGB(0xC5DEAA);
                }
                if([name isEqualToString:@"health"])
                {
                    cell.imgIcon.backgroundColor = UIColorFromRGB(0xFACFD4);
                    cell.imgIconChevronRight.tintColor = UIColorFromRGB(0xFACFD4);
                }
                if([name isEqualToString:@"others"])
                {
                    cell.imgIcon.backgroundColor = UIColorFromRGB(0xDFCCDC);
                    cell.imgIconChevronRight.tintColor = UIColorFromRGB(0xDFCCDC);
                }
                if ([name isEqualToString:@"pregnancy"]) {
                    cell.imgIcon.backgroundColor = UIColorFromRGB(0xDCC4E8);
                    cell.imgIconChevronRight.tintColor = UIColorFromRGB(0xDCC4E8);
                }
                
                UIImage *imgIcon = [UIImage imageNamed:[NSString stringWithFormat:@"icon_%@_Selected.png", name]];
                if([type isEqualToString:@"reminder"])
                {
                    imgIcon = [UIImage imageNamed:[NSString stringWithFormat:@"icon_%@.png", type]];
                }
                
                cell.imgIconWidthConstraint.constant = imgIcon.size.width;
                cell.imgIconHeightConstraint.constant = imgIcon.size.height;
                if (IS_IPAD) {
                    cell.imgIconWidthConstraint.constant = cell.imgIconWidthConstraint.constant + 12;
                    cell.imgIconHeightConstraint.constant = cell.imgIconHeightConstraint.constant + 12;
                }
                cell.imgIcon.image = imgIcon;
                cell.imgIcon.layer.cornerRadius = cell.imgIconWidthConstraint.constant/2;
                
                cell.txtName.text = [COMMON_HELPER checkForNullValue:[[milestoneList objectAtIndex:indexPath.row] objectForKey:@"attribute_name"]];
                cell.txtValue.text = [COMMON_HELPER checkForNullValue:[[milestoneList objectAtIndex:indexPath.row] objectForKey:@"attribute_value"]];
                cell.txtNotes.text = [COMMON_HELPER checkForNullValue:[[milestoneList objectAtIndex:indexPath.row] objectForKey:@"notes"]];
                cell.txtDate.text = [COMMON_HELPER checkForNullValue:[[milestoneList objectAtIndex:indexPath.row] objectForKey:@"date"]];
                
                
                cell.tag = indexPath.row;
                
                if([type isEqualToString:@"reminder"])
                {
                    formatter.dateFormat = @"yyyy-MM-dd HH:mm:ss ZZZ";
                    NSDate *reminderDateTime = [formatter dateFromString:[[milestoneList objectAtIndex:indexPath.row] objectForKey:@"reminder_time"]];
                    
                    formatter.dateFormat = @"yyyy-MM-dd hh:mm a";
                    cell.txtNotes.text = [NSString stringWithFormat:@"%@", [formatter stringFromDate:reminderDateTime]];
                    //cell.txtName.text = @"Medical Reminder";
                }
                
                CGFloat timeFontSize = 20.0f;
                CGFloat timeSuffixFontSize = 12.0f;
                if (IS_IPAD) {
                    timeFontSize = 26.0;
                    timeSuffixFontSize = 18.0;
                }
                
                NSString *time = [NSString stringWithFormat:@"%@",[[milestoneList objectAtIndex:indexPath.row] objectForKey:@"time"]];
                
                NSMutableAttributedString *timeString = [[NSMutableAttributedString alloc] initWithString:time attributes:@{NSFontAttributeName:[UIFont fontWithName:@"SFUIDisplay-Medium" size:timeFontSize]}];
                
                if([[shareObject objectForKey:@"language"] hasPrefix:@"zh"])
                {
                    NSArray *timeArray = [time componentsSeparatedByString:@" "];
                    NSLog(@"timeArray=%@", timeArray);
                    time = [NSString stringWithFormat:@"%@ %@", LocalizedString(timeArray[1], nil), timeArray[0]];
                    timeString = [[NSMutableAttributedString alloc] initWithString:time attributes:@{NSFontAttributeName:[UIFont fontWithName:@"SFUIDisplay-Medium" size:timeFontSize]}];
                    [timeString setAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"SFUIDisplay-Medium" size:timeSuffixFontSize], NSBaselineOffsetAttributeName:@(4)} range:NSMakeRange(0,2)];
                }
                else
                {
                    [timeString setAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"SFUIDisplay-Medium" size:timeSuffixFontSize]} range:NSMakeRange(6,2)];
                }
                [cell.txtTime setAttributedText:timeString];
                
                if(indexPath.row == 0) {
                    cell.imgIcon.transform = CGAffineTransformMakeScale(1.5, 1.5);
                }else {
                    cell.imgIcon.transform = CGAffineTransformMakeScale(1, 1);
                }
                [cell layoutSubviews];
                
                // Debug
                /*NSString *tempPhotoUrl = [[milestoneList objectAtIndex:indexPath.row] objectForKey:@"photo_url"];
                 if(![tempPhotoUrl isEqual:[NSNull null]])
                 {
                 cell.imgIcon.contentMode = UIViewContentModeScaleAspectFit;
                 [cell.imgIcon sd_setImageWithURL:[NSURL URLWithString:tempPhotoUrl]];
                 }
                 else
                 {
                 cell.imgIcon.image = imgIcon;
                 }*/
                
                return cell;
            }
        }
    }
}


#pragma mark -
#pragma mark UIScrollViewDelegate Methods
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if([scrollView isEqual:self.babyScrollView])
    {
        CGFloat pageWidth = self.view.bounds.size.width;
        int pageNo = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
        NSLog(@"pageNo=%d", pageNo);
        NSLog(@"backupPageNo=%d", backupPageNo);
        
        if(pageNo == 0)
        {
            NSLog(@"selectedBabyIndex=%d", selectedBabyIndex);
            if(backupPageNo != pageNo)
            {
                int nextIndex = selectedBabyIndex;
                nextIndex = (nextIndex >= babyList.count)?0:nextIndex;
                selectedBabyIndex--;
                selectedBabyIndex = (selectedBabyIndex < 0)?(int)(babyList.count-1):selectedBabyIndex;
                int prevIndex = selectedBabyIndex-1;
                prevIndex = (prevIndex < 0)?(int)(babyList.count-1):prevIndex;
                
                NSLog(@"selectedBabyIndex=%d", selectedBabyIndex);
                NSLog(@"prevIndex=%d", prevIndex);
                NSLog(@"nextIndex=%d", nextIndex);
                
                [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"feed_info"]] forKey:@"feed_info"];
                [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"pump_info"]] forKey:@"pump_info"];
                [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"sleep_info"]] forKey:@"sleep_info"];
                [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"poo_info"]] forKey:@"poo_info"];
                [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"wee_info"]] forKey:@"wee_info"];
                [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"medical_info"]] forKey:@"medical_info"];
                [shareObject synchronize];
                
                [self.imgBabyPic1 sd_setImageWithURL:[NSURL URLWithString:[[babyList objectAtIndex:prevIndex] objectForKey:@"photo_url"]] placeholderImage:[UIImage imageNamed:@"dummy.png"]];
                self.txtName1.text = [[babyList objectAtIndex:prevIndex] objectForKey:@"name"];
                self.txtInfo1.text = [NSString stringWithFormat:LocalizedString(@"txt_baby_info", nil), [[babyList objectAtIndex:prevIndex] objectForKey:@"age"], [[babyList objectAtIndex:prevIndex] objectForKey:@"height"], [[babyList objectAtIndex:prevIndex] objectForKey:@"weight"], [[babyList objectAtIndex:prevIndex] objectForKey:@"blood_type"]];
                
                [self.imgBabyPic2 sd_setImageWithURL:[NSURL URLWithString:[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"photo_url"]] placeholderImage:[UIImage imageNamed:@"dummy.png"]];
                self.txtName2.text = [[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"name"];
                self.txtInfo2.text = [NSString stringWithFormat:LocalizedString(@"txt_baby_info", nil), [[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"age"], [[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"height"], [[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"weight"], [[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"blood_type"]];
                
                [self.imgBabyPic3 sd_setImageWithURL:[NSURL URLWithString:[[babyList objectAtIndex:nextIndex] objectForKey:@"photo_url"]] placeholderImage:[UIImage imageNamed:@"dummy.png"]];
                self.txtName3.text = [[babyList objectAtIndex:nextIndex] objectForKey:@"name"];
                self.txtInfo3.text = [NSString stringWithFormat:LocalizedString(@"txt_baby_info", nil), [[babyList objectAtIndex:nextIndex] objectForKey:@"age"], [[babyList objectAtIndex:nextIndex] objectForKey:@"height"], [[babyList objectAtIndex:nextIndex] objectForKey:@"weight"], [[babyList objectAtIndex:nextIndex] objectForKey:@"blood_type"]];
                
                self.babyScrollView.contentOffset = CGPointMake(self.view.frame.size.width, self.babyScrollView.contentOffset.y);
                backupPageNo = 1;
                
                [self refreshAction];
            }
        }
        else if(pageNo == 2)
        {
            if(backupPageNo != pageNo)
            {
                int prevIndex = selectedBabyIndex;
                prevIndex = (prevIndex < 0)?(int)(babyList.count-1):prevIndex;
                selectedBabyIndex++;
                selectedBabyIndex = (selectedBabyIndex >= babyList.count)?0:selectedBabyIndex;
                int nextIndex = selectedBabyIndex+1;
                nextIndex = (nextIndex >= babyList.count)?0:nextIndex;
                
                NSLog(@"selectedBabyIndex=%d", selectedBabyIndex);
                NSLog(@"prevIndex=%d", prevIndex);
                NSLog(@"nextIndex=%d", nextIndex);
                
                [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"feed_info"]] forKey:@"feed_info"];
                [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"pump_info"]] forKey:@"pump_info"];
                [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"sleep_info"]] forKey:@"sleep_info"];
                [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"poo_info"]] forKey:@"poo_info"];
                [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"wee_info"]] forKey:@"wee_info"];
                [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"medical_info"]] forKey:@"medical_info"];
                [shareObject synchronize];
                
                [self.imgBabyPic1 sd_setImageWithURL:[NSURL URLWithString:[[babyList objectAtIndex:prevIndex] objectForKey:@"photo_url"]] placeholderImage:[UIImage imageNamed:@"dummy.png"]];
                self.txtName1.text = [[babyList objectAtIndex:prevIndex] objectForKey:@"name"];
                self.txtInfo1.text = [NSString stringWithFormat:LocalizedString(@"txt_baby_info", nil), [[babyList objectAtIndex:prevIndex] objectForKey:@"age"], [[babyList objectAtIndex:prevIndex] objectForKey:@"height"], [[babyList objectAtIndex:prevIndex] objectForKey:@"weight"], [[babyList objectAtIndex:prevIndex] objectForKey:@"blood_type"]];
                
                [self.imgBabyPic2 sd_setImageWithURL:[NSURL URLWithString:[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"photo_url"]] placeholderImage:[UIImage imageNamed:@"dummy.png"]];
                self.txtName2.text = [[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"name"];
                self.txtInfo2.text = [NSString stringWithFormat:LocalizedString(@"txt_baby_info", nil), [[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"age"], [[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"height"], [[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"weight"], [[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"blood_type"]];
                
                [self.imgBabyPic3 sd_setImageWithURL:[NSURL URLWithString:[[babyList objectAtIndex:nextIndex] objectForKey:@"photo_url"]] placeholderImage:[UIImage imageNamed:@"dummy.png"]];
                self.txtName3.text = [[babyList objectAtIndex:nextIndex] objectForKey:@"name"];
                self.txtInfo3.text = [NSString stringWithFormat:LocalizedString(@"txt_baby_info", nil), [[babyList objectAtIndex:nextIndex] objectForKey:@"age"], [[babyList objectAtIndex:nextIndex] objectForKey:@"height"], [[babyList objectAtIndex:nextIndex] objectForKey:@"weight"], [[babyList objectAtIndex:nextIndex] objectForKey:@"blood_type"]];
                
                self.babyScrollView.contentOffset = CGPointMake(self.view.frame.size.width, self.babyScrollView.contentOffset.y);
                backupPageNo = 1;
                
                [self refreshAction];
            }
        }
        else backupPageNo = pageNo;
        
        // Check for Subscription of selected baby
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"is_Subscription_Enabled"];
        if (![[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"subscription"] isKindOfClass:[NSNull class]]) {
            BOOL isSubscribed = [[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"subscription"] boolValue];
            [[NSUserDefaults standardUserDefaults]setBool:isSubscribed forKey:@"is_Subscription_Enabled"];
        }
    }
//    else if ([scrollView isEqual:viewPortScroll])
//    {
//        int indexOfPage = scrollView.contentOffset.x / scrollView.frame.size.width;
//        if (prevPageIndex == indexOfPage) {
//            return;
//        }
//
//        pgControl.currentPage = indexOfPage;
//        prevPageIndex = indexOfPage;
//        NSArray *arr = [viewPortScroll subviews];
//        UIView *v = [arr objectAtIndex:indexOfPage];
//
//        NSArray *imgExt = [NSArray arrayWithObjects:@"mp4",@"mov",@"m3u8", nil];
//        NSString *urlStr = [mediaUrlsArray objectAtIndex:indexOfPage];
//        NSURL *url = [NSURL URLWithString:urlStr];
//        NSString *pathExt = [url pathExtension];
//
//        if ([imgExt containsObject:pathExt]) {
//            //video
//            [vManager removePlayerInstance];
//            vManager = [VideoPlayerManager new];
//            vManager.videoUrl = urlStr;
//            [vManager initilizePlayerwithView:v andController:self];
//            [vManager.player pause];
//        }else {
//            //image
//            AsyncImageView *imgView = [[AsyncImageView alloc] initWithFrame:CGRectMake(0, 0, v.frame.size.width, v.frame.size.height)];
//            imgView.contentMode = UIViewContentModeScaleAspectFit;
//            imgView.imageURL = [NSURL URLWithString:urlStr];
//            [v addSubview:imgView];
//        }
//
//    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if([scrollView isEqual:self.tableView])
    {
        if(milestoneList.count > 0 && !isLoading)
        {
            NSIndexPath *indexPath2 = [[self.tableView indexPathsForVisibleRows] objectAtIndex:0];
            NSLog(@"indexPath2=%ld", (long)indexPath2.row);
            
            NSUInteger row = ceil(self.tableView.contentOffset.y/78);
            if(indexPath2.row > 4) row = (int)indexPath2.row+4;
            if(row >= milestoneList.count) row = milestoneList.count-1;
            
            NSLog(@"row=%lu", (unsigned long)row);
            NSLog(@"self.tableView.contentOffset.y=%f", self.tableView.contentOffset.y);
            
            for(UITableViewCell *tempCell in self.tableView.visibleCells)
            {
                if([tempCell isKindOfClass:[MilestonesTableViewCell class]])
                {
                    MilestonesTableViewCell *cell = (MilestonesTableViewCell *)tempCell;
                    cell.imgIcon.transform = CGAffineTransformMakeScale(1, 1);
                }
                if([tempCell isKindOfClass:[MilestonesGrowTableViewCell class]])
                {
                    MilestonesGrowTableViewCell *cell = (MilestonesGrowTableViewCell *)tempCell;
                    cell.imgIcon.transform = CGAffineTransformMakeScale(1, 1);
                }
            }
            
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:0];
            MilestonesTableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
            NSLog(@"cell.frame.size.height=%f", cell.frame.size.height);
            cell.imgIcon.transform = CGAffineTransformMakeScale(1.5, 1.5);
            
            
            NSDictionary * milestone = [milestoneList objectAtIndex:indexPath.row];
            
            if([milestone hasValue:@"photo_url"] || [milestone hasValue:@"photo_url2"] ||
               [milestone hasValue:@"photo_url3"] || [milestone hasValue:@"video_url"] || [milestone hasValue:@"video_url2"] ||
               [milestone hasValue:@"video_url3"]){
                
                [imgArray removeAllObjects];
                [ogImgArray removeAllObjects];
                [mediaUrlsArray removeAllObjects];
                
                [imgArray addIfNotNull:[[milestoneList objectAtIndex:indexPath.row] objectForKey:@"photo_url"]];
                [imgArray addIfNotNull:[[milestoneList objectAtIndex:indexPath.row] valueForKey:@"video_url"]];
                [imgArray addIfNotNull:[[milestoneList objectAtIndex:indexPath.row] objectForKey:@"photo_url2"]];
                [imgArray addIfNotNull:[[milestoneList objectAtIndex:indexPath.row] valueForKey:@"video_url2"]];
                [imgArray addIfNotNull:[[milestoneList objectAtIndex:indexPath.row] objectForKey:@"photo_url3"]];
                [imgArray addIfNotNull:[[milestoneList objectAtIndex:indexPath.row] valueForKey:@"video_url3"]];

                [ogImgArray addIfNotNull:[[milestoneList objectAtIndex:indexPath.row] objectForKey:@"photo_url_original"]];
                [ogImgArray addIfNotNull:[[milestoneList objectAtIndex:indexPath.row] valueForKey:@"video_url"]];
                [ogImgArray addIfNotNull:[[milestoneList objectAtIndex:indexPath.row] objectForKey:@"photo_url_original2"]];
                [ogImgArray addIfNotNull:[[milestoneList objectAtIndex:indexPath.row] valueForKey:@"video_url2"]];
                [ogImgArray addIfNotNull:[[milestoneList objectAtIndex:indexPath.row] objectForKey:@"photo_url_original3"]];
                [ogImgArray addIfNotNull:[[milestoneList objectAtIndex:indexPath.row] valueForKey:@"video_url3"]];

                [mediaUrlsArray addIfNotNull:[[milestoneList objectAtIndex:indexPath.row] objectForKey:@"photo_url_original"]];
                [mediaUrlsArray addIfNotNull:[[milestoneList objectAtIndex:indexPath.row] valueForKey:@"video_url"]];
                [mediaUrlsArray addIfNotNull:[[milestoneList objectAtIndex:indexPath.row] objectForKey:@"photo_url_original2"]];
                [mediaUrlsArray addIfNotNull:[[milestoneList objectAtIndex:indexPath.row] valueForKey:@"video_url2"]];
                [mediaUrlsArray addIfNotNull:[[milestoneList objectAtIndex:indexPath.row] objectForKey:@"photo_url_original3"]];
                [mediaUrlsArray addIfNotNull:[[milestoneList objectAtIndex:indexPath.row] valueForKey:@"video_url3"]];

            }
            
            //[self createScrollPages];
            [_pager reload];
            
        }
    }
}

//- (void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView
//{
//    if ([scrollView isEqual:viewPortScroll])
//    {
//        NSLog(@"scrollViewWillBeginDecelerating");
//        int indexOfPage = scrollView.contentOffset.x / scrollView.frame.size.width;
//        if (prevPageIndex != indexOfPage) {
//            [vManager removePlayerInstance];
//        }
//    }
//}

#pragma mark - Subfilters
-(void)hideSubfilter
{
    self.subFilterTblView.hidden = YES;
    [UIView animateWithDuration:0.2 animations:^{
        self.subFilterTblViewHeight.constant = 0;
    }];
    
}

-(void)showSubfilter
{
    self.subFilterTblView.hidden = NO;
    CGFloat h = 124.0;
    if (IS_IPAD) {
        h = 150.0;
    }
    [UIView animateWithDuration:0.2 animations:^{
        self.subFilterTblViewHeight.constant = h;
    }];
    
}

#pragma mark - datasource
-(UIViewController *)kdViewPager:(KDViewPager *)viewPager controllerAtIndex:(NSUInteger)index cachedController:(ContentViewController *)cachedController {
    
    NSString * imgURL = @"";
    NSString * ogImg = @"";
    
    if(imgArray.count > 0){
        if (index<imgArray.count) {
            imgURL = [imgArray objectAtIndex:index];
        }
    }
    
    if(ogImgArray.count > 0){
        if (index<ogImgArray.count) {
            ogImg = [ogImgArray objectAtIndex:index];
        }
    }
    
    if (cachedController == nil) {
        cachedController = [[ContentViewController alloc] initWithImage:imgURL withFrame:self.tableView.frame];
        cachedController.delegate = self;
    }
    else {
        NSArray *imgExt = [NSArray arrayWithObjects:@"mp4",@"mov",@"m3u8", nil];
        NSURL *mediaUrl = [NSURL URLWithString:imgURL];
        NSString *pathExt = [mediaUrl pathExtension];
        
        if ([imgExt containsObject:pathExt]) {
            //video
            [cachedController setVideo:imgURL];
        }else {
            //image
            [cachedController setImage:imgURL];
        }
    }
    cachedController.originalImage = ogImg;
    
    // For page count Info
    _pager.pgCntrlNoOfPages = imgArray.count;
    [_pager setpPageControlView];
    [cachedController.view bringSubviewToFront:viewPager.pgCountView];
    
    return cachedController;
}

-(NSUInteger)numberOfPages:(KDViewPager *)viewPager {
    
    if(imgArray.count == 0) {
        return 1;
    }
    return imgArray.count;
    
}

#pragma mark - delegate
-(void)kdViewpager:(KDViewPager *)viewPager didSelectPage:(NSUInteger)index direction:(UIPageViewControllerNavigationDirection)direction {
    //NSLog(@"didSelectpage: %lu direction: %lu", index, direction);
    dispatch_async(dispatch_get_main_queue(), ^{
    _pager.pgCountView.text = [NSString stringWithFormat:@"%d/%d",(int)index+1,(int)imgArray.count];
    });
}

-(void)kdViewpager:(KDViewPager *)viewPager willSelectPage:(NSUInteger)index direction:(UIPageViewControllerNavigationDirection)direction {
    
}

-(void)pagerSelectedWithUrl:(NSString *)url {
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Settings" bundle:nil];
    UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"MediaViewerViewController"];
    viewController.data = [[NSMutableDictionary alloc] initWithDictionary:@{@"viewController":self, @"urls":mediaUrlsArray,@"SelectedMediaUrl":url,@"view_check":@(YES)}];
    [self presentViewController:viewController animated:YES completion:nil];
    
//    if(![url isNull]){
//        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//        PhotoViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"PhotoViewController"];
//
//        NSData * imageData = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:url]];
//        UIImage *image = [UIImage imageWithData: imageData];
//        viewController.data = [[NSMutableDictionary alloc] initWithDictionary:@{@"viewController":self, @"image":image, @"view_check":@(YES)}];
//        viewController.originalImgArray = ogImgArray;
//        viewController.index = [ogImgArray indexOfObject:url];
//        viewController.hidesBottomBarWhenPushed = YES;
//        viewController.isMultiple = YES;
//        [self presentViewController:viewController animated:YES completion:nil];
//    }
    
}

//- (IBAction)healthClinicFiltersAction:(id)sender
//{
//    [UIView animateWithDuration:0.1f delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
//        self.filterView.frame = CGRectMake(0, -self.filterView.frame.size.height, self.filterView.frame.size.width, self.filterView.frame.size.height);
//    } completion:^(BOOL finished) {
//        //[btnFilter setImage:[UIImage imageNamed:@"icon_health"] forState:UIControlStateNormal];
//        selectedSubFilter = 25;
//        [self refreshAction];
//    }];
//
//}

#pragma mark - ViewPort Implementation
//-(void)createScrollPages
//{
//    [viewPortScroll removeFromSuperview];
//    viewPortScroll = nil;
//    viewPortScroll = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, headerView.frame.size.width, headerView.frame.size.height)];
//    viewPortScroll.pagingEnabled = YES;
//    viewPortScroll.showsHorizontalScrollIndicator = NO;
//    viewPortScroll.bounces = NO;
//    viewPortScroll.delegate = self;
//    viewPortScroll.backgroundColor = [UIColor darkTextColor];
//    prevPageIndex = 0;
//    CGRect frame = CGRectMake(0, 0, 0, 0);
//
//    for (int i =0; i<mediaUrlsArray.count; i++) {
//
//        NSArray *imgExt = [NSArray arrayWithObjects:@"mp4",@"mov",@"m3u8", nil];
//        NSString *urlStr = [mediaUrlsArray objectAtIndex:i];
//        NSURL *url = [NSURL URLWithString:urlStr];
//        NSString *pathExt = [url pathExtension];
//
//        if ([imgExt containsObject:pathExt]) {
//            //video
//            frame.origin.x = headerView.frame.size.width*i;
//            frame.size = headerView.frame.size;
//            UIView *subview = [[UIView alloc]initWithFrame:frame];
//            subview.backgroundColor = [UIColor clearColor];
//
//            [vManager removePlayerInstance];
//            vManager = [VideoPlayerManager new];
//            vManager.videoUrl = urlStr;
//            [vManager initilizePlayerwithView:subview andController:self];
//            [vManager.player pause];
//        }else {
//            //image
//            UIView *subview = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, self.tableView.frame.size.height)];
//            subview.backgroundColor = [UIColor clearColor];
//
//            AsyncImageView *imgView = [[AsyncImageView alloc] initWithFrame:CGRectMake(0, 0, subview.frame.size.width, subview.frame.size.height/2)];
//            imgView.contentMode = UIViewContentModeScaleAspectFit;
//            imgPhoto.clipsToBounds = YES;
//            imgView.imageURL = [NSURL URLWithString:urlStr];
//            [subview addSubview:imgView];
//            [viewPortScroll addSubview:subview];
//        }
//
//
//    }
//
//    viewPortScroll.contentSize = CGSizeMake(viewPortScroll.frame.size.width * mediaUrlsArray.count, viewPortScroll.frame.size.height);
//
//    [viewPortBtn removeFromSuperview];
//    viewPortBtn = nil;
//    viewPortBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, headerView.frame.size.width, headerView.frame.size.height)];
//    [viewPortBtn addTarget:self action:@selector(showPhotoAction:) forControlEvents:UIControlEventTouchUpInside];
//
//    [viewPortScroll addSubview:viewPortBtn];
//    [headerView addSubview:viewPortScroll];
//
//    [self configurePageControl];
//}

//
//-(void) configurePageControl
//{
//    [pgControl removeFromSuperview];
//    pgControl = nil;
//    pgControl = [[UIPageControl alloc] initWithFrame:CGRectMake(headerView.frame.size.width/2 - 50, headerView.frame.size.height-20, 100, 20)];
//    pgControl.numberOfPages = mediaUrlsArray.count;
//    pgControl.currentPage = 0;
//    pgControl.hidesForSinglePage = YES;
//    pgControl.pageIndicatorTintColor = [UIColor groupTableViewBackgroundColor];
//    pgControl.currentPageIndicatorTintColor = UIColorFromRGB(0x007AFF);
//    [headerView addSubview:pgControl];
//}
//
//#pragma mark - VideoManager Observer Handler
//-(void)itemDidFinishPlaying:(NSNotification *) notification {
//    NSLog(@"itemDidFinishPlaying");
//    [vManager.player pause];
//    [vManager.player seekToTime:kCMTimeZero];
//}
//
//-(void) applicationWillResignActive:(NSNotification *)notification {
//    [vManager.player pause];
//    if (vManager.isStatusObserverSet) {
//        [vManager.player.currentItem removeObserver:self forKeyPath:@"status" ];
//        vManager.isStatusObserverSet = NO;
//    }
//}

@end
