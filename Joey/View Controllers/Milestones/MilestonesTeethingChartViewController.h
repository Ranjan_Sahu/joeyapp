//
//  MilestonesTeethingChartViewController.h
//  Joey
//
//  Created by csl on 8/6/2017.
//  Copyright © 2017 Auxilia. All rights reserved.
//

#import "BaseViewController.h"

@interface MilestonesTeethingChartViewController : BaseViewController

@property (nonatomic, weak) IBOutlet UILabel *txtChartName;

//Baby
@property (nonatomic, weak) IBOutlet UIView *viewTeethBaby;
@property (nonatomic, weak) IBOutlet UIView *viewTeethGuideBaby;
@property (nonatomic, weak) IBOutlet UIView *viewTeethingBaby;
@property (nonatomic, weak) IBOutlet UILabel *lblTeethingBabyUpper;
@property (nonatomic, weak) IBOutlet UILabel *lblTeethingBabyLower;

// Baby Guide color views
@property (nonatomic, weak) IBOutlet UIView *viewTeethingBaby1;
@property (nonatomic, weak) IBOutlet UIView *viewTeethingBaby2;
@property (nonatomic, weak) IBOutlet UIView *viewTeethingBaby3;
@property (nonatomic, weak) IBOutlet UIView *viewTeethingBaby4;
@property (nonatomic, weak) IBOutlet UIView *viewTeethingBaby5;
@property (nonatomic, weak) IBOutlet UIView *viewTeethingBaby6;

// Baby Guide month labels
@property (nonatomic, weak) IBOutlet UILabel *lblTeethingBaby1;
@property (nonatomic, weak) IBOutlet UILabel *lblTeethingBaby2;
@property (nonatomic, weak) IBOutlet UILabel *lblTeethingBaby3;
@property (nonatomic, weak) IBOutlet UILabel *lblTeethingBaby4;
@property (nonatomic, weak) IBOutlet UILabel *lblTeethingBaby5;
@property (nonatomic, weak) IBOutlet UILabel *lblTeethingBaby6;

// Baby Button Teeths
@property (nonatomic, weak) IBOutlet UIButton *btnTeethingBaby1;
@property (nonatomic, weak) IBOutlet UIButton *btnTeethingBaby2;
@property (nonatomic, weak) IBOutlet UIButton *btnTeethingBaby3;
@property (nonatomic, weak) IBOutlet UIButton *btnTeethingBaby4;
@property (nonatomic, weak) IBOutlet UIButton *btnTeethingBaby5;
@property (nonatomic, weak) IBOutlet UIButton *btnTeethingBaby6;
@property (nonatomic, weak) IBOutlet UIButton *btnTeethingBaby7;
@property (nonatomic, weak) IBOutlet UIButton *btnTeethingBaby8;
@property (nonatomic, weak) IBOutlet UIButton *btnTeethingBaby9;
@property (nonatomic, weak) IBOutlet UIButton *btnTeethingBaby10;
@property (nonatomic, weak) IBOutlet UIButton *btnTeethingBaby11;
@property (nonatomic, weak) IBOutlet UIButton *btnTeethingBaby12;
@property (nonatomic, weak) IBOutlet UIButton *btnTeethingBaby13;
@property (nonatomic, weak) IBOutlet UIButton *btnTeethingBaby14;
@property (nonatomic, weak) IBOutlet UIButton *btnTeethingBaby15;
@property (nonatomic, weak) IBOutlet UIButton *btnTeethingBaby16;
@property (nonatomic, weak) IBOutlet UIButton *btnTeethingBaby17;
@property (nonatomic, weak) IBOutlet UIButton *btnTeethingBaby18;
@property (nonatomic, weak) IBOutlet UIButton *btnTeethingBaby19;
@property (nonatomic, weak) IBOutlet UIButton *btnTeethingBaby20;

//Adult
@property (nonatomic, weak) IBOutlet UIView *viewTeethAdult;
@property (nonatomic, weak) IBOutlet UIView *viewTeethGuideAdult;
@property (nonatomic, weak) IBOutlet UIView *viewTeethingAdult;
@property (nonatomic, weak) IBOutlet UILabel *lblTeethingAdultUpper;
@property (nonatomic, weak) IBOutlet UILabel *lblTeethingAdultLower;

// Adult Guide color views
@property (nonatomic, weak) IBOutlet UIView *viewTeethingAdult1;
@property (nonatomic, weak) IBOutlet UIView *viewTeethingAdult2;
@property (nonatomic, weak) IBOutlet UIView *viewTeethingAdult3;
@property (nonatomic, weak) IBOutlet UIView *viewTeethingAdult4;
@property (nonatomic, weak) IBOutlet UIView *viewTeethingAdult5;

// Adult Guide year labels
@property (nonatomic, weak) IBOutlet UILabel *lblTeethingAdult1;
@property (nonatomic, weak) IBOutlet UILabel *lblTeethingAdult2;
@property (nonatomic, weak) IBOutlet UILabel *lblTeethingAdult3;
@property (nonatomic, weak) IBOutlet UILabel *lblTeethingAdult4;
@property (nonatomic, weak) IBOutlet UILabel *lblTeethingAdult5;

// Adult Button Teeths
@property (nonatomic, weak) IBOutlet UIButton *btnTeethingAdult1;
@property (nonatomic, weak) IBOutlet UIButton *btnTeethingAdult2;
@property (nonatomic, weak) IBOutlet UIButton *btnTeethingAdult3;
@property (nonatomic, weak) IBOutlet UIButton *btnTeethingAdult4;
@property (nonatomic, weak) IBOutlet UIButton *btnTeethingAdult5;
@property (nonatomic, weak) IBOutlet UIButton *btnTeethingAdult6;
@property (nonatomic, weak) IBOutlet UIButton *btnTeethingAdult7;
@property (nonatomic, weak) IBOutlet UIButton *btnTeethingAdult8;
@property (nonatomic, weak) IBOutlet UIButton *btnTeethingAdult9;
@property (nonatomic, weak) IBOutlet UIButton *btnTeethingAdult10;
@property (nonatomic, weak) IBOutlet UIButton *btnTeethingAdult11;
@property (nonatomic, weak) IBOutlet UIButton *btnTeethingAdult12;
@property (nonatomic, weak) IBOutlet UIButton *btnTeethingAdult13;
@property (nonatomic, weak) IBOutlet UIButton *btnTeethingAdult14;
@property (nonatomic, weak) IBOutlet UIButton *btnTeethingAdult15;
@property (nonatomic, weak) IBOutlet UIButton *btnTeethingAdult16;
@property (nonatomic, weak) IBOutlet UIButton *btnTeethingAdult17;
@property (nonatomic, weak) IBOutlet UIButton *btnTeethingAdult18;
@property (nonatomic, weak) IBOutlet UIButton *btnTeethingAdult19;
@property (nonatomic, weak) IBOutlet UIButton *btnTeethingAdult20;
@property (nonatomic, weak) IBOutlet UIButton *btnTeethingAdult21;
@property (nonatomic, weak) IBOutlet UIButton *btnTeethingAdult22;
@property (nonatomic, weak) IBOutlet UIButton *btnTeethingAdult23;
@property (nonatomic, weak) IBOutlet UIButton *btnTeethingAdult24;
@property (nonatomic, weak) IBOutlet UIButton *btnTeethingAdult25;
@property (nonatomic, weak) IBOutlet UIButton *btnTeethingAdult26;
@property (nonatomic, weak) IBOutlet UIButton *btnTeethingAdult27;
@property (nonatomic, weak) IBOutlet UIButton *btnTeethingAdult28;
@property (nonatomic, weak) IBOutlet UIButton *btnTeethingAdult29;
@property (nonatomic, weak) IBOutlet UIButton *btnTeethingAdult30;
@property (nonatomic, weak) IBOutlet UIButton *btnTeethingAdult31;
@property (nonatomic, weak) IBOutlet UIButton *btnTeethingAdult32;

// Labels for Teeth Info
@property (weak, nonatomic) IBOutlet UILabel *lblLostTooth;
@property (weak, nonatomic) IBOutlet UILabel *lblNewTooth;

// Buttons for switching(Baby/Adult)
@property (nonatomic, weak) IBOutlet UIButton *btnBaby;
@property (nonatomic, weak) IBOutlet UIButton *btnAdult;

@end
