//
//  MilestoneAddViewController.m
//  Joey
//
//  Created by csl on 31/3/2017.
//  Copyright © 2017 Auxilia. All rights reserved.
//

#import "AppDelegate.h"
#import "MilestoneAddViewController.h"
#import "TeethButton.h"

@interface MilestoneAddViewController ()<PECropViewControllerDelegate>
{
    NSUserDefaults *shareObject;
    AppDelegate *appDelegate;
    NSMutableDictionary *unitInfo;
    NSDateFormatter *formatter;
    NSMutableArray *no100List;
    NSMutableArray *no200List;
    NSMutableArray *no300List;
    NSMutableArray *no24List;
    NSMutableArray *no60List;
    NSMutableArray *digitList;
    NSMutableArray *digit2List;
    NSMutableArray *babyTeethList;
    NSMutableArray *babyTeethBackupList;
    NSMutableArray *babyTeethColorList;
    NSMutableArray *adultTeethList;
    NSMutableArray *adultTeethBackupList;
    NSMutableArray *adultTeethColorList;

    NSString *selectedName;
    NSArray *selectedWeightIndexes;
    NSArray *selectedHeightIndexes;
    NSArray *selectedHeadSizeIndexes;
    NSDate *selectedMilestoneDate;
    NSDate *selectedMilestoneTime;
    NSDate *selectedMilestoneDateTime;
    NSDate *selectedReminderDate;
    NSDate *selectedReminderTime;
    NSDate *selectedReminderDateTime;
    
    BOOL saveCheck;
    BOOL babyTeeth1Check;
    BOOL babyTeeth2Check;
    BOOL babyTeeth3Check;
    BOOL babyTeeth4Check;
    BOOL babyTeeth5Check;
    BOOL babyTeeth6Check;
    BOOL babyTeeth7Check;
    BOOL babyTeeth8Check;
    BOOL babyTeeth9Check;
    BOOL babyTeeth10Check;
    BOOL babyTeeth11Check;
    BOOL babyTeeth12Check;
    BOOL babyTeeth13Check;
    BOOL babyTeeth14Check;
    BOOL babyTeeth15Check;
    BOOL babyTeeth16Check;
    BOOL babyTeeth17Check;
    BOOL babyTeeth18Check;
    BOOL babyTeeth19Check;
    BOOL babyTeeth20Check;
    int selectedIndex;
    float selectedWeight;
    float selectedHeight;
    float selectedHeadSize;
    
    UIImageView *largeImageView;
    UIImage *imgUploadCropped;
    UIImage *imgUploadOriginal;
    UIImage *imgUploadCropped2;
    UIImage *imgUploadOriginal2;
    UIImage *imgUploadCropped3;
    UIImage *imgUploadOriginal3;
    NSMutableArray *keywordsArray;
    NSDictionary *keywordslistDict;
    
    NSString *videoUrl;
    NSString *videoUrl2;
    NSString *videoUrl3;
}

@property (nonatomic) UIPopoverController *popover;

@end

@implementation MilestoneAddViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    shareObject = [NSUserDefaults standardUserDefaults];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    formatter = [[NSDateFormatter alloc] init];
    [formatter setTimeZone:[NSTimeZone localTimeZone]];
    [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
    
    unitInfo = (NSMutableDictionary *)[NSKeyedUnarchiver unarchiveObjectWithData:[shareObject objectForKey:@"unit_info"]];
    
    no100List = [[NSMutableArray alloc] init];
    for(int i=0; i<=100; i++) [no100List addObject:[NSString stringWithFormat:@"%d", i]];
    
    no200List = [[NSMutableArray alloc] init];
    for(int i=0; i<=200; i++) [no200List addObject:[NSString stringWithFormat:@"%d", i]];
    
    no300List = [[NSMutableArray alloc] init];
    for(int i=0; i<=300; i++) [no300List addObject:[NSString stringWithFormat:@"%d", i]];
        
    no24List = [[NSMutableArray alloc] init];
    for(int i=0; i<24; i++) [no24List addObject:[NSString stringWithFormat:@"%d", i]];
    
    no60List = [[NSMutableArray alloc] init];
    for(int i=0; i<60; i++) [no60List addObject:[NSString stringWithFormat:@"%d", i]];
    
    digitList = [[NSMutableArray alloc] init];
    for(int i=0; i<10; i++) [digitList addObject:[NSString stringWithFormat:@"%d", i]];
    
    digit2List = [[NSMutableArray alloc] init];
    for(int i=0; i<100; i++) [digit2List addObject:[NSString stringWithFormat:@"%02d", i]];
    
    babyTeethList = [[NSMutableArray alloc] init];
    babyTeethBackupList = [[NSMutableArray alloc] init];
    babyTeethColorList = [CommonHelper getChildTeethColors];
    
    adultTeethList = [[NSMutableArray alloc] init];
    adultTeethBackupList = [[NSMutableArray alloc] init];
    adultTeethColorList = [CommonHelper getAdultTeethColors];
    
    for(int i=1; i<=20; i++)
    {
        [babyTeethList addObject:@(0)];
        [babyTeethBackupList addObject:@(0)];
    }
    
    for(int i=1; i<=32; i++)
    {
        [adultTeethList addObject:@(0)];
        [adultTeethBackupList addObject:@(0)];
    }
    
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:UIColorFromRGB(0x8D8082), NSForegroundColorAttributeName, [UIFont fontWithName:@"SFUIDisplay-Medium" size:18.0f], NSFontAttributeName, nil]];
    [self.navigationController.navigationBar setBarTintColor:UIColorFromRGB(0xEEE8E4)];
    [self.navigationController.navigationBar setTranslucent:NO];
    
    [self.navigationItem setTitle:LocalizedString(@"title_new_milestone", nil)];
    [self customBackButton];
    
    self.btnSave.title = LocalizedString(@"btn_save", nil);
    
    self.btnPhoto1.imageView.contentMode = UIViewContentModeScaleAspectFill;
    
    self.lblDate.text = LocalizedString(@"txt_datetime", nil);
    self.lblHeight.text = LocalizedString(@"txt_height", nil);
    self.lblWeight.text = LocalizedString(@"txt_weight", nil);
    self.lblHeadSize.text = LocalizedString(@"txt_circumference", nil);
    self.lblTeething.text = LocalizedString(@"txt_teething", nil);
    self.lblTeethingBabyUpper.text = LocalizedString(@"txt_teething_upper", nil);
    self.lblTeethingBabyLower.text = LocalizedString(@"txt_teething_lower", nil);
    
    self.lblTeethingBaby1.text = LocalizedString(@"txt_teething_6_10_months", nil);
    self.lblTeethingBaby2.text = LocalizedString(@"txt_teething_8_13_months", nil);
    self.lblTeethingBaby3.text = LocalizedString(@"txt_teething_10_16_months", nil);
    self.lblTeethingBaby4.text = LocalizedString(@"txt_teething_13_19_months", nil);
    self.lblTeethingBaby5.text = LocalizedString(@"txt_teething_16_23_months", nil);
    self.lblTeethingBaby6.text = LocalizedString(@"txt_teething_23_33_months", nil);
    
    self.lblTeethingAdult1.text = LocalizedString(@"txt_teething_6_8_years", nil);
    self.lblTeethingAdult2.text = LocalizedString(@"txt_teething_7_9_years", nil);
    self.lblTeethingAdult3.text = LocalizedString(@"txt_teething_9_12_years", nil);
    self.lblTeethingAdult4.text = LocalizedString(@"txt_teething_11_13_years", nil);
    self.lblTeethingAdult5.text = LocalizedString(@"txt_teething_17_21_years", nil);
    
    self.lblMovement.text = LocalizedString(@"txt_movement", nil);
    self.lblVerbal.text = LocalizedString(@"txt_verbal", nil);
    self.lblLearning.text = LocalizedString(@"txt_learning", nil);
    self.lblMoods.text = LocalizedString(@"txt_moods", nil);
    self.lblVaccine.text = LocalizedString(@"txt_vaccine", nil);
    self.lblAllergies.text = LocalizedString(@"txt_allergies", nil);
    self.lblSymptom.text = LocalizedString(@"txt_symptom", nil);
    self.lblDiagnosis.text = LocalizedString(@"txt_diagnosis", nil);
    self.lblMedication.text = LocalizedString(@"txt_prescription", nil);
    self.lblClinic.text = LocalizedString(@"txt_clinic", nil);
    self.lblReminderTitle.text = LocalizedString(@"txt_reminder_title", nil);
    self.lblReminder.text = LocalizedString(@"txt_reminder", nil);
    self.lblOthers.text = LocalizedString(@"txt_others", nil);
    self.lblChecklist.text = LocalizedString(@"txt_checklist", nil);
    self.lblPhoto.text = LocalizedString(@"txt_photo", nil);
    self.lblNotes.text = LocalizedString(@"txt_notes", nil);
    
    self.viewTeethingBaby1.layer.cornerRadius = self.viewTeethingBaby1.frame.size.width/2;
    self.viewTeethingBaby2.layer.cornerRadius = self.viewTeethingBaby2.frame.size.width/2;
    self.viewTeethingBaby3.layer.cornerRadius = self.viewTeethingBaby3.frame.size.width/2;
    self.viewTeethingBaby4.layer.cornerRadius = self.viewTeethingBaby4.frame.size.width/2;
    self.viewTeethingBaby5.layer.cornerRadius = self.viewTeethingBaby5.frame.size.width/2;
    self.viewTeethingBaby6.layer.cornerRadius = self.viewTeethingBaby6.frame.size.width/2;
    
    self.viewTeethingAdult1.layer.cornerRadius = self.viewTeethingAdult1.frame.size.width/2;
    self.viewTeethingAdult2.layer.cornerRadius = self.viewTeethingAdult2.frame.size.width/2;
    self.viewTeethingAdult3.layer.cornerRadius = self.viewTeethingAdult3.frame.size.width/2;
    self.viewTeethingAdult4.layer.cornerRadius = self.viewTeethingAdult4.frame.size.width/2;
    self.viewTeethingAdult5.layer.cornerRadius = self.viewTeethingAdult5.frame.size.width/2;

    self.btnTeethingBaby1.tag = 1;
    self.btnTeethingBaby2.tag = 2;
    self.btnTeethingBaby3.tag = 3;
    self.btnTeethingBaby4.tag = 4;
    self.btnTeethingBaby5.tag = 5;
    self.btnTeethingBaby6.tag = 6;
    self.btnTeethingBaby7.tag = 7;
    self.btnTeethingBaby8.tag = 8;
    self.btnTeethingBaby9.tag = 9;
    self.btnTeethingBaby10.tag = 10;
    self.btnTeethingBaby11.tag = 11;
    self.btnTeethingBaby12.tag = 12;
    self.btnTeethingBaby13.tag = 13;
    self.btnTeethingBaby14.tag = 14;
    self.btnTeethingBaby15.tag = 15;
    self.btnTeethingBaby16.tag = 16;
    self.btnTeethingBaby17.tag = 17;
    self.btnTeethingBaby18.tag = 18;
    self.btnTeethingBaby19.tag = 19;
    self.btnTeethingBaby20.tag = 20;
    
    self.btnTeethingAdult1.tag = 101;
    self.btnTeethingAdult2.tag = 102;
    self.btnTeethingAdult3.tag = 103;
    self.btnTeethingAdult4.tag = 104;
    self.btnTeethingAdult5.tag = 105;
    self.btnTeethingAdult6.tag = 106;
    self.btnTeethingAdult7.tag = 107;
    self.btnTeethingAdult8.tag = 108;
    self.btnTeethingAdult9.tag = 109;
    self.btnTeethingAdult10.tag = 110;
    self.btnTeethingAdult11.tag = 111;
    self.btnTeethingAdult12.tag = 112;
    self.btnTeethingAdult13.tag = 113;
    self.btnTeethingAdult14.tag = 114;
    self.btnTeethingAdult15.tag = 115;
    self.btnTeethingAdult16.tag = 116;
    self.btnTeethingAdult17.tag = 117;
    self.btnTeethingAdult18.tag = 118;
    self.btnTeethingAdult19.tag = 119;
    self.btnTeethingAdult20.tag = 120;
    self.btnTeethingAdult11.tag = 111;
    self.btnTeethingAdult12.tag = 112;
    self.btnTeethingAdult13.tag = 113;
    self.btnTeethingAdult14.tag = 114;
    self.btnTeethingAdult15.tag = 115;
    self.btnTeethingAdult16.tag = 116;
    self.btnTeethingAdult17.tag = 117;
    self.btnTeethingAdult18.tag = 118;
    self.btnTeethingAdult19.tag = 119;
    self.btnTeethingAdult20.tag = 120;
    self.btnTeethingAdult21.tag = 121;
    self.btnTeethingAdult22.tag = 122;
    self.btnTeethingAdult23.tag = 123;
    self.btnTeethingAdult24.tag = 124;
    self.btnTeethingAdult25.tag = 125;
    self.btnTeethingAdult26.tag = 126;
    self.btnTeethingAdult27.tag = 127;
    self.btnTeethingAdult28.tag = 128;
    self.btnTeethingAdult29.tag = 129;
    self.btnTeethingAdult30.tag = 130;
    self.btnTeethingAdult31.tag = 131;
    self.btnTeethingAdult32.tag = 132;
    
    selectedMilestoneDateTime = [NSDate date];
    if([[shareObject objectForKey:@"language"] hasPrefix:@"zh"])
    {
        [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"zh_HK"]];
        [formatter setDateStyle:NSDateFormatterMediumStyle];
        [formatter setTimeStyle:NSDateFormatterShortStyle];
    }
    else
    {
        [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
        formatter.dateFormat = @"EE d MMM, yyyy hh:mm a";
    }
    self.txtDate.text = [NSString stringWithFormat:@"%@", [formatter stringFromDate:selectedMilestoneDateTime]];
    
    [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
    formatter.dateFormat = @"yyyy-MM-dd";
    selectedMilestoneDate = [NSDate date];
    
    formatter.dateFormat = @"hh:mm a";
    selectedMilestoneTime = [NSDate date];
    
    formatter.dateFormat = @"yyyy-MM-dd";
    selectedReminderDate = [NSDate date];
    
    formatter.dateFormat = @"hh:mm a";
    selectedReminderTime = [NSDate date];
    
    // Photo Picking & Display
    self.btnPhoto1.hidden = YES;
    self.btnPhoto2.hidden = YES;
    self.btnPhoto3.hidden = YES;
    self.btnPhoto1.tag = 1;
    self.btnPhoto2.tag = 2;
    self.btnPhoto3.tag = 3;
    [self.btnPhoto1 addTarget:self action:@selector(showPhotoAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.btnPhoto2 addTarget:self action:@selector(showPhotoAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.btnPhoto3 addTarget:self action:@selector(showPhotoAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.btnGrow sendActionsForControlEvents:UIControlEventTouchUpInside];
    [self doSetupForKeywordView];
    [self doSetupForTeethingView];
    [self customizeFont];
    [self getTeethingInfo];
    [self getKeywordsList];
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:(BOOL)animated];
    
    NSLog(@"self.data=%@", self.data);
    if([[self.data objectForKey:@"deleteCheck"] boolValue])
    {
        if ([[self.data objectForKey:@"imageTag"] integerValue] == 1) {
            self.btnPhoto1.hidden = YES;
            [self.data removeObjectForKey:@"imageTag"];
            videoUrl = nil;
        }else if ([[self.data objectForKey:@"imageTag"] integerValue] == 2) {
            self.btnPhoto2.hidden = YES;
            [self.data removeObjectForKey:@"imageTag"];
            videoUrl2 = nil;
        }else if ([[self.data objectForKey:@"imageTag"] integerValue] == 3) {
            self.btnPhoto3.hidden = YES;
            [self.data removeObjectForKey:@"imageTag"];
            videoUrl3 = nil;
        }
        
        [self.data removeObjectForKey:@"deleteCheck"];
        
    }
    
}

- (void)customBackButton
{
    if([self.navigationController.viewControllers count] > 1)
    {
        UIImage *imgBack = [UIImage imageNamed:@"nav_btn_back"];
        UIButton *btnBack = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnBack setImage:imgBack forState:UIControlStateNormal];
        btnBack.frame = CGRectMake(0, 0, imgBack.size.width, imgBack.size.height);
        [btnBack addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
        [btnBack setImageEdgeInsets:UIEdgeInsetsMake(0, -16, 0, 16)];
        UIBarButtonItem *backBarItem = [[UIBarButtonItem alloc] initWithCustomView:btnBack];
        self.navigationItem.leftBarButtonItem = backBarItem;
    }
}

- (void)backAction
{
    [COMMON_HELPER clearTmpDirectory];
    UIViewController *viewController = [self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count-2];
    viewController.data = [[NSMutableDictionary alloc] initWithDictionary:@{@"save_check":@(saveCheck)}];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)getTeethingInfo
{
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setValue:@([shareObject integerForKey:@"user_id"]) forKey:@"user_id"];
    [parameters setValue:@([shareObject integerForKey:@"baby_id"]) forKey:@"baby_id"];
    [parameters setValue:@"teething" forKey:@"type"];
    [[WebServiceManager sharedManager] initWithBaseURL:@"api/getGrowthChartList" parameters:parameters success:^(id result) {
        if ([[result objectForKey:@"baby_chart_list"] count]>0){
            [self setBabyTeeth:[result objectForKey:@"baby_chart_list"]];
        }        
        if ([[result objectForKey:@"adult_chart_list"] count]>0) {
            [self setAdultTeeth:[result objectForKey:@"adult_chart_list"]];
        }
    } failure:^(NSError *error) {
    }];
}

- (void)selectMilestoneTime:(NSString *)milestoneDate
{
    [ActionSheetDatePicker showPickerWithTitle:@"" datePickerMode:UIDatePickerModeTime selectedDate:selectedMilestoneTime minimumDate:nil maximumDate:[NSDate date] doneBlock:^(ActionSheetDatePicker *picker, id selectedDate, id origin) {
        
        selectedMilestoneTime = selectedDate;
        NSLog(@"selectedMilestoneTime=%@", selectedMilestoneTime);
        
        formatter.dateFormat = @"hh:mm a";
        NSString *milestoneTime = [formatter stringFromDate:selectedDate];
        
        formatter.dateFormat = @"d MMM yyyy hh:mm a";
        selectedMilestoneDateTime = [formatter dateFromString:[NSString stringWithFormat:@"%@ %@", milestoneDate, milestoneTime]];
        NSLog(@"selectedMilestoneDateTime=%@", selectedMilestoneDateTime);
        
        if([[shareObject objectForKey:@"language"] hasPrefix:@"zh"])
        {
            [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"zh_HK"]];
            [formatter setDateStyle:NSDateFormatterMediumStyle];
            [formatter setTimeStyle:NSDateFormatterShortStyle];
        }
        else
        {
            [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
            formatter.dateFormat = @"EE d MMM, yyyy hh:mm a";
        }
        self.txtDate.text = [NSString stringWithFormat:@"%@", [formatter stringFromDate:selectedMilestoneDateTime]];
        //[NSString stringWithFormat:@"%@ %@", milestoneDate, milestoneTime];
        
        [self.tableView reloadData];
        
    } cancelBlock:^(ActionSheetDatePicker *picker) {
        
    } origin:self.view];
}

- (void)selectReminderTime:(NSString *)reminderDate
{
    [ActionSheetDatePicker showPickerWithTitle:@"" datePickerMode:UIDatePickerModeTime selectedDate:selectedReminderTime minimumDate:nil maximumDate:nil doneBlock:^(ActionSheetDatePicker *picker, id selectedDate, id origin) {
        
        selectedReminderTime = selectedDate;
        NSLog(@"selectedReminderTime=%@", selectedReminderTime);
        
        formatter.dateFormat = @"hh:mm a";
        NSString *reminderTime = [formatter stringFromDate:selectedDate];
        formatter.dateFormat = @"d MMM yyyy hh:mm a";
        selectedReminderDateTime = [formatter dateFromString:[NSString stringWithFormat:@"%@ %@", reminderDate, reminderTime]];
        NSLog(@"selectedReminderDateTime=%@", selectedReminderDateTime);
        
        if([[shareObject objectForKey:@"language"] hasPrefix:@"zh"])
        {
            [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"zh_HK"]];
            [formatter setDateStyle:NSDateFormatterMediumStyle];
            [formatter setTimeStyle:NSDateFormatterShortStyle];
        }
        else
        {
            [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
            formatter.dateFormat = @"d MMM, yyyy hh:mm a";
        }
        self.txtReminder.text = [NSString stringWithFormat:@"%@", [formatter stringFromDate:selectedReminderDateTime]];
        //[NSString stringWithFormat:@"%@ %@", reminderDate, reminderTime];
        
        [self.tableView reloadData];
        
    } cancelBlock:^(ActionSheetDatePicker *picker) {
        
    } origin:self.view];
}

- (void)showCameraAction
{
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    if ([[NSUserDefaults standardUserDefaults]boolForKey:@"is_Subscription_Enabled"]) {
        imagePicker.mediaTypes = [[NSArray alloc] initWithObjects: (NSString *) kUTTypeMovie, (NSString *) kUTTypeVideo, (NSString *) kUTTypeImage, nil];
        imagePicker.videoQuality = UIImagePickerControllerQualityTypeLow;
        imagePicker.videoMaximumDuration = 120;
    }else {
        imagePicker.mediaTypes = [[NSArray alloc] initWithObjects: (NSString *) kUTTypeImage, nil];
    }

    [self presentViewController:imagePicker animated:NO completion:nil];
}

- (void)showPhotoAlbumAction
{
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    if ([[NSUserDefaults standardUserDefaults]boolForKey:@"is_Subscription_Enabled"]) {
        imagePicker.mediaTypes = [[NSArray alloc] initWithObjects: (NSString *) kUTTypeMovie, (NSString *) kUTTypeVideo, (NSString *) kUTTypeImage, nil];
    }else {
        imagePicker.mediaTypes = [[NSArray alloc] initWithObjects: (NSString *) kUTTypeImage, nil];
    }

    [self presentViewController:imagePicker animated:NO completion:nil];
}

- (void)showPhotoAction:(UIButton *)sender
{
    if (sender.hidden)
        return;
    
    UIImage *img;
    NSString *vStr;
    if (sender.tag == 1) {
        if (videoUrl && videoUrl.length>0) {
            vStr = videoUrl;
        }else {
            img = self.btnPhoto1.imageView.image;
        }
    }else if (sender.tag == 2) {
        if (videoUrl2 && videoUrl2.length>0) {
            vStr = videoUrl2;
        }else {
            img = self.btnPhoto2.imageView.image;
        }
    }else if (sender.tag == 3) {
        if (videoUrl3 && videoUrl3.length>0) {
            vStr = videoUrl3;
        }else {
            img = self.btnPhoto3.imageView.image;
        }
    }
    
    if (vStr && vStr.length>0) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"VideoViewController"];
        viewController.data = [[NSMutableDictionary alloc] initWithDictionary:@{@"viewController":self, @"video":vStr,@"imageTag":@(sender.tag)}];
        [self presentViewController:viewController animated:YES completion:nil];
    }
    else
    {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"PhotoViewController"];
        viewController.data = [[NSMutableDictionary alloc] initWithDictionary:@{@"viewController":self, @"image":img,@"imageTag":@(sender.tag)}];
        [self presentViewController:viewController animated:YES completion:nil];
    }
    
//    UIImage *img;
//    if (sender.tag == 1) {
//        img = self.btnPhoto1.imageView.image;
//    }else if (sender.tag == 2) {
//        img = self.btnPhoto2.imageView.image;
//    }else if (sender.tag == 3) {
//        img = self.btnPhoto3.imageView.image;
//    }
//
//    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"PhotoViewController"];
//    viewController.data = [[NSMutableDictionary alloc] initWithDictionary:@{@"viewController":self, @"image":img,@"imageTag":@(sender.tag)}];
//    [self presentViewController:viewController animated:YES completion:nil];
}

- (void)takePhotoAction
{
    AVAuthorizationStatus status = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    if(status == AVAuthorizationStatusDenied || status == AVAuthorizationStatusRestricted)
    {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:LocalizedString(@"error_camera", nil) message:@"" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *btnOk = [UIAlertAction actionWithTitle:LocalizedString(@"btn_ok", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [alert dismissViewControllerAnimated:YES completion:nil];
        }];
        
        [alert addAction:btnOk];
        [self presentViewController:alert animated:YES completion:nil];
        
    }
    else
    {
        [self showCameraAction];
    }
}

- (void)getPhotoAlbumAction
{
    PHAuthorizationStatus status = [PHPhotoLibrary authorizationStatus];
    if(status == PHAuthorizationStatusDenied || status == PHAuthorizationStatusRestricted)
    {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:LocalizedString(@"error_photo", nil) message:@"" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *btnOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            
            [alert dismissViewControllerAnimated:YES completion:nil];
        }];
        
        [alert addAction:btnOk];
        
        [self presentViewController:alert animated:YES completion:nil];
        
    }
    else
    {
        [self showPhotoAlbumAction];
    }
}

- (IBAction)getGrowAction:(UIButton *)sender
{
    sender.selected = YES;
    selectedIndex = 0;
    selectedName = @"grow";
    
    self.btnMove.selected = NO;
    self.btnIQ.selected = NO;
    self.btnHealth.selected = NO;
    self.btnOthers.selected = NO;
    
    NSString *str = [keywordslistDict objectForKey:selectedName];
    if (str.length>0) {
        keywordsArray = [[NSMutableArray alloc] initWithArray:[str componentsSeparatedByString:@","]];
    }else {
        keywordsArray = [[NSMutableArray alloc] init];
    }
    [self addKeywordLabel];
    
    [self.tableView reloadData];
}

- (IBAction)getMoveAction:(UIButton *)sender
{
    sender.selected = YES;
    selectedIndex = 1;
    selectedName = @"move";
    
    self.btnGrow.selected = NO;
    self.btnIQ.selected = NO;
    self.btnHealth.selected = NO;
    self.btnOthers.selected = NO;
    
    NSString *str = [keywordslistDict objectForKey:selectedName];
    if (str.length>0) {
        keywordsArray = [[NSMutableArray alloc] initWithArray:[str componentsSeparatedByString:@","]];
    }else {
        keywordsArray = [[NSMutableArray alloc] init];
    }
    [self addKeywordLabel];
    
    [self.tableView reloadData];
}

- (IBAction)getIQAction:(UIButton *)sender
{
    sender.selected = YES;
    selectedIndex = 2;
    selectedName = @"iq";
    
    self.btnGrow.selected = NO;
    self.btnMove.selected = NO;
    self.btnHealth.selected = NO;
    self.btnOthers.selected = NO;
    
    NSString *str = [keywordslistDict objectForKey:selectedName];
    if (str.length>0) {
        keywordsArray = [[NSMutableArray alloc] initWithArray:[str componentsSeparatedByString:@","]];
    }else {
        keywordsArray = [[NSMutableArray alloc] init];
    }
    [self addKeywordLabel];

    [self.tableView reloadData];
}

- (IBAction)getHealthAction:(UIButton *)sender
{
    sender.selected = YES;
    selectedIndex = 3;
    selectedName = @"health";
    
    self.btnGrow.selected = NO;
    self.btnMove.selected = NO;
    self.btnIQ.selected = NO;
    self.btnOthers.selected = NO;
    
    NSString *str = [keywordslistDict objectForKey:selectedName];
    if (str.length>0) {
        keywordsArray = [[NSMutableArray alloc] initWithArray:[str componentsSeparatedByString:@","]];
    }else {
        keywordsArray = [[NSMutableArray alloc] init];
    }
    [self addKeywordLabel];

    [self.tableView reloadData];
}

- (IBAction)getOthersAction:(UIButton *)sender
{
    sender.selected = YES;
    selectedIndex = 4;
    selectedName = @"others";
    
    self.btnGrow.selected = NO;
    self.btnMove.selected = NO;
    self.btnIQ.selected = NO;
    self.btnHealth.selected = NO;
    
    NSString *str = [keywordslistDict objectForKey:selectedName];
    if (str.length>0) {
        keywordsArray = [[NSMutableArray alloc] initWithArray:[str componentsSeparatedByString:@","]];
    }else {
        keywordsArray = [[NSMutableArray alloc] init];
    }
    [self addKeywordLabel];

    [self.tableView reloadData];
}

- (IBAction)selectBabyTeethAction:(UIButton *)btn
{
    UIImage *selectedImage = [UIImage imageNamed:[NSString stringWithFormat:@"btn_teeth%d_selected", (int)btn.tag]];
    selectedImage = [selectedImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    UIImage *unselectedImage = [UIImage imageNamed:[NSString stringWithFormat:@"btn_teeth%d", (int)btn.tag]];
    
    if([[babyTeethBackupList objectAtIndex:btn.tag-1] intValue] == -1) return;
    else if([[babyTeethBackupList objectAtIndex:btn.tag-1] intValue] == 0)
    {
        NSLog(@"new tooth[%d]=%d", (int)btn.tag, [[babyTeethList objectAtIndex:btn.tag-1] intValue]);
        if([[babyTeethList objectAtIndex:btn.tag-1] intValue] == 0)
        {
            [babyTeethList replaceObjectAtIndex:btn.tag-1 withObject:@(1)];
            [btn setImage:selectedImage forState:UIControlStateNormal];
            btn.imageView.tintColor = [babyTeethColorList objectAtIndex:btn.tag-1];
        }
        else
        {
            [babyTeethList replaceObjectAtIndex:btn.tag-1 withObject:@(0)];
            [btn setImage:unselectedImage forState:UIControlStateNormal];
        }
        NSLog(@"new tooth[%d]=%d", (int)btn.tag, [[babyTeethList objectAtIndex:btn.tag-1] intValue]);
    }
    else
    {
        NSLog(@"lost tooth[%d]=%d", (int)btn.tag, [[babyTeethList objectAtIndex:btn.tag-1] intValue]);
        //if (([[babyTeethBackupList objectAtIndex:btn.tag-1] intValue] == 1)&&(([[babyTeethList objectAtIndex:btn.tag-1] intValue] == 0))) {
        //    [babyTeethList replaceObjectAtIndex:btn.tag-1 withObject:@(1)];
        //}
        
        if([[babyTeethList objectAtIndex:btn.tag-1] intValue] == 1)
        {
            [babyTeethList replaceObjectAtIndex:btn.tag-1 withObject:@(-1)];
            [btn setImage:selectedImage forState:UIControlStateNormal];
            btn.imageView.tintColor = UIColorFromRGB(0x8D8082);
        }
        else
        {
            [babyTeethList replaceObjectAtIndex:btn.tag-1 withObject:@(1)];
            [btn setImage:selectedImage forState:UIControlStateNormal];
            btn.imageView.tintColor = [babyTeethColorList objectAtIndex:btn.tag-1];
        }
        NSLog(@"lost tooth[%d]=%d", (int)btn.tag, [[babyTeethList objectAtIndex:btn.tag-1] intValue]);
    }
}

- (IBAction)selectAdultTeethAction:(TeethButton *)btn
{
    int btnId = (int)btn.tag-100;
    
    UIImage *selectedImage = [UIImage imageNamed:[NSString stringWithFormat:@"btn_teeth%d_selected",(btnId+20)]];
    selectedImage = [selectedImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    UIImage *unselectedImage = [UIImage imageNamed:[NSString stringWithFormat:@"btn_teeth%d",(btnId+20)]];
    
    if([[adultTeethBackupList objectAtIndex:btnId-1] intValue] == -1) return;
    else if([[adultTeethBackupList objectAtIndex:btnId-1] intValue] == 0)
    {
        NSLog(@"new tooth[%d]=%d", btnId, [[adultTeethList objectAtIndex:btnId-1] intValue]);
        if([[adultTeethList objectAtIndex:btnId-1] intValue] == 0)
        {
            [adultTeethList replaceObjectAtIndex:btnId-1 withObject:@(1)];
            [btn setImage:selectedImage forState:UIControlStateNormal];
            btn.imageView.tintColor = [adultTeethColorList objectAtIndex:btnId-1];
        }
        else
        {
            [adultTeethList replaceObjectAtIndex:btnId-1 withObject:@(0)];
            [btn setImage:unselectedImage forState:UIControlStateNormal];
        }
        NSLog(@"new tooth[%d]=%d", (int)btnId, [[adultTeethList objectAtIndex:btnId-1] intValue]);
    }
    else
    {
        NSLog(@"lost tooth[%d]=%d", btnId, [[adultTeethList objectAtIndex:btnId-1] intValue]);
        //if (([[adultTeethBackupList objectAtIndex:btnId-1] intValue] == 1)&&(([[adultTeethList objectAtIndex:btnId-1] intValue] == 0))) {
        //    [adultTeethList replaceObjectAtIndex:btnId-1 withObject:@(1)];
        //}
        
        if([[adultTeethList objectAtIndex:btnId-1] intValue] == 1)
        {
            [adultTeethList replaceObjectAtIndex:btnId-1 withObject:@(-1)];
            [btn setImage:selectedImage forState:UIControlStateNormal];
            btn.imageView.tintColor = UIColorFromRGB(0x8D8082);
        }
        else
        {
            [adultTeethList replaceObjectAtIndex:btnId-1 withObject:@(1)];
            [btn setImage:selectedImage forState:UIControlStateNormal];
            btn.imageView.tintColor = [adultTeethColorList objectAtIndex:btnId-1];
        }
        NSLog(@"lost tooth[%d]=%d", btnId, [[adultTeethList objectAtIndex:btnId-1] intValue]);
    }
    
}

- (IBAction)nextAction:(id)sender
{
    [SVProgressHUD show];
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setValue:@([shareObject integerForKey:@"user_id"]) forKey:@"user_id"];
    [parameters setValue:@([shareObject integerForKey:@"baby_id"]) forKey:@"baby_id"];
    [parameters setValue:selectedName forKey:@"name"];
    
    int validCount = 0;
    if(selectedIndex == 0)
    {
        if(selectedWeight > 0)
        {
            //[parameters setValue:[NSString stringWithFormat:@"%f", [self convertUnit:selectedWeight unit:[unitInfo objectForKey:@"weight"] newUnit:@"kg"]] forKey:@"weight"];
            [parameters setValue:[NSString stringWithFormat:@"%f", [self convertUnit:selectedWeight unit:[unitInfo objectForKey:@"weight"] newUnit:[unitInfo objectForKey:@"weight"]]] forKey:@"weight"];
            validCount++;
        }
        if(selectedHeight > 0)
        {
            //[parameters setValue:[NSString stringWithFormat:@"%f", [self convertUnit:selectedHeight unit:[unitInfo objectForKey:@"height"] newUnit:@"cm"]] forKey:@"height"];
            [parameters setValue:[NSString stringWithFormat:@"%f", [self convertUnit:selectedHeight unit:[unitInfo objectForKey:@"height"] newUnit:[unitInfo objectForKey:@"height"]]] forKey:@"height"];
            validCount++;
        }
        if(selectedHeadSize > 0)
        {
            //[parameters setValue:[NSString stringWithFormat:@"%f", [self convertUnit:selectedHeadSize unit:[unitInfo objectForKey:@"head_size"] newUnit:@"cm"]] forKey:@"head_size"];
            [parameters setValue:[NSString stringWithFormat:@"%f", [self convertUnit:selectedHeadSize unit:[unitInfo objectForKey:@"head_size"] newUnit:[unitInfo objectForKey:@"head_size"]]] forKey:@"head_size"];
            validCount++;
        }
        
        for(int i=0; i<babyTeethList.count; i++)
        {
            if([[babyTeethList objectAtIndex:i] intValue] != 0)
            {
                [parameters setValue:[babyTeethList componentsJoinedByString:@","] forKey:@"baby_teeth"];
                validCount++;
                break;
            }
        }
        
        for(int i=0; i<adultTeethList.count; i++)
        {
            if([[adultTeethList objectAtIndex:i] intValue] != 0)
            {
                [parameters setValue:[adultTeethList componentsJoinedByString:@","] forKey:@"adult_teeth"];
                validCount++;
                break;
            }
        }
        
        if(validCount == 0)
        {
            [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:LocalizedString(@"error_no_milestone", nil) input:nil];
            [SVProgressHUD dismiss];
            return;
        }
    }
    else if(selectedIndex == 1)
    {
        if(self.txtMovement.text.length > 0)
        {
            [parameters setValue:self.txtMovement.text forKey:@"movement"];
            validCount++;
        }
    }
    else if(selectedIndex == 2)
    {
        if(self.txtVerbal.text.length > 0)
        {
            [parameters setValue:self.txtVerbal.text forKey:@"verbal"];
            validCount++;
        }
        if(self.txtLearning.text.length > 0)
        {
            [parameters setValue:self.txtLearning.text forKey:@"learning"];
            validCount++;
        }
        if(self.txtMoods.text.length > 0)
        {
            [parameters setValue:self.txtMoods.text forKey:@"moods"];
            validCount++;
        }
        
        if(validCount == 0)
        {
            [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:LocalizedString(@"error_no_milestone", nil) input:nil];
            [SVProgressHUD dismiss];
            return;
        }
    }
    else if(selectedIndex == 3)
    {
        if(self.txtVaccine.text.length > 0)
        {
            [parameters setValue:self.txtVaccine.text forKey:@"vaccine"];
            validCount++;
        }
        if(self.txtAllergies.text.length > 0)
        {
            [parameters setValue:self.txtAllergies.text forKey:@"allergies"];
            validCount++;
        }
        if(self.txtSymptom.text.length > 0)
        {
            [parameters setValue:self.txtSymptom.text forKey:@"symptom"];
            validCount++;
        }
        if(self.txtDiagnosis.text.length > 0)
        {
            [parameters setValue:self.txtDiagnosis.text forKey:@"diagnosis"];
            validCount++;
        }
        if(self.txtMedication.text.length > 0)
        {
            [parameters setValue:self.txtMedication.text forKey:@"medication"];
            validCount++;
        }
        if(self.txtClinic.text.length > 0)
        {
            [parameters setValue:self.txtClinic.text forKey:@"clinic"];
            validCount++;
        }
        if(self.txtReminderTitle.text.length > 0 && self.txtReminder.text.length > 0)
        {
            [parameters setValue:self.txtReminderTitle.text forKey:@"reminder_title"];
            [parameters setValue:selectedReminderDateTime forKey:@"reminder"];
            validCount++;
        }
        else
        {
            if(self.txtReminderTitle.text.length > 0)
            {
                if(self.txtReminder.text.length == 0)
                {
                    [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:LocalizedString(@"error_no_milestone_reminder", nil) input:nil];
                    [SVProgressHUD dismiss];
                    return;
                }
            }
            else
            {
                if(self.txtReminder.text.length > 0)
                {
                    [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:LocalizedString(@"error_no_milestone_reminder_title", nil) input:nil];
                    [SVProgressHUD dismiss];
                    return;
                }
            }
        }
        
        if(validCount == 0)
        {
            [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:LocalizedString(@"error_no_milestone", nil) input:nil];
            [SVProgressHUD dismiss];
            return;
        }
    }
    else if(selectedIndex == 4)
    {
        if(self.txtOthers.text.length > 0)
        {
            [parameters setValue:self.txtOthers.text forKey:@"others"];
            validCount++;
        }
        
        if(validCount == 0)
        {
            [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:LocalizedString(@"error_no_milestone", nil) input:nil];
            [SVProgressHUD dismiss];
            return;
        }
    }
    
    [parameters setValue:self.txtNotes.text forKey:@"notes"];
    [parameters setValue:[self formatDateTime:selectedMilestoneDateTime] forKey:@"time"];
    //[parameters setValue:[[NSString stringWithFormat:@"%@", selectedMilestoneDateTime] substringWithRange:NSMakeRange(0, 19)] forKey:@"time"];
    
    if(!self.btnPhoto1.hidden || !self.btnPhoto2.hidden || !self.btnPhoto3.hidden)
    {
        [[WebServiceManager sharedManager] initWithBaseURL:@"api/addBabyMilestone" parameters:parameters formData:^(id<AFMultipartFormData> formData) {
            
            if (!self.btnPhoto1.hidden) {
                if (videoUrl && videoUrl.length>0) {
                    [formData appendPartWithFileURL:[NSURL URLWithString:videoUrl] name:@"video" fileName:@"video.mp4" mimeType:@"video" error: nil];
                }else {
                    [formData appendPartWithFileData:UIImageJPEGRepresentation(imgUploadCropped, 0.8f) name:@"image" fileName:@"image.jpg" mimeType:@"image/jpeg"];
                    [formData appendPartWithFileData:UIImageJPEGRepresentation(imgUploadOriginal, 0.8f) name:@"image_original" fileName:@"image_original.jpg" mimeType:@"image/jpeg"];
                }
            }
            
            if (!self.btnPhoto2.hidden) {
                if (videoUrl2 && videoUrl2.length>0) {
                    [formData appendPartWithFileURL:[NSURL URLWithString:videoUrl2] name:@"video2" fileName:@"video2.mp4" mimeType:@"video" error: nil];
                }else {
                    [formData appendPartWithFileData:UIImageJPEGRepresentation(imgUploadCropped2, 0.8f) name:@"image2" fileName:@"image2.jpg" mimeType:@"image/jpeg"];
                    [formData appendPartWithFileData:UIImageJPEGRepresentation(imgUploadOriginal2, 0.8f) name:@"image_original2" fileName:@"image_original2.jpg" mimeType:@"image/jpeg"];
                }
            }
            
            if (!self.btnPhoto3.hidden) {
                if (videoUrl3 && videoUrl3.length>0) {
                    [formData appendPartWithFileURL:[NSURL URLWithString:videoUrl3] name:@"video3" fileName:@"video3.mp4" mimeType:@"video" error: nil];
                }else {
                    [formData appendPartWithFileData:UIImageJPEGRepresentation(imgUploadCropped3, 0.8f) name:@"image3" fileName:@"image3.jpg" mimeType:@"image/jpeg"];
                    [formData appendPartWithFileData:UIImageJPEGRepresentation(imgUploadOriginal3, 0.8f) name:@"image_original3" fileName:@"image_original3.jpg" mimeType:@"image/jpeg"];
                }
            }
            
//            if (imgUploadOriginal) {
//                [formData appendPartWithFileData:UIImageJPEGRepresentation(imgUploadCropped, 0.8f) name:@"image" fileName:@"image.jpg" mimeType:@"image/jpeg"];
//                [formData appendPartWithFileData:UIImageJPEGRepresentation(imgUploadOriginal, 0.8f) name:@"image_original" fileName:@"image_original.jpg" mimeType:@"image/jpeg"];
//            }
//            if (imgUploadOriginal2) {
//                [formData appendPartWithFileData:UIImageJPEGRepresentation(imgUploadCropped2, 0.8f) name:@"image2" fileName:@"image2.jpg" mimeType:@"image/jpeg"];
//                [formData appendPartWithFileData:UIImageJPEGRepresentation(imgUploadOriginal2, 0.8f) name:@"image_original2" fileName:@"image_original2.jpg" mimeType:@"image/jpeg"];
//            }
//            if (imgUploadOriginal3) {
//                [formData appendPartWithFileData:UIImageJPEGRepresentation(imgUploadCropped3, 0.8f) name:@"image3" fileName:@"image3.jpg" mimeType:@"image/jpeg"];
//                [formData appendPartWithFileData:UIImageJPEGRepresentation(imgUploadOriginal3, 0.8f) name:@"image_original3" fileName:@"image_original3.jpg" mimeType:@"image/jpeg"];
//            }
            
        } success:^(id result) {
            
            NSLog(@"result=%@", result);
            if([[result objectForKey:@"success"] boolValue])
            {
                if(self.txtReminderTitle.text.length > 0 && self.txtReminder.text.length > 0)
                {
                    [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
                    formatter.dateFormat = @"yyyy";
                    int year = [[formatter stringFromDate:selectedReminderDateTime] intValue];
                    formatter.dateFormat = @"MM";
                    int month = [[formatter stringFromDate:selectedReminderDateTime] intValue];
                    formatter.dateFormat = @"dd";
                    int day = [[formatter stringFromDate:selectedReminderDateTime] intValue];
                    formatter.dateFormat = @"HH";
                    int hour = [[formatter stringFromDate:selectedReminderDateTime] intValue];
                    formatter.dateFormat = @"mm";
                    int minute = [[formatter stringFromDate:selectedReminderDateTime] intValue];
                    NSLog(@"year=%d, month=%d, day=%d, hour=%d, minute=%d", year, month, day, hour, minute);

                    NSCalendar *calendar = [NSCalendar currentCalendar];
                    NSDateComponents *components = [calendar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitWeekOfYear | NSCalendarUnitWeekday fromDate:selectedReminderDateTime];
                    components.year = year;
                    components.month = month;
                    components.day = day;
                    components.hour = hour;
                    components.minute = minute;

                    UILocalNotification *local = [[UILocalNotification alloc] init];
                    local.fireDate = [calendar dateFromComponents:components];
                    NSLog(@"local.fireDate=%@", local.fireDate);
                    local.timeZone = [NSTimeZone defaultTimeZone];
                    local.alertTitle = [NSString stringWithFormat:@"[%@] - %@", [self.data objectForKey:@"name"], LocalizedString(@"txt_reminder", nil)];
                    local.alertBody = self.txtReminderTitle.text;
                    local.soundName = UILocalNotificationDefaultSoundName;
                    local.userInfo = [[NSMutableDictionary alloc] initWithDictionary:@{@"id":[result objectForKey:@"reference_id"], @"title":self.txtReminderTitle.text, @"baby_id":@([shareObject integerForKey:@"baby_id"]), @"baby_name":[self.data objectForKey:@"name"], @"type":@"milestones", @"repeat":@(0)}];
                    [[UIApplication sharedApplication] scheduleLocalNotification:local];
                }
                
                NSMutableArray *babyList = (NSMutableArray *)[NSKeyedUnarchiver unarchiveObjectWithData:[shareObject objectForKey:@"baby_list"]];
                [babyList replaceObjectAtIndex:[shareObject integerForKey:@"baby_index"] withObject:[result valueForKey:@"info"]];
                [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:babyList] forKey:@"baby_list"];
                [shareObject synchronize];
                
                saveCheck = YES;
                appDelegate.updateMilestonesCheck = YES;
                [self backAction];
            }
            else
            {
                [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:[result objectForKey:@"message"] input:nil];
            }
            
        } failure:^(NSError *error) {
            [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:LocalizedString(@"error_unknown", nil) input:nil];
        }];
    }
    else
    {
        [[WebServiceManager sharedManager] initWithBaseURL:@"api/addBabyMilestone" parameters:parameters success:^(id result) {
            
            NSLog(@"result=%@", result);
            if([[result objectForKey:@"success"] boolValue])
            {
                if(self.txtReminderTitle.text.length > 0 && self.txtReminder.text.length > 0)
                {
                    [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
                    formatter.dateFormat = @"yyyy";
                    int year = [[formatter stringFromDate:selectedReminderDateTime] intValue];
                    formatter.dateFormat = @"MM";
                    int month = [[formatter stringFromDate:selectedReminderDateTime] intValue];
                    formatter.dateFormat = @"dd";
                    int day = [[formatter stringFromDate:selectedReminderDateTime] intValue];
                    formatter.dateFormat = @"HH";
                    int hour = [[formatter stringFromDate:selectedReminderDateTime] intValue];
                    formatter.dateFormat = @"mm";
                    int minute = [[formatter stringFromDate:selectedReminderDateTime] intValue];
                    NSLog(@"year=%d, month=%d, day=%d, hour=%d, minute=%d", year, month, day, hour, minute);

                    NSCalendar *calendar = [NSCalendar currentCalendar];
                    NSDateComponents *components = [calendar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitWeekOfYear | NSCalendarUnitWeekday fromDate:selectedReminderDateTime];
                    components.year = year;
                    components.month = month;
                    components.day = day;
                    components.hour = hour;
                    components.minute = minute;

                    UILocalNotification *local = [[UILocalNotification alloc] init];
                    local.fireDate = [calendar dateFromComponents:components];
                    NSLog(@"local.fireDate=%@", local.fireDate);
                    local.timeZone = [NSTimeZone defaultTimeZone];
                    local.alertTitle = [NSString stringWithFormat:@"[%@] - %@", [self.data objectForKey:@"name"], LocalizedString(@"txt_reminder", nil)];
                    local.alertBody = self.txtReminderTitle.text;
                    local.soundName = UILocalNotificationDefaultSoundName;
                    local.userInfo = [[NSMutableDictionary alloc] initWithDictionary:@{@"id":[result objectForKey:@"reference_id"], @"title":self.txtReminderTitle.text, @"baby_id":@([shareObject integerForKey:@"baby_id"]), @"baby_name":[self.data objectForKey:@"name"], @"type":@"milestones", @"repeat":@(0)}];
                    [[UIApplication sharedApplication] scheduleLocalNotification:local];
                }
                
                NSMutableArray *babyList = (NSMutableArray *)[NSKeyedUnarchiver unarchiveObjectWithData:[shareObject objectForKey:@"baby_list"]];
                [babyList replaceObjectAtIndex:[shareObject integerForKey:@"baby_index"] withObject:[result valueForKey:@"info"]];
                [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:babyList] forKey:@"baby_list"];
                [shareObject synchronize];
                
                saveCheck = YES;
                appDelegate.updateMilestonesCheck = YES;
                [self backAction];
            }
            else
            {
                [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:[result objectForKey:@"message"] input:nil];
            }
            
        } failure:^(NSError *error) {
            [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:LocalizedString(@"error_unknown", nil) input:nil];
        }];
    }
}

#pragma mark - IBActions
-(IBAction)didSelectAddPhoto:(UIButton *)sender
{
    if (self.btnPhoto1.hidden || self.btnPhoto2.hidden || self.btnPhoto3.hidden)
    {
        UIAlertController *alertController=[UIAlertController alertControllerWithTitle:LocalizedString(@"title_selectImage", nil) message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        
        UIAlertAction *takePhoto=[UIAlertAction actionWithTitle:LocalizedString(@"title_camera", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self takePhotoAction];
        }];
        UIAlertAction *choosePhoto=[UIAlertAction actionWithTitle:LocalizedString(@"title_camera_roll", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self getPhotoAlbumAction];
        }];
        UIAlertAction *actionCancel=[UIAlertAction actionWithTitle:LocalizedString(@"title_cancel", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            [alertController dismissViewControllerAnimated:YES completion:nil];
        }];

        [alertController addAction:takePhoto];
        [alertController addAction:choosePhoto];
        [alertController addAction:actionCancel];
        
        if (IS_IPAD) {
            UIPopoverPresentationController *popPresenter = [alertController popoverPresentationController];
            popPresenter.sourceView = sender;
            popPresenter.sourceRect = CGRectMake(sender.bounds.origin.x, sender.bounds.size.height/2, 0, 0);
            //popPresenter.presentedViewController.preferredContentSize =  CGSizeMake(520, 400);
        }
        [self presentViewController:alertController animated:YES completion:nil];
    }
    else
    {
        [self showAlertWithTitle:@"" message:@"You can not add more than 3 photos. " input:nil];
    }
    
}

#pragma mark - Custom Methods
-(NSString *)formatDateTime:(NSDate *)sDate
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    //[dateFormatter setDateFormat:@"E d MMM, yyyy hh:mm a"];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    [dateFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
    NSString *dateStr = [dateFormatter stringFromDate:sDate];
    NSLog(@"%@",dateStr);
    return dateStr;
    
}

#pragma mark - Custom Actions
-(void)doSetupForKeywordView
{
    
    [self.btnaddKeyword addTarget:self action:@selector(didSelectAddKeyword:) forControlEvents:UIControlEventTouchUpInside];
    
    if (IS_IPAD) {
        self.keyword1.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.keyword2.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.keyword3.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.keyword4.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.keyword5.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.keyword6.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.keyword7.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.keyword8.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.keyword9.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
    }
    
    // keyword view
    keywordsArray = [[NSMutableArray alloc] init];
    self.keywordView.backgroundColor = [UIColor clearColor];
    self.keyword1.backgroundColor = [UIColor clearColor];
    self.keyword2.backgroundColor = [UIColor clearColor];
    self.keyword3.backgroundColor = [UIColor clearColor];
    self.keyword4.backgroundColor = [UIColor clearColor];
    self.keyword5.backgroundColor = [UIColor clearColor];
    self.keyword6.backgroundColor = [UIColor clearColor];
    self.keyword7.backgroundColor = [UIColor clearColor];
    self.keyword8.backgroundColor = [UIColor clearColor];
    self.keyword9.backgroundColor = [UIColor clearColor];
    
    self.keyword1.text = @"";
    self.keyword2.text = @"";
    self.keyword3.text = @"";
    self.keyword4.text = @"";
    self.keyword5.text = @"";
    self.keyword6.text = @"";
    self.keyword7.text = @"";
    self.keyword8.text = @"";
    self.keyword9.text = @"";
    
    self.keyword1.layer.cornerRadius = 4.0f;
    self.keyword1.layer.masksToBounds = YES;
    self.keyword2.layer.cornerRadius = 4.0f;
    self.keyword2.layer.masksToBounds = YES;
    self.keyword3.layer.cornerRadius = 4.0f;
    self.keyword3.layer.masksToBounds = YES;
    self.keyword4.layer.cornerRadius = 4.0f;
    self.keyword4.layer.masksToBounds = YES;
    self.keyword5.layer.cornerRadius = 4.0f;
    self.keyword5.layer.masksToBounds = YES;
    self.keyword6.layer.cornerRadius = 4.0f;
    self.keyword5.layer.masksToBounds = YES;
    self.keyword7.layer.cornerRadius = 4.0f;
    self.keyword7.layer.masksToBounds = YES;
    self.keyword8.layer.cornerRadius = 4.0f;
    self.keyword8.layer.masksToBounds = YES;
    self.keyword9.layer.cornerRadius = 4.0f;
    self.keyword9.layer.masksToBounds = YES;
    
    self.keyword1.userInteractionEnabled = YES;
    self.keyword2.userInteractionEnabled = YES;
    self.keyword3.userInteractionEnabled = YES;
    self.keyword4.userInteractionEnabled = YES;
    self.keyword5.userInteractionEnabled = YES;
    self.keyword6.userInteractionEnabled = YES;
    self.keyword7.userInteractionEnabled = YES;
    self.keyword8.userInteractionEnabled = YES;
    self.keyword9.userInteractionEnabled = YES;
    
    self.keyword1.tag = 1;
    self.keyword2.tag = 2;
    self.keyword3.tag = 3;
    self.keyword4.tag = 4;
    self.keyword5.tag = 5;
    self.keyword6.tag = 6;
    self.keyword7.tag = 7;
    self.keyword8.tag = 8;
    self.keyword9.tag = 9;

    UITapGestureRecognizer *gesture1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)] ;
    UITapGestureRecognizer *gesture2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)] ;
    UITapGestureRecognizer *gesture3 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)] ;
    UITapGestureRecognizer *gesture4 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)] ;
    UITapGestureRecognizer *gesture5 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)] ;
    UITapGestureRecognizer *gesture6 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)] ;
    UITapGestureRecognizer *gesture7 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)] ;
    UITapGestureRecognizer *gesture8 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)] ;
    UITapGestureRecognizer *gesture9 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)] ;
        
    [self.keyword1 addGestureRecognizer:gesture1];
    [self.keyword2 addGestureRecognizer:gesture2];
    [self.keyword3 addGestureRecognizer:gesture3];
    [self.keyword4 addGestureRecognizer:gesture4];
    [self.keyword5 addGestureRecognizer:gesture5];
    [self.keyword6 addGestureRecognizer:gesture6];
    [self.keyword7 addGestureRecognizer:gesture7];
    [self.keyword8 addGestureRecognizer:gesture8];
    [self.keyword9 addGestureRecognizer:gesture9];

    UILongPressGestureRecognizer *longPressGesture1 = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongGesture:)];
    longPressGesture1.minimumPressDuration = 1.5;
    UILongPressGestureRecognizer *longPressGesture2 = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongGesture:)];
    longPressGesture2.minimumPressDuration = 1.5;
    UILongPressGestureRecognizer *longPressGesture3 = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongGesture:)];
    longPressGesture3.minimumPressDuration = 1.5;
    UILongPressGestureRecognizer *longPressGesture4 = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongGesture:)];
    longPressGesture4.minimumPressDuration = 1.5;
    UILongPressGestureRecognizer *longPressGesture5 = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongGesture:)];
    longPressGesture5.minimumPressDuration = 1.5;
    UILongPressGestureRecognizer *longPressGesture6 = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongGesture:)];
    longPressGesture6.minimumPressDuration = 1.5;
    UILongPressGestureRecognizer *longPressGesture7 = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongGesture:)];
    longPressGesture7.minimumPressDuration = 1.5;
    UILongPressGestureRecognizer *longPressGesture8 = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongGesture:)];
    longPressGesture8.minimumPressDuration = 1.5;
    UILongPressGestureRecognizer *longPressGesture9 = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongGesture:)];
    longPressGesture9.minimumPressDuration = 1.5;

    [self.keyword1 addGestureRecognizer:longPressGesture1];
    [self.keyword2 addGestureRecognizer:longPressGesture2];
    [self.keyword3 addGestureRecognizer:longPressGesture3];
    [self.keyword4 addGestureRecognizer:longPressGesture4];
    [self.keyword5 addGestureRecognizer:longPressGesture5];
    [self.keyword6 addGestureRecognizer:longPressGesture6];
    [self.keyword7 addGestureRecognizer:longPressGesture7];
    [self.keyword8 addGestureRecognizer:longPressGesture8];
    [self.keyword9 addGestureRecognizer:longPressGesture9];
    
}

-(void)handleTap:(UITapGestureRecognizer *)sender
{
    NSString * seletedKey = @"";
    
    if (sender.view.tag == 1) {
        seletedKey = self.keyword1.text;
    }else if (sender.view.tag == 2) {
        seletedKey = self.keyword2.text;
    }else if (sender.view.tag == 3) {
        seletedKey = self.keyword3.text;
    }else if (sender.view.tag == 4) {
        seletedKey = self.keyword4.text;
    }else if (sender.view.tag == 5) {
        seletedKey = self.keyword5.text;
    }else if (sender.view.tag == 6) {
        seletedKey = self.keyword6.text;
    }else if (sender.view.tag == 7) {
        seletedKey = self.keyword7.text;
    }else if (sender.view.tag == 8) {
        seletedKey = self.keyword8.text;
    }else if (sender.view.tag == 9) {
        seletedKey = self.keyword9.text;
    }
    
    self.txtNotes.text = [NSString stringWithFormat:@"%@ %@",self.txtNotes.text, seletedKey];
    
}

-(void)handleLongGesture:(UILongPressGestureRecognizer *)sender
{
    int tag = (int)sender.view.tag;
    CGPoint buttonPosition = [sender.view convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *indexPathToReload = [self.tableView indexPathForRowAtPoint:buttonPosition];

//    if ( sender.state == UIGestureRecognizerStateEnded ) {
    
        if (keywordsArray.count>0) {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Remove Keyword" message:@"Are you sure you want to remove this Keyword !" preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *btnOk = [UIAlertAction actionWithTitle:LocalizedString(@"btn_ok", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                
//                if (tag == 1) {
//                    self.keyword1.text = @"";
//                    self.keyword1.backgroundColor = [UIColor clearColor];
//                }else if (tag == 2) {
//                    self.keyword2.text = @"";
//                    self.keyword2.backgroundColor = [UIColor clearColor];
//                }else if (tag == 3) {
//                    self.keyword3.text = @"";
//                    self.keyword3.backgroundColor = [UIColor clearColor];
//                }else if (tag == 4) {
//                    self.keyword4.text = @"";
//                    self.keyword4.backgroundColor = [UIColor clearColor];
//                }else if (tag == 5) {
//                    self.keyword5.text = @"";
//                    self.keyword5.backgroundColor = [UIColor clearColor];
//                }else if (tag == 6) {
//                    self.keyword6.text = @"";
//                    self.keyword6.backgroundColor = [UIColor clearColor];
//                }else if (tag == 7) {
//                    self.keyword7.text = @"";
//                    self.keyword7.backgroundColor = [UIColor clearColor];
//                }else if (tag == 8) {
//                    self.keyword8.text = @"";
//                    self.keyword8.backgroundColor = [UIColor clearColor];
//                }else if (tag == 9) {
//                    self.keyword9.text = @"";
//                    self.keyword9.backgroundColor = [UIColor clearColor];
//                }
                
                [keywordsArray removeObjectAtIndex:tag-1];

                [self addKeywordLabel];
                [self addKeywordsList];
                [self.tableView reloadData];
                //[self.tableView reloadRowsAtIndexPaths:@[indexPathToReload] withRowAnimation:UITableViewRowAnimationTop];
                
            }];
            
            UIAlertAction *btnCancel = [UIAlertAction actionWithTitle:LocalizedString(@"btn_cancel", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                [alert dismissViewControllerAnimated:YES completion:nil];
            }];
            [alert addAction:btnCancel];
            [alert addAction:btnOk];
            [self presentViewController:alert animated:YES completion:nil];
        }
//    }
}

-(void)didSelectAddKeyword:(UIButton *)sender
{
    if (keywordsArray.count<9) {
        
        CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
        NSIndexPath *indexPathToReload = [self.tableView indexPathForRowAtPoint:buttonPosition];
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:@"Add Keyword" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *btnOk = [UIAlertAction actionWithTitle:LocalizedString(@"btn_ok", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            if(alert.textFields[0].text.length > 0)
            {
                [alert dismissViewControllerAnimated:YES completion:nil];
                
                NSString *text = alert.textFields[0].text;
                [keywordsArray addObject:text];
                [self addKeywordLabel];
                [self addKeywordsList];
                [self.tableView reloadData];
                //[self.tableView reloadRowsAtIndexPaths:@[indexPathToReload] withRowAnimation:UITableViewRowAnimationBottom];
            }
            else
            {
                [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:LocalizedString(@"error_family_name", nil) input:nil];
            }
        }];
        
        UIAlertAction *btnCancel = [UIAlertAction actionWithTitle:LocalizedString(@"btn_cancel", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            
            [alert dismissViewControllerAnimated:YES completion:nil];
        }];
        
        [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
            textField.autocapitalizationType = UITextAutocapitalizationTypeWords;
        }];
        
        [alert addAction:btnCancel];
        [alert addAction:btnOk];
        
        [self presentViewController:alert animated:YES completion:nil];

    }
    else {
        [self showAlertWithTitle:LocalizedString(@"Keywords", nil) message:@"You can not add more than 9 keywords !" input:nil];
    }
    
}

-(void)addKeywordLabel
{
    self.keyword1.text = @"";
    self.keyword2.text = @"";
    self.keyword3.text = @"";
    self.keyword4.text = @"";
    self.keyword5.text = @"";
    self.keyword6.text = @"";
    self.keyword7.text = @"";
    self.keyword8.text = @"";
    self.keyword9.text = @"";
    
    self.keyword1.backgroundColor = [UIColor clearColor];
    self.keyword2.backgroundColor = [UIColor clearColor];
    self.keyword3.backgroundColor = [UIColor clearColor];
    self.keyword4.backgroundColor = [UIColor clearColor];
    self.keyword5.backgroundColor = [UIColor clearColor];
    self.keyword6.backgroundColor = [UIColor clearColor];
    self.keyword7.backgroundColor = [UIColor clearColor];
    self.keyword8.backgroundColor = [UIColor clearColor];
    self.keyword9.backgroundColor = [UIColor clearColor];

    if (keywordsArray.count>0) {
        
        for (int i=0;i<keywordsArray.count;i++) {
            if (i==0) {
                self.keyword1.text =[NSString stringWithFormat:@"%@",[keywordsArray objectAtIndex:i]];
                self.keyword1.backgroundColor = UIColorFromRGB(0xB7ADA6);
            }
            else if (i==1) {
                self.keyword2.text =[NSString stringWithFormat:@"%@",[keywordsArray objectAtIndex:i]];
                self.keyword2.backgroundColor = UIColorFromRGB(0xB7ADA6);
            }
            else if (i==2) {
                self.keyword3.text =[NSString stringWithFormat:@"%@",[keywordsArray objectAtIndex:i]];
                self.keyword3.backgroundColor = UIColorFromRGB(0xB7ADA6);
            }
            else if (i==3) {
                self.keyword4.text =[NSString stringWithFormat:@"%@",[keywordsArray objectAtIndex:i]];
                self.keyword4.backgroundColor = UIColorFromRGB(0xB7ADA6);
            }
            else if (i==4) {
                self.keyword5.text =[NSString stringWithFormat:@"%@",[keywordsArray objectAtIndex:i]];
                self.keyword5.backgroundColor = UIColorFromRGB(0xB7ADA6);
            }
            else if (i==5) {
                self.keyword6.text =[NSString stringWithFormat:@"%@",[keywordsArray objectAtIndex:i]];
                self.keyword6.backgroundColor = UIColorFromRGB(0xB7ADA6);
            }
            else if (i==6) {
                self.keyword7.text =[NSString stringWithFormat:@"%@",[keywordsArray objectAtIndex:i]];
                self.keyword7.backgroundColor = UIColorFromRGB(0xB7ADA6);
            }
            else if (i==7) {
                self.keyword8.text =[NSString stringWithFormat:@"%@",[keywordsArray objectAtIndex:i]];
                self.keyword8.backgroundColor = UIColorFromRGB(0xB7ADA6);
            }
            else if (i==8) {
                self.keyword9.text =[NSString stringWithFormat:@"%@",[keywordsArray objectAtIndex:i]];
                self.keyword9.backgroundColor = UIColorFromRGB(0xB7ADA6);
            }
        }
        
    }
    
    //                UIFont *lblFont;
    //                if (IS_IPAD) {
    //                    lblFont = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
    //                }else {
    //                    lblFont = [UIFont fontWithName:@"SFUIDisplay-Medium" size:16.0f];
    //                }
    
    //                NSDictionary *userAttributes = @{NSFontAttributeName: lblFont};
    //                NSString *text = alert.textFields[0].text;
    //                const CGSize textSize = [text sizeWithAttributes: userAttributes];
    
    //                CGRect lastKeyFrame = CGRectMake(0, 0, 0, 0);
    //                if (keywordsArray.count>0) {
    //                    UILabel *lastKeyLabel = [keywordsArray lastObject];
    //                    lastKeyFrame = lastKeyLabel.frame;
    //                }
    
    //                UILabel *lblKey = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, textSize.width+6, 36)];
    //                lblKey.text = text;
    //                lblKey.textColor = UIColorFromRGB(0xFFFFFF);
    //                lblKey.textAlignment =  NSTextAlignmentCenter;
    //                lblKey.backgroundColor = UIColorFromRGB(0xB7ADA6);
    //                lblKey.layer.cornerRadius = 6.0;
    //                lblKey.numberOfLines = 0;
    //                [self.keywordView addSubview:lblKey];
    //                [self.keywordView layoutIfNeeded];
    
    //                [keywordsArray addObject:lblKey];
    
}

-(void)getKeywordsList
{
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setValue:@([shareObject integerForKey:@"user_id"]) forKey:@"user_id"];
    [parameters setValue:@([shareObject integerForKey:@"baby_id"]) forKey:@"baby_id"];
    [parameters setValue:@"milestones" forKey:@"type"];
    //[parameters setValue:selectedName forKey:@"category"];
    
    [[WebServiceManager sharedManager] initWithBaseURL:@"api/getKeywords" parameters:parameters success:^(id result) {
        
        if ([[result valueForKey:@"success"] boolValue]) {
            NSMutableArray *list = (NSMutableArray *)[result valueForKey:@"list"];
            if(list.count > 0){
                
                keywordslistDict = [[NSDictionary alloc] initWithDictionary:[result valueForKey:@"list"]];
                NSString *str = [keywordslistDict objectForKey:selectedName];
                if (str.length>0) {
                    keywordsArray = [[NSMutableArray alloc] initWithArray:[str componentsSeparatedByString:@","]];
                }else {
                    keywordsArray = [[NSMutableArray alloc] init];
                }

                [self addKeywordLabel];
                [self.tableView reloadData];
            }
        }
        else {
            NSLog(@"error");
        }
        
    } failure:^(NSError *error) {
        
    }];
}

-(void)addKeywordsList
{
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setValue:@([shareObject integerForKey:@"user_id"]) forKey:@"user_id"];
    [parameters setValue:@([shareObject integerForKey:@"baby_id"]) forKey:@"baby_id"];
    [parameters setValue:@"milestones" forKey:@"type"];
    [parameters setValue:selectedName forKey:@"category"];
    [parameters setValue:@1 forKey:@"notes"];
    
    if (keywordsArray.count>0) {
        for(int i=0; i<keywordsArray.count; i++){
            [parameters setValue:[keywordsArray componentsJoinedByString:@","] forKey:@"keywords"];
        }
    }else{
        [parameters setValue:@"" forKey:@"keywords"];
    }
    
    [[WebServiceManager sharedManager] initWithBaseURL:@"api/setKeywords" parameters:parameters success:^(id result) {
        
        if ([[result valueForKey:@"success"] boolValue]) {
            NSMutableArray *list = (NSMutableArray *)[result valueForKey:@"list"];
            if(list.count > 0){
                
                keywordslistDict = [[NSDictionary alloc] initWithDictionary:[result valueForKey:@"list"]];
                NSString *str = [keywordslistDict objectForKey:selectedName];
                if (str.length>0) {
                    keywordsArray = [[NSMutableArray alloc] initWithArray:[str componentsSeparatedByString:@","]];
                }else {
                    keywordsArray = [[NSMutableArray alloc] init];
                }

                //keywordsArray = list;
                //[list removeAllObjects];
            }
        }
        else {
            NSLog(@"error");
        }
        
    } failure:^(NSError *error) {
        
    }];
    
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)aTableView
{
    return 7;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return nil;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(selectedIndex == 0)
    {
        if(indexPath.section == 2 || indexPath.section == 3 || indexPath.section == 4 || indexPath.section == 5) return 0;
    }
    else if(selectedIndex == 1)
    {
        if(indexPath.section == 1 || indexPath.section == 3 || indexPath.section == 4 || indexPath.section == 5) return 0;
    }
    else if(selectedIndex == 2)
    {
        if(indexPath.section == 1 || indexPath.section == 2 || indexPath.section == 4 || indexPath.section == 5) return 0;
    }
    else if(selectedIndex == 3)
    {
        if(indexPath.section == 1 || indexPath.section == 2 || indexPath.section == 3 || indexPath.section == 5) return 0;
    }
    else if(selectedIndex == 4)
    {
        if(indexPath.section == 1 || indexPath.section == 2 || indexPath.section == 3 || indexPath.section == 4) return 0;
    }
    
    return UITableViewAutomaticDimension;
    
}
- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(selectedIndex == 0)
    {
        if(indexPath.section == 2 || indexPath.section == 3 || indexPath.section == 4 || indexPath.section == 5) return 0;
    }
    else if(selectedIndex == 1)
    {
        if(indexPath.section == 1 || indexPath.section == 3 || indexPath.section == 4 || indexPath.section == 5) return 0;
    }
    else if(selectedIndex == 2)
    {
        if(indexPath.section == 1 || indexPath.section == 2 || indexPath.section == 4 || indexPath.section == 5) return 0;
    }
    else if(selectedIndex == 3)
    {
        if(indexPath.section == 1 || indexPath.section == 2 || indexPath.section == 3 || indexPath.section == 5) return 0;
    }
    else if(selectedIndex == 4)
    {
        if(indexPath.section == 1 || indexPath.section == 2 || indexPath.section == 3 || indexPath.section == 4) return 0;
    }
    
    return UITableViewAutomaticDimension;

}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    if(indexPath.section == 0)
    {
        // Date & Time
        if(indexPath.row == 1)
        {
            NSDate *minimumDate = [[NSDate date] dateByAddingTimeInterval:-20*365*24*60*60];
            NSDate *maximumDate = [NSDate date];

            NSLog(@"selectedMilestoneDateTime=%@", selectedMilestoneDateTime);
            [ActionSheetDatePicker showPickerWithTitle:@"" datePickerMode:UIDatePickerModeDate selectedDate:selectedMilestoneDateTime minimumDate:minimumDate maximumDate:maximumDate doneBlock:^(ActionSheetDatePicker *picker, id selectedDate, id origin) {

                selectedMilestoneDate = selectedDate;
                NSLog(@"selectedMilestoneDate=%@", selectedMilestoneDate);

                formatter.dateFormat = @"d MMM yyyy";
                NSString *milestoneDate = [formatter stringFromDate:selectedDate];

                [self performSelector:@selector(selectMilestoneTime:) withObject:milestoneDate afterDelay:0.5];

            } cancelBlock:^(ActionSheetDatePicker *picker) {

            } origin:self.view];
        }
    }
    else if(indexPath.section == 1)
    {
        NSString *unit = @"";
        NSArray *stringList = [[NSArray alloc] init];
        NSArray *initialList = [[NSArray alloc] init];
        ActionSheetMultipleStringPicker *picker;
        
        NSMutableArray *babyList = (NSMutableArray *)[NSKeyedUnarchiver unarchiveObjectWithData:[shareObject objectForKey:@"baby_list"]];
        int selectedBabyIndex = (int)[shareObject integerForKey:@"baby_index"];

        if(indexPath.row == 0)
        {
            unit = [unitInfo objectForKey:@"height"];
            if ([unit isKindOfClass:[NSNull class]]) {
                unit = @"cm";
            }
            if([[unitInfo objectForKey:@"height"] isEqualToString:@"cm"]){
                stringList = [NSArray arrayWithObjects:no300List, digitList, nil];
            }else{
                stringList = [NSArray arrayWithObjects:no100List, digitList, nil];
            }
            
            if (selectedHeightIndexes == nil) {
                NSString *babyHeight = [[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"height"];
                NSString *str = [self convertUnit:babyHeight unit:[unitInfo objectForKey:@"height"] spacing:NO];
                str = [str stringByReplacingOccurrencesOfString:@"cm" withString:@""];
                str = [str stringByReplacingOccurrencesOfString:@"in" withString:@""];
                selectedHeightIndexes = [str componentsSeparatedByString:@"."];
            }
            
            if (selectedHeightIndexes.count>1) {
                initialList = [NSArray arrayWithObjects:@([selectedHeightIndexes[0] intValue]), @([selectedHeightIndexes[1] intValue]), nil];
            }else {
                initialList = [NSArray arrayWithObjects:@([selectedHeightIndexes[0] intValue]), @0, nil];
            }

            picker = [ActionSheetMultipleStringPicker showPickerWithTitle:@"" rows:stringList initialSelection:initialList doneBlock:^(ActionSheetMultipleStringPicker *picker, NSArray *selectedIndexes, id selectedValues) {
                
                selectedHeight = [[selectedValues componentsJoinedByString:@"."] floatValue];
                NSLog(@"selectedHeight=%f", selectedHeight);
                selectedHeightIndexes = selectedIndexes;
                self.txtHeight.text = [self convertUnit:[NSString stringWithFormat:@"%f %@", selectedHeight, [unitInfo objectForKey:@"height"]] unit:[unitInfo objectForKey:@"height"] spacing:YES];
                
                [self.tableView reloadData];
                
            } cancelBlock:^(ActionSheetMultipleStringPicker *picker) {
                
            } origin:self.view];
        }
        else if(indexPath.row == 1)
        {
            unit = [unitInfo objectForKey:@"weight"];
            if ([unit isKindOfClass:[NSNull class]]) {
                unit = @"kg";
            }
            stringList = [NSArray arrayWithObjects:no200List, digit2List, nil];
            
            if (selectedWeightIndexes == nil) {
                NSString *babyWeight = [[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"weight"];
                NSString *str = [self convertUnit:babyWeight unit:[unitInfo objectForKey:@"weight"] spacing:NO];
                str = [str stringByReplacingOccurrencesOfString:@"kg" withString:@""];
                str = [str stringByReplacingOccurrencesOfString:@"lb" withString:@""];
                selectedWeightIndexes = [str componentsSeparatedByString:@"."];
            }
            
            if (selectedWeightIndexes.count>1) {
                initialList = [NSArray arrayWithObjects:@([selectedWeightIndexes[0] intValue]), @([selectedWeightIndexes[1] intValue]), nil];
            }else {
                initialList = [NSArray arrayWithObjects:@([selectedWeightIndexes[0] intValue]), @0, nil];
            }

            picker = [ActionSheetMultipleStringPicker showPickerWithTitle:@"" rows:stringList initialSelection:initialList doneBlock:^(ActionSheetMultipleStringPicker *picker, NSArray *selectedIndexes, id selectedValues) {
                
                selectedWeight = [[selectedValues componentsJoinedByString:@"."] floatValue];
                NSLog(@"selectedWeight=%f", selectedWeight);
                selectedWeightIndexes = selectedIndexes;
                self.txtWeight.text = [self convertUnit:[NSString stringWithFormat:@"%.2f %@", selectedWeight, [unitInfo objectForKey:@"weight"]] unit:[unitInfo objectForKey:@"weight"] spacing:YES];
                
                [self.tableView reloadData];
                
            } cancelBlock:^(ActionSheetMultipleStringPicker *picker) {
                
            } origin:self.view];
        }
        else if(indexPath.row == 2)
        {
            unit = [unitInfo objectForKey:@"head_size"];
            if ([unit isKindOfClass:[NSNull class]]) {
                unit = @"cm";
            }
            if([[unitInfo objectForKey:@"head_size"] isEqualToString:@"cm"]){
                stringList = [NSArray arrayWithObjects:no100List, digitList, nil];
            }else{
                stringList = [NSArray arrayWithObjects:no60List, digitList, nil];
            }
            
            if (selectedHeadSizeIndexes == nil) {
                NSString *babyHeadSize = [[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"head_size"];
                NSString *str = [self convertUnit:babyHeadSize unit:[unitInfo objectForKey:@"head_size"] spacing:NO];
                str = [str stringByReplacingOccurrencesOfString:@"cm" withString:@""];
                str = [str stringByReplacingOccurrencesOfString:@"in" withString:@""];
                selectedHeadSizeIndexes = [str componentsSeparatedByString:@"."];
            }
            
            if (selectedHeadSizeIndexes.count>1) {
                initialList = [NSArray arrayWithObjects:@([selectedHeadSizeIndexes[0] intValue]), @([selectedHeadSizeIndexes[1] intValue]), nil];
            }else {
                initialList = [NSArray arrayWithObjects:@([selectedHeadSizeIndexes[0] intValue]), @0, nil];
            }
                
            picker = [ActionSheetMultipleStringPicker showPickerWithTitle:@"" rows:stringList initialSelection:initialList doneBlock:^(ActionSheetMultipleStringPicker *picker, NSArray *selectedIndexes, id selectedValues) {
                
                selectedHeadSize = [[selectedValues componentsJoinedByString:@"."] floatValue];
                NSLog(@"selectedHeadSize=%f", selectedHeadSize);
                selectedHeadSizeIndexes = selectedIndexes;
                self.txtHeadSize.text = [self convertUnit:[NSString stringWithFormat:@"%f %@", selectedHeadSize, [unitInfo objectForKey:@"head_size"]] unit:[unitInfo objectForKey:@"head_size"] spacing:YES];
                
                [self.tableView reloadData];
                
            } cancelBlock:^(ActionSheetMultipleStringPicker *picker) {
                
            } origin:self.view];
        }
        
        UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(40, 0, (picker.viewSize.width-40)/2, 36)];
        label1.text = @".";
        label1.textAlignment = NSTextAlignmentRight;
        label1.font = [UIFont systemFontOfSize:20.0f];
        label1.backgroundColor = [[UIColor redColor] colorWithAlphaComponent:0.f];
        [[picker.pickerView.subviews objectAtIndex:1] addSubview:label1];
        
        UILabel *label2 = [[UILabel alloc] initWithFrame:CGRectMake(40+(picker.viewSize.width-40)/2, 0, (picker.viewSize.width-40)/2, 36)];
        label2.text = [NSString stringWithFormat:@"      %@", unit];
        label2.textAlignment = NSTextAlignmentCenter;
        label2.font = [UIFont systemFontOfSize:20.0f];
        label2.backgroundColor = [[UIColor greenColor] colorWithAlphaComponent:0.f];
        [[picker.pickerView.subviews objectAtIndex:1] addSubview:label2];
    }
    else if(indexPath.section == 4)
    {
        // Date & Time
        if(indexPath.row == 7)
        {
            NSDate *minimumDate = [NSDate date];
            NSDate *maximumDate = [[NSDate date] dateByAddingTimeInterval:4*365*24*60*60];
            NSLog(@"here");
            
            [ActionSheetDatePicker showPickerWithTitle:@"" datePickerMode:UIDatePickerModeDate selectedDate:selectedReminderTime minimumDate:minimumDate maximumDate:maximumDate doneBlock:^(ActionSheetDatePicker *picker, id selectedDate, id origin) {
                
                selectedReminderDate = selectedDate;
                NSLog(@"selectedReminderDate=%@", selectedReminderDate);
                
                formatter.dateFormat = @"d MMM yyyy";
                NSString *reminderDate = [formatter stringFromDate:selectedDate];
                
                [self performSelector:@selector(selectReminderTime:) withObject:reminderDate afterDelay:0.5];
                
            } cancelBlock:^(ActionSheetDatePicker *picker) {
                
            } origin:self.view];
        }
    }
}

#pragma mark -
#pragma mark UIScrollViewDelegate Methods
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if([self.navigationController.visibleViewController isKindOfClass:[MilestoneAddViewController class]])
    {
        CGPoint translation = [scrollView.panGestureRecognizer translationInView:scrollView.superview];
        if(translation.y > 0)
        {
            [self.view endEditing:YES];
        }
    }
}




#pragma mark - UIImagePickerController Delegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    NSString *mediaType = info[UIImagePickerControllerMediaType];
    if ([mediaType isEqualToString:(NSString *)kUTTypeImage])
    {
        NSLog(@"Media is an image");
        // Handle selected image
        UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
        NSLog(@"image=%@", image);
        
        if (self.btnPhoto1.hidden) {
            imgUploadOriginal = [info objectForKey:UIImagePickerControllerOriginalImage];
        }else if (self.btnPhoto2.hidden) {
            imgUploadOriginal2 = [info objectForKey:UIImagePickerControllerOriginalImage];
        }else if (self.btnPhoto3.hidden) {
            imgUploadOriginal3 = [info objectForKey:UIImagePickerControllerOriginalImage];
        }
        
        if (picker.sourceType == UIImagePickerControllerSourceTypeCamera) {
            UIImageWriteToSavedPhotosAlbum([info objectForKey:UIImagePickerControllerOriginalImage], self, @selector(savedPhotoImage:didFinishSavingWithError:contextInfo:), nil);
        }
        
        if (IS_IPAD) {
            if (self.popover.isPopoverVisible) {
                [self.popover dismissPopoverAnimated:NO];
            }
            [picker dismissViewControllerAnimated:YES completion:^{
                [self openEditor:[info objectForKey:UIImagePickerControllerOriginalImage]];
            }];
        } else {
            [picker dismissViewControllerAnimated:YES completion:^{
                [self openEditor:[info objectForKey:UIImagePickerControllerOriginalImage]];
            }];
        }
    }
    else if ([mediaType isEqualToString:(NSString *)kUTTypeMovie])
    {
        BOOL ntwrkFlagStatus = [[NSUserDefaults standardUserDefaults] boolForKey:@"can_Upload_Using_Mobile_Network"];
        BOOL checkWifiStatus = [COMMON_HELPER checkforWifiNetwork];
        
        if ((checkWifiStatus==NO) && (ntwrkFlagStatus==NO)) {
            [self dismissViewControllerAnimated:NO completion:^{
            }];
            [self showAlertWithTitle:LocalizedString(@"mobile_data_usages_title", nil) message:LocalizedString(@"mobile_data_usages_message", nil) input:nil];
            return;
        }else {
            
            NSLog(@"Media is a video");
            NSURL *vUrl= (NSURL*)[info objectForKey:UIImagePickerControllerMediaURL];
            NSString *moviePath = [vUrl path];
            
            NSURL* uploadURL;
            if (self.btnPhoto1.hidden) {
                [[NSFileManager defaultManager] removeItemAtPath:[NSString stringWithFormat:@"%@video1.mov", NSTemporaryDirectory()] error:NULL];
                uploadURL = [NSURL fileURLWithPath:
                             [NSTemporaryDirectory() stringByAppendingPathComponent:@"video1.mov"]];
            }else if (self.btnPhoto2.hidden) {
                [[NSFileManager defaultManager] removeItemAtPath:[NSString stringWithFormat:@"%@video2.mov", NSTemporaryDirectory()] error:NULL];
                uploadURL = [NSURL fileURLWithPath:
                             [NSTemporaryDirectory() stringByAppendingPathComponent:@"video2.mov"]];
            }else if (self.btnPhoto3.hidden) {
                [[NSFileManager defaultManager] removeItemAtPath:[NSString stringWithFormat:@"%@video3.mov", NSTemporaryDirectory()] error:NULL];
                uploadURL = [NSURL fileURLWithPath:
                             [NSTemporaryDirectory() stringByAppendingPathComponent:@"video3.mov"]];
            }
            
            // Generate Thumbnail Image.
            UIImage *tImg = [COMMON_HELPER generateThumbnailImageFromVideoWithUrl:vUrl];
            
            [SVProgressHUD showWithStatus:LocalizedString(@"processing_video", nil)];
            [COMMON_HELPER compressVideo:vUrl outputURL:uploadURL handler:^(AVAssetExportSession *completion) {
                
                if (completion.status == AVAssetExportSessionStatusCompleted) {
                    NSData *newDataForUpload = [NSData dataWithContentsOfURL:uploadURL];
                    NSLog(@"Size of new Video after compression is (bytes):%lu",(unsigned long)[newDataForUpload length]);
                    
                    CGFloat sizeInMB = ([newDataForUpload length]/1000)/1000;
                    if (sizeInMB>15) {
                        [self showAlertWithTitleCallback:@"Large video size" message:@"Can not add this video." callback:^(UIAlertAction *action) {
                            [[NSFileManager defaultManager] removeItemAtPath:[NSString stringWithFormat:@"%@", uploadURL] error:NULL];
                        }];
                    }
                    else {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            if (self.btnPhoto1.hidden) {
                                self.btnPhoto1.hidden = false;
                                videoUrl = [uploadURL absoluteString];
                                [self.btnPhoto1 setImage:tImg forState:UIControlStateNormal];
                            }else if (self.btnPhoto2.hidden) {
                                self.btnPhoto2.hidden = false;
                                videoUrl2 = [uploadURL absoluteString];
                                [self.btnPhoto2 setImage:tImg forState:UIControlStateNormal];
                            }else if (self.btnPhoto3.hidden) {
                                self.btnPhoto3.hidden = false;
                                videoUrl3 = [uploadURL absoluteString];
                                [self.btnPhoto3 setImage:tImg forState:UIControlStateNormal];
                            }
                        });
                        
                    }
                }
                [SVProgressHUD dismiss];
                
            }];
            
            if (picker.sourceType == UIImagePickerControllerSourceTypeCamera) {
                if (UIVideoAtPathIsCompatibleWithSavedPhotosAlbum (moviePath)) {
                    UISaveVideoAtPathToSavedPhotosAlbum (moviePath, nil, nil, nil);
                }
            }
            
            [self dismissViewControllerAnimated:NO completion:^{
            }];
        }
    }
    
    /*
    float scale = self.view.bounds.size.width*2/image.size.width;
    CGSize newSize = CGSizeMake(self.view.bounds.size.width*2, image.size.height*scale);
    if(image.size.width > image.size.height)
    {
        scale = self.view.bounds.size.width*2/image.size.height;
        newSize = CGSizeMake(image.size.width*scale, self.view.bounds.size.width*2);
    }
    UIGraphicsBeginImageContext(newSize);
    [image drawInRect:(CGRect){0, 0, newSize}];
    image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    [self.btnPhoto1 setImage:image forState:UIControlStateNormal];
    self.btnPhoto1.hidden = NO;
    
    [self dismissViewControllerAnimated:NO completion:^{
        
    }];
     */
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)openEditor:(UIImage*) image1
{
    PECropViewController *controller = [[PECropViewController alloc] init];
    controller.delegate = self;
    controller.toolbarHidden = YES;
    controller.image = image1;
    
//    CGFloat width = image1.size.width;
//    CGFloat height = image1.size.height;
//    CGFloat length = MIN(width, height);
//    controller.imageCropRect = CGRectMake((width - length) / 2,(height - length) / 2,length,length);
    
    controller.cropAspectRatio = [[NSUserDefaults standardUserDefaults] floatForKey:@"Img_Aspect"];
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:controller];
    
    if (IS_IPAD) {
        navigationController.modalPresentationStyle = UIModalPresentationFullScreen;
    }
    [self presentViewController:navigationController animated:YES completion:NULL];
    
}


- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return largeImageView;
}

#pragma mark - PECropViewControllerDelegate methods

- (void)cropViewController:(PECropViewController *)controller didFinishCroppingImage:(UIImage *)croppedImage transform:(CGAffineTransform)transform cropRect:(CGRect)cropRect
{
    [controller dismissViewControllerAnimated:YES completion:NULL];
    
    float scale = self.view.bounds.size.width*2/croppedImage.size.width;
    CGSize newSize = CGSizeMake(self.view.bounds.size.width*2, croppedImage.size.height*scale);
    
//    if (self.btnPhoto1.hidden) {
//        imgUploadCropped = croppedImage;
//    }else if (!imgUploadCropped2) {
//        imgUploadCropped2 = croppedImage;
//    }else if (!imgUploadCropped3) {
//        imgUploadCropped3 = croppedImage;
//    }
    
    if(croppedImage.size.width > croppedImage.size.height)
    {
        scale = self.view.bounds.size.width*2/croppedImage.size.height;
        newSize = CGSizeMake(croppedImage.size.width*scale, self.view.bounds.size.width*2);
    }
    UIGraphicsBeginImageContext(newSize);
    [croppedImage drawInRect:(CGRect){0, 0, newSize}];
    croppedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    if (self.btnPhoto1.hidden) {
        imgUploadCropped = croppedImage;
        self.btnPhoto1.hidden = false;
        [self.btnPhoto1 setImage:croppedImage forState:UIControlStateNormal];
    }else if (self.btnPhoto2.hidden) {
        imgUploadCropped2 = croppedImage;
        self.btnPhoto2.hidden = false;
        [self.btnPhoto2 setImage:croppedImage forState:UIControlStateNormal];
    }else if (self.btnPhoto3.hidden) {
        imgUploadCropped3 = croppedImage;
        self.btnPhoto3.hidden = false;
        [self.btnPhoto3 setImage:croppedImage forState:UIControlStateNormal];
    }
    
}

- (void)cropViewControllerDidCancel:(PECropViewController *)controller
{
//    self.btnPhoto1.hidden = YES;
//    self.btnPhoto2.hidden = YES;
//    self.btnPhoto3.hidden = YES;
    
//    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
//        // [self updateEditButtonEnabled];
//    }
    [controller dismissViewControllerAnimated:YES completion:NULL];
}


- (void)customizeFont
{
    if (IS_IPAD) {
        self.lblDate.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f];
        self.txtDate.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f];
        self.lblHeight.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f];
        self.txtHeight.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f];
        self.lblWeight.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f];
        self.txtWeight.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f];
        self.lblHeadSize.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f];
        self.txtHeadSize.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f];
        self.lblMovement.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f];
        self.txtMovement.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f];
        self.lblMoods.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f];
        self.txtMoods.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f];
        self.lblVerbal.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f];
        self.txtVerbal.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f];
        self.lblLearning.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f];
        self.txtLearning.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f];
        self.lblVaccine.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f];
        self.txtVaccine.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f];
        self.lblAllergies.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f];
        self.txtAllergies.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f];
        self.lblSymptom.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f];
        self.txtSymptom.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f];
        self.lblDiagnosis.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f];
        self.txtDiagnosis.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f];
        self.lblMedication.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f];
        self.txtMedication.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f];
        self.lblClinic.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f];
        self.txtClinic.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f];
        self.lblReminder.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f];
        self.txtReminder.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f];
        self.lblReminderTitle.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f];
        self.txtReminderTitle.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f];
        self.lblOthers.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f];
        self.txtOthers.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f];
        self.lblChecklist.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f];
        self.txtChecklist.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f];
        self.lblNotes.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f];
        self.txtNotes.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f];
        
        self.lblTeething.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f];
        self.lblPhoto.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f];
        
        self.titleGrow.font = [UIFont fontWithName:@"SFUIDisplay-Bold" size:22.0f];
        self.titleMove.font = [UIFont fontWithName:@"SFUIDisplay-Bold" size:22.0f];
        self.titleIQ.font = [UIFont fontWithName:@"SFUIDisplay-Bold" size:22.0f];
        self.titleHealth.font = [UIFont fontWithName:@"SFUIDisplay-Bold" size:22.0f];
        self.titleOthers.font = [UIFont fontWithName:@"SFUIDisplay-Bold" size:22.0f];
        
    }
    
}

- (void) savedPhotoImage:(UIImage *)image
didFinishSavingWithError:(NSError *)error
             contextInfo:(void *)contextInfo
{
    NSLog(@"This image has been saved to your camera roll");
}

#pragma mark- Teething view setup
-(void) doSetupForTeethingView
{
    self.btnBaby.backgroundColor = UIColorFromRGB(0x8D8082);
    self.btnBaby.selected = YES;
    self.viewTeethingBaby.hidden = NO;
    self.btnAdult.backgroundColor = UIColorFromRGB(0xE0D3CB);
    self.btnAdult.selected = NO;
    self.viewTeethingAdult.hidden = YES;
    self.btnBaby.layer.cornerRadius = 3.f;
    self.btnBaby.layer.masksToBounds = YES;
    self.btnAdult.layer.cornerRadius = 3.f;
    self.btnAdult.layer.masksToBounds = YES;
    if(IS_IPAD) {
        [self.btnBaby.titleLabel setFont:[UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f]];
        [self.btnAdult.titleLabel setFont:[UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f]];
        
        self.viewTeethBaby.transform = CGAffineTransformMakeScale(1.2, 1.2);
        self.viewTeethGuideBaby.transform = CGAffineTransformMakeScale(1.2, 1.2);
        self.viewTeethAdult.transform = CGAffineTransformMakeScale(1.2, 1.2);
        self.viewTeethGuideAdult.transform = CGAffineTransformMakeScale(1.2, 1.2);
    }
    [self.btnBaby addTarget:self action:@selector(didSelectBaby:) forControlEvents:UIControlEventTouchUpInside];
    [self.btnAdult addTarget:self action:@selector(didSelectAdult:) forControlEvents:UIControlEventTouchUpInside];
    
}

-(void)didSelectBaby:(UIButton *)sender {
    self.btnBaby.backgroundColor = UIColorFromRGB(0x8D8082);
    self.btnBaby.selected = YES;
    self.btnAdult.backgroundColor = UIColorFromRGB(0xE0D3CB);
    self.btnAdult.selected = NO;
    self.viewTeethingBaby.hidden = NO;
    self.viewTeethingAdult.hidden = YES;
}

-(void)didSelectAdult:(UIButton *)sender {
    self.btnAdult.backgroundColor = UIColorFromRGB(0x8D8082);
    self.btnAdult.selected = YES;
    self.btnBaby.backgroundColor = UIColorFromRGB(0xE0D3CB);
    self.btnBaby.selected = NO;
    self.viewTeethingAdult.hidden = NO;
    self.viewTeethingBaby.hidden = YES;
    
}

-(void)setAdultTeeth:(NSMutableArray *)teethArray
{
    for(int i=0; i<[teethArray count]; i++)
    {
        int toothId = [[[teethArray objectAtIndex:i] objectForKey:@"teeth_id"] intValue];
        NSString *content = [[teethArray objectAtIndex:i] objectForKey:@"content"];
        
        [adultTeethBackupList replaceObjectAtIndex:toothId-20-1 withObject:@(1)];
        if([content isEqualToString:@"Lost Tooth"]){
            [adultTeethBackupList replaceObjectAtIndex:toothId-20-1 withObject:@(-1)];
        }

        UIImage *selectedImage = [UIImage imageNamed:[NSString stringWithFormat:@"btn_teeth%d_selected", toothId]];
        selectedImage = [selectedImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];

        TeethButton * button = [self.viewTeethAdult viewWithTag:toothId + 80];
        [button setImage:selectedImage forState:UIControlStateNormal];
        button.imageView.tintColor = [adultTeethColorList objectAtIndex:toothId-20-1];
        if([content isEqualToString:@"Lost Tooth"]){
            button.imageView.tintColor = UIColorFromRGB(0x8D8082);
        }

    }
    NSLog(@"adultTeethBackupList=%@", adultTeethBackupList);
    
}

-(void)setBabyTeeth:(NSMutableArray *)babyTeethArray
{
    for(int i=0; i<[babyTeethArray count]; i++)
    {
        int toothId = [[[babyTeethArray objectAtIndex:i] objectForKey:@"teeth_id"] intValue];
        NSString *content = [[babyTeethArray objectAtIndex:i] objectForKey:@"content"];
        [babyTeethBackupList replaceObjectAtIndex:toothId-1 withObject:@(1)];
        if([content isEqualToString:@"Lost Tooth"])
        {
            [babyTeethBackupList replaceObjectAtIndex:toothId-1 withObject:@(-1)];
        }
        
        UIImage *selectedImage = [UIImage imageNamed:[NSString stringWithFormat:@"btn_teeth%d_selected", toothId]];
        selectedImage = [selectedImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        
        UIButton * button = [self.viewTeethBaby viewWithTag:toothId];
        [button setImage:selectedImage forState:UIControlStateNormal];
        button.imageView.tintColor = [babyTeethColorList objectAtIndex:toothId-1];
        if([content isEqualToString:@"Lost Tooth"]){
            button.imageView.tintColor = UIColorFromRGB(0x8D8082);
        }
        
        
    }
    NSLog(@"babyTeethBackupList=%@", babyTeethBackupList);
}

@end
