//
//  MilestoneEditViewController.m
//  Joey
//
//  Created by csl on 6/4/2017.
//  Copyright © 2017 Auxilia. All rights reserved.
//

#import "AppDelegate.h"
#import "MilestoneEditViewController.h"
#import "TeethButton.h"

@interface MilestoneEditViewController ()<PECropViewControllerDelegate>
{
    NSUserDefaults *shareObject;
    AppDelegate *appDelegate;
    NSDateFormatter *formatter;
    NSMutableDictionary *unitInfo;
    NSMutableDictionary *milestoneInfo;
    NSMutableArray *no100List;
    NSMutableArray *no300List;
    NSMutableArray *no200List;
    NSMutableArray *no24List;
    NSMutableArray *no60List;
    NSMutableArray *digitList;
    NSMutableArray *digit2List;
    NSMutableArray *babyTeethList;
    NSMutableArray *adultTeethList;
    
    NSArray *selectedWeightIndexes;
    NSArray *selectedHeightIndexes;
    NSArray *selectedHeadSizeIndexes;
    NSDate *selectedMilestoneDate;
    NSDate *selectedMilestoneTime;
    NSDate *selectedMilestoneDateTime;
    NSDate *selectedReminderDate;
    NSDate *selectedReminderTime;
    NSDate *selectedReminderDateTime;
    
    
    BOOL saveCheck;
    float selectedWeight;
    float selectedHeight;
    float selectedHeadSize;
    
    UIImageView *largeImageView;
    NSMutableArray *keywordsArray;
    
    UIImage *imgUploadPhoto;
    UIImage *imgUploadCropped;
    UIImage *imgUploadPhoto2;
    UIImage *imgUploadCropped2;
    UIImage *imgUploadPhoto3;
    UIImage *imgUploadCropped3;
    NSDictionary *keywordslistDict;

    NSString *videoUrl;
    NSString *videoUrl2;
    NSString *videoUrl3;
    BOOL havePhotoUrl;
    BOOL havePhotoUrl2;
    BOOL havePhotoUrl3;
    
}

@property (nonatomic) UIPopoverController *popover;

@end

@implementation MilestoneEditViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    shareObject = [NSUserDefaults standardUserDefaults];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    formatter = [[NSDateFormatter alloc] init];
    [formatter setTimeZone:[NSTimeZone localTimeZone]];
    [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
    
    unitInfo = (NSMutableDictionary *)[NSKeyedUnarchiver unarchiveObjectWithData:[shareObject objectForKey:@"unit_info"]];
    milestoneInfo = [self.data objectForKey:@"milestone_info"];
    
    no100List = [[NSMutableArray alloc] init];
    for(int i=0; i<=100; i++) [no100List addObject:[NSString stringWithFormat:@"%d", i]];
    
    no300List = [[NSMutableArray alloc] init];
    for(int i=0; i<=300; i++) [no300List addObject:[NSString stringWithFormat:@"%d", i]];

    no200List = [[NSMutableArray alloc] init];
    for(int i=0; i<=1000; i++) [no200List addObject:[NSString stringWithFormat:@"%d", i]];
    
    no24List = [[NSMutableArray alloc] init];
    for(int i=0; i<24; i++) [no24List addObject:[NSString stringWithFormat:@"%d", i]];
    
    no60List = [[NSMutableArray alloc] init];
    for(int i=0; i<60; i++) [no60List addObject:[NSString stringWithFormat:@"%d", i]];
    
    digitList = [[NSMutableArray alloc] init];
    for(int i=0; i<10; i++) [digitList addObject:[NSString stringWithFormat:@"%d", i]];
    
    digit2List = [[NSMutableArray alloc] init];
    for(int i=0; i<100; i++) [digit2List addObject:[NSString stringWithFormat:@"%02d", i]];
    
    babyTeethList = [[NSMutableArray alloc] init];
    for(int i=1; i<=20; i++) {
        [babyTeethList addObject:@(0)];
    }
    adultTeethList = [[NSMutableArray alloc] init];
    for(int i=1; i<=32; i++) {
        [adultTeethList addObject:@(0)];
    }
    
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:UIColorFromRGB(0x8D8082), NSForegroundColorAttributeName, [UIFont fontWithName:@"SFUIDisplay-Medium" size:18.0f], NSFontAttributeName, nil]];
    [self.navigationController.navigationBar setBarTintColor:UIColorFromRGB(0xEEE8E4)];
    [self.navigationController.navigationBar setTranslucent:NO];
    
    [self.navigationItem setTitle:LocalizedString(@"title_edit_milestone", nil)];
    [self customBackButton];
    
    self.btnSave.title = LocalizedString(@"btn_save", nil);
    
    [self.btnDelete setTitle:LocalizedString(@"btn_delete", nil) forState:UIControlStateNormal];
    self.btnPhoto1.imageView.contentMode = UIViewContentModeScaleAspectFill;
    self.btnPhoto2.imageView.contentMode = UIViewContentModeScaleAspectFill;
    self.btnPhoto3.imageView.contentMode = UIViewContentModeScaleAspectFill;
    
    self.lblDate.text = LocalizedString(@"txt_datetime", nil);
    self.lblHeight.text = LocalizedString(@"txt_height", nil);
    self.lblWeight.text = LocalizedString(@"txt_weight", nil);
    self.lblHeadSize.text = LocalizedString(@"txt_circumference", nil);
    self.lblTeething.text = LocalizedString(@"txt_teething", nil);
    self.lblTeethingBabyUpper.text = LocalizedString(@"txt_teething_upper", nil);
    self.lblTeethingBabyLower.text = LocalizedString(@"txt_teething_lower", nil);
    
    self.lblTeethingBaby1.text = LocalizedString(@"txt_teething_6_10_months", nil);
    self.lblTeethingBaby2.text = LocalizedString(@"txt_teething_8_13_months", nil);
    self.lblTeethingBaby3.text = LocalizedString(@"txt_teething_10_16_months", nil);
    self.lblTeethingBaby4.text = LocalizedString(@"txt_teething_13_19_months", nil);
    self.lblTeethingBaby5.text = LocalizedString(@"txt_teething_16_23_months", nil);
    self.lblTeethingBaby6.text = LocalizedString(@"txt_teething_23_33_months", nil);
    
    self.lblTeethingAdult1.text = LocalizedString(@"txt_teething_6_8_years", nil);
    self.lblTeethingAdult2.text = LocalizedString(@"txt_teething_7_9_years", nil);
    self.lblTeethingAdult3.text = LocalizedString(@"txt_teething_9_12_years", nil);
    self.lblTeethingAdult4.text = LocalizedString(@"txt_teething_11_13_years", nil);
    self.lblTeethingAdult5.text = LocalizedString(@"txt_teething_17_21_years", nil);

    self.lblMovement.text = LocalizedString(@"txt_movement", nil);
    self.lblVerbal.text = LocalizedString(@"txt_verbal", nil);
    self.lblLearning.text = LocalizedString(@"txt_learning", nil);
    self.lblMoods.text = LocalizedString(@"txt_moods", nil);
    self.lblVaccine.text = LocalizedString(@"txt_vaccine", nil);
    self.lblAllergies.text = LocalizedString(@"txt_allergies", nil);
    self.lblSymptom.text = LocalizedString(@"txt_symptom", nil);
    self.lblDiagnosis.text = LocalizedString(@"txt_diagnosis", nil);
    self.lblMedication.text = LocalizedString(@"txt_prescription", nil);
    self.lblClinic.text = LocalizedString(@"txt_clinic", nil);
    self.lblReminderTitle.text = LocalizedString(@"txt_reminder_title", nil);
    self.lblReminder.text = LocalizedString(@"txt_reminder", nil);
    self.lblOthers.text = LocalizedString(@"txt_others", nil);
    self.lblChecklist.text = LocalizedString(@"txt_checklist", nil);
    self.lblPhoto.text = LocalizedString(@"txt_photo", nil);
    self.lblNotes.text = LocalizedString(@"txt_notes", nil);
    
    self.viewTeethingBaby1.layer.cornerRadius = self.viewTeethingBaby1.frame.size.width/2;
    self.viewTeethingBaby2.layer.cornerRadius = self.viewTeethingBaby2.frame.size.width/2;
    self.viewTeethingBaby3.layer.cornerRadius = self.viewTeethingBaby3.frame.size.width/2;
    self.viewTeethingBaby4.layer.cornerRadius = self.viewTeethingBaby4.frame.size.width/2;
    self.viewTeethingBaby5.layer.cornerRadius = self.viewTeethingBaby5.frame.size.width/2;
    self.viewTeethingBaby6.layer.cornerRadius = self.viewTeethingBaby6.frame.size.width/2;
    
    self.viewTeethingAdult1.layer.cornerRadius = self.viewTeethingAdult1.frame.size.width/2;
    self.viewTeethingAdult2.layer.cornerRadius = self.viewTeethingAdult2.frame.size.width/2;
    self.viewTeethingAdult3.layer.cornerRadius = self.viewTeethingAdult3.frame.size.width/2;
    self.viewTeethingAdult4.layer.cornerRadius = self.viewTeethingAdult4.frame.size.width/2;
    self.viewTeethingAdult5.layer.cornerRadius = self.viewTeethingAdult5.frame.size.width/2;

    self.btnTeethingBaby1.tag = 1;
    self.btnTeethingBaby2.tag = 2;
    self.btnTeethingBaby3.tag = 3;
    self.btnTeethingBaby4.tag = 4;
    self.btnTeethingBaby5.tag = 5;
    self.btnTeethingBaby6.tag = 6;
    self.btnTeethingBaby7.tag = 7;
    self.btnTeethingBaby8.tag = 8;
    self.btnTeethingBaby9.tag = 9;
    self.btnTeethingBaby10.tag = 10;
    self.btnTeethingBaby11.tag = 11;
    self.btnTeethingBaby12.tag = 12;
    self.btnTeethingBaby13.tag = 13;
    self.btnTeethingBaby14.tag = 14;
    self.btnTeethingBaby15.tag = 15;
    self.btnTeethingBaby16.tag = 16;
    self.btnTeethingBaby17.tag = 17;
    self.btnTeethingBaby18.tag = 18;
    self.btnTeethingBaby19.tag = 19;
    self.btnTeethingBaby20.tag = 20;
    
    self.btnTeethingBaby1.alpha = 0.3f;
    self.btnTeethingBaby2.alpha = 0.3f;
    self.btnTeethingBaby3.alpha = 0.3f;
    self.btnTeethingBaby4.alpha = 0.3f;
    self.btnTeethingBaby5.alpha = 0.3f;
    self.btnTeethingBaby6.alpha = 0.3f;
    self.btnTeethingBaby7.alpha = 0.3f;
    self.btnTeethingBaby8.alpha = 0.3f;
    self.btnTeethingBaby9.alpha = 0.3f;
    self.btnTeethingBaby10.alpha = 0.3f;
    self.btnTeethingBaby11.alpha = 0.3f;
    self.btnTeethingBaby12.alpha = 0.3f;
    self.btnTeethingBaby13.alpha = 0.3f;
    self.btnTeethingBaby14.alpha = 0.3f;
    self.btnTeethingBaby15.alpha = 0.3f;
    self.btnTeethingBaby16.alpha = 0.3f;
    self.btnTeethingBaby17.alpha = 0.3f;
    self.btnTeethingBaby18.alpha = 0.3f;
    self.btnTeethingBaby19.alpha = 0.3f;
    self.btnTeethingBaby20.alpha = 0.3f;
    
    self.btnTeethingAdult1.alpha = 0.3f;
    self.btnTeethingAdult2.alpha = 0.3f;
    self.btnTeethingAdult3.alpha = 0.3f;
    self.btnTeethingAdult4.alpha = 0.3f;
    self.btnTeethingAdult5.alpha = 0.3f;
    self.btnTeethingAdult6.alpha = 0.3f;
    self.btnTeethingAdult7.alpha = 0.3f;
    self.btnTeethingAdult8.alpha = 0.3f;
    self.btnTeethingAdult9.alpha = 0.3f;
    self.btnTeethingAdult10.alpha = 0.3f;
    self.btnTeethingAdult11.alpha = 0.3f;
    self.btnTeethingAdult12.alpha = 0.3f;
    self.btnTeethingAdult13.alpha = 0.3f;
    self.btnTeethingAdult14.alpha = 0.3f;
    self.btnTeethingAdult15.alpha = 0.3f;
    self.btnTeethingAdult16.alpha = 0.3f;
    self.btnTeethingAdult17.alpha = 0.3f;
    self.btnTeethingAdult18.alpha = 0.3f;
    self.btnTeethingAdult19.alpha = 0.3f;
    self.btnTeethingAdult20.alpha = 0.3f;
    self.btnTeethingAdult21.alpha = 0.3f;
    self.btnTeethingAdult22.alpha = 0.3f;
    self.btnTeethingAdult23.alpha = 0.3f;
    self.btnTeethingAdult24.alpha = 0.3f;
    self.btnTeethingAdult25.alpha = 0.3f;
    self.btnTeethingAdult26.alpha = 0.3f;
    self.btnTeethingAdult27.alpha = 0.3f;
    self.btnTeethingAdult28.alpha = 0.3f;
    self.btnTeethingAdult29.alpha = 0.3f;
    self.btnTeethingAdult30.alpha = 0.3f;
    self.btnTeethingAdult31.alpha = 0.3f;
    self.btnTeethingAdult32.alpha = 0.3f;
    
    // Photo Picking & Display
    self.btnPhoto1.hidden = YES;
    self.btnPhoto2.hidden = YES;
    self.btnPhoto3.hidden = YES;
    self.btnPhoto1.tag = 1;
    self.btnPhoto2.tag = 2;
    self.btnPhoto3.tag = 3;
    [self.btnPhoto1 addTarget:self action:@selector(showPhotoAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.btnPhoto2 addTarget:self action:@selector(showPhotoAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.btnPhoto3 addTarget:self action:@selector(showPhotoAction:) forControlEvents:UIControlEventTouchUpInside];
    
    NSString *photoUrl = [milestoneInfo objectForKey:@"photo_url"];
    NSString *photoUrlOriginal = [milestoneInfo objectForKey:@"photo_url_original"];
    videoUrl = [milestoneInfo objectForKey:@"video_url"];
    if ((photoUrlOriginal != nil)&&(![photoUrlOriginal isEqual:[NSNull null]])&&(photoUrlOriginal.length>0)) {
        self.btnPhoto1.hidden = NO;
        havePhotoUrl = YES;
        [self.btnPhoto1 sd_setImageWithURL:[NSURL URLWithString:photoUrlOriginal] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"dummy.png"]];
    }
    else if((photoUrl != nil)&&(![photoUrl isEqual:[NSNull null]])&&(photoUrl.length>0))
    {
        self.btnPhoto1.hidden = NO;
        [self.btnPhoto1 sd_setImageWithURL:[NSURL URLWithString:photoUrl] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"dummy.png"]];
    }
    else if ((videoUrl != nil)&&(![videoUrl isEqual:[NSNull null]])&&(videoUrl.length>0))
    {
//        UIImage *thImg = [COMMON_HELPER generateThumbnailImageFromVideoWithUrl:[NSURL URLWithString:videoUrl]];
//        [self.btnPhoto1 setImage:thImg forState:UIControlStateNormal];
//        self.btnPhoto1.hidden = NO;
        [COMMON_HELPER generateThumbnailImageFromVideoWithUrl:[NSURL URLWithString:videoUrl] inView:self.btnPhoto1];
        self.btnPhoto1.hidden = NO;
    }
    
    NSString *photoUrl2 = [milestoneInfo objectForKey:@"photo_url2"];
    NSString *photoUrlOriginal2 = [milestoneInfo objectForKey:@"photo_url_original2"];
    videoUrl2 = [milestoneInfo objectForKey:@"video_url2"];
    if ((photoUrlOriginal2 != nil)&&(![photoUrlOriginal2 isEqual:[NSNull null]])&&(photoUrlOriginal2.length>0)) {
        self.btnPhoto2.hidden = NO;
        havePhotoUrl2 = YES;
        [self.btnPhoto2 sd_setImageWithURL:[NSURL URLWithString:photoUrlOriginal2] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"dummy.png"]];
    }
    else if((photoUrl2 != nil)&&(![photoUrl2 isEqual:[NSNull null]])&&(photoUrl2.length>0))
    {
        self.btnPhoto2.hidden = NO;
        [self.btnPhoto2 sd_setImageWithURL:[NSURL URLWithString:photoUrl2] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"dummy.png"]];
    }
    else if ((videoUrl2 != nil)&&(![videoUrl2 isEqual:[NSNull null]])&&(videoUrl2.length>0))
    {
//        UIImage *thImg = [COMMON_HELPER generateThumbnailImageFromVideoWithUrl:[NSURL URLWithString:videoUrl2]];
//        [self.btnPhoto2 setImage:thImg forState:UIControlStateNormal];
//        self.btnPhoto2.hidden = NO;
        [COMMON_HELPER generateThumbnailImageFromVideoWithUrl:[NSURL URLWithString:videoUrl2] inView:self.btnPhoto2];
        self.btnPhoto2.hidden = NO;
    }
    
    NSString *photoUrl3 = [milestoneInfo objectForKey:@"photo_url3"];
    NSString *photoUrlOriginal3 = [milestoneInfo objectForKey:@"photo_url_original3"];
    videoUrl3 = [milestoneInfo objectForKey:@"video_url3"];
    if ((photoUrlOriginal3 != nil)&&(![photoUrlOriginal3 isEqual:[NSNull null]])&&(photoUrlOriginal3.length>0)) {
        self.btnPhoto3.hidden = NO;
        havePhotoUrl3 = YES;
        [self.btnPhoto3 sd_setImageWithURL:[NSURL URLWithString:photoUrlOriginal3] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"dummy.png"]];
    }
    else if((photoUrl3 != nil)&&(![photoUrl3 isEqual:[NSNull null]])&&(photoUrl3.length>0))
    {
        self.btnPhoto3.hidden = NO;
        [self.btnPhoto3 sd_setImageWithURL:[NSURL URLWithString:photoUrl3] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"dummy.png"]];
    }
    else if ((videoUrl3 != nil)&&(![videoUrl3 isEqual:[NSNull null]])&&(videoUrl3.length>0))
    {
//        UIImage *thImg = [COMMON_HELPER generateThumbnailImageFromVideoWithUrl:[NSURL URLWithString:videoUrl3]];
//        [self.btnPhoto3 setImage:thImg forState:UIControlStateNormal];
//        self.btnPhoto3.hidden = NO;
        [COMMON_HELPER generateThumbnailImageFromVideoWithUrl:[NSURL URLWithString:videoUrl3] inView:self.btnPhoto3];
        self.btnPhoto3.hidden = NO;
    }
    
    NSString *notes = [milestoneInfo objectForKey:@"notes"];
    if(![notes isEqual:[NSNull null]]) self.txtNotes.text = notes;

    [self doSetupForKeywordView];
    [self doSetupForTeethingView];
    [self customizeFont];
    [self getTeethingInfo];
    [self changeAction];
    [self getKeywordsList];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:(BOOL)animated];
    
    NSLog(@"self.data=%@", self.data);
    if([[self.data objectForKey:@"deleteCheck"] boolValue])
    {
        if ([[self.data objectForKey:@"imageTag"] integerValue] == 1) {
            self.btnPhoto1.hidden = YES;
            [self.data removeObjectForKey:@"imageTag"];
            videoUrl = nil;
            havePhotoUrl = NO;
        }else if ([[self.data objectForKey:@"imageTag"] integerValue] == 2) {
            self.btnPhoto2.hidden = YES;
            [self.data removeObjectForKey:@"imageTag"];
            videoUrl2 = nil;
            havePhotoUrl2 = NO;
        }else if ([[self.data objectForKey:@"imageTag"] integerValue] == 3) {
            self.btnPhoto3.hidden = YES;
            [self.data removeObjectForKey:@"imageTag"];
            videoUrl3 = nil;
            havePhotoUrl3 = NO;
        }
        
        [self.data removeObjectForKey:@"deleteCheck"];
        
    }
    
    if ([[self.data objectForKey:@"cropCheck"] boolValue]) {
        
        if ([[self.data objectForKey:@"imageTag"] integerValue] == 1) {
            imgUploadCropped = [self.data objectForKey:@"newScaledImage"];
            imgUploadPhoto = self.btnPhoto1.imageView.image;
            self.btnPhoto1.hidden = NO;
            [self.data removeObjectForKey:@"imageTag"];
        }
        else if ([[self.data objectForKey:@"imageTag"] integerValue] == 2) {
            imgUploadCropped2 = [self.data objectForKey:@"newScaledImage"];
            imgUploadPhoto2 = self.btnPhoto2.imageView.image;
            [self.data removeObjectForKey:@"imageTag"];
            self.btnPhoto2.hidden = NO;
        }
        else if ([[self.data objectForKey:@"imageTag"] integerValue] == 3) {
            imgUploadCropped3 = [self.data objectForKey:@"newScaledImage"];
            imgUploadPhoto3 = self.btnPhoto3.imageView.image;
            self.btnPhoto3.hidden = NO;
            [self.data removeObjectForKey:@"imageTag"];
        }
        
        [self.data removeObjectForKey:@"cropCheck"];
        [self.data removeObjectForKey:@"newScaledImage"];
        
    }
}

- (void)customBackButton
{
    if([self.navigationController.viewControllers count] > 1)
    {
        UIImage *imgBack = [UIImage imageNamed:@"nav_btn_back"];
        UIButton *btnBack = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnBack setImage:imgBack forState:UIControlStateNormal];
        btnBack.frame = CGRectMake(0, 0, imgBack.size.width, imgBack.size.height);
        [btnBack addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
        [btnBack setImageEdgeInsets:UIEdgeInsetsMake(0, -16, 0, 16)];
        UIBarButtonItem *backBarItem = [[UIBarButtonItem alloc] initWithCustomView:btnBack];
        self.navigationItem.leftBarButtonItem = backBarItem;
    }
}

- (void)backAction
{
    [COMMON_HELPER clearTmpDirectory];
    UIViewController *viewController = [self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count-2];
    viewController.data = [[NSMutableDictionary alloc] initWithDictionary:@{@"save_check":@(saveCheck)}];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)changeAction
{
    if([[milestoneInfo objectForKey:@"name"] isEqualToString:@"grow"])
    {
        float value = [[milestoneInfo objectForKey:@"value"] floatValue];
        NSString *unit = [milestoneInfo objectForKey:@"unit"];
        
        if([[milestoneInfo objectForKey:@"type"] isEqualToString:@"height"])
        {
            self.txtHeight.text = [self convertUnit:[NSString stringWithFormat:@"%f %@", value, unit] unit:[unitInfo objectForKey:@"height"] spacing:YES];
            selectedHeight = [self convertUnit:value unit:unit newUnit:[unitInfo objectForKey:@"height"]];
            NSLog(@"selectedHeight=%f", selectedHeight);
            selectedHeightIndexes = [[NSString stringWithFormat:@"%.1f", selectedHeight] componentsSeparatedByString:@"."];
        }
        else if([[milestoneInfo objectForKey:@"type"] isEqualToString:@"weight"])
        {
            self.txtWeight.text = [self convertUnit:[NSString stringWithFormat:@"%f %@", value, unit] unit:[unitInfo objectForKey:@"weight"] spacing:YES];
            selectedWeight = [self convertUnit:value unit:unit newUnit:[unitInfo objectForKey:@"weight"]];
            NSLog(@"selectedWeight=%f", selectedWeight);
            selectedWeightIndexes = [[NSString stringWithFormat:@"%.1f", selectedWeight] componentsSeparatedByString:@"."];
        }
        else if([[milestoneInfo objectForKey:@"type"] isEqualToString:@"head_size"])
        {
            self.txtHeadSize.text = [self convertUnit:[NSString stringWithFormat:@"%f %@", value, unit] unit:[unitInfo objectForKey:@"head_size"] spacing:YES];
            selectedHeadSize = [self convertUnit:value unit:unit newUnit:[unitInfo objectForKey:@"head_size"]];
            NSLog(@"selectedHeadSize=%f", selectedHeadSize);
            selectedHeadSizeIndexes = [[NSString stringWithFormat:@"%.1f", selectedHeadSize] componentsSeparatedByString:@"."];
        }
        else
        {
            self.txtTeething.text = [milestoneInfo objectForKey:@"tooth_name"];
        }
    }
    else if([[milestoneInfo objectForKey:@"name"] isEqualToString:@"move"])
    {
        if([[milestoneInfo objectForKey:@"type"] isEqualToString:@"movement"])
        {
            self.txtMovement.text = [milestoneInfo objectForKey:@"content"];
        }
    }
    else if([[milestoneInfo objectForKey:@"name"] isEqualToString:@"iq"])
    {
        if([[milestoneInfo objectForKey:@"type"] isEqualToString:@"verbal"])
        {
            self.txtVerbal.text = [milestoneInfo objectForKey:@"content"];
        }
        else if([[milestoneInfo objectForKey:@"type"] isEqualToString:@"learning"])
        {
            self.txtLearning.text = [milestoneInfo objectForKey:@"content"];
        }
        else if([[milestoneInfo objectForKey:@"type"] isEqualToString:@"moods"])
        {
            self.txtMoods.text = [milestoneInfo objectForKey:@"content"];
        }
    }
    else if([[milestoneInfo objectForKey:@"name"] isEqualToString:@"health"])
    {
        if([[milestoneInfo objectForKey:@"type"] isEqualToString:@"vaccine"])
        {
            self.txtVaccine.text = [milestoneInfo objectForKey:@"content"];
        }
        if([[milestoneInfo objectForKey:@"type"] isEqualToString:@"allergies"])
        {
            self.txtAllergies.text = [milestoneInfo objectForKey:@"content"];
        }
        else if([[milestoneInfo objectForKey:@"type"] isEqualToString:@"symptom"])
        {
            self.txtSymptom.text = [milestoneInfo objectForKey:@"content"];
        }
        else if([[milestoneInfo objectForKey:@"type"] isEqualToString:@"diagnosis"])
        {
            self.txtDiagnosis.text = [milestoneInfo objectForKey:@"content"];
        }
        else if([[milestoneInfo objectForKey:@"type"] isEqualToString:@"medication"])
        {
            self.txtMedication.text = [milestoneInfo objectForKey:@"content"];
        }
        else if([[milestoneInfo objectForKey:@"type"] isEqualToString:@"clinic"])
        {
            self.txtClinic.text = [milestoneInfo objectForKey:@"content"];
        }
        else if([[milestoneInfo objectForKey:@"type"] isEqualToString:@"reminder"])
        {
            NSLog(@"hey=%@", milestoneInfo);
            
            formatter.dateFormat = @"yyyy-MM-dd HH:mm:ss ZZZ";
            selectedReminderDateTime = [formatter dateFromString:[milestoneInfo objectForKey:@"reminder_time"]];
            NSLog(@"selectedReminderDateTime=%@", selectedReminderDateTime);
            
            selectedReminderDate = selectedReminderDateTime;
            selectedReminderTime = selectedReminderDateTime;
            
            if([[shareObject objectForKey:@"language"] hasPrefix:@"zh"])
            {
                [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"zh_HK"]];
                [formatter setDateStyle:NSDateFormatterMediumStyle];
                [formatter setTimeStyle:NSDateFormatterShortStyle];
            }
            else
            {
                [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
                formatter.dateFormat = @"EE d MMM, yyyy hh:mm a";
            }
            
            self.txtReminder.text = [NSString stringWithFormat:@"%@", [formatter stringFromDate:selectedReminderDateTime]];
            
            self.txtReminderTitle.text = [milestoneInfo objectForKey:@"content"];
        }
    }
    else if([[milestoneInfo objectForKey:@"name"] isEqualToString:@"others"])
    {
        if([[milestoneInfo objectForKey:@"type"] isEqualToString:@"others"])
        {
            self.txtOthers.text = [milestoneInfo objectForKey:@"content"];
        }
    }
    
    [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
    formatter.dateFormat = @"yyyy-MM-dd hh:mm a";
    selectedMilestoneDateTime = [formatter dateFromString:[NSString stringWithFormat:@"%@ %@", [milestoneInfo objectForKey:@"date"], [milestoneInfo objectForKey:@"time"]]];
    
    if([[shareObject objectForKey:@"language"] hasPrefix:@"zh"])
    {
        [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"zh_HK"]];
        [formatter setDateStyle:NSDateFormatterMediumStyle];
        [formatter setTimeStyle:NSDateFormatterShortStyle];
    }
    else
    {
        [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
        formatter.dateFormat = @"EE d MMM, yyyy hh:mm a";
    }
    self.txtDate.text = [NSString stringWithFormat:@"%@", [formatter stringFromDate:selectedMilestoneDateTime]];
    
    NSLog(@"selectedDateTime=%@", selectedMilestoneDateTime);
}

- (void)getTeethingInfo
{
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setValue:@([shareObject integerForKey:@"user_id"]) forKey:@"user_id"];
    [parameters setValue:@([shareObject integerForKey:@"baby_id"]) forKey:@"baby_id"];
    [parameters setValue:@"teething" forKey:@"type"];
    
    [[WebServiceManager sharedManager] initWithBaseURL:@"api/getGrowthChartList" parameters:parameters success:^(id result) {
        if ([[result objectForKey:@"baby_chart_list"] count]>0) {
            [self setupBabyTeeth:[result objectForKey:@"baby_chart_list"]];
        }
        if ([[result objectForKey:@"adult_chart_list"] count]>0) {
            [self setupAdultTeeth:[result objectForKey:@"adult_chart_list"]];
        }
    } failure:^(NSError *error) {
    }];
}

- (void)selectMilestoneTime:(NSString *)milestoneDate
{
    [ActionSheetDatePicker showPickerWithTitle:@"" datePickerMode:UIDatePickerModeTime selectedDate:selectedMilestoneDate minimumDate:nil maximumDate:nil doneBlock:^(ActionSheetDatePicker *picker, id selectedDate, id origin) {
        
        selectedMilestoneTime = selectedDate;
        NSLog(@"selectedMilestoneTime=%@", selectedMilestoneTime);
        
        formatter.dateFormat = @"hh:mm a";
        NSString *milestoneTime = [formatter stringFromDate:selectedDate];
        
        formatter.dateFormat = @"d MMM yyyy hh:mm a";
        selectedMilestoneDateTime = [formatter dateFromString:[NSString stringWithFormat:@"%@ %@", milestoneDate, milestoneTime]];
        NSLog(@"selectedMilestoneDateTime=%@", selectedMilestoneDateTime);
        
        if([[shareObject objectForKey:@"language"] hasPrefix:@"zh"])
        {
            [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"zh_HK"]];
            [formatter setDateStyle:NSDateFormatterMediumStyle];
            [formatter setTimeStyle:NSDateFormatterShortStyle];
        }
        else
        {
            [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
            formatter.dateFormat = @"EE d MMM, yyyy hh:mm a";
        }
        self.txtDate.text = [NSString stringWithFormat:@"%@", [formatter stringFromDate:selectedMilestoneDateTime]];
        //self.txtDate.text = [NSString stringWithFormat:@"%@ %@", milestoneDate, milestoneTime];
        
        [self.tableView reloadData];
        
    } cancelBlock:^(ActionSheetDatePicker *picker) {
        
    } origin:self.view];
}

- (void)selectReminderTime:(NSString *)reminderDate
{
    [ActionSheetDatePicker showPickerWithTitle:@"" datePickerMode:UIDatePickerModeTime selectedDate:selectedReminderTime minimumDate:nil maximumDate:nil doneBlock:^(ActionSheetDatePicker *picker, id selectedDate, id origin) {
        
        selectedReminderTime = selectedDate;
        NSLog(@"selectedReminderTime=%@", selectedReminderTime);
        
        formatter.dateFormat = @"hh:mm a";
        NSString *reminderTime = [formatter stringFromDate:selectedDate];
        formatter.dateFormat = @"d MMM yyyy hh:mm a";
        selectedReminderDateTime = [formatter dateFromString:[NSString stringWithFormat:@"%@ %@", reminderDate, reminderTime]];
        NSLog(@"selectedReminderDateTime=%@", selectedReminderDateTime);
        
        if([[shareObject objectForKey:@"language"] hasPrefix:@"zh"])
        {
            [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"zh_HK"]];
            [formatter setDateStyle:NSDateFormatterMediumStyle];
            [formatter setTimeStyle:NSDateFormatterShortStyle];
        }
        else
        {
            [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
            formatter.dateFormat = @"d MMM, yyyy hh:mm a";
        }
        self.txtReminder.text = [NSString stringWithFormat:@"%@", [formatter stringFromDate:selectedReminderDateTime]];
        //self.txtReminder.text = [NSString stringWithFormat:@"%@ %@", reminderDate, reminderTime];
        
        [self.tableView reloadData];
        
    } cancelBlock:^(ActionSheetDatePicker *picker) {
        
    } origin:self.view];
}

- (void)showCameraAction
{
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    if ([[NSUserDefaults standardUserDefaults]boolForKey:@"is_Subscription_Enabled"]) {
        imagePicker.mediaTypes = [[NSArray alloc] initWithObjects: (NSString *) kUTTypeMovie, (NSString *) kUTTypeVideo, (NSString *) kUTTypeImage, nil];
        imagePicker.videoQuality = UIImagePickerControllerQualityTypeLow;
        imagePicker.videoMaximumDuration = 120;
    }else {
        imagePicker.mediaTypes = [[NSArray alloc] initWithObjects: (NSString *) kUTTypeImage, nil];
    }
    [self presentViewController:imagePicker animated:NO completion:nil];
}

- (void)showPhotoAlbumAction
{
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    if ([[NSUserDefaults standardUserDefaults]boolForKey:@"is_Subscription_Enabled"]) {
        imagePicker.mediaTypes = [[NSArray alloc] initWithObjects: (NSString *) kUTTypeMovie, (NSString *) kUTTypeVideo, (NSString *) kUTTypeImage, nil];
    }else {
        imagePicker.mediaTypes = [[NSArray alloc] initWithObjects: (NSString *) kUTTypeImage, nil];
    }
    [self presentViewController:imagePicker animated:NO completion:nil];
}

- (void)showPhotoAction:(UIButton *)sender
{
    
    if (sender.hidden)
        return;
    
    UIImage *img;
    NSString *vStr;
    if (sender.tag == 1) {
        if (videoUrl && videoUrl.length>0) {
            vStr = videoUrl;
        }else {
            img = self.btnPhoto1.imageView.image;
        }
    }else if (sender.tag == 2) {
        if (videoUrl2 && videoUrl2.length>0) {
            vStr = videoUrl2;
        }else {
            img = self.btnPhoto2.imageView.image;
        }
    }else if (sender.tag == 3) {
        if (videoUrl3 && videoUrl3.length>0) {
            vStr = videoUrl3;
        }else {
            img = self.btnPhoto3.imageView.image;
        }
    }
    
    if (vStr && vStr.length>0) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"VideoViewController"];
        viewController.data = [[NSMutableDictionary alloc] initWithDictionary:@{@"viewController":self, @"video":vStr,@"imageTag":@(sender.tag)}];
        [self presentViewController:viewController animated:YES completion:nil];
    }
    else
    {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"PhotoViewController"];
        viewController.data = [[NSMutableDictionary alloc] initWithDictionary:@{@"viewController":self, @"image":img,@"imageTag":@(sender.tag)}];
        [self presentViewController:viewController animated:YES completion:nil];
    }

//    UIImage *img;
//    if (sender.tag == 1) {
//        img = self.btnPhoto1.imageView.image;
//    }else if (sender.tag == 2) {
//        img = self.btnPhoto2.imageView.image;
//    }else if (sender.tag == 3) {
//        img = self.btnPhoto3.imageView.image;
//    }
//
//    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"PhotoViewController"];
//    viewController.data = [[NSMutableDictionary alloc] initWithDictionary:@{@"viewController":self, @"image":img,@"imageTag":@(sender.tag),@"show_crop":@(YES)}];
//    [self presentViewController:viewController animated:YES completion:nil];
}

- (void)takePhotoAction
{
    AVAuthorizationStatus status = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    if(status == AVAuthorizationStatusDenied || status == AVAuthorizationStatusRestricted)
    {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:LocalizedString(@"error_camera", nil) message:@"" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *btnOk = [UIAlertAction actionWithTitle:LocalizedString(@"btn_ok", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            
            [alert dismissViewControllerAnimated:YES completion:nil];
        }];
        
        [alert addAction:btnOk];
        
        [self presentViewController:alert animated:YES completion:nil];
        
    }
    else
    {
        [self showCameraAction];
    }
}

- (void)getPhotoAlbumAction
{
    PHAuthorizationStatus status = [PHPhotoLibrary authorizationStatus];
    if(status == PHAuthorizationStatusDenied || status == PHAuthorizationStatusRestricted)
    {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:LocalizedString(@"error_photo", nil) message:@"" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *btnOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            
            [alert dismissViewControllerAnimated:YES completion:nil];
        }];
        
        [alert addAction:btnOk];
        
        [self presentViewController:alert animated:YES completion:nil];
        
    }
    else
    {
        [self showPhotoAlbumAction];
    }
}

- (IBAction)selectBabyTeethAction:(UIButton *)btn
{
    NSLog(@"%d",btn.tag);
}

- (IBAction)selectAdultTeethAction:(UIButton *)btn
{
    NSLog(@"%d",btn.tag);
}

- (IBAction)deleteAction:(id)sender
{
    [self showConfirmAlert:LocalizedString(@"alert_are_you_sure", nil) message:@"" callback:^(UIAlertAction *action) {
        
        [SVProgressHUD show];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setValue:@([[milestoneInfo objectForKey:@"id"] intValue]) forKey:@"id"];
        [parameters setValue:@([shareObject integerForKey:@"user_id"]) forKey:@"user_id"];
        [parameters setValue:@([shareObject integerForKey:@"baby_id"]) forKey:@"baby_id"];
        [parameters setValue:[milestoneInfo objectForKey:@"photo_url"] forKey:@"photo_url"];
        
        [[WebServiceManager sharedManager] initWithBaseURL:@"api/deleteBabyMilestone" parameters:parameters success:^(id result) {
            
            if([[result objectForKey:@"success"] boolValue])
            {
                NSMutableArray *babyList = (NSMutableArray *)[NSKeyedUnarchiver unarchiveObjectWithData:[shareObject objectForKey:@"baby_list"]];
                [babyList replaceObjectAtIndex:[shareObject integerForKey:@"baby_index"] withObject:[result valueForKey:@"info"]];
                [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:babyList] forKey:@"baby_list"];
                [shareObject synchronize];
                
                saveCheck = YES;
                appDelegate.updateMilestonesCheck = YES;
                [self backAction];
            }
            else
            {
                [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:[result objectForKey:@"message"] input:nil];
            }
            
        } failure:^(NSError *error) {
            [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:LocalizedString(@"error_unknown", nil) input:nil];
        }];
    }];
}

- (IBAction)nextAction:(id)sender
{
    [SVProgressHUD show];
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setValue:@([[milestoneInfo objectForKey:@"id"] intValue]) forKey:@"id"];
    [parameters setValue:@([shareObject integerForKey:@"user_id"]) forKey:@"user_id"];
    [parameters setValue:@([shareObject integerForKey:@"baby_id"]) forKey:@"baby_id"];
    [parameters setValue:[milestoneInfo objectForKey:@"name"] forKey:@"name"];
    [parameters setValue:[milestoneInfo objectForKey:@"type"] forKey:@"type"];
    
    if([[milestoneInfo objectForKey:@"type"] isEqualToString:@"weight"])
    {
        //[parameters setValue:[NSString stringWithFormat:@"%f", [self convertUnit:selectedWeight unit:[unitInfo objectForKey:@"weight"] newUnit:@"kg"]] forKey:@"weight"];
        [parameters setValue:[NSString stringWithFormat:@"%f", [self convertUnit:selectedWeight unit:[unitInfo objectForKey:@"weight"] newUnit:[unitInfo objectForKey:@"weight"]]] forKey:@"weight"];
    }
    else if([[milestoneInfo objectForKey:@"type"] isEqualToString:@"height"])
    {
        //[parameters setValue:[NSString stringWithFormat:@"%f", [self convertUnit:selectedHeight unit:[unitInfo objectForKey:@"height"] newUnit:@"cm"]] forKey:@"height"];
        [parameters setValue:[NSString stringWithFormat:@"%f", [self convertUnit:selectedHeight unit:[unitInfo objectForKey:@"height"] newUnit:[unitInfo objectForKey:@"height"]]] forKey:@"height"];
    }
    else if([[milestoneInfo objectForKey:@"type"] isEqualToString:@"head_size"])
    {
        //[parameters setValue:[NSString stringWithFormat:@"%f", [self convertUnit:selectedHeadSize unit:[unitInfo objectForKey:@"head_size"] newUnit:@"cm"]] forKey:@"head_size"];
        [parameters setValue:[NSString stringWithFormat:@"%f", [self convertUnit:selectedHeadSize unit:[unitInfo objectForKey:@"head_size"] newUnit:[unitInfo objectForKey:@"head_size"]]] forKey:@"head_size"];
    }
    else if([[milestoneInfo objectForKey:@"type"] isEqualToString:@"teething"])
    {
        NSLog(@"babyTeethList=%@", babyTeethList);
        int validCount = 0;
        for(int i=0; i<babyTeethList.count; i++)
        {
            if([[babyTeethList objectAtIndex:i] intValue] == 1)
            {
                [parameters setValue:[babyTeethList componentsJoinedByString:@","] forKey:@"baby_teeth"];
                validCount++;
                break;
            }
        }
        
        if(validCount == 0)
        {
            [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:LocalizedString(@"error_no_milestone", nil) input:nil];
            [SVProgressHUD dismiss];
            return;
        }
    }
    else if([[milestoneInfo objectForKey:@"type"] isEqualToString:@"adult_teething"])
    {
        NSLog(@"adultTeethList=%@", adultTeethList);
        int validCount = 0;
        for(int i=0; i<adultTeethList.count; i++)
        {
            if([[adultTeethList objectAtIndex:i] intValue] == 1)
            {
                [parameters setValue:[adultTeethList componentsJoinedByString:@","] forKey:@"adult_teeth"];
                validCount++;
                break;
            }
        }
        
        if(validCount == 0)
        {
            [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:LocalizedString(@"error_no_milestone", nil) input:nil];
            [SVProgressHUD dismiss];
            return;
        }
    }
    else if([[milestoneInfo objectForKey:@"type"] isEqualToString:@"movement"])
    {
        if(self.txtMovement.text.length == 0)
        {
            [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:LocalizedString(@"error_no_milestone", nil) input:nil];
            [SVProgressHUD dismiss];
            return;
        }
        [parameters setValue:self.txtMovement.text forKey:@"movement"];
    }
    else if([[milestoneInfo objectForKey:@"type"] isEqualToString:@"verbal"])
    {
        if(self.txtVerbal.text.length == 0)
        {
            [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:LocalizedString(@"error_no_milestone", nil) input:nil];
            [SVProgressHUD dismiss];
            return;
        }
        [parameters setValue:self.txtVerbal.text forKey:@"verbal"];
    }
    else if([[milestoneInfo objectForKey:@"type"] isEqualToString:@"learning"])
    {
        if(self.txtLearning.text.length == 0)
        {
            [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:LocalizedString(@"error_no_milestone", nil) input:nil];
            [SVProgressHUD dismiss];
            return;
        }
        [parameters setValue:self.txtLearning.text forKey:@"learning"];
    }
    else if([[milestoneInfo objectForKey:@"type"] isEqualToString:@"moods"])
    {
        if(self.txtMoods.text.length == 0)
        {
            [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:LocalizedString(@"error_no_milestone", nil) input:nil];
            [SVProgressHUD dismiss];
            return;
        }
        [parameters setValue:self.txtMoods.text forKey:@"moods"];
    }
    else if([[milestoneInfo objectForKey:@"type"] isEqualToString:@"vaccine"])
    {
        if(self.txtVaccine.text.length == 0)
        {
            [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:LocalizedString(@"error_no_milestone", nil) input:nil];
            [SVProgressHUD dismiss];
            return;
        }
        [parameters setValue:self.txtVaccine.text forKey:@"vaccine"];
    }
    else if([[milestoneInfo objectForKey:@"type"] isEqualToString:@"allergies"])
    {
        if(self.txtAllergies.text.length == 0)
        {
            [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:LocalizedString(@"error_no_milestone", nil) input:nil];
            [SVProgressHUD dismiss];
            return;
        }
        [parameters setValue:self.txtAllergies.text forKey:@"allergies"];
    }
    else if([[milestoneInfo objectForKey:@"type"] isEqualToString:@"symptom"])
    {
        if(self.txtSymptom.text.length == 0)
        {
            [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:LocalizedString(@"error_no_milestone", nil) input:nil];
            [SVProgressHUD dismiss];
            return;
        }
        [parameters setValue:self.txtSymptom.text forKey:@"symptom"];
    }
    else if([[milestoneInfo objectForKey:@"type"] isEqualToString:@"diagnosis"])
    {
        if(self.txtDiagnosis.text.length == 0)
        {
            [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:LocalizedString(@"error_no_milestone", nil) input:nil];
            [SVProgressHUD dismiss];
            return;
        }
        [parameters setValue:self.txtDiagnosis.text forKey:@"diagnosis"];
    }
    else if([[milestoneInfo objectForKey:@"type"] isEqualToString:@"medication"])
    {
        if(self.txtMedication.text.length == 0)
        {
            [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:LocalizedString(@"error_no_milestone", nil) input:nil];
            [SVProgressHUD dismiss];
            return;
        }
        [parameters setValue:self.txtMedication.text forKey:@"medication"];
    }
    else if([[milestoneInfo objectForKey:@"type"] isEqualToString:@"clinic"])
    {
        if(self.txtClinic.text.length == 0)
        {
            [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:LocalizedString(@"error_no_milestone", nil) input:nil];
            [SVProgressHUD dismiss];
            return;
        }
        [parameters setValue:self.txtClinic.text forKey:@"clinic"];
    }
    else if([[milestoneInfo objectForKey:@"type"] isEqualToString:@"reminder"])
    {
        if(self.txtReminderTitle.text.length == 0)
        {
            [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:LocalizedString(@"error_no_milestone_reminder_title", nil) input:nil];
            [SVProgressHUD dismiss];
            return;
        }
        [parameters setValue:self.txtReminderTitle.text forKey:@"reminder_title"];
        [parameters setValue:selectedReminderDateTime forKey:@"reminder"];
    }
    else if([[milestoneInfo objectForKey:@"type"] isEqualToString:@"others"])
    {
        if(self.txtOthers.text.length == 0)
        {
            [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:LocalizedString(@"error_no_milestone", nil) input:nil];
            [SVProgressHUD dismiss];
            return;
        }
        [parameters setValue:self.txtOthers.text forKey:@"others"];
    }
    [parameters setValue:self.txtNotes.text forKey:@"notes"];
    [parameters setValue:[[NSString stringWithFormat:@"%@", selectedMilestoneDateTime] substringWithRange:NSMakeRange(0, 19)] forKey:@"time"];
    
//    if (self.btnPhoto1.hidden) {
//        NSString *vUrlStr = [milestoneInfo objectForKey:@"video_url"];
//        if (vUrlStr && vUrlStr.length>0) {
//            [parameters setValue:[milestoneInfo objectForKey:@"video_url"] forKey:@"video_url"];
//        }else {
//            [parameters setValue:[milestoneInfo objectForKey:@"photo_url"] forKey:@"photo_url"];
//            [parameters setValue:[milestoneInfo objectForKey:@"photo_url_original"] forKey:@"photo_url_original"];
//        }
//    }
//    if (self.btnPhoto2.hidden) {
//        NSString *vUrlStr = [milestoneInfo objectForKey:@"video_url2"];
//        if (vUrlStr && vUrlStr.length>0) {
//            [parameters setValue:[milestoneInfo objectForKey:@"video_url2"] forKey:@"video_url2"];
//        }else {
//            [parameters setValue:[milestoneInfo objectForKey:@"photo_url2"] forKey:@"photo_url2"];
//            [parameters setValue:[milestoneInfo objectForKey:@"photo_url_original2"] forKey:@"photo_url_original2"];
//        }
//    }
//    if (self.btnPhoto3.hidden) {
//        NSString *vUrlStr = [milestoneInfo objectForKey:@"video_url3"];
//        if (vUrlStr && vUrlStr.length>0) {
//            [parameters setValue:[milestoneInfo objectForKey:@"video_url3"] forKey:@"video_url3"];
//        }else {
//            [parameters setValue:[milestoneInfo objectForKey:@"photo_url3"] forKey:@"photo_url3"];
//            [parameters setValue:[milestoneInfo objectForKey:@"photo_url_original3"] forKey:@"photo_url_original3"];
//        }
//    }

    // To delete existing media file fro server
    NSString *vUrlStr = [milestoneInfo objectForKey:@"video_url"];
    if ((!videoUrl || videoUrl.length==0) && vUrlStr.length>0) {
        [parameters setValue:[milestoneInfo objectForKey:@"video_url"] forKey:@"video_url"];
    }
    NSString *vUrlStr2 = [milestoneInfo objectForKey:@"video_url2"];
    if ((!videoUrl2 || videoUrl2.length==0) && vUrlStr2.length>0) {
        [parameters setValue:[milestoneInfo objectForKey:@"video_url2"] forKey:@"video_url2"];
    }
    NSString *vUrlStr3 = [milestoneInfo objectForKey:@"video_url3"];
    if ((!videoUrl3 || videoUrl3.length==0) && vUrlStr3.length>0) {
        [parameters setValue:[milestoneInfo objectForKey:@"video_url3"] forKey:@"video_url3"];
    }
    
    NSString *pUrlStr = [milestoneInfo objectForKey:@"photo_url"];
    if (!havePhotoUrl && pUrlStr.length>0) {
        [parameters setValue:[milestoneInfo objectForKey:@"photo_url"] forKey:@"photo_url"];
        [parameters setValue:[milestoneInfo objectForKey:@"photo_url_original"] forKey:@"photo_url_original"];
    }
    NSString *pUrlStr2 = [milestoneInfo objectForKey:@"photo_url2"];
    if (!havePhotoUrl2 && pUrlStr2.length>0) {
        [parameters setValue:[milestoneInfo objectForKey:@"photo_url2"] forKey:@"photo_url2"];
        [parameters setValue:[milestoneInfo objectForKey:@"photo_url_original2"] forKey:@"photo_url_original2"];
    }
    NSString *pUrlStr3 = [milestoneInfo objectForKey:@"photo_url3"];
    if (!havePhotoUrl3 && pUrlStr3.length>0) {
        [parameters setValue:[milestoneInfo objectForKey:@"photo_url3"] forKey:@"photo_url3"];
        [parameters setValue:[milestoneInfo objectForKey:@"photo_url_original3"] forKey:@"photo_url_original3"];
    }
    
    if(!self.btnPhoto1.hidden || !self.btnPhoto2.hidden || !self.btnPhoto3.hidden)
    {
        [[WebServiceManager sharedManager] initWithBaseURL:@"api/editBabyMilestone" parameters:parameters formData:^(id<AFMultipartFormData> formData) {
            
            if (!self.btnPhoto1.hidden) {
                if (videoUrl && videoUrl.length>0) {
                    [formData appendPartWithFileURL:[NSURL URLWithString:videoUrl] name:@"video" fileName:@"video.mp4" mimeType:@"video" error: nil];
                }else if (imgUploadPhoto){
                    [formData appendPartWithFileData:UIImageJPEGRepresentation(imgUploadCropped, 0.8f) name:@"image" fileName:@"image.jpg" mimeType:@"image/jpeg"];
                    [formData appendPartWithFileData:UIImageJPEGRepresentation(imgUploadPhoto, 0.8f) name:@"image_original" fileName:@"image_original.jpg" mimeType:@"image/jpeg"];
                }
            }
            
            if (!self.btnPhoto2.hidden) {
                if (videoUrl2 && videoUrl2.length>0) {
                    [formData appendPartWithFileURL:[NSURL URLWithString:videoUrl2] name:@"video2" fileName:@"video2.mp4" mimeType:@"video" error: nil];
                }else if (imgUploadPhoto2){
                    [formData appendPartWithFileData:UIImageJPEGRepresentation(imgUploadCropped2, 0.8f) name:@"image2" fileName:@"image2.jpg" mimeType:@"image/jpeg"];
                    [formData appendPartWithFileData:UIImageJPEGRepresentation(imgUploadPhoto2, 0.8f) name:@"image_original2" fileName:@"image_original2.jpg" mimeType:@"image/jpeg"];
                }
            }
            
            if (!self.btnPhoto3.hidden) {
                if (videoUrl3 && videoUrl3.length>0) {
                    [formData appendPartWithFileURL:[NSURL URLWithString:videoUrl3] name:@"video3" fileName:@"video3.mp4" mimeType:@"video" error: nil];
                }else if (imgUploadPhoto3){
                    [formData appendPartWithFileData:UIImageJPEGRepresentation(imgUploadCropped3, 0.8f) name:@"image3" fileName:@"image3.jpg" mimeType:@"image/jpeg"];
                    [formData appendPartWithFileData:UIImageJPEGRepresentation(imgUploadPhoto3, 0.8f) name:@"image_original3" fileName:@"image_original3.jpg" mimeType:@"image/jpeg"];
                }
            }
            
//            if (imgUploadPhoto) {
//            [formData appendPartWithFileData:UIImageJPEGRepresentation(imgUploadCropped, 0.8f) name:@"image" fileName:@"image.jpg" mimeType:@"image/jpeg"];
//            [formData appendPartWithFileData:UIImageJPEGRepresentation(imgUploadPhoto, 0.8f) name:@"image_original" fileName:@"image_original.jpg" mimeType:@"image/jpeg"];
//            }
//            if (imgUploadPhoto2) {
//                [formData appendPartWithFileData:UIImageJPEGRepresentation(imgUploadCropped2, 0.8f) name:@"image2" fileName:@"image2.jpg" mimeType:@"image/jpeg"];
//                [formData appendPartWithFileData:UIImageJPEGRepresentation(imgUploadPhoto2, 0.8f) name:@"image_original2" fileName:@"image_original2.jpg" mimeType:@"image/jpeg"];
//            }
//            if (imgUploadPhoto3) {
//                [formData appendPartWithFileData:UIImageJPEGRepresentation(imgUploadCropped3, 0.8f) name:@"image3" fileName:@"image3.jpg" mimeType:@"image/jpeg"];
//                [formData appendPartWithFileData:UIImageJPEGRepresentation(imgUploadPhoto3, 0.8f) name:@"image_original3" fileName:@"image_original3.jpg" mimeType:@"image/jpeg"];
//            }
            
            NSLog(@"formData=%@", formData);
            
        } success:^(id result) {
            
            NSLog(@"result=%@", result);
            if([[result objectForKey:@"success"] boolValue])
            {
                if([[milestoneInfo objectForKey:@"name"] isEqualToString:@"grow"])
                {
                    NSMutableArray *babyList = (NSMutableArray *)[NSKeyedUnarchiver unarchiveObjectWithData:[shareObject objectForKey:@"baby_list"]];
                    [babyList replaceObjectAtIndex:[shareObject integerForKey:@"baby_index"] withObject:[result valueForKey:@"info"]];
                    [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:babyList] forKey:@"baby_list"];
                    [shareObject synchronize];
                }
                
                if([[milestoneInfo objectForKey:@"type"] isEqualToString:@"reminder"])
                {
                    for(UILocalNotification *notification in [[UIApplication sharedApplication] scheduledLocalNotifications])
                    {
                        NSLog(@"id=%@, %@", [notification.userInfo objectForKey:@"id"], [result objectForKey:@"reference_id"]);
                        if([[notification.userInfo objectForKey:@"id"] intValue] == [[result objectForKey:@"reference_id"] intValue])
                        {
                            [[UIApplication sharedApplication] cancelLocalNotification:notification];
                            break;
                        }
                    }

                    [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
                    formatter.dateFormat = @"yyyy";
                    int year = [[formatter stringFromDate:selectedReminderDateTime] intValue];
                    formatter.dateFormat = @"MM";
                    int month = [[formatter stringFromDate:selectedReminderDateTime] intValue];
                    formatter.dateFormat = @"dd";
                    int day = [[formatter stringFromDate:selectedReminderDateTime] intValue];
                    formatter.dateFormat = @"HH";
                    int hour = [[formatter stringFromDate:selectedReminderDateTime] intValue];
                    formatter.dateFormat = @"mm";
                    int minute = [[formatter stringFromDate:selectedReminderDateTime] intValue];
                    NSLog(@"year=%d, month=%d, day=%d, hour=%d, minute=%d", year, month, day, hour, minute);

                    NSCalendar *calendar = [NSCalendar currentCalendar];
                    NSDateComponents *components = [calendar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitWeekOfYear | NSCalendarUnitWeekday fromDate:selectedReminderDateTime];
                    components.year = year;
                    components.month = month;
                    components.day = day;
                    components.hour = hour;
                    components.minute = minute;

                    UILocalNotification *local = [[UILocalNotification alloc] init];
                    local.fireDate = [calendar dateFromComponents:components];
                    NSLog(@"local.fireDate=%@", local.fireDate);
                    local.timeZone = [NSTimeZone defaultTimeZone];
                    local.alertTitle = [NSString stringWithFormat:@"[%@] - %@", [self.data objectForKey:@"name"], LocalizedString(@"txt_reminder", nil)];
                    local.alertBody = self.txtReminderTitle.text;
                    local.soundName = UILocalNotificationDefaultSoundName;
                    local.userInfo = [[NSMutableDictionary alloc] initWithDictionary:@{@"id":[result objectForKey:@"reference_id"], @"title":self.txtReminderTitle.text, @"baby_id":@([shareObject integerForKey:@"baby_id"]), @"baby_name":[self.data objectForKey:@"name"], @"type":@"milestones", @"repeat":@(0)}];
                    [[UIApplication sharedApplication] scheduleLocalNotification:local];
                }
                
                saveCheck = YES;
                appDelegate.updateMilestonesCheck = YES;
                [self backAction];
            }
            else
            {
                
            }
            
        } failure:^(NSError *error) {
            
        }];
    }
    else
    {
        [[WebServiceManager sharedManager] initWithBaseURL:@"api/editBabyMilestone" parameters:parameters success:^(id result) {
             
            NSLog(@"result=%@", result);
            if([[result objectForKey:@"success"] boolValue])
            {
                if([[milestoneInfo objectForKey:@"name"] isEqualToString:@"grow"])
                {
                    NSMutableArray *babyList = (NSMutableArray *)[NSKeyedUnarchiver unarchiveObjectWithData:[shareObject objectForKey:@"baby_list"]];
                    [babyList replaceObjectAtIndex:[shareObject integerForKey:@"baby_index"] withObject:[result valueForKey:@"info"]];
                    [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:babyList] forKey:@"baby_list"];
                    [shareObject synchronize];
                }
                
                if([[milestoneInfo objectForKey:@"type"] isEqualToString:@"reminder"])
                {
                    for(UILocalNotification *notification in [[UIApplication sharedApplication] scheduledLocalNotifications])
                    {
                        NSLog(@"id=%@, %@", [notification.userInfo objectForKey:@"id"], [result objectForKey:@"reference_id"]);
                        if([[notification.userInfo objectForKey:@"id"] intValue] == [[result objectForKey:@"reference_id"] intValue])
                        {
                            [[UIApplication sharedApplication] cancelLocalNotification:notification];
                            break;
                        }
                    }

                    [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
                    formatter.dateFormat = @"yyyy";
                    int year = [[formatter stringFromDate:selectedReminderDateTime] intValue];
                    formatter.dateFormat = @"MM";
                    int month = [[formatter stringFromDate:selectedReminderDateTime] intValue];
                    formatter.dateFormat = @"dd";
                    int day = [[formatter stringFromDate:selectedReminderDateTime] intValue];
                    formatter.dateFormat = @"HH";
                    int hour = [[formatter stringFromDate:selectedReminderDateTime] intValue];
                    formatter.dateFormat = @"mm";
                    int minute = [[formatter stringFromDate:selectedReminderDateTime] intValue];
                    NSLog(@"year=%d, month=%d, day=%d, hour=%d, minute=%d", year, month, day, hour, minute);

                    NSCalendar *calendar = [NSCalendar currentCalendar];
                    NSDateComponents *components = [calendar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitWeekOfYear | NSCalendarUnitWeekday fromDate:selectedReminderDateTime];
                    components.year = year;
                    components.month = month;
                    components.day = day;
                    components.hour = hour;
                    components.minute = minute;

                    UILocalNotification *local = [[UILocalNotification alloc] init];
                    local.fireDate = [calendar dateFromComponents:components];
                    NSLog(@"local.fireDate=%@", local.fireDate);
                    local.timeZone = [NSTimeZone defaultTimeZone];
                    local.alertTitle = [NSString stringWithFormat:@"[%@] - %@", [self.data objectForKey:@"name"], LocalizedString(@"txt_reminder", nil)];
                    local.alertBody = self.txtReminderTitle.text;
                    local.soundName = UILocalNotificationDefaultSoundName;
                    local.userInfo = [[NSMutableDictionary alloc] initWithDictionary:@{@"id":[result objectForKey:@"reference_id"], @"title":self.txtReminderTitle.text, @"baby_id":@([shareObject integerForKey:@"baby_id"]), @"baby_name":[self.data objectForKey:@"name"], @"type":@"milestones", @"repeat":@(0)}];
                    [[UIApplication sharedApplication] scheduleLocalNotification:local];
                }
                
                saveCheck = YES;
                appDelegate.updateMilestonesCheck = YES;
                [self backAction];
            }
            else
            {
                
            }
            
        } failure:^(NSError *error) {
            
        }];
    }
}

#pragma mark - IBActions
-(IBAction)didSelectAddPhoto:(UIButton *)sender
{
    if (self.btnPhoto1.hidden || self.btnPhoto2.hidden || self.btnPhoto3.hidden)
    {
        UIAlertController *alertController=[UIAlertController alertControllerWithTitle:LocalizedString(@"title_selectImage", nil) message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        
        UIAlertAction *takePhoto=[UIAlertAction actionWithTitle:LocalizedString(@"title_camera", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self takePhotoAction];
        }];
        UIAlertAction *choosePhoto=[UIAlertAction actionWithTitle:LocalizedString(@"title_camera_roll", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self getPhotoAlbumAction];
        }];
        UIAlertAction *actionCancel=[UIAlertAction actionWithTitle:LocalizedString(@"title_cancel", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            [alertController dismissViewControllerAnimated:YES completion:nil];
        }];

        [alertController addAction:takePhoto];
        [alertController addAction:choosePhoto];
        [alertController addAction:actionCancel];
        
        if (IS_IPAD) {
            UIPopoverPresentationController *popPresenter = [alertController popoverPresentationController];
            popPresenter.sourceView = sender;
            popPresenter.sourceRect = CGRectMake(sender.bounds.origin.x, sender.bounds.size.height/2, 0, 0);
            //popPresenter.presentedViewController.preferredContentSize =  CGSizeMake(520, 400);
        }
        [self presentViewController:alertController animated:YES completion:nil];
    }
    else
    {
        [self showAlertWithTitle:@"" message:@"You can not add more than 3 photos. " input:nil];
    }
    
}

#pragma mark - Custom Actions
-(void)doSetupForKeywordView
{
    [self.btnaddKeyword addTarget:self action:@selector(didSelectAddKeyword:) forControlEvents:UIControlEventTouchUpInside];

    if (IS_IPAD) {
        self.keyword1.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.keyword2.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.keyword3.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.keyword4.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.keyword5.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.keyword6.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.keyword7.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.keyword8.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.keyword9.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
    }

    // keyword view
    keywordsArray = [[NSMutableArray alloc] init];
    self.keywordView.backgroundColor = [UIColor clearColor];
    self.keyword1.backgroundColor = [UIColor clearColor];
    self.keyword2.backgroundColor = [UIColor clearColor];
    self.keyword3.backgroundColor = [UIColor clearColor];
    self.keyword4.backgroundColor = [UIColor clearColor];
    self.keyword5.backgroundColor = [UIColor clearColor];
    self.keyword6.backgroundColor = [UIColor clearColor];
    self.keyword7.backgroundColor = [UIColor clearColor];
    self.keyword8.backgroundColor = [UIColor clearColor];
    self.keyword9.backgroundColor = [UIColor clearColor];
    
    self.keyword1.text = @"";
    self.keyword2.text = @"";
    self.keyword3.text = @"";
    self.keyword4.text = @"";
    self.keyword5.text = @"";
    self.keyword6.text = @"";
    self.keyword7.text = @"";
    self.keyword8.text = @"";
    self.keyword9.text = @"";
    
    self.keyword1.layer.cornerRadius = 4.0f;
    self.keyword1.layer.masksToBounds = YES;
    self.keyword2.layer.cornerRadius = 4.0f;
    self.keyword2.layer.masksToBounds = YES;
    self.keyword3.layer.cornerRadius = 4.0f;
    self.keyword3.layer.masksToBounds = YES;
    self.keyword4.layer.cornerRadius = 4.0f;
    self.keyword4.layer.masksToBounds = YES;
    self.keyword5.layer.cornerRadius = 4.0f;
    self.keyword5.layer.masksToBounds = YES;
    self.keyword6.layer.cornerRadius = 4.0f;
    self.keyword6.layer.masksToBounds = YES;
    self.keyword7.layer.cornerRadius = 4.0f;
    self.keyword7.layer.masksToBounds = YES;
    self.keyword8.layer.cornerRadius = 4.0f;
    self.keyword8.layer.masksToBounds = YES;
    self.keyword9.layer.cornerRadius = 4.0f;
    self.keyword9.layer.masksToBounds = YES;
    
    self.keyword1.userInteractionEnabled = YES;
    self.keyword2.userInteractionEnabled = YES;
    self.keyword3.userInteractionEnabled = YES;
    self.keyword4.userInteractionEnabled = YES;
    self.keyword5.userInteractionEnabled = YES;
    self.keyword6.userInteractionEnabled = YES;
    self.keyword7.userInteractionEnabled = YES;
    self.keyword8.userInteractionEnabled = YES;
    self.keyword9.userInteractionEnabled = YES;
    
    self.keyword1.tag = 1;
    self.keyword2.tag = 2;
    self.keyword3.tag = 3;
    self.keyword4.tag = 4;
    self.keyword5.tag = 5;
    self.keyword6.tag = 6;
    self.keyword7.tag = 7;
    self.keyword8.tag = 8;
    self.keyword9.tag = 9;
    
    UITapGestureRecognizer *gesture1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)] ;
    UITapGestureRecognizer *gesture2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)] ;
    UITapGestureRecognizer *gesture3 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)] ;
    UITapGestureRecognizer *gesture4 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)] ;
    UITapGestureRecognizer *gesture5 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)] ;
    UITapGestureRecognizer *gesture6 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)] ;
    UITapGestureRecognizer *gesture7 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)] ;
    UITapGestureRecognizer *gesture8 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)] ;
    UITapGestureRecognizer *gesture9 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)] ;
    
    [self.keyword1 addGestureRecognizer:gesture1];
    [self.keyword2 addGestureRecognizer:gesture2];
    [self.keyword3 addGestureRecognizer:gesture3];
    [self.keyword4 addGestureRecognizer:gesture4];
    [self.keyword5 addGestureRecognizer:gesture5];
    [self.keyword6 addGestureRecognizer:gesture6];
    [self.keyword7 addGestureRecognizer:gesture7];
    [self.keyword8 addGestureRecognizer:gesture8];
    [self.keyword9 addGestureRecognizer:gesture9];
    
    UILongPressGestureRecognizer *longPressGesture1 = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongGesture:)];
    longPressGesture1.minimumPressDuration = 1.5;
    UILongPressGestureRecognizer *longPressGesture2 = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongGesture:)];
    longPressGesture2.minimumPressDuration = 1.5;
    UILongPressGestureRecognizer *longPressGesture3 = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongGesture:)];
    longPressGesture3.minimumPressDuration = 1.5;
    UILongPressGestureRecognizer *longPressGesture4 = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongGesture:)];
    longPressGesture4.minimumPressDuration = 1.5;
    UILongPressGestureRecognizer *longPressGesture5 = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongGesture:)];
    longPressGesture5.minimumPressDuration = 1.5;
    UILongPressGestureRecognizer *longPressGesture6 = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongGesture:)];
    longPressGesture6.minimumPressDuration = 1.5;
    UILongPressGestureRecognizer *longPressGesture7 = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongGesture:)];
    longPressGesture7.minimumPressDuration = 1.5;
    UILongPressGestureRecognizer *longPressGesture8 = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongGesture:)];
    longPressGesture8.minimumPressDuration = 1.5;
    UILongPressGestureRecognizer *longPressGesture9 = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongGesture:)];
    longPressGesture9.minimumPressDuration = 1.5;
    
    [self.keyword1 addGestureRecognizer:longPressGesture1];
    [self.keyword2 addGestureRecognizer:longPressGesture2];
    [self.keyword3 addGestureRecognizer:longPressGesture3];
    [self.keyword4 addGestureRecognizer:longPressGesture4];
    [self.keyword5 addGestureRecognizer:longPressGesture5];
    [self.keyword6 addGestureRecognizer:longPressGesture6];
    [self.keyword7 addGestureRecognizer:longPressGesture7];
    [self.keyword8 addGestureRecognizer:longPressGesture8];
    [self.keyword9 addGestureRecognizer:longPressGesture9];
    
}

-(void)handleTap:(UITapGestureRecognizer *)sender
{
    NSString * seletedKey = @"";
    
    if (sender.view.tag == 1) {
        seletedKey = self.keyword1.text;
    }else if (sender.view.tag == 2) {
        seletedKey = self.keyword2.text;
    }else if (sender.view.tag == 3) {
        seletedKey = self.keyword3.text;
    }else if (sender.view.tag == 4) {
        seletedKey = self.keyword4.text;
    }else if (sender.view.tag == 5) {
        seletedKey = self.keyword5.text;
    }else if (sender.view.tag == 6) {
        seletedKey = self.keyword6.text;
    }else if (sender.view.tag == 7) {
        seletedKey = self.keyword7.text;
    }else if (sender.view.tag == 8) {
        seletedKey = self.keyword8.text;
    }else if (sender.view.tag == 9) {
        seletedKey = self.keyword9.text;
    }
    
    self.txtNotes.text = [NSString stringWithFormat:@"%@ %@",self.txtNotes.text, seletedKey];
    
}

-(void)handleLongGesture:(UILongPressGestureRecognizer *)sender
{
    int tag = (int)sender.view.tag;
    CGPoint buttonPosition = [sender.view convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *indexPathToReload = [self.tableView indexPathForRowAtPoint:buttonPosition];
    
    //    if ( sender.state == UIGestureRecognizerStateEnded ) {
    if (keywordsArray.count>0) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Remove Keyword" message:@"Are you sure you want to remove this Keyword !" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *btnOk = [UIAlertAction actionWithTitle:LocalizedString(@"btn_ok", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            
            [keywordsArray removeObjectAtIndex:tag-1];
            [self addKeywordLabel];
            [self addKeywordsList];
            [self.tableView reloadData];
            //[self.tableView reloadRowsAtIndexPaths:@[indexPathToReload] withRowAnimation:UITableViewRowAnimationTop];
            
        }];
        
        UIAlertAction *btnCancel = [UIAlertAction actionWithTitle:LocalizedString(@"btn_cancel", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [alert dismissViewControllerAnimated:YES completion:nil];
        }];
        [alert addAction:btnCancel];
        [alert addAction:btnOk];
        [self presentViewController:alert animated:YES completion:nil];
    }
    //    }
}

-(void)didSelectAddKeyword:(UIButton *)sender
{
    if (keywordsArray.count<9) {
        
        CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
        NSIndexPath *indexPathToReload = [self.tableView indexPathForRowAtPoint:buttonPosition];
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:@"Add Keyword" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *btnOk = [UIAlertAction actionWithTitle:LocalizedString(@"btn_ok", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            if(alert.textFields[0].text.length > 0)
            {
                [alert dismissViewControllerAnimated:YES completion:nil];
                
                NSString *text = alert.textFields[0].text;
                [keywordsArray addObject:text];
                [self addKeywordLabel];
                [self addKeywordsList];
                [self.tableView reloadData];
                //[self.tableView reloadRowsAtIndexPaths:@[indexPathToReload] withRowAnimation:UITableViewRowAnimationBottom];
            }
            else
            {
                [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:LocalizedString(@"error_family_name", nil) input:nil];
            }
        }];
        
        UIAlertAction *btnCancel = [UIAlertAction actionWithTitle:LocalizedString(@"btn_cancel", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [alert dismissViewControllerAnimated:YES completion:nil];
        }];
        
        [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
            textField.autocapitalizationType = UITextAutocapitalizationTypeWords;
        }];
        
        [alert addAction:btnCancel];
        [alert addAction:btnOk];
        [self presentViewController:alert animated:YES completion:nil];
        
    }
    else {
        [self showAlertWithTitle:LocalizedString(@"Keywords", nil) message:@"You can not add more than 9 keywords !" input:nil];
    }
    
}

-(void)addKeywordLabel
{
    self.keyword1.text = @"";
    self.keyword2.text = @"";
    self.keyword3.text = @"";
    self.keyword4.text = @"";
    self.keyword5.text = @"";
    self.keyword6.text = @"";
    self.keyword7.text = @"";
    self.keyword8.text = @"";
    self.keyword9.text = @"";
    
    self.keyword1.backgroundColor = [UIColor clearColor];
    self.keyword2.backgroundColor = [UIColor clearColor];
    self.keyword3.backgroundColor = [UIColor clearColor];
    self.keyword4.backgroundColor = [UIColor clearColor];
    self.keyword5.backgroundColor = [UIColor clearColor];
    self.keyword6.backgroundColor = [UIColor clearColor];
    self.keyword7.backgroundColor = [UIColor clearColor];
    self.keyword8.backgroundColor = [UIColor clearColor];
    self.keyword9.backgroundColor = [UIColor clearColor];
    
    if (keywordsArray.count>0) {
        
        for (int i=0;i<keywordsArray.count;i++) {
            
            if (i==0) {
                self.keyword1.text =[NSString stringWithFormat:@"%@",[keywordsArray objectAtIndex:i]];
                self.keyword1.backgroundColor = UIColorFromRGB(0xB7ADA6);
            }else if (i==1) {
                self.keyword2.text =[NSString stringWithFormat:@"%@",[keywordsArray objectAtIndex:i]];
                self.keyword2.backgroundColor = UIColorFromRGB(0xB7ADA6);
            }else if (i==2) {
                self.keyword3.text =[NSString stringWithFormat:@"%@",[keywordsArray objectAtIndex:i]];
                self.keyword3.backgroundColor = UIColorFromRGB(0xB7ADA6);
            }else if (i==3) {
                self.keyword4.text =[NSString stringWithFormat:@"%@",[keywordsArray objectAtIndex:i]];
                self.keyword4.backgroundColor = UIColorFromRGB(0xB7ADA6);
            }else if (i==4) {
                self.keyword5.text =[NSString stringWithFormat:@"%@",[keywordsArray objectAtIndex:i]];
                self.keyword5.backgroundColor = UIColorFromRGB(0xB7ADA6);
            }else if (i==5) {
                self.keyword6.text =[NSString stringWithFormat:@"%@",[keywordsArray objectAtIndex:i]];
                self.keyword6.backgroundColor = UIColorFromRGB(0xB7ADA6);
            }else if (i==6) {
                self.keyword7.text =[NSString stringWithFormat:@"%@",[keywordsArray objectAtIndex:i]];
                self.keyword7.backgroundColor = UIColorFromRGB(0xB7ADA6);
            }else if (i==7) {
                self.keyword8.text =[NSString stringWithFormat:@"%@",[keywordsArray objectAtIndex:i]];
                self.keyword8.backgroundColor = UIColorFromRGB(0xB7ADA6);
            }else if (i==8) {
                self.keyword9.text =[NSString stringWithFormat:@"%@",[keywordsArray objectAtIndex:i]];
                self.keyword9.backgroundColor = UIColorFromRGB(0xB7ADA6);
            }
        }
        
    }
}

-(void)getKeywordsList
{
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setValue:@([shareObject integerForKey:@"user_id"]) forKey:@"user_id"];
    [parameters setValue:@([shareObject integerForKey:@"baby_id"]) forKey:@"baby_id"];
    [parameters setValue:@"milestones" forKey:@"type"];
    NSString *categoryName = [milestoneInfo objectForKey:@"name"];
    //[parameters setValue:categoryName forKey:@"category"];
    
    [[WebServiceManager sharedManager] initWithBaseURL:@"api/getKeywords" parameters:parameters success:^(id result) {
        
        if ([[result valueForKey:@"success"] boolValue]) {
            NSMutableArray *list = (NSMutableArray *)[result valueForKey:@"list"];
            if(list.count > 0){
                
                keywordslistDict = [[NSDictionary alloc] initWithDictionary:[result valueForKey:@"list"]];
                NSString *str = [keywordslistDict objectForKey:categoryName];
                if (str.length>0) {
                    keywordsArray = [[NSMutableArray alloc] initWithArray:[str componentsSeparatedByString:@","]];
                }else {
                    keywordsArray = [[NSMutableArray alloc] init];
                }

                [self addKeywordLabel];
                [self.tableView reloadData];
            }
        }
        else {
            NSLog(@"error");
        }
        
    } failure:^(NSError *error) {
        
    }];
}

-(void)addKeywordsList
{
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setValue:@([shareObject integerForKey:@"user_id"]) forKey:@"user_id"];
    [parameters setValue:@([shareObject integerForKey:@"baby_id"]) forKey:@"baby_id"];
    [parameters setValue:@"milestones" forKey:@"type"];
    NSString *categoryName = [milestoneInfo objectForKey:@"name"];
    [parameters setValue:categoryName forKey:@"category"];
    [parameters setValue:@1 forKey:@"notes"];
    
    if (keywordsArray.count>0) {
        for(int i=0; i<keywordsArray.count; i++){
            [parameters setValue:[keywordsArray componentsJoinedByString:@","] forKey:@"keywords"];
        }
    }else{
        [parameters setValue:@"" forKey:@"keywords"];
    }
    
    [[WebServiceManager sharedManager] initWithBaseURL:@"api/setKeywords" parameters:parameters success:^(id result) {
        
        if ([[result valueForKey:@"success"] boolValue]) {
            NSMutableArray *list = (NSMutableArray *)[result valueForKey:@"list"];
            if(list.count > 0){
                
                keywordslistDict = [[NSDictionary alloc] initWithDictionary:[result valueForKey:@"list"]];
                NSString *str = [keywordslistDict objectForKey:categoryName];
                if (str.length>0) {
                    keywordsArray = [[NSMutableArray alloc] initWithArray:[str componentsSeparatedByString:@","]];
                }else {
                    keywordsArray = [[NSMutableArray alloc] init];
                }

                //keywordsArray = list;
                //[list removeAllObjects];
            }
        }
        else {
            NSLog(@"error");
        }
        
    } failure:^(NSError *error) {
        
    }];
    
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)aTableView
{
    return 7;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return nil;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([[milestoneInfo objectForKey:@"name"] isEqualToString:@"grow"])
    {
        if(indexPath.section == 1)
        {
            if([[milestoneInfo objectForKey:@"type"] isEqualToString:@"height"])
            {
                if(indexPath.row != 0) return 0;
            }
            if([[milestoneInfo objectForKey:@"type"] isEqualToString:@"weight"])
            {
                if(indexPath.row != 1) return 0;
            }
            if([[milestoneInfo objectForKey:@"type"] isEqualToString:@"head_size"])
            {
                if(indexPath.row != 2) return 0;
            }
            if([[milestoneInfo objectForKey:@"type"] isEqualToString:@"teething"] || [[milestoneInfo objectForKey:@"type"] isEqualToString:@"adult_teething"])
            {
                if(indexPath.row != 3) return 0;
            }
        }
        if(indexPath.section == 2 || indexPath.section == 3 || indexPath.section == 4 || indexPath.section == 5) return 0;
    }
    else if([[milestoneInfo objectForKey:@"name"] isEqualToString:@"move"])
    {
        if(indexPath.section == 1 || indexPath.section == 3 || indexPath.section == 4 || indexPath.section == 5) return 0;
    }
    else if([[milestoneInfo objectForKey:@"name"] isEqualToString:@"iq"])
    {
        if(indexPath.section == 3)
        {
            if([[milestoneInfo objectForKey:@"type"] isEqualToString:@"verbal"])
            {
                if(indexPath.row != 0) return 0;
            }
            if([[milestoneInfo objectForKey:@"type"] isEqualToString:@"learning"])
            {
                if(indexPath.row != 1) return 0;
            }
            if([[milestoneInfo objectForKey:@"type"] isEqualToString:@"moods"])
            {
                if(indexPath.row != 2) return 0;
            }
        }
        if(indexPath.section == 1 || indexPath.section == 2 || indexPath.section == 4 || indexPath.section == 5) return 0;
    }
    else if([[milestoneInfo objectForKey:@"name"] isEqualToString:@"health"])
    {
        if(indexPath.section == 4)
        {
            if([[milestoneInfo objectForKey:@"type"] isEqualToString:@"vaccine"])
            {
                if(indexPath.row != 0) return 0;
            }
            if([[milestoneInfo objectForKey:@"type"] isEqualToString:@"allergies"])
            {
                if(indexPath.row != 1) return 0;
            }
            if([[milestoneInfo objectForKey:@"type"] isEqualToString:@"symptom"])
            {
                if(indexPath.row != 2) return 0;
            }
            if([[milestoneInfo objectForKey:@"type"] isEqualToString:@"diagnosis"])
            {
                if(indexPath.row != 3) return 0;
            }
            if([[milestoneInfo objectForKey:@"type"] isEqualToString:@"medication"])
            {
                if(indexPath.row != 4) return 0;
            }
            if([[milestoneInfo objectForKey:@"type"] isEqualToString:@"clinic"])
            {
                if(indexPath.row != 5) return 0;
            }
            if([[milestoneInfo objectForKey:@"type"] isEqualToString:@"reminder"])
            {
                if(indexPath.row != 6 && indexPath.row != 7) return 0;
            }
        }
        if(indexPath.section == 1 || indexPath.section == 2 || indexPath.section == 3 || indexPath.section == 5) return 0;
    }
    else if([[milestoneInfo objectForKey:@"name"] isEqualToString:@"others"])
    {
        if(indexPath.section == 1 || indexPath.section == 2 || indexPath.section == 3 || indexPath.section == 4) return 0;
    }
    
    return UITableViewAutomaticDimension;
    
}
- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([[milestoneInfo objectForKey:@"name"] isEqualToString:@"grow"])
    {
        if(indexPath.section == 1)
        {
            if([[milestoneInfo objectForKey:@"type"] isEqualToString:@"height"])
            {
                if(indexPath.row != 0) return 0;
            }
            if([[milestoneInfo objectForKey:@"type"] isEqualToString:@"weight"])
            {
                if(indexPath.row != 1) return 0;
            }
            if([[milestoneInfo objectForKey:@"type"] isEqualToString:@"head_size"])
            {
                if(indexPath.row != 2) return 0;
            }
            if([[milestoneInfo objectForKey:@"type"] isEqualToString:@"teething"] || [[milestoneInfo objectForKey:@"type"] isEqualToString:@"adult_teething"])
            {
                if(indexPath.row != 3) return 0;
            }
        }
        else if(indexPath.section == 2 || indexPath.section == 3 || indexPath.section == 4 || indexPath.section == 5) return 0;
    }
    else if([[milestoneInfo objectForKey:@"name"] isEqualToString:@"move"])
    {
        if(indexPath.section == 1 || indexPath.section == 3 || indexPath.section == 4 || indexPath.section == 5 ) return 0;
    }
    else if([[milestoneInfo objectForKey:@"name"] isEqualToString:@"iq"])
    {
        if(indexPath.section == 3)
        {
            if([[milestoneInfo objectForKey:@"type"] isEqualToString:@"verbal"])
            {
                if(indexPath.row != 0) return 0;
            }
            if([[milestoneInfo objectForKey:@"type"] isEqualToString:@"learning"])
            {
                if(indexPath.row != 1) return 0;
            }
            if([[milestoneInfo objectForKey:@"type"] isEqualToString:@"moods"])
            {
                if(indexPath.row != 2) return 0;
            }
        }
        if(indexPath.section == 1 || indexPath.section == 2 || indexPath.section == 4 || indexPath.section == 5) return 0;
    }
    else if([[milestoneInfo objectForKey:@"name"] isEqualToString:@"health"])
    {
        if(indexPath.section == 4)
        {
            if([[milestoneInfo objectForKey:@"type"] isEqualToString:@"vaccine"])
            {
                if(indexPath.row != 0) return 0;
            }
            if([[milestoneInfo objectForKey:@"type"] isEqualToString:@"allergies"])
            {
                if(indexPath.row != 1) return 0;
            }
            if([[milestoneInfo objectForKey:@"type"] isEqualToString:@"symptom"])
            {
                if(indexPath.row != 2) return 0;
            }
            if([[milestoneInfo objectForKey:@"type"] isEqualToString:@"diagnosis"])
            {
                if(indexPath.row != 3) return 0;
            }
            if([[milestoneInfo objectForKey:@"type"] isEqualToString:@"medication"])
            {
                if(indexPath.row != 4) return 0;
            }
            if([[milestoneInfo objectForKey:@"type"] isEqualToString:@"clinic"])
            {
                if(indexPath.row != 5) return 0;
            }
            if([[milestoneInfo objectForKey:@"type"] isEqualToString:@"reminder"])
            {
                if(indexPath.row != 6 && indexPath.row != 7) return 0;
            }
        }
        else if(indexPath.section == 1 || indexPath.section == 2 || indexPath.section == 3 || indexPath.section == 5) return 0;
    }
    else if([[milestoneInfo objectForKey:@"name"] isEqualToString:@"others"])
    {
        if(indexPath.section == 1 || indexPath.section == 2 || indexPath.section == 3 || indexPath.section == 4) return 0;
    }
    
    return UITableViewAutomaticDimension;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 0)
    {
        // Date & Time
        if(indexPath.row == 0)
        {
            NSDate *minimumDate = [[NSDate date] dateByAddingTimeInterval:-20*365*24*60*60];
            NSDate *maximumDate = [NSDate date];
            
            NSLog(@"selectedMilestoneDateTime=%@", selectedMilestoneDateTime);
            [ActionSheetDatePicker showPickerWithTitle:@"" datePickerMode:UIDatePickerModeDate selectedDate:selectedMilestoneDateTime minimumDate:minimumDate maximumDate:maximumDate doneBlock:^(ActionSheetDatePicker *picker, id selectedDate, id origin) {
                
                selectedMilestoneDate = selectedDate;
                NSLog(@"selectedMilestoneDate=%@", selectedMilestoneDate);
                
                formatter.dateFormat = @"d MMM yyyy";
                NSString *milestoneDate = [formatter stringFromDate:selectedDate];
                NSLog(@"milestoneDate=%@", milestoneDate);
                
                [self performSelector:@selector(selectMilestoneTime:) withObject:milestoneDate afterDelay:0.5];
                
            } cancelBlock:^(ActionSheetDatePicker *picker) {
                
            } origin:self.view];
        }
    }
    else if(indexPath.section == 1)
    {
        NSString *unit = @"";
        NSArray *stringList = [[NSArray alloc] init];
        NSArray *initialList = [[NSArray alloc] init];
        ActionSheetMultipleStringPicker *picker;
        
        if(indexPath.row == 0)
        {
            unit = [unitInfo objectForKey:@"height"];
            if ([unit isKindOfClass:[NSNull class]]) {
                unit = @"cm";
            }
            if([[unitInfo objectForKey:@"height"] isEqualToString:@"cm"]){
                stringList = [NSArray arrayWithObjects:no300List, digitList, nil];
            }else{
                stringList = [NSArray arrayWithObjects:no100List, digitList, nil];
            }
            
            if (selectedHeightIndexes == nil) {
                NSString *babyHeight = [milestoneInfo objectForKey:@"attribute_value"];
                NSString *str = [self convertUnit:babyHeight unit:[unitInfo objectForKey:@"height"] spacing:NO];
                str = [str stringByReplacingOccurrencesOfString:@"cm" withString:@""];
                str = [str stringByReplacingOccurrencesOfString:@"in" withString:@""];
                selectedHeightIndexes = [str componentsSeparatedByString:@"."];
            }
            
            if (selectedHeightIndexes.count>1) {
                initialList = [NSArray arrayWithObjects:@([selectedHeightIndexes[0] intValue]), @([selectedHeightIndexes[1] intValue]), nil];
            }else {
                initialList = [NSArray arrayWithObjects:@([selectedHeightIndexes[0] intValue]), @0, nil];
            }
            
            picker = [ActionSheetMultipleStringPicker showPickerWithTitle:@"" rows:stringList initialSelection:initialList doneBlock:^(ActionSheetMultipleStringPicker *picker, NSArray *selectedIndexes, id selectedValues) {
                
                selectedHeight = [[selectedValues componentsJoinedByString:@"."] floatValue];
                NSLog(@"selectedHeight=%f", selectedHeight);
                selectedHeightIndexes = selectedIndexes;
                self.txtHeight.text = [self convertUnit:[NSString stringWithFormat:@"%f %@", selectedHeight, [unitInfo objectForKey:@"height"]] unit:[unitInfo objectForKey:@"height"] spacing:YES];
                
                [self.tableView reloadData];
                
            } cancelBlock:^(ActionSheetMultipleStringPicker *picker) {
                
            } origin:self.view];
        }
        if(indexPath.row == 1)
        {
            unit = [unitInfo objectForKey:@"weight"];
            if ([unit isKindOfClass:[NSNull class]]) {
                unit = @"kg";
            }
            stringList = [NSArray arrayWithObjects:no200List, digit2List, nil];
            
            if (selectedWeightIndexes == nil) {
                NSString *babyWeight = [milestoneInfo objectForKey:@"attribute_value"];
                NSString *str = [self convertUnit:babyWeight unit:[unitInfo objectForKey:@"weight"] spacing:NO];
                str = [str stringByReplacingOccurrencesOfString:@"kg" withString:@""];
                str = [str stringByReplacingOccurrencesOfString:@"lb" withString:@""];
                selectedWeightIndexes = [str componentsSeparatedByString:@"."];
            }
            
            if (selectedWeightIndexes.count>1) {
                initialList = [NSArray arrayWithObjects:@([selectedWeightIndexes[0] intValue]), @([selectedWeightIndexes[1] intValue]), nil];
            }else {
                initialList = [NSArray arrayWithObjects:@([selectedWeightIndexes[0] intValue]), @0, nil];
            }
            
            picker = [ActionSheetMultipleStringPicker showPickerWithTitle:@"" rows:stringList initialSelection:initialList doneBlock:^(ActionSheetMultipleStringPicker *picker, NSArray *selectedIndexes, id selectedValues) {
                
                selectedWeight = [[selectedValues componentsJoinedByString:@"."] floatValue];
                NSLog(@"selectedWeight=%f", selectedWeight);
                selectedWeightIndexes = selectedIndexes;
                self.txtWeight.text = [self convertUnit:[NSString stringWithFormat:@"%.2f %@", selectedWeight, [unitInfo objectForKey:@"weight"]] unit:[unitInfo objectForKey:@"weight"] spacing:YES];
                
                [self.tableView reloadData];
                
            } cancelBlock:^(ActionSheetMultipleStringPicker *picker) {
                
            } origin:self.view];
        }
        if(indexPath.row == 2)
        {
            unit = [unitInfo objectForKey:@"head_size"];
            if ([unit isKindOfClass:[NSNull class]]) {
                unit = @"cm";
            }
            if([[unitInfo objectForKey:@"head_size"] isEqualToString:@"cm"]){
                stringList = [NSArray arrayWithObjects:no100List, digitList, nil];
            }else{
                stringList = [NSArray arrayWithObjects:no60List, digitList, nil];
            }
            
            if (selectedHeadSizeIndexes == nil) {
                NSString *babyHeadSize = [milestoneInfo objectForKey:@"attribute_value"];
                NSString *str = [self convertUnit:babyHeadSize unit:[unitInfo objectForKey:@"head_size"] spacing:NO];
                str = [str stringByReplacingOccurrencesOfString:@"cm" withString:@""];
                str = [str stringByReplacingOccurrencesOfString:@"in" withString:@""];
                selectedHeadSizeIndexes = [str componentsSeparatedByString:@"."];
            }
            
            if (selectedHeadSizeIndexes.count>1) {
                initialList = [NSArray arrayWithObjects:@([selectedHeadSizeIndexes[0] intValue]), @([selectedHeadSizeIndexes[1] intValue]), nil];
            }else {
                initialList = [NSArray arrayWithObjects:@([selectedHeadSizeIndexes[0] intValue]), @0, nil];
            }
            
            picker = [ActionSheetMultipleStringPicker showPickerWithTitle:@"" rows:stringList initialSelection:initialList doneBlock:^(ActionSheetMultipleStringPicker *picker, NSArray *selectedIndexes, id selectedValues) {
                
                selectedHeadSize = [[selectedValues componentsJoinedByString:@"."] floatValue];
                NSLog(@"selectedHeadSize=%f", selectedHeadSize);
                selectedHeadSizeIndexes = selectedIndexes;
                self.txtHeadSize.text = [self convertUnit:[NSString stringWithFormat:@"%f %@", selectedHeadSize, [unitInfo objectForKey:@"head_size"]] unit:[unitInfo objectForKey:@"head_size"] spacing:YES];
                
                [self.tableView reloadData];
                
            } cancelBlock:^(ActionSheetMultipleStringPicker *picker) {
                
            } origin:self.view];
        }
        
        UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(40, 0, (picker.viewSize.width-40)/2, 36)];
        label1.text = @".";
        label1.textAlignment = NSTextAlignmentRight;
        label1.font = [UIFont systemFontOfSize:20.0f];
        label1.backgroundColor = [[UIColor redColor] colorWithAlphaComponent:0.f];
        [[picker.pickerView.subviews objectAtIndex:1] addSubview:label1];
        
        UILabel *label2 = [[UILabel alloc] initWithFrame:CGRectMake(40+(picker.viewSize.width-40)/2, 0, (picker.viewSize.width-40)/2, 36)];
        label2.text = [NSString stringWithFormat:@"      %@", unit];
        label2.textAlignment = NSTextAlignmentCenter;
        label2.font = [UIFont systemFontOfSize:20.0f];
        label2.backgroundColor = [[UIColor greenColor] colorWithAlphaComponent:0.f];
        [[picker.pickerView.subviews objectAtIndex:1] addSubview:label2];
    }
    if(indexPath.section == 4)
    {
        // Date & Time
        if(indexPath.row == 7)
        {
            NSDate *minimumDate = [NSDate date];
            NSDate *maximumDate = [[NSDate date] dateByAddingTimeInterval:4*365*24*60*60];
            [ActionSheetDatePicker showPickerWithTitle:@"" datePickerMode:UIDatePickerModeDate selectedDate:selectedReminderDateTime minimumDate:minimumDate maximumDate:maximumDate doneBlock:^(ActionSheetDatePicker *picker, id selectedDate, id origin) {
                
                selectedReminderDate = selectedDate;
                NSLog(@"selectedReminderDate=%@", selectedReminderDate);
                formatter.dateFormat = @"d MMM yyyy";
                NSString *reminderDate = [formatter stringFromDate:selectedDate];
                NSLog(@"reminderDate=%@", reminderDate);
                
                [self performSelector:@selector(selectReminderTime:) withObject:reminderDate afterDelay:0.5];
                
            } cancelBlock:^(ActionSheetDatePicker *picker) {
                
            } origin:self.view];
        }
    }
}

#pragma mark -
#pragma mark UIScrollViewDelegate Methods
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if([self.navigationController.visibleViewController isKindOfClass:[MilestoneEditViewController class]])
    {
        CGPoint translation = [scrollView.panGestureRecognizer translationInView:scrollView.superview];
        if(translation.y > 0)
        {
            [self.view endEditing:YES];
        }
    }
}




#pragma mark - UIImagePickerController Delegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    NSString *mediaType = info[UIImagePickerControllerMediaType];
    if ([mediaType isEqualToString:(NSString *)kUTTypeImage])
    {
        NSLog(@"Media is an image");
        // Handle selected image
        UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
        NSLog(@"image=%@", image);
        
        if (self.btnPhoto1.hidden) {
            imgUploadPhoto = [info objectForKey:UIImagePickerControllerOriginalImage];
        }else if (self.btnPhoto2.hidden) {
            imgUploadPhoto2 = [info objectForKey:UIImagePickerControllerOriginalImage];
        }else if (self.btnPhoto3.hidden) {
            imgUploadPhoto3 = [info objectForKey:UIImagePickerControllerOriginalImage];
        }
        
        if (picker.sourceType == UIImagePickerControllerSourceTypeCamera) {
            UIImageWriteToSavedPhotosAlbum([info objectForKey:UIImagePickerControllerOriginalImage], self, @selector(savedPhotoImage:didFinishSavingWithError:contextInfo:), nil);
        }
        
        if (IS_IPAD) {
            if (self.popover.isPopoverVisible) {
                [self.popover dismissPopoverAnimated:NO];
            }
            [picker dismissViewControllerAnimated:YES completion:^{
                [self openEditor:[info objectForKey:UIImagePickerControllerOriginalImage]];
            }];
        } else {
            [picker dismissViewControllerAnimated:YES completion:^{
                [self openEditor:[info objectForKey:UIImagePickerControllerOriginalImage]];
            }];
        }
    }
    else if ([mediaType isEqualToString:(NSString *)kUTTypeMovie])
    {
        BOOL ntwrkFlagStatus = [[NSUserDefaults standardUserDefaults] boolForKey:@"can_Upload_Using_Mobile_Network"];
        BOOL checkWifiStatus = [COMMON_HELPER checkforWifiNetwork];
        
        if ((checkWifiStatus==NO) && (ntwrkFlagStatus==NO)) {
            [self dismissViewControllerAnimated:NO completion:^{
            }];
            [self showAlertWithTitle:LocalizedString(@"mobile_data_usages_title", nil) message:LocalizedString(@"mobile_data_usages_message", nil) input:nil];
            return;
        }else {
            
            NSLog(@"Media is a video");
            NSURL *vUrl= (NSURL*)[info objectForKey:UIImagePickerControllerMediaURL];
            NSString *moviePath = [vUrl path];
            
            NSURL* uploadURL;
            if (self.btnPhoto1.hidden) {
                [[NSFileManager defaultManager] removeItemAtPath:[NSString stringWithFormat:@"%@video1.mov", NSTemporaryDirectory()] error:NULL];
                uploadURL = [NSURL fileURLWithPath:
                             [NSTemporaryDirectory() stringByAppendingPathComponent:@"video1.mov"]];
            }else if (self.btnPhoto2.hidden) {
                [[NSFileManager defaultManager] removeItemAtPath:[NSString stringWithFormat:@"%@video2.mov", NSTemporaryDirectory()] error:NULL];
                uploadURL = [NSURL fileURLWithPath:
                             [NSTemporaryDirectory() stringByAppendingPathComponent:@"video2.mov"]];
            }else if (self.btnPhoto3.hidden) {
                [[NSFileManager defaultManager] removeItemAtPath:[NSString stringWithFormat:@"%@video3.mov", NSTemporaryDirectory()] error:NULL];
                uploadURL = [NSURL fileURLWithPath:
                             [NSTemporaryDirectory() stringByAppendingPathComponent:@"video3.mov"]];
            }
            
            // Generate Thumbnail Image.
            UIImage *tImg = [COMMON_HELPER generateThumbnailImageFromVideoWithUrl:vUrl];
            
            [SVProgressHUD showWithStatus:LocalizedString(@"processing_video", nil)];
            [COMMON_HELPER compressVideo:vUrl outputURL:uploadURL handler:^(AVAssetExportSession *completion) {
                
                if (completion.status == AVAssetExportSessionStatusCompleted) {
                    NSData *newDataForUpload = [NSData dataWithContentsOfURL:uploadURL];
                    NSLog(@"Size of new Video after compression is (bytes):%lu",(unsigned long)[newDataForUpload length]);
                    
                    CGFloat sizeInMB = ([newDataForUpload length]/1000)/1000;
                    if (sizeInMB>15) {
                        [self showAlertWithTitleCallback:@"Large video size" message:@"Can not add this video." callback:^(UIAlertAction *action) {
                            [[NSFileManager defaultManager] removeItemAtPath:[NSString stringWithFormat:@"%@", uploadURL] error:NULL];
                        }];
                    }
                    else {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            if (self.btnPhoto1.hidden) {
                                self.btnPhoto1.hidden = false;
                                videoUrl = [uploadURL absoluteString];
                                [self.btnPhoto1 setImage:tImg forState:UIControlStateNormal];
                            }else if (self.btnPhoto2.hidden) {
                                self.btnPhoto2.hidden = false;
                                videoUrl2 = [uploadURL absoluteString];
                                [self.btnPhoto2 setImage:tImg forState:UIControlStateNormal];
                            }else if (self.btnPhoto3.hidden) {
                                self.btnPhoto3.hidden = false;
                                videoUrl3 = [uploadURL absoluteString];
                                [self.btnPhoto3 setImage:tImg forState:UIControlStateNormal];
                            }
                        });
                        
                    }
                }
                [SVProgressHUD dismiss];
                
            }];
            
            if (picker.sourceType == UIImagePickerControllerSourceTypeCamera) {
                if (UIVideoAtPathIsCompatibleWithSavedPhotosAlbum (moviePath)) {
                    UISaveVideoAtPathToSavedPhotosAlbum (moviePath, nil, nil, nil);
                }
            }
            
            [self dismissViewControllerAnimated:NO completion:^{
            }];
        }
    }
    
    /*
    float scale = self.view.bounds.size.width*2/imgUploadPhoto.size.width;
    CGSize newSize = CGSizeMake(self.view.bounds.size.width*2, imgUploadPhoto.size.height*scale);
    if(imgUploadPhoto.size.width > imgUploadPhoto.size.height)
    {
        scale = self.view.bounds.size.width*2/imgUploadPhoto.size.height;
        newSize = CGSizeMake(imgUploadPhoto.size.width*scale, self.view.bounds.size.width*2);
    }
    UIGraphicsBeginImageContext(newSize);
    [imgUploadPhoto drawInRect:(CGRect){0, 0, newSize}];
    imgUploadPhoto = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    [self.btnPhoto setImage:imgUploadPhoto forState:UIControlStateNormal];
    self.btnPhoto.hidden = NO;
    
    [self dismissViewControllerAnimated:NO completion:^{
        
    }];
     
     */
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)openEditor:(UIImage*) image1
{
    PECropViewController *controller = [[PECropViewController alloc] init];
    controller.delegate = self;
    controller.image = image1;
    controller.toolbarHidden = YES;
    
//    CGFloat width = image1.size.width;
//    CGFloat height = image1.size.height;
//    CGFloat length = MIN(width, height);
//    controller.imageCropRect = CGRectMake((width - length) / 2,(height - length) / 2,length,length);
    
    controller.cropAspectRatio = [[NSUserDefaults standardUserDefaults] floatForKey:@"Img_Aspect"];
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:controller];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        navigationController.modalPresentationStyle = UIModalPresentationFullScreen;
    }
    [self presentViewController:navigationController animated:YES completion:NULL];
}


- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return largeImageView;
}

#pragma mark - PECropViewControllerDelegate methods

- (void)cropViewController:(PECropViewController *)controller didFinishCroppingImage:(UIImage *)croppedImage transform:(CGAffineTransform)transform cropRect:(CGRect)cropRect
{
    [controller dismissViewControllerAnimated:YES completion:NULL];
    
    float scale = self.view.bounds.size.width*2/croppedImage.size.width;
    CGSize newSize = CGSizeMake(self.view.bounds.size.width*2, croppedImage.size.height*scale);
    
    if(croppedImage.size.width > croppedImage.size.height)
    {
        scale = self.view.bounds.size.width*2/croppedImage.size.height;
        newSize = CGSizeMake(croppedImage.size.width*scale, self.view.bounds.size.width*2);
    }
    UIGraphicsBeginImageContext(newSize);
    [croppedImage drawInRect:(CGRect){0, 0, newSize}];
    croppedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    if (self.btnPhoto1.hidden) {
        imgUploadCropped = croppedImage;
        self.btnPhoto1.hidden = false;
        [self.btnPhoto1 setImage:croppedImage forState:UIControlStateNormal];
    }else if (self.btnPhoto2.hidden) {
        imgUploadCropped2 = croppedImage;
        self.btnPhoto2.hidden = false;
        [self.btnPhoto2 setImage:croppedImage forState:UIControlStateNormal];
    }else if (self.btnPhoto3.hidden) {
        imgUploadCropped3 = croppedImage;
        self.btnPhoto3.hidden = false;
        [self.btnPhoto3 setImage:croppedImage forState:UIControlStateNormal];
    }

}

- (void)cropViewControllerDidCancel:(PECropViewController *)controller
{
    //    self.btnPhoto1.hidden = YES;
    //    self.btnPhoto2.hidden = YES;
    //    self.btnPhoto3.hidden = YES;
    
    //    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
    //        // [self updateEditButtonEnabled];
    //    }
    [controller dismissViewControllerAnimated:YES completion:NULL];
}

- (void) savedPhotoImage:(UIImage *)image
didFinishSavingWithError:(NSError *)error
             contextInfo:(void *)contextInfo
{
    NSLog(@"This image has been saved to your camera roll");
}

- (void)customizeFont
{
    if (IS_IPAD) {
        self.lblDate.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f];
        self.txtDate.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f];
        self.lblHeight.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f];
        self.txtHeight.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f];
        self.lblWeight.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f];
        self.txtWeight.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f];
        self.lblHeadSize.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f];
        self.txtHeadSize.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f];
        self.lblMovement.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f];
        self.txtMovement.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f];
        self.lblMoods.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f];
        self.txtMoods.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f];
        self.lblVerbal.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f];
        self.txtVerbal.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f];
        self.lblLearning.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f];
        self.txtLearning.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f];
        self.lblVaccine.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f];
        self.txtVaccine.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f];
        self.lblAllergies.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f];
        self.txtAllergies.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f];
        self.lblSymptom.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f];
        self.txtSymptom.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f];
        self.lblDiagnosis.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f];
        self.txtDiagnosis.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f];
        self.lblMedication.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f];
        self.txtMedication.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f];
        self.lblClinic.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f];
        self.txtClinic.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f];
        self.lblReminder.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f];
        self.txtReminder.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f];
        self.lblReminderTitle.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f];
        self.txtReminderTitle.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f];
        self.lblOthers.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f];
        self.txtOthers.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f];
        self.lblChecklist.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f];
        self.txtChecklist.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f];
        self.lblNotes.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f];
        self.txtNotes.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f];
        
        self.lblTeething.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f];
        self.txtTeething.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f];
        self.lblPhoto.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f];
        [self.btnDelete.titleLabel setFont:[UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f]];
    }
}

#pragma mark- Teething view setup
-(void) doSetupForTeethingView
{
    if ([[milestoneInfo objectForKey:@"type"] isEqualToString:@"adult_teething"]) {
        self.btnAdult.backgroundColor = UIColorFromRGB(0x8D8082);
        self.btnAdult.selected = YES;
        self.btnBaby.backgroundColor = UIColorFromRGB(0xE0D3CB);
        self.btnBaby.selected = NO;
        self.viewTeethingAdult.hidden = NO;
        self.viewTeethingBaby.hidden = YES;
    }else {
        self.btnBaby.backgroundColor = UIColorFromRGB(0x8D8082);
        self.btnBaby.selected = YES;
        self.btnAdult.backgroundColor = UIColorFromRGB(0xE0D3CB);
        self.btnAdult.selected = NO;
        self.viewTeethingBaby.hidden = NO;
        self.viewTeethingAdult.hidden = YES;
    }
    
//    self.btnBaby.backgroundColor = UIColorFromRGB(0x8D8082);
//    self.btnBaby.selected = YES;
//    self.viewTeethingBaby.hidden = NO;
//    self.btnAdult.backgroundColor = UIColorFromRGB(0xE0D3CB);
//    self.btnAdult.selected = NO;
//    self.viewTeethingAdult.hidden = YES;
    
    self.btnBaby.layer.cornerRadius = 3.f;
    self.btnBaby.layer.masksToBounds = YES;
    self.btnAdult.layer.cornerRadius = 3.f;
    self.btnAdult.layer.masksToBounds = YES;
    
    if(IS_IPAD) {
        [self.btnBaby.titleLabel setFont:[UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f]];
        [self.btnAdult.titleLabel setFont:[UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f]];
        
        self.viewTeethBaby.transform = CGAffineTransformMakeScale(1.2, 1.2);
        self.viewTeethGuideBaby.transform = CGAffineTransformMakeScale(1.2, 1.2);
        self.viewTeethAdult.transform = CGAffineTransformMakeScale(1.2, 1.2);
        self.viewTeethGuideAdult.transform = CGAffineTransformMakeScale(1.2, 1.2);
    }
    [self.btnBaby addTarget:self action:@selector(didSelectBaby:) forControlEvents:UIControlEventTouchUpInside];
    [self.btnAdult addTarget:self action:@selector(didSelectAdult:) forControlEvents:UIControlEventTouchUpInside];
    
}

-(void)didSelectBaby:(UIButton *)sender {
    self.btnBaby.backgroundColor = UIColorFromRGB(0x8D8082);
    self.btnBaby.selected = YES;
    self.btnAdult.backgroundColor = UIColorFromRGB(0xE0D3CB);
    self.btnAdult.selected = NO;
    self.viewTeethingBaby.hidden = NO;
    self.viewTeethingAdult.hidden = YES;
}

-(void)didSelectAdult:(UIButton *)sender {
    self.btnAdult.backgroundColor = UIColorFromRGB(0x8D8082);
    self.btnAdult.selected = YES;
    self.btnBaby.backgroundColor = UIColorFromRGB(0xE0D3CB);
    self.btnBaby.selected = NO;
    self.viewTeethingAdult.hidden = NO;
    self.viewTeethingBaby.hidden = YES;
}

-(void)setupAdultTeeth:(NSMutableArray *)teethArray
{
    for(int i=0; i<[teethArray count]; i++)
    {
        int teethId = [[[teethArray objectAtIndex:i] objectForKey:@"teeth_id"] intValue];
        NSString *content = [[teethArray objectAtIndex:i] objectForKey:@"content"];
        
        if(teethId == 21)
        {
            [adultTeethList replaceObjectAtIndex:0 withObject:@(1)];
            [self.btnTeethingAdult1 setImage:[UIImage imageNamed:@"btn_teeth21_selected"] forState:UIControlStateNormal];
            if(teethId == [[milestoneInfo objectForKey:@"reference_id"] intValue]) self.btnTeethingAdult1.alpha = 1.0f;
            if([content isEqualToString:@"Lost Tooth"])
            {
                [adultTeethList replaceObjectAtIndex:0 withObject:@(0)];
                self.btnTeethingAdult1.imageView.image = [self.btnTeethingAdult1.imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                self.btnTeethingAdult1.imageView.tintColor = UIColorFromRGB(0x8D8082);
            }
        }
        if(teethId == 22)
        {
            [adultTeethList replaceObjectAtIndex:1 withObject:@(1)];
            [self.btnTeethingAdult2 setImage:[UIImage imageNamed:@"btn_teeth22_selected"] forState:UIControlStateNormal];
            if(teethId == [[milestoneInfo objectForKey:@"reference_id"] intValue]) self.btnTeethingAdult2.alpha = 1.0f;
            if([content isEqualToString:@"Lost Tooth"])
            {
                [adultTeethList replaceObjectAtIndex:1 withObject:@(0)];
                self.btnTeethingAdult2.imageView.image = [self.btnTeethingAdult2.imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                self.btnTeethingAdult2.imageView.tintColor = UIColorFromRGB(0x8D8082);
            }
        }
        if(teethId == 23)
        {
            [adultTeethList replaceObjectAtIndex:2 withObject:@(1)];
            [self.btnTeethingAdult3 setImage:[UIImage imageNamed:@"btn_teeth23_selected"] forState:UIControlStateNormal];
            if(teethId == [[milestoneInfo objectForKey:@"reference_id"] intValue]) self.btnTeethingAdult3.alpha = 1.0f;
            if([content isEqualToString:@"Lost Tooth"])
            {
                [adultTeethList replaceObjectAtIndex:2 withObject:@(0)];
                self.btnTeethingAdult3.imageView.image = [self.btnTeethingAdult3.imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                self.btnTeethingAdult3.imageView.tintColor = UIColorFromRGB(0x8D8082);
            }
        }
        if(teethId == 24)
        {
            [adultTeethList replaceObjectAtIndex:3 withObject:@(1)];
            [self.btnTeethingAdult4 setImage:[UIImage imageNamed:@"btn_teeth24_selected"] forState:UIControlStateNormal];
            if(teethId == [[milestoneInfo objectForKey:@"reference_id"] intValue]) self.btnTeethingAdult4.alpha = 1.0f;
            if([content isEqualToString:@"Lost Tooth"])
            {
                [adultTeethList replaceObjectAtIndex:3 withObject:@(0)];
                self.btnTeethingAdult4.imageView.image = [self.btnTeethingAdult4.imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                self.btnTeethingAdult4.imageView.tintColor = UIColorFromRGB(0x8D8082);
            }
        }
        if(teethId == 25)
        {
            [adultTeethList replaceObjectAtIndex:4 withObject:@(1)];
            [self.btnTeethingAdult5 setImage:[UIImage imageNamed:@"btn_teeth25_selected"] forState:UIControlStateNormal];
            if(teethId == [[milestoneInfo objectForKey:@"reference_id"] intValue]) self.btnTeethingAdult5.alpha = 1.0f;
            if([content isEqualToString:@"Lost Tooth"])
            {
                [adultTeethList replaceObjectAtIndex:4 withObject:@(0)];
                self.btnTeethingAdult5.imageView.image = [self.btnTeethingAdult5.imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                self.btnTeethingAdult5.imageView.tintColor = UIColorFromRGB(0x8D8082);
            }
        }
        if(teethId == 26)
        {
            [adultTeethList replaceObjectAtIndex:5 withObject:@(1)];
            [self.btnTeethingAdult6 setImage:[UIImage imageNamed:@"btn_teeth26_selected"] forState:UIControlStateNormal];
            if(teethId == [[milestoneInfo objectForKey:@"reference_id"] intValue]) self.btnTeethingAdult6.alpha = 1.0f;
            if([content isEqualToString:@"Lost Tooth"])
            {
                [adultTeethList replaceObjectAtIndex:5 withObject:@(0)];
                self.btnTeethingAdult6.imageView.image = [self.btnTeethingAdult6.imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                self.btnTeethingAdult6.imageView.tintColor = UIColorFromRGB(0x8D8082);
            }
        }
        if(teethId == 27)
        {
            [adultTeethList replaceObjectAtIndex:6 withObject:@(1)];
            [self.btnTeethingAdult7 setImage:[UIImage imageNamed:@"btn_teeth27_selected"] forState:UIControlStateNormal];
            if(teethId == [[milestoneInfo objectForKey:@"reference_id"] intValue]) self.btnTeethingAdult7.alpha = 1.0f;
            if([content isEqualToString:@"Lost Tooth"])
            {
                [adultTeethList replaceObjectAtIndex:6 withObject:@(0)];
                self.btnTeethingAdult7.imageView.image = [self.btnTeethingAdult7.imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                self.btnTeethingAdult7.imageView.tintColor = UIColorFromRGB(0xBFB5AE);
            }
        }
        if(teethId == 28)
        {
            [adultTeethList replaceObjectAtIndex:7 withObject:@(1)];
            [self.btnTeethingAdult8 setImage:[UIImage imageNamed:@"btn_teeth28_selected"] forState:UIControlStateNormal];
            if(teethId == [[milestoneInfo objectForKey:@"reference_id"] intValue]) self.btnTeethingAdult8.alpha = 1.0f;
            if([content isEqualToString:@"Lost Tooth"])
            {
                [adultTeethList replaceObjectAtIndex:7 withObject:@(0)];
                self.btnTeethingAdult8.imageView.image = [self.btnTeethingAdult8.imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                self.btnTeethingAdult8.imageView.tintColor = UIColorFromRGB(0x8D8082);
            }
        }
        if(teethId == 29)
        {
            [adultTeethList replaceObjectAtIndex:8 withObject:@(1)];
            [self.btnTeethingAdult9 setImage:[UIImage imageNamed:@"btn_teeth29_selected"] forState:UIControlStateNormal];
            if(teethId == [[milestoneInfo objectForKey:@"reference_id"] intValue]) self.btnTeethingAdult9.alpha = 1.0f;
            if([content isEqualToString:@"Lost Tooth"])
            {
                [adultTeethList replaceObjectAtIndex:8 withObject:@(0)];
                self.btnTeethingAdult9.imageView.image = [self.btnTeethingAdult9.imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                self.btnTeethingAdult9.imageView.tintColor = UIColorFromRGB(0x8D8082);
            }
        }
        if(teethId == 30)
        {
            [adultTeethList replaceObjectAtIndex:9 withObject:@(1)];
            [self.btnTeethingAdult10 setImage:[UIImage imageNamed:@"btn_teeth30_selected"] forState:UIControlStateNormal];
            if(teethId == [[milestoneInfo objectForKey:@"reference_id"] intValue]) self.btnTeethingAdult10.alpha = 1.0f;
            if([content isEqualToString:@"Lost Tooth"])
            {
                [adultTeethList replaceObjectAtIndex:9 withObject:@(0)];
                self.btnTeethingAdult10.imageView.image = [self.btnTeethingAdult10.imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                self.btnTeethingAdult10.imageView.tintColor = UIColorFromRGB(0x8D8082);
            }
        }
        if(teethId == 31)
        {
            [adultTeethList replaceObjectAtIndex:10 withObject:@(1)];
            [self.btnTeethingAdult11 setImage:[UIImage imageNamed:@"btn_teeth31_selected"] forState:UIControlStateNormal];
            if(teethId == [[milestoneInfo objectForKey:@"reference_id"] intValue]) self.btnTeethingAdult11.alpha = 1.0f;
            if([content isEqualToString:@"Lost Tooth"])
            {
                [adultTeethList replaceObjectAtIndex:10 withObject:@(0)];
                self.btnTeethingAdult11.imageView.image = [self.btnTeethingAdult11.imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                self.btnTeethingAdult11.imageView.tintColor = UIColorFromRGB(0x8D8082);
            }
        }
        if(teethId == 32)
        {
            [adultTeethList replaceObjectAtIndex:11 withObject:@(1)];
            [self.btnTeethingAdult12 setImage:[UIImage imageNamed:@"btn_teeth32_selected"] forState:UIControlStateNormal];
            if(teethId == [[milestoneInfo objectForKey:@"reference_id"] intValue]) self.btnTeethingAdult12.alpha = 1.0f;
            if([content isEqualToString:@"Lost Tooth"])
            {
                [adultTeethList replaceObjectAtIndex:11 withObject:@(0)];
                self.btnTeethingAdult12.imageView.image = [self.btnTeethingAdult12.imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                self.btnTeethingAdult12.imageView.tintColor = UIColorFromRGB(0x8D8082);
            }
        }
        if(teethId == 33)
        {
            [adultTeethList replaceObjectAtIndex:12 withObject:@(1)];
            [self.btnTeethingAdult13 setImage:[UIImage imageNamed:@"btn_teeth33_selected"] forState:UIControlStateNormal];
            if(teethId == [[milestoneInfo objectForKey:@"reference_id"] intValue]) self.btnTeethingAdult13.alpha = 1.0f;
            if([content isEqualToString:@"Lost Tooth"])
            {
                [adultTeethList replaceObjectAtIndex:12 withObject:@(0)];
                self.btnTeethingAdult13.imageView.image = [self.btnTeethingAdult13.imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                self.btnTeethingAdult13.imageView.tintColor = UIColorFromRGB(0x8D8082);
            }
        }
        if(teethId == 34)
        {
            [adultTeethList replaceObjectAtIndex:13 withObject:@(1)];
            [self.btnTeethingAdult14 setImage:[UIImage imageNamed:@"btn_teeth34_selected"] forState:UIControlStateNormal];
            if(teethId == [[milestoneInfo objectForKey:@"reference_id"] intValue]) self.btnTeethingAdult14.alpha = 1.0f;
            if([content isEqualToString:@"Lost Tooth"])
            {
                [adultTeethList replaceObjectAtIndex:13 withObject:@(0)];
                self.btnTeethingAdult14.imageView.image = [self.btnTeethingAdult14.imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                self.btnTeethingAdult14.imageView.tintColor = UIColorFromRGB(0x8D8082);
            }
        }
        if(teethId == 35)
        {
            [adultTeethList replaceObjectAtIndex:14 withObject:@(1)];
            [self.btnTeethingAdult15 setImage:[UIImage imageNamed:@"btn_teeth35_selected"] forState:UIControlStateNormal];
            if(teethId == [[milestoneInfo objectForKey:@"reference_id"] intValue]) self.btnTeethingAdult15.alpha = 1.0f;
            if([content isEqualToString:@"Lost Tooth"])
            {
                [adultTeethList replaceObjectAtIndex:14 withObject:@(0)];
                self.btnTeethingAdult15.imageView.image = [self.btnTeethingAdult15.imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                self.btnTeethingAdult15.imageView.tintColor = UIColorFromRGB(0x8D8082);
            }
        }
        if(teethId == 36)
        {
            [adultTeethList replaceObjectAtIndex:15 withObject:@(1)];
            [self.btnTeethingAdult16 setImage:[UIImage imageNamed:@"btn_teeth36_selected"] forState:UIControlStateNormal];
            if(teethId == [[milestoneInfo objectForKey:@"reference_id"] intValue]) self.btnTeethingAdult16.alpha = 1.0f;
            if([content isEqualToString:@"Lost Tooth"])
            {
                [adultTeethList replaceObjectAtIndex:15 withObject:@(0)];
                self.btnTeethingAdult16.imageView.image = [self.btnTeethingAdult16.imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                self.btnTeethingAdult16.imageView.tintColor = UIColorFromRGB(0xBFB5AE);
            }
        }
        if(teethId == 37)
        {
            [adultTeethList replaceObjectAtIndex:16 withObject:@(1)];
            [self.btnTeethingAdult17 setImage:[UIImage imageNamed:@"btn_teeth37_selected"] forState:UIControlStateNormal];
            if(teethId == [[milestoneInfo objectForKey:@"reference_id"] intValue]) self.btnTeethingAdult17.alpha = 1.0f;
            if([content isEqualToString:@"Lost Tooth"])
            {
                [adultTeethList replaceObjectAtIndex:16 withObject:@(0)];
                self.btnTeethingAdult17.imageView.image = [self.btnTeethingAdult17.imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                self.btnTeethingAdult17.imageView.tintColor = UIColorFromRGB(0xBFB5AE);
            }
        }
        if(teethId == 38)
        {
            [adultTeethList replaceObjectAtIndex:17 withObject:@(1)];
            [self.btnTeethingAdult18 setImage:[UIImage imageNamed:@"btn_teeth38_selected"] forState:UIControlStateNormal];
            if(teethId == [[milestoneInfo objectForKey:@"reference_id"] intValue]) self.btnTeethingAdult18.alpha = 1.0f;
            if([content isEqualToString:@"Lost Tooth"])
            {
                [adultTeethList replaceObjectAtIndex:17 withObject:@(0)];
                self.btnTeethingAdult18.imageView.image = [self.btnTeethingAdult18.imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                self.btnTeethingAdult18.imageView.tintColor = UIColorFromRGB(0xBFB5AE);
            }
        }
        if(teethId == 39)
        {
            [adultTeethList replaceObjectAtIndex:18 withObject:@(1)];
            [self.btnTeethingAdult19 setImage:[UIImage imageNamed:@"btn_teeth39_selected"] forState:UIControlStateNormal];
            if(teethId == [[milestoneInfo objectForKey:@"reference_id"] intValue]) self.btnTeethingAdult19.alpha = 1.0f;
            if([content isEqualToString:@"Lost Tooth"])
            {
                [adultTeethList replaceObjectAtIndex:18 withObject:@(0)];
                self.btnTeethingAdult19.imageView.image = [self.btnTeethingAdult19.imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                self.btnTeethingAdult19.imageView.tintColor = UIColorFromRGB(0xBFB5AE);
            }
        }
        if(teethId == 40)
        {
            [adultTeethList replaceObjectAtIndex:19 withObject:@(1)];
            [self.btnTeethingAdult20 setImage:[UIImage imageNamed:@"btn_teeth40_selected"] forState:UIControlStateNormal];
            if(teethId == [[milestoneInfo objectForKey:@"reference_id"] intValue]) self.btnTeethingAdult20.alpha = 1.0f;
            if([content isEqualToString:@"Lost Tooth"])
            {
                [adultTeethList replaceObjectAtIndex:19 withObject:@(0)];
                self.btnTeethingAdult20.imageView.image = [self.btnTeethingAdult20.imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                self.btnTeethingAdult20.imageView.tintColor = UIColorFromRGB(0xBFB5AE);
            }
        }
        if(teethId == 41)
        {
            [adultTeethList replaceObjectAtIndex:20 withObject:@(1)];
            [self.btnTeethingAdult21 setImage:[UIImage imageNamed:@"btn_teeth41_selected"] forState:UIControlStateNormal];
            if(teethId == [[milestoneInfo objectForKey:@"reference_id"] intValue]) self.btnTeethingAdult21.alpha = 1.0f;
            if([content isEqualToString:@"Lost Tooth"])
            {
                [adultTeethList replaceObjectAtIndex:20 withObject:@(0)];
                self.btnTeethingAdult21.imageView.image = [self.btnTeethingAdult21.imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                self.btnTeethingAdult21.imageView.tintColor = UIColorFromRGB(0xBFB5AE);
            }
        }
        if(teethId == 42)
        {
            [adultTeethList replaceObjectAtIndex:21 withObject:@(1)];
            [self.btnTeethingAdult22 setImage:[UIImage imageNamed:@"btn_teeth42_selected"] forState:UIControlStateNormal];
            if(teethId == [[milestoneInfo objectForKey:@"reference_id"] intValue]) self.btnTeethingAdult22.alpha = 1.0f;
            if([content isEqualToString:@"Lost Tooth"])
            {
                [adultTeethList replaceObjectAtIndex:21 withObject:@(0)];
                self.btnTeethingAdult22.imageView.image = [self.btnTeethingAdult22.imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                self.btnTeethingAdult22.imageView.tintColor = UIColorFromRGB(0xBFB5AE);
            }
        }
        if(teethId == 43)
        {
            [adultTeethList replaceObjectAtIndex:22 withObject:@(1)];
            [self.btnTeethingAdult23 setImage:[UIImage imageNamed:@"btn_teeth43_selected"] forState:UIControlStateNormal];
            if(teethId == [[milestoneInfo objectForKey:@"reference_id"] intValue]) self.btnTeethingAdult23.alpha = 1.0f;
            if([content isEqualToString:@"Lost Tooth"])
            {
                [adultTeethList replaceObjectAtIndex:22 withObject:@(0)];
                self.btnTeethingAdult23.imageView.image = [self.btnTeethingAdult23.imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                self.btnTeethingAdult23.imageView.tintColor = UIColorFromRGB(0xBFB5AE);
            }
        }
        if(teethId == 44)
        {
            [adultTeethList replaceObjectAtIndex:23 withObject:@(1)];
            [self.btnTeethingAdult24 setImage:[UIImage imageNamed:@"btn_teeth44_selected"] forState:UIControlStateNormal];
            if(teethId == [[milestoneInfo objectForKey:@"reference_id"] intValue]) self.btnTeethingAdult24.alpha = 1.0f;
            if([content isEqualToString:@"Lost Tooth"])
            {
                [adultTeethList replaceObjectAtIndex:23 withObject:@(0)];
                self.btnTeethingAdult24.imageView.image = [self.btnTeethingAdult24.imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                self.btnTeethingAdult24.imageView.tintColor = UIColorFromRGB(0xBFB5AE);
            }
        }
        if(teethId == 45)
        {
            [adultTeethList replaceObjectAtIndex:24 withObject:@(1)];
            [self.btnTeethingAdult25 setImage:[UIImage imageNamed:@"btn_teeth45_selected"] forState:UIControlStateNormal];
            if(teethId == [[milestoneInfo objectForKey:@"reference_id"] intValue]) self.btnTeethingAdult25.alpha = 1.0f;
            if([content isEqualToString:@"Lost Tooth"])
            {
                [adultTeethList replaceObjectAtIndex:24 withObject:@(0)];
                self.btnTeethingAdult25.imageView.image = [self.btnTeethingAdult25.imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                self.btnTeethingAdult25.imageView.tintColor = UIColorFromRGB(0xBFB5AE);
            }
        }
        if(teethId == 46)
        {
            [adultTeethList replaceObjectAtIndex:25 withObject:@(1)];
            [self.btnTeethingAdult26 setImage:[UIImage imageNamed:@"btn_teeth46_selected"] forState:UIControlStateNormal];
            if(teethId == [[milestoneInfo objectForKey:@"reference_id"] intValue]) self.btnTeethingAdult26.alpha = 1.0f;
            if([content isEqualToString:@"Lost Tooth"])
            {
                [adultTeethList replaceObjectAtIndex:25 withObject:@(0)];
                self.btnTeethingAdult26.imageView.image = [self.btnTeethingAdult26.imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                self.btnTeethingAdult26.imageView.tintColor = UIColorFromRGB(0xBFB5AE);
            }
        }
        if(teethId == 47)
        {
            [adultTeethList replaceObjectAtIndex:26 withObject:@(1)];
            [self.btnTeethingAdult27 setImage:[UIImage imageNamed:@"btn_teeth47_selected"] forState:UIControlStateNormal];
            if(teethId == [[milestoneInfo objectForKey:@"reference_id"] intValue]) self.btnTeethingAdult27.alpha = 1.0f;
            if([content isEqualToString:@"Lost Tooth"])
            {
                [adultTeethList replaceObjectAtIndex:26 withObject:@(0)];
                self.btnTeethingAdult27.imageView.image = [self.btnTeethingAdult27.imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                self.btnTeethingAdult27.imageView.tintColor = UIColorFromRGB(0xBFB5AE);
            }
        }
        if(teethId == 48)
        {
            [adultTeethList replaceObjectAtIndex:27 withObject:@(1)];
            [self.btnTeethingAdult28 setImage:[UIImage imageNamed:@"btn_teeth48_selected"] forState:UIControlStateNormal];
            if(teethId == [[milestoneInfo objectForKey:@"reference_id"] intValue]) self.btnTeethingAdult28.alpha = 1.0f;
            if([content isEqualToString:@"Lost Tooth"])
            {
                [adultTeethList replaceObjectAtIndex:27 withObject:@(0)];
                self.btnTeethingAdult28.imageView.image = [self.btnTeethingAdult28.imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                self.btnTeethingAdult28.imageView.tintColor = UIColorFromRGB(0xBFB5AE);
            }
        }
        if(teethId == 49)
        {
            [adultTeethList replaceObjectAtIndex:28 withObject:@(1)];
            [self.btnTeethingAdult29 setImage:[UIImage imageNamed:@"btn_teeth49_selected"] forState:UIControlStateNormal];
            if(teethId == [[milestoneInfo objectForKey:@"reference_id"] intValue]) self.btnTeethingAdult29.alpha = 1.0f;
            if([content isEqualToString:@"Lost Tooth"])
            {
                [adultTeethList replaceObjectAtIndex:28 withObject:@(0)];
                self.btnTeethingAdult29.imageView.image = [self.btnTeethingAdult29.imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                self.btnTeethingAdult29.imageView.tintColor = UIColorFromRGB(0xBFB5AE);
            }
        }
        if(teethId == 50)
        {
            [adultTeethList replaceObjectAtIndex:29 withObject:@(1)];
            [self.btnTeethingAdult30 setImage:[UIImage imageNamed:@"btn_teeth50_selected"] forState:UIControlStateNormal];
            if(teethId == [[milestoneInfo objectForKey:@"reference_id"] intValue]) self.btnTeethingAdult30.alpha = 1.0f;
            if([content isEqualToString:@"Lost Tooth"])
            {
                [adultTeethList replaceObjectAtIndex:29 withObject:@(0)];
                self.btnTeethingAdult30.imageView.image = [self.btnTeethingAdult30.imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                self.btnTeethingAdult30.imageView.tintColor = UIColorFromRGB(0xBFB5AE);
            }
        }
        if(teethId == 51)
        {
            [adultTeethList replaceObjectAtIndex:30 withObject:@(1)];
            [self.btnTeethingAdult31 setImage:[UIImage imageNamed:@"btn_teeth51_selected"] forState:UIControlStateNormal];
            if(teethId == [[milestoneInfo objectForKey:@"reference_id"] intValue]) self.btnTeethingAdult31.alpha = 1.0f;
            if([content isEqualToString:@"Lost Tooth"])
            {
                [adultTeethList replaceObjectAtIndex:22 withObject:@(0)];
                self.btnTeethingAdult31.imageView.image = [self.btnTeethingAdult31.imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                self.btnTeethingAdult31.imageView.tintColor = UIColorFromRGB(0xBFB5AE);
            }
        }
        if(teethId == 52)
        {
            [adultTeethList replaceObjectAtIndex:31 withObject:@(1)];
            [self.btnTeethingAdult32 setImage:[UIImage imageNamed:@"btn_teeth52_selected"] forState:UIControlStateNormal];
            if(teethId == [[milestoneInfo objectForKey:@"reference_id"] intValue]) self.btnTeethingAdult32.alpha = 1.0f;
            if([content isEqualToString:@"Lost Tooth"])
            {
                [adultTeethList replaceObjectAtIndex:31 withObject:@(0)];
                self.btnTeethingAdult32.imageView.image = [self.btnTeethingAdult32.imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                self.btnTeethingAdult32.imageView.tintColor = UIColorFromRGB(0xBFB5AE);
            }
        }
    }
}

-(void)setupBabyTeeth:(NSMutableArray *)babyTeethArray
{
    if ([babyTeethArray count]>0)
    {
        for(int i=0; i<[babyTeethArray count]; i++)
        {
            int teethId = [[[babyTeethArray objectAtIndex:i] objectForKey:@"teeth_id"] intValue];
            NSString *content = [[babyTeethArray objectAtIndex:i] objectForKey:@"content"];
            if(teethId == 1)
            {
                [babyTeethList replaceObjectAtIndex:0 withObject:@(1)];
                [self.btnTeethingBaby1 setImage:[UIImage imageNamed:@"btn_teeth1_selected"] forState:UIControlStateNormal];
                if(teethId == [[milestoneInfo objectForKey:@"reference_id"] intValue]) self.btnTeethingBaby1.alpha = 1.0f;
                if([content isEqualToString:@"Lost Tooth"])
                {
                    [babyTeethList replaceObjectAtIndex:0 withObject:@(0)];
                    self.btnTeethingBaby1.imageView.image = [self.btnTeethingBaby1.imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                    self.btnTeethingBaby1.imageView.tintColor = UIColorFromRGB(0x8D8082);
                }
            }
            if(teethId == 2)
            {
                [babyTeethList replaceObjectAtIndex:1 withObject:@(1)];
                [self.btnTeethingBaby2 setImage:[UIImage imageNamed:@"btn_teeth2_selected"] forState:UIControlStateNormal];
                if(teethId == [[milestoneInfo objectForKey:@"reference_id"] intValue]) self.btnTeethingBaby2.alpha = 1.0f;
                if([content isEqualToString:@"Lost Tooth"])
                {
                    [babyTeethList replaceObjectAtIndex:1 withObject:@(0)];
                    self.btnTeethingBaby2.imageView.image = [self.btnTeethingBaby2.imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                    self.btnTeethingBaby2.imageView.tintColor = UIColorFromRGB(0x8D8082);
                }
            }
            if(teethId == 3)
            {
                [babyTeethList replaceObjectAtIndex:2 withObject:@(1)];
                [self.btnTeethingBaby3 setImage:[UIImage imageNamed:@"btn_teeth3_selected"] forState:UIControlStateNormal];
                if(teethId == [[milestoneInfo objectForKey:@"reference_id"] intValue]) self.btnTeethingBaby3.alpha = 1.0f;
                if([content isEqualToString:@"Lost Tooth"])
                {
                    [babyTeethList replaceObjectAtIndex:2 withObject:@(0)];
                    self.btnTeethingBaby3.imageView.image = [self.btnTeethingBaby3.imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                    self.btnTeethingBaby3.imageView.tintColor = UIColorFromRGB(0x8D8082);
                }
            }
            if(teethId == 4)
            {
                [babyTeethList replaceObjectAtIndex:3 withObject:@(1)];
                [self.btnTeethingBaby4 setImage:[UIImage imageNamed:@"btn_teeth4_selected"] forState:UIControlStateNormal];
                if(teethId == [[milestoneInfo objectForKey:@"reference_id"] intValue]) self.btnTeethingBaby4.alpha = 1.0f;
                if([content isEqualToString:@"Lost Tooth"])
                {
                    [babyTeethList replaceObjectAtIndex:3 withObject:@(0)];
                    self.btnTeethingBaby4.imageView.image = [self.btnTeethingBaby4.imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                    self.btnTeethingBaby4.imageView.tintColor = UIColorFromRGB(0x8D8082);
                }
            }
            if(teethId == 5)
            {
                [babyTeethList replaceObjectAtIndex:4 withObject:@(1)];
                [self.btnTeethingBaby5 setImage:[UIImage imageNamed:@"btn_teeth5_selected"] forState:UIControlStateNormal];
                if(teethId == [[milestoneInfo objectForKey:@"reference_id"] intValue]) self.btnTeethingBaby5.alpha = 1.0f;
                if([content isEqualToString:@"Lost Tooth"])
                {
                    [babyTeethList replaceObjectAtIndex:4 withObject:@(0)];
                    self.btnTeethingBaby5.imageView.image = [self.btnTeethingBaby5.imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                    self.btnTeethingBaby5.imageView.tintColor = UIColorFromRGB(0x8D8082);
                }
            }
            if(teethId == 6)
            {
                [babyTeethList replaceObjectAtIndex:5 withObject:@(1)];
                [self.btnTeethingBaby6 setImage:[UIImage imageNamed:@"btn_teeth6_selected"] forState:UIControlStateNormal];
                if(teethId == [[milestoneInfo objectForKey:@"reference_id"] intValue]) self.btnTeethingBaby6.alpha = 1.0f;
                if([content isEqualToString:@"Lost Tooth"])
                {
                    [babyTeethList replaceObjectAtIndex:5 withObject:@(0)];
                    self.btnTeethingBaby6.imageView.image = [self.btnTeethingBaby6.imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                    self.btnTeethingBaby6.imageView.tintColor = UIColorFromRGB(0x8D8082);
                }
            }
            if(teethId == 7)
            {
                [babyTeethList replaceObjectAtIndex:6 withObject:@(1)];
                [self.btnTeethingBaby7 setImage:[UIImage imageNamed:@"btn_teeth7_selected"] forState:UIControlStateNormal];
                if(teethId == [[milestoneInfo objectForKey:@"reference_id"] intValue]) self.btnTeethingBaby7.alpha = 1.0f;
                if([content isEqualToString:@"Lost Tooth"])
                {
                    [babyTeethList replaceObjectAtIndex:6 withObject:@(0)];
                    self.btnTeethingBaby7.imageView.image = [self.btnTeethingBaby7.imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                    self.btnTeethingBaby7.imageView.tintColor = UIColorFromRGB(0xBFB5AE);
                }
            }
            if(teethId == 8)
            {
                [babyTeethList replaceObjectAtIndex:7 withObject:@(1)];
                [self.btnTeethingBaby8 setImage:[UIImage imageNamed:@"btn_teeth8_selected"] forState:UIControlStateNormal];
                if(teethId == [[milestoneInfo objectForKey:@"reference_id"] intValue]) self.btnTeethingBaby8.alpha = 1.0f;
                if([content isEqualToString:@"Lost Tooth"])
                {
                    [babyTeethList replaceObjectAtIndex:7 withObject:@(0)];
                    self.btnTeethingBaby8.imageView.image = [self.btnTeethingBaby8.imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                    self.btnTeethingBaby8.imageView.tintColor = UIColorFromRGB(0x8D8082);
                }
            }
            if(teethId == 9)
            {
                [babyTeethList replaceObjectAtIndex:8 withObject:@(1)];
                [self.btnTeethingBaby9 setImage:[UIImage imageNamed:@"btn_teeth9_selected"] forState:UIControlStateNormal];
                if(teethId == [[milestoneInfo objectForKey:@"reference_id"] intValue]) self.btnTeethingBaby9.alpha = 1.0f;
                if([content isEqualToString:@"Lost Tooth"])
                {
                    [babyTeethList replaceObjectAtIndex:8 withObject:@(0)];
                    self.btnTeethingBaby9.imageView.image = [self.btnTeethingBaby9.imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                    self.btnTeethingBaby9.imageView.tintColor = UIColorFromRGB(0x8D8082);
                }
            }
            if(teethId == 10)
            {
                [babyTeethList replaceObjectAtIndex:9 withObject:@(1)];
                [self.btnTeethingBaby10 setImage:[UIImage imageNamed:@"btn_teeth10_selected"] forState:UIControlStateNormal];
                if(teethId == [[milestoneInfo objectForKey:@"reference_id"] intValue]) self.btnTeethingBaby10.alpha = 1.0f;
                if([content isEqualToString:@"Lost Tooth"])
                {
                    [babyTeethList replaceObjectAtIndex:9 withObject:@(0)];
                    self.btnTeethingBaby10.imageView.image = [self.btnTeethingBaby10.imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                    self.btnTeethingBaby10.imageView.tintColor = UIColorFromRGB(0x8D8082);
                }
            }
            if(teethId == 11)
            {
                [babyTeethList replaceObjectAtIndex:10 withObject:@(1)];
                [self.btnTeethingBaby11 setImage:[UIImage imageNamed:@"btn_teeth11_selected"] forState:UIControlStateNormal];
                if(teethId == [[milestoneInfo objectForKey:@"reference_id"] intValue]) self.btnTeethingBaby11.alpha = 1.0f;
                if([content isEqualToString:@"Lost Tooth"])
                {
                    [babyTeethList replaceObjectAtIndex:10 withObject:@(0)];
                    self.btnTeethingBaby11.imageView.image = [self.btnTeethingBaby11.imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                    self.btnTeethingBaby11.imageView.tintColor = UIColorFromRGB(0x8D8082);
                }
            }
            if(teethId == 12)
            {
                [babyTeethList replaceObjectAtIndex:11 withObject:@(1)];
                [self.btnTeethingBaby12 setImage:[UIImage imageNamed:@"btn_teeth12_selected"] forState:UIControlStateNormal];
                if(teethId == [[milestoneInfo objectForKey:@"reference_id"] intValue]) self.btnTeethingBaby12.alpha = 1.0f;
                if([content isEqualToString:@"Lost Tooth"])
                {
                    [babyTeethList replaceObjectAtIndex:11 withObject:@(0)];
                    self.btnTeethingBaby12.imageView.image = [self.btnTeethingBaby12.imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                    self.btnTeethingBaby12.imageView.tintColor = UIColorFromRGB(0x8D8082);
                }
            }
            if(teethId == 13)
            {
                [babyTeethList replaceObjectAtIndex:12 withObject:@(1)];
                [self.btnTeethingBaby13 setImage:[UIImage imageNamed:@"btn_teeth13_selected"] forState:UIControlStateNormal];
                if(teethId == [[milestoneInfo objectForKey:@"reference_id"] intValue]) self.btnTeethingBaby13.alpha = 1.0f;
                if([content isEqualToString:@"Lost Tooth"])
                {
                    [babyTeethList replaceObjectAtIndex:12 withObject:@(0)];
                    self.btnTeethingBaby13.imageView.image = [self.btnTeethingBaby13.imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                    self.btnTeethingBaby13.imageView.tintColor = UIColorFromRGB(0x8D8082);
                }
            }
            if(teethId == 14)
            {
                [babyTeethList replaceObjectAtIndex:13 withObject:@(1)];
                [self.btnTeethingBaby14 setImage:[UIImage imageNamed:@"btn_teeth14_selected"] forState:UIControlStateNormal];
                if(teethId == [[milestoneInfo objectForKey:@"reference_id"] intValue]) self.btnTeethingBaby14.alpha = 1.0f;
                if([content isEqualToString:@"Lost Tooth"])
                {
                    [babyTeethList replaceObjectAtIndex:13 withObject:@(0)];
                    self.btnTeethingBaby14.imageView.image = [self.btnTeethingBaby14.imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                    self.btnTeethingBaby14.imageView.tintColor = UIColorFromRGB(0x8D8082);
                }
            }
            if(teethId == 15)
            {
                [babyTeethList replaceObjectAtIndex:14 withObject:@(1)];
                [self.btnTeethingBaby15 setImage:[UIImage imageNamed:@"btn_teeth15_selected"] forState:UIControlStateNormal];
                if(teethId == [[milestoneInfo objectForKey:@"reference_id"] intValue]) self.btnTeethingBaby15.alpha = 1.0f;
                if([content isEqualToString:@"Lost Tooth"])
                {
                    [babyTeethList replaceObjectAtIndex:14 withObject:@(0)];
                    self.btnTeethingBaby15.imageView.image = [self.btnTeethingBaby15.imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                    self.btnTeethingBaby15.imageView.tintColor = UIColorFromRGB(0x8D8082);
                }
            }
            if(teethId == 16)
            {
                [babyTeethList replaceObjectAtIndex:15 withObject:@(1)];
                [self.btnTeethingBaby16 setImage:[UIImage imageNamed:@"btn_teeth16_selected"] forState:UIControlStateNormal];
                if(teethId == [[milestoneInfo objectForKey:@"reference_id"] intValue]) self.btnTeethingBaby16.alpha = 1.0f;
                if([content isEqualToString:@"Lost Tooth"])
                {
                    [babyTeethList replaceObjectAtIndex:15 withObject:@(0)];
                    self.btnTeethingBaby16.imageView.image = [self.btnTeethingBaby16.imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                    self.btnTeethingBaby16.imageView.tintColor = UIColorFromRGB(0xBFB5AE);
                }
            }
            if(teethId == 17)
            {
                [babyTeethList replaceObjectAtIndex:16 withObject:@(1)];
                [self.btnTeethingBaby17 setImage:[UIImage imageNamed:@"btn_teeth17_selected"] forState:UIControlStateNormal];
                if(teethId == [[milestoneInfo objectForKey:@"reference_id"] intValue]) self.btnTeethingBaby17.alpha = 1.0f;
                if([content isEqualToString:@"Lost Tooth"])
                {
                    [babyTeethList replaceObjectAtIndex:16 withObject:@(0)];
                    self.btnTeethingBaby17.imageView.image = [self.btnTeethingBaby17.imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                    self.btnTeethingBaby17.imageView.tintColor = UIColorFromRGB(0xBFB5AE);
                }
            }
            if(teethId == 18)
            {
                [babyTeethList replaceObjectAtIndex:17 withObject:@(1)];
                [self.btnTeethingBaby18 setImage:[UIImage imageNamed:@"btn_teeth18_selected"] forState:UIControlStateNormal];
                if(teethId == [[milestoneInfo objectForKey:@"reference_id"] intValue]) self.btnTeethingBaby18.alpha = 1.0f;
                if([content isEqualToString:@"Lost Tooth"])
                {
                    [babyTeethList replaceObjectAtIndex:17 withObject:@(0)];
                    self.btnTeethingBaby18.imageView.image = [self.btnTeethingBaby18.imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                    self.btnTeethingBaby18.imageView.tintColor = UIColorFromRGB(0xBFB5AE);
                }
            }
            if(teethId == 19)
            {
                [babyTeethList replaceObjectAtIndex:18 withObject:@(1)];
                [self.btnTeethingBaby19 setImage:[UIImage imageNamed:@"btn_teeth19_selected"] forState:UIControlStateNormal];
                if(teethId == [[milestoneInfo objectForKey:@"reference_id"] intValue]) self.btnTeethingBaby19.alpha = 1.0f;
                if([content isEqualToString:@"Lost Tooth"])
                {
                    [babyTeethList replaceObjectAtIndex:18 withObject:@(0)];
                    self.btnTeethingBaby19.imageView.image = [self.btnTeethingBaby19.imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                    self.btnTeethingBaby19.imageView.tintColor = UIColorFromRGB(0xBFB5AE);
                }
            }
            if(teethId == 20)
            {
                [babyTeethList replaceObjectAtIndex:19 withObject:@(1)];
                [self.btnTeethingBaby20 setImage:[UIImage imageNamed:@"btn_teeth20_selected"] forState:UIControlStateNormal];
                if(teethId == [[milestoneInfo objectForKey:@"reference_id"] intValue]) self.btnTeethingBaby20.alpha = 1.0f;
                if([content isEqualToString:@"Lost Tooth"])
                {
                    [babyTeethList replaceObjectAtIndex:20 withObject:@(0)];
                    self.btnTeethingBaby20.imageView.image = [self.btnTeethingBaby20.imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                    self.btnTeethingBaby20.imageView.tintColor = UIColorFromRGB(0xBFB5AE);
                }
            }
        }
    }
}


@end
