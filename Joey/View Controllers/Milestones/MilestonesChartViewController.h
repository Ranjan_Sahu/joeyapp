//
//  MilestonesChartViewController.h
//  Joey
//
//  Created by csl on 8/6/2017.
//  Copyright © 2017 Auxilia. All rights reserved.
//

#import "BaseViewController.h"
@import Charts;

@interface MilestonesChartViewController : BaseViewController

@property (nonatomic, weak) IBOutlet UILabel *txtChartName;
@property (nonatomic, weak) IBOutlet UILabel *txtLeftAxis;
@property (nonatomic, weak) IBOutlet UILabel *txtBottomAxis;
@property (nonatomic, weak) IBOutlet LineChartView *chartView;

@end
