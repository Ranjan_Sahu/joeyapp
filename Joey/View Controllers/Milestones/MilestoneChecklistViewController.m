//
//  MilestoneChecklistViewController.m
//  Joey
//
//  Created by csl on 9/6/2017.
//  Copyright © 2017 Auxilia. All rights reserved.
//

#import "AppDelegate.h"
#import "MilestoneChecklistViewController.h"
#import "MilestonesChecklistTableViewCell.h"

@interface MilestoneChecklistViewController ()
{
    NSUserDefaults *shareObject;
    AppDelegate *appDelegate;
    NSMutableArray *milestoneList;
    
    BOOL saveCheck;
    int selectedMonth;
}
@end

@implementation MilestoneChecklistViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    shareObject = [NSUserDefaults standardUserDefaults];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    milestoneList = [[NSMutableArray alloc] init];
    
    [self.navigationController setNavigationBarHidden:NO];
    [self.navigationController.navigationBar setBarTintColor:UIColorFromRGB(0xEEE8E4)];
    [self.navigationController.navigationBar setTranslucent:NO];
    [self.navigationItem setTitle:LocalizedString(@"title_checklist", nil)];
    [self customBackButton];
    
    //Anil
    self.btnTrimester1.layer.cornerRadius = 3.0f;
    self.btnTrimester2.layer.cornerRadius = 3.0f;
    self.btnTrimester3.layer.cornerRadius = 3.0f;
    
    self.btnMonth1.layer.cornerRadius = 3.f;
    self.btnMonth2.layer.cornerRadius = 3.f;
    self.btnMonth3.layer.cornerRadius = 3.f;
    self.btnMonth4.layer.cornerRadius = 3.f;
    self.btnMonth5.layer.cornerRadius = 3.f;
    self.btnMonth6.layer.cornerRadius = 3.f;
    self.btnMonth7.layer.cornerRadius = 3.f;
    self.btnMonth8.layer.cornerRadius = 3.f;
    self.btnMonth9.layer.cornerRadius = 3.f;
    self.btnMonth10.layer.cornerRadius = 3.f;
    self.btnMonth11.layer.cornerRadius = 3.f;
    self.btnMonth12.layer.cornerRadius = 3.f;
    self.btnMonth13.layer.cornerRadius = 3.f;
    self.btnMonth14.layer.cornerRadius = 3.f;
    
    // Anil
    [self.btnTrimester1 setTitle:LocalizedString(@"txt_trimester_1", nil) forState:UIControlStateNormal];
    [self.btnTrimester2 setTitle:LocalizedString(@"txt_trimester_2", nil) forState:UIControlStateNormal];
    [self.btnTrimester3 setTitle:LocalizedString(@"txt_trimester_3", nil) forState:UIControlStateNormal];
    
    [self.btnMonth1 setTitle:LocalizedString(@"txt_milestone_1_months", nil) forState:UIControlStateNormal];
    [self.btnMonth2 setTitle:LocalizedString(@"txt_milestone_2_months", nil) forState:UIControlStateNormal];
    [self.btnMonth3 setTitle:LocalizedString(@"txt_milestone_3_months", nil) forState:UIControlStateNormal];
    [self.btnMonth4 setTitle:LocalizedString(@"txt_milestone_4_months", nil) forState:UIControlStateNormal];
    [self.btnMonth5 setTitle:LocalizedString(@"txt_milestone_5_6_months", nil) forState:UIControlStateNormal];
    [self.btnMonth6 setTitle:LocalizedString(@"txt_milestone_7_9_months", nil) forState:UIControlStateNormal];
    [self.btnMonth7 setTitle:LocalizedString(@"txt_milestone_10_12_months", nil) forState:UIControlStateNormal];
    [self.btnMonth8 setTitle:LocalizedString(@"txt_milestone_13_15_months", nil) forState:UIControlStateNormal];
    [self.btnMonth9 setTitle:LocalizedString(@"txt_milestone_16_18_months", nil) forState:UIControlStateNormal];
    [self.btnMonth10 setTitle:LocalizedString(@"txt_milestone_19_24_months", nil) forState:UIControlStateNormal];
    [self.btnMonth11 setTitle:LocalizedString(@"txt_milestone_25_36_months", nil) forState:UIControlStateNormal];
    [self.btnMonth12 setTitle:LocalizedString(@"txt_milestone_37_48_months", nil) forState:UIControlStateNormal];
    [self.btnMonth13 setTitle:LocalizedString(@"txt_milestone_49_60_months", nil) forState:UIControlStateNormal];
    [self.btnMonth14 setTitle:LocalizedString(@"txt_milestone_61_84_months", nil) forState:UIControlStateNormal];
    
    if (IS_IPAD) {
        //Anil
        [self.btnTrimester1.titleLabel setFont:[UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f]];
        [self.btnTrimester2.titleLabel setFont:[UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f]];
        [self.btnTrimester3.titleLabel setFont:[UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f]];
        
        [self.btnMonth1.titleLabel setFont:[UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f]];
        [self.btnMonth2.titleLabel setFont:[UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f]];
        [self.btnMonth3.titleLabel setFont:[UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f]];
        [self.btnMonth4.titleLabel setFont:[UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f]];
        [self.btnMonth5.titleLabel setFont:[UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f]];
        [self.btnMonth6.titleLabel setFont:[UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f]];
        [self.btnMonth7.titleLabel setFont:[UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f]];
        [self.btnMonth8.titleLabel setFont:[UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f]];
        [self.btnMonth9.titleLabel setFont:[UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f]];
        [self.btnMonth10.titleLabel setFont:[UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f]];
        [self.btnMonth11.titleLabel setFont:[UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f]];
        [self.btnMonth12.titleLabel setFont:[UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f]];
        [self.btnMonth13.titleLabel setFont:[UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f]];
        [self.btnMonth14.titleLabel setFont:[UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f]];
    }
    
    [self.tableView registerNib:[UINib nibWithNibName:@"MilestonesChecklistTableViewCell" bundle:nil] forCellReuseIdentifier:@"MilestonesChecklistTableViewCell"];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;

    NSLog(@"self.data=%@", self.data);
    if([self.data objectForKey:@"month"])
    {
        int month = [[self.data objectForKey:@"month"] intValue];
        //Anil
       if(month == -1) [self.btnTrimester1 sendActionsForControlEvents:UIControlEventTouchUpInside];
        if(month == -2) [self.btnTrimester2 sendActionsForControlEvents:UIControlEventTouchUpInside];
        if(month == -3) [self.btnTrimester3 sendActionsForControlEvents:UIControlEventTouchUpInside];
        
       else if(month == 0 || month == 1) [self.btnMonth1 sendActionsForControlEvents:UIControlEventTouchUpInside];
        else if(month == 2) [self.btnMonth2 sendActionsForControlEvents:UIControlEventTouchUpInside];
        else if(month == 3) [self.btnMonth3 sendActionsForControlEvents:UIControlEventTouchUpInside];
        else if(month == 4) [self.btnMonth4 sendActionsForControlEvents:UIControlEventTouchUpInside];
        else if(month <= 6) [self.btnMonth5 sendActionsForControlEvents:UIControlEventTouchUpInside];
        else if(month <= 9) [self.btnMonth6 sendActionsForControlEvents:UIControlEventTouchUpInside];
        else if(month <= 12) [self.btnMonth7 sendActionsForControlEvents:UIControlEventTouchUpInside];
        else if(month <= 15) [self.btnMonth8 sendActionsForControlEvents:UIControlEventTouchUpInside];
        else if(month <= 18) [self.btnMonth9 sendActionsForControlEvents:UIControlEventTouchUpInside];
        else if(month <= 24) [self.btnMonth10 sendActionsForControlEvents:UIControlEventTouchUpInside];
        else if(month <= 36) [self.btnMonth11 sendActionsForControlEvents:UIControlEventTouchUpInside];
        else if(month <= 48) [self.btnMonth12 sendActionsForControlEvents:UIControlEventTouchUpInside];
        else if(month <= 60) [self.btnMonth13 sendActionsForControlEvents:UIControlEventTouchUpInside];
        else if(month > 60) [self.btnMonth14 sendActionsForControlEvents:UIControlEventTouchUpInside];
    }
    
    [self.view layoutIfNeeded];
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.scrollView.contentSize = CGSizeMake(((self.btnTrimester1.frame.size.width+10)*18 + 10), 1);
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:(BOOL)animated];
    [self.scrollView layoutIfNeeded];
    [appDelegate.tracker send:[[[GAIDictionaryBuilder createScreenView] set:@"Development Calendar" forKey:kGAIScreenName] build]];
    
    NSLog(@"here=%@", self.data);
    NSLog(@"appDelegate.updateMilestonesCheck=%@", appDelegate.updateMilestonesCheck?@"YES":@"NO");
    if([[self.data objectForKey:@"save_check"] boolValue] || appDelegate.updateMilestonesCheck)
    {
        saveCheck = YES;
        [self getMilestoneList];
    }
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    //self.scrollView.contentSize = CGSizeMake(((self.btnTrimester1.frame.size.width+10)*18 + 10), 1);
    
    if([self.data objectForKey:@"month"])
    {
        int month = [[self.data objectForKey:@"month"] intValue];
        
        if (month == -1 || month == -2 || month == -3)self.scrollView.contentOffset = CGPointMake(0, self.scrollView.contentOffset.y);
        else if(month <= 3)self.scrollView.contentOffset = CGPointMake(4*self.btnTrimester1.frame.size.width+30, self.scrollView.contentOffset.y);
        else if(month <= 9) self.scrollView.contentOffset = CGPointMake(7*self.btnTrimester1.frame.size.width+40, self.scrollView.contentOffset.y);
        else if(month <= 18) self.scrollView.contentOffset = CGPointMake(10*self.btnTrimester1.frame.size.width+70, self.scrollView.contentOffset.y);
        else if(month <= 48) self.scrollView.contentOffset = CGPointMake(13*self.btnTrimester1.frame.size.width+100, self.scrollView.contentOffset.y);
        else if(month > 48) self.scrollView.contentOffset = CGPointMake(16*self.btnTrimester1.frame.size.width+130, self.scrollView.contentOffset.y);
    }
    
}

- (void)customBackButton
{
    if([self.navigationController.viewControllers count] > 1)
    {
        UIImage *imgBack = [UIImage imageNamed:@"nav_btn_back"];
        UIButton *btnBack = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnBack setImage:imgBack forState:UIControlStateNormal];
        btnBack.frame = CGRectMake(0, 0, imgBack.size.width, imgBack.size.height);
        [btnBack addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
        [btnBack setImageEdgeInsets:UIEdgeInsetsMake(0, -16, 0, 16)];
        UIBarButtonItem *backBarItem = [[UIBarButtonItem alloc] initWithCustomView:btnBack];
        self.navigationItem.leftBarButtonItem = backBarItem;
    }
}

- (void)backAction
{
    UIViewController *viewController = [self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count-2];
    viewController.data = [[NSMutableDictionary alloc] initWithDictionary:@{@"save_check":@(saveCheck)}];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)getMilestoneList
{
    [SVProgressHUD show];
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setValue:@([shareObject integerForKey:@"user_id"]) forKey:@"user_id"];
    [parameters setValue:@([shareObject integerForKey:@"baby_id"]) forKey:@"baby_id"];
    [parameters setValue:@(selectedMonth) forKey:@"month"];
    [[WebServiceManager sharedManager] initWithBaseURL:@"api/getDevelopmentCalendarList" parameters:parameters success:^(id result) {
        milestoneList = (NSMutableArray *)[result valueForKey:@"list"];
        [self.tableView reloadData];
    } failure:^(NSError *error) {
    }];
}


-(IBAction)getTrimester1ListAction:(UIButton *)sender {
    
    selectedMonth = -1;
    [self.btnTrimester1 setBackgroundColor:UIColorFromRGB(0x8D8082)];
    [self.btnTrimester2 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnTrimester3 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth1 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth2 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth3 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth4 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth5 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth6 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth7 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth8 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth9 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth10 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth11 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth12 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth13 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth14 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self getMilestoneList];
}

-(IBAction)getTrimester2ListAction:(UIButton *)sender {
    
    selectedMonth = -2;
    [self.btnTrimester1 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnTrimester2 setBackgroundColor:UIColorFromRGB(0x8D8082)];
    [self.btnTrimester3 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth1 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth2 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth3 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth4 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth5 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth6 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth7 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth8 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth9 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth10 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth11 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth12 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth13 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth14 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self getMilestoneList];
}
- (IBAction)getTrimester3ListAction:(UIButton *)sender {
    
    selectedMonth = -3;
    [self.btnTrimester1 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnTrimester2 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnTrimester3 setBackgroundColor:UIColorFromRGB(0x8D8082)];
    [self.btnMonth1 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth2 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth3 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth4 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth5 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth6 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth7 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth8 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth9 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth10 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth11 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth12 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth13 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth14 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self getMilestoneList];
}



- (IBAction)getMonth1ListAction:(id)sender
{
    
    selectedMonth = 1;
    [self.btnTrimester1 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnTrimester2 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnTrimester3 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth1 setBackgroundColor:UIColorFromRGB(0x8D8082)];
    [self.btnMonth2 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth3 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth4 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth5 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth6 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth7 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth8 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth9 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth10 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth11 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth12 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth13 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth14 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self getMilestoneList];
}

- (IBAction)getMonth2ListAction:(id)sender
{
    selectedMonth = 2;
    [self.btnTrimester1 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnTrimester2 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnTrimester3 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth1 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth2 setBackgroundColor:UIColorFromRGB(0x8D8082)];
    [self.btnMonth3 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth4 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth5 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth6 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth7 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth8 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth9 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth10 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth11 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth12 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth13 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth14 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self getMilestoneList];
}

- (IBAction)getMonth3ListAction:(id)sender
{
    selectedMonth = 3;
    [self.btnTrimester1 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnTrimester2 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnTrimester3 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth1 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth2 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth3 setBackgroundColor:UIColorFromRGB(0x8D8082)];
    [self.btnMonth4 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth5 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth6 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth7 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth8 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth9 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth10 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth11 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth12 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth13 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth14 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self getMilestoneList];
}

- (IBAction)getMonth4ListAction:(id)sender
{
    selectedMonth = 4;
    [self.btnTrimester1 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnTrimester2 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnTrimester3 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth1 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth2 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth3 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth4 setBackgroundColor:UIColorFromRGB(0x8D8082)];
    [self.btnMonth5 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth6 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth7 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth8 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth9 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth10 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth11 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth12 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth13 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth14 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self getMilestoneList];
}

- (IBAction)getMonth5ListAction:(id)sender
{
    selectedMonth = 5;
    [self.btnTrimester1 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnTrimester2 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnTrimester3 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth1 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth2 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth3 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth4 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth5 setBackgroundColor:UIColorFromRGB(0x8D8082)];
    [self.btnMonth6 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth7 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth8 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth9 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth10 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth11 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth12 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth13 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth14 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self getMilestoneList];
}

- (IBAction)getMonth6ListAction:(id)sender
{
    selectedMonth = 7;
    [self.btnTrimester1 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnTrimester2 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnTrimester3 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth1 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth2 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth3 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth4 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth5 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth6 setBackgroundColor:UIColorFromRGB(0x8D8082)];
    [self.btnMonth7 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth8 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth9 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth10 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth11 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth12 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth13 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth14 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self getMilestoneList];
}

- (IBAction)getMonth7ListAction:(id)sender
{
    self.scrollView.contentOffset = CGPointMake(6*self.btnMonth1.frame.size.width+60, self.scrollView.contentOffset.y);
    
    selectedMonth = 10;
    [self.btnTrimester1 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnTrimester2 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnTrimester3 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth1 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth2 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth3 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth4 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth5 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth6 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth7 setBackgroundColor:UIColorFromRGB(0x8D8082)];
    [self.btnMonth8 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth9 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth10 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth11 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth12 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth13 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth14 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self getMilestoneList];
}

- (IBAction)getMonth8ListAction:(id)sender
{
    selectedMonth = 13;
    [self.btnTrimester1 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnTrimester2 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnTrimester3 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth1 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth2 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth3 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth4 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth5 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth6 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth7 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth8 setBackgroundColor:UIColorFromRGB(0x8D8082)];
    [self.btnMonth9 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth10 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth11 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth12 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth13 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth14 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self getMilestoneList];
}

- (IBAction)getMonth9ListAction:(id)sender
{
    selectedMonth = 16;
    [self.btnTrimester1 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnTrimester2 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnTrimester3 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth1 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth2 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth3 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth4 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth5 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth6 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth7 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth8 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth9 setBackgroundColor:UIColorFromRGB(0x8D8082)];
    [self.btnMonth10 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth11 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth12 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth13 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth14 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self getMilestoneList];
}

- (IBAction)getMonth10ListAction:(id)sender
{
    selectedMonth = 19;
    [self.btnTrimester1 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnTrimester2 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnTrimester3 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth1 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth2 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth3 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth4 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth5 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth6 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth7 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth8 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth9 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth10 setBackgroundColor:UIColorFromRGB(0x8D8082)];
    [self.btnMonth11 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth12 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth13 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth14 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self getMilestoneList];
}

- (IBAction)getMonth11ListAction:(id)sender
{
    selectedMonth = 25;
    [self.btnTrimester1 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnTrimester2 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnTrimester3 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth1 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth2 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth3 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth4 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth5 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth6 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth7 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth8 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth9 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth10 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth11 setBackgroundColor:UIColorFromRGB(0x8D8082)];
    [self.btnMonth12 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth13 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth14 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self getMilestoneList];
}

- (IBAction)getMonth12ListAction:(id)sender
{
    selectedMonth = 37;
    [self.btnTrimester1 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnTrimester2 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnTrimester3 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth1 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth2 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth3 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth4 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth5 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth6 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth7 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth8 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth9 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth10 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth11 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth12 setBackgroundColor:UIColorFromRGB(0x8D8082)];
    [self.btnMonth13 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth14 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self getMilestoneList];
}

- (IBAction)getMonth13ListAction:(id)sender
{
    selectedMonth = 49;
    [self.btnTrimester1 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnTrimester2 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnTrimester3 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth1 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth2 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth3 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth4 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth5 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth6 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth7 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth8 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth9 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth10 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth11 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth12 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth13 setBackgroundColor:UIColorFromRGB(0x8D8082)];
    [self.btnMonth14 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self getMilestoneList];
}

- (IBAction)getMonth14ListAction:(id)sender
{
    selectedMonth = 61;
    [self.btnTrimester1 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnTrimester2 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnTrimester3 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth1 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth2 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth3 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth4 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth5 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth6 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth7 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth8 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth9 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth10 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth11 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth12 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth13 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth14 setBackgroundColor:UIColorFromRGB(0x8D8082)];
    [self getMilestoneList];
}


#pragma mark - Table view data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
        return milestoneList.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}
- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    MilestonesChecklistTableViewCell *cell = (MilestonesChecklistTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    
    if(cell.checkBoxSelected.hidden)
    {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"MilestoneChecklistAddViewController"];
        viewController.data = [[NSMutableDictionary alloc] initWithDictionary:@{@"milestone_info":[milestoneList objectAtIndex:indexPath.row]}];
        [self.navigationController pushViewController:viewController animated:YES];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MilestonesChecklistTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MilestonesChecklistTableViewCell"];
    if(!cell)
    {
        [tableView registerNib:[UINib nibWithNibName:@"MilestonesChecklistTableViewCell" bundle:nil] forCellReuseIdentifier:@"MilestonesChecklistTableViewCell"];
        cell = [tableView dequeueReusableCellWithIdentifier:@"MilestonesChecklistTableViewCell"];
    }
    
    NSString *name = [[milestoneList objectAtIndex:indexPath.row] objectForKey:@"name"];
    if([name isEqualToString:@"grow"]){
        cell.imgIcon.backgroundColor = UIColorFromRGB(0xF3E99F);
    }if([name isEqualToString:@"move"]){
        cell.imgIcon.backgroundColor = UIColorFromRGB(0xFFC896);
    }if([name isEqualToString:@"iq"]){
        cell.imgIcon.backgroundColor = UIColorFromRGB(0xC5DEAA);
    }if([name isEqualToString:@"health"]){
        cell.imgIcon.backgroundColor = UIColorFromRGB(0xFACFD4);
    }if([name isEqualToString:@"others"]){
        cell.imgIcon.backgroundColor = UIColorFromRGB(0xDFCCDC);
    }if([name isEqualToString:@"pregnancy"]){
        cell.imgIcon.backgroundColor = UIColorFromRGB(0xDFCCDC);
    }
    
    cell.checkBox.layer.borderWidth = 1;
    cell.checkBox.layer.cornerRadius = 3.f;
    cell.checkBox.layer.masksToBounds = YES;
    cell.checkBox.layer.borderColor = UIColorFromRGB(0xEDE8E4).CGColor;
    
    NSString *type = [NSString stringWithFormat:@"txt_%@", [[milestoneList objectAtIndex:indexPath.row] objectForKey:@"type"]];
    cell.lblName.text = LocalizedString(type, nil);
    if ([[[milestoneList objectAtIndex:indexPath.row] objectForKey:@"done_check"] boolValue]) {
        cell.lblName.text = [NSString stringWithFormat:@"%@ - Checked %@",cell.lblName.text,[[milestoneList objectAtIndex:indexPath.row] objectForKey:@"checked_date"]];
    }
    cell.txtName.text = [[milestoneList objectAtIndex:indexPath.row] objectForKey:@"content"];
    
    UIImage *imgIcon = [UIImage imageNamed:[NSString stringWithFormat:@"icon_%@_Selected.png", name]];
    cell.imgIconWidthConstraint.constant = imgIcon.size.width;
    cell.imgIconHeightConstraint.constant = imgIcon.size.height;
    if (IS_IPAD) {
        cell.imgIconWidthConstraint.constant = cell.imgIconWidthConstraint.constant + 12;
        cell.imgIconHeightConstraint.constant = cell.imgIconHeightConstraint.constant + 12;
    }
    cell.imgIcon.image = imgIcon;
    cell.imgIcon.layer.cornerRadius = cell.imgIconWidthConstraint.constant/2;
    cell.checkBoxSelected.hidden = ![[[milestoneList objectAtIndex:indexPath.row] objectForKey:@"done_check"] boolValue];
    
    return cell;
}

@end
