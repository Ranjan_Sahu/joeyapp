//
//  MilestoneChecklistViewController.h
//  Joey
//
//  Created by csl on 9/6/2017.
//  Copyright © 2017 Auxilia. All rights reserved.
//

#import "BaseViewController.h"

@interface MilestoneChecklistViewController : BaseViewController <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, weak) IBOutlet UIScrollView *scrollView;

//Anil
@property (weak, nonatomic) IBOutlet UIButton *btnTrimester1;
@property (weak, nonatomic) IBOutlet UIButton *btnTrimester2;
@property (weak, nonatomic) IBOutlet UIButton *btnTrimester3;

@property (nonatomic, weak) IBOutlet UIButton *btnMonth1;
@property (nonatomic, weak) IBOutlet UIButton *btnMonth2;
@property (nonatomic, weak) IBOutlet UIButton *btnMonth3;
@property (nonatomic, weak) IBOutlet UIButton *btnMonth4;
@property (nonatomic, weak) IBOutlet UIButton *btnMonth5;
@property (nonatomic, weak) IBOutlet UIButton *btnMonth6;
@property (nonatomic, weak) IBOutlet UIButton *btnMonth7;
@property (nonatomic, weak) IBOutlet UIButton *btnMonth8;
@property (nonatomic, weak) IBOutlet UIButton *btnMonth9;
@property (nonatomic, weak) IBOutlet UIButton *btnMonth10;
@property (nonatomic, weak) IBOutlet UIButton *btnMonth11;
@property (nonatomic, weak) IBOutlet UIButton *btnMonth12;
@property (nonatomic, weak) IBOutlet UIButton *btnMonth13;
@property (nonatomic, weak) IBOutlet UIButton *btnMonth14;

@end
