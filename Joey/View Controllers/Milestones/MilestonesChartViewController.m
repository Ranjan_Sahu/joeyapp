//
//  MilestonesChartViewController.m
//  Joey
//
//  Created by csl on 8/6/2017.
//  Copyright © 2017 Auxilia. All rights reserved.
//

#import "AppDelegate.h"
#import "MilestonesChartViewController.h"

@interface MilestonesChartViewController ()
{
    NSUserDefaults *shareObject;
    AppDelegate *appDelegate;
    NSMutableDictionary *unitInfo;
}
@end

@implementation MilestonesChartViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    shareObject = [NSUserDefaults standardUserDefaults];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    unitInfo = (NSMutableDictionary *)[NSKeyedUnarchiver unarchiveObjectWithData:[shareObject objectForKey:@"unit_info"]];
    
    [self.navigationController setNavigationBarHidden:NO];
    [self.navigationController.navigationBar setBarTintColor:UIColorFromRGB(0xEEE8E4)];
    [self.navigationController.navigationBar setTranslucent:NO];
    [self.navigationItem setTitle:LocalizedString(@"title_growth_chart", nil)];
    [self customBackButton];
    
    NSString *type = [NSString stringWithFormat:@"txt_%@_chart", [self.data objectForKey:@"type"]];
    NSString *unit = @"";
    if([[self.data objectForKey:@"type"] isEqualToString:@"weight"])
        unit = [unitInfo objectForKey:@"weight"];
    else if([[self.data objectForKey:@"type"] isEqualToString:@"height"])
        unit = [unitInfo objectForKey:@"height"];
    else
        unit = [unitInfo objectForKey:@"head_size"];
    
    self.txtChartName.text = LocalizedString(type, nil);
    self.txtLeftAxis.text = LocalizedString(unit, nil);
    self.txtBottomAxis.text = LocalizedString(@"txt_months", nil);
    self.chartView.hidden = YES;
    [self getGrowthChartList];
    
    if (IS_IPAD) {
        self.txtChartName.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.txtLeftAxis.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:19.0f];
        self.txtBottomAxis.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:19.0f];
    }
}

- (void)customBackButton
{
    if([self.navigationController.viewControllers count] > 1)
    {
        UIImage *imgBack = [UIImage imageNamed:@"nav_btn_back"];
        UIButton *btnBack = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnBack setImage:imgBack forState:UIControlStateNormal];
        btnBack.frame = CGRectMake(0, 0, imgBack.size.width, imgBack.size.height);
        [btnBack addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
        [btnBack setImageEdgeInsets:UIEdgeInsetsMake(0, -16, 0, 16)];
        UIBarButtonItem *backBarItem = [[UIBarButtonItem alloc] initWithCustomView:btnBack];
        self.navigationItem.leftBarButtonItem = backBarItem;
    }
}

- (void)backAction
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)getGrowthChartList
{
    [SVProgressHUD show];
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setValue:@([shareObject integerForKey:@"user_id"]) forKey:@"user_id"];
    [parameters setValue:@([shareObject integerForKey:@"baby_id"]) forKey:@"baby_id"];
    [parameters setValue:[self.data objectForKey:@"type"] forKey:@"type"];
    [parameters setValue:[self.data objectForKey:@"gender"] forKey:@"gender"];
    [parameters setValue:[self.data objectForKey:@"month"] forKey:@"month"];
    [[WebServiceManager sharedManager] initWithBaseURL:@"api/getGrowthChartList" parameters:parameters success:^(id result) {
        
        NSMutableArray *yVals1 = [[NSMutableArray alloc] init];
        NSMutableArray *yVals2 = [[NSMutableArray alloc] init];
        NSMutableArray *yVals3 = [[NSMutableArray alloc] init];
        NSMutableArray *yVals4 = [[NSMutableArray alloc] init];
        NSMutableArray *yVals5 = [[NSMutableArray alloc] init];
        NSMutableArray *yVals6 = [[NSMutableArray alloc] init];
        NSMutableArray *yVals7 = [[NSMutableArray alloc] init];
        NSMutableArray *yVals8 = [[NSMutableArray alloc] init];
        NSMutableArray *yVals9 = [[NSMutableArray alloc] init];
        NSMutableArray *yVals10 = [[NSMutableArray alloc] init];
        
        NSLog(@"count=%d", [[result objectForKey:@"list"] count]);
        for(int i=0; i<[[result objectForKey:@"list"] count]; i++)
        {
            [yVals1 addObject:[[ChartDataEntry alloc] initWithX:[[[[result objectForKey:@"list"] objectAtIndex:i] objectForKey:@"month"] intValue] y:[[[[result objectForKey:@"list"] objectAtIndex:i] objectForKey:@"percentile_2"] doubleValue]]];
            [yVals2 addObject:[[ChartDataEntry alloc] initWithX:[[[[result objectForKey:@"list"] objectAtIndex:i] objectForKey:@"month"] intValue] y:[[[[result objectForKey:@"list"] objectAtIndex:i] objectForKey:@"percentile_5"] doubleValue]]];
            [yVals3 addObject:[[ChartDataEntry alloc] initWithX:[[[[result objectForKey:@"list"] objectAtIndex:i] objectForKey:@"month"] intValue] y:[[[[result objectForKey:@"list"] objectAtIndex:i] objectForKey:@"percentile_10"] doubleValue]]];
            [yVals4 addObject:[[ChartDataEntry alloc] initWithX:[[[[result objectForKey:@"list"] objectAtIndex:i] objectForKey:@"month"] intValue] y:[[[[result objectForKey:@"list"] objectAtIndex:i] objectForKey:@"percentile_25"] doubleValue]]];
            [yVals5 addObject:[[ChartDataEntry alloc] initWithX:[[[[result objectForKey:@"list"] objectAtIndex:i] objectForKey:@"month"] intValue] y:[[[[result objectForKey:@"list"] objectAtIndex:i] objectForKey:@"percentile_50"] doubleValue]]];
            [yVals6 addObject:[[ChartDataEntry alloc] initWithX:[[[[result objectForKey:@"list"] objectAtIndex:i] objectForKey:@"month"] intValue] y:[[[[result objectForKey:@"list"] objectAtIndex:i] objectForKey:@"percentile_75"] doubleValue]]];
            [yVals7 addObject:[[ChartDataEntry alloc] initWithX:[[[[result objectForKey:@"list"] objectAtIndex:i] objectForKey:@"month"] intValue] y:[[[[result objectForKey:@"list"] objectAtIndex:i] objectForKey:@"percentile_90"] doubleValue]]];
            [yVals8 addObject:[[ChartDataEntry alloc] initWithX:[[[[result objectForKey:@"list"] objectAtIndex:i] objectForKey:@"month"] intValue] y:[[[[result objectForKey:@"list"] objectAtIndex:i] objectForKey:@"percentile_95"] doubleValue]]];
            [yVals9 addObject:[[ChartDataEntry alloc] initWithX:[[[[result objectForKey:@"list"] objectAtIndex:i] objectForKey:@"month"] intValue] y:[[[[result objectForKey:@"list"] objectAtIndex:i] objectForKey:@"percentile_98"] doubleValue]]];
            
            for(int j=0; j<[[result objectForKey:@"chart_list"] count]; j++)
            {
                if(i == [[[[result objectForKey:@"chart_list"] objectAtIndex:j] objectForKey:@"month"] intValue])
                {
                    [yVals10 addObject:[[ChartDataEntry alloc] initWithX:[[[[result objectForKey:@"list"] objectAtIndex:i] objectForKey:@"month"] intValue] y:[[[[result objectForKey:@"chart_list"] objectAtIndex:j] objectForKey:@"value"] doubleValue]]];
                }
            }
            NSLog(@"yVals10=%@", yVals10);
        }
        
        
//        [yVals10 addObject:[[ChartDataEntry alloc] initWithX:2 y:10]];
//        [yVals10 addObject:[[ChartDataEntry alloc] initWithX:8 y:20]];
//        [yVals10 addObject:[[ChartDataEntry alloc] initWithX:13 y:15]];
//        [yVals10 addObject:[[ChartDataEntry alloc] initWithX:15 y:35]];
//        [yVals10 addObject:[[ChartDataEntry alloc] initWithX:20 y:50]];
//        [yVals10 addObject:[[ChartDataEntry alloc] initWithX:22 y:60]];
//        [yVals10 addObject:[[ChartDataEntry alloc] initWithX:23 y:80]];

        LineChartData *lineData = [[LineChartData alloc] init];
        LineChartDataSet *set1 = [[LineChartDataSet alloc] initWithValues:yVals1 label:@"2"];
        [set1 setColor:[UIColorFromRGB(0x8D8082) colorWithAlphaComponent:0.5f]];
        set1.lineDashLengths = @[@2, @4];
        set1.highlightLineDashLengths = @[@2, @4];
        set1.lineWidth = 0.5;
        set1.circleRadius = 0;
        set1.drawValuesEnabled = YES;
        [lineData addDataSet:set1];
        
        LineChartDataSet *set2 = [[LineChartDataSet alloc] initWithValues:yVals2 label:@"5"];
        [set2 setColor:[UIColorFromRGB(0x8D8082) colorWithAlphaComponent:0.5f]];
        set2.lineWidth = 0.5;
        set2.circleRadius = 0;
        set2.drawValuesEnabled = NO;
        [lineData addDataSet:set2];
        
        LineChartDataSet *set3 = [[LineChartDataSet alloc] initWithValues:yVals3 label:@"10"];
        [set3 setColor:[UIColorFromRGB(0x8D8082) colorWithAlphaComponent:0.5f]];
        set3.lineDashLengths = @[@2, @4];
        set3.highlightLineDashLengths = @[@2, @4];
        set3.lineWidth = 0.5;
        set3.circleRadius = 0;
        set3.drawValuesEnabled = NO;
        [lineData addDataSet:set3];
        
        LineChartDataSet *set4 = [[LineChartDataSet alloc] initWithValues:yVals4 label:@"25"];
        [set4 setColor:[UIColorFromRGB(0x8D8082) colorWithAlphaComponent:0.5f]];
        set4.lineWidth = 0.5;
        set4.circleRadius = 0;
        set4.drawValuesEnabled = NO;
        [lineData addDataSet:set4];
        
        LineChartDataSet *set5 = [[LineChartDataSet alloc] initWithValues:yVals5 label:@"50"];
        [set5 setColor:[UIColorFromRGB(0x8D8082) colorWithAlphaComponent:0.5f]];
        set5.lineDashLengths = @[@2, @4];
        set5.highlightLineDashLengths = @[@2, @4];
        set5.lineWidth = 0.5;
        set5.circleRadius = 0;
        set5.drawValuesEnabled = NO;
        [lineData addDataSet:set5];
        
        LineChartDataSet *set6 = [[LineChartDataSet alloc] initWithValues:yVals6 label:@"75"];
        [set6 setColor:[UIColorFromRGB(0x8D8082) colorWithAlphaComponent:0.5f]];
        set6.lineWidth = 0.5;
        set6.circleRadius = 0;
        set6.drawValuesEnabled = NO;
        [lineData addDataSet:set6];
        
        LineChartDataSet *set7 = [[LineChartDataSet alloc] initWithValues:yVals7 label:@"90"];
        [set7 setColor:[UIColorFromRGB(0x8D8082) colorWithAlphaComponent:0.5f]];
        set7.lineDashLengths = @[@2, @4];
        set7.highlightLineDashLengths = @[@2, @4];
        set7.lineWidth = 0.5;
        set7.circleRadius = 0;
        set7.drawValuesEnabled = NO;
        [lineData addDataSet:set7];
        
        LineChartDataSet *set8 = [[LineChartDataSet alloc] initWithValues:yVals8 label:@"95"];
        [set8 setColor:[UIColorFromRGB(0x8D8082) colorWithAlphaComponent:0.5f]];
        set8.lineWidth = 0.5;
        set8.circleRadius = 0;
        set8.drawValuesEnabled = NO;
        [lineData addDataSet:set8];
        
        LineChartDataSet *set9 = [[LineChartDataSet alloc] initWithValues:yVals9 label:@"98"];
        [set9 setColor:[UIColorFromRGB(0x8D8082) colorWithAlphaComponent:0.5f]];
        set9.lineDashLengths = @[@2, @4];
        set9.highlightLineDashLengths = @[@2, @4];
        set9.lineWidth = 0.5;
        set9.circleRadius = 0;
        set9.drawCubicEnabled = NO;
        [lineData addDataSet:set9];
        
        LineChartDataSet *set10 = [[LineChartDataSet alloc] initWithValues:yVals10];
        [set10 setColor:UIColorFromRGB(0xB8D8D8)];
        [set10 setCircleColor:UIColorFromRGB(0xB8D8D8)];
        set10.lineWidth = 2;
        set10.circleRadius = 4;
        set10.drawCubicEnabled = YES;
        set10.mode = LineChartModeCubicBezier;
        [lineData addDataSet:set10];
        
        ChartXAxis *xAxis = self.chartView.xAxis;
        xAxis.labelPosition = XAxisLabelPositionBottom;
        xAxis.labelFont = [UIFont fontWithName:@"SFUIDisplay-Medium" size:10.0f];
        if (IS_IPAD) {
            xAxis.labelFont = [UIFont fontWithName:@"SFUIDisplay-Medium" size:16.0f];
        }
        xAxis.labelTextColor = UIColorFromRGB(0xBFB5AE);
        xAxis.axisLineColor = UIColorFromRGB(0xEEE8E4);
        xAxis.drawGridLinesEnabled = YES;
        xAxis.gridColor = UIColorFromRGB(0xEEE8E4);
        xAxis.labelCount = 12;
        
        /*if([[self.data objectForKey:@"month"] intValue] > 24)
        {
            xAxis.axisMinimum = 25;
            xAxis.axisMaximum = [[self.data objectForKey:@"month"] intValue]+6;
            NSLog(@"xAxis.axisMaximum=%d", (int)xAxis.axisMaximum);
        }
        else
        {
            xAxis.axisMinimum = 0;
            xAxis.axisMaximum = 24;
        }*/
        
        ChartYAxis *leftAxis = self.chartView.leftAxis;
        leftAxis.labelPosition = YAxisLabelPositionOutsideChart;
        leftAxis.labelFont = [UIFont fontWithName:@"SFUIDisplay-Medium" size:10.0f];
        if (IS_IPAD) {
            leftAxis.labelFont = [UIFont fontWithName:@"SFUIDisplay-Medium" size:16.0f];
        }
        leftAxis.labelTextColor = UIColorFromRGB(0xBFB5AE);
        leftAxis.axisLineColor = UIColorFromRGB(0xEEE8E4);
        leftAxis.drawGridLinesEnabled = YES;
        leftAxis.gridColor = UIColorFromRGB(0xEEE8E4);
        leftAxis.labelCount = 12;
        
        ChartYAxis *rightAxis = self.chartView.rightAxis;
        rightAxis.labelPosition = YAxisLabelPositionOutsideChart;
        rightAxis.labelFont = [UIFont fontWithName:@"SFUIDisplay-Medium" size:10.0f];
        if (IS_IPAD) {
            rightAxis.labelFont = [UIFont fontWithName:@"SFUIDisplay-Medium" size:16.0f];
        }
        rightAxis.labelTextColor = UIColorFromRGB(0xBFB5AE);
        rightAxis.axisLineColor = UIColorFromRGB(0xEEE8E4);
        rightAxis.drawGridLinesEnabled  = YES;
        rightAxis.gridColor = UIColorFromRGB(0xEEE8E4);
        rightAxis.labelCount = 12;
        
        self.chartView.legend.enabled = NO;
        self.chartView.chartDescription.enabled = NO;
        self.chartView.pinchZoomEnabled = YES;//NO;
        self.chartView.highlightPerTapEnabled = NO;
        self.chartView.data = lineData;
        self.chartView.hidden = NO;
        self.chartView.userInteractionEnabled = true;
        
    } failure:^(NSError *error) {
    }];
}

@end
