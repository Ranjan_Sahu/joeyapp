//
//  MilestoneChecklistAddViewController.m
//  Joey
//
//  Created by csl on 16/6/2017.
//  Copyright © 2017 Auxilia. All rights reserved.
//

#import "AppDelegate.h"
#import "MilestoneChecklistAddViewController.h"
#import "VideoViewController.h"

@interface MilestoneChecklistAddViewController ()
{
    NSUserDefaults *shareObject;
    AppDelegate *appDelegate;
    NSMutableDictionary *unitInfo;
    NSMutableDictionary *milestoneInfo;
    NSDateFormatter *formatter;
    NSMutableArray *no100List;
    NSMutableArray *no1000List;
    NSMutableArray *no24List;
    NSMutableArray *no60List;
    NSMutableArray *digitList;
    NSMutableArray *digit2List;
    NSMutableArray *babyTeethList;
   	NSDate *selectedDateTime;
    
    BOOL saveCheck;
    NSMutableArray *keywordsArray;
    
    UIImageView *largeImageView;
    
    UIImage *imgUploadOriginal;
    UIImage *imgUploadOriginal2;
    UIImage *imgUploadOriginal3;
    
    UIImage *imgUploadCropped;
    UIImage *imgUploadCropped2;
    UIImage *imgUploadCropped3;

    NSString *videoUrl;
    NSString *videoUrl2;
    NSString *videoUrl3;
}

@property (nonatomic) UIPopoverController *popover;

@end

@implementation MilestoneChecklistAddViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    shareObject = [NSUserDefaults standardUserDefaults];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    formatter = [[NSDateFormatter alloc] init];
    [formatter setTimeZone:[NSTimeZone localTimeZone]];
    [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
    
    unitInfo = (NSMutableDictionary *)[NSKeyedUnarchiver unarchiveObjectWithData:[shareObject objectForKey:@"unit_info"]];
    milestoneInfo = [self.data objectForKey:@"milestone_info"];
    
    no100List = [[NSMutableArray alloc] init];
    for(int i=0; i<=100; i++) [no100List addObject:[NSString stringWithFormat:@"%d", i]];
    
    no1000List = [[NSMutableArray alloc] init];
    for(int i=0; i<=1000; i++) [no1000List addObject:[NSString stringWithFormat:@"%d", i]];
    
    no24List = [[NSMutableArray alloc] init];
    for(int i=0; i<24; i++) [no24List addObject:[NSString stringWithFormat:@"%d", i]];
    
    no60List = [[NSMutableArray alloc] init];
    for(int i=0; i<60; i++) [no60List addObject:[NSString stringWithFormat:@"%d", i]];
    
    digitList = [[NSMutableArray alloc] init];
    for(int i=0; i<10; i++) [digitList addObject:[NSString stringWithFormat:@"%d", i]];
    
    digit2List = [[NSMutableArray alloc] init];
    for(int i=0; i<100; i++) [digit2List addObject:[NSString stringWithFormat:@"%02d", i]];
    
    babyTeethList = [[NSMutableArray alloc] init];
    for(int i=1; i<=20; i++) [babyTeethList addObject:@(0)];
    
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:UIColorFromRGB(0x8D8082), NSForegroundColorAttributeName, [UIFont fontWithName:@"SFUIDisplay-Medium" size:18.0f], NSFontAttributeName, nil]];
    [self.navigationController.navigationBar setBarTintColor:UIColorFromRGB(0xEEE8E4)];
    [self.navigationController.navigationBar setTranslucent:NO];
    
    [self.navigationItem setTitle:LocalizedString(@"title_new_milestone", nil)];
    [self customBackButton];
    
    self.btnSave.title = LocalizedString(@"btn_save", nil);
    
    self.btnPhoto1.imageView.contentMode = UIViewContentModeScaleAspectFill;
    self.btnPhoto2.imageView.contentMode = UIViewContentModeScaleAspectFill;
    self.btnPhoto3.imageView.contentMode = UIViewContentModeScaleAspectFill;

    self.lblDate.text = LocalizedString(@"txt_date", nil);
    self.lblType.text = LocalizedString(@"txt_type", nil);
    self.lblContent.text = LocalizedString(@"txt_checklist", nil);
    self.lblPhoto.text = LocalizedString(@"txt_photo", nil);
    self.lblNotes.text = LocalizedString(@"txt_notes", nil);
    
    selectedDateTime = [NSDate date];
    if([[shareObject objectForKey:@"language"] hasPrefix:@"zh"])
    {
        [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"zh_HK"]];
        [formatter setDateStyle:NSDateFormatterMediumStyle];
        [formatter setTimeStyle:NSDateFormatterShortStyle];
    }
    else
    {
        [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
        formatter.dateFormat = @"EE d MMM, yyyy hh:mm a";
    }
    self.txtDate.text = [NSString stringWithFormat:@"%@", [formatter stringFromDate:selectedDateTime]];

    // Photo Picking & Display
    self.btnPhoto1.hidden = YES;
    self.btnPhoto2.hidden = YES;
    self.btnPhoto3.hidden = YES;
    self.btnPhoto1.tag = 1;
    self.btnPhoto2.tag = 2;
    self.btnPhoto3.tag = 3;
    [self.btnPhoto1 addTarget:self action:@selector(showPhotoAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.btnPhoto2 addTarget:self action:@selector(showPhotoAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.btnPhoto3 addTarget:self action:@selector(showPhotoAction:) forControlEvents:UIControlEventTouchUpInside];
    
    NSString *type = [NSString stringWithFormat:@"txt_%@", [milestoneInfo objectForKey:@"type"]];
    self.txtType.text = LocalizedString(type, nil);
    self.txtContent.text = [milestoneInfo objectForKey:@"content"];
    
    [self doSetupForKeywordView];
    [self cutomizeFont];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:(BOOL)animated];
    
    NSLog(@"self.data=%@", self.data);
    if([[self.data objectForKey:@"deleteCheck"] boolValue])
    {
        if ([[self.data objectForKey:@"imageTag"] integerValue] == 1) {
            self.btnPhoto1.hidden = YES;
            [self.data removeObjectForKey:@"imageTag"];
            videoUrl = nil;
        }else if ([[self.data objectForKey:@"imageTag"] integerValue] == 2) {
            self.btnPhoto2.hidden = YES;
            [self.data removeObjectForKey:@"imageTag"];
            videoUrl2 = nil;
        }else if ([[self.data objectForKey:@"imageTag"] integerValue] == 3) {
            self.btnPhoto3.hidden = YES;
            [self.data removeObjectForKey:@"imageTag"];
            videoUrl3 = nil;
        }
        
        [self.data removeObjectForKey:@"deleteCheck"];
        
    }
}

- (void)customBackButton
{
    if([self.navigationController.viewControllers count] > 1)
    {
        UIImage *imgBack = [UIImage imageNamed:@"nav_btn_back"];
        UIButton *btnBack = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnBack setImage:imgBack forState:UIControlStateNormal];
        btnBack.frame = CGRectMake(0, 0, imgBack.size.width, imgBack.size.height);
        [btnBack addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
        [btnBack setImageEdgeInsets:UIEdgeInsetsMake(0, -16, 0, 16)];
        UIBarButtonItem *backBarItem = [[UIBarButtonItem alloc] initWithCustomView:btnBack];
        self.navigationItem.leftBarButtonItem = backBarItem;
    }
}

- (void)backAction
{
    [COMMON_HELPER clearTmpDirectory];
    UIViewController *viewController = [self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count-2];
    viewController.data = [[NSMutableDictionary alloc] initWithDictionary:@{@"save_check":@(saveCheck)}];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)showCameraAction
{
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    if ([[NSUserDefaults standardUserDefaults]boolForKey:@"is_Subscription_Enabled"]) {
        imagePicker.mediaTypes = [[NSArray alloc] initWithObjects: (NSString *) kUTTypeMovie, (NSString *) kUTTypeVideo, (NSString *) kUTTypeImage, nil];
        imagePicker.videoQuality = UIImagePickerControllerQualityTypeLow;
        imagePicker.videoMaximumDuration = 120;
    }else {
        imagePicker.mediaTypes = [[NSArray alloc] initWithObjects: (NSString *) kUTTypeImage, nil];
    }
    
    [self presentViewController:imagePicker animated:NO completion:nil];
}

- (void)showPhotoAlbumAction
{
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    if ([[NSUserDefaults standardUserDefaults]boolForKey:@"is_Subscription_Enabled"]) {
        imagePicker.mediaTypes = [[NSArray alloc] initWithObjects: (NSString *) kUTTypeMovie, (NSString *) kUTTypeVideo, (NSString *) kUTTypeImage, nil];
    }else {
        imagePicker.mediaTypes = [[NSArray alloc] initWithObjects: (NSString *) kUTTypeImage, nil];
    }
    
    [self presentViewController:imagePicker animated:NO completion:nil];
}

- (void)showPhotoAction:(UIButton *)sender
{
    if (sender.hidden)
        return;
    
    UIImage *img;
    NSString *vStr;
    if (sender.tag == 1) {
        if (videoUrl && videoUrl.length>0) {
            vStr = videoUrl;
        }else {
            img = self.btnPhoto1.imageView.image;
        }
    }else if (sender.tag == 2) {
        if (videoUrl2 && videoUrl2.length>0) {
            vStr = videoUrl2;
        }else {
            img = self.btnPhoto2.imageView.image;
        }
    }else if (sender.tag == 3) {
        if (videoUrl3 && videoUrl3.length>0) {
            vStr = videoUrl3;
        }else {
            img = self.btnPhoto3.imageView.image;
        }
    }
    
    if (vStr && vStr.length>0) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"VideoViewController"];
        viewController.data = [[NSMutableDictionary alloc] initWithDictionary:@{@"viewController":self, @"video":vStr,@"imageTag":@(sender.tag)}];
        [self presentViewController:viewController animated:YES completion:nil];
    }
    else
    {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"PhotoViewController"];
        viewController.data = [[NSMutableDictionary alloc] initWithDictionary:@{@"viewController":self, @"image":img,@"imageTag":@(sender.tag)}];
        [self presentViewController:viewController animated:YES completion:nil];
    }

//    if (sender.hidden)
//        return;
//
//    UIImage *img;
//    if (sender.tag == 1) {
//        img = self.btnPhoto1.imageView.image;
//    }else if (sender.tag == 2) {
//        img = self.btnPhoto2.imageView.image;
//    }else if (sender.tag == 3) {
//        img = self.btnPhoto3.imageView.image;
//    }
//
//    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"PhotoViewController"];
//    viewController.data = [[NSMutableDictionary alloc] initWithDictionary:@{@"viewController":self, @"image":img,@"imageTag":@(sender.tag)}];
//    [self presentViewController:viewController animated:YES completion:nil];
}

- (IBAction)takePhotoAction
{
    AVAuthorizationStatus status = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    if(status == AVAuthorizationStatusDenied || status == AVAuthorizationStatusRestricted)
    {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:LocalizedString(@"error_camera", nil) message:@"" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *btnOk = [UIAlertAction actionWithTitle:LocalizedString(@"btn_ok", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            
            [alert dismissViewControllerAnimated:YES completion:nil];
        }];
        
        [alert addAction:btnOk];
        
        [self presentViewController:alert animated:YES completion:nil];
        
    }
    else
    {
        [self showCameraAction];
    }
}

- (IBAction)getPhotoAlbumAction
{
    PHAuthorizationStatus status = [PHPhotoLibrary authorizationStatus];
    if(status == PHAuthorizationStatusDenied || status == PHAuthorizationStatusRestricted)
    {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:LocalizedString(@"error_photo", nil) message:@"" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *btnOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            
            [alert dismissViewControllerAnimated:YES completion:nil];
        }];
        
        [alert addAction:btnOk];
        
        [self presentViewController:alert animated:YES completion:nil];
        
    }
    else
    {
        [self showPhotoAlbumAction];
    }
}

- (IBAction)nextAction:(id)sender
{
    [SVProgressHUD show];
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setValue:@([shareObject integerForKey:@"user_id"]) forKey:@"user_id"];
    [parameters setValue:@([shareObject integerForKey:@"baby_id"]) forKey:@"baby_id"];
    
    [parameters setValue:[milestoneInfo objectForKey:@"name"] forKey:@"name"];
    [parameters setValue:@([[milestoneInfo objectForKey:@"id"] intValue]) forKey:@"reference_id"];
    [parameters setValue:[milestoneInfo objectForKey:@"content"] forKey:[milestoneInfo objectForKey:@"type"]];
    [parameters setValue:self.txtNotes.text forKey:@"notes"];
    [parameters setValue:[self formatDateTime:selectedDateTime] forKey:@"time"];
    //[parameters setValue:[[NSString stringWithFormat:@"%@", selectedDateTime] substringWithRange:NSMakeRange(0, 19)] forKey:@"time"];
    
    if(!self.btnPhoto1.hidden || !self.btnPhoto2.hidden || !self.btnPhoto2.hidden)
    {
        [[WebServiceManager sharedManager] initWithBaseURL:@"api/addBabyMilestone" parameters:parameters formData:^(id<AFMultipartFormData> formData) {
            
            if (!self.btnPhoto1.hidden) {
                if (videoUrl && videoUrl.length>0) {
                    [formData appendPartWithFileURL:[NSURL URLWithString:videoUrl] name:@"video" fileName:@"video.mp4" mimeType:@"video" error: nil];
                }else {
                    [formData appendPartWithFileData:UIImageJPEGRepresentation(imgUploadCropped, 0.8f) name:@"image" fileName:@"image.jpg" mimeType:@"image/jpeg"];
                    [formData appendPartWithFileData:UIImageJPEGRepresentation(imgUploadOriginal, 0.8f) name:@"image_original" fileName:@"image_original.jpg" mimeType:@"image/jpeg"];
                }
            }
            
            if (!self.btnPhoto2.hidden) {
                if (videoUrl2 && videoUrl2.length>0) {
                    [formData appendPartWithFileURL:[NSURL URLWithString:videoUrl2] name:@"video2" fileName:@"video2.mp4" mimeType:@"video" error: nil];
                }else {
                    [formData appendPartWithFileData:UIImageJPEGRepresentation(imgUploadCropped2, 0.8f) name:@"image2" fileName:@"image2.jpg" mimeType:@"image/jpeg"];
                    [formData appendPartWithFileData:UIImageJPEGRepresentation(imgUploadOriginal2, 0.8f) name:@"image_original2" fileName:@"image_original2.jpg" mimeType:@"image/jpeg"];
                }
            }
            
            if (!self.btnPhoto3.hidden) {
                if (videoUrl3 && videoUrl3.length>0) {
                    [formData appendPartWithFileURL:[NSURL URLWithString:videoUrl3] name:@"video3" fileName:@"video3.mp4" mimeType:@"video" error: nil];
                }else {
                    [formData appendPartWithFileData:UIImageJPEGRepresentation(imgUploadCropped3, 0.8f) name:@"image3" fileName:@"image3.jpg" mimeType:@"image/jpeg"];
                    [formData appendPartWithFileData:UIImageJPEGRepresentation(imgUploadOriginal3, 0.8f) name:@"image_original3" fileName:@"image_original3.jpg" mimeType:@"image/jpeg"];
                }
            }

//            if (imgUploadOriginal) {
//                [formData appendPartWithFileData:UIImageJPEGRepresentation(imgUploadCropped, 0.8f) name:@"image" fileName:@"image.jpg" mimeType:@"image/jpeg"];
//                [formData appendPartWithFileData:UIImageJPEGRepresentation(imgUploadOriginal, 0.8f) name:@"image_original" fileName:@"image_original.jpg" mimeType:@"image/jpeg"];
//            }
//            if (imgUploadOriginal2) {
//                [formData appendPartWithFileData:UIImageJPEGRepresentation(imgUploadCropped2, 0.8f) name:@"image2" fileName:@"image2.jpg" mimeType:@"image/jpeg"];
//                [formData appendPartWithFileData:UIImageJPEGRepresentation(imgUploadOriginal2, 0.8f) name:@"image_original2" fileName:@"image_original2.jpg" mimeType:@"image/jpeg"];
//            }
//            if (imgUploadOriginal3) {
//                [formData appendPartWithFileData:UIImageJPEGRepresentation(imgUploadCropped3, 0.8f) name:@"image3" fileName:@"image3.jpg" mimeType:@"image/jpeg"];
//                [formData appendPartWithFileData:UIImageJPEGRepresentation(imgUploadOriginal3, 0.8f) name:@"image_original3" fileName:@"image_original3.jpg" mimeType:@"image/jpeg"];
//            }

            //NSLog(@"formData=%@", formData);
        } success:^(id result) {
            
            NSLog(@"result=%@", result);
            if([[result objectForKey:@"success"] boolValue])
            {
                NSMutableArray *babyList = (NSMutableArray *)[NSKeyedUnarchiver unarchiveObjectWithData:[shareObject objectForKey:@"baby_list"]];
                [babyList replaceObjectAtIndex:[shareObject integerForKey:@"baby_index"] withObject:[result valueForKey:@"info"]];
                [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:babyList] forKey:@"baby_list"];
                [shareObject synchronize];
                
                saveCheck = YES;
                appDelegate.updateMilestonesCheck = YES;
                [self backAction];
            }
            else
            {
                
            }
            
        } failure:^(NSError *error) {
            
        }];
    }
    else
    {
        [[WebServiceManager sharedManager] initWithBaseURL:@"api/addBabyMilestone" parameters:parameters success:^(id result) {
            
            NSLog(@"result=%@", result);
            if([[result objectForKey:@"success"] boolValue])
            {
                NSMutableArray *babyList = (NSMutableArray *)[NSKeyedUnarchiver unarchiveObjectWithData:[shareObject objectForKey:@"baby_list"]];
                [babyList replaceObjectAtIndex:[shareObject integerForKey:@"baby_index"] withObject:[result valueForKey:@"info"]];
                [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:babyList] forKey:@"baby_list"];
                [shareObject synchronize];
                
                saveCheck = YES;
                appDelegate.updateMilestonesCheck = YES;
                [self backAction];
            }
            else
            {
                
            }
            
        } failure:^(NSError *error) {
            
        }];
    }
}

#pragma mark - Custom Methods
-(NSString *)formatDateTime:(NSDate *)sDate
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    //[dateFormatter setDateFormat:@"E d MMM, yyyy hh:mm a"];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    [dateFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
    NSString *dateStr = [dateFormatter stringFromDate:sDate];
    NSLog(@"%@",dateStr);
    return dateStr;
    
}

#pragma mark - IBActions
-(IBAction)didSelectAddPhoto:(UIButton *)sender
{
    if (self.btnPhoto1.hidden || self.btnPhoto2.hidden || self.btnPhoto3.hidden)
    {
        UIAlertController *alertController=[UIAlertController alertControllerWithTitle:LocalizedString(@"title_selectImage", nil) message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        
        UIAlertAction *takePhoto=[UIAlertAction actionWithTitle:LocalizedString(@"title_camera", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self takePhotoAction];
        }];
        UIAlertAction *choosePhoto=[UIAlertAction actionWithTitle:LocalizedString(@"title_camera_roll", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self getPhotoAlbumAction];
        }];
        UIAlertAction *actionCancel=[UIAlertAction actionWithTitle:LocalizedString(@"title_cancel", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            [alertController dismissViewControllerAnimated:YES completion:nil];
        }];
        
        [alertController addAction:takePhoto];
        [alertController addAction:choosePhoto];
        [alertController addAction:actionCancel];
        
        if (IS_IPAD) {
            UIPopoverPresentationController *popPresenter = [alertController popoverPresentationController];
            popPresenter.sourceView = sender;
            popPresenter.sourceRect = CGRectMake(sender.bounds.origin.x, sender.bounds.size.height/2, 0, 0);
            //popPresenter.presentedViewController.preferredContentSize =  CGSizeMake(520, 400);
        }
        [self presentViewController:alertController animated:YES completion:nil];
    }
    else
    {
        [self showAlertWithTitle:@"" message:@"You can not add more than 3 photos. " input:nil];
    }
    
}

#pragma mark - Custom Actions
-(void)doSetupForKeywordView
{
    [self.btnaddKeyword addTarget:self action:@selector(didSelectAddKeyword:) forControlEvents:UIControlEventTouchUpInside];
    
    if (IS_IPAD) {
        self.keyword1.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.keyword2.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.keyword3.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.keyword4.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.keyword5.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.keyword6.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.keyword7.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.keyword8.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.keyword9.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
    }
    
    // keyword view
    keywordsArray = [[NSMutableArray alloc] init];
    self.keywordView.backgroundColor = [UIColor clearColor];
    self.keyword1.backgroundColor = [UIColor clearColor];
    self.keyword2.backgroundColor = [UIColor clearColor];
    self.keyword3.backgroundColor = [UIColor clearColor];
    self.keyword4.backgroundColor = [UIColor clearColor];
    self.keyword5.backgroundColor = [UIColor clearColor];
    self.keyword6.backgroundColor = [UIColor clearColor];
    self.keyword7.backgroundColor = [UIColor clearColor];
    self.keyword8.backgroundColor = [UIColor clearColor];
    self.keyword9.backgroundColor = [UIColor clearColor];
    
    self.keyword1.text = @"";
    self.keyword2.text = @"";
    self.keyword3.text = @"";
    self.keyword4.text = @"";
    self.keyword5.text = @"";
    self.keyword6.text = @"";
    self.keyword7.text = @"";
    self.keyword8.text = @"";
    self.keyword9.text = @"";
    
    self.keyword1.layer.cornerRadius = 4.0f;
    self.keyword1.layer.masksToBounds = YES;
    self.keyword2.layer.cornerRadius = 4.0f;
    self.keyword2.layer.masksToBounds = YES;
    self.keyword3.layer.cornerRadius = 4.0f;
    self.keyword3.layer.masksToBounds = YES;
    self.keyword4.layer.cornerRadius = 4.0f;
    self.keyword4.layer.masksToBounds = YES;
    self.keyword5.layer.cornerRadius = 4.0f;
    self.keyword5.layer.masksToBounds = YES;
    self.keyword6.layer.cornerRadius = 4.0f;
    self.keyword6.layer.masksToBounds = YES;
    self.keyword7.layer.cornerRadius = 4.0f;
    self.keyword7.layer.masksToBounds = YES;
    self.keyword8.layer.cornerRadius = 4.0f;
    self.keyword8.layer.masksToBounds = YES;
    self.keyword9.layer.cornerRadius = 4.0f;
    self.keyword9.layer.masksToBounds = YES;
    
    self.keyword1.userInteractionEnabled = YES;
    self.keyword2.userInteractionEnabled = YES;
    self.keyword3.userInteractionEnabled = YES;
    self.keyword4.userInteractionEnabled = YES;
    self.keyword5.userInteractionEnabled = YES;
    self.keyword6.userInteractionEnabled = YES;
    self.keyword7.userInteractionEnabled = YES;
    self.keyword8.userInteractionEnabled = YES;
    self.keyword9.userInteractionEnabled = YES;
    
    self.keyword1.tag = 1;
    self.keyword2.tag = 2;
    self.keyword3.tag = 3;
    self.keyword4.tag = 4;
    self.keyword5.tag = 5;
    self.keyword6.tag = 6;
    self.keyword7.tag = 7;
    self.keyword8.tag = 8;
    self.keyword9.tag = 9;
    
    UITapGestureRecognizer *gesture1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)] ;
    UITapGestureRecognizer *gesture2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)] ;
    UITapGestureRecognizer *gesture3 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)] ;
    UITapGestureRecognizer *gesture4 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)] ;
    UITapGestureRecognizer *gesture5 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)] ;
    UITapGestureRecognizer *gesture6 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)] ;
    UITapGestureRecognizer *gesture7 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)] ;
    UITapGestureRecognizer *gesture8 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)] ;
    UITapGestureRecognizer *gesture9 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)] ;
    
    [self.keyword1 addGestureRecognizer:gesture1];
    [self.keyword2 addGestureRecognizer:gesture2];
    [self.keyword3 addGestureRecognizer:gesture3];
    [self.keyword4 addGestureRecognizer:gesture4];
    [self.keyword5 addGestureRecognizer:gesture5];
    [self.keyword6 addGestureRecognizer:gesture6];
    [self.keyword7 addGestureRecognizer:gesture7];
    [self.keyword8 addGestureRecognizer:gesture8];
    [self.keyword9 addGestureRecognizer:gesture9];
    
    UILongPressGestureRecognizer *longPressGesture1 = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongGesture:)];
    longPressGesture1.minimumPressDuration = 1.5;
    UILongPressGestureRecognizer *longPressGesture2 = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongGesture:)];
    longPressGesture2.minimumPressDuration = 1.5;
    UILongPressGestureRecognizer *longPressGesture3 = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongGesture:)];
    longPressGesture3.minimumPressDuration = 1.5;
    UILongPressGestureRecognizer *longPressGesture4 = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongGesture:)];
    longPressGesture4.minimumPressDuration = 1.5;
    UILongPressGestureRecognizer *longPressGesture5 = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongGesture:)];
    longPressGesture5.minimumPressDuration = 1.5;
    UILongPressGestureRecognizer *longPressGesture6 = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongGesture:)];
    longPressGesture6.minimumPressDuration = 1.5;
    UILongPressGestureRecognizer *longPressGesture7 = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongGesture:)];
    longPressGesture7.minimumPressDuration = 1.5;
    UILongPressGestureRecognizer *longPressGesture8 = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongGesture:)];
    longPressGesture8.minimumPressDuration = 1.5;
    UILongPressGestureRecognizer *longPressGesture9 = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongGesture:)];
    longPressGesture9.minimumPressDuration = 1.5;
    
    [self.keyword1 addGestureRecognizer:longPressGesture1];
    [self.keyword2 addGestureRecognizer:longPressGesture2];
    [self.keyword3 addGestureRecognizer:longPressGesture3];
    [self.keyword4 addGestureRecognizer:longPressGesture4];
    [self.keyword5 addGestureRecognizer:longPressGesture5];
    [self.keyword6 addGestureRecognizer:longPressGesture6];
    [self.keyword7 addGestureRecognizer:longPressGesture7];
    [self.keyword8 addGestureRecognizer:longPressGesture8];
    [self.keyword9 addGestureRecognizer:longPressGesture9];
    
}

-(void)handleTap:(UITapGestureRecognizer *)sender
{
    NSString * seletedKey = @"";
    
    if (sender.view.tag == 1) {
        seletedKey = self.keyword1.text;
    }else if (sender.view.tag == 2) {
        seletedKey = self.keyword2.text;
    }else if (sender.view.tag == 3) {
        seletedKey = self.keyword3.text;
    }else if (sender.view.tag == 4) {
        seletedKey = self.keyword4.text;
    }else if (sender.view.tag == 5) {
        seletedKey = self.keyword5.text;
    }else if (sender.view.tag == 6) {
        seletedKey = self.keyword6.text;
    }else if (sender.view.tag == 7) {
        seletedKey = self.keyword7.text;
    }else if (sender.view.tag == 8) {
        seletedKey = self.keyword8.text;
    }else if (sender.view.tag == 9) {
        seletedKey = self.keyword9.text;
    }
    
    self.txtNotes.text = [NSString stringWithFormat:@"%@ %@",self.txtNotes.text, seletedKey];
    
}

-(void)handleLongGesture:(UILongPressGestureRecognizer *)sender
{
    int tag = (int)sender.view.tag;
    CGPoint buttonPosition = [sender.view convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *indexPathToReload = [self.tableView indexPathForRowAtPoint:buttonPosition];
    
    //    if ( sender.state == UIGestureRecognizerStateEnded ) {
    if (keywordsArray.count>0) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Remove Keyword" message:@"Are you sure you want to remove this Keyword !" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *btnOk = [UIAlertAction actionWithTitle:LocalizedString(@"btn_ok", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            
            [keywordsArray removeObjectAtIndex:tag-1];
            [self addKeywordLabel];
            [self.tableView reloadRowsAtIndexPaths:@[indexPathToReload] withRowAnimation:UITableViewRowAnimationTop];
            
        }];
        
        UIAlertAction *btnCancel = [UIAlertAction actionWithTitle:LocalizedString(@"btn_cancel", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [alert dismissViewControllerAnimated:YES completion:nil];
        }];
        [alert addAction:btnCancel];
        [alert addAction:btnOk];
        [self presentViewController:alert animated:YES completion:nil];
    }
    //    }
}

-(void)didSelectAddKeyword:(UIButton *)sender
{
    if (keywordsArray.count<9) {
        
        CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
        NSIndexPath *indexPathToReload = [self.tableView indexPathForRowAtPoint:buttonPosition];
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:@"Add Keyword" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *btnOk = [UIAlertAction actionWithTitle:LocalizedString(@"btn_ok", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            if(alert.textFields[0].text.length > 0)
            {
                [alert dismissViewControllerAnimated:YES completion:nil];
                
                NSString *text = alert.textFields[0].text;
                [keywordsArray addObject:text];
                [self addKeywordLabel];
                [self.tableView reloadRowsAtIndexPaths:@[indexPathToReload] withRowAnimation:UITableViewRowAnimationBottom];
            }
            else
            {
                [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:LocalizedString(@"error_family_name", nil) input:nil];
            }
        }];
        
        UIAlertAction *btnCancel = [UIAlertAction actionWithTitle:LocalizedString(@"btn_cancel", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [alert dismissViewControllerAnimated:YES completion:nil];
        }];
        
        [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
            textField.autocapitalizationType = UITextAutocapitalizationTypeWords;
        }];
        
        [alert addAction:btnCancel];
        [alert addAction:btnOk];
        [self presentViewController:alert animated:YES completion:nil];
        
    }
    else {
        [self showAlertWithTitle:LocalizedString(@"Keywords", nil) message:@"You can not add more than 9 keywords !" input:nil];
    }
    
}

-(void)addKeywordLabel
{
    self.keyword1.text = @"";
    self.keyword2.text = @"";
    self.keyword3.text = @"";
    self.keyword4.text = @"";
    self.keyword5.text = @"";
    self.keyword6.text = @"";
    self.keyword7.text = @"";
    self.keyword8.text = @"";
    self.keyword9.text = @"";
    
    self.keyword1.backgroundColor = [UIColor clearColor];
    self.keyword2.backgroundColor = [UIColor clearColor];
    self.keyword3.backgroundColor = [UIColor clearColor];
    self.keyword4.backgroundColor = [UIColor clearColor];
    self.keyword5.backgroundColor = [UIColor clearColor];
    self.keyword6.backgroundColor = [UIColor clearColor];
    self.keyword7.backgroundColor = [UIColor clearColor];
    self.keyword8.backgroundColor = [UIColor clearColor];
    self.keyword9.backgroundColor = [UIColor clearColor];
    
    if (keywordsArray.count>0) {
        
        for (int i=0;i<keywordsArray.count;i++) {
            
            if (i==0) {
                self.keyword1.text =[NSString stringWithFormat:@"%@",[keywordsArray objectAtIndex:i]];
                self.keyword1.backgroundColor = UIColorFromRGB(0xB7ADA6);
            }else if (i==1) {
                self.keyword2.text =[NSString stringWithFormat:@"%@",[keywordsArray objectAtIndex:i]];
                self.keyword2.backgroundColor = UIColorFromRGB(0xB7ADA6);
            }else if (i==2) {
                self.keyword3.text =[NSString stringWithFormat:@"%@",[keywordsArray objectAtIndex:i]];
                self.keyword3.backgroundColor = UIColorFromRGB(0xB7ADA6);
            }else if (i==3) {
                self.keyword4.text =[NSString stringWithFormat:@"%@",[keywordsArray objectAtIndex:i]];
                self.keyword4.backgroundColor = UIColorFromRGB(0xB7ADA6);
            }else if (i==4) {
                self.keyword5.text =[NSString stringWithFormat:@"%@",[keywordsArray objectAtIndex:i]];
                self.keyword5.backgroundColor = UIColorFromRGB(0xB7ADA6);
            }else if (i==5) {
                self.keyword6.text =[NSString stringWithFormat:@"%@",[keywordsArray objectAtIndex:i]];
                self.keyword6.backgroundColor = UIColorFromRGB(0xB7ADA6);
            }else if (i==6) {
                self.keyword7.text =[NSString stringWithFormat:@"%@",[keywordsArray objectAtIndex:i]];
                self.keyword7.backgroundColor = UIColorFromRGB(0xB7ADA6);
            }else if (i==7) {
                self.keyword8.text =[NSString stringWithFormat:@"%@",[keywordsArray objectAtIndex:i]];
                self.keyword8.backgroundColor = UIColorFromRGB(0xB7ADA6);
            }else if (i==8) {
                self.keyword9.text =[NSString stringWithFormat:@"%@",[keywordsArray objectAtIndex:i]];
                self.keyword9.backgroundColor = UIColorFromRGB(0xB7ADA6);
            }
        }
    }
}

- (void)selectCheckListTime:(NSString *)milestoneDate
{
    [ActionSheetDatePicker showPickerWithTitle:@"" datePickerMode:UIDatePickerModeTime selectedDate:selectedDateTime minimumDate:nil maximumDate:[NSDate date] doneBlock:^(ActionSheetDatePicker *picker, id selectedDate, id origin) {
        
        selectedDateTime = selectedDate;
        NSLog(@"selectedMilestoneTime=%@", selectedDateTime);
        
        formatter.dateFormat = @"hh:mm a";
        NSString *milestoneTime = [formatter stringFromDate:selectedDate];
        
        formatter.dateFormat = @"d MMM yyyy hh:mm a";
        selectedDateTime = [formatter dateFromString:[NSString stringWithFormat:@"%@ %@", milestoneDate, milestoneTime]];
        NSLog(@"selectedMilestoneDateTime=%@", selectedDateTime);
        
        if([[shareObject objectForKey:@"language"] hasPrefix:@"zh"])
        {
            [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"zh_HK"]];
            [formatter setDateStyle:NSDateFormatterMediumStyle];
            [formatter setTimeStyle:NSDateFormatterShortStyle];
        }
        else
        {
            [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
            formatter.dateFormat = @"EE d MMM, yyyy hh:mm a";
        }
        self.txtDate.text = [NSString stringWithFormat:@"%@", [formatter stringFromDate:selectedDateTime]];
        //[NSString stringWithFormat:@"%@ %@", milestoneDate, milestoneTime];
        
        [self.tableView reloadData];
        
    } cancelBlock:^(ActionSheetDatePicker *picker) {
        
    } origin:self.view];
}

#pragma mark - Table view data source
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}
- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Date & Time
    if(indexPath.row == 0)
    {
        NSDate *minimumDate = [[NSDate date] dateByAddingTimeInterval:-20*365*24*60*60];
        NSDate *maximumDate = [NSDate date];
        
        NSLog(@"selectedMilestoneDateTime=%@", selectedDateTime);
        [ActionSheetDatePicker showPickerWithTitle:@"" datePickerMode:UIDatePickerModeDate selectedDate:selectedDateTime minimumDate:minimumDate maximumDate:maximumDate doneBlock:^(ActionSheetDatePicker *picker, id selectedDate, id origin) {
            
            selectedDateTime = selectedDate;
            NSLog(@"selectedMilestoneDate=%@", selectedDateTime);
            
            formatter.dateFormat = @"d MMM yyyy";
            NSString *milestoneDate = [formatter stringFromDate:selectedDate];
            
            [self performSelector:@selector(selectCheckListTime:) withObject:milestoneDate afterDelay:0.5];
            
        } cancelBlock:^(ActionSheetDatePicker *picker) {
            
        } origin:self.view];
    }
}

#pragma mark -
#pragma mark UIScrollViewDelegate Methods
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if([self.navigationController.visibleViewController isKindOfClass:[MilestoneChecklistAddViewController class]])
    {
        CGPoint translation = [scrollView.panGestureRecognizer translationInView:scrollView.superview];
        if(translation.y > 0)
        {
            [self.view endEditing:YES];
        }
    }
}




#pragma mark - UIImagePickerController Delegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    NSString *mediaType = info[UIImagePickerControllerMediaType];
    if ([mediaType isEqualToString:(NSString *)kUTTypeImage])
    {
        NSLog(@"Media is an image");
        // Handle selected image
        UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
        NSLog(@"image=%@", image);
        
        if (self.btnPhoto1.hidden) {
            imgUploadOriginal = [info objectForKey:UIImagePickerControllerOriginalImage];
        }else if (self.btnPhoto2.hidden) {
            imgUploadOriginal2 = [info objectForKey:UIImagePickerControllerOriginalImage];
        }else if (self.btnPhoto3.hidden) {
            imgUploadOriginal3 = [info objectForKey:UIImagePickerControllerOriginalImage];
        }
        
        if (picker.sourceType == UIImagePickerControllerSourceTypeCamera) {
            UIImageWriteToSavedPhotosAlbum([info objectForKey:UIImagePickerControllerOriginalImage], self, @selector(savedPhotoImage:didFinishSavingWithError:contextInfo:), nil);
        }
        
        if (IS_IPAD) {
            if (self.popover.isPopoverVisible) {
                [self.popover dismissPopoverAnimated:NO];
            }
            [picker dismissViewControllerAnimated:YES completion:^{
                [self openEditor:[info objectForKey:UIImagePickerControllerOriginalImage]];
            }];
        } else {
            [picker dismissViewControllerAnimated:YES completion:^{
                [self openEditor:[info objectForKey:UIImagePickerControllerOriginalImage]];
            }];
        }
    }
    else if ([mediaType isEqualToString:(NSString *)kUTTypeMovie])
    {
        BOOL ntwrkFlagStatus = [[NSUserDefaults standardUserDefaults] boolForKey:@"can_Upload_Using_Mobile_Network"];
        BOOL checkWifiStatus = [COMMON_HELPER checkforWifiNetwork];
        
        if ((checkWifiStatus==NO) && (ntwrkFlagStatus==NO)) {
            [self dismissViewControllerAnimated:NO completion:^{
            }];
            [self showAlertWithTitle:LocalizedString(@"mobile_data_usages_title", nil) message:LocalizedString(@"mobile_data_usages_message", nil) input:nil];
            return;
        }else {
            
            NSLog(@"Media is a video");
            NSURL *vUrl= (NSURL*)[info objectForKey:UIImagePickerControllerMediaURL];
            NSString *moviePath = [vUrl path];
            
            NSURL* uploadURL;
            if (self.btnPhoto1.hidden) {
                [[NSFileManager defaultManager] removeItemAtPath:[NSString stringWithFormat:@"%@video1.mov", NSTemporaryDirectory()] error:NULL];
                uploadURL = [NSURL fileURLWithPath:
                             [NSTemporaryDirectory() stringByAppendingPathComponent:@"video1.mov"]];
            }else if (self.btnPhoto2.hidden) {
                [[NSFileManager defaultManager] removeItemAtPath:[NSString stringWithFormat:@"%@video2.mov", NSTemporaryDirectory()] error:NULL];
                uploadURL = [NSURL fileURLWithPath:
                             [NSTemporaryDirectory() stringByAppendingPathComponent:@"video2.mov"]];
            }else if (self.btnPhoto3.hidden) {
                [[NSFileManager defaultManager] removeItemAtPath:[NSString stringWithFormat:@"%@video3.mov", NSTemporaryDirectory()] error:NULL];
                uploadURL = [NSURL fileURLWithPath:
                             [NSTemporaryDirectory() stringByAppendingPathComponent:@"video3.mov"]];
            }
            
            // Generate Thumbnail Image.
            UIImage *tImg = [COMMON_HELPER generateThumbnailImageFromVideoWithUrl:vUrl];
            
            [SVProgressHUD showWithStatus:LocalizedString(@"processing_video", nil)];
            [COMMON_HELPER compressVideo:vUrl outputURL:uploadURL handler:^(AVAssetExportSession *completion) {
                
                if (completion.status == AVAssetExportSessionStatusCompleted) {
                    NSData *newDataForUpload = [NSData dataWithContentsOfURL:uploadURL];
                    NSLog(@"Size of new Video after compression is (bytes):%lu",(unsigned long)[newDataForUpload length]);
                    
                    CGFloat sizeInMB = ([newDataForUpload length]/1000)/1000;
                    if (sizeInMB>15) {
                        [self showAlertWithTitleCallback:@"Large video size" message:@"Can not add this video." callback:^(UIAlertAction *action) {
                            [[NSFileManager defaultManager] removeItemAtPath:[NSString stringWithFormat:@"%@", uploadURL] error:NULL];
                        }];
                    }
                    else {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            if (self.btnPhoto1.hidden) {
                                self.btnPhoto1.hidden = false;
                                videoUrl = [uploadURL absoluteString];
                                [self.btnPhoto1 setImage:tImg forState:UIControlStateNormal];
                            }else if (self.btnPhoto2.hidden) {
                                self.btnPhoto2.hidden = false;
                                videoUrl2 = [uploadURL absoluteString];
                                [self.btnPhoto2 setImage:tImg forState:UIControlStateNormal];
                            }else if (self.btnPhoto3.hidden) {
                                self.btnPhoto3.hidden = false;
                                videoUrl3 = [uploadURL absoluteString];
                                [self.btnPhoto3 setImage:tImg forState:UIControlStateNormal];
                            }
                        });
                        
                    }
                }
                [SVProgressHUD dismiss];
                
            }];
            
            if (picker.sourceType == UIImagePickerControllerSourceTypeCamera) {
                if (UIVideoAtPathIsCompatibleWithSavedPhotosAlbum (moviePath)) {
                    UISaveVideoAtPathToSavedPhotosAlbum (moviePath, nil, nil, nil);
                }
            }
            
            [self dismissViewControllerAnimated:NO completion:^{
            }];
        }
    }
    
    /*
     float scale = self.view.bounds.size.width*2/image.size.width;
     CGSize newSize = CGSizeMake(self.view.bounds.size.width*2, image.size.height*scale);
     if(image.size.width > image.size.height)
     {
     scale = self.view.bounds.size.width*2/image.size.height;
     newSize = CGSizeMake(image.size.width*scale, self.view.bounds.size.width*2);
     }
     UIGraphicsBeginImageContext(newSize);
     [image drawInRect:(CGRect){0, 0, newSize}];
     image = UIGraphicsGetImageFromCurrentImageContext();
     UIGraphicsEndImageContext();
     
     [self.btnPhoto1 setImage:image forState:UIControlStateNormal];
     self.btnPhoto1.hidden = NO;
     
     [self dismissViewControllerAnimated:NO completion:^{
     
     }];
     */
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void) savedPhotoImage:(UIImage *)image
didFinishSavingWithError:(NSError *)error
             contextInfo:(void *)contextInfo
{
    NSLog(@"This image has been saved to your camera roll");
}

- (void)openEditor:(UIImage*) image1
{
    PECropViewController *controller = [[PECropViewController alloc] init];
    controller.delegate = self;
    controller.toolbarHidden = YES;
    controller.image = image1;
    
    //    CGFloat width = image1.size.width;
    //    CGFloat height = image1.size.height;
    //    CGFloat length = MIN(width, height);
    //    controller.imageCropRect = CGRectMake((width - length) / 2,(height - length) / 2,length,length);
    
    controller.cropAspectRatio = [[NSUserDefaults standardUserDefaults] floatForKey:@"Img_Aspect"];
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:controller];
    
    if (IS_IPAD) {
        navigationController.modalPresentationStyle = UIModalPresentationFullScreen;
    }
    [self presentViewController:navigationController animated:YES completion:NULL];
    
}


- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return largeImageView;
}

#pragma mark - PECropViewControllerDelegate methods

- (void)cropViewController:(PECropViewController *)controller didFinishCroppingImage:(UIImage *)croppedImage transform:(CGAffineTransform)transform cropRect:(CGRect)cropRect
{
    [controller dismissViewControllerAnimated:YES completion:NULL];
    
    float scale = self.view.bounds.size.width*2/croppedImage.size.width;
    CGSize newSize = CGSizeMake(self.view.bounds.size.width*2, croppedImage.size.height*scale);
    
    //    if (self.btnPhoto1.hidden) {
    //        imgUploadCropped = croppedImage;
    //    }else if (!imgUploadCropped2) {
    //        imgUploadCropped2 = croppedImage;
    //    }else if (!imgUploadCropped3) {
    //        imgUploadCropped3 = croppedImage;
    //    }
    
    if(croppedImage.size.width > croppedImage.size.height)
    {
        scale = self.view.bounds.size.width*2/croppedImage.size.height;
        newSize = CGSizeMake(croppedImage.size.width*scale, self.view.bounds.size.width*2);
    }
    UIGraphicsBeginImageContext(newSize);
    [croppedImage drawInRect:(CGRect){0, 0, newSize}];
    croppedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    if (self.btnPhoto1.hidden) {
        imgUploadCropped = croppedImage;
        self.btnPhoto1.hidden = false;
        [self.btnPhoto1 setImage:croppedImage forState:UIControlStateNormal];
    }else if (self.btnPhoto2.hidden) {
        imgUploadCropped2 = croppedImage;
        self.btnPhoto2.hidden = false;
        [self.btnPhoto2 setImage:croppedImage forState:UIControlStateNormal];
    }else if (self.btnPhoto3.hidden) {
        imgUploadCropped3 = croppedImage;
        self.btnPhoto3.hidden = false;
        [self.btnPhoto3 setImage:croppedImage forState:UIControlStateNormal];
    }
    
}

- (void)cropViewControllerDidCancel:(PECropViewController *)controller
{
    //    self.btnPhoto1.hidden = YES;
    //    self.btnPhoto2.hidden = YES;
    //    self.btnPhoto3.hidden = YES;
    
    //    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
    //        // [self updateEditButtonEnabled];
    //    }
    [controller dismissViewControllerAnimated:YES completion:NULL];
}


- (void)cutomizeFont
{
    if (IS_IPAD) {
        self.lblDate.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f];
        self.txtDate.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f];
        self.lblType.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f];
        self.txtType.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f];
        self.lblContent.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f];
        self.txtContent.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f];
        self.lblPhoto.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f];
        self.lblNotes.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f];
        self.txtNotes.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f];
    }
}

@end
