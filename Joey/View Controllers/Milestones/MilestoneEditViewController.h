//
//  MilestoneEditViewController.h
//  Joey
//
//  Created by csl on 6/4/2017.
//  Copyright © 2017 Auxilia. All rights reserved.
//

#import "BaseTableViewController.h"
#import <Photos/Photos.h>
#import <AVFoundation/AVFoundation.h>

@interface MilestoneEditViewController : BaseTableViewController <UINavigationControllerDelegate, UIImagePickerControllerDelegate>

@property (nonatomic, weak) IBOutlet UIBarButtonItem *btnSave;
@property (nonatomic, weak) IBOutlet UILabel *lblDate;
@property (nonatomic, weak) IBOutlet UILabel *txtDate;
@property (nonatomic, weak) IBOutlet UILabel *lblHeight;
@property (nonatomic, weak) IBOutlet UILabel *txtHeight;
@property (nonatomic, weak) IBOutlet UILabel *lblWeight;
@property (nonatomic, weak) IBOutlet UILabel *txtWeight;
@property (nonatomic, weak) IBOutlet UILabel *lblHeadSize;
@property (nonatomic, weak) IBOutlet UILabel *txtHeadSize;
@property (nonatomic, weak) IBOutlet UILabel *lblTeething;
@property (nonatomic, weak) IBOutlet UILabel *txtTeething;
@property (nonatomic, weak) IBOutlet UILabel *lblMovement;
@property (nonatomic, weak) IBOutlet UITextField *txtMovement;
@property (nonatomic, weak) IBOutlet UILabel *lblVerbal;
@property (nonatomic, weak) IBOutlet UITextField *txtVerbal;
@property (nonatomic, weak) IBOutlet UILabel *lblLearning;
@property (nonatomic, weak) IBOutlet UITextField *txtLearning;
@property (nonatomic, weak) IBOutlet UILabel *lblMoods;
@property (nonatomic, weak) IBOutlet UITextField *txtMoods;
@property (nonatomic, weak) IBOutlet UILabel *lblVaccine;
@property (nonatomic, weak) IBOutlet UITextField *txtVaccine;
@property (nonatomic, weak) IBOutlet UILabel *lblAllergies;
@property (nonatomic, weak) IBOutlet UITextField *txtAllergies;
@property (nonatomic, weak) IBOutlet UILabel *lblSymptom;
@property (nonatomic, weak) IBOutlet UITextField *txtSymptom;
@property (nonatomic, weak) IBOutlet UILabel *lblDiagnosis;
@property (nonatomic, weak) IBOutlet UITextField *txtDiagnosis;
@property (nonatomic, weak) IBOutlet UILabel *lblMedication;
@property (nonatomic, weak) IBOutlet UITextField *txtMedication;
@property (nonatomic, weak) IBOutlet UILabel *lblClinic;
@property (nonatomic, weak) IBOutlet UITextField *txtClinic;
@property (nonatomic, weak) IBOutlet UILabel *lblReminderTitle;
@property (nonatomic, weak) IBOutlet UITextField *txtReminderTitle;
@property (nonatomic, weak) IBOutlet UILabel *lblReminder;
@property (nonatomic, weak) IBOutlet UILabel *txtReminder;
@property (nonatomic, weak) IBOutlet UILabel *lblOthers;
@property (nonatomic, weak) IBOutlet UITextField *txtOthers;
@property (nonatomic, weak) IBOutlet UILabel *lblChecklist;
@property (nonatomic, weak) IBOutlet UILabel *txtChecklist;
@property (nonatomic, weak) IBOutlet UILabel *lblPhoto;

@property (nonatomic, weak) IBOutlet UIButton *btnAddPhoto;
@property (nonatomic, weak) IBOutlet UIButton *btnPhoto1;
@property (nonatomic, weak) IBOutlet UIButton *btnPhoto2;
@property (nonatomic, weak) IBOutlet UIButton *btnPhoto3;

@property (nonatomic, weak) IBOutlet UILabel *lblNotes;
@property (nonatomic, weak) IBOutlet UITextView *txtNotes;
@property (nonatomic, weak) IBOutlet UIButton *btnDelete;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *btnDeleteHeight;

// Baby
@property (nonatomic, weak) IBOutlet UIView *viewTeethingBaby1;
@property (nonatomic, weak) IBOutlet UIView *viewTeethingBaby2;
@property (nonatomic, weak) IBOutlet UIView *viewTeethingBaby3;
@property (nonatomic, weak) IBOutlet UIView *viewTeethingBaby4;
@property (nonatomic, weak) IBOutlet UIView *viewTeethingBaby5;
@property (nonatomic, weak) IBOutlet UIView *viewTeethingBaby6;

@property (nonatomic, weak) IBOutlet UILabel *lblTeethingBaby1;
@property (nonatomic, weak) IBOutlet UILabel *lblTeethingBaby2;
@property (nonatomic, weak) IBOutlet UILabel *lblTeethingBaby3;
@property (nonatomic, weak) IBOutlet UILabel *lblTeethingBaby4;
@property (nonatomic, weak) IBOutlet UILabel *lblTeethingBaby5;
@property (nonatomic, weak) IBOutlet UILabel *lblTeethingBaby6;

@property (nonatomic, weak) IBOutlet UIButton *btnTeethingBaby1;
@property (nonatomic, weak) IBOutlet UIButton *btnTeethingBaby2;
@property (nonatomic, weak) IBOutlet UIButton *btnTeethingBaby3;
@property (nonatomic, weak) IBOutlet UIButton *btnTeethingBaby4;
@property (nonatomic, weak) IBOutlet UIButton *btnTeethingBaby5;
@property (nonatomic, weak) IBOutlet UIButton *btnTeethingBaby6;
@property (nonatomic, weak) IBOutlet UIButton *btnTeethingBaby7;
@property (nonatomic, weak) IBOutlet UIButton *btnTeethingBaby8;
@property (nonatomic, weak) IBOutlet UIButton *btnTeethingBaby9;
@property (nonatomic, weak) IBOutlet UIButton *btnTeethingBaby10;
@property (nonatomic, weak) IBOutlet UIButton *btnTeethingBaby11;
@property (nonatomic, weak) IBOutlet UIButton *btnTeethingBaby12;
@property (nonatomic, weak) IBOutlet UIButton *btnTeethingBaby13;
@property (nonatomic, weak) IBOutlet UIButton *btnTeethingBaby14;
@property (nonatomic, weak) IBOutlet UIButton *btnTeethingBaby15;
@property (nonatomic, weak) IBOutlet UIButton *btnTeethingBaby16;
@property (nonatomic, weak) IBOutlet UIButton *btnTeethingBaby17;
@property (nonatomic, weak) IBOutlet UIButton *btnTeethingBaby18;
@property (nonatomic, weak) IBOutlet UIButton *btnTeethingBaby19;
@property (nonatomic, weak) IBOutlet UIButton *btnTeethingBaby20;

@property (nonatomic, weak) IBOutlet UIView *viewTeethingBaby;
@property (nonatomic, weak) IBOutlet UIView *viewTeethBaby;
@property (nonatomic, weak) IBOutlet UIView *viewTeethGuideBaby;

@property (nonatomic, weak) IBOutlet UIButton *btnBaby;
@property (nonatomic, weak) IBOutlet UILabel *lblTeethingBabyUpper;
@property (nonatomic, weak) IBOutlet UILabel *lblTeethingBabyLower;

// Adult

@property (nonatomic, weak) IBOutlet UIView *viewTeethingAdult1;
@property (nonatomic, weak) IBOutlet UIView *viewTeethingAdult2;
@property (nonatomic, weak) IBOutlet UIView *viewTeethingAdult3;
@property (nonatomic, weak) IBOutlet UIView *viewTeethingAdult4;
@property (nonatomic, weak) IBOutlet UIView *viewTeethingAdult5;

@property (nonatomic, weak) IBOutlet UILabel *lblTeethingAdult1;
@property (nonatomic, weak) IBOutlet UILabel *lblTeethingAdult2;
@property (nonatomic, weak) IBOutlet UILabel *lblTeethingAdult3;
@property (nonatomic, weak) IBOutlet UILabel *lblTeethingAdult4;
@property (nonatomic, weak) IBOutlet UILabel *lblTeethingAdult5;

@property (nonatomic, weak) IBOutlet UIView *viewTeethingAdult;
@property (nonatomic, weak) IBOutlet UIView *viewTeethAdult;
@property (nonatomic, weak) IBOutlet UIView *viewTeethGuideAdult;

@property (nonatomic, weak) IBOutlet UIButton *btnAdult;
@property (nonatomic, weak) IBOutlet UILabel *lblTeethingAdultUpper;
@property (nonatomic, weak) IBOutlet UILabel *lblTeethingAdultLower;

// Adult Button Teeths
@property (nonatomic, weak) IBOutlet UIButton *btnTeethingAdult1;
@property (nonatomic, weak) IBOutlet UIButton *btnTeethingAdult2;
@property (nonatomic, weak) IBOutlet UIButton *btnTeethingAdult3;
@property (nonatomic, weak) IBOutlet UIButton *btnTeethingAdult4;
@property (nonatomic, weak) IBOutlet UIButton *btnTeethingAdult5;
@property (nonatomic, weak) IBOutlet UIButton *btnTeethingAdult6;
@property (nonatomic, weak) IBOutlet UIButton *btnTeethingAdult7;
@property (nonatomic, weak) IBOutlet UIButton *btnTeethingAdult8;
@property (nonatomic, weak) IBOutlet UIButton *btnTeethingAdult9;
@property (nonatomic, weak) IBOutlet UIButton *btnTeethingAdult10;
@property (nonatomic, weak) IBOutlet UIButton *btnTeethingAdult11;
@property (nonatomic, weak) IBOutlet UIButton *btnTeethingAdult12;
@property (nonatomic, weak) IBOutlet UIButton *btnTeethingAdult13;
@property (nonatomic, weak) IBOutlet UIButton *btnTeethingAdult14;
@property (nonatomic, weak) IBOutlet UIButton *btnTeethingAdult15;
@property (nonatomic, weak) IBOutlet UIButton *btnTeethingAdult16;
@property (nonatomic, weak) IBOutlet UIButton *btnTeethingAdult17;
@property (nonatomic, weak) IBOutlet UIButton *btnTeethingAdult18;
@property (nonatomic, weak) IBOutlet UIButton *btnTeethingAdult19;
@property (nonatomic, weak) IBOutlet UIButton *btnTeethingAdult20;
@property (nonatomic, weak) IBOutlet UIButton *btnTeethingAdult21;
@property (nonatomic, weak) IBOutlet UIButton *btnTeethingAdult22;
@property (nonatomic, weak) IBOutlet UIButton *btnTeethingAdult23;
@property (nonatomic, weak) IBOutlet UIButton *btnTeethingAdult24;
@property (nonatomic, weak) IBOutlet UIButton *btnTeethingAdult25;
@property (nonatomic, weak) IBOutlet UIButton *btnTeethingAdult26;
@property (nonatomic, weak) IBOutlet UIButton *btnTeethingAdult27;
@property (nonatomic, weak) IBOutlet UIButton *btnTeethingAdult28;
@property (nonatomic, weak) IBOutlet UIButton *btnTeethingAdult29;
@property (nonatomic, weak) IBOutlet UIButton *btnTeethingAdult30;
@property (nonatomic, weak) IBOutlet UIButton *btnTeethingAdult31;
@property (nonatomic, weak) IBOutlet UIButton *btnTeethingAdult32;

// Keywords View
@property (nonatomic, weak) IBOutlet UIButton *btnaddKeyword;

@property (weak, nonatomic) IBOutlet UIView *keywordView;
@property (nonatomic, weak) IBOutlet UILabel *keyword1;
@property (nonatomic, weak) IBOutlet UILabel *keyword2;
@property (nonatomic, weak) IBOutlet UILabel *keyword3;
@property (nonatomic, weak) IBOutlet UILabel *keyword4;
@property (nonatomic, weak) IBOutlet UILabel *keyword5;
@property (nonatomic, weak) IBOutlet UILabel *keyword6;
@property (nonatomic, weak) IBOutlet UILabel *keyword7;
@property (nonatomic, weak) IBOutlet UILabel *keyword8;
@property (nonatomic, weak) IBOutlet UILabel *keyword9;

@end
