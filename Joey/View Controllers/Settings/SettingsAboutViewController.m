//
//  SettingsAboutViewController.m
//  Joey
//
//  Created by csl on 24/1/2017.
//  Copyright © 2017 Auxilia. All rights reserved.
//

#import "AppDelegate.h"
#import "SettingsAboutViewController.h"

@interface SettingsAboutViewController ()
{
    NSUserDefaults *shareObject;
}
@end

@implementation SettingsAboutViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    shareObject = [NSUserDefaults standardUserDefaults];
    
    [self.navigationController setNavigationBarHidden:NO];
    [self.navigationController.navigationBar setBarTintColor:UIColorFromRGB(0xEEE8E4)];
    [self.navigationController.navigationBar setTranslucent:NO];
    [self.navigationItem setTitle:LocalizedString(@"title_settings_about", nil)];
    [self customBackButton];
    
    CGFloat termsFontSize = 14.0f;
    if (IS_IPAD) {
        self.lblTitle.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:28.0f];
        self.lblMessage.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:22.0f];
        self.btnTerms.titleLabel.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:22.0f];
        self.btnPrivacy.titleLabel.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:22.0f];
        self.btnPrivacyHeightConstraints.constant = 50;
        self.btnTermsHeightConstraints.constant = 50;
        self.logoWidth.constant = self.logoWidth.constant + 10;
        self.logoHeight.constant = self.logoHeight.constant + 10;
        termsFontSize = 22.0f;
    }

    self.lblTitle.text = LocalizedString(@"txt_about_title", nil);
    self.lblMessage.text = [NSString stringWithFormat:LocalizedString(@"txt_about_message", nil), [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"]];
    self.lblMessage.urlLinkTapHandler = ^(KILabel *label, NSString *string, NSRange range) {
        NSLog(@"URL tapped %@", string);
        
        NSString *url = @"";
//        if([string isEqualToString:@"www.joeyforparents.com/support"]){
//            url = @"http://www.joeyforparents.com/support";
//        }
        if([string isEqualToString:@"help@joeyforparents.com"]){
            url = @"mailto:help@joeyforparents.com";
        }
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Settings" bundle:nil];
        UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"SettingsAboutWebViewController"];
        viewController.data = [[NSMutableDictionary alloc] initWithDictionary:@{@"title":LocalizedString(@"title_settings_about", nil), @"url":url}];
        [self.navigationController pushViewController:viewController animated:YES];
    };
    
    NSMutableAttributedString *termsString = [[NSMutableAttributedString alloc] initWithString:LocalizedString(@"txt_about_terms", nil) attributes:@{NSFontAttributeName:[UIFont fontWithName:@"SFUIDisplay-Medium" size:termsFontSize], NSForegroundColorAttributeName:UIColorFromRGB(0x8D8082), NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle)}];
    [self.btnTerms setAttributedTitle:termsString forState:UIControlStateNormal];
    
    NSMutableAttributedString *privacyString = [[NSMutableAttributedString alloc] initWithString:LocalizedString(@"txt_about_privacy", nil) attributes:@{NSFontAttributeName:[UIFont fontWithName:@"SFUIDisplay-Medium" size:termsFontSize], NSForegroundColorAttributeName:UIColorFromRGB(0x8D8082), NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle)}];
    [self.btnPrivacy setAttributedTitle:privacyString forState:UIControlStateNormal];
}

- (void)customBackButton
{
    if([self.navigationController.viewControllers count] > 1)
    {
        UIImage *imgBack = [UIImage imageNamed:@"nav_btn_back"];
        UIButton *btnBack = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnBack setImage:imgBack forState:UIControlStateNormal];
        btnBack.frame = CGRectMake(0, 0, imgBack.size.width, imgBack.size.height);
        [btnBack addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
        [btnBack setImageEdgeInsets:UIEdgeInsetsMake(0, -16, 0, 16)];
        UIBarButtonItem *backBarItem = [[UIBarButtonItem alloc] initWithCustomView:btnBack];
        self.navigationItem.leftBarButtonItem = backBarItem;
    }
}

- (void)backAction
{
    if([self.data objectForKey:@"signup_check"])
    {
        [self.navigationController.navigationBar setTranslucent:YES];
        self.navigationController.view.backgroundColor = [UIColor clearColor];
        [self.navigationController.navigationBar setBackgroundImage:[[UIImage alloc] init] forBarMetrics:UIBarMetricsDefault];
        self.navigationController.navigationBar.shadowImage = [[UIImage alloc] init];
        self.navigationController.navigationBar.backgroundColor = [UIColor clearColor];
    }
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)getTermsAction:(id)sender
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Settings" bundle:nil];
    UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"SettingsAboutWebViewController"];
    viewController.data = [[NSMutableDictionary alloc] initWithDictionary:@{@"title":LocalizedString(@"title_settings_about", nil), @"url":@"http://www.joeyforparents.com/mobile-app-terms-of-use"}];
    [self.navigationController pushViewController:viewController animated:YES];
}

- (IBAction)getPrivacyAction:(id)sender
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Settings" bundle:nil];
    UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"SettingsAboutWebViewController"];
    viewController.data = [[NSMutableDictionary alloc] initWithDictionary:@{@"title":LocalizedString(@"title_settings_about", nil), @"url":@"http://www.joeyforparents.com/privacy-policy"}];
    [self.navigationController pushViewController:viewController animated:YES];
}

@end
