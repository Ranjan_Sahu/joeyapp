//
//  SettingsFamilyViewController.h
//  Joey
//
//  Created by csl on 24/1/2017.
//  Copyright © 2017 Auxilia. All rights reserved.
//

#import "BaseViewController.h"

@interface SettingsFamilyViewController : BaseViewController <UINavigationControllerDelegate,UICollectionViewDelegate, UICollectionViewDataSource>

@property (nonatomic, weak) IBOutlet UIBarButtonItem *btnAdd;
@property (nonatomic, weak) IBOutlet UITableView *tableView;

- (void)updateBabyView:(int)familyIndex;
- (void)saveFamilyNameAction:(NSString *)familyName;

@end
