//
//  SettingsViewController.h
//  Joey
//
//  Created by csl on 23/1/2017.
//  Copyright © 2017 Auxilia. All rights reserved.
//

#import "BaseViewController.h"

@interface SettingsViewController : BaseViewController

@property (nonatomic, weak) IBOutlet UIButton *btnLogout;
@property (nonatomic, weak) IBOutlet UITableView *tableView;

@end
