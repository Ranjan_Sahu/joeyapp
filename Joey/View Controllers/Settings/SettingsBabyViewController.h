//
//  SettingsBabyViewController.h
//  Joey
//
//  Created by csl on 3/2/2017.
//  Copyright © 2017 Auxilia. All rights reserved.
//

#import "BaseTableViewController.h"
#import <Photos/Photos.h>
#import <AVFoundation/AVFoundation.h>


@interface SettingsBabyViewController : BaseTableViewController <UINavigationControllerDelegate, UIImagePickerControllerDelegate>

@property (nonatomic, weak) IBOutlet UIButton *btnAddImage;
@property (nonatomic, weak) IBOutlet UIButton *btnRemoveImage;
@property (nonatomic, weak) IBOutlet UIScrollView *scrollView;
@property (nonatomic, weak) IBOutlet UIView *contentView;
@property (nonatomic, weak) IBOutlet UIView *cropView;
@property (nonatomic, weak) IBOutlet UIImageView *maskView;
@property (nonatomic, weak) IBOutlet UIImageView *photoView;
@property (nonatomic, weak) IBOutlet UIBarButtonItem *btnSave;
@property (nonatomic, weak) IBOutlet UILabel *lblName;
@property (nonatomic, weak) IBOutlet UILabel *lblBirthDate;
@property (nonatomic, weak) IBOutlet UILabel *lblGender;
@property (nonatomic, weak) IBOutlet UILabel *lblWeight;
@property (nonatomic, weak) IBOutlet UILabel *lblHeight;
@property (nonatomic, weak) IBOutlet UILabel *lblHeadSize;
@property (nonatomic, weak) IBOutlet UILabel *lblBloodType;
@property (nonatomic, weak) IBOutlet UILabel *txtBirthDate;
@property (nonatomic, weak) IBOutlet UILabel *txtWeight;
@property (nonatomic, weak) IBOutlet UILabel *txtHeight;
@property (nonatomic, weak) IBOutlet UILabel *txtHeadSize;
@property (nonatomic, weak) IBOutlet UILabel *txtBloodType;
@property (nonatomic, weak) IBOutlet UIButton *btnGender1;
@property (nonatomic, weak) IBOutlet UIButton *btnGender2;
@property (nonatomic, weak) IBOutlet UIButton *btnDelete;
@property (nonatomic, weak) IBOutlet UITextField *txtName;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *btnDeleteHeightConstraint;

@end
