//
//  SettingsExportViewController.h
//  Joey
//
//  Created by csl on 24/1/2017.
//  Copyright © 2017 Auxilia. All rights reserved.
//

#import "BaseViewController.h"
#import <MessageUI/MessageUI.h>

@interface SettingsExportViewController : BaseViewController<UIScrollViewDelegate, UITableViewDelegate, UITableViewDataSource,MFMailComposeViewControllerDelegate>

@property (nonatomic, weak) IBOutlet UITableView *tblView;

@property (nonatomic, weak) IBOutlet UIScrollView *babyScrollView;

@property (nonatomic, weak) IBOutlet UIImageView *imgBabyPic1;
@property (nonatomic, weak) IBOutlet UIImageView *imgBabyPic2;
@property (nonatomic, weak) IBOutlet UIImageView *imgBabyPic3;
@property (nonatomic, weak) IBOutlet UILabel *txtName1;
@property (nonatomic, weak) IBOutlet UILabel *txtInfo1;
@property (nonatomic, weak) IBOutlet UILabel *txtName2;
@property (nonatomic, weak) IBOutlet UILabel *txtInfo2;
@property (nonatomic, weak) IBOutlet UILabel *txtName3;
@property (nonatomic, weak) IBOutlet UILabel *txtInfo3;

@property (nonatomic, weak) IBOutlet UILabel *lblStart;
@property (nonatomic, weak) IBOutlet UILabel *lblEnd;
@property (nonatomic, weak) IBOutlet UILabel *txtStart;
@property (nonatomic, weak) IBOutlet UILabel *txtEnd;

@end
