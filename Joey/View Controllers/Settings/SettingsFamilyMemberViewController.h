//
//  SettingsFamilyMemberViewController.h
//  Joey
//
//  Created by csl on 13/2/2017.
//  Copyright © 2017 Auxilia. All rights reserved.
//

#import "BaseTableViewController.h"

@interface SettingsFamilyMemberViewController : BaseTableViewController

@property (nonatomic, weak) IBOutlet UIBarButtonItem *btnInvite;
@property (nonatomic, weak) IBOutlet UILabel *lblName;
@property (nonatomic, weak) IBOutlet UILabel *lblEmail;
@property (nonatomic, weak) IBOutlet UILabel *lblRelationship;
@property (nonatomic, weak) IBOutlet UILabel *txtRelationship;
@property (nonatomic, weak) IBOutlet UITextField *txtName;
@property (nonatomic, weak) IBOutlet UITextField *txtEmail;
@property (weak, nonatomic) IBOutlet UILabel *lblAssignMaster;
@property (weak, nonatomic) IBOutlet UILabel *lblDescription;
@property (weak, nonatomic) IBOutlet UISwitch *switchMaster;
@property (weak, nonatomic) IBOutlet UIImageView *assignMasterBorderImage;

@end
