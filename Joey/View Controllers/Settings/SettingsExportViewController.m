//
//  SettingsExportViewController.m
//  Joey
//
//  Created by csl on 24/1/2017.
//  Copyright © 2017 Auxilia. All rights reserved.
//

#import "AppDelegate.h"
#import "SettingsExportViewController.h"
#import "NotificationsTableViewCell.h"

@interface SettingsExportViewController ()
{
    NSUserDefaults *shareObject;
    NSDateFormatter *formatter;
    NSCalendar *calendar;
    NSDate *selectedStartDate;
    NSDate *selectedEndDate;
    NSMutableArray *babyList;
    NSMutableArray *activityList;
    NSMutableArray *milestoneList;
    NSMutableDictionary *unitInfo;
    
    int selectedBabyIndex;
    
    AppDelegate *appDelegate;
    NSMutableArray *exportList;
    NSMutableArray *iconColorList;
    NSMutableArray *iconImageList;
    NSMutableDictionary *list;
    NSMutableArray *switchValues;
    int backupPageNo;
    NSString *babyName;

}
@end

@implementation SettingsExportViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    shareObject = [NSUserDefaults standardUserDefaults];
    formatter = [[NSDateFormatter alloc] init];
    [formatter setTimeZone:[NSTimeZone localTimeZone]];
    [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
    calendar = [[NSCalendar alloc] initWithCalendarIdentifier: NSCalendarIdentifierGregorian];
    unitInfo = (NSMutableDictionary *)[NSKeyedUnarchiver unarchiveObjectWithData:[shareObject objectForKey:@"unit_info"]];
    
    activityList = [[NSMutableArray alloc] init];
    milestoneList = [[NSMutableArray alloc] init];
    
    [self.navigationController setNavigationBarHidden:NO];
    [self.navigationController.navigationBar setBarTintColor:UIColorFromRGB(0xEEE8E4)];
    [self.navigationController.navigationBar setTranslucent:NO];
    [self.navigationItem setTitle:LocalizedString(@"txt_export", nil)];
    [self customBackButton];
    [self customExportButton];
    
    self.lblStart.text = LocalizedString(@"txt_start", nil);
    self.lblEnd.text = LocalizedString(@"txt_end", nil);
    
    if (IS_IPAD) {
        _lblStart.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        _txtStart.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        _lblEnd.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        _txtEnd.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        
        self.txtName1.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.txtInfo1.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:22.0f];
        self.txtName2.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.txtInfo2.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:22.0f];
        self.txtName3.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.txtInfo3.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:22.0f];

    }
    
    selectedStartDate = [NSDate date];
    NSLog(@"selectedStartDate=%@", selectedStartDate);
    NSDateComponents *components = [calendar components:NSUIntegerMax fromDate:[NSDate date]];
    [components setHour:0];
    [components setMinute:0];
    [components setSecond:0];
    selectedStartDate = [calendar dateFromComponents:components];
    NSLog(@"selectedStartDate=%@", selectedStartDate);
    
    components = [calendar components:NSUIntegerMax fromDate:[NSDate date]];
    [components setHour:23];
    [components setMinute:59];
    [components setSecond:59];
    selectedEndDate = [calendar dateFromComponents:components];
    NSLog(@"selectedEndDate=%@", selectedEndDate);
    
    if([[shareObject objectForKey:@"language"] hasPrefix:@"zh"])
    {
        [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"zh_HK"]];
        [formatter setDateStyle:NSDateFormatterMediumStyle];
        [formatter setTimeStyle:NSDateFormatterNoStyle];
    }
    else
    {
        [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
        formatter.dateFormat = @"EE d MMM, yyyy";
    }
    self.txtStart.text = [NSString stringWithFormat:@"%@", [formatter stringFromDate:selectedStartDate]];
    self.txtEnd.text = [NSString stringWithFormat:@"%@", [formatter stringFromDate:selectedEndDate]];
    
    UITapGestureRecognizer *tapRecognizer1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTapOnTxtStart)];
    tapRecognizer1.numberOfTapsRequired = 1;
    self.txtStart.userInteractionEnabled = YES;
    [self.txtStart addGestureRecognizer:tapRecognizer1];

    UITapGestureRecognizer *tapRecognizer2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTapOnTxtEnd)];
    tapRecognizer2.numberOfTapsRequired = 1;
    self.txtEnd.userInteractionEnabled = YES;
    [self.txtEnd addGestureRecognizer:tapRecognizer2];

    
    exportList = [[NSMutableArray alloc] initWithObjects:LocalizedString(@"txt_babyReportCard", nil), LocalizedString(@"feed", nil), LocalizedString(@"pump", nil),  LocalizedString(@"sleep", nil), LocalizedString(@"poo", nil), LocalizedString(@"wee", nil), LocalizedString(@"medical", nil), LocalizedString(@"milestones", nil),  nil];
    iconColorList = [[NSMutableArray alloc] initWithObjects:UIColorFromRGB(0xE25F0F), UIColorFromRGB(0xF4EA9E), UIColorFromRGB(0xF9C99B), UIColorFromRGB(0xC5D7E1), UIColorFromRGB(0xE0C5B8), UIColorFromRGB(0xCADAA9), UIColorFromRGB(0xF4D2D5), UIColorFromRGB(0xE2D3CA), nil];
    iconImageList = [[NSMutableArray alloc] initWithObjects:@"icon_repotCard", @"icon_feed", @"icon_pump", @"icon_sleep", @"icon_poo", @"icon_wee", @"icon_medical", @"icon_milestones", nil];
    
    babyName = @"";
    list = [[NSMutableDictionary alloc] init];
    
    [self.tblView registerNib:[UINib nibWithNibName:@"NotificationsTableViewCell" bundle:nil] forCellReuseIdentifier:@"NotificationsTableViewCell"];
    
    self.tblView.delegate = self;
    self.tblView.dataSource = self;
    self.babyScrollView.delegate = self;
    [self setupBabyList];
    
}

#pragma mark - UI Customization & Layout Hadling Methods
- (void)customBackButton
{
    if([self.navigationController.viewControllers count] > 1)
    {
        UIImage *imgBack = [UIImage imageNamed:@"nav_btn_back"];
        UIButton *btnBack = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnBack setImage:imgBack forState:UIControlStateNormal];
        btnBack.frame = CGRectMake(0, 0, imgBack.size.width, imgBack.size.height);
        [btnBack addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
        [btnBack setImageEdgeInsets:UIEdgeInsetsMake(0, -16, 0, 16)];
        UIBarButtonItem *backBarItem = [[UIBarButtonItem alloc] initWithCustomView:btnBack];
        self.navigationItem.leftBarButtonItem = backBarItem;
    }
}

-(void)customExportButton
{
    UIImage *imgExport = [UIImage imageNamed:@"nav_btn_export"];
    UIButton *btnExport = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnExport setImage:imgExport forState:UIControlStateNormal];
    btnExport.frame = CGRectMake(52, 0, imgExport.size.width, imgExport.size.height);
    [btnExport addTarget:self action:@selector(exportAction:) forControlEvents:UIControlEventTouchUpInside];
    
    UIView *buttonView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 88, 44)];
    [buttonView addSubview:btnExport];
    
    UIBarButtonItem *rightBarItem = [[UIBarButtonItem alloc] initWithCustomView:buttonView];
    self.navigationItem.rightBarButtonItem = rightBarItem;
}

- (void)viewDidLayoutSubviews
{
    if(self.babyScrollView.contentSize.width == 0)
    {
        self.babyScrollView.contentSize = CGSizeMake(self.view.frame.size.width*3, 1);
        self.babyScrollView.contentOffset = CGPointMake(self.view.frame.size.width, self.babyScrollView.contentOffset.y);
    }
    
    self.imgBabyPic1.clipsToBounds = self.imgBabyPic2.clipsToBounds = self.imgBabyPic3.clipsToBounds = YES;
    self.imgBabyPic1.layer.cornerRadius = self.imgBabyPic1.frame.size.width/2;
    self.imgBabyPic2.layer.cornerRadius = self.imgBabyPic2.frame.size.width/2;
    self.imgBabyPic3.layer.cornerRadius = self.imgBabyPic3.frame.size.width/2;
}

#pragma mark - Custom Actions
- (void)backAction
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)didTapOnTxtStart
{
    NSDate *minimumDate = nil;
    NSDate *maximumDate = [NSDate date];
    [ActionSheetDatePicker showPickerWithTitle:@"" datePickerMode:UIDatePickerModeDate selectedDate:selectedStartDate minimumDate:minimumDate maximumDate:maximumDate doneBlock:^(ActionSheetDatePicker *picker, id selectedDate, id origin) {
        
        selectedStartDate = selectedDate;
        
        NSDateComponents *components = [calendar components:NSUIntegerMax fromDate:selectedStartDate];
        [components setHour:0];
        [components setMinute:0];
        [components setSecond:0];
        selectedStartDate = [calendar dateFromComponents:components];
        NSLog(@"selectedStartDate=%@", selectedStartDate);
        
        if([[shareObject objectForKey:@"language"] hasPrefix:@"zh"])
        {
            [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"zh_HK"]];
            [formatter setDateStyle:NSDateFormatterMediumStyle];
            [formatter setTimeStyle:NSDateFormatterNoStyle];
        }
        else
        {
            [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
            formatter.dateFormat = @"EE d MMM, yyyy";
        }
        self.txtStart.text = [formatter stringFromDate:selectedStartDate];
        
    } cancelBlock:^(ActionSheetDatePicker *picker) {
        
    } origin:self.view];
}

-(void)didTapOnTxtEnd
{
    NSDate *minimumDate = selectedStartDate;
    NSDate *maximumDate = [NSDate date];
    [ActionSheetDatePicker showPickerWithTitle:@"" datePickerMode:UIDatePickerModeDate selectedDate:selectedEndDate minimumDate:minimumDate maximumDate:maximumDate doneBlock:^(ActionSheetDatePicker *picker, id selectedDate, id origin) {
        
        selectedEndDate = selectedDate;
        NSLog(@"selectedEndDate=%@", selectedEndDate);
        NSDateComponents *components = [calendar components:NSUIntegerMax fromDate:selectedEndDate];
        [components setHour:23];
        [components setMinute:59];
        [components setSecond:59];
        selectedEndDate = [calendar dateFromComponents:components];
        NSLog(@"selectedEndDate=%@", selectedEndDate);
        
        if([[shareObject objectForKey:@"language"] hasPrefix:@"zh"])
        {
            [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"zh_HK"]];
            [formatter setDateStyle:NSDateFormatterMediumStyle];
            [formatter setTimeStyle:NSDateFormatterNoStyle];
        }
        else
        {
            [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
            formatter.dateFormat = @"EE d MMM, yyyy";
        }
        self.txtEnd.text = [formatter stringFromDate:selectedEndDate];
        
    } cancelBlock:^(ActionSheetDatePicker *picker) {
        
    } origin:self.view];
}

- (void)getSwitchAction:(UISwitch *)sender
{
    if (sender.tag == 0) {
        if (![[NSUserDefaults standardUserDefaults]boolForKey:@"is_Subscription_Enabled"]) {
            [sender setOn:NO];
            [self showAlertWithTitle:nil message:@"Baby Report Cards are only available with a Joey Premium subscription" input:nil];
            return;
        }
    }
    
    if (sender.isOn ){
        [switchValues replaceObjectAtIndex:sender.tag withObject:@1];
    }else {
        [switchValues replaceObjectAtIndex:sender.tag withObject:@0];
    }
}

- (IBAction)exportAction:(UIButton *)sender
{
    NSString *names = @"";
    NSMutableArray *namesArray = [NSMutableArray new];
    
    if([[switchValues objectAtIndex:0] boolValue])
    {
//        names = [NSString stringWithFormat:@"%@,babyReportCard", names];
        [namesArray addObject:@"babyReportCard"];
    }
    if([[switchValues objectAtIndex:1] boolValue])
    {
//        names = [NSString stringWithFormat:@"%@,feed", names];
        [namesArray addObject:@" feed"];
    }
    if([[switchValues objectAtIndex:2] boolValue])
    {
//        names = [NSString stringWithFormat:@"%@,pump", names];
        [namesArray addObject:@"pump"];
    }
    if([[switchValues objectAtIndex:3] boolValue])
    {
//        names = [NSString stringWithFormat:@"%@,sleep", names];
        [namesArray addObject:@"sleep"];
    }
    if([[switchValues objectAtIndex:4] boolValue])
    {
//        names = [NSString stringWithFormat:@"%@,poo", names];
        [namesArray addObject:@"poo"];
    }
    if([[switchValues objectAtIndex:5] boolValue])
    {
//        names = [NSString stringWithFormat:@"%@,wee", names];
        [namesArray addObject:@"wee"];
    }
    if([[switchValues objectAtIndex:6] boolValue])
    {
//        names = [NSString stringWithFormat:@"%@,medical", names];
        [namesArray addObject:@"medical"];
    }
    if([[switchValues objectAtIndex:7] boolValue])
    {
//        names = [NSString stringWithFormat:@"%@,milestones", names];
        [namesArray addObject:@"milestones"];
    }
    
    names = [namesArray componentsJoinedByString:@","];
    
    if(names.length > 0)
    {
        names = [names substringFromIndex:1];
    }
    else
    {
        [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:LocalizedString(@"error_export", nil) input:nil];
        return;
    }
    
    NSLog(@"%@",names);
    
    [SVProgressHUD show];
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setValue:@([shareObject integerForKey:@"user_id"]) forKey:@"user_id"];
    [parameters setValue:@([shareObject integerForKey:@"baby_id"]) forKey:@"baby_id"];
    [parameters setValue:names forKey:@"names"];
    [parameters setValue:selectedStartDate forKey:@"start_date"];
    [parameters setValue:selectedEndDate forKey:@"end_date"];
    
    [[WebServiceManager sharedManager] initWithBaseURL:@"api/getExportList" parameters:parameters success:^(id result) {

        if ([[result valueForKey:@"success"] boolValue]) {
            [self showAlertWithTitle:LocalizedString(@"Success", nil) message:@"Please check your email !" input:nil];
        }else {
            [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:[result objectForKey:@"message"] input:nil];
      }
        
//        activityList = [result valueForKey:@"activity_list"];
//        milestoneList = [result valueForKey:@"milestone_list"];
//
//        if(activityList.count == 0 && milestoneList.count == 0)
//        {
//            [self showAlertWithTitle:LocalizedString(@"title_sorry", nil) message:LocalizedString(@"error_export_no_records", nil) input:nil];
//            return;
//        }
//
//        [SVProgressHUD show];
//
//        babyList = (NSMutableArray *)[NSKeyedUnarchiver unarchiveObjectWithData:[shareObject objectForKey:@"baby_list"]];
//        NSDictionary *babyInfo =  [babyList objectAtIndex:[shareObject integerForKey:@"baby_index"]];
//        babyName = [babyInfo valueForKey:@"name"];
//
//        NSString *s = [NSString stringWithFormat:@"%@: %@\n%@: %@\n%@: %@\n\n", LocalizedString(@"txt_baby_name", nil), babyName, self.lblStart.text, self.txtStart.text, self.lblEnd.text, self.txtEnd.text];
//
//        if(activityList.count > 0)
//        {
//            s = [NSString stringWithFormat:@"%@Activity:\n", s];
//        }
//
//        for(int i=0; i<activityList.count; i++)
//        {
//            if(i == 0 || ![[[activityList objectAtIndex:i-1] objectForKey:@"name"] isEqualToString:[[activityList objectAtIndex:i] objectForKey:@"name"]])
//            {
//                s = [NSString stringWithFormat:@"%@\n%@\n", s, [[activityList objectAtIndex:i] objectForKey:@"name"]];
//            }
//
//            for(int j=0; j<[[[activityList objectAtIndex:i] objectForKey:@"list"] count]; j++)
//            {
//                NSString *name = [[[[activityList objectAtIndex:i] objectForKey:@"list"] objectAtIndex:j] objectForKey:@"name"];
//                NSString *type = [[[[activityList objectAtIndex:i] objectForKey:@"list"] objectAtIndex:j] objectForKey:@"type"];
//
//                if([name isEqualToString:@"feed"])
//                {
//                    if([type isEqualToString:@"solids"])
//                    {
//                        [[[[activityList objectAtIndex:i] objectForKey:@"list"] objectAtIndex:j] setObject:[self convertUnit:[[[[activityList objectAtIndex:i] objectForKey:@"list"] objectAtIndex:j] objectForKey:@"attribute_value"] unit:[unitInfo objectForKey:@"serving"] spacing:YES] forKey:@"attribute_value"];
//                    }
//                    else if([type isEqualToString:@"bbottle"] || [type isEqualToString:@"formula"] || [type isEqualToString:@"water"] || [type isEqualToString:@"juice"])
//                    {
//                        [[[[activityList objectAtIndex:i] objectForKey:@"list"] objectAtIndex:j] setObject:[self convertUnit:[[[[activityList objectAtIndex:i] objectForKey:@"list"] objectAtIndex:j] objectForKey:@"attribute_value"] unit:[unitInfo objectForKey:@"volume"] spacing:YES] forKey:@"attribute_value"];
//                    }
//                }
//                else if([name isEqualToString:@"pump"])
//                {
//                    [[[[activityList objectAtIndex:i] objectForKey:@"list"] objectAtIndex:j] setObject:[self convertUnit:[[[[activityList objectAtIndex:i] objectForKey:@"list"] objectAtIndex:j] objectForKey:@"attribute_value"] unit:[unitInfo objectForKey:@"volume"] spacing:YES] forKey:@"attribute_value"];
//                }
//                else if([name isEqualToString:@"medical"])
//                {
//                    if([type isEqualToString:@"temperature"])
//                    {
//                        [[[[activityList objectAtIndex:i] objectForKey:@"list"] objectAtIndex:j] setObject:[self convertUnit:[[[[activityList objectAtIndex:i] objectForKey:@"list"] objectAtIndex:j] objectForKey:@"attribute_value"] unit:[unitInfo objectForKey:@"temperature"] spacing:YES] forKey:@"attribute_value"];
//                    }
//                    else if([type containsString:@"med"])
//                    {
//                        [[[[activityList objectAtIndex:i] objectForKey:@"list"] objectAtIndex:j] setObject:[self convertUnit:[[[[activityList objectAtIndex:i] objectForKey:@"list"] objectAtIndex:j] objectForKey:@"attribute_value"] unit:[unitInfo objectForKey:@"medicine"] spacing:YES] forKey:@"attribute_value"];
//                    }
//                }
//
//                s = [NSString stringWithFormat:@"%@%@, %@, %@\n", s, [[[[activityList objectAtIndex:i] objectForKey:@"list"] objectAtIndex:j] objectForKey:@"time"], [[[[activityList objectAtIndex:i] objectForKey:@"list"] objectAtIndex:j] objectForKey:@"attribute_name"], [[[[activityList objectAtIndex:i] objectForKey:@"list"] objectAtIndex:j] objectForKey:@"attribute_value"]];
//
//            }
//        }
//
//        if(milestoneList.count > 0)
//        {
//            s = [NSString stringWithFormat:@"%@\n\n\nMilestone:\n", s];
//        }
//
//        for(int i=0; i<milestoneList.count; i++)
//        {
//            if(i == 0 || ![[[milestoneList objectAtIndex:i-1] objectForKey:@"name"] isEqualToString:[[milestoneList objectAtIndex:i] objectForKey:@"name"]])
//            {
//                s = [NSString stringWithFormat:@"%@\n%@\n", s, [[milestoneList objectAtIndex:i] objectForKey:@"name"]];
//            }
//
//            for(int j=0; j<[[[milestoneList objectAtIndex:i] objectForKey:@"list"] count]; j++)
//            {
//                NSString *name = [[[[milestoneList objectAtIndex:i] objectForKey:@"list"] objectAtIndex:j] objectForKey:@"name"];
//                NSString *type = [[[[milestoneList objectAtIndex:i] objectForKey:@"list"] objectAtIndex:j] objectForKey:@"type"];
//
//                NSLog(@"type=%@", type);
//                if([name isEqualToString:@"grow"])
//                {
//                    if([type isEqualToString:@"weight"])
//                    {
//                        [[[[milestoneList objectAtIndex:i] objectForKey:@"list"] objectAtIndex:j] setObject:[self convertUnit:[[[[milestoneList objectAtIndex:i] objectForKey:@"list"] objectAtIndex:j] objectForKey:@"attribute_value"] unit:[unitInfo objectForKey:@"weight"] spacing:YES] forKey:@"attribute_value"];
//
//                    }
//                    else if([type isEqualToString:@"height"])
//                    {
//                        [[[[milestoneList objectAtIndex:i] objectForKey:@"list"] objectAtIndex:j] setObject:[self convertUnit:[[[[milestoneList objectAtIndex:i] objectForKey:@"list"] objectAtIndex:j] objectForKey:@"attribute_value"] unit:[unitInfo objectForKey:@"height"] spacing:YES] forKey:@"attribute_value"];
//                    }
//                    else if([type isEqualToString:@"head_size"])
//                    {
//                        [[[[milestoneList objectAtIndex:i] objectForKey:@"list"] objectAtIndex:j] setObject:[self convertUnit:[[[[milestoneList objectAtIndex:i] objectForKey:@"list"] objectAtIndex:j] objectForKey:@"attribute_value"] unit:[unitInfo objectForKey:@"head_size"] spacing:YES] forKey:@"attribute_value"];
//                    }
//                }
//
//                s = [NSString stringWithFormat:@"%@%@, %@, %@\n", s, [[[[milestoneList objectAtIndex:i] objectForKey:@"list"] objectAtIndex:j] objectForKey:@"time"], [[[[milestoneList objectAtIndex:i] objectForKey:@"list"] objectAtIndex:j] objectForKey:@"attribute_name"], [[[[milestoneList objectAtIndex:i] objectForKey:@"list"] objectAtIndex:j] objectForKey:@"attribute_value"]];
//            }
//        }
//        NSLog(@"s=%@", s);
//        [self saveFile:s];

    } failure:^(NSError *error) {
        [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:LocalizedString(@"error_unknown", nil) input:nil];
    }];
}

- (NSString *)dataFilePath
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    return [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.csv", babyName]];
    return @"";
}

- (void)saveFile:(NSString *)string
{
    if(![[NSFileManager defaultManager] fileExistsAtPath:[self dataFilePath]])
    {
        [[NSFileManager defaultManager] createFileAtPath: [self dataFilePath] contents:nil attributes:nil];
    }

    NSFileHandle *handle;
    handle = [NSFileHandle fileHandleForWritingAtPath:[self dataFilePath]];
    //say to handle where's the file fo write
    [handle truncateFileAtOffset:[handle seekToEndOfFile]];
    //position handle cursor to the end of file
    [handle writeData:[string dataUsingEncoding:NSUTF8StringEncoding]];

    if (![MFMailComposeViewController canSendMail ]) {
        [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:@"Can not send e-mail. " input:nil];
        return;
    }else {
        MFMailComposeViewController *controller = [[MFMailComposeViewController alloc] init];
        [controller addAttachmentData:[NSData dataWithContentsOfFile:[self dataFilePath]]
                             mimeType:@"text/csv"
                             fileName:[NSString stringWithFormat:@"%@.csv", babyName]];
        [controller setSubject:[NSString stringWithFormat:LocalizedString(@"txt_export_summary", nil), babyName]];
        [controller setMessageBody:string isHTML:NO];
        [controller setMailComposeDelegate:self];
        [self presentViewController:controller animated:YES completion:^{
            [SVProgressHUD dismiss];
        }];
    }
}

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    BOOL isSent;
    switch(result)
    {
        case MFMailComposeResultSent: {
            isSent = YES;
            break;
        }
        default:
            isSent = NO;
        break;
    }
    
    [self dismissViewControllerAnimated:YES completion:^{
        NSError *error;
        if ([[NSFileManager defaultManager] isDeletableFileAtPath:[self dataFilePath]]) {
            BOOL success = [[NSFileManager defaultManager] removeItemAtPath:[self dataFilePath] error:&error];
            if (!success) {
                NSLog(@"Error removing file at path: %@", error.localizedDescription);
            }
        }
    }];
}

#pragma mark - Table view data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return exportList.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}
- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NotificationsTableViewCell *cell = [self.tblView dequeueReusableCellWithIdentifier:@"NotificationsTableViewCell"];
    if(!cell){
        cell = [self.tblView dequeueReusableCellWithIdentifier:@"NotificationsTableViewCell"];
    }
    
    cell.imgIcon.image = nil;
    cell.imgIcon.image = [UIImage imageNamed:[iconImageList objectAtIndex:indexPath.row]];
    cell.imgIcon.layer.cornerRadius = cell.imgIcon.frame.size.width/2;
    cell.imgIcon.backgroundColor = [iconColorList objectAtIndex:indexPath.row];
    
    if (IS_IPAD) {
        cell.lblTitle.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
    }
    
    cell.lblTitle.text = [exportList objectAtIndex:indexPath.row];
    cell.toggleSwitch.tag = indexPath.row;
    [cell.toggleSwitch addTarget: self action: @selector(getSwitchAction:) forControlEvents:UIControlEventValueChanged];
    cell.toggleSwitch.on = [[switchValues objectAtIndex:indexPath.row] boolValue];
    
    cell.toggleSwitch.alpha = 1.0;
    cell.lblTitle.alpha = 1.0;
    cell.imgIcon.alpha = 1.0;
    
    if (![[NSUserDefaults standardUserDefaults]boolForKey:@"is_Subscription_Enabled"]) {
        if (indexPath.row == 0) {
            cell.toggleSwitch.on = NO;
            cell.toggleSwitch.alpha = 0.5;
            cell.lblTitle.alpha = 0.5;
            cell.imgIcon.alpha = 0.5;
        }
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//    if(indexPath.row == 0)
//    {
//        [ActionSheetStringPicker showPickerWithTitle:@"" rows:babyNameList initialSelection:selectedBabyIndex doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
//
//            selectedBabyIndex = (int)selectedIndex;
//            self.txtBaby.text = [babyNameList objectAtIndex:selectedBabyIndex];
//
//        } cancelBlock:^(ActionSheetStringPicker *picker) {
//
//        } origin:self.view];
//    }
//    if(indexPath.row == 1)
//    {
//        NSDate *minimumDate = nil;
//        NSDate *maximumDate = [NSDate date];
//        [ActionSheetDatePicker showPickerWithTitle:@"" datePickerMode:UIDatePickerModeDate selectedDate:selectedStartDate minimumDate:minimumDate maximumDate:maximumDate doneBlock:^(ActionSheetDatePicker *picker, id selectedDate, id origin) {
//
//            selectedStartDate = selectedDate;
//            NSLog(@"selectedStartDate=%@", selectedStartDate);
//
//            NSDateComponents *components = [calendar components:NSUIntegerMax fromDate:selectedStartDate];
//            [components setHour:0];
//            [components setMinute:0];
//            [components setSecond:0];
//            selectedStartDate = [calendar dateFromComponents:components];
//            NSLog(@"selectedStartDate=%@", selectedStartDate);
//
//            if([[shareObject objectForKey:@"language"] hasPrefix:@"zh"])
//            {
//                [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"zh_HK"]];
//                [formatter setDateStyle:NSDateFormatterMediumStyle];
//                [formatter setTimeStyle:NSDateFormatterNoStyle];
//            }
//            else
//            {
//                [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
//                formatter.dateFormat = @"EE d MMM, yyyy";
//            }
//            self.txtStart.text = [formatter stringFromDate:selectedStartDate];
//
//        } cancelBlock:^(ActionSheetDatePicker *picker) {
//
//        } origin:self.view];
//    }
//    if(indexPath.row == 2)
//    {
//        NSDate *minimumDate = selectedStartDate;
//        NSDate *maximumDate = [NSDate date];
//        [ActionSheetDatePicker showPickerWithTitle:@"" datePickerMode:UIDatePickerModeDate selectedDate:selectedEndDate minimumDate:minimumDate maximumDate:maximumDate doneBlock:^(ActionSheetDatePicker *picker, id selectedDate, id origin) {
//
//            selectedEndDate = selectedDate;
//            NSLog(@"selectedEndDate=%@", selectedEndDate);
//            NSDateComponents *components = [calendar components:NSUIntegerMax fromDate:selectedEndDate];
//            [components setHour:23];
//            [components setMinute:59];
//            [components setSecond:59];
//            selectedEndDate = [calendar dateFromComponents:components];
//            NSLog(@"selectedEndDate=%@", selectedEndDate);
//
//            if([[shareObject objectForKey:@"language"] hasPrefix:@"zh"])
//            {
//                [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"zh_HK"]];
//                [formatter setDateStyle:NSDateFormatterMediumStyle];
//                [formatter setTimeStyle:NSDateFormatterNoStyle];
//            }
//            else
//            {
//                [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
//                formatter.dateFormat = @"EE d MMM, yyyy";
//            }
//            self.txtEnd.text = [formatter stringFromDate:selectedEndDate];
//
//        } cancelBlock:^(ActionSheetDatePicker *picker) {
//
//        } origin:self.view];
//    }
}

#pragma mark - View's data Configuration methos
- (void)setupBabyList
{
    babyList = (NSMutableArray *)[NSKeyedUnarchiver unarchiveObjectWithData:[shareObject objectForKey:@"baby_list"]];
    
    selectedBabyIndex = (int)[shareObject integerForKey:@"baby_index"];
    selectedBabyIndex = (selectedBabyIndex >= babyList.count)?0:selectedBabyIndex;
    NSLog(@"selectedBabyIndex1=%d", selectedBabyIndex);
    
    for(int i=0; i<babyList.count; i++)
    {
        NSLog(@"weight=%@", [[babyList objectAtIndex:i] objectForKey:@"weight"]);
        NSLog(@"unit=%@", [unitInfo objectForKey:@"weight"]);
        [[babyList objectAtIndex:i] setObject:[self convertUnit:[[babyList objectAtIndex:i] objectForKey:@"height"] unit:[unitInfo objectForKey:@"height"] spacing:YES] forKey:@"height"];
        [[babyList objectAtIndex:i] setObject:[self convertUnit:[[babyList objectAtIndex:i] objectForKey:@"weight"] unit:[unitInfo objectForKey:@"weight"] spacing:YES] forKey:@"weight"];
    }
    
    // Check for Subscription of selected baby
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"is_Subscription_Enabled"];
    if (![[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"subscription"] isKindOfClass:[NSNull class]]) {
        BOOL isSubscribed = [[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"subscription"] boolValue];
        [[NSUserDefaults standardUserDefaults]setBool:isSubscribed forKey:@"is_Subscription_Enabled"];
    }
    
    NSLog(@"babyList2=%@", babyList);
    if(babyList.count == 1)
    {
        self.babyScrollView.scrollEnabled = NO;
        [self.imgBabyPic2 sd_setImageWithURL:[NSURL URLWithString:[[babyList objectAtIndex:0] objectForKey:@"photo_url"]] placeholderImage:[UIImage imageNamed:@"dummy.png"]];
        self.txtName2.text = [[babyList objectAtIndex:0] objectForKey:@"name"];
        self.txtInfo2.text = [NSString stringWithFormat:LocalizedString(@"txt_baby_info", nil), [[babyList objectAtIndex:0] objectForKey:@"age"], [[babyList objectAtIndex:0] objectForKey:@"height"], [[babyList objectAtIndex:0] objectForKey:@"weight"], [[babyList objectAtIndex:0] objectForKey:@"blood_type"]];
    }
    else
    {
        int prevIndex = selectedBabyIndex-1;
        prevIndex = (prevIndex < 0)?(int)(babyList.count-1):prevIndex;
        int nextIndex = selectedBabyIndex+1;
        nextIndex = (nextIndex >= babyList.count)?0:nextIndex;
        
        self.babyScrollView.scrollEnabled = YES;
        [self.imgBabyPic1 sd_setImageWithURL:[NSURL URLWithString:[[babyList objectAtIndex:prevIndex] objectForKey:@"photo_url"]] placeholderImage:[UIImage imageNamed:@"dummy.png"]];
        self.txtName1.text = [[babyList objectAtIndex:prevIndex] objectForKey:@"name"];
        self.txtInfo1.text = [NSString stringWithFormat:LocalizedString(@"txt_baby_info", nil), [[babyList objectAtIndex:prevIndex] objectForKey:@"age"], [[babyList objectAtIndex:prevIndex] objectForKey:@"height"], [[babyList objectAtIndex:prevIndex] objectForKey:@"weight"], [[babyList objectAtIndex:prevIndex] objectForKey:@"blood_type"]];
        
        [self.imgBabyPic2 sd_setImageWithURL:[NSURL URLWithString:[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"photo_url"]] placeholderImage:[UIImage imageNamed:@"dummy.png"]];
        self.txtName2.text = [[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"name"];
        self.txtInfo2.text = [NSString stringWithFormat:LocalizedString(@"txt_baby_info", nil), [[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"age"], [[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"height"], [[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"weight"], [[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"blood_type"]];
        
        [self.imgBabyPic3 sd_setImageWithURL:[NSURL URLWithString:[[babyList objectAtIndex:nextIndex] objectForKey:@"photo_url"]] placeholderImage:[UIImage imageNamed:@"dummy.png"]];
        self.txtName3.text = [[babyList objectAtIndex:nextIndex] objectForKey:@"name"];
        self.txtInfo3.text = [NSString stringWithFormat:LocalizedString(@"txt_baby_info", nil), [[babyList objectAtIndex:nextIndex] objectForKey:@"age"], [[babyList objectAtIndex:nextIndex] objectForKey:@"height"], [[babyList objectAtIndex:nextIndex] objectForKey:@"weight"], [[babyList objectAtIndex:nextIndex] objectForKey:@"blood_type"]];
    }
    
    [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"feed_info"]] forKey:@"feed_info"];
    [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"pump_info"]] forKey:@"pump_info"];
    [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"sleep_info"]] forKey:@"sleep_info"];
    [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"poo_info"]] forKey:@"poo_info"];
    [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"wee_info"]] forKey:@"wee_info"];
    [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"medical_info"]] forKey:@"medical_info"];
    [shareObject synchronize];
    
    [self setSwitchValues];
    [self.tblView reloadData];
}

#pragma mark UIScrollViewDelegate Methods
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if([scrollView isEqual:self.babyScrollView])
    {
        CGFloat pageWidth = self.view.bounds.size.width;
        int pageNo = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
        NSLog(@"pageNo=%d", pageNo);
        NSLog(@"backupPageNo=%d", backupPageNo);
        
        if(pageNo == 0)
        {
            NSLog(@"selectedBabyIndex=%d", selectedBabyIndex);
            if(backupPageNo != pageNo)
            {
                int nextIndex = selectedBabyIndex;
                nextIndex = (nextIndex >= babyList.count)?0:nextIndex;
                selectedBabyIndex--;
                selectedBabyIndex = (selectedBabyIndex < 0)?(int)(babyList.count-1):selectedBabyIndex;
                int prevIndex = selectedBabyIndex-1;
                prevIndex = (prevIndex < 0)?(int)(babyList.count-1):prevIndex;
                
                NSLog(@"selectedBabyIndex=%d", selectedBabyIndex);
                NSLog(@"prevIndex=%d", prevIndex);
                NSLog(@"nextIndex=%d", nextIndex);
                
                [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"feed_info"]] forKey:@"feed_info"];
                [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"pump_info"]] forKey:@"pump_info"];
                [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"sleep_info"]] forKey:@"sleep_info"];
                [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"poo_info"]] forKey:@"poo_info"];
                [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"wee_info"]] forKey:@"wee_info"];
                [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"medical_info"]] forKey:@"medical_info"];
                [shareObject synchronize];
                
                [self.imgBabyPic1 sd_setImageWithURL:[NSURL URLWithString:[[babyList objectAtIndex:prevIndex] objectForKey:@"photo_url"]] placeholderImage:[UIImage imageNamed:@"dummy.png"]];
                self.txtName1.text = [[babyList objectAtIndex:prevIndex] objectForKey:@"name"];
                self.txtInfo1.text = [NSString stringWithFormat:LocalizedString(@"txt_baby_info", nil), [[babyList objectAtIndex:prevIndex] objectForKey:@"age"], [[babyList objectAtIndex:prevIndex] objectForKey:@"height"], [[babyList objectAtIndex:prevIndex] objectForKey:@"weight"], [[babyList objectAtIndex:prevIndex] objectForKey:@"blood_type"]];
                
                [self.imgBabyPic2 sd_setImageWithURL:[NSURL URLWithString:[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"photo_url"]] placeholderImage:[UIImage imageNamed:@"dummy.png"]];
                self.txtName2.text = [[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"name"];
                self.txtInfo2.text = [NSString stringWithFormat:LocalizedString(@"txt_baby_info", nil), [[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"age"], [[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"height"], [[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"weight"], [[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"blood_type"]];
                
                [self.imgBabyPic3 sd_setImageWithURL:[NSURL URLWithString:[[babyList objectAtIndex:nextIndex] objectForKey:@"photo_url"]] placeholderImage:[UIImage imageNamed:@"dummy.png"]];
                self.txtName3.text = [[babyList objectAtIndex:nextIndex] objectForKey:@"name"];
                self.txtInfo3.text = [NSString stringWithFormat:LocalizedString(@"txt_baby_info", nil), [[babyList objectAtIndex:nextIndex] objectForKey:@"age"], [[babyList objectAtIndex:nextIndex] objectForKey:@"height"], [[babyList objectAtIndex:nextIndex] objectForKey:@"weight"], [[babyList objectAtIndex:nextIndex] objectForKey:@"blood_type"]];
                
                self.babyScrollView.contentOffset = CGPointMake(self.view.frame.size.width, self.babyScrollView.contentOffset.y);
                backupPageNo = 1;
                
            }
        }
        else if(pageNo == 2)
        {
            if(backupPageNo != pageNo)
            {
                int prevIndex = selectedBabyIndex;
                prevIndex = (prevIndex < 0)?(int)(babyList.count-1):prevIndex;
                selectedBabyIndex++;
                selectedBabyIndex = (selectedBabyIndex >= babyList.count)?0:selectedBabyIndex;
                int nextIndex = selectedBabyIndex+1;
                nextIndex = (nextIndex >= babyList.count)?0:nextIndex;
                
                NSLog(@"selectedBabyIndex=%d", selectedBabyIndex);
                NSLog(@"prevIndex=%d", prevIndex);
                NSLog(@"nextIndex=%d", nextIndex);
                
                [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"feed_info"]] forKey:@"feed_info"];
                [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"pump_info"]] forKey:@"pump_info"];
                [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"sleep_info"]] forKey:@"sleep_info"];
                [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"poo_info"]] forKey:@"poo_info"];
                [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"wee_info"]] forKey:@"wee_info"];
                [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"medical_info"]] forKey:@"medical_info"];
                [shareObject synchronize];
                
                [self.imgBabyPic1 sd_setImageWithURL:[NSURL URLWithString:[[babyList objectAtIndex:prevIndex] objectForKey:@"photo_url"]] placeholderImage:[UIImage imageNamed:@"dummy.png"]];
                self.txtName1.text = [[babyList objectAtIndex:prevIndex] objectForKey:@"name"];
                self.txtInfo1.text = [NSString stringWithFormat:LocalizedString(@"txt_baby_info", nil), [[babyList objectAtIndex:prevIndex] objectForKey:@"age"], [[babyList objectAtIndex:prevIndex] objectForKey:@"height"], [[babyList objectAtIndex:prevIndex] objectForKey:@"weight"], [[babyList objectAtIndex:prevIndex] objectForKey:@"blood_type"]];
                
                [self.imgBabyPic2 sd_setImageWithURL:[NSURL URLWithString:[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"photo_url"]] placeholderImage:[UIImage imageNamed:@"dummy.png"]];
                self.txtName2.text = [[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"name"];
                self.txtInfo2.text = [NSString stringWithFormat:LocalizedString(@"txt_baby_info", nil), [[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"age"], [[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"height"], [[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"weight"], [[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"blood_type"]];
                
                [self.imgBabyPic3 sd_setImageWithURL:[NSURL URLWithString:[[babyList objectAtIndex:nextIndex] objectForKey:@"photo_url"]] placeholderImage:[UIImage imageNamed:@"dummy.png"]];
                self.txtName3.text = [[babyList objectAtIndex:nextIndex] objectForKey:@"name"];
                self.txtInfo3.text = [NSString stringWithFormat:LocalizedString(@"txt_baby_info", nil), [[babyList objectAtIndex:nextIndex] objectForKey:@"age"], [[babyList objectAtIndex:nextIndex] objectForKey:@"height"], [[babyList objectAtIndex:nextIndex] objectForKey:@"weight"], [[babyList objectAtIndex:nextIndex] objectForKey:@"blood_type"]];
                
                self.babyScrollView.contentOffset = CGPointMake(self.view.frame.size.width, self.babyScrollView.contentOffset.y);
                backupPageNo = 1;
                
            }
        }
        else backupPageNo = pageNo;
        
        // Check for Subscription of selected baby
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"is_Subscription_Enabled"];
        if (![[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"subscription"] isKindOfClass:[NSNull class]]) {
            BOOL isSubscribed = [[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"subscription"] boolValue];
            [[NSUserDefaults standardUserDefaults]setBool:isSubscribed forKey:@"is_Subscription_Enabled"];
        }
        
        [self setSwitchValues];
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.tblView reloadData];
        });
        
    }
}

-(void)setSwitchValues
{
    switchValues = [[NSMutableArray alloc] init];
    for (int i =0; i<exportList.count; i++) {
        if ((i==0) && (![[NSUserDefaults standardUserDefaults]boolForKey:@"is_Subscription_Enabled"])) {
            [switchValues addObject:@0];
        }else {
            [switchValues addObject:@1];
        }
    }
}

@end
