//
//  SettingsFamilyMemberViewController.m
//  Joey
//
//  Created by csl on 13/2/2017.
//  Copyright © 2017 Auxilia. All rights reserved.
//

#import "AppDelegate.h"
#import "ActionSheetPicker.h"
#import "SettingsFamilyMemberViewController.h"

@interface SettingsFamilyMemberViewController ()
{
    NSUserDefaults *shareObject;
    NSMutableDictionary *familyInfo;
    NSMutableArray *relationshipValueList;
    NSMutableArray *relationshipList;
    NSString *selectedRelationship;
    int selectedRelationshipIndex;
    
    BOOL saveCheck;
    BOOL isMasterCheck;
}
@end

@implementation SettingsFamilyMemberViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    shareObject = [NSUserDefaults standardUserDefaults];
    familyInfo = [self.data objectForKey:@"family_info"];
    
    relationshipValueList = [[NSMutableArray alloc] initWithObjects: @"mommy", @"daddy", @"grandpa", @"grandma", @"helper", @"relative", @"caregiver", nil];
    relationshipList = [[NSMutableArray alloc] init];
    for(int i=0; i<relationshipValueList.count; i++) [relationshipList addObject:LocalizedString([relationshipValueList objectAtIndex:i], nil)];
    
    [self.navigationController setNavigationBarHidden:NO];
    [self.navigationController.navigationBar setBarTintColor:UIColorFromRGB(0xEEE8E4)];
    [self.navigationController.navigationBar setTranslucent:NO];
    [self.navigationItem setTitle:LocalizedString(@"title_settings_family_add_member", nil)];
    [self customBackButton];
    
    isMasterCheck = 0;
    
    self.btnInvite.title = LocalizedString(@"btn_invite", nil);
    self.lblName.text = LocalizedString(@"txt_name", nil);
    self.lblEmail.text = LocalizedString(@"txt_email", nil);
    self.lblRelationship.text = LocalizedString(@"txt_relation_to_baby", nil);
    self.lblAssignMaster.text = LocalizedString(@"txt_assign_as_master", nil);
    self.lblDescription.text = LocalizedString(@"txt_assign_as_master_subtext", nil);
    
    
    //Hid last row of master check on Client's advice
    self.lblAssignMaster.hidden = true;
    self.lblDescription.hidden = true;
    self.switchMaster.hidden = true;
    self.assignMasterBorderImage.hidden = true;
    

    if(IS_IPAD){
        self.lblEmail.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.lblName.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.lblRelationship.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.txtName.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.txtEmail.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.txtRelationship.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.lblAssignMaster.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.lblDescription.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];

    }
    
    self.switchMaster.tintColor = UIColorFromRGB(0xEDE6E1);
    self.switchMaster.onTintColor = UIColorFromRGB(0xD6E8C2);
    self.switchMaster.on = NO;
    [self.switchMaster addTarget: self action: @selector(masterSwitchAction:) forControlEvents:UIControlEventValueChanged];
    
    [self.txtName becomeFirstResponder];
    
}

#pragma mark - UI Customization & Layout Hadling Methods
- (void)customBackButton
{
    if([self.navigationController.viewControllers count] > 1)
    {
        UIImage *imgBack = [UIImage imageNamed:@"nav_btn_back"];
        UIButton *btnBack = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnBack setImage:imgBack forState:UIControlStateNormal];
        btnBack.frame = CGRectMake(0, 0, imgBack.size.width, imgBack.size.height);
        [btnBack addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
        [btnBack setImageEdgeInsets:UIEdgeInsetsMake(0, -16, 0, 16)];
        UIBarButtonItem *backBarItem = [[UIBarButtonItem alloc] initWithCustomView:btnBack];
        self.navigationItem.leftBarButtonItem = backBarItem;
    }
}

#pragma mark - Custom Actions
- (void)backAction
{
    UIViewController *viewController = [self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count-2];
    viewController.data = [[NSMutableDictionary alloc] initWithDictionary:@{@"save_check":@(saveCheck)}];
    [self.navigationController popViewControllerAnimated:YES];
}

-(void) masterSwitchAction : (UISwitch *)sender{
    isMasterCheck = sender.on;
}

- (BOOL)validateInputs
{
    ALPValidator *validator = nil;
    
    validator = [ALPValidator validatorWithType:ALPValidatorTypeString];
    [validator addValidationToEnsurePresenceWithInvalidMessage:LocalizedString(@"error_full_name", nil)];
    [validator validate:self.txtName.text];
    if(![validator isValid])
    {
        [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:[[validator errorMessages] objectAtIndex:0] input:self.txtName];
        return NO;
    }
    
    validator = [ALPValidator validatorWithType:ALPValidatorTypeString];
    [validator addValidationToEnsureRegularExpressionIsMetWithPattern:@"^[_A-Za-z0-9-+]+(\\.[_A-Za-z0-9-+]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z‌​]{2,10})$" invalidMessage:LocalizedString(@"error_email", nil)];
    [validator validate:self.txtEmail.text];
    if(![validator isValid])
    {
        [self.txtEmail becomeFirstResponder];
        [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:[[validator errorMessages] objectAtIndex:0] input:self.txtEmail];
        return NO;
    }
    
    validator = [ALPValidator validatorWithType:ALPValidatorTypeString];
    [validator addValidationToEnsurePresenceWithInvalidMessage:LocalizedString(@"error_relationship", nil)];
    [validator validate:self.txtRelationship.text];
    if(![validator isValid])
    {
        [self.txtRelationship becomeFirstResponder];
        [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:[[validator errorMessages] objectAtIndex:0] input:self.txtRelationship];
        return NO;
    }
    
    return YES;
}

- (IBAction)nextAction:(id)sender
{
    if([self validateInputs])
    {
        [SVProgressHUD show];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setValue:@([shareObject integerForKey:@"user_id"]) forKey:@"user_id"];
        [parameters setValue:@([[familyInfo objectForKey:@"id"] intValue]) forKey:@"family_id"];
        [parameters setValue:self.txtName.text forKey:@"name"];
        [parameters setValue:self.txtEmail.text forKey:@"email"];
        [parameters setValue:selectedRelationship forKey:@"relationship"];
        [parameters setValue:@(isMasterCheck) forKey:@"is_master"];
        
        [[WebServiceManager sharedManager] initWithBaseURL:@"api/inviteUser" parameters:parameters success:^(id result) {
            
            self.txtName.text = @"";
            self.txtEmail.text = @"";
            self.txtRelationship.text = @"";
            
            if([[result objectForKey:@"success"] boolValue])
            {
                [self showAlertWithTitle:LocalizedString(@"title_success", nil) message:[result objectForKey:@"message"] input:nil];
                saveCheck = YES;
                [self backAction];
            }
            else
            {
                [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:[result objectForKey:@"message"] input:nil];
            }
            
        } failure:^(NSError *error) {
        }];
    }
}

#pragma mark - Table view data source
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == 2)
    {
        [ActionSheetStringPicker showPickerWithTitle:@"" rows:relationshipList initialSelection:selectedRelationshipIndex doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
            
            selectedRelationshipIndex = (int)selectedIndex;
            selectedRelationship = [relationshipValueList objectAtIndex:selectedRelationshipIndex];
            self.txtRelationship.text = selectedValue;
            
        } cancelBlock:^(ActionSheetStringPicker *picker) {
            
        } origin:self.view];
    }
}

#pragma mark UITextFieldDelegate Methods
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if([textField isEqual:self.txtName])
    {
        [self.txtEmail becomeFirstResponder];
    }
    if([textField isEqual:self.txtEmail])
    {
        [self.txtRelationship becomeFirstResponder];
    }
    return YES;
}

@end
