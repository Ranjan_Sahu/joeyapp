//
//  SettingsUnitsViewController.h
//  Joey
//
//  Created by csl on 13/1/2017.
//  Copyright © 2017 Auxilia. All rights reserved.
//

#import "BaseTableViewController.h"

@interface SettingsUnitsViewController : BaseTableViewController 

@property (nonatomic, weak) IBOutlet UILabel *lblWeight;
@property (nonatomic, weak) IBOutlet UILabel *lblHeight;
@property (nonatomic, weak) IBOutlet UILabel *lblHeadSize;
@property (nonatomic, weak) IBOutlet UILabel *lblVolume;
@property (nonatomic, weak) IBOutlet UILabel *lblServing;
@property (nonatomic, weak) IBOutlet UILabel *lblTemperature;
@property (nonatomic, weak) IBOutlet UIButton *btnWeight1;
@property (nonatomic, weak) IBOutlet UIButton *btnWeight2;
@property (nonatomic, weak) IBOutlet UIButton *btnHeight1;
@property (nonatomic, weak) IBOutlet UIButton *btnHeight2;
@property (nonatomic, weak) IBOutlet UIButton *btnHeadSize1;
@property (nonatomic, weak) IBOutlet UIButton *btnHeadSize2;
@property (nonatomic, weak) IBOutlet UIButton *btnVolume1;
@property (nonatomic, weak) IBOutlet UIButton *btnVolume2;
@property (nonatomic, weak) IBOutlet UIButton *btnServing1;
@property (nonatomic, weak) IBOutlet UIButton *btnServing2;
@property (nonatomic, weak) IBOutlet UIButton *btnTemperature1;
@property (nonatomic, weak) IBOutlet UIButton *btnTemperature2;

@property (weak, nonatomic) IBOutlet UILabel *lblStartOfDay;
@property (weak, nonatomic) IBOutlet UIPickerView *timeValue;
@property (weak, nonatomic) IBOutlet UIPickerView *timeUnit;

//
@property (nonatomic, weak) IBOutlet UILabel *lblVideoUpload;
@property (nonatomic, weak) IBOutlet UISwitch *switchVideoUpload;

@end
