//
//  SettingsAccountViewController.h
//  Joey
//
//  Created by csl on 24/1/2017.
//  Copyright © 2017 Auxilia. All rights reserved.
//

#import "BaseTableViewController.h"

@interface SettingsAccountViewController : BaseTableViewController

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *btnLogoutHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *btnDeleteHeightConstraint;

@property (nonatomic, weak) IBOutlet UIBarButtonItem *btnSave;
@property (nonatomic, weak) IBOutlet UILabel *lblName;
@property (nonatomic, weak) IBOutlet UILabel *lblEmail;
@property (nonatomic, weak) IBOutlet UILabel *lblOldPassword;
@property (nonatomic, weak) IBOutlet UILabel *lblNewPassword;
@property (nonatomic, weak) IBOutlet UILabel *lblRetypePassword;
@property (nonatomic, weak) IBOutlet UITextField *txtName;
@property (nonatomic, weak) IBOutlet UITextField *txtEmail;
@property (nonatomic, weak) IBOutlet UITextField *txtOldPassword;
@property (nonatomic, weak) IBOutlet UITextField *txtNewPassword;
@property (nonatomic, weak) IBOutlet UITextField *txtRetypePassword;

@property (nonatomic, weak) IBOutlet UIButton *btnLogout;
@property (nonatomic, weak) IBOutlet UIButton *btnDelete;

@end
