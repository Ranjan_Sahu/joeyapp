//
//  SettingsUnitsViewController.m
//  Joey
//
//  Created by csl on 13/1/2017.
//  Copyright © 2017 Auxilia. All rights reserved.
//

#import "AppDelegate.h"
#import "UIViewController+data.h"
#import "SettingsUnitsViewController.h"

@interface SettingsUnitsViewController ()
{
    NSUserDefaults *shareObject;
    AppDelegate *appDelegate;
    NSMutableDictionary *unitInfo;
}
@end

@implementation SettingsUnitsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    shareObject = [NSUserDefaults standardUserDefaults];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    unitInfo = (NSMutableDictionary *)[NSKeyedUnarchiver unarchiveObjectWithData:[shareObject objectForKey:@"unit_info"]];
    NSLog(@"unitInfo=%@",unitInfo);
    
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:UIColorFromRGB(0x8D8082), NSForegroundColorAttributeName, [UIFont fontWithName:@"SFUIDisplay-Medium" size:18.0f], NSFontAttributeName, nil]];
    [self.navigationController.navigationBar setBarTintColor:UIColorFromRGB(0xEEE8E4)];
    [self.navigationController.navigationBar setTranslucent:NO];
    [self.navigationItem setTitle:LocalizedString(@"title_settings_others", nil)];
    [self customBackButton];
    
    if(IS_IPAD) {
        self.lblWeight.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.lblHeight.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.lblHeadSize.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.lblVolume.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.lblServing.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.lblTemperature.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        //self.lblMedicine.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        
        [self.btnHeight1.titleLabel setFont:[UIFont fontWithName:@"SFUIDisplay-Medium" size:23.0f]];
        [self.btnHeight2.titleLabel setFont:[UIFont fontWithName:@"SFUIDisplay-Medium" size:23.0f]];
        [self.btnWeight1.titleLabel setFont:[UIFont fontWithName:@"SFUIDisplay-Medium" size:23.0f]];
        [self.btnWeight2.titleLabel setFont:[UIFont fontWithName:@"SFUIDisplay-Medium" size:23.0f]];
        [self.btnHeadSize1.titleLabel setFont:[UIFont fontWithName:@"SFUIDisplay-Medium" size:23.0f]];
        [self.btnHeadSize2.titleLabel setFont:[UIFont fontWithName:@"SFUIDisplay-Medium" size:23.0f]];
        [self.btnVolume1.titleLabel setFont:[UIFont fontWithName:@"SFUIDisplay-Medium" size:23.0f]];
        [self.btnVolume2.titleLabel setFont:[UIFont fontWithName:@"SFUIDisplay-Medium" size:23.0f]];
        [self.btnServing1.titleLabel setFont:[UIFont fontWithName:@"SFUIDisplay-Medium" size:23.0f]];
        [self.btnServing2.titleLabel setFont:[UIFont fontWithName:@"SFUIDisplay-Medium" size:23.0f]];
        //[self.btnMedicine1.titleLabel setFont:[UIFont fontWithName:@"SFUIDisplay-Medium" size:23.0f]];
        //[self.btnMedicine2.titleLabel setFont:[UIFont fontWithName:@"SFUIDisplay-Medium" size:23.0f]];
        [self.btnTemperature1.titleLabel setFont:[UIFont fontWithName:@"SFUIDisplay-Medium" size:23.0f]];
        [self.btnTemperature2.titleLabel setFont:[UIFont fontWithName:@"SFUIDisplay-Medium" size:23.0f]];
    }
    
    self.lblWeight.text = LocalizedString(@"txt_weight", nil);
    self.btnWeight1.layer.cornerRadius = 3.f;
    [self.btnWeight1 setTitle:LocalizedString(@"kg", nil) forState:UIControlStateNormal];
    self.btnWeight2.layer.cornerRadius = 3.f;
    [self.btnWeight2 setTitle:LocalizedString(@"lb", nil) forState:UIControlStateNormal];
    if([[unitInfo objectForKey:@"weight"] isEqualToString:@"kg"]){
        [self.btnWeight1 setBackgroundColor:UIColorFromRGB(0x8D8082)];
    }else{
        [self.btnWeight2 setBackgroundColor:UIColorFromRGB(0x8D8082)];
    }
    
    self.lblHeight.text = LocalizedString(@"txt_height", nil);
    self.btnHeight1.layer.cornerRadius = 3.f;
    [self.btnHeight1 setTitle:LocalizedString(@"cm", nil) forState:UIControlStateNormal];
    self.btnHeight2.layer.cornerRadius = 3.f;
    [self.btnHeight2 setTitle:LocalizedString(@"in", nil) forState:UIControlStateNormal];
    if([[unitInfo objectForKey:@"height"] isEqualToString:@"cm"]){
        [self.btnHeight1 setBackgroundColor:UIColorFromRGB(0x8D8082)];
    }else{
        [self.btnHeight2 setBackgroundColor:UIColorFromRGB(0x8D8082)];
    }
    
    self.lblHeadSize.text = LocalizedString(@"txt_headsize", nil);
    self.btnHeadSize1.layer.cornerRadius = 3.f;
    [self.btnHeadSize1 setTitle:LocalizedString(@"cm", nil) forState:UIControlStateNormal];
    self.btnHeadSize2.layer.cornerRadius = 3.f;
    [self.btnHeadSize2 setTitle:LocalizedString(@"in", nil) forState:UIControlStateNormal];
    if([[unitInfo objectForKey:@"head_size"] isEqualToString:@"cm"]){
        [self.btnHeadSize1 setBackgroundColor:UIColorFromRGB(0x8D8082)];
    }else{
        [self.btnHeadSize2 setBackgroundColor:UIColorFromRGB(0x8D8082)];
    }
    
    self.lblVolume.text = LocalizedString(@"txt_volume", nil);
    self.btnVolume1.layer.cornerRadius = 3.f;
    [self.btnVolume1 setTitle:LocalizedString(@"ml", nil) forState:UIControlStateNormal];
    self.btnVolume2.layer.cornerRadius = 3.f;
    [self.btnVolume2 setTitle:LocalizedString(@"oz", nil) forState:UIControlStateNormal];
    NSLog(@"volume=%@", [unitInfo objectForKey:@"volume"]);
    if([[unitInfo objectForKey:@"volume"] isEqualToString:@"ml"]){
        [self.btnVolume1 setBackgroundColor:UIColorFromRGB(0x8D8082)];
    }else{
        [self.btnVolume2 setBackgroundColor:UIColorFromRGB(0x8D8082)];
    }
    
    self.lblServing.text = LocalizedString(@"txt_serving", nil);
    self.btnServing1.layer.cornerRadius = 3.f;
    [self.btnServing1 setTitle:LocalizedString(@"gm", nil) forState:UIControlStateNormal];
    self.btnServing2.layer.cornerRadius = 3.f;
    [self.btnServing2 setTitle:LocalizedString(@"oz", nil) forState:UIControlStateNormal];
    NSLog(@"serving=%@", [unitInfo objectForKey:@"serving"]);
    if([[unitInfo objectForKey:@"serving"] isEqualToString:@"gm"]){
        [self.btnServing1 setBackgroundColor:UIColorFromRGB(0x8D8082)];
    }else{
        [self.btnServing2 setBackgroundColor:UIColorFromRGB(0x8D8082)];
    }
    
    self.lblTemperature.text = LocalizedString(@"txt_temperature", nil);
    self.btnTemperature1.layer.cornerRadius = 3.f;
    [self.btnTemperature1 setTitle:LocalizedString(@"°C", nil) forState:UIControlStateNormal];
    self.btnTemperature2.layer.cornerRadius = 3.f;
    [self.btnTemperature2 setTitle:LocalizedString(@"°F", nil) forState:UIControlStateNormal];
    if([[unitInfo objectForKey:@"temperature"] isEqualToString:@"°C"]){
        [self.btnTemperature1 setBackgroundColor:UIColorFromRGB(0x8D8082)];
    }else{
        [self.btnTemperature2 setBackgroundColor:UIColorFromRGB(0x8D8082)];
    }
    
//    self.lblMedicine.text = LocalizedString(@"txt_medicine", nil);
//    self.btnMedicine1.layer.cornerRadius = 3.f;
//    [self.btnMedicine1 setTitle:LocalizedString(@"ml", nil) forState:UIControlStateNormal];
//    self.btnMedicine2.layer.cornerRadius = 3.f;
//    [self.btnMedicine2 setTitle:LocalizedString(@"tsp", nil) forState:UIControlStateNormal];
//
//    if([[unitInfo objectForKey:@"medicine"] isEqualToString:@"ml"])
//    {
//        [self.btnMedicine1 setBackgroundColor:UIColorFromRGB(0x8D8082)];
//    }
//    else
//    {
//        [self.btnMedicine2 setBackgroundColor:UIColorFromRGB(0x8D8082)];
//    }
    
    self.switchVideoUpload.tintColor = UIColorFromRGB(0xEDE6E1);
    self.switchVideoUpload.onTintColor = UIColorFromRGB(0xD6E8C2);
    
    BOOL ntwrkFlagStatus = NO;
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"can_Upload_Using_Mobile_Network"]) {
        ntwrkFlagStatus = [[NSUserDefaults standardUserDefaults] boolForKey:@"can_Upload_Using_Mobile_Network"];
        self.switchVideoUpload.on = ntwrkFlagStatus;
    }else {
        [[NSUserDefaults standardUserDefaults] setBool:ntwrkFlagStatus forKey:@"can_Upload_Using_Mobile_Network"];
    }
    
}

- (void)customBackButton
{
    if([self.navigationController.viewControllers count] > 1)
    {
        UIImage *imgBack = [UIImage imageNamed:@"nav_btn_back"];
        UIButton *btnBack = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnBack setImage:imgBack forState:UIControlStateNormal];
        btnBack.frame = CGRectMake(0, 0, imgBack.size.width, imgBack.size.height);
        [btnBack addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
        [btnBack setImageEdgeInsets:UIEdgeInsetsMake(0, -16, 0, 16)];
        UIBarButtonItem *backBarItem = [[UIBarButtonItem alloc] initWithCustomView:btnBack];
        self.navigationItem.leftBarButtonItem = backBarItem;
    }
}

- (void)backAction
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)getWeightAction:(id)sender
{
    [SVProgressHUD show];
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setValue:@([shareObject integerForKey:@"user_id"]) forKey:@"user_id"];
    
    UIButton *btn = (UIButton *)sender;
    if(btn == self.btnWeight1)
    {
        [self.btnWeight1 setBackgroundColor:UIColorFromRGB(0x8D8082)];
        [self.btnWeight2 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
        [parameters setValue:@"kg" forKey:@"weight"];
    }
    else
    {
        [self.btnWeight1 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
        [self.btnWeight2 setBackgroundColor:UIColorFromRGB(0x8D8082)];
        [parameters setValue:@"lb" forKey:@"weight"];
    }
    
    [[WebServiceManager sharedManager] initWithBaseURL:@"api/updateUserSettingsUnits" parameters:parameters success:^(id result) {
        //updateUserUnit
        [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[result valueForKey:@"info"]] forKey:@"unit_info"];
        [shareObject synchronize];
        
        appDelegate.updateTimelineCheck = appDelegate.updateActivitiesCheck = appDelegate.updateMilestonesCheck = appDelegate.updateGalleryCheck = YES;
        
        //show alert
        [self showAlertWithTitle:LocalizedString(@"title_success", nil) message:LocalizedString(@"alert_unit_updated", nil) input:nil];
        [SVProgressHUD dismiss];
        
    } failure:^(NSError *error) {
    }];
}

- (IBAction)getHeightAction:(id)sender
{
    [SVProgressHUD show];
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setValue:@([shareObject integerForKey:@"user_id"]) forKey:@"user_id"];
    
    UIButton *btn = (UIButton *)sender;
    if(btn == self.btnHeight1)
    {
        [self.btnHeight1 setBackgroundColor:UIColorFromRGB(0x8D8082)];
        [self.btnHeight2 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
        [parameters setValue:@"cm" forKey:@"height"];
    }
    else
    {
        [self.btnHeight1 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
        [self.btnHeight2 setBackgroundColor:UIColorFromRGB(0x8D8082)];
        [parameters setValue:@"in" forKey:@"height"];
    }
    
    [[WebServiceManager sharedManager] initWithBaseURL:@"api/updateUserSettingsUnits" parameters:parameters success:^(id result) {
        
        [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[result valueForKey:@"info"]] forKey:@"unit_info"];
        [shareObject synchronize];
        
        
        appDelegate.updateTimelineCheck = appDelegate.updateActivitiesCheck = appDelegate.updateMilestonesCheck = appDelegate.updateGalleryCheck = YES;
        
        [self showAlertWithTitle:LocalizedString(@"title_success", nil) message:LocalizedString(@"alert_unit_updated", nil) input:nil];
        [SVProgressHUD dismiss];
        
    } failure:^(NSError *error) {
    }];
}

- (IBAction)getHeadSizeAction:(id)sender
{
    [SVProgressHUD show];
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setValue:@([shareObject integerForKey:@"user_id"]) forKey:@"user_id"];
    
    UIButton *btn = (UIButton *)sender;
    if(btn == self.btnHeadSize1)
    {
        [self.btnHeadSize1 setBackgroundColor:UIColorFromRGB(0x8D8082)];
        [self.btnHeadSize2 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
        [parameters setValue:@"cm" forKey:@"head_size"];
    }
    else
    {
        [self.btnHeadSize1 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
        [self.btnHeadSize2 setBackgroundColor:UIColorFromRGB(0x8D8082)];
        [parameters setValue:@"in" forKey:@"head_size"];
    }
    
    [[WebServiceManager sharedManager] initWithBaseURL:@"api/updateUserSettingsUnits" parameters:parameters success:^(id result) {
        
        [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[result valueForKey:@"info"]] forKey:@"unit_info"];
        [shareObject synchronize];
        
        
        appDelegate.updateTimelineCheck = appDelegate.updateActivitiesCheck = appDelegate.updateMilestonesCheck = appDelegate.updateGalleryCheck = YES;
        
        [self showAlertWithTitle:LocalizedString(@"title_success", nil) message:LocalizedString(@"alert_unit_updated", nil) input:nil];
        [SVProgressHUD dismiss];
        
    } failure:^(NSError *error) {
    }];
}
- (IBAction)getVolumeAction:(id)sender
{
    [SVProgressHUD show];
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setValue:@([shareObject integerForKey:@"user_id"]) forKey:@"user_id"];
    
    UIButton *btn = (UIButton *)sender;
    if(btn == self.btnVolume1)
    {
        [self.btnVolume1 setBackgroundColor:UIColorFromRGB(0x8D8082)];
        [self.btnVolume2 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
        [parameters setValue:@"ml" forKey:@"volume"];
    }
    else
    {
        [self.btnVolume1 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
        [self.btnVolume2 setBackgroundColor:UIColorFromRGB(0x8D8082)];
        [parameters setValue:@"oz" forKey:@"volume"];
    }
    
    [[WebServiceManager sharedManager] initWithBaseURL:@"api/updateUserSettingsUnits" parameters:parameters success:^(id result) {
        
        [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[result valueForKey:@"info"]] forKey:@"unit_info"];
        [shareObject synchronize];
        
        appDelegate.updateTimelineCheck = appDelegate.updateActivitiesCheck = appDelegate.updateMilestonesCheck = appDelegate.updateGalleryCheck = YES;
        
        [self showAlertWithTitle:LocalizedString(@"title_success", nil) message:LocalizedString(@"alert_unit_updated", nil) input:nil];
        [SVProgressHUD dismiss];
        
    } failure:^(NSError *error) {
    }];
}
- (IBAction)getServingAction:(id)sender
{
    [SVProgressHUD show];
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setValue:@([shareObject integerForKey:@"user_id"]) forKey:@"user_id"];
    
    UIButton *btn = (UIButton *)sender;
    if(btn == self.btnServing1)
    {
        [self.btnServing1 setBackgroundColor:UIColorFromRGB(0x8D8082)];
        [self.btnServing2 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
        [parameters setValue:@"gm" forKey:@"serving"];
    }
    else
    {
        [self.btnServing1 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
        [self.btnServing2 setBackgroundColor:UIColorFromRGB(0x8D8082)];
        [parameters setValue:@"oz" forKey:@"serving"];
    }
    
    [[WebServiceManager sharedManager] initWithBaseURL:@"api/updateUserSettingsUnits" parameters:parameters success:^(id result) {
        
        [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[result valueForKey:@"info"]] forKey:@"unit_info"];
        [shareObject synchronize];
        
        appDelegate.updateTimelineCheck = appDelegate.updateActivitiesCheck = appDelegate.updateMilestonesCheck = appDelegate.updateGalleryCheck = YES;
        
        [self showAlertWithTitle:LocalizedString(@"title_success", nil) message:LocalizedString(@"alert_unit_updated", nil) input:nil];
        [SVProgressHUD dismiss];
    } failure:^(NSError *error) {
    }];
}
- (IBAction)getTemperatureAction:(id)sender
{
    [SVProgressHUD show];
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setValue:@([shareObject integerForKey:@"user_id"]) forKey:@"user_id"];
    
    UIButton *btn = (UIButton *)sender;
    if(btn == self.btnTemperature1)
    {
        [self.btnTemperature1 setBackgroundColor:UIColorFromRGB(0x8D8082)];
        [self.btnTemperature2 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
        [parameters setValue:@"°C" forKey:@"temperature"];
    }
    else
    {
        [self.btnTemperature1 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
        [self.btnTemperature2 setBackgroundColor:UIColorFromRGB(0x8D8082)];
        [parameters setValue:@"°F" forKey:@"temperature"];
    }
    
    [[WebServiceManager sharedManager] initWithBaseURL:@"api/updateUserSettingsUnits" parameters:parameters success:^(id result) {
        
        [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[result valueForKey:@"info"]] forKey:@"unit_info"];
        [shareObject synchronize];
        
        appDelegate.updateTimelineCheck = appDelegate.updateActivitiesCheck = appDelegate.updateMilestonesCheck = appDelegate.updateGalleryCheck = YES;
        
        [self showAlertWithTitle:LocalizedString(@"title_success", nil) message:LocalizedString(@"alert_unit_updated", nil) input:nil];
        [SVProgressHUD dismiss];
        
    } failure:^(NSError *error) {
    }];
}

//- (IBAction)getMedicineAction:(id)sender
//{
//    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
//    [parameters setValue:@([shareObject integerForKey:@"user_id"]) forKey:@"user_id"];
//
//    UIButton *btn = (UIButton *)sender;
//    if(btn == self.btnMedicine1)
//    {
//        [self.btnMedicine1 setBackgroundColor:UIColorFromRGB(0x8D8082)];
//        [self.btnMedicine2 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
//        [parameters setValue:@"ml" forKey:@"medicine"];
//    }
//    else
//    {
//        [self.btnMedicine1 setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
//        [self.btnMedicine2 setBackgroundColor:UIColorFromRGB(0x8D8082)];
//        [parameters setValue:@"tsp" forKey:@"medicine"];
//    }
//
//    [[WebServiceManager sharedManager] initWithBaseURL:@"api/updateUserSettingsUnits" parameters:parameters success:^(id result) {
//
//        [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[result valueForKey:@"info"]] forKey:@"unit_info"];
//        [shareObject synchronize];
//
//        appDelegate.updateTimelineCheck = appDelegate.updateActivitiesCheck = appDelegate.updateActivityCheck = YES;
//
//    } failure:^(NSError *error) {
//    }];
//}

- (IBAction)uploadVideoSwitchAction:(UISwitch *)sender {
    BOOL ntwrkFlagStatus = sender.isOn;
    if ([[NSUserDefaults standardUserDefaults]objectForKey:@"can_Upload_Using_Mobile_Network"]) {
        [[NSUserDefaults standardUserDefaults] setBool:ntwrkFlagStatus forKey:@"can_Upload_Using_Mobile_Network"];
    }
}

#pragma mark - Table View methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (section == 0) return LocalizedString(@"title_settings_units", nil);
    else if (section == 1) return LocalizedString(@"title_settings_video", nil);
    //else if (section == 2) return LocalizedString(@"title_timeline", nil);
    return nil;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UILabel *txtHeader;
    UIView *headerView;
    
    if (IS_IPAD) {
        txtHeader = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, tableView.bounds.size.width-130, 60)];
        txtHeader.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:26.0f];
        headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 60)];
    }
    else{
        txtHeader = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, tableView.bounds.size.width-130, 40)];
        txtHeader.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:18.0f];
        headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 40)];
    }
    
    txtHeader.textColor = [UIColor whiteColor];
    txtHeader.text = [self tableView:tableView titleForHeaderInSection:section];
    
    headerView.backgroundColor = UIColorFromRGB(0xBFB5AE);
    [headerView addSubview:txtHeader];
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (IS_IPAD) {
        return 60;
    }else {
        return 40;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

@end
