//
//  SettingsNotificationsViewController.m
//  Joey
//
//  Created by csl on 29/3/2017.
//  Copyright © 2017 Auxilia. All rights reserved.
//

#import "SettingsNotificationsViewController.h"
#import "AppDelegate.h"
#import "NotificationsTableViewCell.h"

@interface SettingsNotificationsViewController ()
{
    NSUserDefaults *shareObject;
    AppDelegate *appDelegate;
    NSMutableArray *babyList;
    NSMutableDictionary *unitInfo;
    NSMutableArray *notificationList;
    NSMutableArray *iconColorList;
    NSMutableArray *iconImageList;
    NSMutableDictionary *list;
    NSMutableArray *switchValues;
    
    BOOL isSubscribed;
    int backupPageNo;
    int selectedBabyIndex;
}
@end

@implementation SettingsNotificationsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    shareObject = [NSUserDefaults standardUserDefaults];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    unitInfo = (NSMutableDictionary *)[NSKeyedUnarchiver unarchiveObjectWithData:[shareObject objectForKey:@"unit_info"]];
    isSubscribed = [shareObject objectForKey:@"subscription_status"];
    backupPageNo = 1;
    
    [self.navigationController setNavigationBarHidden:NO];
    [self.navigationController.navigationBar setBarTintColor:UIColorFromRGB(0xEEE8E4)];
    [self.navigationController.navigationBar setTranslucent:NO];
    [self.navigationItem setTitle:LocalizedString(@"title_settings_notifications", nil)];
    [self customBackButton];
    
    if ([[NSUserDefaults standardUserDefaults]boolForKey:@"is_Subscription_Enabled"]) {
        notificationList = [[NSMutableArray alloc] initWithObjects:LocalizedString(@"txt_all", nil), LocalizedString(@"feed", nil), LocalizedString(@"pump", nil),  LocalizedString(@"sleep", nil), LocalizedString(@"poo", nil), LocalizedString(@"wee", nil), LocalizedString(@"medical", nil), LocalizedString(@"milestones", nil), LocalizedString(@"proactiveParenting", nil),  nil];
    }
    else {
        notificationList = [[NSMutableArray alloc] initWithObjects:LocalizedString(@"txt_all", nil), LocalizedString(@"feed", nil), LocalizedString(@"pump", nil),  LocalizedString(@"sleep", nil), LocalizedString(@"poo", nil), LocalizedString(@"wee", nil), LocalizedString(@"medical", nil), LocalizedString(@"milestones", nil),  nil];
    }
    
    iconColorList = [[NSMutableArray alloc] initWithObjects:UIColorFromRGB(0xD6E8C2), UIColorFromRGB(0xF4EA9E), UIColorFromRGB(0xF9C99B), UIColorFromRGB(0xC5D7E1), UIColorFromRGB(0xE0C5B8), UIColorFromRGB(0xCADAA9), UIColorFromRGB(0xF4D2D5), UIColorFromRGB(0xE2D3CA), UIColorFromRGB(0xE25F0F), nil];
    iconImageList = [[NSMutableArray alloc] initWithObjects:@"", @"icon_feed", @"icon_pump", @"icon_sleep", @"icon_poo", @"icon_wee", @"icon_medical", @"icon_milestones", @"icon_proactiveParenting", nil];
    
    list = [[NSMutableDictionary alloc] init];
    switchValues = [[NSMutableArray alloc] init];
    for (int i =0; i<notificationList.count; i++) {
        [switchValues addObject:@0];
    }
    
    [self.tblView registerNib:[UINib nibWithNibName:@"NotificationsTableViewCell" bundle:nil] forCellReuseIdentifier:@"NotificationsTableViewCell"];

    self.tblView.delegate = self;
    self.tblView.dataSource = self;
    self.babyScrollView.delegate = self;
    
    if (IS_IPAD) {
        self.txtName1.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.txtInfo1.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:22.0f];
        self.txtName2.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.txtInfo2.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:22.0f];
        self.txtName3.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.txtInfo3.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:22.0f];
    }
    
    [self setupBabyList];
    [self getNotificationList];
    
}

#pragma mark - UI Customization & Layout Hadling Methods
- (void)customBackButton
{
    if([self.navigationController.viewControllers count] > 1)
    {
        UIImage *imgBack = [UIImage imageNamed:@"nav_btn_back"];
        UIButton *btnBack = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnBack setImage:imgBack forState:UIControlStateNormal];
        btnBack.frame = CGRectMake(0, 0, imgBack.size.width, imgBack.size.height);
        [btnBack addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
        [btnBack setImageEdgeInsets:UIEdgeInsetsMake(0, -16, 0, 16)];
        UIBarButtonItem *backBarItem = [[UIBarButtonItem alloc] initWithCustomView:btnBack];
        self.navigationItem.leftBarButtonItem = backBarItem;
    }
}

- (void)viewDidLayoutSubviews
{
    if(self.babyScrollView.contentSize.width == 0)
    {
        self.babyScrollView.contentSize = CGSizeMake(self.view.frame.size.width*3, 1);
        self.babyScrollView.contentOffset = CGPointMake(self.view.frame.size.width, self.babyScrollView.contentOffset.y);
    }
    
    self.imgBabyPic1.clipsToBounds = self.imgBabyPic2.clipsToBounds = self.imgBabyPic3.clipsToBounds = YES;
    self.imgBabyPic1.layer.cornerRadius = self.imgBabyPic1.frame.size.width/2;
    self.imgBabyPic2.layer.cornerRadius = self.imgBabyPic2.frame.size.width/2;
    self.imgBabyPic3.layer.cornerRadius = self.imgBabyPic3.frame.size.width/2;
}

#pragma mark - Custom Actions
-(void)refreshNotificationListArray
{
    if ([[NSUserDefaults standardUserDefaults]boolForKey:@"is_Subscription_Enabled"]) {
        notificationList = [[NSMutableArray alloc] initWithObjects:LocalizedString(@"txt_all", nil), LocalizedString(@"feed", nil), LocalizedString(@"pump", nil),  LocalizedString(@"sleep", nil), LocalizedString(@"poo", nil), LocalizedString(@"wee", nil), LocalizedString(@"medical", nil), LocalizedString(@"milestones", nil), LocalizedString(@"proactiveParenting", nil),  nil];
    }
    else {
        notificationList = [[NSMutableArray alloc] initWithObjects:LocalizedString(@"txt_all", nil), LocalizedString(@"feed", nil), LocalizedString(@"pump", nil),  LocalizedString(@"sleep", nil), LocalizedString(@"poo", nil), LocalizedString(@"wee", nil), LocalizedString(@"medical", nil), LocalizedString(@"milestones", nil),  nil];
    }
    
    switchValues = [[NSMutableArray alloc] init];
    for (int i =0; i<notificationList.count; i++) {
        [switchValues addObject:@0];
    }

    //[self.tblView reloadData];
}

- (void)backAction
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)getNotificationList
{
    [SVProgressHUD show];
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setValue:@([shareObject integerForKey:@"user_id"]) forKey:@"user_id"];
    [parameters setValue:@([[[babyList objectAtIndex:selectedBabyIndex] valueForKey:@"id"] intValue]) forKey:@"baby_id"];
    
    [[WebServiceManager sharedManager] initWithBaseURL:@"api/getNotificationList" parameters:parameters success:^(id result) {
        
        list = (NSMutableDictionary *)[result valueForKey:@"list"];

        int allCount = 0;

        if([[list objectForKey:@"feed_check"] boolValue])
        {
            [switchValues replaceObjectAtIndex:1 withObject:@1];
            allCount++;
        }
        else
        {
            [switchValues replaceObjectAtIndex:1 withObject:@0];
        }
        if([[list objectForKey:@"pump_check"] boolValue])
        {
            [switchValues replaceObjectAtIndex:2 withObject:@1];
            allCount++;
        }
        else
        {
            [switchValues replaceObjectAtIndex:2 withObject:@0];
        }
        if([[list objectForKey:@"sleep_check"] boolValue])
        {
            [switchValues replaceObjectAtIndex:3 withObject:@1];
            allCount++;
        }
        else
        {
            [switchValues replaceObjectAtIndex:3 withObject:@0];
        }
        if([[list objectForKey:@"poo_check"] boolValue])
        {
            [switchValues replaceObjectAtIndex:4 withObject:@1];
            allCount++;
        }
        else
        {
            [switchValues replaceObjectAtIndex:4 withObject:@0];
        }
        if([[list objectForKey:@"wee_check"] boolValue])
        {
            [switchValues replaceObjectAtIndex:5 withObject:@1];
            allCount++;
        }
        else
        {
            [switchValues replaceObjectAtIndex:5 withObject:@0];
        }
        if([[list objectForKey:@"medical_check"] boolValue])
        {
            [switchValues replaceObjectAtIndex:6 withObject:@1];
            allCount++;
        }
        else
        {
            [switchValues replaceObjectAtIndex:6 withObject:@0];
        }
        if([[list objectForKey:@"milestone_check"] boolValue])
        {
            [switchValues replaceObjectAtIndex:7 withObject:@1];
            allCount++;
        }
        else
        {
            [switchValues replaceObjectAtIndex:7 withObject:@0];
        }
        
        if ([notificationList containsObject:LocalizedString(@"proactiveParenting", nil)]) {
            
            if([[list objectForKey:@"proactive_parenting_check"] boolValue])
            {
                [switchValues replaceObjectAtIndex:8 withObject:@1];
                allCount++;
            }
            else
            {
                [switchValues replaceObjectAtIndex:8 withObject:@0];
            }
        }
        
        if(allCount == notificationList.count-1) {
            [switchValues replaceObjectAtIndex:0 withObject:@1];
        }else {
            [switchValues replaceObjectAtIndex:0 withObject:@0];
        }
        
        [SVProgressHUD dismiss];
        [self.tblView reloadData];

    } failure:^(NSError *error) {
        [SVProgressHUD dismiss];
    }];
}

- (void)getSwitchAction:(UISwitch *)sender
{
    if(sender.tag == 0)
    {
        if (sender.isOn ){
            for (int i=0; i<notificationList.count; i++) {
                [switchValues replaceObjectAtIndex:i withObject:@1];
            }
        }else {
            for (int i=0; i<notificationList.count; i++) {
                [switchValues replaceObjectAtIndex:i withObject:@0];
            }
        }
    }
    else
    {
        if (sender.isOn) {
            [switchValues replaceObjectAtIndex:sender.tag withObject:@1];
        }
        else {
            [switchValues replaceObjectAtIndex:sender.tag withObject:@0];
        }
        
    }

    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setValue:@([shareObject integerForKey:@"user_id"]) forKey:@"user_id"];
    [parameters setValue:@([[[babyList objectAtIndex:selectedBabyIndex] valueForKey:@"id"] intValue]) forKey:@"baby_id"];

    [parameters setValue:[switchValues objectAtIndex:1] forKey:@"feed_check"];
    [parameters setValue:[switchValues objectAtIndex:2] forKey:@"pump_check"];
    [parameters setValue:[switchValues objectAtIndex:3] forKey:@"sleep_check"];
    [parameters setValue:[switchValues objectAtIndex:4] forKey:@"poo_check"];
    [parameters setValue:[switchValues objectAtIndex:5] forKey:@"wee_check"];
    [parameters setValue:[switchValues objectAtIndex:6] forKey:@"medical_check"];
    [parameters setValue:[switchValues objectAtIndex:7] forKey:@"milestone_check"];
    if ([notificationList containsObject:LocalizedString(@"proactiveParenting", nil)]) {
        [parameters setValue:[switchValues objectAtIndex:8] forKey:@"proactive_parenting_check"];
    }
    

    [[WebServiceManager sharedManager] initWithBaseURL:@"api/updateUserNotification" parameters:parameters success:^(id result) {
        NSLog(@"Success");
        
        list = (NSMutableDictionary *)[result valueForKey:@"info"];

        int allCount = 0;
        
        if([[list objectForKey:@"feed_check"] boolValue]){
            [switchValues replaceObjectAtIndex:1 withObject:@1];
            allCount++;
        }
        else{
            [switchValues replaceObjectAtIndex:1 withObject:@0];
        }
        
        if([[list objectForKey:@"pump_check"] boolValue]){
            [switchValues replaceObjectAtIndex:2 withObject:@1];
            allCount++;
        }
        else{
            [switchValues replaceObjectAtIndex:2 withObject:@0];
        }
        
        if([[list objectForKey:@"sleep_check"] boolValue]){
            [switchValues replaceObjectAtIndex:3 withObject:@1];
            allCount++;
        }
        else{
            [switchValues replaceObjectAtIndex:3 withObject:@0];
        }
        
        if([[list objectForKey:@"poo_check"] boolValue]){
            [switchValues replaceObjectAtIndex:4 withObject:@1];
            allCount++;
        }
        else{
            [switchValues replaceObjectAtIndex:4 withObject:@0];
        }
        
        if([[list objectForKey:@"wee_check"] boolValue]){
            [switchValues replaceObjectAtIndex:5 withObject:@1];
            allCount++;
        }
        else{
            [switchValues replaceObjectAtIndex:5 withObject:@0];
        }
        
        if([[list objectForKey:@"medical_check"] boolValue]){
            [switchValues replaceObjectAtIndex:6 withObject:@1];
            allCount++;
        }
        else{
            [switchValues replaceObjectAtIndex:6 withObject:@0];
        }
        
        if([[list objectForKey:@"milestone_check"] boolValue]){
            [switchValues replaceObjectAtIndex:7 withObject:@1];
            allCount++;
        }
        else{
            [switchValues replaceObjectAtIndex:7 withObject:@0];
        }
        
        if ([notificationList containsObject:LocalizedString(@"proactiveParenting", nil)]) {
            if([[list objectForKey:@"proactive_parenting_check"] boolValue]){
                [switchValues replaceObjectAtIndex:8 withObject:@1];
                allCount++;
            }
            else{
                [switchValues replaceObjectAtIndex:8 withObject:@0];
            }
        }
        
        if(allCount == notificationList.count-1) {
            [switchValues replaceObjectAtIndex:0 withObject:@1];
        }
        else {
            [switchValues replaceObjectAtIndex:0 withObject:@0];
        }
        
        [SVProgressHUD dismiss];
        [self.tblView reloadData];
        
    } failure:^(NSError *error) {
        NSLog(@"Failed");
    }];
    
}

#pragma mark - Table view delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return notificationList.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NotificationsTableViewCell *cell = [self.tblView dequeueReusableCellWithIdentifier:@"NotificationsTableViewCell"];
    if(!cell){
        cell = [self.tblView dequeueReusableCellWithIdentifier:@"NotificationsTableViewCell"];
    }
    
    cell.imgIcon.image = nil;
    if (indexPath.row != 0) {
        cell.imgIcon.image = [UIImage imageNamed:[iconImageList objectAtIndex:indexPath.row]];
    }
    cell.imgIcon.layer.cornerRadius = cell.imgIcon.frame.size.width/2;
    cell.imgIcon.backgroundColor = [iconColorList objectAtIndex:indexPath.row];
    
    if (IS_IPAD) {
        cell.lblTitle.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
    }
    cell.lblTitle.text = [notificationList objectAtIndex:indexPath.row];
    cell.toggleSwitch.tag = indexPath.row;
    [cell.toggleSwitch addTarget: self action: @selector(getSwitchAction:) forControlEvents:UIControlEventValueChanged];
    cell.toggleSwitch.on = [[switchValues objectAtIndex:indexPath.row] boolValue];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
}

#pragma mark - View's data Configuration methos
- (void)setupBabyList
{
    babyList = (NSMutableArray *)[NSKeyedUnarchiver unarchiveObjectWithData:[shareObject objectForKey:@"baby_list"]];
    
    selectedBabyIndex = (int)[shareObject integerForKey:@"baby_index"];
    selectedBabyIndex = (selectedBabyIndex >= babyList.count)?0:selectedBabyIndex;
    NSLog(@"selectedBabyIndex1=%d", selectedBabyIndex);
    
    for(int i=0; i<babyList.count; i++)
    {
        NSLog(@"weight=%@", [[babyList objectAtIndex:i] objectForKey:@"weight"]);
        NSLog(@"unit=%@", [unitInfo objectForKey:@"weight"]);
        [[babyList objectAtIndex:i] setObject:[self convertUnit:[[babyList objectAtIndex:i] objectForKey:@"height"] unit:[unitInfo objectForKey:@"height"] spacing:YES] forKey:@"height"];
        [[babyList objectAtIndex:i] setObject:[self convertUnit:[[babyList objectAtIndex:i] objectForKey:@"weight"] unit:[unitInfo objectForKey:@"weight"] spacing:YES] forKey:@"weight"];
    }
    
    // Check for Subscription of selected baby
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"is_Subscription_Enabled"];
    if (![[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"subscription"] isKindOfClass:[NSNull class]]) {
        BOOL isSubscribed = [[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"subscription"] boolValue];
        [[NSUserDefaults standardUserDefaults]setBool:isSubscribed forKey:@"is_Subscription_Enabled"];
    }
    
    NSLog(@"babyList2=%@", babyList);
    if(babyList.count == 1)
    {
        self.babyScrollView.scrollEnabled = NO;
        [self.imgBabyPic2 sd_setImageWithURL:[NSURL URLWithString:[[babyList objectAtIndex:0] objectForKey:@"photo_url"]] placeholderImage:[UIImage imageNamed:@"dummy.png"]];
        self.txtName2.text = [[babyList objectAtIndex:0] objectForKey:@"name"];
        self.txtInfo2.text = [NSString stringWithFormat:LocalizedString(@"txt_baby_info", nil), [[babyList objectAtIndex:0] objectForKey:@"age"], [[babyList objectAtIndex:0] objectForKey:@"height"], [[babyList objectAtIndex:0] objectForKey:@"weight"], [[babyList objectAtIndex:0] objectForKey:@"blood_type"]];
    }
    else
    {
        int prevIndex = selectedBabyIndex-1;
        prevIndex = (prevIndex < 0)?(int)(babyList.count-1):prevIndex;
        int nextIndex = selectedBabyIndex+1;
        nextIndex = (nextIndex >= babyList.count)?0:nextIndex;
        
        self.babyScrollView.scrollEnabled = YES;
        [self.imgBabyPic1 sd_setImageWithURL:[NSURL URLWithString:[[babyList objectAtIndex:prevIndex] objectForKey:@"photo_url"]] placeholderImage:[UIImage imageNamed:@"dummy.png"]];
        self.txtName1.text = [[babyList objectAtIndex:prevIndex] objectForKey:@"name"];
        self.txtInfo1.text = [NSString stringWithFormat:LocalizedString(@"txt_baby_info", nil), [[babyList objectAtIndex:prevIndex] objectForKey:@"age"], [[babyList objectAtIndex:prevIndex] objectForKey:@"height"], [[babyList objectAtIndex:prevIndex] objectForKey:@"weight"], [[babyList objectAtIndex:prevIndex] objectForKey:@"blood_type"]];
        
        [self.imgBabyPic2 sd_setImageWithURL:[NSURL URLWithString:[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"photo_url"]] placeholderImage:[UIImage imageNamed:@"dummy.png"]];
        self.txtName2.text = [[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"name"];
        self.txtInfo2.text = [NSString stringWithFormat:LocalizedString(@"txt_baby_info", nil), [[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"age"], [[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"height"], [[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"weight"], [[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"blood_type"]];
        
        [self.imgBabyPic3 sd_setImageWithURL:[NSURL URLWithString:[[babyList objectAtIndex:nextIndex] objectForKey:@"photo_url"]] placeholderImage:[UIImage imageNamed:@"dummy.png"]];
        self.txtName3.text = [[babyList objectAtIndex:nextIndex] objectForKey:@"name"];
        self.txtInfo3.text = [NSString stringWithFormat:LocalizedString(@"txt_baby_info", nil), [[babyList objectAtIndex:nextIndex] objectForKey:@"age"], [[babyList objectAtIndex:nextIndex] objectForKey:@"height"], [[babyList objectAtIndex:nextIndex] objectForKey:@"weight"], [[babyList objectAtIndex:nextIndex] objectForKey:@"blood_type"]];
    }
    
    [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"feed_info"]] forKey:@"feed_info"];
    [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"pump_info"]] forKey:@"pump_info"];
    [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"sleep_info"]] forKey:@"sleep_info"];
    [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"poo_info"]] forKey:@"poo_info"];
    [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"wee_info"]] forKey:@"wee_info"];
    [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"medical_info"]] forKey:@"medical_info"];
    [shareObject synchronize];
}

#pragma mark UIScrollViewDelegate Methods
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if([scrollView isEqual:self.babyScrollView])
    {
        CGFloat pageWidth = self.view.bounds.size.width;
        int pageNo = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
        NSLog(@"pageNo=%d", pageNo);
        NSLog(@"backupPageNo=%d", backupPageNo);
        
        if(pageNo == 0)
        {
            NSLog(@"selectedBabyIndex=%d", selectedBabyIndex);
            if(backupPageNo != pageNo)
            {
                int nextIndex = selectedBabyIndex;
                nextIndex = (nextIndex >= babyList.count)?0:nextIndex;
                selectedBabyIndex--;
                selectedBabyIndex = (selectedBabyIndex < 0)?(int)(babyList.count-1):selectedBabyIndex;
                int prevIndex = selectedBabyIndex-1;
                prevIndex = (prevIndex < 0)?(int)(babyList.count-1):prevIndex;
                
                NSLog(@"selectedBabyIndex=%d", selectedBabyIndex);
                NSLog(@"prevIndex=%d", prevIndex);
                NSLog(@"nextIndex=%d", nextIndex);
                
                [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"feed_info"]] forKey:@"feed_info"];
                [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"pump_info"]] forKey:@"pump_info"];
                [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"sleep_info"]] forKey:@"sleep_info"];
                [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"poo_info"]] forKey:@"poo_info"];
                [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"wee_info"]] forKey:@"wee_info"];
                [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"medical_info"]] forKey:@"medical_info"];
                [shareObject synchronize];
                
                [self.imgBabyPic1 sd_setImageWithURL:[NSURL URLWithString:[[babyList objectAtIndex:prevIndex] objectForKey:@"photo_url"]] placeholderImage:[UIImage imageNamed:@"dummy.png"]];
                self.txtName1.text = [[babyList objectAtIndex:prevIndex] objectForKey:@"name"];
                self.txtInfo1.text = [NSString stringWithFormat:LocalizedString(@"txt_baby_info", nil), [[babyList objectAtIndex:prevIndex] objectForKey:@"age"], [[babyList objectAtIndex:prevIndex] objectForKey:@"height"], [[babyList objectAtIndex:prevIndex] objectForKey:@"weight"], [[babyList objectAtIndex:prevIndex] objectForKey:@"blood_type"]];
                
                [self.imgBabyPic2 sd_setImageWithURL:[NSURL URLWithString:[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"photo_url"]] placeholderImage:[UIImage imageNamed:@"dummy.png"]];
                self.txtName2.text = [[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"name"];
                self.txtInfo2.text = [NSString stringWithFormat:LocalizedString(@"txt_baby_info", nil), [[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"age"], [[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"height"], [[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"weight"], [[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"blood_type"]];
                
                [self.imgBabyPic3 sd_setImageWithURL:[NSURL URLWithString:[[babyList objectAtIndex:nextIndex] objectForKey:@"photo_url"]] placeholderImage:[UIImage imageNamed:@"dummy.png"]];
                self.txtName3.text = [[babyList objectAtIndex:nextIndex] objectForKey:@"name"];
                self.txtInfo3.text = [NSString stringWithFormat:LocalizedString(@"txt_baby_info", nil), [[babyList objectAtIndex:nextIndex] objectForKey:@"age"], [[babyList objectAtIndex:nextIndex] objectForKey:@"height"], [[babyList objectAtIndex:nextIndex] objectForKey:@"weight"], [[babyList objectAtIndex:nextIndex] objectForKey:@"blood_type"]];
                
                self.babyScrollView.contentOffset = CGPointMake(self.view.frame.size.width, self.babyScrollView.contentOffset.y);
                backupPageNo = 1;
                
                [self getNotificationList];
            }
        }
        else if(pageNo == 2)
        {
            if(backupPageNo != pageNo)
            {
                int prevIndex = selectedBabyIndex;
                prevIndex = (prevIndex < 0)?(int)(babyList.count-1):prevIndex;
                selectedBabyIndex++;
                selectedBabyIndex = (selectedBabyIndex >= babyList.count)?0:selectedBabyIndex;
                int nextIndex = selectedBabyIndex+1;
                nextIndex = (nextIndex >= babyList.count)?0:nextIndex;
                
                NSLog(@"selectedBabyIndex=%d", selectedBabyIndex);
                NSLog(@"prevIndex=%d", prevIndex);
                NSLog(@"nextIndex=%d", nextIndex);
                
                [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"feed_info"]] forKey:@"feed_info"];
                [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"pump_info"]] forKey:@"pump_info"];
                [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"sleep_info"]] forKey:@"sleep_info"];
                [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"poo_info"]] forKey:@"poo_info"];
                [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"wee_info"]] forKey:@"wee_info"];
                [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"medical_info"]] forKey:@"medical_info"];
                [shareObject synchronize];
                
                [self.imgBabyPic1 sd_setImageWithURL:[NSURL URLWithString:[[babyList objectAtIndex:prevIndex] objectForKey:@"photo_url"]] placeholderImage:[UIImage imageNamed:@"dummy.png"]];
                self.txtName1.text = [[babyList objectAtIndex:prevIndex] objectForKey:@"name"];
                self.txtInfo1.text = [NSString stringWithFormat:LocalizedString(@"txt_baby_info", nil), [[babyList objectAtIndex:prevIndex] objectForKey:@"age"], [[babyList objectAtIndex:prevIndex] objectForKey:@"height"], [[babyList objectAtIndex:prevIndex] objectForKey:@"weight"], [[babyList objectAtIndex:prevIndex] objectForKey:@"blood_type"]];
                
                [self.imgBabyPic2 sd_setImageWithURL:[NSURL URLWithString:[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"photo_url"]] placeholderImage:[UIImage imageNamed:@"dummy.png"]];
                self.txtName2.text = [[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"name"];
                self.txtInfo2.text = [NSString stringWithFormat:LocalizedString(@"txt_baby_info", nil), [[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"age"], [[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"height"], [[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"weight"], [[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"blood_type"]];
                
                [self.imgBabyPic3 sd_setImageWithURL:[NSURL URLWithString:[[babyList objectAtIndex:nextIndex] objectForKey:@"photo_url"]] placeholderImage:[UIImage imageNamed:@"dummy.png"]];
                self.txtName3.text = [[babyList objectAtIndex:nextIndex] objectForKey:@"name"];
                self.txtInfo3.text = [NSString stringWithFormat:LocalizedString(@"txt_baby_info", nil), [[babyList objectAtIndex:nextIndex] objectForKey:@"age"], [[babyList objectAtIndex:nextIndex] objectForKey:@"height"], [[babyList objectAtIndex:nextIndex] objectForKey:@"weight"], [[babyList objectAtIndex:nextIndex] objectForKey:@"blood_type"]];
                
                self.babyScrollView.contentOffset = CGPointMake(self.view.frame.size.width, self.babyScrollView.contentOffset.y);
                backupPageNo = 1;
                
            }
        }
        else backupPageNo = pageNo;
        
        // Check for Subscription of selected baby
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"is_Subscription_Enabled"];
        if (![[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"subscription"] isKindOfClass:[NSNull class]]) {
            BOOL isSubscribed = [[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"subscription"] boolValue];
            [[NSUserDefaults standardUserDefaults]setBool:isSubscribed forKey:@"is_Subscription_Enabled"];
        }
        
        [self refreshNotificationListArray];
        [self getNotificationList];
        
    }
}

@end

