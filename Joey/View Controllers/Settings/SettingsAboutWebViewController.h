//
//  SettingsAboutWebViewController.h
//  Joey
//
//  Created by csl on 3/5/2017.
//  Copyright © 2017 Auxilia. All rights reserved.
//

#import "BaseViewController.h"

@interface SettingsAboutWebViewController : BaseViewController <UIWebViewDelegate>

@property (weak, nonatomic) IBOutlet UIWebView *webView;

@end
