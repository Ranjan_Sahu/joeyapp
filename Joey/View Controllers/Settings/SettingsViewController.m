//
//  SettingsViewController.m
//  Joey
//
//  Created by csl on 23/1/2017.
//  Copyright © 2017 Auxilia. All rights reserved.
//

#import "AppDelegate.h"
#import "SettingsViewController.h"
#import "SettingsBabyTableViewCell.h"
#import "SettingsTableViewCell.h"
#import "SubscriptionsViewController.h"
#import "iRate.h"

@interface SettingsViewController ()
{
    NSUserDefaults *shareObject;
    AppDelegate *appDelegate;
    NSMutableArray *settingList;
}
@end

@implementation SettingsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    shareObject = [NSUserDefaults standardUserDefaults];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    [self.navigationController setNavigationBarHidden:NO];
    [self.navigationController.navigationBar setBarTintColor:UIColorFromRGB(0xEEE8E4)];
    [self.navigationController.navigationBar setTranslucent:NO];
    [self.navigationItem setTitle:LocalizedString(@"title_settings", nil)];
    [self customBackButton];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"SettingsTableViewCell" bundle:nil] forCellReuseIdentifier:@"SettingsTableViewCell"];
    
    settingList = [[NSMutableArray alloc] initWithObjects: LocalizedString(@"txt_account", nil), LocalizedString(@"txt_family", nil), LocalizedString(@"txt_subscription", nil), LocalizedString(@"txt_notifications", nil), LocalizedString(@"txt_export", nil), LocalizedString(@"txt_units", nil),  LocalizedString(@"txt_support", nil), LocalizedString(@"txt_review",nil), LocalizedString(@"txt_about", nil), nil];

}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:(BOOL)animated];
}

- (void)customBackButton
{
    if([self.navigationController.viewControllers count] > 1)
    {
        UIImage *imgBack = [UIImage imageNamed:@"nav_btn_back"];
        UIButton *btnBack = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnBack setImage:imgBack forState:UIControlStateNormal];
        btnBack.frame = CGRectMake(0, 0, imgBack.size.width, imgBack.size.height);
        [btnBack addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
        [btnBack setImageEdgeInsets:UIEdgeInsetsMake(0, -16, 0, 16)];
        UIBarButtonItem *backBarItem = [[UIBarButtonItem alloc] initWithCustomView:btnBack];
        self.navigationItem.leftBarButtonItem = backBarItem;
    }
}

- (void)backAction
{
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - Table view data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return settingList.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SettingsTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"SettingsTableViewCell"];
    if(!cell){
        cell = [self.tableView dequeueReusableCellWithIdentifier:@"SettingsTableViewCell"];
    }
    
    if (IS_IPAD) {
        cell.txtName.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
    }
    cell.txtName.text = [settingList objectAtIndex:indexPath.row];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Settings" bundle:nil];
    
    if(indexPath.section == 0)
    {
        if(indexPath.row == 0)
        {
            UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"SettingsAccountViewController"];
            [self.navigationController pushViewController:viewController animated:YES];
        }
        else if(indexPath.row == 1)
        {
            UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"SettingsFamilyViewController"];
            [self.navigationController pushViewController:viewController animated:YES];
        }
        else if(indexPath.row == 2)
        {
            UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"SubscriptionsViewController"];
            [self.navigationController pushViewController:viewController animated:YES];
        }
        else if(indexPath.row == 3)
        {
            UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"SettingsNotificationsViewController"];
            [self.navigationController pushViewController:viewController animated:YES];
        }
        else if(indexPath.row == 4)
        {
            UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"SettingsExportViewController"];
            [self.navigationController pushViewController:viewController animated:YES];
        }
        else if(indexPath.row == 5)
        {
            UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"SettingsUnitsViewController"];
            [self.navigationController pushViewController:viewController animated:YES];
        }
        else if (indexPath.row == 6) {
            UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"SettingsSupportViewController"];
            [self.navigationController pushViewController:viewController animated:YES];
        }
        else if (indexPath.row == 7) {
//#define YOUR_APP_STORE_ID 1256721871 //Change this one to your ID
//
//            static NSString *const iOS7AppStoreURLFormat = @"itms-apps://itunes.apple.com/app/id%d";
//            static NSString *const iOSAppStoreURLFormat = @"itms-apps://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=%d";
//
//            [NSURL URLWithString:[NSString stringWithFormat:([[UIDevice currentDevice].systemVersion floatValue] >= 7.0f)? iOS7AppStoreURLFormat: iOSAppStoreURLFormat, YOUR_APP_STORE_ID]];
            
            [iRate sharedInstance].delegate = (id)self;
            [iRate sharedInstance].previewMode = YES;
            [[iRate sharedInstance] logEvent:NO];
        }
        else if(indexPath.row == 8)
        {
            UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"SettingsAboutViewController"];
            [self.navigationController pushViewController:viewController animated:YES];
        }
    }
}

@end
