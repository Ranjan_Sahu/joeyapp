//
//  SettingsAboutWebViewController.m
//  Joey
//
//  Created by csl on 3/5/2017.
//  Copyright © 2017 Auxilia. All rights reserved.
//

#import "SettingsAboutWebViewController.h"

@interface SettingsAboutWebViewController ()
{
    NSUserDefaults *shareObject;
}
@end

@implementation SettingsAboutWebViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    shareObject = [NSUserDefaults standardUserDefaults];
    
    [self.navigationController setNavigationBarHidden:NO];
    [self.navigationController.navigationBar setBarTintColor:UIColorFromRGB(0xEEE8E4)];
    [self.navigationController.navigationBar setTranslucent:NO];
    [self.navigationItem setTitle:[self.data objectForKey:@"title"]];
    [self customBackButton];
    
    NSURL *url = [NSURL URLWithString:[self.data objectForKey:@"url"]];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    [self.webView loadRequest:requestObj];
    
    if([[self.data objectForKey:@"url"] hasPrefix:@"mailto:"])
    {
        [self.navigationController popViewControllerAnimated:NO];
    }
}

- (void)customBackButton
{
    if([self.navigationController.viewControllers count] > 1)
    {
        UIImage *imgBack = [UIImage imageNamed:@"nav_btn_back"];
        UIButton *btnBack = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnBack setImage:imgBack forState:UIControlStateNormal];
        btnBack.frame = CGRectMake(0, 0, imgBack.size.width, imgBack.size.height);
        [btnBack addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
        [btnBack setImageEdgeInsets:UIEdgeInsetsMake(0, -16, 0, 16)];
        UIBarButtonItem *backBarItem = [[UIBarButtonItem alloc] initWithCustomView:btnBack];
        self.navigationItem.leftBarButtonItem = backBarItem;
    }
}

- (void)backAction
{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
