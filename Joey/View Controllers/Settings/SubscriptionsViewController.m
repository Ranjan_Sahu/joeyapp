//
//  SubscriptionsViewController.m
//  Joey
//
//  Created by webwerks on 2/13/18.
//  Copyright © 2018 Auxilia. All rights reserved.
//

#import "SubscriptionsViewController.h"
#import "AppDelegate.h"
#import "SubscriptionCollectionViewCell.h"
#import "IAPManager.h"
#import <StoreKit/StoreKit.h>

static NSString *yearlyProductId = @"Joey_Yearly_subscription";
static NSString *monthlyProductId = @"Joey_Monthly_subscription";

//IAP Product Identifiers
static NSString *OneBabyGroupMonthly = @"1_Baby_Group_Monthly_New";
static NSString *TwoBabyGroupMonthly = @"2_Baby_Group_Monthly";
static NSString *ThreeBabyGroupMonthly = @"3_Baby_Group_Monthly";
static NSString *FourBabyGroupMonthly = @"4_Baby_Group_Monthly";
static NSString *FiveAndMoreBabyGroupMonthly = @"5_Baby_Group_Monthly";

static NSString *OneBabyGroupYearly = @"1_Baby_Group_Yearly_New";
static NSString *TwoBabyGroupYearly = @"2_Baby_Group_Yearly";
static NSString *ThreeBabyGroupYearly = @"3_Baby_Group_Yearly";
static NSString *FourBabyGroupYearly = @"4_Baby_Group_Yearly";
static NSString *FiveAndMoreBabyGroupYearly = @"5_Baby_Group_Yearly";

static NSString *IAPSharedSecret = @"9cd99e3116da49eca89402a81808355b";

@interface SubscriptionsViewController ()
{
    NSUserDefaults *shareObject;
    AppDelegate *appDelegate;
    NSMutableArray *babyList;
    NSMutableDictionary *unitInfo;
    int backupPageNo;
    int selectedBabyIndex;

    NSMutableArray *subscriptionInfoArr;
    NSMutableArray *monthlyPriceArray;
    NSMutableArray *yearlyPriceArray;
    NSString *currentActivePlan;
}
@end

@implementation SubscriptionsViewController

#pragma mark - View Controller Methods
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    shareObject = [NSUserDefaults standardUserDefaults];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    unitInfo = (NSMutableDictionary *)[NSKeyedUnarchiver unarchiveObjectWithData:[shareObject objectForKey:@"unit_info"]];
    backupPageNo = 1;
    
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:UIColorFromRGB(0x8D8082), NSForegroundColorAttributeName, [UIFont fontWithName:@"SFUIDisplay-Medium" size:18.0f], NSFontAttributeName, nil]];
    [self.navigationController.navigationBar setBarTintColor:UIColorFromRGB(0xEEE8E4)];
    [self.navigationController.navigationBar setTranslucent:NO];
    
    [self.navigationItem setTitle:LocalizedString(@"txt_subscription", nil)];
    [self customBackButton];
    
    self.collectionView.dataSource = self;
    self.collectionView.delegate = self;
    self.babyScrollView.delegate = self;
    [self.collectionView registerNib:[UINib nibWithNibName:@"gallaryImageCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"gallaryImageCollectionViewCell"];
    
    if (IS_IPAD) {
        self.txtName1.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.txtInfo1.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:22.0f];
        self.txtName2.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.txtInfo2.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:22.0f];
        self.txtName3.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.txtInfo3.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:22.0f];
        self.lblCoupon.font = [UIFont fontWithName:@"SFUIDisplay-Regular" size:22.0f];
        
        self.lblStatus.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.lblStatusTxt.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];

        self.btnMonthly.titleLabel.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:20.0f];
        self.btnYearly.titleLabel.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:20.0f];

    }
    
    // Monthly Prices
    monthlyPriceArray = [NSMutableArray new];
    [monthlyPriceArray addObject:@"4.99"];
    [monthlyPriceArray addObject:@"9.49"];
    [monthlyPriceArray addObject:@"13.49"];
    [monthlyPriceArray addObject:@"16.99"];
    [monthlyPriceArray addObject:@"19.99"];
    //Yearly Prices
    yearlyPriceArray = [NSMutableArray new];
    [yearlyPriceArray addObject:@"49.99"];
    [yearlyPriceArray addObject:@"94.99"];
    [yearlyPriceArray addObject:@"134.99"];
    [yearlyPriceArray addObject:@"169.99"];
    [yearlyPriceArray addObject:@"199.99"];
    
    self.lblStatusTxt.text = @"Not Subscribed !";
    self.btnMonthly.layer.cornerRadius = 4.0f;
    self.btnYearly.layer.cornerRadius = 4.0f;
    self.txtFieldCouponCode.delegate = self;
    self.btnMonthly.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    self.btnMonthly.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.btnYearly.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    self.btnYearly.titleLabel.textAlignment = NSTextAlignmentCenter;

    [self setupBabyList];
    [self getSubscriptionDetails];
    [self checkAndRestoreReceipt];
//    [self validateReceipt];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:(BOOL)animated];
    [self.navigationController.navigationBar setBarTintColor:UIColorFromRGB(0xEEE8E4)];
    
    unitInfo = (NSMutableDictionary *)[NSKeyedUnarchiver unarchiveObjectWithData:[shareObject objectForKey:@"unit_info"]];
    NSLog(@"unitInfo=%@", unitInfo);
    NSLog(@"here=%@", self.data);
    NSLog(@"selectedBabyIndex 5=%d", selectedBabyIndex);
    
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}

#pragma mark - UI Customization & Layout Hadling Methods
- (void)customBackButton
{
    if([self.navigationController.viewControllers count] > 1)
    {
        UIImage *imgBack = [UIImage imageNamed:@"nav_btn_back"];
        UIButton *btnBack = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnBack setImage:imgBack forState:UIControlStateNormal];
        btnBack.frame = CGRectMake(0, 0, imgBack.size.width, imgBack.size.height);
        [btnBack addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
        [btnBack setImageEdgeInsets:UIEdgeInsetsMake(0, -16, 0, 16)];
        UIBarButtonItem *backBarItem = [[UIBarButtonItem alloc] initWithCustomView:btnBack];
        self.navigationItem.leftBarButtonItem = backBarItem;
    }
}

- (void)viewDidLayoutSubviews
{
    if(self.babyScrollView.contentSize.width == 0)
    {
        self.babyScrollView.contentSize = CGSizeMake(self.view.frame.size.width*3, 1);
        self.babyScrollView.contentOffset = CGPointMake(self.view.frame.size.width, self.babyScrollView.contentOffset.y);
    }
    
    self.imgBabyPic1.clipsToBounds = self.imgBabyPic2.clipsToBounds = self.imgBabyPic3.clipsToBounds = YES;
    self.imgBabyPic1.layer.cornerRadius = self.imgBabyPic1.frame.size.width/2;
    self.imgBabyPic2.layer.cornerRadius = self.imgBabyPic2.frame.size.width/2;
    self.imgBabyPic3.layer.cornerRadius = self.imgBabyPic3.frame.size.width/2;
}

#pragma mark - IBAction Methods
- (IBAction)didSelectYearly:(UIButton *)sender
{
    if ([sender.titleLabel.text isEqualToString:@"Cancel"]) {
        [self showAlertWithTitle:@"Subscription" message:@"To cancel the subscription please visit your apple account." input:nil];
        return;
    }
    
    int idIndex = 0;
    if (subscriptionInfoArr && ![subscriptionInfoArr isKindOfClass:[NSNull class]] && subscriptionInfoArr.count>0) {
        idIndex = (int)subscriptionInfoArr.count;
        if (subscriptionInfoArr.count>=5) {
            [self showAlertWithTitle:@"Subscription" message:@"You have already subscribed to highest level." input:nil];
            return;
        }
    }
    
    self.btnYearly.alpha = 1.0;
    self.btnMonthly.alpha = 0.7;
    
    NSString *requestingProductId = OneBabyGroupYearly;
    if (idIndex == 0) {
        requestingProductId = OneBabyGroupYearly;
    }else if (idIndex == 1) {
        requestingProductId = TwoBabyGroupYearly;
    }else if (idIndex == 2) {
        requestingProductId = ThreeBabyGroupYearly;
    }else if (idIndex == 3) {
        requestingProductId = FourBabyGroupYearly;
    }else if (idIndex == 4) {
        requestingProductId = FiveAndMoreBabyGroupYearly;
    }
    
    [SVProgressHUD show];

    [[IAPManager sharedIAPManager] getProductsForIds:@[requestingProductId] completion:^(NSArray *products) {
        BOOL isProductAvailable = [products count] != 0;
        if(!isProductAvailable) {
            NSLog(@" Product not found !");
            self.btnYearly.alpha = 1.0;
        }
        else {
            SKProduct *premium = products[0];
            
            NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];                                                                                                                 [numberFormatter setFormatterBehavior:NSNumberFormatterBehavior10_4];
            [numberFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
            [numberFormatter setLocale:premium.priceLocale];
            NSString *formattedPrice = [numberFormatter stringFromNumber:premium.price];
            NSLog(@"super premium: %@ for %@", premium.localizedTitle, formattedPrice);
            
            [self startPurchaseWithName:premium.productIdentifier];
            self.btnYearly.alpha = 1.0;
        }
    } error:^(NSError *error) {
        NSLog(@"Product not found !");
    }];
    
}

- (IBAction)didSelectMonthly:(UIButton *)sender
{
    //for testing only, remove next two lines
//    [self callSubscriptionServiceWithProduct:OneBabyGroupMonthly andSubscriptionMode:@"purchase"];
//    return;
    
    
    if ([sender.titleLabel.text isEqualToString:@"Cancel"]) {
        [self showAlertWithTitle:@"Subscription" message:@"To cancel the subscription please visit your apple account." input:nil];
        return;
    }
    
    int idIndex = 0;
    if (subscriptionInfoArr && ![subscriptionInfoArr isKindOfClass:[NSNull class]] && subscriptionInfoArr.count>0) {
        idIndex = (int)subscriptionInfoArr.count;
        if (subscriptionInfoArr.count>=5) {
            [self showAlertWithTitle:@"Subscription" message:@"You have already subscribed to highest level." input:nil];
            return;
        }
    }
    
    self.btnYearly.alpha = 1.0;
    self.btnMonthly.alpha = 0.7;

    NSString *requestingProductId = OneBabyGroupMonthly;
    if (idIndex == 0) {
        requestingProductId = OneBabyGroupMonthly;
    }else if (idIndex == 1) {
        requestingProductId = TwoBabyGroupMonthly;
    }else if (idIndex == 2) {
        requestingProductId = ThreeBabyGroupMonthly;
    }else if (idIndex == 3) {
        requestingProductId = FourBabyGroupMonthly;
    }else if (idIndex == 4) {
        requestingProductId = FiveAndMoreBabyGroupMonthly;
    }
    
    [SVProgressHUD show];
    
    [[IAPManager sharedIAPManager] getProductsForIds:@[requestingProductId] completion:^(NSArray *products) {
        
        [SVProgressHUD dismiss];
        BOOL isProductAvailable = [products count] != 0;
        if(!isProductAvailable) {
            NSLog(@" Product not found !");
            self.btnMonthly.alpha = 1.0;
        }
        else {
            SKProduct *premium = products[0];
            
            NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];                                                                                                                 [numberFormatter setFormatterBehavior:NSNumberFormatterBehavior10_4];
            [numberFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
            [numberFormatter setLocale:premium.priceLocale];
            NSString *formattedPrice = [numberFormatter stringFromNumber:premium.price];
            NSLog(@"super premium: %@ for %@", premium.localizedTitle, formattedPrice);
            
            [self startPurchaseWithName:premium.productIdentifier];
            self.btnMonthly.alpha = 1.0;
            
        }
    } error:^(NSError *error) {
        [SVProgressHUD dismiss];
        NSLog(@"Product not found !");
    }];
    
}

-(void)startPurchaseWithName:(NSString *)productId
{
    [SVProgressHUD showWithStatus:@"Purchasing... "];
    [[IAPManager sharedIAPManager] purchaseProductForId:productId
                                             completion:^(SKPaymentTransaction *transaction) {
                                                 
                                                 [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                                                 [SVProgressHUD dismissWithCompletion:^{
                                                     [self showAlertWithTitleCallback:@"Thanks !" message:@"Product is purchased successfully" callback:^(UIAlertAction *action) {
                                                         [self callSubscriptionServiceWithProduct:productId andSubscriptionMode:@"purchase"];
                                                     }];
                                                 }];
                                                 
                                             } error:^(NSError *err) {
                                                 
                                                 [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                                                 [SVProgressHUD dismissWithCompletion:^{
                                                     [self showAlertWithTitleCallback:@"Failed !" message:@"An error occured while purchasing" callback:^(UIAlertAction *action) {
                                                         
                                                     }];
                                                 }];
                                             }];
}

#pragma mark - UITextfield Delegate
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    NSLog(@"textFieldDidEndEditing");
    if (textField.text.length>0) {
        //[self callSubscriptionServiceWithProduct:textField.text andSubscriptionMode:@"coupon"];
    }
}


#pragma mark - Custom Actions
- (void)backAction
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)updateSubscriptionButtonState
{
    if (subscriptionInfoArr.count==0) {
        return;
    }
    self.btnYearly.alpha = 1.0;
    self.btnMonthly.alpha = 1.0;
    
    //    subscriptionInfoArr = [NSMutableArray new];
    //    for (NSDictionary *dict in babyList) {
    //        if (![[dict objectForKey:@"subscription"] isKindOfClass:[NSNull class]]) {
    //            BOOL isSubscribed = [[dict objectForKey:@"subscription"] boolValue];
    //            if (isSubscribed) {
    //                [subscriptionInfoArr addObject:[dict valueForKey:@"name"]];
    //            }
    //        }
    //    }
    
    int index = 0;
    if (subscriptionInfoArr && ![subscriptionInfoArr isKindOfClass:[NSNull class]] && subscriptionInfoArr.count>0) {
        index = (int)subscriptionInfoArr.count;
        if (subscriptionInfoArr.count>=5) {
            index = 0;
        }
    }
    
    NSString *mPrice = [NSString stringWithFormat:@"Monthly\nUS$%@\nPer Month",[monthlyPriceArray objectAtIndex:index]];
    NSString *yPrice = [NSString stringWithFormat:@"Yearly\nUS$%@\nPer Year",[yearlyPriceArray objectAtIndex:index]];
    
    if (subscriptionInfoArr && ![subscriptionInfoArr isKindOfClass:[NSNull class]] && subscriptionInfoArr.count>0) {
        if (subscriptionInfoArr.count>5) {
            mPrice = [NSString stringWithFormat:@"Unlimited\nAccess"];
            yPrice = [NSString stringWithFormat:@"Unlimited\nAccess"];
        }
    }
    
    [self.btnMonthly setTitle:mPrice forState:UIControlStateNormal];
    [self.btnYearly setTitle:yPrice forState:UIControlStateNormal];
    
    if ([[NSUserDefaults standardUserDefaults]boolForKey:@"is_Subscription_Enabled"]) {
        if (currentActivePlan && ![currentActivePlan isKindOfClass:[NSNull class]] && currentActivePlan.length>0) {
            if ([currentActivePlan isEqualToString:@"month"]) {
                [self.btnMonthly setTitle:@"Cancel" forState:UIControlStateNormal];
                if (index != 0){
                    yPrice = [NSString stringWithFormat:@"Yearly\nUS$%@\nPer Year",[yearlyPriceArray objectAtIndex:index - 1]];
                    [self.btnYearly setTitle:yPrice forState:UIControlStateNormal];
                }
            }else {
                [self.btnYearly setTitle:@"Cancel" forState:UIControlStateNormal];
            }
        }
    }
    
}

#pragma mark - Webservice calls
- (void)getSubscriptionDetails
{
    [SVProgressHUD show];
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setValue:@([shareObject integerForKey:@"user_id"]) forKey:@"user_id"];
    
    [[WebServiceManager sharedManager] initWithBaseURL:@"api/getSubscription" parameters:parameters success:^(id result) {
        
        if ([[result valueForKey:@"success"] boolValue]) {
            [SVProgressHUD dismiss];
            // Handle response
            if ([[result objectForKey:@"list"] isKindOfClass:[NSNull class]]) {
                [subscriptionInfoArr removeAllObjects];
            }else {
                subscriptionInfoArr = [[NSMutableArray alloc] initWithArray:[result objectForKey:@"list"]];
            }
            currentActivePlan = [result objectForKey:@"current_active_plan"];
            [self updateSubscriptionButtonState];
            
        }else{
            [SVProgressHUD dismiss];
            [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:[result objectForKey:@"message"] input:nil];
        }
        
    } failure:^(NSError *error) {
        [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:LocalizedString(@"error_unknown", nil) input:nil];
        [SVProgressHUD dismiss];
    }];
    
}

- (void)callSubscriptionServiceWithProduct:(NSString *)pName andSubscriptionMode:(NSString *)mode
{
    [SVProgressHUD show];

    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setValue:@([shareObject integerForKey:@"user_id"]) forKey:@"user_id"];
    
    // set assigned babies
    NSMutableArray *idsArray = [NSMutableArray new];
    if (subscriptionInfoArr && ![subscriptionInfoArr isKindOfClass:[NSNull class]] && subscriptionInfoArr.count>0) {
        for (NSDictionary *dict in subscriptionInfoArr) {
            NSString *bID = [NSString stringWithFormat:@"%@",[dict objectForKey:@"baby_id"]];
            [idsArray addObject:bID];
        }
    }
    NSString *babyId = [NSString stringWithFormat:@"%ld",(long)[shareObject integerForKey:@"baby_id"]];
    [idsArray addObject:babyId];
    [parameters setValue:[idsArray componentsJoinedByString:@","] forKey:@"baby_id"];
    
    if ([pName containsString:@"Monthly"]) {
        [parameters setValue:@"month" forKey:@"product_name"];
    }else if ([pName containsString:@"Yearly"]){
        [parameters setValue:@"year" forKey:@"product_name"];
    }else {
        [parameters setValue:pName forKey:@"product_name"]; //Coupon Code
    }
    
    NSData *receiptData = [[IAPManager sharedIAPManager] loadReceipt];
    if (!receiptData) {
        NSLog(@"Receipt not found !");
        [self showRestoreAlert];
        [parameters setValue:@"" forKey:@"receipt_data"];
        [parameters setValue:@"" forKey:@"shared_secret"];
    }
    else{
        NSString *receiptString = [receiptData base64EncodedStringWithOptions:0];
        [parameters setValue:receiptString forKey:@"receipt_data"];
        [parameters setValue:IAPSharedSecret forKey:@"shared_secret"];
    }
    
    [SVProgressHUD showWithStatus:@"Please wait..."];
    
//    NSDictionary *rDict = @{@"receipt-data" : receiptString, @"shared_secret" : IAPSharedSecret};
    
    [parameters setValue:mode forKey:@"mode"];

    [[WebServiceManager sharedManager] initWithBaseURL:@"api/addSubscripton" parameters:parameters success:^(id result) {

        if ([[result valueForKey:@"success"] boolValue] == false) {
            [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:[result objectForKey:@"message"] input:nil];
            [SVProgressHUD dismiss];
        }else{
            
            selectedBabyIndex = (int)[shareObject integerForKey:@"baby_index"];
            // Update Baby Details dictionary
            NSMutableArray *babyList = (NSMutableArray *)[NSKeyedUnarchiver unarchiveObjectWithData:[shareObject objectForKey:@"baby_list"]];
            
//            NSMutableArray *allBabyList = (NSMutableArray )[NSKeyedUnarchiver unarchiveObjectWithData:[shareObject objectForKey:@"baby_list"]];
            
            //filtered baby list
            //change by Pradumna
//            NSMutableArray *babyList = [[allBabyList filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(master_check == 1)"]]mutableCopy];
            
            
            [babyList replaceObjectAtIndex:selectedBabyIndex withObject:[result valueForKey:@"baby_list"]];
            
            [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:babyList] forKey:@"baby_list"];
            [shareObject synchronize];
            // Update Subscription Status
            babyList = (NSMutableArray *)[NSKeyedUnarchiver unarchiveObjectWithData:[shareObject objectForKey:@"baby_list"]];
            //NSString *subscriptionStatus = [[babyList objectAtIndex:selectedBabyIndex] valueForKey:@"subscription_status"];
            //self.lblStatusTxt.text = subscriptionStatus;
            
            [SVProgressHUD dismiss];
            //[self updateSubscriptionButtonState];
            //[self showAlertWithTitle:@"Congratulations !" message:@"Now you are a premium user of Joey" input:nil];
            [self showAlertWithTitleCallback:@"Congratulations !" message:@"Now you are a premium user of Joey" callback:^(UIAlertAction *action) {
                [self getSubscriptionDetails];
            }];

        }

    } failure:^(NSError *error) {
        [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:LocalizedString(@"error_unknown", nil) input:nil];
        [SVProgressHUD dismiss];
    }];
    
}

- (void)cancelSubscription
{
    [SVProgressHUD show];
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setValue:@([shareObject integerForKey:@"user_id"]) forKey:@"user_id"];
    
    [[WebServiceManager sharedManager] initWithBaseURL:@"api/cancelSubscription" parameters:parameters success:^(id result) {
        
        if ([[result valueForKey:@"success"] boolValue]) {
            [SVProgressHUD dismiss];
            // Handle response
        }else{
            [SVProgressHUD dismiss];
            [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:[result objectForKey:@"message"] input:nil];
        }
        
    } failure:^(NSError *error) {
        [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:LocalizedString(@"error_unknown", nil) input:nil];
        [SVProgressHUD dismiss];
    }];
    
}

#pragma mark - Collection view Data Source & Delegates
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 5;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    [collectionView registerNib:[UINib nibWithNibName:@"SubscriptionCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"SubscriptionCollectionViewCell"];
    SubscriptionCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"SubscriptionCollectionViewCell" forIndexPath:indexPath];
    
    cell.lblTitle.text = @"Get Joey Premium Now !";
    cell.lblSubTitle.text = @"";
    cell.lblDescription1.text = @"";
    cell.lblDescription2.text = @"";
    cell.lblDescription3.text = @"";
    cell.lblDescription4.text = @"";
    cell.lblDescription5.text = @"";
    
    if (indexPath.row == 0) {
        cell.lblDescription1.text = @"Subscribe to get more enjoy these additional benefits to make parenting easier!";
        cell.lblDescription2.text = @" - Proactive Parenting to always be one step ahead.";
        cell.lblDescription3.text = @" - Make recording easier using wearables or your voice.";
        cell.lblDescription4.text = @" - Relive your baby’s firsts with automated snapshots.";
        cell.lblDescription5.text = @" - Add Video to your special moments.";
    }
    else if (indexPath.row == 1) {
        cell.lblSubTitle.text = @"Proactive Parenting";
        cell.lblDescription1.text = @"Get rewarded for all your effort recording! Let Joey help analyze, identify and notify you of insights and potential issues regarding your baby’s on-going health and development.";
        cell.lblDescription2.text = @"Also receive reminders on upcoming baby milestones for each month of baby’s age to never miss out!";

    }else if (indexPath.row == 2) {
        cell.lblSubTitle.text = @"Make Recording Easier";
        cell.lblDescription1.text = @"Log baby’s activities easier using your smartwatch like Apple Watch and Android Wear 2 compatible devices) or your voice on smart speakers like Amazon Alexa or Google Home.";

    }else if (indexPath.row == 3) {
        cell.lblSubTitle.text = @"Relive Baby’s Firsts";
        cell.lblDescription1.text = @"Joey will send you beautifully designed baby cards with their achievements and first moments captured in Joey periodically or on-demand.";

    }else if (indexPath.row == 4) {
        cell.lblSubTitle.text = @"Add Video";
        cell.lblDescription1.text = @"in addition to photos, add and share videos of baby’s activities and milestones securely and privately between your extended family.";
    }
    
    [cell layoutIfNeeded];
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    int w = self.collectionView.bounds.size.width;
    int h = self.collectionView.bounds.size.height;
    return CGSizeMake(w, h);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    //    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Activities" bundle:nil];
    //    UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"ActivityFeedViewController"];
    //    viewController.data = [[NSMutableDictionary alloc] initWithDictionary:@{@"monthsold":[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"monthsold"]}];
    //    viewController.hidesBottomBarWhenPushed = YES;
    //    [self.navigationController pushViewController:viewController animated:YES];
}

-(void)collectionView:(UICollectionView *)collectionView willDisplayCell:(nonnull UICollectionViewCell *)cell forItemAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    self.pgControl.currentPage = indexPath.row;
}

#pragma mark - UIScrollViewDelegate Methods
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if([scrollView isEqual:self.babyScrollView])
    {
        CGFloat pageWidth = self.view.bounds.size.width;
        int pageNo = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
        NSLog(@"pageNo=%d", pageNo);
        NSLog(@"backupPageNo=%d", backupPageNo);
        
        if(pageNo == 0)
        {
            NSLog(@"selectedBabyIndex=%d", selectedBabyIndex);
            if(backupPageNo != pageNo)
            {
                int nextIndex = selectedBabyIndex;
                nextIndex = (nextIndex >= babyList.count)?0:nextIndex;
                selectedBabyIndex--;
                selectedBabyIndex = (selectedBabyIndex < 0)?(int)(babyList.count-1):selectedBabyIndex;
                int prevIndex = selectedBabyIndex-1;
                prevIndex = (prevIndex < 0)?(int)(babyList.count-1):prevIndex;
                
                NSLog(@"selectedBabyIndex=%d", selectedBabyIndex);
                NSLog(@"prevIndex=%d", prevIndex);
                NSLog(@"nextIndex=%d", nextIndex);
                
                [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"feed_info"]] forKey:@"feed_info"];
                [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"pump_info"]] forKey:@"pump_info"];
                [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"sleep_info"]] forKey:@"sleep_info"];
                [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"poo_info"]] forKey:@"poo_info"];
                [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"wee_info"]] forKey:@"wee_info"];
                [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"medical_info"]] forKey:@"medical_info"];
                [shareObject synchronize];
                
                [self.imgBabyPic1 sd_setImageWithURL:[NSURL URLWithString:[[babyList objectAtIndex:prevIndex] objectForKey:@"photo_url"]] placeholderImage:[UIImage imageNamed:@"dummy.png"]];
                self.txtName1.text = [[babyList objectAtIndex:prevIndex] objectForKey:@"name"];
                self.txtInfo1.text = [NSString stringWithFormat:LocalizedString(@"txt_baby_info", nil), [[babyList objectAtIndex:prevIndex] objectForKey:@"age"], [[babyList objectAtIndex:prevIndex] objectForKey:@"height"], [[babyList objectAtIndex:prevIndex] objectForKey:@"weight"], [[babyList objectAtIndex:prevIndex] objectForKey:@"blood_type"]];
                
                [self.imgBabyPic2 sd_setImageWithURL:[NSURL URLWithString:[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"photo_url"]] placeholderImage:[UIImage imageNamed:@"dummy.png"]];
                self.txtName2.text = [[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"name"];
                self.txtInfo2.text = [NSString stringWithFormat:LocalizedString(@"txt_baby_info", nil), [[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"age"], [[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"height"], [[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"weight"], [[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"blood_type"]];
                
                [self.imgBabyPic3 sd_setImageWithURL:[NSURL URLWithString:[[babyList objectAtIndex:nextIndex] objectForKey:@"photo_url"]] placeholderImage:[UIImage imageNamed:@"dummy.png"]];
                self.txtName3.text = [[babyList objectAtIndex:nextIndex] objectForKey:@"name"];
                self.txtInfo3.text = [NSString stringWithFormat:LocalizedString(@"txt_baby_info", nil), [[babyList objectAtIndex:nextIndex] objectForKey:@"age"], [[babyList objectAtIndex:nextIndex] objectForKey:@"height"], [[babyList objectAtIndex:nextIndex] objectForKey:@"weight"], [[babyList objectAtIndex:nextIndex] objectForKey:@"blood_type"]];
                
                self.babyScrollView.contentOffset = CGPointMake(self.view.frame.size.width, self.babyScrollView.contentOffset.y);
                backupPageNo = 1;
                
                [self refreshAction];
            }
        }
        else if(pageNo == 2)
        {
            if(backupPageNo != pageNo)
            {
                int prevIndex = selectedBabyIndex;
                prevIndex = (prevIndex < 0)?(int)(babyList.count-1):prevIndex;
                selectedBabyIndex++;
                selectedBabyIndex = (selectedBabyIndex >= babyList.count)?0:selectedBabyIndex;
                int nextIndex = selectedBabyIndex+1;
                nextIndex = (nextIndex >= babyList.count)?0:nextIndex;
                
                NSLog(@"selectedBabyIndex=%d", selectedBabyIndex);
                NSLog(@"prevIndex=%d", prevIndex);
                NSLog(@"nextIndex=%d", nextIndex);
                
                [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"feed_info"]] forKey:@"feed_info"];
                [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"pump_info"]] forKey:@"pump_info"];
                [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"sleep_info"]] forKey:@"sleep_info"];
                [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"poo_info"]] forKey:@"poo_info"];
                [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"wee_info"]] forKey:@"wee_info"];
                [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"medical_info"]] forKey:@"medical_info"];
                [shareObject synchronize];
                
                [self.imgBabyPic1 sd_setImageWithURL:[NSURL URLWithString:[[babyList objectAtIndex:prevIndex] objectForKey:@"photo_url"]] placeholderImage:[UIImage imageNamed:@"dummy.png"]];
                self.txtName1.text = [[babyList objectAtIndex:prevIndex] objectForKey:@"name"];
                self.txtInfo1.text = [NSString stringWithFormat:LocalizedString(@"txt_baby_info", nil), [[babyList objectAtIndex:prevIndex] objectForKey:@"age"], [[babyList objectAtIndex:prevIndex] objectForKey:@"height"], [[babyList objectAtIndex:prevIndex] objectForKey:@"weight"], [[babyList objectAtIndex:prevIndex] objectForKey:@"blood_type"]];
                
                [self.imgBabyPic2 sd_setImageWithURL:[NSURL URLWithString:[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"photo_url"]] placeholderImage:[UIImage imageNamed:@"dummy.png"]];
                self.txtName2.text = [[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"name"];
                self.txtInfo2.text = [NSString stringWithFormat:LocalizedString(@"txt_baby_info", nil), [[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"age"], [[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"height"], [[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"weight"], [[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"blood_type"]];
                
                [self.imgBabyPic3 sd_setImageWithURL:[NSURL URLWithString:[[babyList objectAtIndex:nextIndex] objectForKey:@"photo_url"]] placeholderImage:[UIImage imageNamed:@"dummy.png"]];
                self.txtName3.text = [[babyList objectAtIndex:nextIndex] objectForKey:@"name"];
                self.txtInfo3.text = [NSString stringWithFormat:LocalizedString(@"txt_baby_info", nil), [[babyList objectAtIndex:nextIndex] objectForKey:@"age"], [[babyList objectAtIndex:nextIndex] objectForKey:@"height"], [[babyList objectAtIndex:nextIndex] objectForKey:@"weight"], [[babyList objectAtIndex:nextIndex] objectForKey:@"blood_type"]];
                
                self.babyScrollView.contentOffset = CGPointMake(self.view.frame.size.width, self.babyScrollView.contentOffset.y);
                backupPageNo = 1;
                
                [self refreshAction];
            }
        }
        else backupPageNo = pageNo;
        
        // Check for Subscription of selected baby
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"is_Subscription_Enabled"];
        if (![[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"subscription"] isKindOfClass:[NSNull class]]) {
            BOOL isSubscribed = [[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"subscription"] boolValue];
            [[NSUserDefaults standardUserDefaults]setBool:isSubscribed forKey:@"is_Subscription_Enabled"];
        }
        
        self.lblStatusTxt.text = @"Not Subscribed !";
        NSString *subscriptionStatus = [[babyList objectAtIndex:selectedBabyIndex] valueForKey:@"subscription_status"];
        if (![subscriptionStatus isKindOfClass:[NSNull class]] && subscriptionStatus.length>0) {
            self.lblStatusTxt.text = [[babyList objectAtIndex:selectedBabyIndex] valueForKey:@"subscription_status"];
        }
        
        [self updateSubscriptionButtonState];
    }
    else if ([scrollView isEqual:self.collectionView]) {
        self.pgControl.currentPage = self.collectionView.contentOffset.x / self.collectionView.frame.size.width;
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if ([scrollView isEqual:self.collectionView]) {
        self.pgControl.currentPage = self.collectionView.contentOffset.x / self.collectionView.frame.size.width;
    }
    
}
#pragma mark -  View's Data Configuration methods
-(void) refreshAction
{
    [SVProgressHUD show];
    [SVProgressHUD dismiss];
    [shareObject setInteger:[[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"id"] intValue] forKey:@"baby_id"];
    [shareObject setInteger:selectedBabyIndex forKey:@"baby_index"];
    [shareObject synchronize];

    //[self.collectionView.pullToRefreshView stopAnimating];
}

- (void)setupBabyList
{
    babyList = (NSMutableArray *)[NSKeyedUnarchiver unarchiveObjectWithData:[shareObject objectForKey:@"baby_list"]];\
    
    //filtered baby list acc to master check
    //change by Pradumna
    //    NSMutableArray allBabyList = (NSMutableArray )[NSKeyedUnarchiver unarchiveObjectWithData:[shareObject objectForKey:@"baby_list"]];
    //    babyList = [[allBabyList filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(master_check == 1)"]]mutableCopy];
    
    
    selectedBabyIndex = (int)[shareObject integerForKey:@"baby_index"];
    selectedBabyIndex = (selectedBabyIndex >= babyList.count)?0:selectedBabyIndex;
    NSLog(@"selectedBabyIndex1=%d", selectedBabyIndex);
    
    for(int i=0; i<babyList.count; i++)
    {
        NSLog(@"weight=%@", [[babyList objectAtIndex:i] objectForKey:@"weight"]);
        NSLog(@"unit=%@", [unitInfo objectForKey:@"weight"]);
        [[babyList objectAtIndex:i] setObject:[self convertUnit:[[babyList objectAtIndex:i] objectForKey:@"height"] unit:[unitInfo objectForKey:@"height"] spacing:YES] forKey:@"height"];
        [[babyList objectAtIndex:i] setObject:[self convertUnit:[[babyList objectAtIndex:i] objectForKey:@"weight"] unit:[unitInfo objectForKey:@"weight"] spacing:YES] forKey:@"weight"];
    }
    
    // Check for Subscription of selected baby
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"is_Subscription_Enabled"];
    if (![[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"subscription"] isKindOfClass:[NSNull class]]) {
        BOOL isSubscribed = [[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"subscription"] boolValue];
        [[NSUserDefaults standardUserDefaults]setBool:isSubscribed forKey:@"is_Subscription_Enabled"];
    }
    
    NSLog(@"babyList2=%@", babyList);
    if(babyList.count == 1)
    {
        self.babyScrollView.scrollEnabled = NO;
        [self.imgBabyPic2 sd_setImageWithURL:[NSURL URLWithString:[[babyList objectAtIndex:0] objectForKey:@"photo_url"]] placeholderImage:[UIImage imageNamed:@"dummy.png"]];
        self.txtName2.text = [[babyList objectAtIndex:0] objectForKey:@"name"];
        self.txtInfo2.text = [NSString stringWithFormat:LocalizedString(@"txt_baby_info", nil), [[babyList objectAtIndex:0] objectForKey:@"age"], [[babyList objectAtIndex:0] objectForKey:@"height"], [[babyList objectAtIndex:0] objectForKey:@"weight"], [[babyList objectAtIndex:0] objectForKey:@"blood_type"]];
    }
    else
    {
        int prevIndex = selectedBabyIndex-1;
        prevIndex = (prevIndex < 0)?(int)(babyList.count-1):prevIndex;
        int nextIndex = selectedBabyIndex+1;
        nextIndex = (nextIndex >= babyList.count)?0:nextIndex;
        
        self.babyScrollView.scrollEnabled = YES;
        [self.imgBabyPic1 sd_setImageWithURL:[NSURL URLWithString:[[babyList objectAtIndex:prevIndex] objectForKey:@"photo_url"]] placeholderImage:[UIImage imageNamed:@"dummy.png"]];
        self.txtName1.text = [[babyList objectAtIndex:prevIndex] objectForKey:@"name"];
        self.txtInfo1.text = [NSString stringWithFormat:LocalizedString(@"txt_baby_info", nil), [[babyList objectAtIndex:prevIndex] objectForKey:@"age"], [[babyList objectAtIndex:prevIndex] objectForKey:@"height"], [[babyList objectAtIndex:prevIndex] objectForKey:@"weight"], [[babyList objectAtIndex:prevIndex] objectForKey:@"blood_type"]];
        
        [self.imgBabyPic2 sd_setImageWithURL:[NSURL URLWithString:[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"photo_url"]] placeholderImage:[UIImage imageNamed:@"dummy.png"]];
        self.txtName2.text = [[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"name"];
        self.txtInfo2.text = [NSString stringWithFormat:LocalizedString(@"txt_baby_info", nil), [[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"age"], [[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"height"], [[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"weight"], [[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"blood_type"]];
        
        [self.imgBabyPic3 sd_setImageWithURL:[NSURL URLWithString:[[babyList objectAtIndex:nextIndex] objectForKey:@"photo_url"]] placeholderImage:[UIImage imageNamed:@"dummy.png"]];
        self.txtName3.text = [[babyList objectAtIndex:nextIndex] objectForKey:@"name"];
        self.txtInfo3.text = [NSString stringWithFormat:LocalizedString(@"txt_baby_info", nil), [[babyList objectAtIndex:nextIndex] objectForKey:@"age"], [[babyList objectAtIndex:nextIndex] objectForKey:@"height"], [[babyList objectAtIndex:nextIndex] objectForKey:@"weight"], [[babyList objectAtIndex:nextIndex] objectForKey:@"blood_type"]];
    }
    
    [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"feed_info"]] forKey:@"feed_info"];
    [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"pump_info"]] forKey:@"pump_info"];
    [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"sleep_info"]] forKey:@"sleep_info"];
    [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"poo_info"]] forKey:@"poo_info"];
    [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"wee_info"]] forKey:@"wee_info"];
    [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"medical_info"]] forKey:@"medical_info"];
    [shareObject synchronize];
    
    self.lblStatusTxt.text = @"Not Subscribed !";
    NSString *subscriptionStatus = [[babyList objectAtIndex:selectedBabyIndex] valueForKey:@"subscription_status"];
    if (![subscriptionStatus isKindOfClass:[NSNull class]] && subscriptionStatus.length>0) {
        self.lblStatusTxt.text = [[babyList objectAtIndex:selectedBabyIndex] valueForKey:@"subscription_status"];
    }
    
    [self updateSubscriptionButtonState];
}

#pragma mark - Auto-Renewable Subscription
- (void) showRestoreAlert
{
    [self showConfirmAlert:@"Subscription Issue " message:@"We are having a hard time finding your subscription. If you've recently reinstalled the app or got a new device please choose to restore your purchase." callback:^(UIAlertAction *action) {
        [self startRestore];
    }];
}

- (void)startRestore
{
    [SVProgressHUD showWithStatus:@"Restoring Purchase... "];
    [[IAPManager sharedIAPManager] restorePurchasesWithCompletion:^{
        [SVProgressHUD showWithStatus:@"Your Purchase is restored."];
        [SVProgressHUD dismissWithDelay:1.0];
    } error:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:@"Your Purchase can not be restored."];
        [SVProgressHUD dismissWithDelay:1.0];
    }];
}

- (void)checkAndRestoreReceipt
{
    NSData *receiptData = [[IAPManager sharedIAPManager] loadReceipt];
    NSString *receiptString = [receiptData base64EncodedStringWithOptions:0];
    NSLog(@"%@",receiptString);
    if (!receiptData) {
        NSLog(@"Receipt not found !");
        [self showRestoreAlert];
        return;
    }
}

- (void)validateReceipt
{
    NSError *error;
    NSData *receiptData = [[IAPManager sharedIAPManager] loadReceipt];
    if (!receiptData) {
        NSLog(@"Receipt not found !");
        [self showRestoreAlert];
        return;
    }
    
    [SVProgressHUD showWithStatus:@"Please wait..."];
    NSString *receiptString = [receiptData base64EncodedStringWithOptions:0];
    NSDictionary *rDict = @{@"receipt-data" : receiptString, @"password" : IAPSharedSecret};
    NSData *requestData = [NSJSONSerialization dataWithJSONObject:rDict options:0 error:&error];
    NSString *storeUrl = @"https://sandbox.itunes.apple.com/verifyReceipt";
    //NSString *storeUrl = @"https://buy.itunes.apple.com/verifyReceipt";
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:storeUrl]];
    request.HTTPMethod = @"POST";
    request.HTTPBody = requestData;
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    NSURLSessionDataTask *task =  [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError * error) {
        if (!error) {
            NSDictionary *receiptInfoDict = [NSJSONSerialization JSONObjectWithData:data options:(NSJSONReadingMutableContainers) error:&error];
            NSLog(@"%@",receiptInfoDict);
            NSDate *eDate = [self getExpirationDateFromResponse:receiptInfoDict];
            NSLog(@"%@",eDate);
        }
        [SVProgressHUD dismiss];
    }];
    
    [task resume];
}

- (NSDate *)getExpirationDateFromResponse:(NSDictionary *)dict
{
    NSArray *rInfo = [dict valueForKey:@"latest_receipt_info"];
    NSDictionary *lastReceipt = rInfo.lastObject;
    NSDateFormatter *dFormatter = [NSDateFormatter new];
    dFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss VV";
    
    NSString *dateStr = [lastReceipt valueForKey:@"expires_date"];
    NSDate *expDate = [dFormatter dateFromString:dateStr];
    return expDate;
}

@end
