//
//  SettingsFamilyViewController.m
//  Joey
//
//  Created by csl on 24/1/2017.
//  Copyright © 2017 Auxilia. All rights reserved.
//

#import "AppDelegate.h"
#import "SettingsFamilyViewController.h"
#import "SettingsFamilyNameTableViewCell.h"
#import "SettingsFamilyBabyTableViewCell.h"
#import "SettingsFamilyMemberTableViewCell.h"
#import "SettingsFamilyButtonTableViewCell.h"
#import "SettingsFamilyBabyCollectionViewCell.h"

@interface SettingsFamilyViewController ()
{
    NSUserDefaults *shareObject;
    AppDelegate *appDelegate;
    NSDateFormatter *formatter;
    NSMutableArray *familyList;
    NSMutableArray *familyNameList;
    NSMutableArray *babyList;
    NSMutableArray *memberList;
    NSMutableArray *deviceList;
    
    BOOL masterCheck;
    BOOL saveCheck;
    int familyId;
    int backupPageNo;
    int selectedFamilyIndex;
    
}
@end

@implementation SettingsFamilyViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    shareObject = [NSUserDefaults standardUserDefaults];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    formatter = [[NSDateFormatter alloc] init];
    [formatter setTimeZone:[NSTimeZone localTimeZone]];
    [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
    
    selectedFamilyIndex = 0;
    
    babyList = [[NSMutableArray alloc] init];
    memberList = [[NSMutableArray alloc] init];
    deviceList = [[NSMutableArray alloc] init];
    backupPageNo = 1;
    
    [self.navigationController setNavigationBarHidden:NO];
    [self.navigationController.navigationBar setBarTintColor:UIColorFromRGB(0xEEE8E4)];
    [self.navigationController.navigationBar setTranslucent:NO];
    [self.navigationItem setTitle:LocalizedString(@"title_settings_family", nil)];
    [self customBackButton];
    
    __weak __typeof(self)weakSelf = self;
    [self.tableView addPullToRefreshWithActionHandler:^{
        [weakSelf getFamilyList];
    }];
    
    [SVProgressHUD show];
    [self getFamilyList];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:(BOOL)animated];
    
    NSLog(@"here=%@", self.data);
    if([[self.data objectForKey:@"save_check"] boolValue])
    {
        saveCheck = [[self.data objectForKey:@"save_check"] boolValue];
        [self.data removeAllObjects];
        
        [SVProgressHUD show];
        [self getFamilyList];
    }
}


#pragma mark - WS Calls
- (void)getFamilyList
{
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setValue:@([shareObject integerForKey:@"user_id"]) forKey:@"user_id"];
    [[WebServiceManager sharedManager] initWithBaseURL:@"api/getFamilyList" parameters:parameters success:^(id result) {
        
        masterCheck = NO;
        familyList = (NSMutableArray *)[result valueForKey:@"list"];
        NSLog(@"familyList=%@", familyList);
        familyNameList = [[NSMutableArray alloc] init];
        for(int i=0; i<familyList.count; i++) [familyNameList addObject:[[familyList objectAtIndex:i] objectForKey:@"name"]];
        
        [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:familyList] forKey:@"family_list"];
        [shareObject synchronize];
        
        [self setupFamilyList];
        [self.tableView.pullToRefreshView stopAnimating];
        
    } failure:^(NSError *error) {
        NSLog(@"GetFamilyList : Failed");
    }];
}



- (void)setupFamilyList
{
    if(selectedFamilyIndex > familyList.count-1) selectedFamilyIndex--;
    
    familyId = [[[familyList objectAtIndex:selectedFamilyIndex] objectForKey:@"id"] intValue];
    babyList = [[familyList objectAtIndex:selectedFamilyIndex] objectForKey:@"baby_list"];
    memberList = [[familyList objectAtIndex:selectedFamilyIndex] objectForKey:@"member_list"];
    deviceList = [[familyList objectAtIndex:selectedFamilyIndex] objectForKey:@"device_list"];
    NSLog(@"deviceList=%@", deviceList);
    
    [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:deviceList] forKey:@"device_list"];
    [shareObject synchronize];
    
    masterCheck = NO;
    for(int i=0; i<memberList.count; i++)
    {
        if([[[memberList objectAtIndex:i] objectForKey:@"master_check"] boolValue])
        {
            if([[[memberList objectAtIndex:i] objectForKey:@"user_id"] intValue] == (int)[shareObject integerForKey:@"user_id"])
            {
                masterCheck = YES;
                break;
            }
        }
    }
    [self.tableView reloadData];
}

#pragma mark - Button action
- (void)customBackButton
{
    if([self.navigationController.viewControllers count] > 1)
    {
        UIImage *imgBack = [UIImage imageNamed:@"nav_btn_back"];
        UIButton *btnBack = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnBack setImage:imgBack forState:UIControlStateNormal];
        btnBack.frame = CGRectMake(0, 0, imgBack.size.width, imgBack.size.height);
        [btnBack addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
        [btnBack setImageEdgeInsets:UIEdgeInsetsMake(0, -16, 0, 16)];
        UIBarButtonItem *backBarItem = [[UIBarButtonItem alloc] initWithCustomView:btnBack];
        self.navigationItem.leftBarButtonItem = backBarItem;
    }
}

- (void)backAction
{
    NSLog(@"backAction");
    UIViewController *viewController = [self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count-2];
    viewController.data = [[NSMutableDictionary alloc] initWithDictionary:@{@"save_check":@(saveCheck)}];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)updateBabyView:(int)familyIndex
{
    NSLog(@"testing=%d", familyIndex);
    selectedFamilyIndex = familyIndex;
    familyId = [[[familyList objectAtIndex:selectedFamilyIndex] objectForKey:@"id"] intValue];
    NSLog(@"familyId=%d", familyId);
    
    [self setupFamilyList];
}

- (void)saveFamilyNameAction:(NSString *)familyName
{
    if(familyName.length == 0)
    {
        
    }
    else
    {
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setValue:@([shareObject integerForKey:@"user_id"]) forKey:@"user_id"];
        [parameters setValue:@(familyId) forKey:@"family_id"];
        [parameters setValue:familyName forKey:@"name"];
        
        [[WebServiceManager sharedManager] initWithBaseURL:@"api/updateFamilyName" parameters:parameters success:^(id result) {
            
            [[familyList objectAtIndex:selectedFamilyIndex] setObject:familyName forKey:@"name"];
            [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:familyList] forKey:@"family_list"];
            [shareObject synchronize];
            
        } failure:^(NSError *error) {
        }];
    }
}

- (void)editBabyAction:(UIButton *)btn
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Settings" bundle:nil];
    UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"SettingsBabyViewController"];
    
    viewController.data = [[babyList objectAtIndex:btn.tag] mutableCopy];
    NSLog(@"viewController.data=%@", viewController.data);
    [self.navigationController pushViewController:viewController animated:YES];
}

- (void)removeMemberAction:(UIButton *)btn
{
    int index = (int)btn.tag;
    
    NSLog(@"index=%d", index);
    [self showDeleteAlert:LocalizedString(@"alert_are_you_sure", nil) message:LocalizedString(@"alert_delete_member", nil) callback:^(UIAlertAction *action) {
        NSLog(@"member=%@", [memberList objectAtIndex:index]);
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        
        if([[[memberList objectAtIndex:index] objectForKey:@"user_id"] intValue] == 0)
        {
            [parameters setValue:@([[[memberList objectAtIndex:index] objectForKey:@"invite_id"] intValue]) forKey:@"id"];
            [parameters setValue:@([shareObject integerForKey:@"user_id"]) forKey:@"user_id"];
            [parameters setValue:@(familyId) forKey:@"family_id"];
            
            [[WebServiceManager sharedManager] initWithBaseURL:@"api/disinviteUser" parameters:parameters success:^(id result) {
                
                if([[result objectForKey:@"success"] boolValue])
                {
                    [self getFamilyList];
                }
                
            } failure:^(NSError *error) {
                
            }];
        }
        else
        {
            [parameters setValue:@([[[memberList objectAtIndex:index] objectForKey:@"user_id"] intValue]) forKey:@"user_id"];
            [parameters setValue:@(familyId) forKey:@"family_id"];
            
            [[WebServiceManager sharedManager] initWithBaseURL:@"api/removeFamilyMember" parameters:parameters success:^(id result) {
                
                if([[result objectForKey:@"success"] boolValue])
                {
                    [self getFamilyList];
                }
                
            } failure:^(NSError *error) {
                
            }];
        }
    }];
}

- (void)removeAction:(UIButton *)btn
{
    [self showDeleteAlert:LocalizedString(@"alert_are_you_sure", nil) message:LocalizedString(@"alert_delete_family", nil) callback:^(UIAlertAction *action) {
        
        [SVProgressHUD show];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setValue:@([shareObject integerForKey:@"user_id"]) forKey:@"user_id"];
        [parameters setValue:@(familyId) forKey:@"family_id"];
        
        [[WebServiceManager sharedManager] initWithBaseURL:@"api/removeFamily" parameters:parameters success:^(id result) {
            
            if([[result objectForKey:@"success"] boolValue])
            {
                [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableArray *)[result valueForKey:@"list"]] forKey:@"baby_list"];
                [shareObject synchronize];
                
                saveCheck = YES;
                appDelegate.updateTimelineCheck = appDelegate.updateActivitiesCheck = YES;
                
                if(familyList.count > 1)
                {
                    selectedFamilyIndex--;
                    selectedFamilyIndex = (selectedFamilyIndex < 0)?0:selectedFamilyIndex;
                    NSLog(@"selectedFamilyIndex=%d", selectedFamilyIndex);
                    [self getFamilyList];
                }
                else
                {
                    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                    UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"InvitationViewController"];
                    viewController.data = [[NSMutableDictionary alloc] initWithDictionary:@{@"settings_check":@(YES)}];
                    [self.navigationController pushViewController:viewController animated:NO];
                }
            }
            
        } failure:^(NSError *error) {
        }];
    }];
}

- (void)addMemberAction
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Settings" bundle:nil];
    UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"SettingsFamilyMemberViewController"];
    viewController.data = [[NSMutableDictionary alloc] initWithDictionary:@{@"family_info":[[familyList objectAtIndex:selectedFamilyIndex] mutableCopy]}];
    
    [self.navigationController pushViewController:viewController animated:YES];
}

- (void)addDeviceAction
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"SettingsNewDeviceViewController"];
    viewController.data = [[NSMutableDictionary alloc] initWithDictionary:@{@"family_id":[NSString stringWithFormat:@"%d", familyId]}];
    [self.navigationController pushViewController:viewController animated:YES];
}

- (void)addBabyAction
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"NewBabyViewController"];
    viewController.data = [[NSMutableDictionary alloc] initWithDictionary:@{@"settings_check":@(YES), @"family_id":@(familyId)}];
    [self.navigationController pushViewController:viewController animated:YES];
}

- (IBAction)addAction:(id)sender
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"InvitationViewController"];
    viewController.data = [[NSMutableDictionary alloc] initWithDictionary:@{@"settings_check":@(YES)}];
    [self.navigationController pushViewController:viewController animated:YES];
}


#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if(familyList.count > 0) return 4;
    return 0;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    //if(section == 0) return LocalizedString(@"header_family", nil);
    if(section == 1) return LocalizedString(@"header_babies", nil);
    else if(section == 2) return LocalizedString(@"header_members", nil);
    //else return LocalizedString(@"header_devices", nil);
    return nil;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(section == 2) return memberList.count;
    //else if(section == 3) return deviceList.count;
    //else if(section == 4)
    else if(section == 3)
    {
        if(masterCheck) return 1;
        else return 0;
    }
    else return 1;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if(section == 4) return nil;
    //if(section == 3) return nil;
    UILabel *txtHeader;
    UIView *headerView;
    UIButton *btnAdd;
    
    if (IS_IPAD) {
        txtHeader = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, tableView.bounds.size.width-130, 60)];
        txtHeader.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:26.0f];
        headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 60)];
    }
    else{
        txtHeader = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, tableView.bounds.size.width-130, 40)];
        txtHeader.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:15.0f];
        headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 40)];
    }
    
    txtHeader.textColor = [UIColor whiteColor];
    txtHeader.text = [self tableView:tableView titleForHeaderInSection:section];
    
    headerView.backgroundColor = UIColorFromRGB(0xBFB5AE);
    [headerView addSubview:txtHeader];
    
    if(section != 0)
    {
        if (IS_IPAD) {
            btnAdd = [[UIButton alloc] initWithFrame:CGRectMake(self.tableView.bounds.size.width-64-8, -2, 64, 64)];
        }
        else{
            btnAdd = [[UIButton alloc] initWithFrame:CGRectMake(self.tableView.bounds.size.width-44-8, -2, 44, 44)];
        }
        btnAdd.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        [btnAdd setImage:[UIImage imageNamed:@"nav_btn_add.png"] forState:UIControlStateNormal];
        
        if(section == 1){
            [btnAdd addTarget:self action:@selector(addBabyAction) forControlEvents:UIControlEventTouchUpInside];
        }else if(section == 2){
            [btnAdd addTarget:self action:@selector(addMemberAction) forControlEvents:UIControlEventTouchUpInside];
        }
//        else{
//            [btnAdd addTarget:self action:@selector(addDeviceAction) forControlEvents:UIControlEventTouchUpInside];
//        }
        [headerView addSubview:btnAdd];
        btnAdd.hidden = !masterCheck;
    }
    
//    if(IS_IPAD){
//        btnAdd.transform = CGAffineTransformMakeScale(1.2, 1.2);
//    }
    
    return headerView;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (IS_IPAD) {
        if (section == 1) return 60;
        if (section == 2) return 60;
    }
    //if(section == 4) return 0;
    if(section == 1) return 40;
    if(section == 2) return 40;
    return 0;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (IS_IPAD) {
        if (indexPath.section == 1) {
            return 170;
        }
        if(indexPath.section == 3)
        {
            if((50+170+(memberList.count*90)+(2*60))+94 < self.tableView.frame.size.height)
            {
                return self.tableView.frame.size.height-(50+170+(memberList.count*90)+(2*60));
            }
            else return 94;
        }
    }
    
    if (indexPath.section == 1) {
        return 130;
    }
    if(indexPath.section == 3)
    {
        if((50+130+(memberList.count*70)+(2*40))+74 < self.tableView.frame.size.height)
        {
            return self.tableView.frame.size.height-(50+130+(memberList.count*70)+(2*40));
        }
        else return 74;
    }
    
    /*if(indexPath.section == 4)
     {
     if(deviceList.count == 0)
     {
     if((50+140+(memberList.count*70)+(4*40))+74 < self.tableView.frame.size.height)
     {
     return self.tableView.frame.size.height-(50+140+(memberList.count*70)+(4*40));
     }
     else return 74;
     }
     else return 74;
     }*/
    
    return UITableViewAutomaticDimension;
}
- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (IS_IPAD) {
        if (indexPath.section == 1) {
            return 170;
        }
        if(indexPath.section == 3)
        {
            if((50+170+(memberList.count*90)+(2*60))+94 < self.tableView.frame.size.height)
            {
                return self.tableView.frame.size.height-(50+170+(memberList.count*90)+(2*60));
            }
            else return 94;
        }
    }
    if (indexPath.section == 1) {
        return 130;
    }
    if(indexPath.section == 3)
    {
        if((50+130+(memberList.count*70)+(2*40))+74 < self.tableView.frame.size.height)
        {
            return self.tableView.frame.size.height-(50+130+(memberList.count*70)+(2*40));
        }
        else return 74;
    }
    /*
     if(indexPath.section == 3)
     {
     if(deviceList.count == 0)
     {
     if((50+140+(memberList.count*70)+(4*40))+74 < self.tableView.frame.size.height)
     {
     return self.tableView.frame.size.height-(50+140+(memberList.count*70)+(4*40));
     }
     else return 74;
     }
     else return 74;
     }*/
    return UITableViewAutomaticDimension;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 0)
    {
        SettingsFamilyNameTableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
        
        NSString *familyName = cell.lblFamilyName.text;
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:LocalizedString(@"txt_edit_family_name", nil) preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *btnOk = [UIAlertAction actionWithTitle:LocalizedString(@"btn_ok", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            if(alert.textFields[0].text.length > 0)
            {
                NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
                [parameters setValue:@([shareObject integerForKey:@"user_id"]) forKey:@"user_id"];
                [parameters setValue:@(familyId) forKey:@"family_id"];
                [parameters setValue:alert.textFields[0].text forKey:@"name"];
                
                [[WebServiceManager sharedManager] initWithBaseURL:@"api/updateFamilyName" parameters:parameters success:^(id result) {
                    
                    cell.lblFamilyName.text = alert.textFields[0].text;
                    [[familyList objectAtIndex:selectedFamilyIndex] setObject:alert.textFields[0].text forKey:@"name"];
                    [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:familyList] forKey:@"family_list"];
                    [shareObject synchronize];
                    
                } failure:^(NSError *error) {
                }];
                
                [alert dismissViewControllerAnimated:YES completion:nil];
            }
            else
            {
                [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:LocalizedString(@"error_family_name", nil) input:nil];
            }
        }];
        
        UIAlertAction *btnCancel = [UIAlertAction actionWithTitle:LocalizedString(@"btn_cancel", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            
            [alert dismissViewControllerAnimated:YES completion:nil];
        }];
        
        [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
            textField.text = familyName;
            textField.autocapitalizationType = UITextAutocapitalizationTypeWords;
        }];
        
        [alert addAction:btnCancel];
        [alert addAction:btnOk];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
    /*else if(indexPath.section == 3)
     {
     UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
     BOOL isWearable = [[[deviceList objectAtIndex:indexPath.row] objectForKey:@"wearable_check"] boolValue];
     
     if(isWearable)
     {
     UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"SettingsWearableViewController"];
     viewController.data = [[NSMutableDictionary alloc] initWithDictionary:@{@"device_info":[deviceList objectAtIndex:indexPath.row]}];
     [self.navigationController pushViewController:viewController animated:YES];
     }
     else
     {
     UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"SettingsBaseStationViewController"];
     viewController.data = [[NSMutableDictionary alloc] initWithDictionary:@{@"device_info":[deviceList objectAtIndex:indexPath.row]}];
     [self.navigationController pushViewController:viewController animated:YES];
     }
     }*/
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 0)
    {
        SettingsFamilyNameTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SettingsFamilyNameTableViewCell"];
        if(!cell)
        {
            [tableView registerNib:[UINib nibWithNibName:@"SettingsFamilyNameTableViewCell" bundle:nil] forCellReuseIdentifier:@"SettingsFamilyNameTableViewCell"];
            cell = [tableView dequeueReusableCellWithIdentifier:@"SettingsFamilyNameTableViewCell"];
        }
        
        if (IS_IPAD) {
            cell.lblFamilyName.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        }else {
            cell.lblFamilyName.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:16.0f];
        }
        
        cell.lblFamilyName.text = [[familyList objectAtIndex:selectedFamilyIndex] objectForKey:@"name"];
        
//        if (selectedFamilyIndex == 0) {
//            cell.rightImage.hidden = false;
//            cell.leftImage.hidden = true;
//        }
//        else if (selectedFamilyIndex == familyList.count - 1 ){
//            cell.rightImage.hidden = true;
//            cell.leftImage.hidden = false;
//        }
//        else{
//            cell.rightImage.hidden = false;
//            cell.leftImage.hidden = false;
//        }
        
        cell.leftImage.hidden = true;
        cell.rightImage.hidden = true;
        
        if(familyList.count > 1)
        {
            cell.leftImage.hidden = false;
            cell.rightImage.hidden = false;

            UISwipeGestureRecognizer *gestureRecognizer1 = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(leftSwipeHandler:)];
            gestureRecognizer1.direction = UISwipeGestureRecognizerDirectionLeft;
            [cell addGestureRecognizer:gestureRecognizer1];
            
            UISwipeGestureRecognizer *gestureRecognizer2 = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(rightSwipeHandler:)];
            gestureRecognizer2.direction = UISwipeGestureRecognizerDirectionRight;
            [cell addGestureRecognizer:gestureRecognizer2];
        }
        return cell;
    }
    else if(indexPath.section == 1)
    {
        SettingsFamilyBabyTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SettingsFamilyBabyTableViewCell"];
        if(!cell)
        {
            [tableView registerNib:[UINib nibWithNibName:@"SettingsFamilyBabyTableViewCell" bundle:nil] forCellReuseIdentifier:@"SettingsFamilyBabyTableViewCell"];
            cell = [tableView dequeueReusableCellWithIdentifier:@"SettingsFamilyBabyTableViewCell"];
        }
        
        if (IS_IPAD) {
            if (babyList.count>=6) {
                cell.rightArrow.hidden = NO;
            }else {
                cell.rightArrow.hidden = YES;
            }
        }else {
            if (babyList.count>=4) {
                cell.rightArrow.hidden = NO;
            }else {
                cell.rightArrow.hidden = YES;
            }
        }
        cell.familyCollectionView.delegate = self;
        cell.familyCollectionView.dataSource = self;
        [cell.familyCollectionView registerNib:[UINib nibWithNibName:@"SettingsFamilyBabyCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"SettingsFamilyBabyCollectionViewCell"];
        [cell.familyCollectionView reloadData];
        
        
//        cell.viewController = self;
////        cell.familyScrollView.contentSize = CGSizeMake(self.view.frame.size.width*3, 1);
////        cell.familyScrollView.contentOffset = CGPointMake(self.view.frame.size.width, cell.familyScrollView.contentOffset.y);
////
////        for(UIView *v in [cell.scrollView1 subviews]) [v removeFromSuperview];
////        for(UIView *v in [cell.scrollView2 subviews]) [v removeFromSuperview];
////        for(UIView *v in [cell.scrollView3 subviews]) [v removeFromSuperview];
//
//        if(familyList.count == 1)
//        {
//            //cell.familyScrollView.scrollEnabled = NO;
//            familyId = [[[familyList objectAtIndex:0] objectForKey:@"id"] intValue];
//
//            NSMutableArray *babyList1 = [[familyList objectAtIndex:0] objectForKey:@"baby_list"];
//
//            NSLog(@"babyList=%@", babyList1);
//            for(int i=0; i<babyList1.count; i++)
//            {
//                UIImageView *imgBaby = [[UIImageView alloc] initWithFrame:CGRectMake(10+(i*90), 10, 80, 80)];
//                imgBaby.backgroundColor = UIColorFromRGB(0xE0D3CB);
//                imgBaby.image = [UIImage imageNamed:@"dummy"];
//                imgBaby.clipsToBounds = YES;
//                imgBaby.layer.cornerRadius = imgBaby.frame.size.width/2;
//                imgBaby.contentMode = UIViewContentModeScaleAspectFill;
//
//                NSString *photoUrl = [[babyList1 objectAtIndex:i] objectForKey:@"photo_url"];
//                if(![photoUrl isEqual:[NSNull null]])
//                {
//                    [imgBaby sd_setImageWithURL:[NSURL URLWithString:photoUrl] placeholderImage:[UIImage imageNamed:@"dummy"]];
//                }
//                //[cell.scrollView2 addSubview:imgBaby];
//
//                UILabel *txtBabyName = [[UILabel alloc] initWithFrame:CGRectMake(10+(i*90), 100, 80, 20)];
//                txtBabyName.text = [[babyList1 objectAtIndex:i] objectForKey:@"name"];
//                txtBabyName.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:13.0f];
//                txtBabyName.textColor = UIColorFromRGB(0x8D8082);
//                txtBabyName.textAlignment = NSTextAlignmentCenter;
//                txtBabyName.numberOfLines = 0;
//                [txtBabyName sizeToFit];
//                //txtBabyName.backgroundColor = [[UIColor redColor] colorWithAlphaComponent:0.2];
//                txtBabyName.frame = CGRectMake(txtBabyName.frame.origin.x, txtBabyName.frame.origin.y+(20-txtBabyName.frame.size.height)/2, imgBaby.frame.size.width, txtBabyName.frame.size.height);
//                //[cell.scrollView2 addSubview:txtBabyName];
//
//                UIButton *btnBaby = [[UIButton alloc] initWithFrame:CGRectMake(10+(i*90), 10, 80, 140)];
//                btnBaby.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
//                [btnBaby setTag:i];
//                [btnBaby addTarget:self action:@selector(editBabyAction:) forControlEvents:UIControlEventTouchUpInside];
//                //[cell.scrollView2 addSubview:btnBaby];
//
//                //cell.scrollView2.contentSize = CGSizeMake(10+((imgBaby.frame.size.width+10)*babyList1.count), 1);
//            }
//        }
//        else
//        {
//            int prevIndex = selectedFamilyIndex-1;
//            prevIndex = (prevIndex < 0)?(int)(familyList.count-1):prevIndex;
//            int nextIndex = selectedFamilyIndex+1;
//            nextIndex = (nextIndex >= familyList.count)?0:nextIndex;
//            NSLog(@"prevIndex=%d, selectedFamilyIndex=%d, nextIndex=%d", prevIndex, selectedFamilyIndex, nextIndex);
//
//            //cell.familyScrollView.scrollEnabled = YES;
//            familyId = [[[familyList objectAtIndex:selectedFamilyIndex] objectForKey:@"id"] intValue];
//
//            NSMutableArray *babyList1 = [[familyList objectAtIndex:prevIndex] objectForKey:@"baby_list"];
//            NSLog(@"babyList1=%@", babyList1);
//            for(int i=0; i<babyList1.count; i++)
//            {
//                UIImageView *imgBaby = [[UIImageView alloc] initWithFrame:CGRectMake(10+(i*90), 10, 80, 80)];
//                imgBaby.backgroundColor = UIColorFromRGB(0xE0D3CB);
//                imgBaby.image = [UIImage imageNamed:@"dummy"];
//                imgBaby.clipsToBounds = YES;
//                imgBaby.layer.cornerRadius = imgBaby.frame.size.width/2;
//                imgBaby.contentMode = UIViewContentModeScaleAspectFill;
//
//                NSString *photoUrl = [[babyList1 objectAtIndex:i] objectForKey:@"photo_url"];
//                if(![photoUrl isEqual:[NSNull null]])
//                {
//                    [imgBaby sd_setImageWithURL:[NSURL URLWithString:photoUrl] placeholderImage:[UIImage imageNamed:@"dummy"]];
//                }
//                //[cell.scrollView1 addSubview:imgBaby];
//
//                UILabel *txtBabyName = [[UILabel alloc] initWithFrame:CGRectMake(10+(i*90), 100, 80, 20)];
//                txtBabyName.text = [[babyList1 objectAtIndex:i] objectForKey:@"name"];
//                txtBabyName.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:13.0f];
//                txtBabyName.textColor = UIColorFromRGB(0x8D8082);
//                txtBabyName.textAlignment = NSTextAlignmentCenter;
//                txtBabyName.numberOfLines = 0;
//                [txtBabyName sizeToFit];
//                //txtBabyName.backgroundColor = [[UIColor redColor] colorWithAlphaComponent:0.2];
//                txtBabyName.frame = CGRectMake(txtBabyName.frame.origin.x, txtBabyName.frame.origin.y+(20-txtBabyName.frame.size.height)/2, imgBaby.frame.size.width, txtBabyName.frame.size.height);
//                //[cell.scrollView1 addSubview:txtBabyName];
//
//                //cell.scrollView1.contentSize = CGSizeMake(10+((imgBaby.frame.size.width+10)*babyList1.count), 1);
//            }
//
//            NSMutableArray *babyList2 = [[familyList objectAtIndex:selectedFamilyIndex] objectForKey:@"baby_list"];
//            for(int i=0; i<babyList2.count; i++)
//            {
//                UIImageView *imgBaby = [[UIImageView alloc] initWithFrame:CGRectMake(10+(i*90), 10, 80, 80)];
//                imgBaby.tag = i;
//                imgBaby.backgroundColor = UIColorFromRGB(0xE0D3CB);
//                imgBaby.image = [UIImage imageNamed:@"dummy"];
//                imgBaby.clipsToBounds = YES;
//                imgBaby.layer.cornerRadius = imgBaby.frame.size.width/2;
//                imgBaby.contentMode = UIViewContentModeScaleAspectFill;
//
//                NSString *photoUrl = [[babyList2 objectAtIndex:i] objectForKey:@"photo_url"];
//                if(![photoUrl isEqual:[NSNull null]])
//                {
//                    [imgBaby sd_setImageWithURL:[NSURL URLWithString:photoUrl] placeholderImage:[UIImage imageNamed:@"dummy"]];
//                }
//                //[cell.scrollView2 addSubview:imgBaby];
//
//                UILabel *txtBabyName = [[UILabel alloc] initWithFrame:CGRectMake(10+(i*90), 100, 80, 20)];
//                txtBabyName.text = [[babyList2 objectAtIndex:i] objectForKey:@"name"];
//                txtBabyName.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:13.0f];
//                txtBabyName.textColor = UIColorFromRGB(0x8D8082);
//                txtBabyName.textAlignment = NSTextAlignmentCenter;
//                txtBabyName.numberOfLines = 0;
//                [txtBabyName sizeToFit];
//                //txtBabyName.backgroundColor = [[UIColor redColor] colorWithAlphaComponent:0.2];
//                txtBabyName.frame = CGRectMake(txtBabyName.frame.origin.x, txtBabyName.frame.origin.y+(20-txtBabyName.frame.size.height)/2, imgBaby.frame.size.width, txtBabyName.frame.size.height);
//                //[cell.scrollView2 addSubview:txtBabyName];
//
//                UIButton *btnBaby = [[UIButton alloc] initWithFrame:CGRectMake(10+(i*90), 10, 80, 140)];
//                btnBaby.tag = i;
//                [btnBaby addTarget:self action:@selector(editBabyAction:) forControlEvents:UIControlEventTouchUpInside];
//                //[cell.scrollView2 addSubview:btnBaby];
//
//                //cell.scrollView2.contentSize = CGSizeMake(10+((imgBaby.frame.size.width+10)*babyList2.count), 1);
//            }
//
//            NSMutableArray *babyList3 = [[familyList objectAtIndex:nextIndex] objectForKey:@"baby_list"];
//            for(int i=0; i<babyList3.count; i++)
//            {
//                UIImageView *imgBaby = [[UIImageView alloc] initWithFrame:CGRectMake(10+(i*90), 10, 80, 80)];
//                imgBaby.backgroundColor = UIColorFromRGB(0xE0D3CB);
//                imgBaby.image = [UIImage imageNamed:@"dummy"];
//                imgBaby.clipsToBounds = YES;
//                imgBaby.layer.cornerRadius = imgBaby.frame.size.width/2;
//                imgBaby.contentMode = UIViewContentModeScaleAspectFill;
//
//                NSString *photoUrl = [[babyList3 objectAtIndex:i] objectForKey:@"photo_url"];
//                if(![photoUrl isEqual:[NSNull null]])
//                {
//                    [imgBaby sd_setImageWithURL:[NSURL URLWithString:photoUrl] placeholderImage:[UIImage imageNamed:@"dummy"]];
//                }
//
//                //[cell.scrollView3 addSubview:imgBaby];
//
//                UILabel *txtBabyName = [[UILabel alloc] initWithFrame:CGRectMake(10+(i*90), 100, 80, 20)];
//                txtBabyName.text = [[babyList3 objectAtIndex:i] objectForKey:@"name"];
//                txtBabyName.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:13.0f];
//                txtBabyName.textColor = UIColorFromRGB(0x8D8082);
//                txtBabyName.textAlignment = NSTextAlignmentCenter;
//                txtBabyName.numberOfLines = 0;
//                [txtBabyName sizeToFit];
//                //txtBabyName.backgroundColor = [[UIColor redColor] colorWithAlphaComponent:0.2];
//                txtBabyName.frame = CGRectMake(txtBabyName.frame.origin.x, txtBabyName.frame.origin.y+(20-txtBabyName.frame.size.height)/2, imgBaby.frame.size.width, txtBabyName.frame.size.height);
//                //[cell.scrollView3 addSubview:txtBabyName];
//
//                //kcell.scrollView3.contentSize = CGSizeMake(10+((imgBaby.frame.size.width+10)*babyList3.count), 1);
//            }
//        }
        
        return cell;
    }
    else if(indexPath.section == 2)
    {
        SettingsFamilyMemberTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SettingsFamilyMemberTableViewCell"];
        if(!cell)
        {
            [tableView registerNib:[UINib nibWithNibName:@"SettingsFamilyMemberTableViewCell" bundle:nil] forCellReuseIdentifier:@"SettingsFamilyMemberTableViewCell"];
            cell = [tableView dequeueReusableCellWithIdentifier:@"SettingsFamilyMemberTableViewCell"];
        }
        
        if (IS_IPAD) {
            cell.txtName.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
            cell.txtValue.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:23.0f];
            cell.btnDelete.transform = CGAffineTransformMakeScale(1.2, 1.2);
        }
        else{
            cell.txtName.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:16.0f];
            cell.txtValue.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:15.0f];
        }
        cell.txtName.text = [[memberList objectAtIndex:indexPath.row] objectForKey:@"email"];
        
        if([[[memberList objectAtIndex:indexPath.row] objectForKey:@"master_check"] boolValue])
        {
            cell.txtValue.text = [NSString stringWithFormat:@"%@ - %@", LocalizedString([[memberList objectAtIndex:indexPath.row] objectForKey:@"relationship"], nil), LocalizedString(@"master", nil)];
        }
        else
        {
            if([[[memberList objectAtIndex:indexPath.row] objectForKey:@"invite_check"] boolValue])
            {
                formatter.dateFormat = @"yyyy-MM-dd";
                NSDate *inviteDate = [formatter dateFromString:[[memberList objectAtIndex:indexPath.row] objectForKey:@"invite_date"]];
                formatter.dateFormat = @"d MMM yyyy";
                NSString *inviteTimeString = [NSString stringWithFormat:@"%@ %@", LocalizedString(@"txt_invited", nil), [formatter stringFromDate:inviteDate]];
                
                cell.txtValue.text = [NSString stringWithFormat:@"%@ - %@", LocalizedString([[memberList objectAtIndex:indexPath.row] objectForKey:@"relationship"], nil), inviteTimeString];
            }
            else
            {
                cell.txtValue.text = [NSString stringWithFormat:@"%@ - %@", LocalizedString([[memberList objectAtIndex:indexPath.row] objectForKey:@"relationship"], nil), LocalizedString(@"txt_accepted", nil)];
            }
        }
        
        if(masterCheck)
        {
            cell.btnDelete.hidden = [[[memberList objectAtIndex:indexPath.row] objectForKey:@"master_check"] boolValue];
        }
        else cell.btnDelete.hidden = YES;
        
        cell.btnDelete.tag = indexPath.row;
        [cell.btnDelete addTarget:self action:@selector(removeMemberAction:) forControlEvents:UIControlEventTouchUpInside];
        
        return cell;
    }
    /*else if(indexPath.section == 3)
     {
     SettingsDevicesTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"SettingsDevicesTableViewCell"];
     if(!cell)
     {
     [self.tableView registerNib:[UINib nibWithNibName:@"SettingsDevicesTableViewCell" bundle:nil] forCellReuseIdentifier:@"SettingsDevicesTableViewCell"];
     cell = [self.tableView dequeueReusableCellWithIdentifier:@"SettingsDevicesTableViewCell"];
     }
     
     formatter.dateFormat = @"yyyy-MM-dd";
     NSDate *today = [NSDate date];
     NSString *syncedDate = [[deviceList objectAtIndex:indexPath.row] objectForKey:@"date"];
     if([[formatter stringFromDate:today] isEqualToString:syncedDate])
     {
     syncedDate = [NSString stringWithFormat:@"%@, %@", LocalizedString(@"txt_today", nil), [[deviceList objectAtIndex:indexPath.row] objectForKey:@"time"]];
     }
     else
     {
     syncedDate = [NSString stringWithFormat:@"%@ %@", syncedDate, [[deviceList objectAtIndex:indexPath.row] objectForKey:@"time"]];
     }
     [[deviceList objectAtIndex:indexPath.row] setObject:syncedDate forKey:@"synced_at"];
     
     BOOL isWearable = [[[deviceList objectAtIndex:indexPath.row] objectForKey:@"wearable_check"] boolValue];
     if(isWearable)
     {
     cell.imgPhoto.image = [UIImage imageNamed:@"icon_wearable"];
     cell.txtName.text = [[deviceList objectAtIndex:indexPath.row] objectForKey:@"nickname"];
     cell.txtInfo.text = [NSString stringWithFormat:LocalizedString(@"txt_wearable_assigned_to", nil), [[deviceList objectAtIndex:indexPath.row] objectForKey:@"baby_name"]];
     }
     else
     {
     NSLog(@"device=%@", [deviceList objectAtIndex:indexPath.row]);
     cell.imgPhoto.image = [UIImage imageNamed:@"icon_base_station"];
     cell.txtName.text = [[deviceList objectAtIndex:indexPath.row] objectForKey:@"nickname"];
     cell.txtInfo.text = [NSString stringWithFormat:LocalizedString(@"txt_base_station_assigned_to", nil), [[deviceList objectAtIndex:indexPath.row] objectForKey:@"family_name"]];
     }
     
     return cell;
     }*/
    else
    {
        SettingsFamilyButtonTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"SettingsFamilyButtonTableViewCell"];
        if(!cell)
        {
            [self.tableView registerNib:[UINib nibWithNibName:@"SettingsFamilyButtonTableViewCell" bundle:nil] forCellReuseIdentifier:@"SettingsFamilyButtonTableViewCell"];
            cell = [self.tableView dequeueReusableCellWithIdentifier:@"SettingsFamilyButtonTableViewCell"];
        }
        
        if (IS_IPAD) {
            cell.btnDelete.titleLabel.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
            cell.btnDeleteHeightConstraint.constant = 64;
        }
        [cell.btnDelete addTarget:self action:@selector(removeAction:) forControlEvents:UIControlEventTouchUpInside];
        
        return cell;
    }
}

#pragma mark - Swipe Handler Methods

-(void)rightSwipeHandler: (UISwipeGestureRecognizer *)gesture
{
    NSLog(@"%lu" , (unsigned long)gesture.direction);
    
    if (selectedFamilyIndex != 0) {
        selectedFamilyIndex -= 1;
    }else if (selectedFamilyIndex == 0){
        selectedFamilyIndex = familyList.count - 1;
    }
    
    [self setupFamilyList];
}

-(void)leftSwipeHandler: (UISwipeGestureRecognizer *)gesture
{
    NSLog(@"%lu" , (unsigned long)gesture.direction);
    
    if (selectedFamilyIndex < familyList.count - 1 ) {
        selectedFamilyIndex += 1;
    }else if (selectedFamilyIndex == familyList.count-1){
        selectedFamilyIndex = 0;
    }
    
    [self setupFamilyList];
    
}

#pragma mark - Collection view Datasource and Delegate methods

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    SettingsFamilyBabyCollectionViewCell  *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"SettingsFamilyBabyCollectionViewCell" forIndexPath:indexPath];
    
    NSString *photoUrl = [[babyList objectAtIndex:indexPath.row] objectForKey:@"photo_url"];
    if(![photoUrl isEqual:[NSNull null]]){
        [cell.imgBaby sd_setImageWithURL:[NSURL URLWithString:photoUrl] placeholderImage:[UIImage imageNamed:@"dummy"]];
    }
    
    if (IS_IPAD) {
        cell.txtBabyName.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        cell.imgBabyHeightConstraint.constant = 90;
        cell.imgBabyWidthConstraint.constant = 90;
    }else{
        cell.txtBabyName.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:18.0f];
    }
    cell.imgBaby.layer.cornerRadius = cell.imgBabyWidthConstraint.constant/2;
    cell.imgBaby.layer.masksToBounds = YES;
    [cell layoutSubviews];
    
    cell.txtBabyName.text = [[babyList objectAtIndex:indexPath.row] objectForKey:@"name"];
    
    return cell;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return babyList.count;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Settings" bundle:nil];
    UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"SettingsBabyViewController"];
    
    viewController.data = [[babyList objectAtIndex:indexPath.row] mutableCopy];
    NSLog(@"viewController.data=%@", viewController.data);
    [self.navigationController pushViewController:viewController animated:YES];
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (IS_IPAD) {
        return CGSizeMake(130, 170);
    }
    return CGSizeMake(100, 140);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(0, 10, 0, 10);
}

@end
