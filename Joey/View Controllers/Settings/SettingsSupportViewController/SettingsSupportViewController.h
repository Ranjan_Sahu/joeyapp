//
//  SettingsSupportViewController.h
//  Joey
//
//  Created by Aishwarya Rai on 07/12/17.
//  Copyright © 2017 Auxilia. All rights reserved.
//

#import "BaseViewController.h"
#import <Photos/Photos.h>
#import <AVFoundation/AVFoundation.h>

@interface SettingsSupportViewController : BaseViewController<UINavigationControllerDelegate, UIImagePickerControllerDelegate>

@property (nonatomic, weak) IBOutlet UIBarButtonItem *btnSave;
@property (nonatomic, weak) IBOutlet UILabel *lblSupportCategory;

//@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lblSupportCategoryHeightConstraint;
//@property (weak, nonatomic) IBOutlet NSLayoutConstraint *btnSupposrtCategoryHeightConstraint;
//@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lblRequestHeightConstraint;
//@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lblAttachmentsHeightConstraints;
//@property (weak, nonatomic) IBOutlet NSLayoutConstraint *txtRequestHeightContraitns;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *btnSubmitHeightContraints;

@property (nonatomic, weak) IBOutlet UILabel *lblRequest;
@property (nonatomic, weak) IBOutlet UITextView *txtViewRequest;
@property (nonatomic, weak) IBOutlet UILabel *lblAttachments;

@property (nonatomic, weak) IBOutlet UILabel *lblPhoto;
@property (nonatomic, weak) IBOutlet UIButton *btnTakePhoto;
@property (nonatomic, weak) IBOutlet UIButton *btnPhotoAlbum;
@property (nonatomic, weak) IBOutlet UIButton *btnPhoto;

@property (weak, nonatomic) IBOutlet UIButton *btnSubmitRequest;
- (IBAction)submitButtonAction:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *btnSelectCategory;
- (IBAction)selectCategoryAction:(id)sender;

@end
