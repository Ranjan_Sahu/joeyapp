//
//  SettingsSupportViewController.m
//  Joey
//
//  Created by Aishwarya Rai on 07/12/17.
//  Copyright © 2017 Auxilia. All rights reserved.
//
#import "AppDelegate.h"
#import "SettingsSupportViewController.h"

@interface SettingsSupportViewController ()<PECropViewControllerDelegate>{
    NSUserDefaults *shareObject;
    AppDelegate *appDelegate;
    NSMutableDictionary *unitInfo;
    
    NSMutableArray *categoryListArray;
    NSMutableArray *categoryArray;
    int selectedCategoryIndex;
    NSString *selectedCategory;
    
    UIImageView *largeImageView;
    UIView *zoomView;
    
    //UIImage *imgAttachmentCropped;
    UIImage *imgAttachmentOriginal;
}

@property (nonatomic) UIPopoverController *popover;

@end

@implementation SettingsSupportViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    shareObject = [NSUserDefaults standardUserDefaults];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    unitInfo = (NSMutableDictionary *)[NSKeyedUnarchiver unarchiveObjectWithData:[shareObject objectForKey:@"unit_info"]];
    
//    categoryListArray = [[NSMutableArray alloc] initWithObjects:@"Settings – Accounts/Profiles", @"Settings – Other", @"Activities", @"Milestones", @"Timeline", @"@Home", @"Others", nil];
    
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:UIColorFromRGB(0x8D8082), NSForegroundColorAttributeName, [UIFont fontWithName:@"SFUIDisplay-Medium" size:18.0f], NSFontAttributeName, nil]];
    [self.navigationController.navigationBar setBarTintColor:UIColorFromRGB(0xEEE8E4)];
    [self.navigationController.navigationBar setTranslucent:NO];
    
    self.btnTakePhoto.hidden = self.btnPhotoAlbum.hidden = NO;
    self.btnPhoto.hidden = YES;
    self.btnPhoto.imageView.contentMode = UIViewContentModeScaleAspectFill;
    
    if(IS_IPAD){
        self.lblSupportCategory.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.lblRequest.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.lblAttachments.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.txtViewRequest.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.btnSelectCategory.titleLabel.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:22.0f];
        self.btnSubmitRequest.titleLabel.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        
//        self.lblSupportCategoryHeightConstraint.constant = 48;
//        self.btnSupposrtCategoryHeightConstraint.constant = 48;
//        self.lblRequestHeightConstraint.constant = 48;
//        self.txtRequestHeightContraitns.constant = 48;
//        self.lblAttachmentsHeightConstraints.constant = 48;
        self.btnSubmitHeightContraints.constant = 60;
    }
    
    // Textview
//    self.txtViewRequest.translatesAutoresizingMaskIntoConstraints = true;
//    [self.txtViewRequest sizeToFit];
//    [self.txtViewRequest setScrollEnabled:NO];
    self.txtViewRequest.text = @"";
    
    [self getCategoryList];
    [self.navigationItem setTitle:LocalizedString(@"title_settings_support", nil)];
    [self customBackButton];
    self.btnSave.title = @"";
}

- (void)customBackButton
{
    if([self.navigationController.viewControllers count] > 1)
    {
        UIImage *imgBack = [UIImage imageNamed:@"nav_btn_back"];
        UIButton *btnBack = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnBack setImage:imgBack forState:UIControlStateNormal];
        btnBack.frame = CGRectMake(0, 0, imgBack.size.width, imgBack.size.height);
        [btnBack addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
        [btnBack setImageEdgeInsets:UIEdgeInsetsMake(0, -16, 0, 16)];
        UIBarButtonItem *backBarItem = [[UIBarButtonItem alloc] initWithCustomView:btnBack];
        self.navigationItem.leftBarButtonItem = backBarItem;
    }
}

- (void)backAction
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:(BOOL)animated];
    if([[self.data objectForKey:@"deleteCheck"] boolValue])
    {
        [self.data removeObjectForKey:@"deleteCheck"];
        self.btnTakePhoto.hidden = self.btnPhotoAlbum.hidden = NO;
        self.btnPhoto.hidden = YES;
    }
}


- (void)showCameraAction
{
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    [self presentViewController:imagePicker animated:NO completion:nil];
}

- (void)showPhotoAlbumAction
{
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:imagePicker animated:NO completion:nil];
}

- (IBAction)showPhotoAction:(id)sender
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"PhotoViewController"];
    viewController.data = [[NSMutableDictionary alloc] initWithDictionary:@{@"viewController":self, @"image":self.btnPhoto.imageView.image}];
    [self presentViewController:viewController animated:YES completion:nil];
}

- (IBAction)takePhotoAction:(id)sender
{
    AVAuthorizationStatus status = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    if(status == AVAuthorizationStatusDenied || status == AVAuthorizationStatusRestricted)
    {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:LocalizedString(@"error_camera", nil) message:@"" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *btnOk = [UIAlertAction actionWithTitle:LocalizedString(@"btn_ok", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            
            [alert dismissViewControllerAnimated:YES completion:nil];
        }];
        
        [alert addAction:btnOk];
        
        [self presentViewController:alert animated:YES completion:nil];
        
    }
    else
    {
        [self showCameraAction];
    }
}

- (IBAction)getPhotoAlbumAction:(id)sender
{
    PHAuthorizationStatus status = [PHPhotoLibrary authorizationStatus];
    if(status == PHAuthorizationStatusDenied || status == PHAuthorizationStatusRestricted)
    {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:LocalizedString(@"error_photo", nil) message:@"" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *btnOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [alert dismissViewControllerAnimated:YES completion:nil];
        }];
        
        [alert addAction:btnOk];
        [self presentViewController:alert animated:YES completion:nil];
    }
    else{
        [self showPhotoAlbumAction];
    }
}


- (IBAction)submitButtonAction:(id)sender {
    
    if ([self validateInputs])
    {
        [SVProgressHUD show];
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setValue:@([shareObject integerForKey:@"user_id"]) forKey:@"user_id"];
        [parameters setValue:selectedCategory forKey:@"category"];
        [parameters setValue:self.txtViewRequest.text forKey:@"body"];
        
        if(imgAttachmentOriginal)
        {
            //[parameters setValue:[self.data objectForKey:@"photo_url"] forKey:@"photo_url"];
            [[WebServiceManager sharedManager] initWithBaseURL:@"api/support" parameters:parameters formData:^(id<AFMultipartFormData> formData) {
               //[formData appendPartWithFileData:UIImageJPEGRepresentation(imgAttachmentCropped, 1.0f) name:@"attachment" fileName:@"image.jpg" mimeType:@"image/jpeg"];
                [formData appendPartWithFileData:UIImageJPEGRepresentation(imgAttachmentOriginal, 0.8f) name:@"attachment" fileName:@"image.jpg" mimeType:@"image/jpeg"];
            } success:^(id result) {
                
                [SVProgressHUD dismiss];
                
                if([[result objectForKey:@"success"] boolValue]){
                    [self showAlertWithTitle:@"Support Request" message:@"Support request has been submitted." input:nil];
                    [self backAction];
                }
                else{
                    [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:[result objectForKey:@"message"] input:nil];
                }
                
            } failure:^(NSError *error) {
                [SVProgressHUD dismiss];
                [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:LocalizedString(@"error_unknown", nil) input:nil];
            }];
        }
        else
        {
            [[WebServiceManager sharedManager] initWithBaseURL:@"api/support" parameters:parameters success:^(id result) {
                
                [SVProgressHUD dismiss];
                
                if([[result objectForKey:@"success"] boolValue]){
                    [self showAlertWithTitle:@"Support Request" message:@"Support request has been submitted." input:nil];
                    [self backAction];
                }
                else{
                    [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:[result objectForKey:@"message"] input:nil];
                }
                
            } failure:^(NSError *error) {
                [SVProgressHUD dismiss];
                [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:LocalizedString(@"error_unknown", nil) input:nil];
            }];
        }
        
    }
    
}

- (IBAction)selectCategoryAction:(id)sender {
    
    if (categoryListArray.count>0) {
        
        [ActionSheetStringPicker showPickerWithTitle:@"" rows:categoryListArray initialSelection:selectedCategoryIndex doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
            
            selectedCategoryIndex = (int)selectedIndex;
            selectedCategory = [[categoryArray objectAtIndex:selectedCategoryIndex] valueForKey:@"slug"];
            [self.btnSelectCategory setTitle:selectedValue forState:UIControlStateNormal];
            
        } cancelBlock:^(ActionSheetStringPicker *picker) {
            
        } origin:self.view];
    }
}

- (BOOL)validateInputs
{
    if (!selectedCategory || selectedCategory.length==0) {
        [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:@"Please select category !" input:self.btnSelectCategory];
        return NO;
    }
    
    ALPValidator *validator = nil;
    validator = [ALPValidator validatorWithType:ALPValidatorTypeString];
    [validator addValidationToEnsurePresenceWithInvalidMessage:LocalizedString(@"error_baby_name", nil)];
    [validator validate:self.txtViewRequest.text];
    if(![validator isValid])
    {
        [self.txtViewRequest becomeFirstResponder];
        [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:@"Please input request !" input:self.txtViewRequest];
        return NO;
    }
    return YES;
}

-(void)getCategoryList
{
    [[WebServiceManager sharedManager] initWithBaseURL:@"api/supportCategories" parameters:nil success:^(id result) {
        if (![result isKindOfClass:[NSNull class]]) {
            categoryArray = [[NSMutableArray alloc] initWithArray:result];
            categoryListArray = [NSMutableArray new];
            for (int i=0; i<categoryArray.count; i++) {
                [categoryListArray addObject:[[categoryArray objectAtIndex:i]valueForKey:@"name"]];
            }
        }
    } failure:^(id responseObject) {
        NSLog(@"Failed");
    }];
}

#pragma mark - UIImagePickerController Delegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    self.btnTakePhoto.hidden = self.btnPhotoAlbum.hidden = YES;
    self.btnPhoto.hidden = false;
    imgAttachmentOriginal = [info objectForKey:UIImagePickerControllerOriginalImage];
    UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
    
    if (picker.sourceType == UIImagePickerControllerSourceTypeCamera) {
        UIImageWriteToSavedPhotosAlbum(imgAttachmentOriginal, self, @selector(savedPhotoImage:didFinishSavingWithError:contextInfo:), nil);
    }
    
    float scale = self.view.bounds.size.width*2/image.size.width;
    CGSize newSize = CGSizeMake(self.view.bounds.size.width*2, image.size.height*scale);
    if(image.size.width > image.size.height)
    {
        scale = self.view.bounds.size.width*2/image.size.height;
        newSize = CGSizeMake(image.size.width*scale, self.view.bounds.size.width*2);
    }
    UIGraphicsBeginImageContext(newSize);
    [image drawInRect:(CGRect){0, 0, newSize}];
    image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    [self.btnPhoto setImage:image forState:UIControlStateNormal];
    self.btnPhoto.hidden = NO;
    
    [self dismissViewControllerAnimated:NO completion:^{
    }];
    
//    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
//        if (self.popover.isPopoverVisible) {
//            [self.popover dismissPopoverAnimated:NO];
//        }
//        [self openEditor:imgAttachmentOriginal];
//        [picker dismissViewControllerAnimated:YES completion:^{
//            [self openEditor:imgAttachmentOriginal];
//        }];
//    } else {
//        [picker dismissViewControllerAnimated:YES completion:^{
//            [self openEditor:imgAttachmentOriginal];
//        }];
//    }
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void) savedPhotoImage:(UIImage *)image
didFinishSavingWithError:(NSError *)error
             contextInfo:(void *)contextInfo
{
    NSLog(@"This image has been saved to your camera roll");
}


- (void)openEditor:(UIImage*) image1
{
    PECropViewController *controller = [[PECropViewController alloc] init];
    controller.delegate = self;
    controller.toolbarHidden = YES;
    controller.image = image1;
    
    UIImage *image = image1;
    CGFloat width = image.size.width;
    CGFloat height = image.size.height;
    CGFloat length = MIN(width, height);
    controller.imageCropRect = CGRectMake((width - length) / 2,
                                          (height - length) / 2,
                                          length,
                                          length);
    
    //controller.cropAspectRatio = 7.8F / 10.0F;
    //controller.keepingCropAspectRatio = YES;
    
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:controller];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        navigationController.modalPresentationStyle = UIModalPresentationFormSheet;
    }
    
    [self presentViewController:navigationController animated:YES completion:NULL];
}


- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return largeImageView;
}

-(void)DidSelectCloseZoomView:(UIButton*)sender
{
    for (UIView *view in zoomView.subviews) {
        [view removeFromSuperview];
    }
    [zoomView removeFromSuperview];
}

#pragma mark - PECropViewControllerDelegate methods

- (void)cropViewController:(PECropViewController *)controller didFinishCroppingImage:(UIImage *)croppedImage transform:(CGAffineTransform)transform cropRect:(CGRect)cropRect
{
    [controller dismissViewControllerAnimated:YES completion:NULL];
    
    float scale = self.view.bounds.size.width*2/croppedImage.size.width;
    CGSize newSize = CGSizeMake(self.view.bounds.size.width*2, croppedImage.size.height*scale);
    //imgAttachmentCropped = croppedImage;
    
    if(croppedImage.size.width > croppedImage.size.height)
    {
        scale = self.view.bounds.size.width*2/croppedImage.size.height;
        newSize = CGSizeMake(croppedImage.size.width*scale, self.view.bounds.size.width*2);
    }
    UIGraphicsBeginImageContext(newSize);
    [croppedImage drawInRect:(CGRect){0, 0, newSize}];
    croppedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    self.btnTakePhoto.hidden = self.btnPhotoAlbum.hidden = YES;
    self.btnPhoto.hidden = false;
    [self.btnPhoto setImage:croppedImage forState:UIControlStateNormal];

}

- (void)cropViewControllerDidCancel:(PECropViewController *)controller
{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        // [self updateEditButtonEnabled];
    }
    
    [controller dismissViewControllerAnimated:YES completion:NULL];
}


@end
