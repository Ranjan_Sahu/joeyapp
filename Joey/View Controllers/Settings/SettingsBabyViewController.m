//
//  SettingsBabyViewController.m
//  Joey
//
//  Created by csl on 3/2/2017.
//  Copyright © 2017 Auxilia. All rights reserved.
//

#import "AppDelegate.h"
#import "SettingsBabyViewController.h"

@interface SettingsBabyViewController ()
{
    NSUserDefaults *shareObject;
    AppDelegate *appDelegate;
    NSDateFormatter *formatter;
    NSMutableDictionary *unitInfo;
    NSMutableArray *no100List;
    NSMutableArray *digitList;
    NSMutableArray *digit2List;
    NSMutableArray *bloodType1List;
    NSMutableArray *bloodType2List;
    NSDate *selectedBirthday;
    NSDate *selectedBirthDate;
    NSDate *selectedBirthTime;
    NSArray *selectedWeightIndexes;
    NSArray *selectedHeightIndexes;
    NSArray *selectedHeadSizeIndexes;
    NSArray *selectedBloodTypeIndexes;
    NSString *selectedGender;
    UIImage *imgUploadPhoto;
    UIImageView *imageView;
    UIView *baseView;

    float selectedWeight;
    float selectedHeight;
    float selectedHeadSize;
    BOOL removePhotoCheck;
    BOOL saveCheck;
    
}
@end

@implementation SettingsBabyViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    shareObject = [NSUserDefaults standardUserDefaults];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    formatter = [[NSDateFormatter alloc] init];
    [formatter setTimeZone:[NSTimeZone localTimeZone]];
    [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
    
    unitInfo = (NSMutableDictionary *)[NSKeyedUnarchiver unarchiveObjectWithData:[shareObject objectForKey:@"unit_info"]];
    no100List = [[NSMutableArray alloc] init];
    for(int i=0; i<100; i++) [no100List addObject:[NSString stringWithFormat:@"%d", i]];
    digitList = [[NSMutableArray alloc] init];
    for(int i=0; i<10; i++) [digitList addObject:[NSString stringWithFormat:@"%d", i]];
    digit2List = [[NSMutableArray alloc] init];
    for(int i=0; i<100; i++) [digit2List addObject:[NSString stringWithFormat:@"%02d", i]];
    
    formatter.dateFormat = @"yyyy-MM-dd";
    selectedBirthDate = [formatter dateFromString:[self.data objectForKey:@"birth_date"]];
    formatter.dateFormat = @"hh:mm a";
    selectedBirthTime = [formatter dateFromString:[self.data objectForKey:@"birth_time"]];
    formatter.dateFormat = @"yyyy-MM-dd hh:mm a";
    selectedBirthday = [formatter dateFromString:[NSString stringWithFormat:@"%@ %@", [self.data objectForKey:@"birth_date"], [self.data objectForKey:@"birth_time"]]];
    
    self.txtBirthDate.text = [self.data objectForKey:@"birth_date"];
    self.txtBirthDate.text = [NSString stringWithFormat:@"%@", [formatter stringFromDate:selectedBirthday]];
    
    selectedGender = [self.data objectForKey:@"gender"];
    if([selectedGender isEqualToString:@"girl"]) self.btnGender2.alpha = 0.25f;
    else self.btnGender1.alpha = 0.25f;
    
    selectedWeight = [self convertUnit:[[[self.data objectForKey:@"birth_weight"] componentsSeparatedByString:@" "][0] floatValue] unit:[[self.data objectForKey:@"birth_weight"] componentsSeparatedByString:@" "][1] newUnit:[unitInfo objectForKey:@"weight"]];
    selectedHeight = [self convertUnit:[[[self.data objectForKey:@"birth_height"] componentsSeparatedByString:@" "][0] floatValue] unit:[[self.data objectForKey:@"birth_height"] componentsSeparatedByString:@" "][1] newUnit:[unitInfo objectForKey:@"height"]];
    selectedHeadSize = [self convertUnit:[[[self.data objectForKey:@"birth_head_size"] componentsSeparatedByString:@" "][0] floatValue] unit:[[self.data objectForKey:@"birth_head_size"] componentsSeparatedByString:@" "][1] newUnit:[unitInfo objectForKey:@"head_size"]];
    selectedWeightIndexes = [[NSString stringWithFormat:@"%.2f", selectedWeight] componentsSeparatedByString:@"."];
    selectedHeightIndexes = [[NSString stringWithFormat:@"%.1f", selectedHeight] componentsSeparatedByString:@"."];
    selectedHeadSizeIndexes = [[NSString stringWithFormat:@"%.1f", selectedHeadSize] componentsSeparatedByString:@"."];
    self.txtWeight.text = [self convertUnit:[NSString stringWithFormat:@"%.2f %@", selectedWeight, [unitInfo objectForKey:@"weight"]] unit:[unitInfo objectForKey:@"weight"] spacing:YES];
    self.txtHeight.text = [self convertUnit:[NSString stringWithFormat:@"%.2f %@", selectedHeight, [unitInfo objectForKey:@"height"]] unit:[unitInfo objectForKey:@"height"] spacing:YES];
    self.txtHeadSize.text = [self convertUnit:[NSString stringWithFormat:@"%.2f %@", selectedHeadSize, [unitInfo objectForKey:@"head_size"]] unit:[unitInfo objectForKey:@"head_size"] spacing:YES];
    
    NSString *selectedBloodType = [self.data objectForKey:@"blood_type"];
    bloodType1List = [[NSMutableArray alloc] initWithObjects:@"A", @"B", @"AB", @"O", nil];
    bloodType2List = [[NSMutableArray alloc] initWithObjects:@"+", @"-", nil];
    selectedBloodTypeIndexes = [NSArray arrayWithObjects:[selectedBloodType substringToIndex:selectedBloodType.length-1], [selectedBloodType substringFromIndex:selectedBloodType.length-1], nil];
    self.txtBloodType.text = selectedBloodType;
    
    [self.navigationController setNavigationBarHidden:NO];
    [self.navigationController.navigationBar setBarTintColor:UIColorFromRGB(0xEEE8E4)];
    [self.navigationController.navigationBar setTranslucent:NO];
    [self.navigationItem setTitle:LocalizedString(@"title_settings_baby", nil)];
    [self customBackButton];
    
    if (IS_IPAD) {
        self.lblName.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.lblBirthDate.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.lblGender.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.lblWeight.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.lblHeight.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.lblHeadSize.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.lblBloodType.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.txtName.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:23.0f];
        self.txtBirthDate.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:23.0f];
        self.txtWeight.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:23.0f];
        self.txtHeight.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:23.0f];
        self.txtHeadSize.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:23.0f];
        self.txtBloodType.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:23.0f];
        [self.btnDelete.titleLabel setFont:[UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f]];
        self.btnDeleteHeightConstraint.constant = 66;
    }
    
    self.lblName.text = LocalizedString(@"txt_name", nil);
    self.lblBirthDate.text = LocalizedString(@"txt_birth_date", nil);
    self.lblGender.text = LocalizedString(@"txt_gender", nil);
    self.lblWeight.text = LocalizedString(@"txt_birth_weight", nil);
    self.lblHeight.text = LocalizedString(@"txt_birth_length", nil);
    self.lblHeadSize.text = LocalizedString(@"txt_head_size", nil);
    self.lblBloodType.text = LocalizedString(@"txt_blood_type", nil);
    self.btnSave.title = LocalizedString(@"btn_save", nil);
    self.txtName.text = [self.data objectForKey:@"name"];
    
    [self.btnDelete setTitle:LocalizedString(@"btn_delete_baby", nil) forState:UIControlStateNormal];
    self.btnDelete.layer.cornerRadius = 5.f;
    self.btnDelete.hidden = ![[self.data objectForKey:@"master_check"] boolValue];
    
    if([[self.data objectForKey:@"photo_url"] length] > 0)
    {
        self.btnAddImage.hidden = YES;
        self.btnRemoveImage.hidden = NO;
        self.scrollView.scrollEnabled = NO;
        self.photoView.clipsToBounds = YES;
        self.photoView.hidden = NO;
        self.photoView.layer.cornerRadius = self.photoView.frame.size.width/2;
        [self.photoView sd_setImageWithURL:[self.data objectForKey:@"photo_url"] placeholderImage:[UIImage imageNamed:@"dummy"] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
        }];
    }
    else
    {
        self.btnAddImage.hidden = NO;
        self.btnRemoveImage.hidden = YES;
        self.scrollView.scrollEnabled = YES;
        self.photoView.hidden = YES;
    }

    self.scrollView.layer.anchorPoint = CGPointMake(0.5, 0.5);
    self.scrollView.alwaysBounceVertical = YES;
    self.scrollView.alwaysBounceHorizontal = YES;
    //self.scrollView.minimumZoomScale = [self zoomScaleToBound];
    self.scrollView.maximumZoomScale = 2.0f;
    self.scrollView.showsVerticalScrollIndicator = NO;
    self.scrollView.showsHorizontalScrollIndicator = NO;
    self.scrollView.clipsToBounds = NO;
    self.automaticallyAdjustsScrollViewInsets = NO;

}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    self.photoView.layer.cornerRadius = self.photoView.frame.size.width/2;
}

#pragma mark : Custom button Actions
- (void)customBackButton
{
    if([self.navigationController.viewControllers count] > 1)
    {
        UIImage *imgBack = [UIImage imageNamed:@"nav_btn_back"];
        UIButton *btnBack = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnBack setImage:imgBack forState:UIControlStateNormal];
        btnBack.frame = CGRectMake(0, 0, imgBack.size.width, imgBack.size.height);
        [btnBack addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
        [btnBack setImageEdgeInsets:UIEdgeInsetsMake(0, -16, 0, 16)];
        UIBarButtonItem *backBarItem = [[UIBarButtonItem alloc] initWithCustomView:btnBack];
        self.navigationItem.leftBarButtonItem = backBarItem;
    }
}

- (void)backAction
{
    [self.data removeAllObjects];
    
    UIViewController *viewController = [self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count-2];
    viewController.data = [[NSMutableDictionary alloc] initWithDictionary:@{@"save_check":@(saveCheck)}];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)showCameraAction
{
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    [self presentViewController:imagePicker animated:NO completion:nil];
}

- (void)showPhotoAlbumAction
{
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:imagePicker animated:NO completion:nil];
}

- (BOOL)validateInputs
{
    ALPValidator *validator = nil;
    
    validator = [ALPValidator validatorWithType:ALPValidatorTypeString];
    [validator addValidationToEnsurePresenceWithInvalidMessage:LocalizedString(@"error_baby_name", nil)];
    [validator validate:self.txtName.text];
    if(![validator isValid])
    {
        [self.txtName becomeFirstResponder];
        [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:[[validator errorMessages] objectAtIndex:0] input:self.txtName];
        return NO;
    }
    
    return YES;
}

- (IBAction)getPhotoAction:(id)sender
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:LocalizedString(@"title_selectImage", nil) message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *btnTakePhoto = [UIAlertAction actionWithTitle:LocalizedString(@"title_camera", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        
        AVAuthorizationStatus status = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
        if(status == AVAuthorizationStatusDenied || status == AVAuthorizationStatusRestricted)
        {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:LocalizedString(@"error_camera", nil) message:@"" preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *btnOk = [UIAlertAction actionWithTitle:LocalizedString(@"btn_ok", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                
                [alert dismissViewControllerAnimated:YES completion:nil];
            }];
            
            [alert addAction:btnOk];
            
            [self presentViewController:alert animated:YES completion:nil];
            
        }
        else
        {
            [self showCameraAction];
        }
        
        [alertController dismissViewControllerAnimated:YES completion:nil];
    }];
    
    UIAlertAction *btnPhotoAlbum = [UIAlertAction actionWithTitle:LocalizedString(@"title_camera_roll", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        
        PHAuthorizationStatus status = [PHPhotoLibrary authorizationStatus];
        if(status == PHAuthorizationStatusDenied || status == PHAuthorizationStatusRestricted)
        {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:LocalizedString(@"alert_photo", nil) message:@"" preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *btnOk = [UIAlertAction actionWithTitle:@"btn_ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                
                [alert dismissViewControllerAnimated:YES completion:nil];
            }];
            
            [alert addAction:btnOk];
            
            [self presentViewController:alert animated:YES completion:nil];
            
        }
        else
        {
            [self showPhotoAlbumAction];
        }
        
        
        [alertController dismissViewControllerAnimated:YES completion:nil];
    }];
    
    UIAlertAction *btnCancel = [UIAlertAction actionWithTitle:LocalizedString(@"title_cancel", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction * action) {
        
        [alertController dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [alertController addAction:btnTakePhoto];
    [alertController addAction:btnPhotoAlbum];
    [alertController addAction:btnCancel];
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        UIButton *button = (UIButton *)sender;
        alertController.popoverPresentationController.sourceView = button;
        alertController.popoverPresentationController.sourceRect = CGRectMake(button.frame.size.width/2,button.frame.size.height/2,1.0,1.0);
    }
    
    [self presentViewController:alertController animated:YES completion:nil];
}

- (IBAction)removePhotoAction:(id)sender
{
    [self showConfirmAlert:LocalizedString(@"alert_are_you_sure", nil) message:@"" callback:^(UIAlertAction *action) {
        
        [imageView removeFromSuperview];
        [baseView removeFromSuperview];
        self.btnAddImage.hidden = NO;
        self.btnRemoveImage.hidden = YES;
        self.scrollView.scrollEnabled = YES;
        self.photoView.hidden = YES;
    }];
}

- (IBAction)getGenderAction:(id)sender
{
    UIButton *btn = (UIButton *)sender;
    if(btn == self.btnGender1)
    {
        self.btnGender1.alpha = 1.0f;
        self.btnGender2.alpha = 0.25f;
        selectedGender = @"girl";
    }
    else
    {
        self.btnGender1.alpha = 0.25f;
        self.btnGender2.alpha = 1.0f;
        selectedGender = @"boy";
    }
}

- (IBAction)removeAction:(id)sender
{
    NSMutableArray *babyList = (NSMutableArray *)[NSKeyedUnarchiver unarchiveObjectWithData:[shareObject objectForKey:@"baby_list"]];
    NSLog(@"babyList.count=%d", babyList.count);
    if(babyList.count == 1)
    {
        [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:LocalizedString(@"error_remove_baby", nil) input:nil];
        return;
    }
    
    
    [self showDeleteAlert:LocalizedString(@"alert_are_you_sure", nil) message:LocalizedString(@"alert_delete_baby", nil) callback:^(UIAlertAction *action) {
        
        [SVProgressHUD show];

        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setValue:@([shareObject integerForKey:@"user_id"]) forKey:@"user_id"];
        [parameters setValue:@([[self.data objectForKey:@"id"] intValue]) forKey:@"baby_id"];
        
        [[WebServiceManager sharedManager] initWithBaseURL:@"api/removeBaby" parameters:parameters success:^(id result) {
            
            if([[result objectForKey:@"success"] boolValue])
            {
                NSMutableArray *list = [result valueForKey:@"list"];
                [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:list] forKey:@"baby_list"];
                [shareObject synchronize];
                
                if(list.count > 0)
                {
                    saveCheck = YES;
                    appDelegate.updateTimelineCheck = appDelegate.updateActivitiesCheck = YES;
                    [self backAction];
                }
                else
                {
                    [self.view removeFromSuperview];
                    
                    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                    UINavigationController *navigationController = [storyboard instantiateViewControllerWithIdentifier:@"SplashNavigationController"];
                    appDelegate.window.rootViewController = navigationController;
                }
            }
            
        } failure:^(NSError *error) {
        }];
    }];
}

- (IBAction)nextAction:(id)sender
{
    if([self validateInputs])
    {
        [SVProgressHUD show];
        formatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setValue:@([shareObject integerForKey:@"user_id"]) forKey:@"user_id"];
        [parameters setValue:@([[self.data objectForKey:@"id"] intValue]) forKey:@"baby_id"];
        [parameters setValue:self.txtName.text forKey:@"name"];
        [parameters setValue:[formatter stringFromDate:selectedBirthday] forKey:@"birthday"];
        
        // To check if birth date is future date
        formatter.dateFormat = @"yyyy-MM-dd HH:mm:ss ZZZ";
        NSDate *time = [formatter dateFromString:[formatter stringFromDate:selectedBirthday]];
        if([time compare:[NSDate date]] == NSOrderedDescending)
        {
            [parameters setValue:@"" forKey:@"gender"];
            [parameters setValue:@"0" forKey:@"birth_weight"];
            [parameters setValue:@"0" forKey:@"birth_height"];
            [parameters setValue:@"0" forKey:@"birth_head_size"];
            [parameters setValue:@"0" forKey:@"blood_type"];
        }
        else
        {
            [parameters setValue:selectedGender forKey:@"gender"];
            [parameters setValue:[NSString stringWithFormat:@"%f", [self convertUnit:selectedWeight unit:[unitInfo objectForKey:@"weight"] newUnit:@"kg"]] forKey:@"birth_weight"];
            [parameters setValue:[NSString stringWithFormat:@"%f", [self convertUnit:selectedHeight unit:[unitInfo objectForKey:@"height"] newUnit:@"cm"]] forKey:@"birth_height"];
            [parameters setValue:[NSString stringWithFormat:@"%f", [self convertUnit:selectedHeadSize unit:[unitInfo objectForKey:@"head_size"] newUnit:@"cm"]] forKey:@"birth_head_size"];
            [parameters setValue:self.txtBloodType.text forKey:@"blood_type"];
        }
        
//        NSString *profilePicUrl = [self.data objectForKey:@"photo_url"];
//        if (profilePicUrl && profilePicUrl.length>0 && !imgUploadPhoto) {
//            [parameters setObject:[self.data objectForKey:@"photo_url"] forKey:@"photo_url"];
//        }
        
        if(imgUploadPhoto)
        {
            self.maskView.hidden = YES;
            self.btnRemoveImage.hidden = YES;
            
            CGSize newSize = CGSizeMake(self.contentView.frame.size.width, self.contentView.frame.size.height);
            UIGraphicsBeginImageContextWithOptions(newSize, NO, appDelegate.window.screen.scale);
            [self.contentView.layer renderInContext:UIGraphicsGetCurrentContext()];
            UIImage *screenshot = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();

            self.maskView.hidden = NO;
            self.btnRemoveImage.hidden = NO;
            
            [parameters setObject:[self.data objectForKey:@"photo_url"] forKey:@"photo_url"];
            
            [[WebServiceManager sharedManager] initWithBaseURL:@"api/updateBaby" parameters:parameters formData:^(id<AFMultipartFormData> formData) {
                [formData appendPartWithFileData:UIImageJPEGRepresentation(screenshot, 1.0f) name:@"image" fileName:@"image.jpg" mimeType:@"image/jpeg"];
                [formData appendPartWithFileData:UIImageJPEGRepresentation(imgUploadPhoto, 1.0f) name:@"image_original" fileName:@"image_original.jpg" mimeType:@"image/jpeg"];
            } success:^(id result) {
                
                if([[result objectForKey:@"success"] boolValue])
                {
                    [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableArray *)[result valueForKey:@"list"]] forKey:@"baby_list"];
                    [shareObject synchronize];
                    
                    saveCheck = YES;
                    appDelegate.updateTimelineCheck = appDelegate.updateActivitiesCheck = YES;
                    [self backAction];
                }
                else
                {
                    
                }
                
            } failure:^(NSError *error) {
                
            }];
        }
        else
        {
            [[WebServiceManager sharedManager] initWithBaseURL:@"api/updateBaby" parameters:parameters success:^(id result) {
                
                if([[result objectForKey:@"success"] boolValue])
                {
                    [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableArray *)[result valueForKey:@"list"]] forKey:@"baby_list"];
                    [shareObject synchronize];
                    
                    saveCheck = YES;
                    appDelegate.updateTimelineCheck = appDelegate.updateActivitiesCheck = YES;
                    [self backAction];
                }
                else
                {
                    [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:[result objectForKey:@"message"] input:nil];
                }
                
            } failure:^(NSError *error) {
            }];
        }
    }
}

#pragma mark - Custom Methods
- (void)selectTime:(NSString *)birthDate
{
    [ActionSheetDatePicker showPickerWithTitle:@"" datePickerMode:UIDatePickerModeTime selectedDate:selectedBirthTime minimumDate:nil maximumDate:nil doneBlock:^(ActionSheetDatePicker *picker, id selectedDate, id origin) {
        
        selectedBirthTime = selectedDate;
        NSLog(@"selectedMilestoneTime=%@", selectedBirthTime);
        
        formatter.dateFormat = @"hh:mm a";
        NSString *birthTime = [formatter stringFromDate:selectedDate];
        formatter.dateFormat = @"yyyy-MM-dd hh:mm a";
        selectedBirthday = [formatter dateFromString:[NSString stringWithFormat:@"%@ %@", birthDate, birthTime]];
        NSLog(@"selectedMilestoneDateTime=%@", selectedBirthday);
        
        if([[shareObject objectForKey:@"language"] hasPrefix:@"zh"]){
            [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"zh_HK"]];
            [formatter setDateStyle:NSDateFormatterMediumStyle];
            [formatter setTimeStyle:NSDateFormatterShortStyle];
        }else{
            [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
            formatter.dateFormat = @"EE d MMM, yyyy hh:mm a";
        }
        self.txtBirthDate.text = [NSString stringWithFormat:@"%@", [formatter stringFromDate:selectedBirthday]];
        [self.tableView reloadData];
        [self enableDisableInputRows];
        
    } cancelBlock:^(ActionSheetDatePicker *picker) {
        
    } origin:self.view];
}

-(void)enableDisableInputRows
{
    formatter.dateFormat = @"yyyy-MM-dd HH:mm:ss ZZZ";
    NSDate *time = [formatter dateFromString:[formatter stringFromDate:selectedBirthday]];
    
    if([time compare:[NSDate date]] == NSOrderedDescending)
    {
        self.btnGender1.hidden = YES;
        self.btnGender2.hidden = YES;
        self.txtWeight.hidden = YES;
        self.txtHeight.hidden = YES;
        self.txtHeadSize.hidden = YES;
        self.txtBloodType.hidden = YES;
    }
    else {
        self.btnGender1.hidden = NO;
        self.btnGender2.hidden = NO;
        self.txtWeight.hidden = NO;
        self.txtHeight.hidden = NO;
        self.txtHeadSize.hidden = NO;
        self.txtBloodType.hidden = NO;
    }
}

#pragma mark - Table view data source
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    formatter.dateFormat = @"yyyy-MM-dd HH:mm:ss ZZZ";
    NSDate *time = [formatter dateFromString:[formatter stringFromDate:selectedBirthday]];
    if((indexPath.row != 2)&&([time compare:[NSDate date]] == NSOrderedDescending)){
        return;
    }
    
    if(indexPath.row == 2)
    {
        NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
        [dateComponents setWeekdayOrdinal:40]; //change to 43 for 9 months 28 days
        NSCalendar *calendar = [NSCalendar currentCalendar];
        NSDate *maximumDate = [calendar dateByAddingComponents:dateComponents toDate:[NSDate date] options:0];
        
        [ActionSheetDatePicker showPickerWithTitle:@"" datePickerMode:UIDatePickerModeDate selectedDate:selectedBirthDate minimumDate:nil maximumDate:maximumDate doneBlock:^(ActionSheetDatePicker *picker, id selectedDate, id origin) {
            
            formatter.dateFormat = @"yyyy-MM-dd";
            NSString *birthDate = [formatter stringFromDate:selectedDate];
            selectedBirthDate = [formatter dateFromString:birthDate];
            
            formatter.dateFormat = @"EE d MMM, yyyy";
            self.txtBirthDate.text = [formatter stringFromDate:selectedDate];
            
            formatter.dateFormat = @"yyyy-MM-dd hh:mm a";
            selectedBirthday = [formatter dateFromString:[NSString stringWithFormat:@"%@ ", birthDate]];
            
            [self performSelector:@selector(selectTime:) withObject:birthDate afterDelay:0.3];
            
        } cancelBlock:^(ActionSheetDatePicker *picker) {
            
        } origin:self.view];
        
    }
    if(indexPath.row == 4)
    {
        NSArray *stringList = [NSArray arrayWithObjects:no100List, digit2List, nil];
        NSArray *initialList = [NSArray arrayWithObjects:@([selectedWeightIndexes[0] intValue]), @([selectedWeightIndexes[1] intValue]), nil];
        
        ActionSheetMultipleStringPicker *picker = [ActionSheetMultipleStringPicker showPickerWithTitle:@"" rows:stringList initialSelection:initialList doneBlock:^(ActionSheetMultipleStringPicker *picker, NSArray *selectedIndexes, id selectedValues) {
            
            selectedWeightIndexes = selectedIndexes;
            selectedWeight = [[selectedValues componentsJoinedByString:@"."] floatValue];
            NSLog(@"selectedWeight=%f", selectedWeight);
            self.txtWeight.text = [self convertUnit:[NSString stringWithFormat:@"%.2f %@", selectedWeight, [unitInfo objectForKey:@"weight"]] unit:[unitInfo objectForKey:@"weight"] spacing:YES];
            
            [self.tableView reloadData];
            
        } cancelBlock:^(ActionSheetMultipleStringPicker *picker) {
            
        } origin:self.view];
        
        UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(40, 0, (picker.viewSize.width-40)/2, 36)];
        label1.text = @".";
        label1.textAlignment = NSTextAlignmentRight;
        label1.font = [UIFont systemFontOfSize:20.0f];
        label1.backgroundColor = [[UIColor redColor] colorWithAlphaComponent:0.f];
        [[picker.pickerView.subviews objectAtIndex:1] addSubview:label1];
        
        UILabel *label2 = [[UILabel alloc] initWithFrame:CGRectMake(40+(picker.viewSize.width-40)/2, 0, (picker.viewSize.width-40)/2, 36)];
        label2.text = [NSString stringWithFormat:@"      %@", [unitInfo objectForKey:@"weight"]];
        label2.textAlignment = NSTextAlignmentCenter;
        label2.font = [UIFont systemFontOfSize:20.0f];
        label2.backgroundColor = [[UIColor greenColor] colorWithAlphaComponent:0.f];
        [[picker.pickerView.subviews objectAtIndex:1] addSubview:label2];
    }
    if(indexPath.row == 5)
    {
        NSArray *stringList = [NSArray arrayWithObjects:no100List, digitList, nil];
        NSArray *initialList = [NSArray arrayWithObjects:@([selectedHeightIndexes[0] intValue]), @([selectedHeightIndexes[1] intValue]), nil];
        
        ActionSheetMultipleStringPicker *picker = [ActionSheetMultipleStringPicker showPickerWithTitle:@"" rows:stringList initialSelection:initialList doneBlock:^(ActionSheetMultipleStringPicker *picker, NSArray *selectedIndexes, id selectedValues) {
            
            selectedHeightIndexes = selectedIndexes;
            selectedHeight = [[selectedValues componentsJoinedByString:@"."] floatValue];
            self.txtHeight.text = [self convertUnit:[NSString stringWithFormat:@"%f %@", selectedHeight, [unitInfo objectForKey:@"height"]] unit:[unitInfo objectForKey:@"height"] spacing:YES];
            
            [self.tableView reloadData];
            
        } cancelBlock:^(ActionSheetMultipleStringPicker *picker) {
            
        } origin:self.view];
        
        UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(40, 0, (picker.viewSize.width-40)/2, 36)];
        label1.text = @".";
        label1.textAlignment = NSTextAlignmentRight;
        label1.font = [UIFont systemFontOfSize:20.0f];
        label1.backgroundColor = [[UIColor redColor] colorWithAlphaComponent:0.f];
        [[picker.pickerView.subviews objectAtIndex:1] addSubview:label1];
        
        UILabel *label2 = [[UILabel alloc] initWithFrame:CGRectMake(40+(picker.viewSize.width-40)/2, 0, (picker.viewSize.width-40)/2, 36)];
        label2.text = [NSString stringWithFormat:@"      %@", [unitInfo objectForKey:@"height"]];
        label2.textAlignment = NSTextAlignmentCenter;
        label2.font = [UIFont systemFontOfSize:20.0f];
        label2.backgroundColor = [[UIColor greenColor] colorWithAlphaComponent:0.f];
        [[picker.pickerView.subviews objectAtIndex:1] addSubview:label2];
    }
    if(indexPath.row == 6)
    {
        NSArray *stringList = [NSArray arrayWithObjects:no100List, digitList, nil];
        NSArray *initialList = [NSArray arrayWithObjects:@([selectedHeadSizeIndexes[0] intValue]), @([selectedHeadSizeIndexes[1] intValue]), nil];
        
        ActionSheetMultipleStringPicker *picker = [ActionSheetMultipleStringPicker showPickerWithTitle:@"" rows:stringList initialSelection:initialList doneBlock:^(ActionSheetMultipleStringPicker *picker, NSArray *selectedIndexes, id selectedValues) {
            
            selectedHeadSizeIndexes = selectedIndexes;
            selectedHeadSize = [[selectedValues componentsJoinedByString:@"."] floatValue];
            self.txtHeadSize.text = [self convertUnit:[NSString stringWithFormat:@"%f %@", selectedHeadSize, [unitInfo objectForKey:@"head_size"]] unit:[unitInfo objectForKey:@"head_size"] spacing:YES];
            
            [self.tableView reloadData];
            
        } cancelBlock:^(ActionSheetMultipleStringPicker *picker) {
            
        } origin:self.view];
        
        UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(40, 0, (picker.viewSize.width-40)/2, 36)];
        label1.text = @".";
        label1.textAlignment = NSTextAlignmentRight;
        label1.font = [UIFont systemFontOfSize:20.0f];
        label1.backgroundColor = [[UIColor redColor] colorWithAlphaComponent:0.f];
        [[picker.pickerView.subviews objectAtIndex:1] addSubview:label1];
        
        UILabel *label2 = [[UILabel alloc] initWithFrame:CGRectMake(40+(picker.viewSize.width-40)/2, 0, (picker.viewSize.width-40)/2, 36)];
        label2.text = [NSString stringWithFormat:@"      %@", [unitInfo objectForKey:@"head_size"]];
        label2.textAlignment = NSTextAlignmentCenter;
        label2.font = [UIFont systemFontOfSize:20.0f];
        label2.backgroundColor = [[UIColor greenColor] colorWithAlphaComponent:0.f];
        [[picker.pickerView.subviews objectAtIndex:1] addSubview:label2];
    }
    if(indexPath.row == 7)
    {
        NSArray *stringList = [NSArray arrayWithObjects:bloodType1List, bloodType2List, nil];
        NSArray *initialList = [NSArray arrayWithObjects:selectedBloodTypeIndexes[0], selectedBloodTypeIndexes[1], nil];
        
        [ActionSheetMultipleStringPicker showPickerWithTitle:@"" rows:stringList initialSelection:initialList doneBlock:^(ActionSheetMultipleStringPicker *picker, NSArray *selectedIndexes, id selectedValues) {
            
            selectedBloodTypeIndexes = selectedIndexes;
            self.txtBloodType.text = [selectedValues componentsJoinedByString:@""];
            
            [self.tableView reloadData];
            
        } cancelBlock:^(ActionSheetMultipleStringPicker *picker) {
            
        } origin:self.view];
    }
 }


#pragma mark - UIImagePickerController Delegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    dispatch_async(dispatch_get_main_queue(), ^{
        
        self.btnAddImage.hidden = YES;
        self.btnRemoveImage.hidden = NO;
        imgUploadPhoto = [info objectForKey:UIImagePickerControllerOriginalImage];
        NSLog(@"image=%@", imgUploadPhoto);
        
        if (picker.sourceType == UIImagePickerControllerSourceTypeCamera) {
            UIImageWriteToSavedPhotosAlbum(imgUploadPhoto, self, @selector(savedPhotoImage:didFinishSavingWithError:contextInfo:), nil);
        }
        imageView = [[UIImageView alloc] initWithImage:imgUploadPhoto];
        imageView.frame = CGRectMake(0, 0, imgUploadPhoto.size.width, imgUploadPhoto.size.height);

        //NSLog(@"Height/Width Ratio:%f",imgUploadPhoto.size.height/imgUploadPhoto.size.width);
        //NSLog(@"Width/Height Ratio:%f",imgUploadPhoto.size.width/imgUploadPhoto.size.height);

        float height_Constant = 2.0;
        float width_Constant = 2.0;
        if (imgUploadPhoto.size.width > imgUploadPhoto.size.height) {
            height_Constant = width_Constant = imgUploadPhoto.size.width / imgUploadPhoto.size.height;
        } else {
            height_Constant = width_Constant = imgUploadPhoto.size.height / imgUploadPhoto.size.width;
        }

        baseView = [[UIView alloc] initWithFrame:imageView.frame];
        imageView.center = CGPointMake(baseView.center.x-20, baseView.center.y-20);
        self.scrollView.contentSize = CGSizeMake(baseView.frame.size.width-40, baseView.frame.size.height-40);
        
//        imageView.center = baseView.center;
//
//        self.scrollView.contentSize = baseView.frame.size;
        [self.scrollView addSubview:baseView];
        [baseView addSubview:imageView];
        
        self.scrollView.contentOffset = CGPointMake(imgUploadPhoto.size.width, imgUploadPhoto.size.height);
        _scrollView.minimumZoomScale = [self zoomScaleToBound];
        
        [self dismissViewControllerAnimated:NO completion:nil];
    });
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return baseView;
}

- (CGFloat)zoomScaleToBound
{
    CGFloat scaleW = self.scrollView.bounds.size.width / imageView.bounds.size.width;
    CGFloat scaleH = self.scrollView.bounds.size.height / imageView.bounds.size.height;
    CGFloat max = MAX(scaleW, scaleH);
    
    return max;
}

- (void) savedPhotoImage:(UIImage *)image
didFinishSavingWithError:(NSError *)error
             contextInfo:(void *)contextInfo
{
    NSLog(@"This image has been saved to your camera roll");
}

@end
