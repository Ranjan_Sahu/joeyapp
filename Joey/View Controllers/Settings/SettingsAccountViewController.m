//
//  SettingsAccountViewController.m
//  Joey
//
//  Created by csl on 24/1/2017.
//  Copyright © 2017 Auxilia. All rights reserved.
//

#import "AppDelegate.h"
#import "SettingsAccountViewController.h"

@interface SettingsAccountViewController ()
{
    NSUserDefaults *shareObject;
    AppDelegate *appDelegate;
    
    CGFloat rowHeight;
}
@end

@implementation SettingsAccountViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    shareObject = [NSUserDefaults standardUserDefaults];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    [self.navigationController setNavigationBarHidden:NO];
    [self.navigationController.navigationBar setBarTintColor:UIColorFromRGB(0xEEE8E4)];
    [self.navigationController.navigationBar setTranslucent:NO];
    [self.navigationItem setTitle:LocalizedString(@"title_settings_account", nil)];
    [self customBackButton];
    
    self.btnSave.title = LocalizedString(@"btn_save", nil);
    self.lblName.text = LocalizedString(@"txt_name", nil);
    self.lblEmail.text = LocalizedString(@"txt_email", nil);
    self.lblOldPassword.text = LocalizedString(@"txt_old_password", nil);
    self.lblNewPassword.text = LocalizedString(@"txt_new_password", nil);
    self.lblRetypePassword.text = LocalizedString(@"txt_retype_password", nil);
    self.txtName.text = [shareObject objectForKey:@"user_name"];
    self.txtEmail.text = [shareObject objectForKey:@"user_email"];
    
    
    if (IS_IPAD) {
        self.lblName.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.lblEmail.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.lblOldPassword.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.lblNewPassword.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.lblRetypePassword.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.txtName.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.txtEmail.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.txtOldPassword.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.txtNewPassword.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.txtRetypePassword.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        
        self.btnDelete.titleLabel.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.btnLogout.titleLabel.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        
        self.btnLogoutHeightConstraint.constant = 64;
        self.btnDeleteHeightConstraint.constant = 64;
    }
    
    [self.btnLogout setTitle:LocalizedString(@"btn_logout", nil) forState:UIControlStateNormal];
    [self.btnDelete setTitle:LocalizedString(@"btn_delete_account", nil) forState:UIControlStateNormal];
    self.btnLogout.layer.cornerRadius = 5.f;
    self.btnDelete.layer.cornerRadius = 5.f;
}

- (void)customBackButton
{
    if([self.navigationController.viewControllers count] > 1)
    {
        UIImage *imgBack = [UIImage imageNamed:@"nav_btn_back"];
        UIButton *btnBack = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnBack setImage:imgBack forState:UIControlStateNormal];
        btnBack.frame = CGRectMake(0, 0, imgBack.size.width, imgBack.size.height);
        [btnBack addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
        [btnBack setImageEdgeInsets:UIEdgeInsetsMake(0, -16, 0, 16)];
        UIBarButtonItem *backBarItem = [[UIBarButtonItem alloc] initWithCustomView:btnBack];
        self.navigationItem.leftBarButtonItem = backBarItem;
    }
}

- (void)backAction
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (BOOL)validateInputs
{
    ALPValidator *validator = nil;
    
    validator = [ALPValidator validatorWithType:ALPValidatorTypeString];
    [validator addValidationToEnsurePresenceWithInvalidMessage:LocalizedString(@"error_name", nil)];
    [validator validate:self.txtName.text];
    if(![validator isValid])
    {
        [self.txtName becomeFirstResponder];
        [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:[[validator errorMessages] objectAtIndex:0] input:self.txtName];
        return NO;
    }
    
    validator = [ALPValidator validatorWithType:ALPValidatorTypeString];
    [validator addValidationToEnsureRegularExpressionIsMetWithPattern:@"^[_A-Za-z0-9-+]+(\\.[_A-Za-z0-9-+]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z‌​]{2,10})$" invalidMessage:LocalizedString(@"error_email", nil)];
    [validator validate:self.txtEmail.text];
    if(![validator isValid])
    {
        [self.txtEmail becomeFirstResponder];
        [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:[[validator errorMessages] objectAtIndex:0] input:self.txtEmail];
        return NO;
    }
    
    if(self.txtOldPassword.text.length > 0 || self.txtNewPassword.text.length > 0 || self.txtRetypePassword.text.length > 0)
    {
        validator = [ALPValidator validatorWithType:ALPValidatorTypeString];
        [validator addValidationToEnsurePresenceWithInvalidMessage:LocalizedString(@"error_old_password", nil)];
        [validator validate:self.txtOldPassword.text];
        if(![validator isValid])
        {
            [self.txtOldPassword becomeFirstResponder];
            [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:[[validator errorMessages] objectAtIndex:0] input:self.txtOldPassword];
            return NO;
        }
        
        validator = [ALPValidator validatorWithType:ALPValidatorTypeString];
        [validator addValidationToEnsurePresenceWithInvalidMessage:LocalizedString(@"error_new_password", nil)];
        [validator addValidationToEnsureMinimumLength:6 invalidMessage:LocalizedString(@"error_password_min", nil)];
        [validator addValidationToEnsureMaximumLength:50 invalidMessage:LocalizedString(@"error_password_max", nil)];
        [validator validate:self.txtNewPassword.text];
        if(![validator isValid])
        {
            [self.txtNewPassword becomeFirstResponder];
            [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:[[validator errorMessages] objectAtIndex:0] input:self.txtNewPassword];
            return NO;
        }
        
        validator = [ALPValidator validatorWithType:ALPValidatorTypeString];
        [validator addValidationToEnsurePresenceWithInvalidMessage:LocalizedString(@"error_retype_password", nil)];
        [validator validate:self.txtRetypePassword.text];
        if(![validator isValid])
        {
            [self.txtRetypePassword becomeFirstResponder];
            [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:[[validator errorMessages] objectAtIndex:0] input:self.txtRetypePassword];
            return NO;
        }
        
        if(![self.txtNewPassword.text isEqualToString:self.txtRetypePassword.text])
        {
            [self.txtRetypePassword becomeFirstResponder];
            [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:LocalizedString(@"error_retype_not_match_password", nil) input:self.txtRetypePassword];
            return NO;
        }
    }
    
    return YES;
}

- (IBAction)logoutAction:(id)sender
{
    [self showConfirmAlert:LocalizedString(@"alert_are_you_sure", nil) message:@"" callback:^(UIAlertAction *action) {
        [self logout];
    }];
}

- (IBAction)deleteAction:(id)sender
{
    [self showDeleteAlert:LocalizedString(@"alert_are_you_sure", nil) message:LocalizedString(@"alert_delete_account", nil) callback:^(UIAlertAction *action) {
        
        [SVProgressHUD show];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setValue:@([shareObject integerForKey:@"user_id"]) forKey:@"id"];
        
        [[WebServiceManager sharedManager] initWithBaseURL:@"api/removeUser" parameters:parameters success:^(id result) {
            
            if([[result objectForKey:@"success"] boolValue])
            {
                [shareObject removeObjectForKey:@"baby_id"];
                [shareObject removeObjectForKey:@"baby_index"];
                [shareObject removeObjectForKey:@"user_id"];
                [shareObject removeObjectForKey:@"user_name"];
                [shareObject removeObjectForKey:@"user_email"];
                [shareObject removeObjectForKey:@"facebook_id"];
                [shareObject removeObjectForKey:@"email_confirm_check"];
                [shareObject removeObjectForKey:@"family_list"];
                [shareObject removeObjectForKey:@"baby_list"];
                [shareObject removeObjectForKey:@"device_list"];
                [shareObject removeObjectForKey:@"unit_info"];
                [shareObject removeObjectForKey:@"feed_info"];
                [shareObject removeObjectForKey:@"pump_info"];
                [shareObject removeObjectForKey:@"sleep_info"];
                [shareObject removeObjectForKey:@"poo_info"];
                [shareObject removeObjectForKey:@"wee_info"];
                [shareObject removeObjectForKey:@"medical_info"];
                [shareObject synchronize];
                
                [self.view removeFromSuperview];
                
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                UINavigationController *navigationController = [storyboard instantiateViewControllerWithIdentifier:@"SplashNavigationController"];
                appDelegate.window.rootViewController = navigationController;
            }
            else
            {
                
            }
            
        } failure:^(NSError *error) {
            
        }];
    }];
}

- (IBAction)nextAction:(id)sender
{
    if([self validateInputs])
    {
        [SVProgressHUD show];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setValue:@([shareObject integerForKey:@"user_id"]) forKey:@"user_id"];
        if(![self.txtName.text isEqualToString:[shareObject objectForKey:@"user_name"]]) [parameters setValue:self.txtName.text forKey:@"name"];
        if(![self.txtEmail.text isEqualToString:[shareObject objectForKey:@"user_email"]]) [parameters setValue:self.txtEmail.text forKey:@"email"];
        if(self.txtOldPassword != nil && [self.txtOldPassword.text length]>0)
        {
            [parameters setValue:self.txtOldPassword.text forKey:@"old_password"];
            [parameters setValue:self.txtNewPassword.text forKey:@"password"];
        }
        
        [[WebServiceManager sharedManager] initWithBaseURL:@"api/updateUser" parameters:parameters success:^(id result) {
            
            [SVProgressHUD dismiss];
            if([[result objectForKey:@"success"] boolValue])
            {
                if(![self.txtName.text isEqualToString:[shareObject objectForKey:@"user_name"]])
                {
                    [shareObject setObject:self.txtName.text forKey:@"user_name"];
                }
                if(![self.txtEmail.text isEqualToString:[shareObject objectForKey:@"user_email"]])
                {
                    [shareObject setObject:self.txtEmail.text forKey:@"user_email"];
                    [shareObject setBool:NO forKey:@"email_confirm_check"];
                }
                [shareObject synchronize];
                
                self.txtOldPassword.text = @"";
                self.txtNewPassword.text = @"";
                self.txtRetypePassword.text = @"";
                
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:LocalizedString(@"title_success", nil) message:LocalizedString(@"txt_user_updated", nil) preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *btnOk = [UIAlertAction actionWithTitle:LocalizedString(@"btn_ok", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                    [alert dismissViewControllerAnimated:YES completion:nil];
                    [self logout];
                }];
                [alert addAction:btnOk];
                [self presentViewController:alert animated:YES completion:nil];
                
            }else{
                [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:LocalizedString([result objectForKey:@"message"], nil) input:nil];
            }
            
        } failure:^(NSError *error) {
            [SVProgressHUD dismiss];
            [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:LocalizedString(@"error_unknown", nil) input:nil];
        }];
    }
}

#pragma mark - Logout User
-(void)logout
{
    [shareObject removeObjectForKey:@"baby_id"];
    [shareObject removeObjectForKey:@"baby_index"];
    [shareObject removeObjectForKey:@"user_id"];
    [shareObject removeObjectForKey:@"user_name"];
    [shareObject removeObjectForKey:@"user_email"];
    [shareObject removeObjectForKey:@"facebook_id"];
    [shareObject removeObjectForKey:@"email_confirm_check"];
    [shareObject removeObjectForKey:@"family_list"];
    [shareObject removeObjectForKey:@"baby_list"];
    [shareObject removeObjectForKey:@"device_list"];
    [shareObject removeObjectForKey:@"unit_info"];
    [shareObject removeObjectForKey:@"feed_info"];
    [shareObject removeObjectForKey:@"pump_info"];
    [shareObject removeObjectForKey:@"sleep_info"];
    [shareObject removeObjectForKey:@"poo_info"];
    [shareObject removeObjectForKey:@"wee_info"];
    [shareObject removeObjectForKey:@"medical_info"];
    [shareObject synchronize];
    
    //NSString *appDomain = [[NSBundle mainBundle] bundleIdentifier];
    //[[NSUserDefaults standardUserDefaults] removePersistentDomainForName:appDomain];
    [self.view removeFromSuperview];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UINavigationController *navigationController = [storyboard instantiateViewControllerWithIdentifier:@"SplashNavigationController"];
    appDelegate.window.rootViewController = navigationController;

}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)aTableView
{
    return 3;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if(section == 1) {
        return LocalizedString(@"header_password", nil);
    }
    return nil;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    CGFloat headerHeight = 40.0;
    if (IS_IPAD) {
        headerHeight = 60.0;
    }
    
    if(section == 1)
    {
        if([shareObject objectForKey:@"facebook_id"]){
            return nil;
        }
        
        UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, headerHeight)];
        UILabel *txtHeader = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, tableView.bounds.size.width-20, headerHeight)];
        
        if (IS_IPAD) {
            txtHeader.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        }else{
            txtHeader.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:15.0f];
        }
        txtHeader.textColor = [UIColor whiteColor];
        txtHeader.text = [self tableView:tableView titleForHeaderInSection:section];
        
        headerView.backgroundColor = UIColorFromRGB(0xBFB5AE);
        [headerView addSubview:txtHeader];
        return headerView;
    }
    
    return nil;
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (IS_IPAD) {
        if (section == 1) {
            return 60;
        }
    }
    if(section == 1){
        if([shareObject objectForKey:@"facebook_id"]) return 0;
        return 40;
    }
    return 0;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (IS_IPAD) {
        if(indexPath.section == 2 && indexPath.row == 0)
        {
            if([shareObject objectForKey:@"facebook_id"]) return self.tableView.frame.size.height-(2*70);
            return self.tableView.frame.size.height-((5*70)+40);
        }
        if([shareObject objectForKey:@"facebook_id"])
        {
            if(indexPath.section == 1) return 0;
        }
        return UITableViewAutomaticDimension; //return 70;
    }
    if(indexPath.section == 2 && indexPath.row == 0)
    {
        if([shareObject objectForKey:@"facebook_id"]) return self.tableView.frame.size.height-(2*50);
        return self.tableView.frame.size.height-((5*50)+40);
    }
    if([shareObject objectForKey:@"facebook_id"])
    {
        if(indexPath.section == 1) return 0;
    }
    return UITableViewAutomaticDimension; //return 50;
}
- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (IS_IPAD) {
        if(indexPath.section == 2 && indexPath.row == 0)
        {
            if([shareObject objectForKey:@"facebook_id"]) return self.tableView.frame.size.height-(2*70);
            return self.tableView.frame.size.height-((5*70)+40);
        }
        if([shareObject objectForKey:@"facebook_id"])
        {
            if(indexPath.section == 1) return 0;
        }
        return UITableViewAutomaticDimension; //return 70;
    }
    if(indexPath.section == 2 && indexPath.row == 0)
    {
        if([shareObject objectForKey:@"facebook_id"]) return self.tableView.frame.size.height-(2*50);
        return self.tableView.frame.size.height-((5*50)+40);
    }
    if([shareObject objectForKey:@"facebook_id"])
    {
        if(indexPath.section == 1) return 0;
    }
    return UITableViewAutomaticDimension; //return 50;
}

@end
