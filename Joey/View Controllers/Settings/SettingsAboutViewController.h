//
//  SettingsAboutViewController.h
//  Joey
//
//  Created by csl on 24/1/2017.
//  Copyright © 2017 Auxilia. All rights reserved.
//

#import "BaseViewController.h"
#import <KILabel/KILabel.h>

@interface SettingsAboutViewController : BaseViewController

@property (nonatomic, weak) IBOutlet UILabel *lblTitle;
@property (nonatomic, weak) IBOutlet KILabel *lblMessage;
@property (nonatomic, weak) IBOutlet UIButton *btnTerms;
@property (nonatomic, weak) IBOutlet UIButton *btnPrivacy;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *btnPrivacyHeightConstraints;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *btnTermsHeightConstraints;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *logoWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *logoHeight;

@end
