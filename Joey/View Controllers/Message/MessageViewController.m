//
//  MessageViewController.m
//  Joey
//
//  Created by webwerks on 2/8/18.
//  Copyright © 2018 Auxilia. All rights reserved.
//

#import "MessageViewController.h"
#import "AppDelegate.h"
#import "MessageListTableViewCell.h"
#import "MessageDetailsViewController.h"

@interface MessageViewController ()
{
    NSUserDefaults *shareObject;
    AppDelegate *appDelegate;
    NSMutableArray *messageList;
    
    BOOL isLoading;
    int limit;
    int offset;
    
}
@end

@implementation MessageViewController

#pragma mark - View Controller Methods
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    shareObject = [NSUserDefaults standardUserDefaults];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
//    if (IS_IPHONE_X) {
//        self.bottomViewBottomSpace.constant = 84.0f;
//    }

    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:UIColorFromRGB(0x8D8082), NSForegroundColorAttributeName, [UIFont fontWithName:@"SFUIDisplay-Medium" size:18.0f], NSFontAttributeName, nil]];
    [self.navigationController.navigationBar setBarTintColor:UIColorFromRGB(0xEEE8E4)];
    [self.navigationController.navigationBar setTranslucent:NO];
    
    UIButton *btnLogo = [[UIButton alloc] initWithFrame:CGRectZero];
    [btnLogo setImage:[UIImage imageNamed:@"nav_logo.png"] forState:UIControlStateNormal];
    [btnLogo sizeToFit];
    self.navigationItem.titleView = btnLogo;
    [self customSettingsButton];

    messageList = [NSMutableArray new];
    [self.tblView registerNib:[UINib nibWithNibName:@"MessageListTableViewCell" bundle:nil] forCellReuseIdentifier:@"MessageListTableViewCell"];
    self.tblView.dataSource = self;
    self.tblView.delegate = self;
    self.tblView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    __weak __typeof(self)weakSelf = self;
    [self.tblView addPullToRefreshWithActionHandler:^{
        [weakSelf refreshAction];
    }];
    
    [self.tblView addInfiniteScrollingWithActionHandler:^{
        [weakSelf getMessageList];
    }];

}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:(BOOL)animated];
    [self.navigationController.navigationBar setBarTintColor:UIColorFromRGB(0xEEE8E4)];
    [self refreshAction];
    
    //    UITabBarItem *tabbarItem = (UITabBarItem *)[appDelegate.tabbar.tabBar.items objectAtIndex:4];
    //    NSLog(@"tabbarItem=%d", [tabbarItem.badgeValue intValue]);

}

#pragma mark - UI Customization & Layout Hadling Methods
- (void)customSettingsButton
{
    UIImage *imgSettings = [UIImage imageNamed:@"nav_btn_settings"];
    UIButton *btnSettings = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnSettings setImage:imgSettings forState:UIControlStateNormal];
    btnSettings.frame = CGRectMake(52, 0, imgSettings.size.width, imgSettings.size.height);
    [btnSettings addTarget:self action:@selector(getSettingsAction:) forControlEvents:UIControlEventTouchUpInside];
    
    UIView *buttonView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 88, 44)];
    [buttonView addSubview:btnSettings];
    
    UIBarButtonItem *backBarItem = [[UIBarButtonItem alloc] initWithCustomView:buttonView];
    self.navigationItem.rightBarButtonItem = backBarItem;
}

#pragma mark -  TableView Data Source & Delegates
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return messageList.count;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (messageList.count>0) {
        NSString *title = [NSString stringWithFormat:@"%@",[[messageList objectAtIndex:section] valueForKey:@"name"]];
        return title;
    }
    return @"";
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    CGFloat headerHeight = 30;
    if (IS_IPAD) {
        headerHeight = 40;
    }
    
    UILabel *txtHeader = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, tableView.bounds.size.width-20, headerHeight)];
    txtHeader.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:15.0f];
    if(IS_IPAD) {
        txtHeader.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f];
    }
    txtHeader.textColor = UIColorFromRGB(0x8D8082);
    txtHeader.text = [self tableView:tableView titleForHeaderInSection:section];
    
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, headerHeight)];
    headerView.backgroundColor = UIColorFromRGB(0xEEE8E4);
    [headerView addSubview:txtHeader];
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (IS_IPAD) {
        return 40;
    }
    else {
        return 30;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (messageList.count>0) {
        return [[[messageList objectAtIndex:section] objectForKey:@"list"] count];
    }
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MessageListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MessageListTableViewCell"];
    if(!cell){
        cell = [tableView dequeueReusableCellWithIdentifier:@"MessageListTableViewCell"];
    }
    
    if ([[[messageList objectAtIndex:indexPath.section] objectForKey:@"list"] count]>0) {
        
        NSDictionary *msgDict = [[[messageList objectAtIndex:indexPath.section] objectForKey:@"list"] objectAtIndex:indexPath.row];
        
        NSString *name = [msgDict valueForKey:@"name"];
        if ([name isKindOfClass:[NSNull class]]) {
            name = @"others";
        }
        
        UIImage *imgIcon = [UIImage imageNamed:[NSString stringWithFormat:@"icon_%@.png", name]];
        
        if ([name isEqualToString:@"birthday"] || [name isEqualToString:@"reminder"]) {
            imgIcon = [UIImage imageNamed:[NSString stringWithFormat:@"icon_%@.png", name]];
        }
        else if ([name isEqualToString:@"grow"] || [name isEqualToString:@"move"] || [name isEqualToString:@"iq"] || [name isEqualToString:@"health"] || [name isEqualToString:@"others"] || [name isEqualToString:@"pregnancy"]){
            imgIcon = [UIImage imageNamed:[NSString stringWithFormat:@"icon_%@_Selected.png", name]];
        }

        // Activities
        if([name isEqualToString:@"feed"]){
            cell.imgIcon.backgroundColor = UIColorFromRGB(0xF4EA9E);
            cell.imgIconChevronRight.tintColor = UIColorFromRGB(0xF4EA9E);
        }if([name isEqualToString:@"pump"]){
            cell.imgIcon.backgroundColor = UIColorFromRGB(0xF9C99B);
            cell.imgIconChevronRight.tintColor = UIColorFromRGB(0xF9C99B);
        }if([name isEqualToString:@"sleep"]){
            cell.imgIcon.backgroundColor = UIColorFromRGB(0xC5D7E1);
            cell.imgIconChevronRight.tintColor = UIColorFromRGB(0xC5D7E1);
        }if([name isEqualToString:@"poo"]){
            cell.imgIcon.backgroundColor = UIColorFromRGB(0xE0C5B8);
            cell.imgIconChevronRight.tintColor = UIColorFromRGB(0xE0C5B8);
        }if([name isEqualToString:@"wee"]){
            cell.imgIcon.backgroundColor = UIColorFromRGB(0xCADAA9);
            cell.imgIconChevronRight.tintColor = UIColorFromRGB(0xCADAA9);
        }if([name isEqualToString:@"medical"]){
            cell.imgIcon.backgroundColor = UIColorFromRGB(0xF4D2D5);
            cell.imgIconChevronRight.tintColor = UIColorFromRGB(0xF4D2D5);
        }
        
        // Milestones
        if([name isEqualToString:@"birthday"]){
            cell.imgIcon.backgroundColor = UIColorFromRGB(0xB9CDDA);
            cell.imgIconChevronRight.tintColor = UIColorFromRGB(0xB9CDDA);
        }if([name isEqualToString:@"grow"]){
            cell.imgIcon.backgroundColor = UIColorFromRGB(0xF3E99F);
            cell.imgIconChevronRight.tintColor = UIColorFromRGB(0xF3E99F);
        }if([name isEqualToString:@"move"]){
            cell.imgIcon.backgroundColor = UIColorFromRGB(0xFFC896);
            cell.imgIconChevronRight.tintColor = UIColorFromRGB(0xFFC896);
        }if([name isEqualToString:@"iq"]){
            cell.imgIcon.backgroundColor = UIColorFromRGB(0xC5DEAA);
            cell.imgIconChevronRight.tintColor = UIColorFromRGB(0xC5DEAA);
        }if([name isEqualToString:@"health"]){
            cell.imgIcon.backgroundColor = UIColorFromRGB(0xFACFD4);
            cell.imgIconChevronRight.tintColor = UIColorFromRGB(0xFACFD4);
        }if([name isEqualToString:@"others"]){
            cell.imgIcon.backgroundColor = UIColorFromRGB(0xDFCCDC);
            cell.imgIconChevronRight.tintColor = UIColorFromRGB(0xDFCCDC);
        }if ([name isEqualToString:@"pregnancy"]) {
            cell.imgIcon.backgroundColor = UIColorFromRGB(0xDCC4E8);
            cell.imgIconChevronRight.tintColor = UIColorFromRGB(0xDCC4E8);
        }
        
        cell.imgIconWidth.constant = imgIcon.size.width ;
        cell.imgIconHeight.constant = imgIcon.size.height;
        
        if (IS_IPAD) {
            //cell.lblMessageTitle.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
            //cell.lblMessageSubText.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:19.0f];
            cell.imgIconWidth.constant = cell.imgIconWidth.constant + 20;
            cell.imgIconHeight.constant = cell.imgIconHeight.constant + 20;
            cell.indicatorWidth.constant = 16;
            cell.indicatorHeight.constant = 16;
        }
        cell.imgIcon.image = imgIcon;
        cell.imgIcon.layer.cornerRadius = cell.imgIconWidth.constant/2;
        cell.indicatorView.layer.cornerRadius = cell.indicatorWidth.constant/2;
        
        cell.lblMessageTitle.text = [NSString stringWithFormat:@"%@",[msgDict valueForKey:@"message"]];
        cell.lblMessageSubText.text = @"";//Baby activity/milestone notifications If the timer is off/paused, the User can manually set the Duration value by scrolling on the page. value by scrolling on the page User can manually set the User can manually set the User can manually set the alue by scrolling on lestone notifications If the timer  on the page. valu n value by scrollin. ";

        if ([[msgDict valueForKey:@"status"] boolValue]) {
            cell.indicatorView.hidden = YES;
        }else {
            cell.indicatorView.hidden = NO;
        }
        
    }
    
    [cell layoutSubviews];
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"MessageDetailsViewController"];
    
    NSMutableDictionary *msgDict = [[[messageList objectAtIndex:indexPath.section] objectForKey:@"list"] objectAtIndex:indexPath.row];
    viewController.data = msgDict;
    viewController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:viewController animated:YES];
}

#pragma mark -  View's Data Configuration methods
-(void)refreshAction
{
    [self.tblView.pullToRefreshView stopAnimating];
    offset = 0;
    [self getMessageList];
}

#pragma mark - IBAction Methods
- (IBAction)getSettingsAction:(id)sender
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Settings" bundle:nil];
    UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"SettingsViewController"];
    viewController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:viewController animated:YES];
}

#pragma mark - Custom Methods
-(void)getMessageList
{
    [SVProgressHUD show];
    
    if(isLoading) return;
    isLoading = YES;
    limit = 12;
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setValue:@([shareObject integerForKey:@"user_id"]) forKey:@"user_id"];
    [parameters setValue:@(offset) forKey:@"offset"];
    [parameters setValue:@(limit) forKey:@"limit"];
    
    [[WebServiceManager sharedManager] initWithBaseURL:@"api/getUserNotificationMessages" parameters:parameters success:^(id result) {
        
        if ([[result valueForKey:@"success"] boolValue] == false) {
            [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:[result objectForKey:@"message"] input:nil];
        }else{
            
            if(offset == 0) [messageList removeAllObjects];
            NSLog(@"%@",parameters);
            NSMutableArray *list = (NSMutableArray *)[result valueForKey:@"list"];
            if(list.count > 0)
            {
                if(offset == 0)
                {
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        [self.tblView.pullToRefreshView stopAnimating];
                        [self.tblView.infiniteScrollingView stopAnimating];
                    });
                }
                
                self.tblView.showsInfiniteScrolling =YES;
                [messageList addObjectsFromArray:list];
                offset += limit;
            }else {
                self.tblView.showsInfiniteScrolling = NO;
            }
            
            [self.tblView reloadData];
            [self.tblView.pullToRefreshView stopAnimating];
            [self.tblView.infiniteScrollingView stopAnimating];
            [list removeAllObjects];
            list = result = nil;
        }
        
        isLoading = NO;
        [SVProgressHUD dismiss];
        
    } failure:^(NSError *error) {
        
        [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:LocalizedString(@"error_unknown", nil) input:nil];
        
        isLoading = NO;
        [self.tblView.pullToRefreshView stopAnimating];
        [self.tblView.infiniteScrollingView stopAnimating];
        
        [SVProgressHUD dismiss];
    }];
}

@end
