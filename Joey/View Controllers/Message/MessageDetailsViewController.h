//
//  MessageDetailsViewController.h
//  Joey
//
//  Created by webwerks on 2/8/18.
//  Copyright © 2018 Auxilia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface MessageDetailsViewController : BaseViewController

@property (weak, nonatomic) IBOutlet UIImageView *imgIcon;
@property (weak, nonatomic) IBOutlet UILabel *lblMessageTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblMessageDetails;
@property (weak, nonatomic) IBOutlet UILabel *lblMessageDateTime;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imgIconWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imgIconHeight;

@end
