//
//  MessageDetailsViewController.m
//  Joey
//
//  Created by webwerks on 2/8/18.
//  Copyright © 2018 Auxilia. All rights reserved.
//

#import "MessageDetailsViewController.h"
#import "AppDelegate.h"

@interface MessageDetailsViewController ()
{
    NSUserDefaults *shareObject;
    AppDelegate *appDelegate;
    NSMutableDictionary *messageDict;
    
}
@end

@implementation MessageDetailsViewController

#pragma mark - View Controller Methods
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    shareObject = [NSUserDefaults standardUserDefaults];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];

    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:UIColorFromRGB(0x8D8082), NSForegroundColorAttributeName, [UIFont fontWithName:@"SFUIDisplay-Medium" size:18.0f], NSFontAttributeName, nil]];
    [self.navigationController.navigationBar setBarTintColor:UIColorFromRGB(0xEEE8E4)];
    [self.navigationController.navigationBar setTranslucent:NO];
    
    [self.navigationItem setTitle:LocalizedString(@"Message", nil)];
    [self customBackButton];
    
    //messageDict = [[NSMutableDictionary alloc]initWithDictionary: [self.data valueForKey:@"message_Dict"]];
    [self setUpMessageDetails];
    [self setReadMessage];
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:(BOOL)animated];
    [self.navigationController.navigationBar setBarTintColor:UIColorFromRGB(0xEEE8E4)];
    NSLog(@"here=%@", self.data);
}

#pragma mark -  View's Data Configuration methods
-(void)setUpMessageDetails
{
    NSString *name = [self.data objectForKey:@"name"];
    UIImage *imgIcon = [UIImage imageNamed:[NSString stringWithFormat:@"icon_%@.png", name]];
    
    if ([name isEqualToString:@"birthday"] || [name isEqualToString:@"reminder"])
    {
        imgIcon = [UIImage imageNamed:[NSString stringWithFormat:@"icon_%@.png", name]];
    }
    else if ([name isEqualToString:@"grow"] || [name isEqualToString:@"move"] || [name isEqualToString:@"iq"] || [name isEqualToString:@"health"] || [name isEqualToString:@"others"] || [name isEqualToString:@"pregnancy"])
    {
        imgIcon = [UIImage imageNamed:[NSString stringWithFormat:@"icon_%@_Selected.png", name]];
    }

    // Activities
    if([name isEqualToString:@"feed"]){
        self.imgIcon.backgroundColor = UIColorFromRGB(0xF4EA9E);
    }if([name isEqualToString:@"pump"]){
        self.imgIcon.backgroundColor = UIColorFromRGB(0xF9C99B);
    }if([name isEqualToString:@"sleep"]){
        self.imgIcon.backgroundColor = UIColorFromRGB(0xC5D7E1);
    }if([name isEqualToString:@"poo"]){
        self.imgIcon.backgroundColor = UIColorFromRGB(0xE0C5B8);
    }if([name isEqualToString:@"wee"]){
        self.imgIcon.backgroundColor = UIColorFromRGB(0xCADAA9);
    }if([name isEqualToString:@"medical"]){
        self.imgIcon.backgroundColor = UIColorFromRGB(0xF4D2D5);
    }
    
    // Milestones
    if([name isEqualToString:@"birthday"]){
        self.imgIcon.backgroundColor = UIColorFromRGB(0xB9CDDA);
    }if([name isEqualToString:@"grow"]){
        self.imgIcon.backgroundColor = UIColorFromRGB(0xF3E99F);
    }if([name isEqualToString:@"move"]){
        self.imgIcon.backgroundColor = UIColorFromRGB(0xFFC896);
    }if([name isEqualToString:@"iq"]){
        self.imgIcon.backgroundColor = UIColorFromRGB(0xC5DEAA);
    }if([name isEqualToString:@"health"]){
        self.imgIcon.backgroundColor = UIColorFromRGB(0xFACFD4);
    }if([name isEqualToString:@"others"]){
        self.imgIcon.backgroundColor = UIColorFromRGB(0xDFCCDC);
    }if ([name isEqualToString:@"pregnancy"]) {
        self.imgIcon.backgroundColor = UIColorFromRGB(0xDCC4E8);
    }

    self.imgIconWidth.constant = imgIcon.size.width ;
    self.imgIconHeight.constant = imgIcon.size.height;
    
    if (IS_IPAD) {
        self.lblMessageTitle.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.lblMessageDetails.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:19.0f];
        self.imgIconWidth.constant = self.imgIconWidth.constant + 20;
        self.imgIconHeight.constant  = self.imgIconHeight.constant + 20;
    }
    
    self.imgIcon.image = imgIcon;
    self.imgIcon.layer.cornerRadius = self.imgIconWidth.constant/2;
    self.imgIcon.contentMode = UIViewContentModeCenter;
    
    self.lblMessageTitle.text = [NSString stringWithFormat:@"%@",[self.data valueForKey:@"message"]];
    self.lblMessageDetails.text = @"";//Baby activity/milestone notifications If the timer is off/paused, the User can manually set the Duration value by scrolling on the page. If the timer is off/paused, the User can manually set the Duration value by scrolling on the page.";
    self.lblMessageDateTime.text = [NSString stringWithFormat:@"%@ %@",[self.data valueForKey:@"date"],[self.data valueForKey:@"time"]];
    [self.view layoutSubviews];
    
}

#pragma mark - UI Customization & Layout Hadling Methods
- (void)customBackButton
{
    if([self.navigationController.viewControllers count] > 1)
    {
        UIImage *imgBack = [UIImage imageNamed:@"nav_btn_back"];
        UIButton *btnBack = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnBack setImage:imgBack forState:UIControlStateNormal];
        btnBack.frame = CGRectMake(0, 0, imgBack.size.width, imgBack.size.height);
        [btnBack addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
        [btnBack setImageEdgeInsets:UIEdgeInsetsMake(0, -16, 0, 16)];
        UIBarButtonItem *backBarItem = [[UIBarButtonItem alloc] initWithCustomView:btnBack];
        self.navigationItem.leftBarButtonItem = backBarItem;
    }
}

#pragma mark - Custom Actions
- (void)backAction
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Custom Methods
-(void)setReadMessage
{
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setValue:@([shareObject integerForKey:@"user_id"]) forKey:@"user_id"];
    [parameters setValue:@"1" forKey:@"status"];
    [parameters setValue:[self.data valueForKey:@"id"] forKey:@"notification_id"];
    
    [[WebServiceManager sharedManager] initWithBaseURL:@"api/notificationMarkAsReadUnread" parameters:parameters success:^(id result) {
        
        if ([[result valueForKey:@"success"] boolValue] == false) {
            [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:[result objectForKey:@"message"] input:nil];
        }
        else{
            NSLog(@"Set Read");
        }
        
    } failure:^(NSError *error) {
        [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:LocalizedString(@"error_unknown", nil) input:nil];
    }];
    
}
@end
