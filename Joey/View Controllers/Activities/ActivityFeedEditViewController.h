//
//  ActivityFeedEditViewController.h
//  Joey
//
//  Created by csl on 5/1/2017.
//  Copyright © 2017 Auxilia. All rights reserved.
//

#import "BaseTableViewController.h"
#import <Photos/Photos.h>
#import <AVFoundation/AVFoundation.h>

@interface ActivityFeedEditViewController : BaseTableViewController <UINavigationControllerDelegate, UIImagePickerControllerDelegate, UICollectionViewDataSource, UICollectionViewDelegate>

@property (nonatomic, weak) IBOutlet UIBarButtonItem *btnSave;
@property (nonatomic, weak) IBOutlet UILabel *lblVolume;
@property (nonatomic, weak) IBOutlet UILabel *lblDate;
@property (nonatomic, weak) IBOutlet UILabel *txtDate;
@property (nonatomic, weak) IBOutlet UILabel *lblDuration;
@property (nonatomic, weak) IBOutlet UILabel *lblPhoto;

@property (nonatomic, weak) IBOutlet UILabel *lblNotes;
@property (nonatomic, weak) IBOutlet UITextView *txtNotes;
@property (nonatomic, weak) IBOutlet UIButton *btnDelete;

@property (nonatomic, weak) IBOutlet UIButton *btnPhoto1;
@property (nonatomic, weak) IBOutlet UIButton *btnPhoto2;
@property (nonatomic, weak) IBOutlet UIButton *btnPhoto3;

@property (nonatomic, weak) IBOutlet UILabel *lblSide;
@property (nonatomic, weak) IBOutlet UIButton *btnLeft;
@property (nonatomic, weak) IBOutlet UIButton *btnRight;
@property (nonatomic, weak) IBOutlet UIPickerView *durationPicker;
@property (nonatomic, weak) IBOutlet UIPickerView *volumeValuePicker;
@property (nonatomic, weak) IBOutlet UIPickerView *volumeUnitPicker;

@property (nonatomic, weak) IBOutlet UILabel *lblDurationUnit;

@property (nonatomic, weak) IBOutlet UICollectionView *feedSelectionCollectionView;

// Keywords View
@property (nonatomic, weak) IBOutlet UIButton *btnaddKeyword;

@property (weak, nonatomic) IBOutlet UIView *keywordView;
@property (nonatomic, weak) IBOutlet UILabel *keyword1;
@property (nonatomic, weak) IBOutlet UILabel *keyword2;
@property (nonatomic, weak) IBOutlet UILabel *keyword3;
@property (nonatomic, weak) IBOutlet UILabel *keyword4;
@property (nonatomic, weak) IBOutlet UILabel *keyword5;
@property (nonatomic, weak) IBOutlet UILabel *keyword6;
@property (nonatomic, weak) IBOutlet UILabel *keyword7;
@property (nonatomic, weak) IBOutlet UILabel *keyword8;
@property (nonatomic, weak) IBOutlet UILabel *keyword9;

@property (nonatomic, weak) IBOutlet UIImageView *upArrow2;
@property (nonatomic, weak) IBOutlet UIImageView *downArrow2;

@end
