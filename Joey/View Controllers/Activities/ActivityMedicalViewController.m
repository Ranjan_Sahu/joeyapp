//
//  ActivityMedicalViewController.m
//  Joey
//
//  Created by csl on 4/1/2017.
//  Copyright © 2017 Auxilia. All rights reserved.
//

#import "AppDelegate.h"
#import "ActivityMedicalViewController.h"
#import "ActivityMedicalDayChartTableViewCell.h"
#import "ActivityMedicalWeekChartTableViewCell.h"
#import "ActivityMedicalMonthChartTableViewCell.h"
#import "ActivityTableViewCell.h"
#import "ChartAxisValueFormatter.h"
#import "PhotoViewController.h"

@interface ActivityMedicalViewController ()
{
    NSUserDefaults *shareObject;
    AppDelegate *appDelegate;
    NSDateFormatter *formatter;
    NSMutableArray *activityList;
    NSMutableArray *weekList;
    NSMutableArray *weekValueList;
    NSMutableArray *monthList;
    NSMutableArray *monthValueList;
    NSMutableDictionary *unitInfo;
    NSMutableDictionary *chartInfo;
    NSString *selectedHeader;
    NSDate *selectedDay;
    NSInteger selectedWeek;
    NSInteger selectedMonth;
    BOOL saveCheck;
    BOOL isLoading;
    int selectedIndex;
}
@end

@implementation ActivityMedicalViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    shareObject = [NSUserDefaults standardUserDefaults];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    formatter = [[NSDateFormatter alloc] init];
    [formatter setTimeZone:[NSTimeZone localTimeZone]];
    [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
    
    activityList = [[NSMutableArray alloc] init];
    unitInfo = (NSMutableDictionary *)[NSKeyedUnarchiver unarchiveObjectWithData:[shareObject objectForKey:@"unit_info"]];
    selectedIndex = [[self.data objectForKey:@"selected_index"] intValue];
    selectedDay = [NSDate date];
    
    weekList = [[NSMutableArray alloc] init];
    weekValueList = [[NSMutableArray alloc] init];
    NSDate *firstDayOfWeek = [self getFirstDayOfTheWeekFromDate:[NSDate date]];
    for(int i=0; i<=100; i++)
    {
        NSDate *newDate = [firstDayOfWeek dateByAddingTimeInterval:-i*7*24*60*60];
        int diff = [newDate timeIntervalSinceDate:[NSDate date]]/60;
        if(diff <= 0)
        {
            [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
            formatter.dateFormat = @"yyyyww";
            NSString *week = [formatter stringFromDate:newDate];
            [weekValueList addObject:week];
            
            if([[shareObject objectForKey:@"language"] hasPrefix:@"zh"])
            {
                [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"zh_HK"]];
                [formatter setDateStyle:NSDateFormatterMediumStyle];
                [formatter setTimeStyle:NSDateFormatterShortStyle];
                
                NSString *weekString = [NSString stringWithFormat:@"%@, %@", [NSString stringWithFormat:LocalizedString(@"txt_week_no", nil), [[week substringWithRange:NSMakeRange(4,2)] intValue]], [[formatter stringFromDate:newDate] componentsSeparatedByString:@" "][0]];
                
                [weekList addObject:weekString];
            }
            else
            {
                [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
                formatter.dateFormat = @"ww, dd MMM yyyy";
                [weekList addObject:[NSString stringWithFormat:LocalizedString(@"txt_week_no", nil), [formatter stringFromDate:newDate]]];
            }
            
            
            
            if([[self.data objectForKey:@"selected_week"] isEqualToString:week])
            {
                selectedWeek = i;
            }
        }
    }
    
    monthList = [[NSMutableArray alloc] init];
    monthValueList = [[NSMutableArray alloc] init];
    formatter.dateFormat = @"yyyy";
    int year = [[formatter stringFromDate:[NSDate date]] intValue];
    formatter.dateFormat = @"M";
    int month = [[formatter stringFromDate:[NSDate date]] intValue];
    for(int i=year; i>=year-3; i--)
    {
        for(int j=month; j>=1; j--)
        {
            formatter.dateFormat = @"yyyy-MM-dd";
            NSDate *newDate = [formatter dateFromString:[NSString stringWithFormat:@"%d-%02d-01", i, j]];
            
            if([[shareObject objectForKey:@"language"] hasPrefix:@"zh"])
            {
                [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"zh_HK"]];
                [formatter setDateStyle:NSDateFormatterMediumStyle];
                [formatter setTimeStyle:NSDateFormatterShortStyle];
                
                [monthList addObject:[NSString stringWithFormat:@"%@月", [[formatter stringFromDate:newDate] componentsSeparatedByString:@"月"][0]]];
            }
            else
            {
                formatter.dateFormat = @"MMMM yyyy";
                [monthList addObject:[formatter stringFromDate:newDate]];
            }
            
            formatter.dateFormat = @"yyyyMM";
            NSString *month = [formatter stringFromDate:newDate];
            [monthValueList addObject:month];
            
            if([[self.data objectForKey:@"selected_month"] isEqualToString:month])
            {
                selectedMonth = monthValueList.count-1;
            }
        }
        month = 12;
    }
    
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:UIColorFromRGB(0x8D8082), NSForegroundColorAttributeName, [UIFont fontWithName:@"SFUIDisplay-Medium" size:18.0f], NSFontAttributeName, nil]];
    [self.navigationController.navigationBar setBarTintColor:UIColorFromRGB(0xF4D2D5)];
    [self.navigationController.navigationBar setTranslucent:NO];
    [self.navigationItem setTitle:LocalizedString(@"title_medical", nil)];
    [self customBackButton];
    
    self.btnAdd.title = LocalizedString(@"btn_add", nil);
    
    self.btnDay.layer.cornerRadius = 3.f;
    self.btnWeek.layer.cornerRadius = 3.f;
    self.btnMonth.layer.cornerRadius = 3.f;
    [self.btnDay setTitle:LocalizedString(@"btn_day", nil) forState:UIControlStateNormal];
    [self.btnWeek setTitle:LocalizedString(@"btn_week", nil) forState:UIControlStateNormal];
    [self.btnMonth setTitle:LocalizedString(@"btn_month", nil) forState:UIControlStateNormal];
    
    if(selectedIndex == 0)
    {
        [self.btnDay setBackgroundColor:UIColorFromRGB(0x8D8082)];
        [self.btnWeek setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
        [self.btnMonth setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    }
    else if(selectedIndex == 1)
    {
        [self.btnDay setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
        [self.btnWeek setBackgroundColor:UIColorFromRGB(0x8D8082)];
        [self.btnMonth setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    }
    else if(selectedIndex == 2)
    {
        [self.btnDay setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
        [self.btnWeek setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
        [self.btnMonth setBackgroundColor:UIColorFromRGB(0x8D8082)];
    }
    
    __weak __typeof(self)weakSelf = self;
    [self.tableView addPullToRefreshWithActionHandler:^{
        [weakSelf refreshAction];
    }];
    [self.tableView addInfiniteScrollingWithActionHandler:^{
        [weakSelf getActivityList];
    }];
    
    [self refreshAction];
    
    if (IS_IPAD) {
        [_btnDay.titleLabel setFont:[UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f]];
        [_btnWeek.titleLabel setFont:[UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f]];
        [_btnMonth.titleLabel setFont:[UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f]];
    }

}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:(BOOL)animated];
    
    NSLog(@"here=%@", self.data);
    NSLog(@"updateActivityCheck=%@", appDelegate.updateActivityCheck?@"YES":@"NO");
    if([[self.data objectForKey:@"save_check"] boolValue] || appDelegate.updateActivityCheck)
    {
        [self.data removeAllObjects];
        [self refreshAction];
    }
}

- (void)viewDidLayoutSubviews
{
    /*if(self.scrollView.contentSize.width == 0)
     {
     self.scrollView.contentSize = CGSizeMake(self.view.frame.size.width*3, 1);
     self.scrollView.contentOffset = CGPointMake(selectedIndex*self.view.frame.size.width, self.scrollView.contentOffset.y);
     }*/
}

- (void)customBackButton
{
    if([self.navigationController.viewControllers count] > 1)
    {
        UIImage *imgBack = [UIImage imageNamed:@"nav_btn_back"];
        UIButton *btnBack = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnBack setImage:imgBack forState:UIControlStateNormal];
        btnBack.frame = CGRectMake(0, 0, imgBack.size.width, imgBack.size.height);
        [btnBack addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
        [btnBack setImageEdgeInsets:UIEdgeInsetsMake(0, -16, 0, 16)];
        UIBarButtonItem *backBarItem = [[UIBarButtonItem alloc] initWithCustomView:btnBack];
        self.navigationItem.leftBarButtonItem = backBarItem;
    }
}

- (void)backAction
{
    UIViewController *viewController = [self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count-2];
    viewController.data = [[NSMutableDictionary alloc] initWithDictionary:@{@"save_check":@(saveCheck)}];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)refreshAction
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    [COMMON_HELPER hideSpinner];
    isLoading = NO;
    self.tableView.showsInfiniteScrolling = NO;
    [self getActivityList];
}

- (void)getActivityList
{
    [COMMON_HELPER showSpinner];
    if(isLoading) return;
    isLoading = YES;
    
    if(selectedIndex == 0)
    {
        formatter.dateFormat = @"yyyy-MM-dd";
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setValue:@([shareObject integerForKey:@"user_id"]) forKey:@"user_id"];
        [parameters setValue:@([shareObject integerForKey:@"baby_id"]) forKey:@"baby_id"];
        [parameters setValue:[formatter stringFromDate:selectedDay] forKey:@"day"];
        
        [[WebServiceManager sharedManager] initWithBaseURL:@"api/getMedicalActivityListByDay" parameters:parameters success:^(id result) {
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                NSIndexPath *top = [NSIndexPath indexPathForRow:0 inSection:0];
                [self.tableView scrollToRowAtIndexPath:top atScrollPosition:UITableViewScrollPositionTop animated:YES];
            });
            
            NSString *dateString = [result objectForKey:@"header2"];
            [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
            formatter.dateFormat = @"yyyy-MM-dd";
            NSDate *date = [formatter dateFromString:dateString];
            
            if([[shareObject objectForKey:@"language"] hasPrefix:@"zh"])
            {
                [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"zh_HK"]];
                [formatter setDateStyle:NSDateFormatterMediumStyle];
                [formatter setTimeStyle:NSDateFormatterShortStyle];
                
                selectedHeader = [[NSString stringWithFormat:@"%@", [formatter stringFromDate:date]] componentsSeparatedByString:@" "][0];
            }
            else
            {
                [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
                formatter.dateFormat = @"EE d MMM, yyyy";
                selectedHeader = [NSString stringWithFormat:@"%@", [formatter stringFromDate:date]];
            }
            //selectedHeader = [result objectForKey:@"header"];
            activityList = (NSMutableArray *)[result objectForKey:@"list"];
            chartInfo = (NSMutableDictionary *)[result objectForKey:@"info"];
            
            for(int i=0; i<activityList.count; i++)
            {
                for(int j=0; j<[[[activityList objectAtIndex:i] objectForKey:@"list"] count]; j++)
                {
                    if([[[[[activityList objectAtIndex:i] objectForKey:@"list"] objectAtIndex:j] objectForKey:@"type"] containsString:@"med"])
                    {
                        //[[[[activityList objectAtIndex:i] objectForKey:@"list"] objectAtIndex:j] setObject:[self convertUnit:[[[[activityList objectAtIndex:i] objectForKey:@"list"] objectAtIndex:j] objectForKey:@"attribute_value"] unit:[unitInfo objectForKey:@"medicine"] spacing:YES] forKey:@"attribute_value"];
                    }
                    
                    if([[[[[activityList objectAtIndex:i] objectForKey:@"list"] objectAtIndex:j] objectForKey:@"type"] containsString:@"temperature"])
                    {
                        [[[[activityList objectAtIndex:i] objectForKey:@"list"] objectAtIndex:j] setObject:[self convertUnit:[[[[activityList objectAtIndex:i] objectForKey:@"list"] objectAtIndex:j] objectForKey:@"attribute_value"] unit:[unitInfo objectForKey:@"temperature"] spacing:YES] forKey:@"attribute_value"];
                    }
                }
            }

            //[chartInfo setObject:@([self convertUnit:[[chartInfo objectForKey:@"med_max"] floatValue] unit:@"ml" newUnit:[unitInfo objectForKey:@"medicine"]]) forKey:@"med_max"];
            [chartInfo setObject:@([self convertUnit:[[chartInfo objectForKey:@"temp_max"] floatValue] unit:@"°C" newUnit:[unitInfo objectForKey:@"temperature"]]) forKey:@"temp_max"];
            [chartInfo setObject:@([self convertUnit:[[chartInfo objectForKey:@"temp_min"] floatValue] unit:@"°C" newUnit:[unitInfo objectForKey:@"temperature"]]) forKey:@"temp_min"];
            
            //[chartInfo setObject:[self convertUnit:[chartInfo objectForKey:@"med1_total"] unit:[unitInfo objectForKey:@"medicine"] spacing:YES] forKey:@"med1_total"];
            //[chartInfo setObject:[self convertUnit:[chartInfo objectForKey:@"med2_total"] unit:[unitInfo objectForKey:@"medicine"] spacing:YES] forKey:@"med2_total"];
            //[chartInfo setObject:[self convertUnit:[chartInfo objectForKey:@"med3_total"] unit:[unitInfo objectForKey:@"medicine"] spacing:YES] forKey:@"med3_total"];
            //[chartInfo setObject:[self convertUnit:[chartInfo objectForKey:@"med4_total"] unit:[unitInfo objectForKey:@"medicine"] spacing:YES] forKey:@"med4_total"];
            NSLog(@"chartInfo=%@", chartInfo);
            
            isLoading = NO;
            self.tableView.hidden = NO;
            [self.tableView reloadData];
            [self.tableView.pullToRefreshView stopAnimating];
            result = nil;
            [COMMON_HELPER hideSpinner];
        } failure:^(NSError *error) {
            isLoading = NO;
            [self.tableView.pullToRefreshView stopAnimating];
            [COMMON_HELPER hideSpinner];
        }];
    }
    else if(selectedIndex == 1)
    {
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setValue:@([shareObject integerForKey:@"user_id"]) forKey:@"user_id"];
        [parameters setValue:@([shareObject integerForKey:@"baby_id"]) forKey:@"baby_id"];
        [parameters setValue:[weekValueList objectAtIndex:selectedWeek] forKey:@"week"];
        
        [[WebServiceManager sharedManager] initWithBaseURL:@"api/getMedicalActivityListByWeek" parameters:parameters success:^(id result) {
            
            NSString *dateString = [result objectForKey:@"header2"];
            [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
            formatter.dateFormat = @"yyyy-MM-dd";
            NSDate *date = [formatter dateFromString:dateString];
            
            NSDate *firstDayOfWeek = [self getFirstDayOfTheWeekFromDate:date];
            NSDate *endDayOfWeek = [firstDayOfWeek dateByAddingTimeInterval:6*24*60*60];
            
            NSDateComponents *components1 = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:firstDayOfWeek];
            NSDateComponents *components2 = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:endDayOfWeek];
            
            if([[shareObject objectForKey:@"language"] hasPrefix:@"zh"])
            {
                [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"zh_HK"]];
                [formatter setDateStyle:NSDateFormatterMediumStyle];
                [formatter setTimeStyle:NSDateFormatterShortStyle];
                
                NSString *start = [[formatter stringFromDate:firstDayOfWeek]  componentsSeparatedByString:@" "][0];
                NSString *end = [[formatter stringFromDate:endDayOfWeek] componentsSeparatedByString:@" "][0];
                selectedHeader = [NSString stringWithFormat:@"%@ - %@", start, end];
            }
            else
            {
                if([components1 year] == [components2 year])
                {
                    if([components1 month] == [components2 month])
                    {
                        [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
                        formatter.dateFormat = @"ww, dd";
                        NSString *start = [NSString stringWithFormat:LocalizedString(@"txt_week_no", nil), [formatter stringFromDate:firstDayOfWeek]];
                        formatter.dateFormat = @"-dd MMMM yyyy";
                        NSString *end = [formatter stringFromDate:endDayOfWeek];
                        selectedHeader = [NSString stringWithFormat:@"%@%@", start, end];
                    }
                    else
                    {
                        [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
                        formatter.dateFormat = @"ww, dd MMMM";
                        NSString *start = [NSString stringWithFormat:LocalizedString(@"txt_week_no", nil), [formatter stringFromDate:firstDayOfWeek]];
                        formatter.dateFormat = @" - dd MMMM yyyy";
                        NSString *end = [formatter stringFromDate:endDayOfWeek];
                        selectedHeader = [NSString stringWithFormat:@"%@%@", start, end];
                    }
                }
                else
                {
                    [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
                    formatter.dateFormat = @"ww, dd MMMM yyyy";
                    NSString *start = [NSString stringWithFormat:LocalizedString(@"txt_week_no", nil), [formatter stringFromDate:firstDayOfWeek]];
                    formatter.dateFormat = @" - dd MMMM yyyy";
                    NSString *end = [formatter stringFromDate:endDayOfWeek];
                    selectedHeader = [NSString stringWithFormat:@"%@%@", start, end];
                }
            }
            
            //selectedHeader = [result objectForKey:@"header"];
            activityList = (NSMutableArray *)[result objectForKey:@"list"];
            chartInfo = (NSMutableDictionary *)[result objectForKey:@"info"];
            
            for(int i=0; i<activityList.count; i++)
            {
                for(int j=0; j<[[[activityList objectAtIndex:i] objectForKey:@"list"] count]; j++)
                {
                    if([[[[[activityList objectAtIndex:i] objectForKey:@"list"] objectAtIndex:j] objectForKey:@"type"] containsString:@"med"])
                    {
//                        [[[[activityList objectAtIndex:i] objectForKey:@"list"] objectAtIndex:j] setObject:[self convertUnit:[[[[activityList objectAtIndex:i] objectForKey:@"list"] objectAtIndex:j] objectForKey:@"attribute_value"] unit:[unitInfo objectForKey:@"medicine"] spacing:YES] forKey:@"attribute_value"];
                    }
                    
                    if([[[[[activityList objectAtIndex:i] objectForKey:@"list"] objectAtIndex:j] objectForKey:@"type"] containsString:@"temperature"])
                    {
                        [[[[activityList objectAtIndex:i] objectForKey:@"list"] objectAtIndex:j] setObject:[self convertUnit:[[[[activityList objectAtIndex:i] objectForKey:@"list"] objectAtIndex:j] objectForKey:@"attribute_value"] unit:[unitInfo objectForKey:@"temperature"] spacing:YES] forKey:@"attribute_value"];
                    }
                }
            }
            
            [chartInfo setObject:@([self convertUnit:[[chartInfo objectForKey:@"temp_max"] floatValue] unit:@"°C" newUnit:[unitInfo objectForKey:@"temperature"]]) forKey:@"temp_max"];
            [chartInfo setObject:@([self convertUnit:[[chartInfo objectForKey:@"temp_min"] floatValue] unit:@"°C" newUnit:[unitInfo objectForKey:@"temperature"]]) forKey:@"temp_min"];
            
            //[chartInfo setObject:[self convertUnit:[chartInfo objectForKey:@"med1_total"] unit:[unitInfo objectForKey:@"medicine"] spacing:YES] forKey:@"med1_total"];
            //[chartInfo setObject:[self convertUnit:[chartInfo objectForKey:@"med2_total"] unit:[unitInfo objectForKey:@"medicine"] spacing:YES] forKey:@"med2_total"];
            //[chartInfo setObject:[self convertUnit:[chartInfo objectForKey:@"med3_total"] unit:[unitInfo objectForKey:@"medicine"] spacing:YES] forKey:@"med3_total"];
            //[chartInfo setObject:[self convertUnit:[chartInfo objectForKey:@"med4_total"] unit:[unitInfo objectForKey:@"medicine"] spacing:YES] forKey:@"med4_total"];
            //NSLog(@"chartInfo=%@", chartInfo);
            
            isLoading = NO;
            self.tableView.hidden = NO;
            [self.tableView reloadData];
            [self.tableView.pullToRefreshView stopAnimating];
            result = nil;
            [COMMON_HELPER hideSpinner];
        } failure:^(NSError *error) {
            isLoading = NO;
            [self.tableView.pullToRefreshView stopAnimating];
            [COMMON_HELPER hideSpinner];
        }];
    }
    else
    {
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setValue:@([shareObject integerForKey:@"user_id"]) forKey:@"user_id"];
        [parameters setValue:@([shareObject integerForKey:@"baby_id"]) forKey:@"baby_id"];
        [parameters setValue:[monthValueList objectAtIndex:selectedMonth] forKey:@"month"];
        
        [[WebServiceManager sharedManager] initWithBaseURL:@"api/getMedicalActivityListByMonth" parameters:parameters success:^(id result) {
            
            NSString *dateString = [result objectForKey:@"header2"];
            [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
            formatter.dateFormat = @"yyyy-MM";
            NSDate *date = [formatter dateFromString:dateString];
            
            if([[shareObject objectForKey:@"language"] hasPrefix:@"zh"])
            {
                [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"zh_HK"]];
                [formatter setDateStyle:NSDateFormatterMediumStyle];
                [formatter setTimeStyle:NSDateFormatterShortStyle];
                
                selectedHeader = [NSString stringWithFormat:@"%@月", [[formatter stringFromDate:date] componentsSeparatedByString:@"月"][0]];
            }
            else
            {
                [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
                formatter.dateFormat = @"MMMM yyyy";
                selectedHeader = [NSString stringWithFormat:@"%@", [formatter stringFromDate:date]];
            }
            //selectedHeader = [result objectForKey:@"header"];
            activityList = (NSMutableArray *)[result objectForKey:@"list"];
            chartInfo = (NSMutableDictionary *)[result objectForKey:@"info"];
            
            for(int i=0; i<activityList.count; i++)
            {
                for(int j=0; j<[[[activityList objectAtIndex:i] objectForKey:@"list"] count]; j++)
                {
                    if([[[[[activityList objectAtIndex:i] objectForKey:@"list"] objectAtIndex:j] objectForKey:@"type"] containsString:@"med"])
                    {
                        //[[[[activityList objectAtIndex:i] objectForKey:@"list"] objectAtIndex:j] setObject:[self convertUnit:[[[[activityList objectAtIndex:i] objectForKey:@"list"] objectAtIndex:j] objectForKey:@"attribute_value"] unit:[unitInfo objectForKey:@"medicine"] spacing:YES] forKey:@"attribute_value"];
                    }
                    
                    if([[[[[activityList objectAtIndex:i] objectForKey:@"list"] objectAtIndex:j] objectForKey:@"type"] containsString:@"temperature"])
                    {
                        [[[[activityList objectAtIndex:i] objectForKey:@"list"] objectAtIndex:j] setObject:[self convertUnit:[[[[activityList objectAtIndex:i] objectForKey:@"list"] objectAtIndex:j] objectForKey:@"attribute_value"] unit:[unitInfo objectForKey:@"temperature"] spacing:YES] forKey:@"attribute_value"];
                    }
                }
            }

            [chartInfo setObject:@([self convertUnit:[[chartInfo objectForKey:@"temp_max"] floatValue] unit:@"°C" newUnit:[unitInfo objectForKey:@"temperature"]]) forKey:@"temp_max"];
            [chartInfo setObject:@([self convertUnit:[[chartInfo objectForKey:@"temp_min"] floatValue] unit:@"°C" newUnit:[unitInfo objectForKey:@"temperature"]]) forKey:@"temp_min"];
            
            //[chartInfo setObject:[self convertUnit:[chartInfo objectForKey:@"med1_total"] unit:[unitInfo objectForKey:@"medicine"] spacing:YES] forKey:@"med1_total"];
            //[chartInfo setObject:[self convertUnit:[chartInfo objectForKey:@"med2_total"] unit:[unitInfo objectForKey:@"medicine"] spacing:YES] forKey:@"med2_total"];
            //[chartInfo setObject:[self convertUnit:[chartInfo objectForKey:@"med3_total"] unit:[unitInfo objectForKey:@"medicine"] spacing:YES] forKey:@"med3_total"];
            //[chartInfo setObject:[self convertUnit:[chartInfo objectForKey:@"med4_total"] unit:[unitInfo objectForKey:@"medicine"] spacing:YES] forKey:@"med4_total"];
            //NSLog(@"chartInfo=%@", chartInfo);
            
            isLoading = NO;
            self.tableView.hidden = NO;
            [self.tableView reloadData];
            [self.tableView.pullToRefreshView stopAnimating];
            result = nil;
            [COMMON_HELPER hideSpinner];
        } failure:^(NSError *error) {
            isLoading = NO;
            [self.tableView.pullToRefreshView stopAnimating];
            [COMMON_HELPER hideSpinner];
        }];
    }
}

- (void)getDatePickerAction
{
    NSDate *minimumDate = [[NSDate date] dateByAddingTimeInterval:-365*24*60*60];
    NSDate *maximumDate = [NSDate date];
    [ActionSheetDatePicker showPickerWithTitle:@"" datePickerMode:UIDatePickerModeDate selectedDate:selectedDay minimumDate:minimumDate maximumDate:maximumDate doneBlock:^(ActionSheetDatePicker *picker, id selectedDate, id origin) {
        
        selectedDay = selectedDate;
        formatter.dateFormat = @"d MMM yyyy";
        selectedHeader = [formatter stringFromDate:selectedDay];
        
        [self refreshAction];
        
    } cancelBlock:^(ActionSheetDatePicker *picker) {
        
    } origin:self.view];
}

- (void)getWeekPickerAction
{
    [ActionSheetStringPicker showPickerWithTitle:@"" rows:weekList initialSelection:selectedWeek doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex2, id selectedValue) {
        
        selectedWeek = selectedIndex2;
        
        formatter.dateFormat = @"d MMM yyyy";
        selectedHeader = [formatter stringFromDate:selectedDay];
        
        [self refreshAction];
        
    } cancelBlock:^(ActionSheetStringPicker *picker) {
        
    } origin:self.view];
}

- (void)getMonthPickerAction
{
    [ActionSheetStringPicker showPickerWithTitle:@"" rows:monthList initialSelection:selectedMonth doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex2, id selectedValue) {
        
        selectedMonth = selectedIndex2;
        formatter.dateFormat = @"d MMM yyyy";
        selectedHeader = [formatter stringFromDate:selectedDay];
        
        [self refreshAction];
        
    } cancelBlock:^(ActionSheetStringPicker *picker) {
        
    } origin:self.view];
}

- (void)editMedAction:(UIButton *)btn
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:LocalizedString(@"title_edit_medicine_name", nil) message:nil preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *btnOk = [UIAlertAction actionWithTitle:LocalizedString(@"btn_ok", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        [SVProgressHUD show];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setValue:@([shareObject integerForKey:@"user_id"]) forKey:@"user_id"];
        [parameters setValue:@([shareObject integerForKey:@"baby_id"]) forKey:@"baby_id"];
        [parameters setValue:@"medical" forKey:@"name"];
        [parameters setValue:[NSString stringWithFormat:@"med%d", (int)btn.tag] forKey:@"type"];
        [parameters setValue:alert.textFields[0].text forKey:@"medicine"];
        
        if(selectedIndex == 0)
        {
            formatter.dateFormat = @"yyyy-MM-dd";
            [parameters setValue:[formatter stringFromDate:selectedDay] forKey:@"day"];
            
            [[WebServiceManager sharedManager] initWithBaseURL:@"api/editMedicineNameByDay" parameters:parameters success:^(id result) {
                
                if([[result objectForKey:@"success"] boolValue])
                {
                    NSMutableArray *babyList = (NSMutableArray *)[NSKeyedUnarchiver unarchiveObjectWithData:[shareObject objectForKey:@"baby_list"]];
                    [[babyList objectAtIndex:[shareObject integerForKey:@"baby_index"]] setObject:[result valueForKey:@"info"] forKey:@"medical_info"];
                    [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:babyList] forKey:@"baby_list"];
                    [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[result valueForKey:@"info"]] forKey:@"medical_info"];
                    [shareObject synchronize];
                    
                    [self refreshAction];
                }
                
            } failure:^(NSError *error) {
            }];
        }
        else if(selectedIndex == 1)
        {
            [parameters setValue:[weekValueList objectAtIndex:selectedWeek] forKey:@"week"];
            
            [[WebServiceManager sharedManager] initWithBaseURL:@"api/editMedicineNameByWeek" parameters:parameters success:^(id result) {
                
                if([[result objectForKey:@"success"] boolValue])
                {
                    NSMutableArray *babyList = (NSMutableArray *)[NSKeyedUnarchiver unarchiveObjectWithData:[shareObject objectForKey:@"baby_list"]];
                    [[babyList objectAtIndex:[shareObject integerForKey:@"baby_index"]] setObject:[result valueForKey:@"info"] forKey:@"medical_info"];
                    [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:babyList] forKey:@"baby_list"];
                    [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[result valueForKey:@"info"]] forKey:@"medical_info"];
                    [shareObject synchronize];
                    
                    [self refreshAction];
                }
                
            } failure:^(NSError *error) {
            }];
        }
        else
        {
            [parameters setValue:[monthValueList objectAtIndex:selectedMonth] forKey:@"month"];
            
            [[WebServiceManager sharedManager] initWithBaseURL:@"api/editMedicineNameByMonth" parameters:parameters success:^(id result) {
                
                if([[result objectForKey:@"success"] boolValue])
                {
                    NSMutableArray *babyList = (NSMutableArray *)[NSKeyedUnarchiver unarchiveObjectWithData:[shareObject objectForKey:@"baby_list"]];
                    [[babyList objectAtIndex:[shareObject integerForKey:@"baby_index"]] setObject:[result valueForKey:@"info"] forKey:@"medical_info"];
                    [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:babyList] forKey:@"baby_list"];
                    [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[result valueForKey:@"info"]] forKey:@"medical_info"];
                    [shareObject synchronize];
                    
                    [self refreshAction];
                }
                
            } failure:^(NSError *error) {
            }];
        }
        
        [alert dismissViewControllerAnimated:YES completion:nil];
    }];
    
    UIAlertAction *btnCancel = [UIAlertAction actionWithTitle:LocalizedString(@"btn_cancel", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        [alert dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.autocapitalizationType = UITextAutocapitalizationTypeWords;
    }];
    [alert addAction:btnCancel];
    [alert addAction:btnOk];
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (IBAction)getDayListAction:(id)sender
{
    selectedIndex = 0;
    selectedDay = [NSDate date];
    
    [self.btnDay setBackgroundColor:UIColorFromRGB(0x8D8082)];
    [self.btnWeek setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    
    [self refreshAction];
}

- (void)showPhotoAction:(UIButton *)button
{
    CGPoint buttonPosition = [button convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
    
    NSString *videoUrl = [[[[activityList objectAtIndex:indexPath.section-1] objectForKey:@"list"] objectAtIndex:indexPath.row] objectForKey:@"video_url"];
    NSString *videoUrl2 = [[[[activityList objectAtIndex:indexPath.section-1] objectForKey:@"list"] objectAtIndex:indexPath.row] objectForKey:@"video_url2"];
    NSString *videoUrl3 = [[[[activityList objectAtIndex:indexPath.section-1] objectForKey:@"list"] objectAtIndex:indexPath.row] objectForKey:@"video_url3"];
    NSString *photoUrl = [[[[activityList objectAtIndex:indexPath.section-1] objectForKey:@"list"] objectAtIndex:indexPath.row] objectForKey:@"photo_url"];
    NSString *photoUrl2 = [[[[activityList objectAtIndex:indexPath.section-1] objectForKey:@"list"] objectAtIndex:indexPath.row] objectForKey:@"photo_url2"];
    NSString *photoUrl3 = [[[[activityList objectAtIndex:indexPath.section-1] objectForKey:@"list"] objectAtIndex:indexPath.row] objectForKey:@"photo_url3"];
    
    NSMutableArray *mUrls = [[NSMutableArray alloc] init];
    
    if (photoUrl && ![photoUrl isKindOfClass:[NSNull class]] && photoUrl.length>0) {
        [mUrls addObject:photoUrl];
    }
    if (photoUrl2 && ![photoUrl2 isKindOfClass:[NSNull class]] && photoUrl2.length>0) {
        [mUrls addObject:photoUrl2];
    }
    if (photoUrl3 && ![photoUrl3 isKindOfClass:[NSNull class]]  && photoUrl3.length>0) {
        [mUrls addObject:photoUrl3];
    }
    if (videoUrl && ![videoUrl isKindOfClass:[NSNull class]]  && videoUrl.length>0) {
        [mUrls addObject:videoUrl];
    }
    if (videoUrl2 && ![videoUrl2 isKindOfClass:[NSNull class]]   && videoUrl2.length>0) {
        [mUrls addObject:videoUrl2];
    }
    if (videoUrl3 && ![videoUrl3 isKindOfClass:[NSNull class]]   && videoUrl3.length>0) {
        [mUrls addObject:videoUrl3];
    }
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Settings" bundle:nil];
    UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"MediaViewerViewController"];
    viewController.data = [[NSMutableDictionary alloc] initWithDictionary:@{@"viewController":self, @"urls":mUrls,@"view_check":@(YES)}];
    [self presentViewController:viewController animated:YES completion:nil];

//    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    PhotoViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"PhotoViewController"];
//    viewController.data = [[NSMutableDictionary alloc] initWithDictionary:@{@"viewController":self, @"image":button.imageView.image, @"view_check":@(YES)}];
//
//    NSMutableArray *ogImgArray = [NSMutableArray new];
//    CGPoint buttonPosition = [button convertPoint:CGPointZero toView:self.tableView];
//    NSIndexPath *selectedIndexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
//    NSDictionary *selectedRecordDict = [[[activityList objectAtIndex:selectedIndexPath.section-1] objectForKey:@"list"] objectAtIndex:selectedIndexPath.row];
//
//    NSString *photoUrl = @"";
//    photoUrl = [selectedRecordDict objectForKey:@"photo_url"];
//    if (![[selectedRecordDict objectForKey:@"photo_url"] isKindOfClass:[NSNull class]] && photoUrl.length>0) {
//        [ogImgArray addObject:photoUrl];
//    }
//
//    photoUrl = @"";
//    photoUrl = [selectedRecordDict objectForKey:@"photo_url2"];
//    if (![[selectedRecordDict objectForKey:@"photo_url2"] isKindOfClass:[NSNull class]] && photoUrl.length>0) {
//        [ogImgArray addObject:photoUrl];
//    }
//
//    photoUrl = @"";
//    photoUrl = [selectedRecordDict objectForKey:@"photo_url3"];
//    if (![[selectedRecordDict objectForKey:@"photo_url3"] isKindOfClass:[NSNull class]] && photoUrl.length>0) {
//        [ogImgArray addObject:photoUrl];
//    }
//
//    if (ogImgArray.count>0) {
//        viewController.originalImgArray = ogImgArray;
//        viewController.index = 0;
//        viewController.hidesBottomBarWhenPushed = YES;
//        viewController.isMultiple = YES;
//    }
//
//    [self presentViewController:viewController animated:YES completion:nil];
}

- (IBAction)getWeekListAction:(id)sender
{
    selectedIndex = 1;
    selectedWeek = 0;
    
    [self.btnDay setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnWeek setBackgroundColor:UIColorFromRGB(0x8D8082)];
    [self.btnMonth setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    
    [self refreshAction];
}

- (IBAction)getMonthListAction:(id)sender
{
    selectedIndex = 2;
    selectedMonth = 0;
    
    [self.btnDay setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnWeek setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth setBackgroundColor:UIColorFromRGB(0x8D8082)];
    
    [self refreshAction];
}

- (IBAction)addAction:(id)sender
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PooWeeMedStoryboard" bundle:nil];
    UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"ActivityMedicalAddViewController"];
    [self.navigationController pushViewController:viewController animated:YES];
}


#pragma mark - Table view data source
#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if(isLoading) return 0;
    return activityList.count+1;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if(section > 0)
    {
        NSString *dateString = [[activityList objectAtIndex:section-1] objectForKey:@"header"];
        NSLog(@"dateString=%@", dateString);
        
        [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
        formatter.dateFormat = @"yyyy-MM-dd";
        NSDate *date = [formatter dateFromString:dateString];
        
        if([[shareObject objectForKey:@"language"] hasPrefix:@"zh"])
        {
            [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"zh_HK"]];
            [formatter setDateStyle:NSDateFormatterMediumStyle];
            [formatter setTimeStyle:NSDateFormatterShortStyle];
            
            return [[NSString stringWithFormat:@"%@", [formatter stringFromDate:date]] componentsSeparatedByString:@" "][0];
        }
        else
        {
            [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
            formatter.dateFormat = @"EE d MMM, yyyy";
            return [NSString stringWithFormat:@"%@", [formatter stringFromDate:date]];
        }
    }
    return nil;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if(section == 0)
    {
        CGFloat headerHeight = 40;
        if (IS_IPAD) {
            headerHeight = 48;
        }
        UIButton *btnHeader = [[UIButton alloc] initWithFrame:CGRectMake(10, 0, self.tableView.bounds.size.width-20, headerHeight)];
        btnHeader.backgroundColor = [UIColorFromRGB(0xF4D2D5) colorWithAlphaComponent:0.8f];
        btnHeader.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        btnHeader.layer.cornerRadius = 3.f;
        [btnHeader setTitleEdgeInsets:UIEdgeInsetsMake(0, 10, 0, 0)];
        [btnHeader setTitle:selectedHeader forState:UIControlStateNormal];
        [btnHeader setTitleColor:UIColorFromRGB(0x8D8082) forState:UIControlStateNormal];
        [btnHeader.titleLabel setFont:[UIFont fontWithName:@"SFUIDisplay-Medium" size:15.0f]];
        if (IS_IPAD) {
            [btnHeader.titleLabel setFont:[UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f]];
        }
        
        if(selectedIndex == 0)
        {
            [btnHeader addTarget:self action:@selector(getDatePickerAction) forControlEvents:UIControlEventTouchUpInside];
        }
        else if(selectedIndex == 1)
        {
            [btnHeader addTarget:self action:@selector(getWeekPickerAction) forControlEvents:UIControlEventTouchUpInside];
        }
        else
        {
            [btnHeader addTarget:self action:@selector(getMonthPickerAction) forControlEvents:UIControlEventTouchUpInside];
        }
        
        UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, headerHeight)];
        headerView.backgroundColor = [UIColor whiteColor];
        [headerView addSubview:btnHeader];
        return headerView;
    }
    else
    {
        CGFloat headerHeight = 30;
        if (IS_IPAD) {
            headerHeight = 40;
        }

        NSString *header = [[activityList objectAtIndex:section-1] objectForKey:@"date"];
        if(header.length > 0)
        {
            UILabel *txtHeader = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, tableView.bounds.size.width-20, headerHeight)];
            txtHeader.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:15.0f];
            if (IS_IPAD) {
                txtHeader.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f];
            }
            txtHeader.textColor = UIColorFromRGB(0x8D8082);
            txtHeader.text = [self tableView:tableView titleForHeaderInSection:section];
            
            UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, headerHeight)];
            headerView.backgroundColor = UIColorFromRGB(0xEEE8E4);
            [headerView addSubview:txtHeader];
            return headerView;
        }
    }
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (IS_IPAD) {
        if(section == 0) return 48;
        else
        {
            NSString *header = [[activityList objectAtIndex:section-1] objectForKey:@"date"];
            if(header.length == 0) return 0;
        }
        return 40;
    }
    else {
        if(section == 0) return 40;
        else
        {
            NSString *header = [[activityList objectAtIndex:section-1] objectForKey:@"date"];
            if(header.length == 0) return 0;
        }
        return 30;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(section == 0) return 1;
    else return [[[activityList objectAtIndex:section-1] objectForKey:@"list"] count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section > 0)
    {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PooWeeMedStoryboard" bundle:nil];
        
        NSMutableDictionary *data = [[NSMutableDictionary alloc] initWithDictionary:@{@"activity_info":[[[activityList objectAtIndex:indexPath.section-1] objectForKey:@"list"] objectAtIndex:indexPath.row]}];
        NSLog(@"data=%@", data);
        
        UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"ActivityMedicalEditViewController"];
        viewController.data = data;
        [self.navigationController pushViewController:viewController animated:YES];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 0)
    {
        // Temperature
        NSMutableArray *yVals1 = [[NSMutableArray alloc] init];
        for(int i=0; i<[[chartInfo objectForKey:@"temperature_list"] count]; i++)
        {
            if([[[chartInfo objectForKey:@"temperature_list"] objectAtIndex:i] doubleValue] > 0)
            {
                if (selectedIndex==1) {
                    [yVals1 addObject:[[ChartDataEntry alloc] initWithX:i+1 y:[self convertUnit:[[[chartInfo objectForKey:@"temperature_list"] objectAtIndex:i] doubleValue] unit:@"°C" newUnit:[unitInfo objectForKey:@"temperature"]]]];
                }else {
                    [yVals1 addObject:[[ChartDataEntry alloc] initWithX:i y:[self convertUnit:[[[chartInfo objectForKey:@"temperature_list"] objectAtIndex:i] doubleValue] unit:@"°C" newUnit:[unitInfo objectForKey:@"temperature"]]]];
                }
            }
        }
        
        NSMutableArray *yVals2 = [[NSMutableArray alloc] init];
        for(int i=0; i<[[chartInfo objectForKey:@"normal_temperature_list"] count]; i++)
        {
            if([[[chartInfo objectForKey:@"normal_temperature_list"] objectAtIndex:i] doubleValue] > 0)
            {
                if (selectedIndex == 1) {
                    [yVals2 addObject:[[ChartDataEntry alloc] initWithX:i+1 y:[self convertUnit:[[[chartInfo objectForKey:@"normal_temperature_list"] objectAtIndex:i] doubleValue] unit:@"°C" newUnit:[unitInfo objectForKey:@"temperature"]]]];
                }else {
                    [yVals2 addObject:[[ChartDataEntry alloc] initWithX:i y:[self convertUnit:[[[chartInfo objectForKey:@"normal_temperature_list"] objectAtIndex:i] doubleValue] unit:@"°C" newUnit:[unitInfo objectForKey:@"temperature"]]]];
                }
            }
        }
        
        ScatterChartData *scatterData1 = [[ScatterChartData alloc] init];
        ScatterChartDataSet *set1 = [[ScatterChartDataSet alloc] initWithValues:yVals1];
        [set1 setDrawValuesEnabled:NO];
        [set1 setScatterShape:ScatterShapeCircle];
        set1.colors = @[UIColorFromRGB(0x8D8082)];
        set1.scatterShapeSize = 5;
        [scatterData1 addDataSet:set1];
        
        LineChartData *lineData = [[LineChartData alloc] init];
        LineChartDataSet *set2 = [[LineChartDataSet alloc] initWithValues:yVals2];
        [set2 setColor:UIColorFromRGB(0xECECED)];
        set2.lineDashLengths = @[@3, @4];
        set2.highlightLineDashLengths = @[@3, @4];
        set2.lineWidth = 3.0;
        set2.circleRadius = 1;
        set2.drawValuesEnabled = NO;
        set2.mode = LineChartModeHorizontalBezier;
        [lineData addDataSet:set2];
        
        //Symptoms
        NSMutableArray *yVals3 = [[NSMutableArray alloc] init];
        for(int i=0; i<[[chartInfo objectForKey:@"symp_list1"] count]; i++)
        {
            if([[[chartInfo objectForKey:@"symp_list1"] objectAtIndex:i] doubleValue] > 0)
            {
                [yVals3 addObject:[[ChartDataEntry alloc] initWithX:i+1 y:[[[chartInfo objectForKey:@"symp_list1"] objectAtIndex:i] doubleValue]]];
            }
        }
        
        NSMutableArray *yVals4 = [[NSMutableArray alloc] init];
        for(int i=0; i<[[chartInfo objectForKey:@"symp_list2"] count]; i++)
        {
            if([[[chartInfo objectForKey:@"symp_list2"] objectAtIndex:i] doubleValue] > 0)
            {
                [yVals4 addObject:[[ChartDataEntry alloc] initWithX:i+1 y:[[[chartInfo objectForKey:@"symp_list2"] objectAtIndex:i] doubleValue]]];
            }
        }
        
        NSMutableArray *yVals5 = [[NSMutableArray alloc] init];
        for(int i=0; i<[[chartInfo objectForKey:@"symp_list3"] count]; i++)
        {
            if([[[chartInfo objectForKey:@"symp_list3"] objectAtIndex:i] doubleValue] > 0)
            {
                [yVals5 addObject:[[ChartDataEntry alloc] initWithX:i+1 y:[[[chartInfo objectForKey:@"symp_list3"] objectAtIndex:i] doubleValue]]];
            }
        }
        
        // Medicines
        NSMutableArray *yVals6 = [[NSMutableArray alloc] init];
        for(int i=0; i<[[chartInfo objectForKey:@"med1_list"] count]; i++)
        {
            if([[[chartInfo objectForKey:@"med1_list"] objectAtIndex:i] doubleValue] > 0)
            {
                //[yVals6 addObject:[[ChartDataEntry alloc] initWithX:i y:[self convertUnit:[[[chartInfo objectForKey:@"med1_list"] objectAtIndex:i] doubleValue] unit:@"ml" newUnit:[unitInfo objectForKey:@"medicine"]]]];
                [yVals6 addObject:[[ChartDataEntry alloc] initWithX:i+1 y:[self setAxixNoForMedical:[[[chartInfo objectForKey:@"med1_list"] objectAtIndex:i] doubleValue]]]];
            }
        }
        
        NSMutableArray *yVals7 = [[NSMutableArray alloc] init];
        for(int i=0; i<[[chartInfo objectForKey:@"med2_list"] count]; i++)
        {
            if([[[chartInfo objectForKey:@"med2_list"] objectAtIndex:i] doubleValue] > 0)
            {
                //[yVals7 addObject:[[ChartDataEntry alloc] initWithX:i y:[self convertUnit:[[[chartInfo objectForKey:@"med2_list"] objectAtIndex:i] doubleValue] unit:@"ml" newUnit:[unitInfo objectForKey:@"medicine"]]]];
                [yVals7 addObject:[[ChartDataEntry alloc] initWithX:i+1 y:[self setAxixNoForMedical:[[[chartInfo objectForKey:@"med2_list"] objectAtIndex:i] doubleValue]]]];
            }
        }
        
        NSMutableArray *yVals8 = [[NSMutableArray alloc] init];
        for(int i=0; i<[[chartInfo objectForKey:@"med3_list"] count]; i++)
        {
            if([[[chartInfo objectForKey:@"med3_list"] objectAtIndex:i] doubleValue] > 0)
            {
                //[yVals8 addObject:[[ChartDataEntry alloc] initWithX:i y:[self convertUnit:[[[chartInfo objectForKey:@"med3_list"] objectAtIndex:i] doubleValue] unit:@"ml" newUnit:[unitInfo objectForKey:@"medicine"]]]];
                [yVals8 addObject:[[ChartDataEntry alloc] initWithX:i+1 y:[self setAxixNoForMedical:[[[chartInfo objectForKey:@"med3_list"] objectAtIndex:i] doubleValue]]]];
            }
        }
        
        NSMutableArray *yVals9 = [[NSMutableArray alloc] init];
        for(int i=0; i<[[chartInfo objectForKey:@"med4_list"] count]; i++)
        {
            if([[[chartInfo objectForKey:@"med4_list"] objectAtIndex:i] doubleValue] > 0)
            {
                //[yVals9 addObject:[[ChartDataEntry alloc] initWithX:i y:[self convertUnit:[[[chartInfo objectForKey:@"med4_list"] objectAtIndex:i] doubleValue] unit:@"ml" newUnit:[unitInfo objectForKey:@"medicine"]]]];
                [yVals9 addObject:[[ChartDataEntry alloc] initWithX:i+1 y:[self setAxixNoForMedical:[[[chartInfo objectForKey:@"med4_list"] objectAtIndex:i] doubleValue]]]];
            }
        }
        
        NSMutableArray *yVals10 = [[NSMutableArray alloc] init];
        for(int i=0; i<[[chartInfo objectForKey:@"med5_list"] count]; i++)
        {
            if([[[chartInfo objectForKey:@"med5_list"] objectAtIndex:i] doubleValue] > 0)
            {
                //[yVals10 addObject:[[ChartDataEntry alloc] initWithX:i y:[self convertUnit:[[[chartInfo objectForKey:@"med5_list"] objectAtIndex:i] doubleValue] unit:@"ml" newUnit:[unitInfo objectForKey:@"medicine"]]]];
                [yVals10 addObject:[[ChartDataEntry alloc] initWithX:i+1 y:[self setAxixNoForMedical:[[[chartInfo objectForKey:@"med5_list"] objectAtIndex:i] doubleValue]]]];
            }
        }
        
        NSMutableArray *yVals11 = [[NSMutableArray alloc] init];
        for(int i=0; i<[[chartInfo objectForKey:@"med6_list"] count]; i++)
        {
            if([[[chartInfo objectForKey:@"med6_list"] objectAtIndex:i] doubleValue] > 0)
            {
                //[yVals11 addObject:[[ChartDataEntry alloc] initWithX:i y:[self convertUnit:[[[chartInfo objectForKey:@"med6_list"] objectAtIndex:i] doubleValue] unit:@"ml" newUnit:[unitInfo objectForKey:@"medicine"]]]];
                [yVals11 addObject:[[ChartDataEntry alloc] initWithX:i+1 y:[self setAxixNoForMedical:[[[chartInfo objectForKey:@"med6_list"] objectAtIndex:i] doubleValue]]]];
            }
        }
        
        NSMutableArray *yVals12 = [[NSMutableArray alloc] init];
        for(int i=0; i<[[chartInfo objectForKey:@"med7_list"] count]; i++)
        {
            if([[[chartInfo objectForKey:@"med7_list"] objectAtIndex:i] doubleValue] > 0)
            {
                //[yVals12 addObject:[[ChartDataEntry alloc] initWithX:i y:[self convertUnit:[[[chartInfo objectForKey:@"med7_list"] objectAtIndex:i] doubleValue] unit:@"ml" newUnit:[unitInfo objectForKey:@"medicine"]]]];
                [yVals12 addObject:[[ChartDataEntry alloc] initWithX:i+1 y:[self setAxixNoForMedical:[[[chartInfo objectForKey:@"med7_list"] objectAtIndex:i] doubleValue]]]];
            }
        }
        
        if(selectedIndex == 0)
        {
            ActivityMedicalDayChartTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ActivityMedicalDayChartTableViewCell"];
            if(!cell)
            {
                [tableView registerNib:[UINib nibWithNibName:@"ActivityMedicalDayChartTableViewCell" bundle:nil] forCellReuseIdentifier:@"ActivityMedicalDayChartTableViewCell"];
                cell = [tableView dequeueReusableCellWithIdentifier:@"ActivityMedicalDayChartTableViewCell"];
            }
            
            //ChartYAxis *leftAxis = cell.chartView.leftAxis;
            //leftAxis.labelCount = 13;
            //leftAxis.axisMaximum = [[chartInfo objectForKey:@"med_max"] intValue];
            //leftAxis.axisMinimum = -([[chartInfo objectForKey:@"med_max"] floatValue]/10);
            // graph start
            ChartYAxis *leftAxis2 = cell.temperatureChartView.leftAxis;
            ChartYAxis *rightAxis2 = cell.temperatureChartView.rightAxis;
            leftAxis2.labelCount = rightAxis2.labelCount = 5;
            leftAxis2.axisMaximum = rightAxis2.axisMaximum = [[chartInfo objectForKey:@"temp_max"] intValue];
            leftAxis2.axisMinimum = rightAxis2.axisMinimum = [[chartInfo objectForKey:@"temp_min"] intValue];
            
            //cell.txtLeftAxis.text = [NSString stringWithFormat:LocalizedString(@"txt_vol", nil), [unitInfo objectForKey:@"medicine"]];
            cell.txtRightAxis.text = [NSString stringWithFormat:LocalizedString(@"txt_temp", nil), [unitInfo objectForKey:@"temperature"]];
            
            CombinedChartData *data = [[CombinedChartData alloc] init];
            data.lineData = lineData;
            data.scatterData = scatterData1;
            cell.temperatureChartView.data = data;
            
            ScatterChartData *scatterData2 = [[ScatterChartData alloc] init];
            
            ScatterChartDataSet *set3 = [[ScatterChartDataSet alloc] initWithValues:yVals3];
            [set3 setDrawValuesEnabled:NO];
            [set3 setScatterShape:ScatterShapeCircle];
            set3.colors = @[UIColorFromRGB(0xC9DAAB)];
            set3.scatterShapeSize = 15;
            [scatterData2 addDataSet:set3];
            
            ScatterChartDataSet *set4 = [[ScatterChartDataSet alloc] initWithValues:yVals4];
            [set4 setDrawValuesEnabled:NO];
            [set4 setScatterShape:ScatterShapeCircle];
            set4.colors = @[UIColorFromRGB(0xC9DAAB)];
            set4.scatterShapeSize = 15;
            [scatterData2 addDataSet:set4];
            
            ScatterChartDataSet *set5 = [[ScatterChartDataSet alloc] initWithValues:yVals5];
            [set5 setDrawValuesEnabled:NO];
            [set5 setScatterShape:ScatterShapeCircle];
            set5.colors = @[UIColorFromRGB(0xC9DAAB)];
            set5.scatterShapeSize = 15;
            [scatterData2 addDataSet:set5];
            
            ScatterChartDataSet *set6 = [[ScatterChartDataSet alloc] initWithValues:yVals6];
            [set6 setDrawValuesEnabled:NO];
            [set6 setScatterShape:ScatterShapeCircle];
            set6.colors = @[UIColorFromRGB(0xC5D5DF)];
            set6.scatterShapeSize = 15;
            [scatterData2 addDataSet:set6];
            
            ScatterChartDataSet *set7 = [[ScatterChartDataSet alloc] initWithValues:yVals7];
            [set7 setDrawValuesEnabled:NO];
            [set7 setScatterShape:ScatterShapeCircle];
            set7.colors = @[UIColorFromRGB(0xC5D5DF)];
            set7.scatterShapeSize = 15;
            [scatterData2 addDataSet:set7];
            
            ScatterChartDataSet *set8 = [[ScatterChartDataSet alloc] initWithValues:yVals8];
            [set8 setDrawValuesEnabled:NO];
            [set8 setScatterShape:ScatterShapeCircle];
            set8.colors = @[UIColorFromRGB(0xC5D5DF)];
            set8.scatterShapeSize = 15;
            [scatterData2 addDataSet:set8];
            
            ScatterChartDataSet *set9 = [[ScatterChartDataSet alloc] initWithValues:yVals9];
            [set9 setDrawValuesEnabled:NO];
            [set9 setScatterShape:ScatterShapeCircle];
            set9.colors = @[UIColorFromRGB(0xC5D5DF)];
            set9.scatterShapeSize = 15;
            [scatterData2 addDataSet:set9];
            
            ScatterChartDataSet *set10 = [[ScatterChartDataSet alloc] initWithValues:yVals10];
            [set10 setDrawValuesEnabled:NO];
            [set10 setScatterShape:ScatterShapeCircle];
            set10.colors = @[UIColorFromRGB(0xC5D5DF)];
            set10.scatterShapeSize = 15;
            [scatterData2 addDataSet:set10];
            
            ScatterChartDataSet *set11 = [[ScatterChartDataSet alloc] initWithValues:yVals11];
            [set11 setDrawValuesEnabled:NO];
            [set11 setScatterShape:ScatterShapeCircle];
            set11.colors = @[UIColorFromRGB(0xC5D5DF)];
            set11.scatterShapeSize = 15;
            [scatterData2 addDataSet:set11];
            
            ScatterChartDataSet *set12 = [[ScatterChartDataSet alloc] initWithValues:yVals12];
            [set12 setDrawValuesEnabled:NO];
            [set12 setScatterShape:ScatterShapeCircle];
            set12.colors = @[UIColorFromRGB(0xC5D5DF)];
            set12.scatterShapeSize = 15;
            [scatterData2 addDataSet:set12];
            
            cell.chartView.data = scatterData2;
            
            cell.medName1.text = [NSString stringWithFormat:@"M1 : %@",[chartInfo objectForKey:@"med1_name"]];
            cell.medName2.text = [NSString stringWithFormat:@"M2 : %@",[chartInfo objectForKey:@"med2_name"]];
            cell.medName3.text = [NSString stringWithFormat:@"M3 : %@",[chartInfo objectForKey:@"med3_name"]];
            cell.medName4.text = [NSString stringWithFormat:@"M4 : %@",[chartInfo objectForKey:@"med4_name"]];
            cell.medName5.text = [NSString stringWithFormat:@"M5 : %@",[chartInfo objectForKey:@"med5_name"]];
            cell.medName6.text = [NSString stringWithFormat:@"M6 : %@",[chartInfo objectForKey:@"med6_name"]];
            cell.medName7.text = [NSString stringWithFormat:@"M7 : %@",[chartInfo objectForKey:@"med7_name"]];
            
            cell.medValue1.text = [NSString stringWithFormat:@"%@",[chartInfo objectForKey:@"med1_total"]];
            cell.medValue2.text = [NSString stringWithFormat:@"%@",[chartInfo objectForKey:@"med2_total"]];
            cell.medValue3.text = [NSString stringWithFormat:@"%@",[chartInfo objectForKey:@"med3_total"]];
            cell.medValue4.text = [NSString stringWithFormat:@"%@",[chartInfo objectForKey:@"med4_total"]];
            cell.medValue5.text = [NSString stringWithFormat:@"%@",[chartInfo objectForKey:@"med5_total"]];
            cell.medValue6.text = [NSString stringWithFormat:@"%@",[chartInfo objectForKey:@"med6_total"]];
            cell.medValue7.text = [NSString stringWithFormat:@"%@",[chartInfo objectForKey:@"med7_total"]];
            
            cell.sympName1.text = [NSString stringWithFormat:@"S1 : %@",[chartInfo objectForKey:@"symp_name1"]];
            cell.sympName2.text = [NSString stringWithFormat:@"S2 : %@",[chartInfo objectForKey:@"symp_name2"]];
            cell.sympName3.text = [NSString stringWithFormat:@"S3 : %@",[chartInfo objectForKey:@"symp_name3"]];
            
            cell.sympValue1.text = [NSString stringWithFormat:@"%@ times",[chartInfo objectForKey:@"symp_count1"]];
            cell.sympValue2.text = [NSString stringWithFormat:@"%@ times",[chartInfo objectForKey:@"symp_count2"]];
            cell.sympValue3.text = [NSString stringWithFormat:@"%@ times",[chartInfo objectForKey:@"symp_count3"]];
            
            // Make medicine labels dynamic
            cell.viewMed7.hidden = YES;
            cell.viewMed6.hidden = YES;
            cell.viewMed5.hidden = YES;
            cell.viewMed4.hidden = YES;
            cell.viewMed3.hidden = YES;
            cell.viewMed2.hidden = YES;
            cell.viewMed1.hidden = YES;
            
            cell.viewSymp3.hidden = YES;
            cell.viewSymp2.hidden = YES;
            cell.viewSymp1.hidden = YES;
            
            cell.viewMed1HC.constant = 0;
            cell.viewMed2HC.constant = 0;
            cell.viewMed3HC.constant = 0;
            cell.viewMed4HC.constant = 0;
            cell.viewMed5HC.constant = 0;
            cell.viewMed6HC.constant = 0;
            cell.viewMed7HC.constant = 0;
            
            cell.viewSymp1HC.constant = 0;
            cell.viewSymp2HC.constant = 0;
            cell.viewSymp3HC.constant = 0;
            
            int medCount = 0;
            if ([[chartInfo objectForKey:@"med1_count"] intValue]>0) {
                medCount += 1;
                cell.viewMed1.hidden = NO;
                cell.viewMed1HC.constant = 30;
            }
            if ([[chartInfo objectForKey:@"med2_count"] intValue]>0) {
                medCount += 1;
                cell.viewMed2.hidden = NO;
                cell.viewMed2HC.constant = 30;
            }
            if ([[chartInfo objectForKey:@"med3_count"] intValue]>0) {
                medCount += 1;
                cell.viewMed3.hidden = NO;
                cell.viewMed3HC.constant = 30;
            }
            if ([[chartInfo objectForKey:@"med4_count"] intValue]>0) {
                medCount += 1;
                cell.viewMed4.hidden = NO;
                cell.viewMed4HC.constant = 30;
            }
            if ([[chartInfo objectForKey:@"med5_count"] intValue]>0) {
                medCount += 1;
                cell.viewMed5.hidden = NO;
                cell.viewMed5HC.constant = 30;
            }
            if ([[chartInfo objectForKey:@"med6_count"] intValue]>0) {
                medCount += 1;
                cell.viewMed6.hidden = NO;
                cell.viewMed6HC.constant = 30;
            }
            if ([[chartInfo objectForKey:@"med7_count"] intValue]>0) {
                medCount += 1;
                cell.viewMed7.hidden = NO;
                cell.viewMed7HC.constant = 30;
            }
            
            int sympCount = 0;
            if ([[chartInfo objectForKey:@"symp_count1"] intValue]>0) {
                sympCount += 1;
                cell.viewSymp1.hidden = NO;
                cell.viewSymp1HC.constant = 30;
            }
            if ([[chartInfo objectForKey:@"symp_count2"] intValue]>0) {
                sympCount += 1;
                cell.viewSymp2.hidden = NO;
                cell.viewSymp2HC.constant = 30;
            }
            if ([[chartInfo objectForKey:@"symp_count3"] intValue]>0) {
                sympCount += 1;
                cell.viewSymp3.hidden = NO;
                cell.viewSymp3HC.constant = 30;
            }
            
            cell.hcViewMedSynp.constant = (30+5);
            if (medCount>1 || sympCount>1) {
                if (medCount>=sympCount) {
                    cell.hcViewMedSynp.constant = medCount*(30+5);
                }else {
                    cell.hcViewMedSynp.constant = sympCount*(30+5);
                }
            }
            
            cell.viewMed1HC.constant = 30;
            cell.viewMed1.hidden = NO;
            cell.viewSymp1HC.constant = 30;
            cell.viewSymp1.hidden = NO;
            
            if([[chartInfo objectForKey:@"med1_name"] isEqualToString:@""] || [[chartInfo objectForKey:@"symp_name1"] isEqualToString:@""]){
                
                if([[chartInfo objectForKey:@"med1_name"] isEqualToString:@""]){
                    cell.medName1.text = [NSString stringWithFormat:@"M1 : %s","-"];
                    cell.medValue1.text = [NSString stringWithFormat:@"%s","-"];
                }
                if([[chartInfo objectForKey:@"symp_name1"] isEqualToString:@""]){
                    cell.sympName1.text = [NSString stringWithFormat:@"S1 : %s","-"];
                    cell.sympValue1.text = [NSString stringWithFormat:@"%s","-"];
                }
            }
            
            // Anil
            
            //            if([[chartInfo objectForKey:@"med7_name"] isEqualToString:@""]){
            //                //cell.hcViewMedSynp.constant = 240;
            //                cell.viewMed7.hidden = YES;
            //            }
            //
            //            if([[chartInfo objectForKey:@"med6_name"] isEqualToString:@""]){
            //                //cell.hcViewMedSynp.constant = 240;
            //                cell.viewMed6.hidden = YES;
            //            }
            //
            //        if([[chartInfo objectForKey:@"med5_name"] isEqualToString:@""]){
            //            //cell.hcViewMedSynp.constant = 210;
            //            cell.viewMed5.hidden = YES;
            //    }
            //
            //            if([[chartInfo objectForKey:@"med4_name"] isEqualToString:@""]){
            //                //cell.hcViewMedSynp.constant = 180;
            //                cell.viewMed4.hidden = YES;
            //            }
            //
            //            if([[chartInfo objectForKey:@"med3_name"] isEqualToString:@""] || [[chartInfo objectForKey:@"symp_name3"] isEqualToString:@""]){
            //
            //                if([[chartInfo objectForKey:@"med3_name"] isEqualToString:@""]){
            //
            //                    cell.viewMed3.hidden = YES;
            //                }
            //
            //               if([[chartInfo objectForKey:@"symp_name3"] isEqualToString:@""]){
            //                    cell.viewSymp3.hidden = YES;
            //                }
            //
            //                if([[chartInfo objectForKey:@"med3_name"] isEqualToString:@""] && [[chartInfo objectForKey:@"symp_name3"] isEqualToString:@""]){
            //                    cell.hcViewMedSynp.constant = 150;
            //                    cell.viewMed3.hidden = YES;
            //                    cell.viewSymp3.hidden = YES;
            //                }
            //
            //            }
            //
            //            if([[chartInfo objectForKey:@"med2_name"] isEqualToString:@""] || [[chartInfo objectForKey:@"symp_name2"] isEqualToString:@""]){
            //
            //                if([[chartInfo objectForKey:@"med2_name"] isEqualToString:@""]){
            //
            //                    cell.viewMed2.hidden = YES;
            //                }
            //
            //                 if([[chartInfo objectForKey:@"symp_name2"] isEqualToString:@""]){
            //                    cell.viewSymp2.hidden = YES;
            //                }
            //
            //                if([[chartInfo objectForKey:@"med2_name"] isEqualToString:@""] && [[chartInfo objectForKey:@"symp_name2"] isEqualToString:@""]){
            //                    cell.hcViewMedSynp.constant = 120;
            //                    cell.viewMed2.hidden = YES;
            //                    cell.viewSymp2.hidden = YES;
            //                }
            //
            //            }
            //
            
            //// ANil
            
            UISwipeGestureRecognizer *gestureRecognizer1 = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(leftSwipeHandler:)];
            gestureRecognizer1.direction = UISwipeGestureRecognizerDirectionLeft;
            [cell addGestureRecognizer:gestureRecognizer1];
            
            UISwipeGestureRecognizer *gestureRecognizer2 = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(rightSwipeHandler:)];
            gestureRecognizer2.direction = UISwipeGestureRecognizerDirectionRight;
            [cell addGestureRecognizer:gestureRecognizer2];
            
            return cell;
        }
        else if (selectedIndex == 1)
        {
            ActivityMedicalWeekChartTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ActivityMedicalWeekChartTableViewCell"];
            if(!cell)
            {
                [tableView registerNib:[UINib nibWithNibName:@"ActivityMedicalWeekChartTableViewCell" bundle:nil] forCellReuseIdentifier:@"ActivityMedicalWeekChartTableViewCell"];
                cell = [tableView dequeueReusableCellWithIdentifier:@"ActivityMedicalWeekChartTableViewCell"];
            }
            
            //ChartYAxis *leftAxis = cell.chartView.leftAxis;
            //leftAxis.labelCount = 13;
            //leftAxis.axisMaximum = [[chartInfo objectForKey:@"med_max"] intValue];
            //leftAxis.axisMinimum = -([[chartInfo objectForKey:@"med_max"] floatValue]/10);
            // graph start
            ChartYAxis *leftAxis2 = cell.temperatureChartView.leftAxis;
            ChartYAxis *rightAxis2 = cell.temperatureChartView.rightAxis;
            leftAxis2.labelCount = rightAxis2.labelCount = 5;
            leftAxis2.axisMaximum = rightAxis2.axisMaximum = [[chartInfo objectForKey:@"temp_max"] intValue];
            leftAxis2.axisMinimum = rightAxis2.axisMinimum = [[chartInfo objectForKey:@"temp_min"] intValue];
            
            //cell.txtLeftAxis.text = [NSString stringWithFormat:LocalizedString(@"txt_vol", nil), [unitInfo objectForKey:@"medicine"]];
            cell.txtRightAxis.text = [NSString stringWithFormat:LocalizedString(@"txt_temp", nil), [unitInfo objectForKey:@"temperature"]];
            
            CombinedChartData *data = [[CombinedChartData alloc] init];
            data.lineData = lineData;
            data.scatterData = scatterData1;
            cell.temperatureChartView.data = data;
            
            ScatterChartData *scatterData2 = [[ScatterChartData alloc] init];
            
            ScatterChartDataSet *set3 = [[ScatterChartDataSet alloc] initWithValues:yVals3];
            [set3 setDrawValuesEnabled:NO];
            [set3 setScatterShape:ScatterShapeCircle];
            set3.colors = @[UIColorFromRGB(0xC9DAAB)];
            set3.scatterShapeSize = 15;
            [scatterData2 addDataSet:set3];
            
            ScatterChartDataSet *set4 = [[ScatterChartDataSet alloc] initWithValues:yVals4];
            [set4 setDrawValuesEnabled:NO];
            [set4 setScatterShape:ScatterShapeCircle];
            set4.colors = @[UIColorFromRGB(0xC9DAAB)];
            set4.scatterShapeSize = 15;
            [scatterData2 addDataSet:set4];
            
            ScatterChartDataSet *set5 = [[ScatterChartDataSet alloc] initWithValues:yVals5];
            [set5 setDrawValuesEnabled:NO];
            [set5 setScatterShape:ScatterShapeCircle];
            set5.colors = @[UIColorFromRGB(0xC9DAAB)];
            set5.scatterShapeSize = 15;
            [scatterData2 addDataSet:set5];
            
            ScatterChartDataSet *set6 = [[ScatterChartDataSet alloc] initWithValues:yVals6];
            [set6 setDrawValuesEnabled:NO];
            [set6 setScatterShape:ScatterShapeCircle];
            set6.colors = @[UIColorFromRGB(0xC5D5DF)];
            set6.scatterShapeSize = 15;
            [scatterData2 addDataSet:set6];
            
            ScatterChartDataSet *set7 = [[ScatterChartDataSet alloc] initWithValues:yVals7];
            [set7 setDrawValuesEnabled:NO];
            [set7 setScatterShape:ScatterShapeCircle];
            set7.colors = @[UIColorFromRGB(0xC5D5DF)];
            set7.scatterShapeSize = 15;
            [scatterData2 addDataSet:set7];
            
            ScatterChartDataSet *set8 = [[ScatterChartDataSet alloc] initWithValues:yVals8];
            [set8 setDrawValuesEnabled:NO];
            [set8 setScatterShape:ScatterShapeCircle];
            set8.colors = @[UIColorFromRGB(0xC5D5DF)];
            set8.scatterShapeSize = 15;
            [scatterData2 addDataSet:set8];
            
            ScatterChartDataSet *set9 = [[ScatterChartDataSet alloc] initWithValues:yVals9];
            [set9 setDrawValuesEnabled:NO];
            [set9 setScatterShape:ScatterShapeCircle];
            set9.colors = @[UIColorFromRGB(0xC5D5DF)];
            set9.scatterShapeSize = 15;
            [scatterData2 addDataSet:set9];
            
            ScatterChartDataSet *set10 = [[ScatterChartDataSet alloc] initWithValues:yVals10];
            [set10 setDrawValuesEnabled:NO];
            [set10 setScatterShape:ScatterShapeCircle];
            set10.colors = @[UIColorFromRGB(0xC5D5DF)];
            set10.scatterShapeSize = 15;
            [scatterData2 addDataSet:set10];
            
            ScatterChartDataSet *set11 = [[ScatterChartDataSet alloc] initWithValues:yVals11];
            [set11 setDrawValuesEnabled:NO];
            [set11 setScatterShape:ScatterShapeCircle];
            set11.colors = @[UIColorFromRGB(0xC5D5DF)];
            set11.scatterShapeSize = 15;
            [scatterData2 addDataSet:set11];
            
            ScatterChartDataSet *set12 = [[ScatterChartDataSet alloc] initWithValues:yVals12];
            [set12 setDrawValuesEnabled:NO];
            [set12 setScatterShape:ScatterShapeCircle];
            set12.colors = @[UIColorFromRGB(0xC5D5DF)];
            set12.scatterShapeSize = 15;
            [scatterData2 addDataSet:set12];
            
            cell.chartView.data = scatterData2;
            
            cell.medName1.text = [NSString stringWithFormat:@"M1 : %@",[chartInfo objectForKey:@"med1_name"]];
            cell.medName2.text = [NSString stringWithFormat:@"M2 : %@",[chartInfo objectForKey:@"med2_name"]];
            cell.medName3.text = [NSString stringWithFormat:@"M3 : %@",[chartInfo objectForKey:@"med3_name"]];
            cell.medName4.text = [NSString stringWithFormat:@"M4 : %@",[chartInfo objectForKey:@"med4_name"]];
            cell.medName5.text = [NSString stringWithFormat:@"M5 : %@",[chartInfo objectForKey:@"med5_name"]];
            cell.medName6.text = [NSString stringWithFormat:@"M6 : %@",[chartInfo objectForKey:@"med6_name"]];
            cell.medName7.text = [NSString stringWithFormat:@"M7 : %@",[chartInfo objectForKey:@"med7_name"]];
            
            cell.medValue1.text = [NSString stringWithFormat:@"%@",[chartInfo objectForKey:@"med1_total"]];
            cell.medValue2.text = [NSString stringWithFormat:@"%@",[chartInfo objectForKey:@"med2_total"]];
            cell.medValue3.text = [NSString stringWithFormat:@"%@",[chartInfo objectForKey:@"med3_total"]];
            cell.medValue4.text = [NSString stringWithFormat:@"%@",[chartInfo objectForKey:@"med4_total"]];
            cell.medValue5.text = [NSString stringWithFormat:@"%@",[chartInfo objectForKey:@"med5_total"]];
            cell.medValue6.text = [NSString stringWithFormat:@"%@",[chartInfo objectForKey:@"med6_total"]];
            cell.medValue7.text = [NSString stringWithFormat:@"%@",[chartInfo objectForKey:@"med7_total"]];
            
            cell.sympName1.text = [NSString stringWithFormat:@"S1 : %@",[chartInfo objectForKey:@"symp_name1"]];
            cell.sympName2.text = [NSString stringWithFormat:@"S2 : %@",[chartInfo objectForKey:@"symp_name2"]];
            cell.sympName3.text = [NSString stringWithFormat:@"S3 : %@",[chartInfo objectForKey:@"symp_name3"]];
            
            cell.sympValue1.text = [NSString stringWithFormat:@"%@ times",[chartInfo objectForKey:@"symp_count1"]];
            cell.sympValue2.text = [NSString stringWithFormat:@"%@ times",[chartInfo objectForKey:@"symp_count2"]];
            cell.sympValue3.text = [NSString stringWithFormat:@"%@ times",[chartInfo objectForKey:@"symp_count3"]];
            
            // Make medicine labels dynamic
            cell.viewMed7.hidden = YES;
            cell.viewMed6.hidden = YES;
            cell.viewMed5.hidden = YES;
            cell.viewMed4.hidden = YES;
            cell.viewMed3.hidden = YES;
            cell.viewMed2.hidden = YES;
            cell.viewMed1.hidden = YES;
            
            cell.viewSymp3.hidden = YES;
            cell.viewSymp2.hidden = YES;
            cell.viewSymp1.hidden = YES;
            
            cell.viewMed1HC.constant = 0;
            cell.viewMed2HC.constant = 0;
            cell.viewMed3HC.constant = 0;
            cell.viewMed4HC.constant = 0;
            cell.viewMed5HC.constant = 0;
            cell.viewMed6HC.constant = 0;
            cell.viewMed7HC.constant = 0;
            
            cell.viewSymp1HC.constant = 0;
            cell.viewSymp2HC.constant = 0;
            cell.viewSymp3HC.constant = 0;
            
            int medCount = 0;
            if ([[chartInfo objectForKey:@"med1_count"] intValue]>0) {
                medCount += 1;
                cell.viewMed1.hidden = NO;
                cell.viewMed1HC.constant = 30;
            }
            if ([[chartInfo objectForKey:@"med2_count"] intValue]>0) {
                medCount += 1;
                cell.viewMed2.hidden = NO;
                cell.viewMed2HC.constant = 30;
            }
            if ([[chartInfo objectForKey:@"med3_count"] intValue]>0) {
                medCount += 1;
                cell.viewMed3.hidden = NO;
                cell.viewMed3HC.constant = 30;
            }
            if ([[chartInfo objectForKey:@"med4_count"] intValue]>0) {
                medCount += 1;
                cell.viewMed4.hidden = NO;
                cell.viewMed4HC.constant = 30;
            }
            if ([[chartInfo objectForKey:@"med5_count"] intValue]>0) {
                medCount += 1;
                cell.viewMed5.hidden = NO;
                cell.viewMed5HC.constant = 30;
            }
            if ([[chartInfo objectForKey:@"med6_count"] intValue]>0) {
                medCount += 1;
                cell.viewMed6.hidden = NO;
                cell.viewMed6HC.constant = 30;
            }
            if ([[chartInfo objectForKey:@"med7_count"] intValue]>0) {
                medCount += 1;
                cell.viewMed7.hidden = NO;
                cell.viewMed7HC.constant = 30;
            }
            
            int sympCount = 0;
            if ([[chartInfo objectForKey:@"symp_count1"] intValue]>0) {
                sympCount += 1;
                cell.viewSymp1.hidden = NO;
                cell.viewSymp1HC.constant = 30;
            }
            if ([[chartInfo objectForKey:@"symp_count2"] intValue]>0) {
                sympCount += 1;
                cell.viewSymp2.hidden = NO;
                cell.viewSymp2HC.constant = 30;
            }
            if ([[chartInfo objectForKey:@"symp_count3"] intValue]>0) {
                sympCount += 1;
                cell.viewSymp3.hidden = NO;
                cell.viewSymp3HC.constant = 30;
            }
            
            cell.hcViewMedSynp.constant = (30+5);
            if (medCount>1 || sympCount>1) {
                if (medCount>=sympCount) {
                    cell.hcViewMedSynp.constant = medCount*(30+5);
                }else {
                    cell.hcViewMedSynp.constant = sympCount*(30+5);
                }
            }
            
            cell.viewMed1HC.constant = 30;
            cell.viewMed1.hidden = NO;
            cell.viewSymp1HC.constant = 30;
            cell.viewSymp1.hidden = NO;
            
            if([[chartInfo objectForKey:@"med1_name"] isEqualToString:@""] || [[chartInfo objectForKey:@"symp_name1"] isEqualToString:@""]){
                
                if([[chartInfo objectForKey:@"med1_name"] isEqualToString:@""]){
                    cell.medName1.text = [NSString stringWithFormat:@"M1 : %s","-"];
                    cell.medValue1.text = [NSString stringWithFormat:@"%s","-"];
                }
                if([[chartInfo objectForKey:@"symp_name1"] isEqualToString:@""]){
                    cell.sympName1.text = [NSString stringWithFormat:@"S1 : %s","-"];
                    cell.sympValue1.text = [NSString stringWithFormat:@"%s","-"];
                }
            }
            
            UISwipeGestureRecognizer *gestureRecognizer1 = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(leftSwipeHandler:)];
            gestureRecognizer1.direction = UISwipeGestureRecognizerDirectionLeft;
            [cell addGestureRecognizer:gestureRecognizer1];
            
            UISwipeGestureRecognizer *gestureRecognizer2 = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(rightSwipeHandler:)];
            gestureRecognizer2.direction = UISwipeGestureRecognizerDirectionRight;
            [cell addGestureRecognizer:gestureRecognizer2];
            
            return cell;
        }
        else
        {
            ActivityMedicalMonthChartTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ActivityMedicalMonthChartTableViewCell"];
            if(!cell)
            {
                [tableView registerNib:[UINib nibWithNibName:@"ActivityMedicalMonthChartTableViewCell" bundle:nil] forCellReuseIdentifier:@"ActivityMedicalMonthChartTableViewCell"];
                cell = [tableView dequeueReusableCellWithIdentifier:@"ActivityMedicalMonthChartTableViewCell"];
            }
            
            //ChartYAxis *leftAxis = cell.chartView.leftAxis;
            //leftAxis.labelCount = 13;
            //leftAxis.axisMaximum = [[chartInfo objectForKey:@"med_max"] intValue];
            //leftAxis.axisMinimum = -([[chartInfo objectForKey:@"med_max"] floatValue]/10);
            // graph start
            ChartYAxis *leftAxis2 = cell.temperatureChartView.leftAxis;
            ChartYAxis *rightAxis2 = cell.temperatureChartView.rightAxis;
            leftAxis2.labelCount = rightAxis2.labelCount = 5;
            leftAxis2.axisMaximum = rightAxis2.axisMaximum = [[chartInfo objectForKey:@"temp_max"] intValue];
            leftAxis2.axisMinimum = rightAxis2.axisMinimum = [[chartInfo objectForKey:@"temp_min"] intValue];
            
            //cell.txtLeftAxis.text = [NSString stringWithFormat:LocalizedString(@"txt_vol", nil), [unitInfo objectForKey:@"medicine"]];
            cell.txtRightAxis.text = [NSString stringWithFormat:LocalizedString(@"txt_temp", nil), [unitInfo objectForKey:@"temperature"]];
            
            CombinedChartData *data = [[CombinedChartData alloc] init];
            data.lineData = lineData;
            data.scatterData = scatterData1;
            cell.temperatureChartView.data = data;
            
            ScatterChartData *scatterData2 = [[ScatterChartData alloc] init];
            
            ScatterChartDataSet *set3 = [[ScatterChartDataSet alloc] initWithValues:yVals3];
            [set3 setDrawValuesEnabled:NO];
            [set3 setScatterShape:ScatterShapeCircle];
            set3.colors = @[UIColorFromRGB(0xC9DAAB)];
            set3.scatterShapeSize = 10;
            [scatterData2 addDataSet:set3];
            
            ScatterChartDataSet *set4 = [[ScatterChartDataSet alloc] initWithValues:yVals4];
            [set4 setDrawValuesEnabled:NO];
            [set4 setScatterShape:ScatterShapeCircle];
            set4.colors = @[UIColorFromRGB(0xC9DAAB)];
            set4.scatterShapeSize = 10;
            [scatterData2 addDataSet:set4];
            
            ScatterChartDataSet *set5 = [[ScatterChartDataSet alloc] initWithValues:yVals5];
            [set5 setDrawValuesEnabled:NO];
            [set5 setScatterShape:ScatterShapeCircle];
            set5.colors = @[UIColorFromRGB(0xC9DAAB)];
            set5.scatterShapeSize = 10;
            [scatterData2 addDataSet:set5];
            
            ScatterChartDataSet *set6 = [[ScatterChartDataSet alloc] initWithValues:yVals6];
            [set6 setDrawValuesEnabled:NO];
            [set6 setScatterShape:ScatterShapeCircle];
            set6.colors = @[UIColorFromRGB(0xC5D5DF)];
            set6.scatterShapeSize = 10;
            [scatterData2 addDataSet:set6];
            
            ScatterChartDataSet *set7 = [[ScatterChartDataSet alloc] initWithValues:yVals7];
            [set7 setDrawValuesEnabled:NO];
            [set7 setScatterShape:ScatterShapeCircle];
            set7.colors = @[UIColorFromRGB(0xC5D5DF)];
            set7.scatterShapeSize = 10;
            [scatterData2 addDataSet:set7];
            
            ScatterChartDataSet *set8 = [[ScatterChartDataSet alloc] initWithValues:yVals8];
            [set8 setDrawValuesEnabled:NO];
            [set8 setScatterShape:ScatterShapeCircle];
            set8.colors = @[UIColorFromRGB(0xC5D5DF)];
            set8.scatterShapeSize = 10;
            [scatterData2 addDataSet:set8];
            
            ScatterChartDataSet *set9 = [[ScatterChartDataSet alloc] initWithValues:yVals9];
            [set9 setDrawValuesEnabled:NO];
            [set9 setScatterShape:ScatterShapeCircle];
            set9.colors = @[UIColorFromRGB(0xC5D5DF)];
            set9.scatterShapeSize = 10;
            [scatterData2 addDataSet:set9];
            
            ScatterChartDataSet *set10 = [[ScatterChartDataSet alloc] initWithValues:yVals10];
            [set10 setDrawValuesEnabled:NO];
            [set10 setScatterShape:ScatterShapeCircle];
            set10.colors = @[UIColorFromRGB(0xC5D5DF)];
            set10.scatterShapeSize = 10;
            [scatterData2 addDataSet:set10];
            
            ScatterChartDataSet *set11 = [[ScatterChartDataSet alloc] initWithValues:yVals11];
            [set11 setDrawValuesEnabled:NO];
            [set11 setScatterShape:ScatterShapeCircle];
            set11.colors = @[UIColorFromRGB(0xC5D5DF)];
            set11.scatterShapeSize = 10;
            [scatterData2 addDataSet:set11];
            
            ScatterChartDataSet *set12 = [[ScatterChartDataSet alloc] initWithValues:yVals12];
            [set12 setDrawValuesEnabled:NO];
            [set12 setScatterShape:ScatterShapeCircle];
            set12.colors = @[UIColorFromRGB(0xC5D5DF)];
            set12.scatterShapeSize = 10;
            [scatterData2 addDataSet:set12];
            
            cell.chartView.data = scatterData2;
            
            cell.medName1.text = [NSString stringWithFormat:@"M1 : %@",[chartInfo objectForKey:@"med1_name"]];
            cell.medName2.text = [NSString stringWithFormat:@"M2 : %@",[chartInfo objectForKey:@"med2_name"]];
            cell.medName3.text = [NSString stringWithFormat:@"M3 : %@",[chartInfo objectForKey:@"med3_name"]];
            cell.medName4.text = [NSString stringWithFormat:@"M4 : %@",[chartInfo objectForKey:@"med4_name"]];
            cell.medName5.text = [NSString stringWithFormat:@"M5 : %@",[chartInfo objectForKey:@"med5_name"]];
            cell.medName6.text = [NSString stringWithFormat:@"M6 : %@",[chartInfo objectForKey:@"med6_name"]];
            cell.medName7.text = [NSString stringWithFormat:@"M7 : %@",[chartInfo objectForKey:@"med7_name"]];
            
            cell.medValue1.text = [NSString stringWithFormat:@"%@",[chartInfo objectForKey:@"med1_total"]];
            cell.medValue2.text = [NSString stringWithFormat:@"%@",[chartInfo objectForKey:@"med2_total"]];
            cell.medValue3.text = [NSString stringWithFormat:@"%@",[chartInfo objectForKey:@"med3_total"]];
            cell.medValue4.text = [NSString stringWithFormat:@"%@",[chartInfo objectForKey:@"med4_total"]];
            cell.medValue5.text = [NSString stringWithFormat:@"%@",[chartInfo objectForKey:@"med5_total"]];
            cell.medValue6.text = [NSString stringWithFormat:@"%@",[chartInfo objectForKey:@"med6_total"]];
            cell.medValue7.text = [NSString stringWithFormat:@"%@",[chartInfo objectForKey:@"med7_total"]];
            
            cell.sympName1.text = [NSString stringWithFormat:@"S1 : %@",[chartInfo objectForKey:@"symp_name1"]];
            cell.sympName2.text = [NSString stringWithFormat:@"S2 : %@",[chartInfo objectForKey:@"symp_name2"]];
            cell.sympName3.text = [NSString stringWithFormat:@"S3 : %@",[chartInfo objectForKey:@"symp_name3"]];
            
            cell.sympValue1.text = [NSString stringWithFormat:@"%@ times",[chartInfo objectForKey:@"symp_count1"]];
            cell.sympValue2.text = [NSString stringWithFormat:@"%@ times",[chartInfo objectForKey:@"symp_count2"]];
            cell.sympValue3.text = [NSString stringWithFormat:@"%@ times",[chartInfo objectForKey:@"symp_count3"]];
            
            // Make medicine labels dynamic
            cell.viewMed7.hidden = YES;
            cell.viewMed6.hidden = YES;
            cell.viewMed5.hidden = YES;
            cell.viewMed4.hidden = YES;
            cell.viewMed3.hidden = YES;
            cell.viewMed2.hidden = YES;
            cell.viewMed1.hidden = YES;
            
            cell.viewSymp3.hidden = YES;
            cell.viewSymp2.hidden = YES;
            cell.viewSymp1.hidden = YES;
            
            cell.viewMed1HC.constant = 0;
            cell.viewMed2HC.constant = 0;
            cell.viewMed3HC.constant = 0;
            cell.viewMed4HC.constant = 0;
            cell.viewMed5HC.constant = 0;
            cell.viewMed6HC.constant = 0;
            cell.viewMed7HC.constant = 0;
            
            cell.viewSymp1HC.constant = 0;
            cell.viewSymp2HC.constant = 0;
            cell.viewSymp3HC.constant = 0;
            
            int medCount = 0;
            if ([[chartInfo objectForKey:@"med1_count"] intValue]>0) {
                medCount += 1;
                cell.viewMed1.hidden = NO;
                cell.viewMed1HC.constant = 30;
            }
            if ([[chartInfo objectForKey:@"med2_count"] intValue]>0) {
                medCount += 1;
                cell.viewMed2.hidden = NO;
                cell.viewMed2HC.constant = 30;
            }
            if ([[chartInfo objectForKey:@"med3_count"] intValue]>0) {
                medCount += 1;
                cell.viewMed3.hidden = NO;
                cell.viewMed3HC.constant = 30;
            }
            if ([[chartInfo objectForKey:@"med4_count"] intValue]>0) {
                medCount += 1;
                cell.viewMed4.hidden = NO;
                cell.viewMed4HC.constant = 30;
            }
            if ([[chartInfo objectForKey:@"med5_count"] intValue]>0) {
                medCount += 1;
                cell.viewMed5.hidden = NO;
                cell.viewMed5HC.constant = 30;
            }
            if ([[chartInfo objectForKey:@"med6_count"] intValue]>0) {
                medCount += 1;
                cell.viewMed6.hidden = NO;
                cell.viewMed6HC.constant = 30;
            }
            if ([[chartInfo objectForKey:@"med7_count"] intValue]>0) {
                medCount += 1;
                cell.viewMed7.hidden = NO;
                cell.viewMed7HC.constant = 30;
            }
            
            int sympCount = 0;
            if ([[chartInfo objectForKey:@"symp_count1"] intValue]>0) {
                sympCount += 1;
                cell.viewSymp1.hidden = NO;
                cell.viewSymp1HC.constant = 30;
            }
            if ([[chartInfo objectForKey:@"symp_count2"] intValue]>0) {
                sympCount += 1;
                cell.viewSymp2.hidden = NO;
                cell.viewSymp2HC.constant = 30;
            }
            if ([[chartInfo objectForKey:@"symp_count3"] intValue]>0) {
                sympCount += 1;
                cell.viewSymp3.hidden = NO;
                cell.viewSymp3HC.constant = 30;
            }
            
            cell.hcViewMedSynp.constant = (30+5);
            if (medCount>1 || sympCount>1) {
                if (medCount>=sympCount) {
                    cell.hcViewMedSynp.constant = medCount*(30+5);
                }else {
                    cell.hcViewMedSynp.constant = sympCount*(30+5);
                }
            }
            
            cell.viewMed1HC.constant = 30;
            cell.viewMed1.hidden = NO;
            cell.viewSymp1HC.constant = 30;
            cell.viewSymp1.hidden = NO;
            
            if([[chartInfo objectForKey:@"med1_name"] isEqualToString:@""] || [[chartInfo objectForKey:@"symp_name1"] isEqualToString:@""]){
                
                if([[chartInfo objectForKey:@"med1_name"] isEqualToString:@""]){
                    cell.medName1.text = [NSString stringWithFormat:@"M1 : %s","-"];
                    cell.medValue1.text = [NSString stringWithFormat:@"%s","-"];
                }
                if([[chartInfo objectForKey:@"symp_name1"] isEqualToString:@""]){
                    cell.sympName1.text = [NSString stringWithFormat:@"S1 : %s","-"];
                    cell.sympValue1.text = [NSString stringWithFormat:@"%s","-"];
                }
            }
            
            UISwipeGestureRecognizer *gestureRecognizer1 = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(leftSwipeHandler:)];
            gestureRecognizer1.direction = UISwipeGestureRecognizerDirectionLeft;
            [cell addGestureRecognizer:gestureRecognizer1];
            
            UISwipeGestureRecognizer *gestureRecognizer2 = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(rightSwipeHandler:)];
            gestureRecognizer2.direction = UISwipeGestureRecognizerDirectionRight;
            [cell addGestureRecognizer:gestureRecognizer2];
            
            return cell;
            
        }
        
        //        {
        //            ScatterChartData *scatterData = [[ScatterChartData alloc] init];
        //
        //            NSMutableArray *yVals1 = [[NSMutableArray alloc] init];
        //            for(int i=0; i<[[chartInfo objectForKey:@"normal_temperature_list"] count]; i++)
        //            {
        //                if([[[chartInfo objectForKey:@"normal_temperature_list"] objectAtIndex:i] doubleValue] > 0)
        //                {
        //                    [yVals1 addObject:[[ChartDataEntry alloc] initWithX:i y:[self convertUnit:[[[chartInfo objectForKey:@"normal_temperature_list"] objectAtIndex:i] doubleValue] unit:@"°C" newUnit:[unitInfo objectForKey:@"temperature"]]]];
        //                }
        //            }
        //            NSMutableArray *yVals2 = [[NSMutableArray alloc] init];
        //            for(int i=0; i<[[chartInfo objectForKey:@"temperature_list"] count]; i++)
        //            {
        //                if([[[chartInfo objectForKey:@"temperature_list"] objectAtIndex:i] doubleValue] > 0)
        //                {
        //                    [yVals2 addObject:[[ChartDataEntry alloc] initWithX:i y:[self convertUnit:[[[chartInfo objectForKey:@"temperature_list"] objectAtIndex:i] doubleValue] unit:@"°C" newUnit:[unitInfo objectForKey:@"temperature"]]]];
        //                }
        //            }
        //            NSMutableArray *yVals3 = [[NSMutableArray alloc] init];
        //            for(int i=0; i<[[chartInfo objectForKey:@"cough_list"] count]; i++)
        //            {
        //                if([[[chartInfo objectForKey:@"cough_list"] objectAtIndex:i] doubleValue] > 0)
        //                {
        //                    [yVals3 addObject:[[ChartDataEntry alloc] initWithX:i y:[[[chartInfo objectForKey:@"cough_list"] objectAtIndex:i] doubleValue]]];
        //                }
        //            }
        //            NSMutableArray *yVals4 = [[NSMutableArray alloc] init];
        //            for(int i=0; i<[[chartInfo objectForKey:@"vomit_list"] count]; i++)
        //            {
        //                if([[[chartInfo objectForKey:@"vomit_list"] objectAtIndex:i] doubleValue] > 0)
        //                {
        //                    [yVals4 addObject:[[ChartDataEntry alloc] initWithX:i y:[[[chartInfo objectForKey:@"vomit_list"] objectAtIndex:i] doubleValue]]];
        //                }
        //            }
        //
        //            LineChartData *lineData = [[LineChartData alloc] init];
        //            LineChartDataSet *set1 = [[LineChartDataSet alloc] initWithValues:yVals1];
        //            [set1 setColor:UIColorFromRGB(0xECECED)];
        //            set1.lineDashLengths = @[@3, @4];
        //            set1.highlightLineDashLengths = @[@3, @4];
        //            set1.lineWidth = 3.0;
        //            set1.circleRadius = 0;
        //            set1.drawValuesEnabled = NO;
        //            set1.mode = LineChartModeHorizontalBezier;
        //            [lineData addDataSet:set1];
        //
        //            ScatterChartDataSet *set2 = [[ScatterChartDataSet alloc] initWithValues:yVals2];
        //            [set2 setDrawValuesEnabled:NO];
        //            [set2 setScatterShape:ScatterShapeCircle];
        //            set2.colors = @[UIColorFromRGB(0x8D8082)];
        //            set2.scatterShapeSize = 7;
        //            [scatterData addDataSet:set2];
        //
        //            ScatterChartDataSet *set3 = [[ScatterChartDataSet alloc] initWithValues:yVals3];
        //            [set3 setDrawValuesEnabled:NO];
        //            [set3 setScatterShape:ScatterShapeCircle];
        //            set3.colors = @[UIColorFromRGB(0xDDE6C7)];
        //            set3.scatterShapeSize = 15;
        //            [scatterData addDataSet:set3];
        //
        //            ScatterChartDataSet *set4 = [[ScatterChartDataSet alloc] initWithValues:yVals4];
        //            [set4 setDrawValuesEnabled:NO];
        //            [set4 setScatterShape:ScatterShapeCircle];
        //            set4.colors = @[UIColorFromRGB(0xF5EEB3)];
        //            set4.scatterShapeSize = 15;
        //            [scatterData addDataSet:set4];
        //
        //            CombinedChartData *data = [[CombinedChartData alloc] init];
        //            data.lineData = lineData;
        //            data.scatterData = scatterData;
        //
        //            NSMutableArray *yVals = [[NSMutableArray alloc] init];
        //            for(int i=0; i<[[chartInfo objectForKey:@"med1_list"] count]; i++)
        //            {
        //                [yVals addObject:[[BarChartDataEntry alloc] initWithX:i yValues:@[@([[[chartInfo objectForKey:@"med1_list"] objectAtIndex:i] doubleValue]), @([[[chartInfo objectForKey:@"med2_list"] objectAtIndex:i] doubleValue]), @([[[chartInfo objectForKey:@"med3_list"] objectAtIndex:i] doubleValue]), @([[[chartInfo objectForKey:@"med4_list"] objectAtIndex:i] doubleValue])]]];
        //            }
        //
        //            BarChartDataSet *set = [[BarChartDataSet alloc] initWithValues:yVals];
        //            set.drawValuesEnabled = NO;
        //            set.colors = @[UIColorFromRGB(0xFADFE0), UIColorFromRGB(0xE7D7CD), UIColorFromRGB(0xDBE3EA), UIColorFromRGB(0xE6DCE5)];
        //
        //            if(selectedIndex == 1)
        //            {
        //                ActivityMedicalWeekChartTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ActivityMedicalWeekChartTableViewCell"];
        //                if(!cell)
        //                {
        //                    [tableView registerNib:[UINib nibWithNibName:@"ActivityMedicalWeekChartTableViewCell" bundle:nil] forCellReuseIdentifier:@"ActivityMedicalWeekChartTableViewCell"];
        //                    cell = [tableView dequeueReusableCellWithIdentifier:@"ActivityMedicalWeekChartTableViewCell"];
        //                }
        //
        //                ChartYAxis *leftAxis2 = cell.temperatureChartView.leftAxis;
        //                ChartYAxis *rightAxis2 = cell.temperatureChartView.rightAxis;
        //                leftAxis2.labelCount = rightAxis2.labelCount = 5;
        //                leftAxis2.axisMaximum = rightAxis2.axisMaximum = [[chartInfo objectForKey:@"temp_max"] intValue];
        //                leftAxis2.axisMinimum = rightAxis2.axisMinimum = [[chartInfo objectForKey:@"temp_min"] intValue];
        //
        //
        //
        //                /*ChartXAxis *xAxis = cell.chartView.xAxis;
        //                ChartYAxis *rightAxis = cell.chartView.rightAxis;
        //                ChartXAxis *xAxis2 = cell.temperatureChartView.xAxis;
        //                ChartYAxis *leftAxis2 = cell.temperatureChartView.leftAxis;
        //                ChartYAxis *rightAxis2 = cell.temperatureChartView.rightAxis;
        //
        //                if(selectedIndex == 1)
        //                {
        //                    leftAxis2.maxWidth = 45;
        //                    leftAxis2.minWidth = 45;
        //                    rightAxis2.maxWidth = 0;
        //                    rightAxis2.minWidth = 0;
        //                    xAxis.labelCount = xAxis2.labelCount = 7;
        //                    xAxis.drawGridLinesEnabled = NO;
        //                    xAxis.valueFormatter = [[ChartAxisValueFormatter alloc] initForChart:cell.chartView type:ChartAxisTypeForWeek];
        //                }
        //                else
        //                {
        //                    leftAxis2.maxWidth = 25;
        //                    leftAxis2.minWidth = 25;
        //                    rightAxis2.maxWidth = 35;
        //                    rightAxis2.minWidth = 35;
        //                    xAxis.labelCount = xAxis2.labelCount = 6;
        //                    xAxis.valueFormatter = [[ChartAxisValueFormatter alloc] initForChart:cell.chartView type:ChartAxisTypeForMonth];
        //                }
        //
        //                leftAxis2.labelCount = rightAxis.labelCount = 4;
        //                leftAxis2.axisMinimum = rightAxis.axisMinimum = [[chartInfo objectForKey:@"temp_min"] intValue];
        //                leftAxis2.axisMaximum = rightAxis.axisMaximum = [[chartInfo objectForKey:@"temp_max"] intValue];*/
        //
        //                cell.txtLeftAxis.text = [NSString stringWithFormat:LocalizedString(@"txt_vol", nil), [unitInfo objectForKey:@"medicine"]];
        //                cell.txtRightAxis.text = [NSString stringWithFormat:LocalizedString(@"txt_temp", nil), [unitInfo objectForKey:@"temperature"]];
        //
        //                cell.temperatureChartView.data = data;
        //
        //                NSMutableArray *dataSets = [[NSMutableArray alloc] initWithObjects:set, nil];
        //                BarChartData *data = [[BarChartData alloc] initWithDataSets:dataSets];
        //                cell.chartView.data = data;
        //
        //                cell.txtMed1Name.text = [chartInfo objectForKey:@"med1_name"];
        //                cell.txtMed1Value.text = [chartInfo objectForKey:@"med1_total"];
        //                cell.txtMed2Name.text = [chartInfo objectForKey:@"med2_name"];
        //                cell.txtMed2Value.text = [chartInfo objectForKey:@"med2_total"];
        //                cell.txtMed3Name.text = [chartInfo objectForKey:@"med3_name"];
        //                cell.txtMed3Value.text = [chartInfo objectForKey:@"med3_total"];
        //                cell.txtMed4Name.text = [chartInfo objectForKey:@"med4_name"];
        //                cell.txtMed4Value.text = [chartInfo objectForKey:@"med4_total"];
        //
        //                cell.txtVomit.text = [NSString stringWithFormat:@"x %@", [chartInfo objectForKey:@"vomit_count"]];
        //                cell.txtCough.text = [NSString stringWithFormat:@"x %@", [chartInfo objectForKey:@"cough_count"]];
        //
        //                cell.btnMed1.tag = 1;
        //                cell.btnMed2.tag = 2;
        //                cell.btnMed3.tag = 3;
        //                cell.btnMed4.tag = 4;
        //
        //                [cell.btnMed1 addTarget:self action:@selector(editMedAction:) forControlEvents:UIControlEventTouchUpInside];
        //                [cell.btnMed2 addTarget:self action:@selector(editMedAction:) forControlEvents:UIControlEventTouchUpInside];
        //                [cell.btnMed3 addTarget:self action:@selector(editMedAction:) forControlEvents:UIControlEventTouchUpInside];
        //                [cell.btnMed4 addTarget:self action:@selector(editMedAction:) forControlEvents:UIControlEventTouchUpInside];
        //
        //                UISwipeGestureRecognizer *gestureRecognizer1 = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(leftSwipeHandler:)];
        //                gestureRecognizer1.direction = UISwipeGestureRecognizerDirectionLeft;
        //                [cell addGestureRecognizer:gestureRecognizer1];
        //
        //                UISwipeGestureRecognizer *gestureRecognizer2 = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(rightSwipeHandler:)];
        //                gestureRecognizer2.direction = UISwipeGestureRecognizerDirectionRight;
        //                [cell addGestureRecognizer:gestureRecognizer2];
        //
        //                return cell;
        //            }
        //            else
        //            {
        //                ActivityMedicalMonthChartTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ActivityMedicalMonthChartTableViewCell"];
        //                if(!cell)
        //                {
        //                    [tableView registerNib:[UINib nibWithNibName:@"ActivityMedicalMonthChartTableViewCell" bundle:nil] forCellReuseIdentifier:@"ActivityMedicalMonthChartTableViewCell"];
        //                    cell = [tableView dequeueReusableCellWithIdentifier:@"ActivityMedicalMonthChartTableViewCell"];
        //                }
        //
        //                ChartYAxis *leftAxis2 = cell.temperatureChartView.leftAxis;
        //                ChartYAxis *rightAxis2 = cell.temperatureChartView.rightAxis;
        //                leftAxis2.labelCount = rightAxis2.labelCount = 5;
        //                leftAxis2.axisMaximum = rightAxis2.axisMaximum = [[chartInfo objectForKey:@"temp_max"] intValue];
        //                leftAxis2.axisMinimum = rightAxis2.axisMinimum = [[chartInfo objectForKey:@"temp_min"] intValue];
        //
        //                /*ChartXAxis *xAxis = cell.chartView.xAxis;
        //                ChartYAxis *rightAxis = cell.chartView.rightAxis;
        //                ChartXAxis *xAxis2 = cell.temperatureChartView.xAxis;
        //                ChartYAxis *leftAxis2 = cell.temperatureChartView.leftAxis;
        //                ChartYAxis *rightAxis2 = cell.temperatureChartView.rightAxis;
        //
        //                if(selectedIndex == 1)
        //                {
        //                    leftAxis2.maxWidth = 45;
        //                    leftAxis2.minWidth = 45;
        //                    rightAxis2.maxWidth = 0;
        //                    rightAxis2.minWidth = 0;
        //                    xAxis.labelCount = xAxis2.labelCount = 7;
        //                    xAxis.drawGridLinesEnabled = NO;
        //                    xAxis.valueFormatter = [[ChartAxisValueFormatter alloc] initForChart:cell.chartView type:ChartAxisTypeForWeek];
        //                }
        //                else
        //                {
        //                    leftAxis2.maxWidth = 25;
        //                    leftAxis2.minWidth = 25;
        //                    rightAxis2.maxWidth = 35;
        //                    rightAxis2.minWidth = 35;
        //                    xAxis.labelCount = xAxis2.labelCount = 6;
        //                    xAxis.valueFormatter = [[ChartAxisValueFormatter alloc] initForChart:cell.chartView type:ChartAxisTypeForMonth];
        //                }
        //
        //                leftAxis2.labelCount = rightAxis.labelCount = 4;
        //                leftAxis2.axisMinimum = rightAxis.axisMinimum = [[chartInfo objectForKey:@"temp_min"] intValue];
        //                leftAxis2.axisMaximum = rightAxis.axisMaximum = [[chartInfo objectForKey:@"temp_max"] intValue];*/
        //
        //                cell.txtLeftAxis.text = [NSString stringWithFormat:LocalizedString(@"txt_vol", nil), [unitInfo objectForKey:@"medicine"]];
        //                cell.txtRightAxis.text = [NSString stringWithFormat:LocalizedString(@"txt_temp", nil), [unitInfo objectForKey:@"temperature"]];
        //
        //                cell.temperatureChartView.data = data;
        //
        //                NSMutableArray *dataSets = [[NSMutableArray alloc] initWithObjects:set, nil];
        //                BarChartData *data = [[BarChartData alloc] initWithDataSets:dataSets];
        //                cell.chartView.data = data;
        //
        //                cell.lblMedName1.text = [chartInfo objectForKey:@"med1_name"];
        //                cell.lblMedValue1.text = [chartInfo objectForKey:@"med1_total"];
        //                cell.lblMedName2.text = [chartInfo objectForKey:@"med2_name"];
        //                cell.lblMedValue2.text = [chartInfo objectForKey:@"med2_total"];
        //                cell.lblMedName3.text = [chartInfo objectForKey:@"med3_name"];
        //                cell.lblMedValue3.text = [chartInfo objectForKey:@"med3_total"];
        //                cell.lblMedName4.text = [chartInfo objectForKey:@"med4_name"];
        //                cell.lblMedValue4.text = [chartInfo objectForKey:@"med4_total"];
        //
        //                //cell.txtVomit.text = [NSString stringWithFormat:@"x %@", [chartInfo objectForKey:@"vomit_count"]];
        //                //cell.txtCough.text = [NSString stringWithFormat:@"x %@", [chartInfo objectForKey:@"cough_count"]];
        //
        //                //cell.btnMed1.tag = 1;
        //                //cell.btnMed2.tag = 2;
        //                //cell.btnMed3.tag = 3;
        //                //cell.btnMed4.tag = 4;
        //
        //               // [cell.btnMed1 addTarget:self action:@selector(editMedAction:) forControlEvents:UIControlEventTouchUpInside];
        //                //[cell.btnMed2 addTarget:self action:@selector(editMedAction:) forControlEvents:UIControlEventTouchUpInside];
        //                //[cell.btnMed3 addTarget:self action:@selector(editMedAction:) forControlEvents:UIControlEventTouchUpInside];
        //                //[cell.btnMed4 addTarget:self action:@selector(editMedAction:) forControlEvents:UIControlEventTouchUpInside];
        //
        //                UISwipeGestureRecognizer *gestureRecognizer1 = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(leftSwipeHandler:)];
        //                gestureRecognizer1.direction = UISwipeGestureRecognizerDirectionLeft;
        //                [cell addGestureRecognizer:gestureRecognizer1];
        //
        //                UISwipeGestureRecognizer *gestureRecognizer2 = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(rightSwipeHandler:)];
        //                gestureRecognizer2.direction = UISwipeGestureRecognizerDirectionRight;
        //                [cell addGestureRecognizer:gestureRecognizer2];
        //
        //                return cell;
        //
        //                // graph end
        //            }
        //        }
    }
    else
    {
        ActivityTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ActivityTableViewCell"];
        if(!cell)
        {
            [tableView registerNib:[UINib nibWithNibName:@"ActivityTableViewCell" bundle:nil] forCellReuseIdentifier:@"ActivityTableViewCell"];
            cell = [tableView dequeueReusableCellWithIdentifier:@"ActivityTableViewCell"];
        }
        
        if (IS_IPAD) {
            cell.txtTime.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:19.0f];
            cell.txtValue.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:19.0f];
        }
        [cell layoutSubviews];
        
        cell.topSeperator.hidden = YES;
        if (selectedIndex==0 && indexPath.row==0) {
            cell.topSeperator.hidden = NO;
        }
        
        NSString *time = [[[[activityList objectAtIndex:indexPath.section-1] objectForKey:@"list"] objectAtIndex:indexPath.row] objectForKey:@"time"];
        if([[shareObject objectForKey:@"language"] hasPrefix:@"zh"])
        {
            NSArray *timeArray = [time componentsSeparatedByString:@" "];
            time = [NSString stringWithFormat:@"%@ %@", LocalizedString(timeArray[1], nil), timeArray[0]];
        }
        
        cell.txtTime.text = time;
        cell.txtValue.text =  [[[[activityList objectAtIndex:indexPath.section-1] objectForKey:@"list"] objectAtIndex:indexPath.row] objectForKey:@"attribute_value"];
        
        cell.txtNotes.text = @"";
        if (![[[[[activityList objectAtIndex:indexPath.section-1] objectForKey:@"list"] objectAtIndex:indexPath.row] objectForKey:@"notes"] isKindOfClass:[NSNull class]]) {
            
            cell.txtNotes.text =  [[[[activityList objectAtIndex:indexPath.section-1] objectForKey:@"list"] objectAtIndex:indexPath.row] objectForKey:@"notes"];
            
            if ([cell.txtNotes.text length]>0) {
                cell.txtNotes.text = [NSString stringWithFormat:@"Notes : %@", [[[[activityList objectAtIndex:indexPath.section-1] objectForKey:@"list"] objectAtIndex:indexPath.row] objectForKey:@"notes"]];
            }
            
        }
        
        NSString *photoUrl = [[[[activityList objectAtIndex:indexPath.section-1] objectForKey:@"list"] objectAtIndex:indexPath.row] objectForKey:@"photo_url"];
        
        if([photoUrl isEqual:[NSNull null]] || !photoUrl.length){
            photoUrl = [[[[activityList objectAtIndex:indexPath.section-1] objectForKey:@"list"] objectAtIndex:indexPath.row] objectForKey:@"photo_url2"];
            if([photoUrl isEqual:[NSNull null]] || !photoUrl.length){
                photoUrl = [[[[activityList objectAtIndex:indexPath.section-1] objectForKey:@"list"] objectAtIndex:indexPath.row] objectForKey:@"photo_url3"];
            }
        }
        
        if((![photoUrl isEqual:[NSNull null]])&&(photoUrl.length>0))
        {
            cell.btnPhoto.hidden = NO;
            cell.btnPhoto.userInteractionEnabled = YES;
            [cell.btnPhoto sd_setImageWithURL:[NSURL URLWithString:photoUrl] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"dummy.png"]];
            [cell.btnPhoto addTarget:self action:@selector(showPhotoAction:) forControlEvents:UIControlEventTouchUpInside];
        }
        else {
            
            NSString *videoUrl = [[[[activityList objectAtIndex:indexPath.section-1] objectForKey:@"list"] objectAtIndex:indexPath.row] objectForKey:@"video_url"];
            
            if([videoUrl isEqual:[NSNull null]] || !videoUrl.length){
                videoUrl = [[[[activityList objectAtIndex:indexPath.section-1] objectForKey:@"list"] objectAtIndex:indexPath.row] objectForKey:@"video_url2"];
                if([videoUrl isEqual:[NSNull null]] || !videoUrl.length){
                    videoUrl = [[[[activityList objectAtIndex:indexPath.section-1] objectForKey:@"list"] objectAtIndex:indexPath.row] objectForKey:@"video_url3"];
                }
            }
            
            if ((![videoUrl isEqual:[NSNull null]])&&(videoUrl.length>0)) {
                cell.btnPhoto.hidden = NO;
                cell.btnPhoto.userInteractionEnabled = YES;
                // Generate Thumbnail Image.
                //UIImage *tImg = [COMMON_HELPER generateThumbnailImageFromVideoWithUrl:[NSURL URLWithString:videoUrl]];
                //[cell.btnPhoto setImage:tImg forState:UIControlStateNormal];
                //[cell.btnPhoto sd_setImageWithURL:[NSURL URLWithString:videoUrl] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"dummy.png"]];
                [cell.btnPhoto setImage:[UIImage imageNamed:@"video_player_icon"] forState:UIControlStateNormal];
                cell.btnPhoto.backgroundColor = [UIColor darkTextColor];
                [cell.btnPhoto addTarget:self action:@selector(showPhotoAction:) forControlEvents:UIControlEventTouchUpInside];
            }
            else {
                cell.btnPhoto.hidden = YES;
                cell.btnPhoto.userInteractionEnabled = NO;
            }
        }
        
        return cell;
    }
}

#pragma mark - Custom methods
-(double)setAxixNoForMedical:(double)axixNo
{
    axixNo += 3;
    return axixNo;
}

#pragma mark - SwipeGesture methods RS
-(void)rightSwipeHandler: (UISwipeGestureRecognizer *)gesture
{
    NSLog(@"%lu" , (unsigned long)gesture.direction);
    
    if (selectedIndex == 0) {
        
        NSDateComponents *dayComponent = [[NSDateComponents alloc] init];
        dayComponent.day = -1;
        NSCalendar *theCalendar = [NSCalendar currentCalendar];
        NSDate *nextDate = [theCalendar dateByAddingComponents:dayComponent toDate:selectedDay options:0];
        selectedDay = nextDate;
        [self refreshAction];
        
        //        NSLog(@"nextDate: %@ ...", nextDate);
        //        //selectedDay = selectedDate;
        //        formatter.dateFormat = @"d MMM yyyy";
        //        selectedHeader = [formatter stringFromDate:selectedDay];
        //        [self refreshAction];
        
    }else if (selectedIndex == 1){
        
        if ((selectedWeek>=0)&&(selectedWeek<weekList.count-1)) {
            selectedWeek += 1;
            [self refreshAction];
        }
        
        //        //selectedWeek = selectedIndex2;
        //        //NSLog(@"selectedWeek=%d", selectedWeek);
        //        formatter.dateFormat = @"d MMM yyyy";
        //        selectedHeader = [formatter stringFromDate:selectedDay];
        //        NSLog(@"selectedWeek=%@", selectedHeader);
        
    }else if (selectedIndex == 2){
        
        if ((selectedMonth>=0)&&(selectedMonth<monthList.count-1)) {
            selectedMonth += 1;
            [self refreshAction];
        }
        //        // selectedMonth = selectedIndex2;
        //        formatter.dateFormat = @"d MMM yyyy";
        //        selectedHeader = [formatter stringFromDate:selectedDay];
        //        [self refreshAction];
        
    }
    
}

-(void)leftSwipeHandler: (UISwipeGestureRecognizer *)gesture
{
    NSLog(@"%lu" , (unsigned long)gesture.direction);
    
    if (selectedIndex == 0) {
        
        BOOL isToday = [[NSCalendar currentCalendar] isDateInToday:selectedDay];
        if (!isToday) {
            NSDateComponents *dayComponent = [[NSDateComponents alloc] init];
            dayComponent.day = 1;
            NSCalendar *theCalendar = [NSCalendar currentCalendar];
            NSDate *nextDate = [theCalendar dateByAddingComponents:dayComponent toDate:selectedDay options:0];
            selectedDay = nextDate;
            [self refreshAction];
        }
        
        //        NSLog(@"nextDate: %@ ...", nextDate);
        //        //selectedDay = selectedDate;
        //        formatter.dateFormat = @"d MMM yyyy";
        //        selectedHeader = [formatter stringFromDate:selectedDay];
        //        [self refreshAction];
        
    }else if (selectedIndex == 1){
        
        if (selectedWeek > 0) {
            selectedWeek -= 1;
            [self refreshAction];
        }
        
        //        //selectedWeek = selectedIndex2;
        //        //NSLog(@"selectedWeek=%d", selectedWeek);
        //        formatter.dateFormat = @"d MMM yyyy";
        //        selectedHeader = [formatter stringFromDate:selectedDay];
        //        NSLog(@"selectedWeek=%@", selectedHeader);
        //        [self refreshAction];
        
    }else if (selectedIndex == 2){
        
        if (selectedMonth > 0) {
            selectedMonth -= 1;
            [self refreshAction];
        }
        //       // selectedMonth = selectedIndex2;
        //        formatter.dateFormat = @"d MMM yyyy";
        //        selectedHeader = [formatter stringFromDate:selectedDay];
        //        [self refreshAction];
        
    }
    
}

@end
