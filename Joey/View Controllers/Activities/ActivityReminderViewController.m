//
//  ActivityReminderViewController.m
//  Joey
//
//  Created by csl on 26/5/2017.
//  Copyright © 2017 Auxilia. All rights reserved.
//

#import "AppDelegate.h"
#import "ActivityReminderViewController.h"
#import "ActivityReminderTableViewCell.h"

@interface ActivityReminderViewController () <UIPickerViewDelegate, UIPickerViewDataSource>
{
    NSUserDefaults *shareObject;
    AppDelegate *appDelegate;
    NSDateFormatter *formatter;
    NSMutableArray *reminderList;
    NSMutableArray *selectedRecurrenceList;
    NSString *selectedBabyName;
    NSString *selectedType;
    NSString *selectedRecurrence;
    NSDate *selectedTime;
    
    BOOL saveCheck;
    
    NSMutableArray *hourList;
    NSMutableArray *minuteList;
    NSMutableArray *amPmList;
    NSString *selectedHour;
    NSString *selectedMinute;
    NSString *selectedHalf;
    
}
@end

@implementation ActivityReminderViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    shareObject = [NSUserDefaults standardUserDefaults];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    formatter = [[NSDateFormatter alloc] init];
    [formatter setTimeZone:[NSTimeZone localTimeZone]];
    [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
    
    reminderList = [[NSMutableArray alloc] init];
    selectedRecurrenceList = [[NSMutableArray alloc] init];
    
    hourList = [[NSMutableArray alloc] init];
    for(int i=1; i<=12; i++) [hourList addObject:[NSString stringWithFormat:@"%02d", i]];
    minuteList = [[NSMutableArray alloc] init];
    for(int i=0; i<60; i++) [minuteList addObject:[NSString stringWithFormat:@"%02d", i]];

    self.hourPicker.dataSource = self;
    self.minutePicker.dataSource = self;
    self.amPmPicker.dataSource = self;
    self.hourPicker.delegate = self;
    self.minutePicker.delegate = self;
    self.amPmPicker.delegate = self;

    amPmList = [[NSMutableArray alloc] init];
    [amPmList addObject:@"AM"];
    [amPmList addObject:@"PM"];
    
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond fromDate:[NSDate date]];

    int hr = components.hour;
    int min = components.minute;
    int amPm = 0;
    
    selectedHour = [NSString stringWithFormat:@"%d",hr];
    selectedMinute = [NSString stringWithFormat:@"%d",min];
    selectedHalf = @"AM";
    
    if (components.hour > 12) {
        hr = hr%12;
        selectedHalf = @"PM";
        amPm = 1;
        selectedHour = [NSString stringWithFormat:@"%d",hr];
    }
    
    if (hr>0) {
        hr -= 1;
    }
    
    [self.hourPicker selectRow:hr inComponent:0 animated:YES];
    [self.minutePicker selectRow:min inComponent:0 animated:YES];
    [self.amPmPicker selectRow:amPm inComponent:0 animated:YES];
    
    [self.navigationController setNavigationBarHidden:NO];
    [self.navigationController.navigationBar setBarTintColor:UIColorFromRGB(0xEEE8E4)];
    [self.navigationController.navigationBar setTranslucent:NO];
    [self.navigationItem setTitle:LocalizedString(@"title_reminders", nil)];
    [self customBackButton];
    
    self.lblTitle.text = LocalizedString(@"txt_title", nil);
    self.lblTime.text = LocalizedString(@"txt_time", nil);
    self.lblRecurrence.text = LocalizedString(@"txt_recurrence", nil);
    [self.btnAddReminder setTitle:LocalizedString(@"btn_add_reminder", nil) forState:UIControlStateNormal];
    self.btnAddReminder.layer.cornerRadius = 5.f;
    
    if (IS_IPAD) {
        self.lblTitle.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:22.0f];
        self.lblTime.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:22.0f];
        self.lblRecurrence.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:22.0f];
        
        self.txtTitle.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:22.0f];
        
        self.titleLblHgtConstraint.constant = 40;
        self.timelblHgtConstraint.constant = 40;
        self.recurrrenceLblHgtConstraint.constant = 40;
        
        self.btnMonday.titleLabel.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:22.0f];
        self.btnTuesday.titleLabel.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:22.0f];
        self.btnWednesday.titleLabel.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:22.0f];
        self.btnThursday.titleLabel.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:22.0f];
        self.btnFriday.titleLabel.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:22.0f];
        self.btnSaturday.titleLabel.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:22.0f];
        self.btnSunday.titleLabel.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:22.0f];
        
        self.btnAddReminder.titleLabel.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:25.0f];
    }
    
    selectedBabyName = [self.data objectForKey:@"name"];
    selectedType = [self.data objectForKey:@"type"];
    selectedTime = [NSDate date];
    formatter.dateFormat = @"hh:mm a";
    self.txtTitle.text = LocalizedString(selectedType, nil);
    
    [self getReminderList];
    
    for(UILocalNotification *aNotif in [[UIApplication sharedApplication] scheduledLocalNotifications])
    {
        NSLog(@"aNotif=%@", aNotif);
        NSLog(@"id=%@", [aNotif.userInfo objectForKey:@"id"]);
    }
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    NSLog(@"self.data");
    
    [self.view layoutSubviews];
    self.btnMonday.layer.cornerRadius = self.btnMonday.frame.size.height/2;
    self.btnMonday.layer.masksToBounds = YES;
    self.btnTuesday.layer.cornerRadius = self.btnMonday.frame.size.height/2;
    self.btnTuesday.layer.masksToBounds = YES;
    self.btnWednesday.layer.cornerRadius = self.btnMonday.frame.size.height/2;
    self.btnWednesday.layer.masksToBounds = YES;
    self.btnThursday.layer.cornerRadius = self.btnMonday.frame.size.height/2;
    self.btnThursday.layer.masksToBounds = YES;
    self.btnFriday.layer.cornerRadius = self.btnMonday.frame.size.height/2;
    self.btnFriday.layer.masksToBounds = YES;
    self.btnSaturday.layer.cornerRadius = self.btnMonday.frame.size.height/2;
    self.btnSaturday.layer.masksToBounds = YES;
    self.btnSunday.layer.cornerRadius = self.btnMonday.frame.size.height/2;
    self.btnSunday.layer.masksToBounds = YES;

}

-(void)viewDidAppear:(BOOL)animated
{
    self.btnMonday.layer.cornerRadius = self.btnMonday.frame.size.height/2;
    self.btnMonday.layer.masksToBounds = YES;
    self.btnTuesday.layer.cornerRadius = self.btnMonday.frame.size.height/2;
    self.btnTuesday.layer.masksToBounds = YES;
    self.btnWednesday.layer.cornerRadius = self.btnMonday.frame.size.height/2;
    self.btnWednesday.layer.masksToBounds = YES;
    self.btnThursday.layer.cornerRadius = self.btnMonday.frame.size.height/2;
    self.btnThursday.layer.masksToBounds = YES;
    self.btnFriday.layer.cornerRadius = self.btnMonday.frame.size.height/2;
    self.btnFriday.layer.masksToBounds = YES;
    self.btnSaturday.layer.cornerRadius = self.btnMonday.frame.size.height/2;
    self.btnSaturday.layer.masksToBounds = YES;
    self.btnSunday.layer.cornerRadius = self.btnMonday.frame.size.height/2;
    self.btnSunday.layer.masksToBounds = YES;

}
- (void)customBackButton
{
    if([self.navigationController.viewControllers count] > 1)
    {
        UIImage *imgBack = [UIImage imageNamed:@"nav_btn_back"];
        UIButton *btnBack = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnBack setImage:imgBack forState:UIControlStateNormal];
        btnBack.frame = CGRectMake(0, 0, imgBack.size.width, imgBack.size.height);
        [btnBack addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
        [btnBack setImageEdgeInsets:UIEdgeInsetsMake(0, -16, 0, 16)];
        UIBarButtonItem *backBarItem = [[UIBarButtonItem alloc] initWithCustomView:btnBack];
        self.navigationItem.leftBarButtonItem = backBarItem;
    }
}

- (void)backAction
{
    UIViewController *viewController = [self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count-2];
    viewController.data = [[NSMutableDictionary alloc] initWithDictionary:@{@"save_check":@(saveCheck)}];
    [self.navigationController popViewControllerAnimated:YES];
}

- (BOOL)validateInputs
{
    ALPValidator *validator = nil;
    
    validator = [ALPValidator validatorWithType:ALPValidatorTypeString];
    [validator addValidationToEnsurePresenceWithInvalidMessage:LocalizedString(@"error_reminder_title", nil)];
    [validator validate:self.txtTitle.text];
    if(![validator isValid])
    {
        [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:[[validator errorMessages] objectAtIndex:0] input:self.txtTitle];
        return NO;
    }
    
    return YES;
}

- (void)getReminderList
{
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setValue:@([shareObject integerForKey:@"user_id"]) forKey:@"user_id"];
    [parameters setValue:@([shareObject integerForKey:@"baby_id"]) forKey:@"baby_id"];
    [parameters setValue:selectedType forKey:@"type"];
    
    [[WebServiceManager sharedManager] initWithBaseURL:@"api/getActivityReminderList" parameters:parameters success:^(id result) {
        
        reminderList = (NSMutableArray *)[result valueForKey:@"list"];
        for(int i=0; i<reminderList.count; i++)
        {
            BOOL isExist = NO;
            for(UILocalNotification *notification in [[UIApplication sharedApplication] scheduledLocalNotifications])
            {
                if([[notification.userInfo objectForKey:@"id"] intValue] == [[[reminderList objectAtIndex:i] objectForKey:@"id"] intValue])
                {
                    isExist = YES;
                }
            }
            if(!isExist)
            {
                [[reminderList objectAtIndex:i] setObject:@(1) forKey:@"disabled_check"];
                
                NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
                [parameters setValue:@([shareObject integerForKey:@"user_id"]) forKey:@"user_id"];
                [parameters setValue:@([shareObject integerForKey:@"baby_id"]) forKey:@"baby_id"];
                [parameters setValue:@([[[reminderList objectAtIndex:i] objectForKey:@"id"] intValue]) forKey:@"id"];
                [parameters setValue:@(1) forKey:@"disabled_check"];
                
                [[WebServiceManager sharedManager] initWithBaseURL:@"api/editActivityReminder" parameters:parameters success:^(id result) {
                    
                    [self.tableView reloadData];
                    
                } failure:^(NSError *error) {
                }];
            }
        }
        
        [self.tableView reloadData];
        
    } failure:^(NSError *error) {
        
    }];
}

- (void)getSwitchAction:(UISwitch *)sender
{
    if(sender.isOn)
    {
        formatter.dateFormat = @"yyyy-MM-dd HH:mm:ss ZZZ";
        NSDate *time = [formatter dateFromString:[[reminderList objectAtIndex:(int)sender.tag] objectForKey:@"time"]];
        
        formatter.dateFormat = @"HH";
        int hour = [[formatter stringFromDate:time] intValue];
        formatter.dateFormat = @"mm";
        int minute = [[formatter stringFromDate:time] intValue];
        NSLog(@"hour=%d, minute=%d", hour, minute);
        
        if(([[[reminderList objectAtIndex:(int)sender.tag] objectForKey:@"recurrence"] isKindOfClass:[NSNull class]]) || ([[[reminderList objectAtIndex:(int)sender.tag] objectForKey:@"recurrence"] length] == 0))
        {
            NSLog(@"time1=%@", time);
            NSLog(@"time2=%@", [NSDate date]);
            if([time compare:[NSDate date]] == NSOrderedAscending)
            {
                time = [selectedTime dateByAddingTimeInterval:24*60*60];
            }
            NSLog(@"time1=%@", time);
            
            NSCalendar *calendar = [NSCalendar currentCalendar];
            NSDateComponents *components = [calendar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitWeekOfYear | NSCalendarUnitWeekday fromDate:time];
            components.hour = hour;
            components.minute = minute;
            
            UILocalNotification *local = [[UILocalNotification alloc] init];
            local.fireDate = [calendar dateFromComponents:components];
            NSLog(@"local.fireDate=%@", local.fireDate);
            local.timeZone = [NSTimeZone defaultTimeZone];
            local.alertTitle = [NSString stringWithFormat:@"[%@] - %@", selectedBabyName, LocalizedString(@"txt_reminder", nil)];
            local.alertBody = self.txtTitle.text;
            local.soundName = UILocalNotificationDefaultSoundName;
            local.userInfo = [[NSMutableDictionary alloc] initWithDictionary:@{
                                                                               @"id":[[reminderList objectAtIndex:(int)sender.tag] objectForKey:@"id"],
                                                                               @"title":self.txtTitle.text,
                                                                               @"baby_id":@([shareObject integerForKey:@"baby_id"]),
                                                                               @"baby_name":selectedBabyName,
                                                                               @"type":selectedType,
                                                                               @"repeat":@(0)}
                              ];
            [[UIApplication sharedApplication] scheduleLocalNotification:local];
        }
        else
        {
            NSArray *recurrenceList = [[[reminderList objectAtIndex:(int)sender.tag] objectForKey:@"recurrence"] componentsSeparatedByString:@","];
            
            for(int i=0; i<recurrenceList.count; i++)
            {
                int weekday = [[recurrenceList objectAtIndex:i] intValue]+1;
                NSCalendar *calendar = [NSCalendar currentCalendar];
                NSDateComponents *components = [calendar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitWeekOfYear | NSCalendarUnitWeekday fromDate: selectedTime];
                
                if(weekday != 0 && components.weekday > weekday)
                {
                    components.weekOfYear += 1;
                }
                components.weekday = weekday;
                components.hour = hour;
                components.minute = minute;
                
                UILocalNotification *local = [[UILocalNotification alloc] init];
                local.fireDate = [calendar dateFromComponents:components];
                NSLog(@"local.fireDate=%@", local.fireDate);
                local.timeZone = [NSTimeZone defaultTimeZone];
                local.alertTitle = [NSString stringWithFormat:@"[%@] - %@", selectedBabyName, LocalizedString(@"txt_reminder", nil)];
                local.alertBody = self.txtTitle.text;
                local.soundName = UILocalNotificationDefaultSoundName;
                local.repeatInterval = NSCalendarUnitWeekOfYear;
                local.userInfo = [[NSMutableDictionary alloc] initWithDictionary:@{
                                                                                   @"id":[[reminderList objectAtIndex:(int)sender.tag] objectForKey:@"id"],
                                                                                   @"title":self.txtTitle.text,
                                                                                   @"baby_id":@([shareObject integerForKey:@"baby_id"]),
                                                                                   @"baby_name":selectedBabyName,
                                                                                   @"type":selectedType,
                                                                                   @"repeat":@(1)}
                                  ];
                [[UIApplication sharedApplication] scheduleLocalNotification:local];
            }
        }
    }
    else
    {
        NSMutableArray *cancelList = [[NSMutableArray alloc] init];
        for(UILocalNotification *notification in [[UIApplication sharedApplication] scheduledLocalNotifications])
        {
            NSLog(@"id=%@, %@", [notification.userInfo objectForKey:@"id"], [[reminderList objectAtIndex:(int)sender.tag] objectForKey:@"id"]);
            if([[notification.userInfo objectForKey:@"id"] intValue] == [[[reminderList objectAtIndex:(int)sender.tag] objectForKey:@"id"] intValue])
            {
                [cancelList addObject:notification];
            }
        }
        
        for(int i=0; i<cancelList.count; i++)
        {
            [[UIApplication sharedApplication] cancelLocalNotification:[cancelList objectAtIndex:i]];
        }
        [cancelList removeAllObjects];
    }
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setValue:@([shareObject integerForKey:@"user_id"]) forKey:@"user_id"];
    [parameters setValue:@([shareObject integerForKey:@"baby_id"]) forKey:@"baby_id"];
    [parameters setValue:@([[[reminderList objectAtIndex:(int)sender.tag] objectForKey:@"id"] intValue]) forKey:@"id"];
    [parameters setValue:sender.isOn?@(0):@(1) forKey:@"disabled_check"];
    
    [[WebServiceManager sharedManager] initWithBaseURL:@"api/editActivityReminder" parameters:parameters success:^(id result) {
        
        saveCheck = YES;
        
    } failure:^(NSError *error) {
    }];
}

- (void)deleteReminderAction:(UIButton *)sender
{
    [self showConfirmAlert:LocalizedString(@"alert_are_you_sure", nil) message:@"" callback:^(UIAlertAction *action) {

        NSMutableArray *cancelList = [[NSMutableArray alloc] init];
        for(UILocalNotification *notification in [[UIApplication sharedApplication] scheduledLocalNotifications])
        {
            NSLog(@"id=%@, %@", [notification.userInfo objectForKey:@"id"], [[reminderList objectAtIndex:(int)sender.tag] objectForKey:@"id"]);
            if([[notification.userInfo objectForKey:@"id"] intValue] == [[[reminderList objectAtIndex:(int)sender.tag] objectForKey:@"id"] intValue])
            {
                [cancelList addObject:notification];
            }
        }
        
        for(int i=0; i<cancelList.count; i++)
        {
            [[UIApplication sharedApplication] cancelLocalNotification:[cancelList objectAtIndex:i]];
        }
        [cancelList removeAllObjects];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setValue:@([shareObject integerForKey:@"user_id"]) forKey:@"user_id"];
        [parameters setValue:@([shareObject integerForKey:@"baby_id"]) forKey:@"baby_id"];
        [parameters setValue:@([[[reminderList objectAtIndex:(int)sender.tag] objectForKey:@"id"] intValue]) forKey:@"id"];
        
        [[WebServiceManager sharedManager] initWithBaseURL:@"api/deleteActivityReminder" parameters:parameters success:^(id result) {
            
            saveCheck = YES;
            [self getReminderList];
            
        } failure:^(NSError *error) {
        }];
    }];
}

//- (IBAction)getTimeAction:(id)sender
//{
//    [ActionSheetDatePicker showPickerWithTitle:@"" datePickerMode:UIDatePickerModeTime selectedDate:selectedTime minimumDate:nil maximumDate:nil doneBlock:^(ActionSheetDatePicker *picker, id selectedDate, id origin) {
//
//        [[NSCalendar currentCalendar] rangeOfUnit:NSCalendarUnitMinute startDate:&selectedDate interval:NULL forDate:selectedDate];
//        selectedTime = selectedDate;
//
//
//        formatter.dateFormat = @"hh:mm a";
//        //self.txtTime.text = [formatter stringFromDate:selectedDate];
//
//        [self.tableView reloadData];
//
//    } cancelBlock:^(ActionSheetDatePicker *picker) {
//
//    } origin:self.view];
//}
//
//- (IBAction)getRecurrenceAction:(id)sender
//{
//    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Activities" bundle:nil];
//    UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"ActivityReminderRecurrenceViewController"];
//    viewController.data = [[NSMutableDictionary alloc] initWithDictionary:@{@"recurrence_list":selectedRecurrenceList}];
//    [self.navigationController pushViewController:viewController animated:YES];
//}

- (IBAction)addReminderAction:(id)sender
{
    NSInteger selHour = [selectedHour integerValue];
    NSInteger selMinute = [selectedMinute integerValue];
    if ([selectedHalf isEqualToString:@"PM"]) {
        selHour += 12;
    }

    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitWeekOfYear | NSCalendarUnitWeekday fromDate: [NSDate date]];
    components.hour = selHour;
    components.minute = selMinute;
    
    selectedTime = [calendar dateFromComponents:components];
    
    [selectedRecurrenceList removeAllObjects];
    if (self.btnSunday.selected) {
        [selectedRecurrenceList addObject:@0];
    }if (self.btnMonday.selected) {
        [selectedRecurrenceList addObject:@1];
    }if (self.btnTuesday.selected) {
        [selectedRecurrenceList addObject:@2];
    }if (self.btnWednesday.selected) {
        [selectedRecurrenceList addObject:@3];
    }if (self.btnThursday.selected) {
        [selectedRecurrenceList addObject:@4];
    }if (self.btnFriday.selected) {
        [selectedRecurrenceList addObject:@5];
    }if (self.btnSaturday.selected) {
        [selectedRecurrenceList addObject:@6];
    }
    
    if([self validateInputs])
    {
        
        [SVProgressHUD show];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setValue:@([shareObject integerForKey:@"user_id"]) forKey:@"user_id"];
        [parameters setValue:@([shareObject integerForKey:@"baby_id"]) forKey:@"baby_id"];
        [parameters setValue:self.txtTitle.text forKey:@"name"];
        [parameters setValue:selectedType forKey:@"type"];
        [parameters setValue:selectedTime forKey:@"time"];
        [parameters setValue:[selectedRecurrenceList componentsJoinedByString:@","] forKey:@"recurrence"];
        
        [[WebServiceManager sharedManager] initWithBaseURL:@"api/addActivityReminder" parameters:parameters success:^(id result) {
            
            if([[result objectForKey:@"success"] boolValue])
            {
                formatter.dateFormat = @"HH";
                int hour = [[formatter stringFromDate:selectedTime] intValue];
                formatter.dateFormat = @"mm";
                int minute = [[formatter stringFromDate:selectedTime] intValue];
                NSLog(@"hour=%d, minute=%d", hour, minute);
                
                if(selectedRecurrenceList.count == 0)
                {
                    if([selectedTime compare:[NSDate date]] == NSOrderedAscending)
                    {
                        selectedTime = [selectedTime dateByAddingTimeInterval:24*60*60];
                    }
                    
                    NSCalendar *calendar = [NSCalendar currentCalendar];
                    NSDateComponents *components = [calendar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitWeekOfYear | NSCalendarUnitWeekday fromDate: selectedTime];
                    components.hour = hour;
                    components.minute = minute;
                    
                    UILocalNotification *local = [[UILocalNotification alloc] init];
                    local.fireDate = [calendar dateFromComponents:components];
                    NSLog(@"local.fireDate=%@", local.fireDate);
                    local.timeZone = [NSTimeZone defaultTimeZone];
                    local.alertTitle = [NSString stringWithFormat:@"[%@] - %@", selectedBabyName, LocalizedString(@"txt_reminder", nil)];
                    local.alertBody = self.txtTitle.text;
                    local.soundName = UILocalNotificationDefaultSoundName;
                    local.userInfo = [[NSMutableDictionary alloc] initWithDictionary:@{
                                                                                       @"id":[result objectForKey:@"id"],
                                                                                       @"title":self.txtTitle.text,
                                                                                       @"baby_id":@([shareObject integerForKey:@"baby_id"]),
                                                                                       @"baby_name":selectedBabyName,
                                                                                       @"type":selectedType,
                                                                                       @"repeat":@(0)}
                                      ];
                    [[UIApplication sharedApplication] scheduleLocalNotification:local];
                }
                
                for(int i=0; i<selectedRecurrenceList.count; i++)
                {
                    int weekday = [[selectedRecurrenceList objectAtIndex:i] intValue]+1;
                    NSCalendar *calendar = [NSCalendar currentCalendar];
                    NSDateComponents *components = [calendar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitWeekOfYear | NSCalendarUnitWeekday fromDate: selectedTime];
                    
                    if(weekday != 0 && components.weekday > weekday)
                    {
                        components.weekOfYear += 1;
                    }
                    components.weekday = weekday;
                    components.hour = hour;
                    components.minute = minute;
                    
                    UILocalNotification *local = [[UILocalNotification alloc] init];
                    local.fireDate = [calendar dateFromComponents:components];
                    NSLog(@"local.fireDate=%@", local.fireDate);
                    local.timeZone = [NSTimeZone defaultTimeZone];
                    local.alertTitle = [NSString stringWithFormat:@"[%@] - %@", selectedBabyName, LocalizedString(@"txt_reminder", nil)];
                    local.alertBody = self.txtTitle.text;
                    local.soundName = UILocalNotificationDefaultSoundName;
                    local.repeatInterval = NSCalendarUnitWeekOfYear;
                    local.userInfo = [[NSMutableDictionary alloc] initWithDictionary:@{
                                                                                       @"id":[result objectForKey:@"id"],
                                                                                       @"title":self.txtTitle.text,
                                                                                       @"baby_id":@([shareObject integerForKey:@"baby_id"]),
                                                                                       @"baby_name":selectedBabyName,
                                                                                       @"type":selectedType,
                                                                                       @"repeat":@(1)}
                                      ];
                    [[UIApplication sharedApplication] scheduleLocalNotification:local];
                }
                
                saveCheck = YES;
                [self getReminderList];
                [self.view endEditing:YES];
            }
            
        } failure:^(NSError *error) {
        }];
    }
}

-(IBAction)didSelectDay:(UIButton *)sender
{
    sender.selected = !sender.isSelected;
    if (sender.selected)
    {
        sender.titleLabel.textColor = [UIColor whiteColor];
        sender.backgroundColor = UIColorFromRGB(0x827577);
    }
    else
    {
        sender.titleLabel.textColor = UIColorFromRGB(0x827577);
        sender.backgroundColor = UIColorFromRGB(0xBFB5AE);
    }
}

-(NSString *)formatDateTime:(NSDate *)sDate
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    //[dateFormatter setDateFormat:@"E d MMM, yyyy hh:mm a"];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss ZZZ"];
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    [dateFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
    NSString *dateStr = [dateFormatter stringFromDate:sDate];
    NSLog(@"%@",dateStr);
    return dateStr;
    
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if(reminderList.count == 0) return 0;
    return 1;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return LocalizedString(@"header_reminders", nil);
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return reminderList.count;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UILabel *txtHeader;
    UIView *headerView;
    if (IS_IPAD) {
        txtHeader = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, tableView.bounds.size.width-20, 50)];
        txtHeader.font = [UIFont fontWithName:@"SFUIDisplay-Bold" size:22.0f];
        headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 50)];
    }
    else{
        txtHeader = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, tableView.bounds.size.width-20, 40)];
        txtHeader.font = [UIFont fontWithName:@"SFUIDisplay-Bold" size:16.0f];
        headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 40)];
    }
    
    txtHeader.textColor = UIColorFromRGB(0x8D8082);
    txtHeader.text = [self tableView:tableView titleForHeaderInSection:section];
    headerView.backgroundColor = UIColorFromRGB(0xEEE8E4);
    [headerView addSubview:txtHeader];
    return headerView;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (IS_IPAD) {
        return 50;
    }
    return 40;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}
- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ActivityReminderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ActivityReminderTableViewCell"];
    if(!cell)
    {
        [tableView registerNib:[UINib nibWithNibName:@"ActivityReminderTableViewCell" bundle:nil] forCellReuseIdentifier:@"ActivityReminderTableViewCell"];
        cell = [tableView dequeueReusableCellWithIdentifier:@"ActivityReminderTableViewCell"];
    }
    
    formatter.dateFormat = @"yyyy-MM-dd HH:mm:ss ZZZ";
    NSDate *time = [formatter dateFromString:[[reminderList objectAtIndex:indexPath.row] objectForKey:@"time"]];
    formatter.dateFormat = @"hh:mm a";
    
    NSString *recurrenceString = @"";
    if((![[[reminderList objectAtIndex:indexPath.row] objectForKey:@"recurrence"] isKindOfClass:[NSNull class]])&&([[[reminderList objectAtIndex:indexPath.row] objectForKey:@"recurrence"] length] > 0))
    {
        NSArray *recurrenceList = [[[reminderList objectAtIndex:indexPath.row] objectForKey:@"recurrence"] componentsSeparatedByString:@","];
        
        for(int i=0; i<recurrenceList.count; i++)
        {
            NSLog(@"recurrenceList=%@", recurrenceList);
            
            int recurrentNo = [[recurrenceList objectAtIndex:i] intValue];
            if(recurrentNo == 0) recurrenceString = [NSString stringWithFormat:@"%@ %@", recurrenceString, LocalizedString(@"sun", nil)];
            if(recurrentNo == 1) recurrenceString = [NSString stringWithFormat:@"%@ %@", recurrenceString, LocalizedString(@"mon", nil)];
            if(recurrentNo == 2) recurrenceString = [NSString stringWithFormat:@"%@ %@", recurrenceString, LocalizedString(@"tue", nil)];
            if(recurrentNo == 3) recurrenceString = [NSString stringWithFormat:@"%@ %@", recurrenceString, LocalizedString(@"wed", nil)];
            if(recurrentNo == 4) recurrenceString = [NSString stringWithFormat:@"%@ %@", recurrenceString, LocalizedString(@"thu", nil)];
            if(recurrentNo == 5) recurrenceString = [NSString stringWithFormat:@"%@ %@", recurrenceString, LocalizedString(@"fri", nil)];
            if(recurrentNo == 6) recurrenceString = [NSString stringWithFormat:@"%@ %@", recurrenceString, LocalizedString(@"sat", nil)];
        }
        if(recurrenceList.count == 7) recurrenceString = LocalizedString(@"txt_every_day", nil);
        
    }
    else recurrenceString = LocalizedString(@"txt_never", nil);
    
    cell.txtName.text = [[reminderList objectAtIndex:indexPath.row] objectForKey:@"name"];
    cell.txtValue.text = [NSString stringWithFormat:LocalizedString(@"txt_recurrence_value", nil), [formatter stringFromDate:time], recurrenceString];
    [cell.switchReminder setOnTintColor:UIColorFromRGB(0xD6E8C2)];
    
    cell.btnDelete.tag = indexPath.row;
    [cell.btnDelete addTarget:self action:@selector(deleteReminderAction:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.switchReminder.tag = indexPath.row;
    [cell.switchReminder setOn:![[[reminderList objectAtIndex:indexPath.row] objectForKey:@"disabled_check"] boolValue]];
    [cell.switchReminder addTarget:self action:@selector(getSwitchAction:) forControlEvents:UIControlEventValueChanged];
    
    return cell;
}

#pragma mark - Pickerview delegate
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if ([pickerView isEqual:self.hourPicker])
    {
        return hourList.count;
    }
    else if ([pickerView isEqual:self.minutePicker])
    {
        return  minuteList.count;
    }
    else {
        return  amPmList.count;
    }
}
-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if ([pickerView isEqual:self.hourPicker])
    {
        return [hourList objectAtIndex:row];
    }
    else if ([pickerView isEqual:self.minutePicker])
    {
        return [minuteList objectAtIndex:row];
    }
    else
    {
        return [amPmList objectAtIndex:row];
    }
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view{
    
    UILabel* tView = (UILabel*)view;
    if (!tView){
        tView = [[UILabel alloc] init];
    }
    
    tView.frame = pickerView.frame;
    tView.backgroundColor = [UIColor clearColor];
    tView.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:16.0f];
    if (IS_IPAD) {
        tView.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:22.0f];
    }
    tView.textColor = UIColorFromRGB(0x786E6F);
    tView.textAlignment = NSTextAlignmentCenter;
    
    NSString *txt  = @"";
    if ([pickerView isEqual:self.hourPicker])
    {
        txt = [hourList objectAtIndex:row];
    }
    else if ([pickerView isEqual:self.minutePicker])
    {
        txt = [minuteList objectAtIndex:row];
    }
    else
    {
        txt = [amPmList objectAtIndex:row];
    }
    
    tView.text = txt;
    
    [[pickerView.subviews objectAtIndex:1] setBackgroundColor:[UIColor clearColor]];
    [[pickerView.subviews objectAtIndex:2] setBackgroundColor:[UIColor clearColor]];
    
    return tView;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    return pickerView.frame.size.height;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if ([pickerView isEqual:self.hourPicker]) {
        selectedHour = [hourList objectAtIndex:[pickerView selectedRowInComponent:component]] ;
    }
    else if ([pickerView isEqual:self.minutePicker]) {
        selectedMinute = [minuteList objectAtIndex:[pickerView selectedRowInComponent:component]] ;
    }
    else if ([pickerView isEqual:self.amPmPicker]) {
        selectedHalf = [amPmList objectAtIndex:[pickerView selectedRowInComponent:component]] ;
    }
}

@end
