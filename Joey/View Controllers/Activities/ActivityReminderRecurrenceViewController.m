//
//  ActivityReminderRecurrenceViewController.m
//  Joey
//
//  Created by csl on 28/5/2017.
//  Copyright © 2017 Auxilia. All rights reserved.
//

#import "ActivityReminderRecurrenceViewController.h"
#import "ActivityReminderRecurrenceTableViewCell.h"

@interface ActivityReminderRecurrenceViewController ()
{
    NSUserDefaults *shareObject;
    NSMutableArray *recurrenceList;
    NSMutableArray *recurrenceValueList;
}
@end

@implementation ActivityReminderRecurrenceViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    shareObject = [NSUserDefaults standardUserDefaults];
    recurrenceList = [[NSMutableArray alloc] initWithObjects:@"txt_every_sunday", @"txt_every_monday", @"txt_every_tuesday", @"txt_every_wednesday", @"txt_every_thursday", @"txt_every_friday", @"txt_every_saturday", nil];
    recurrenceValueList = [[NSMutableArray alloc] init];
    if([self.data objectForKey:@"recurrence_list"]) recurrenceValueList = [self.data objectForKey:@"recurrence_list"];
    
    [self.navigationController setNavigationBarHidden:NO];
    [self.navigationController.navigationBar setBarTintColor:UIColorFromRGB(0xEEE8E4)];
    [self.navigationController.navigationBar setTranslucent:NO];
    [self.navigationItem setTitle:LocalizedString(@"title_recurrence", nil)];
    [self customBackButton];
}

- (void)customBackButton
{
    if([self.navigationController.viewControllers count] > 1)
    {
        UIImage *imgBack = [UIImage imageNamed:@"nav_btn_back"];
        UIButton *btnBack = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnBack setImage:imgBack forState:UIControlStateNormal];
        btnBack.frame = CGRectMake(0, 0, imgBack.size.width, imgBack.size.height);
        [btnBack addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
        [btnBack setImageEdgeInsets:UIEdgeInsetsMake(0, -16, 0, 16)];
        UIBarButtonItem *backBarItem = [[UIBarButtonItem alloc] initWithCustomView:btnBack];
        self.navigationItem.leftBarButtonItem = backBarItem;
    }
}

- (void)backAction
{
    [recurrenceValueList removeAllObjects];
    
    NSString *recurrenceString = @"";
    int count = 0;
    for(int i=0; i<recurrenceList.count; i++)
    {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];
        ActivityReminderRecurrenceTableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
        if(!cell.iconTick.hidden)
        {
            [recurrenceValueList addObject:@((i >= 7)?0:i)];
            
            if(i == 0) recurrenceString = [NSString stringWithFormat:@"%@ %@", recurrenceString, LocalizedString(@"sun", nil)];
            if(i == 1) recurrenceString = [NSString stringWithFormat:@"%@ %@", recurrenceString, LocalizedString(@"mon", nil)];
            if(i == 2) recurrenceString = [NSString stringWithFormat:@"%@ %@", recurrenceString, LocalizedString(@"tue", nil)];
            if(i == 3) recurrenceString = [NSString stringWithFormat:@"%@ %@", recurrenceString, LocalizedString(@"wed", nil)];
            if(i == 4) recurrenceString = [NSString stringWithFormat:@"%@ %@", recurrenceString, LocalizedString(@"thu", nil)];
            if(i == 5) recurrenceString = [NSString stringWithFormat:@"%@ %@", recurrenceString, LocalizedString(@"fri", nil)];
            if(i == 6) recurrenceString = [NSString stringWithFormat:@"%@ %@", recurrenceString, LocalizedString(@"sat", nil)];
            
            count++;
        }
    }
    if(count == 0) recurrenceString = LocalizedString(@"txt_never", nil);
    if(count == recurrenceList.count) recurrenceString = LocalizedString(@"txt_every_day", nil);
    NSLog(@"recurrenceString=%@", recurrenceString);
    NSLog(@"recurrenceValueList=%@", recurrenceValueList);
    
    
    UIViewController *viewController = [self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count-2];
    viewController.data = [[NSMutableDictionary alloc] initWithDictionary:@{@"recurrence":recurrenceString, @"recurrence_list":recurrenceValueList}];
    [self.navigationController popViewControllerAnimated:YES];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return recurrenceList.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}
- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    ActivityReminderRecurrenceTableViewCell *cell = (ActivityReminderRecurrenceTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    cell.iconTick.hidden = !cell.iconTick.hidden;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ActivityReminderRecurrenceTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ActivityReminderRecurrenceTableViewCell"];
    if(!cell)
    {
        [tableView registerNib:[UINib nibWithNibName:@"ActivityReminderRecurrenceTableViewCell" bundle:nil] forCellReuseIdentifier:@"ActivityReminderRecurrenceTableViewCell"];
        cell = [tableView dequeueReusableCellWithIdentifier:@"ActivityReminderRecurrenceTableViewCell"];
    }
    
    cell.txtName.text = LocalizedString([recurrenceList objectAtIndex:indexPath.row], nil);
    cell.iconTick.hidden = YES;
    
    for(int i=0; i<recurrenceValueList.count; i++)
    {
        if(indexPath.row == [[recurrenceValueList objectAtIndex:i] integerValue])
        {
            cell.iconTick.hidden = NO;
            break;
        }
    }
    
    return cell;
}

@end
