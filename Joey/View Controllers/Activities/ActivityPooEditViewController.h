//
//  ActivityPooEditViewController.h
//  Joey
//
//  Created by csl on 5/1/2017.
//  Copyright © 2017 Auxilia. All rights reserved.
//

#import "BaseTableViewController.h"
#import <Photos/Photos.h>
#import <AVFoundation/AVFoundation.h>

@interface ActivityPooEditViewController : BaseTableViewController <UINavigationControllerDelegate, UIImagePickerControllerDelegate>

@property (nonatomic, weak) IBOutlet UIBarButtonItem *btnSave;
@property (nonatomic, weak) IBOutlet UILabel *lblSize;
@property (nonatomic, weak) IBOutlet UIButton *btnSizeSmall;
@property (nonatomic, weak) IBOutlet UIButton *btnSizeMedium;
@property (nonatomic, weak) IBOutlet UIButton *btnSizeLarge;
@property (nonatomic, weak) IBOutlet UILabel *lblTexture;
@property (nonatomic, weak) IBOutlet UIButton *btnTextureHard;
@property (nonatomic, weak) IBOutlet UIButton *btnTextureSoft;
@property (nonatomic, weak) IBOutlet UIButton *btnTextureRunny;
@property (nonatomic, weak) IBOutlet UIButton *btnTextureAlert;
@property (nonatomic, weak) IBOutlet UILabel *lblColor;
@property (nonatomic, weak) IBOutlet UIButton *btnColorYellow;
@property (nonatomic, weak) IBOutlet UIButton *btnColorBrown;
@property (nonatomic, weak) IBOutlet UIButton *btnColorGreen;
@property (nonatomic, weak) IBOutlet UIButton *btnColorBlack;
@property (nonatomic, weak) IBOutlet UIButton *btnColorWhite;
@property (nonatomic, weak) IBOutlet UIButton *btnColorAlert;
@property (nonatomic, weak) IBOutlet UIButton *btnColorDarkBrown;
@property (nonatomic, weak) IBOutlet UILabel *lblDateTime;
@property (nonatomic, weak) IBOutlet UILabel *txtDateTime;
@property (nonatomic, weak) IBOutlet UILabel *lblPhoto;
@property (nonatomic, weak) IBOutlet UILabel *lblNotes;
@property (nonatomic, weak) IBOutlet UITextView *txtNotes;
@property (nonatomic, weak) IBOutlet UIButton *btnDelete;

@property (nonatomic, weak) IBOutlet UIButton *btnPhoto1;
@property (nonatomic, weak) IBOutlet UIButton *btnPhoto2;
@property (nonatomic, weak) IBOutlet UIButton *btnPhoto3;

@property (nonatomic, weak) IBOutlet UIButton *btnTextureLoose;
@property (nonatomic, weak) IBOutlet UIButton *btnTextureDiarrhea;
@property (nonatomic, weak) IBOutlet UIButton *btnTextureMucus;

// Keywords View
@property (nonatomic, weak) IBOutlet UIButton *btnaddKeyword;
@property (weak, nonatomic) IBOutlet UIView *keywordView;
@property (nonatomic, weak) IBOutlet UILabel *keyword1;
@property (nonatomic, weak) IBOutlet UILabel *keyword2;
@property (nonatomic, weak) IBOutlet UILabel *keyword3;
@property (nonatomic, weak) IBOutlet UILabel *keyword4;
@property (nonatomic, weak) IBOutlet UILabel *keyword5;
@property (nonatomic, weak) IBOutlet UILabel *keyword6;
@property (nonatomic, weak) IBOutlet UILabel *keyword7;
@property (nonatomic, weak) IBOutlet UILabel *keyword8;
@property (nonatomic, weak) IBOutlet UILabel *keyword9;

@end
