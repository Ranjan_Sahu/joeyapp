//
//  ActivitySleepViewController.m
//  Joey
//
//  Created by csl on 4/1/2017.
//  Copyright © 2017 Auxilia. All rights reserved.
//

#import "AppDelegate.h"
#import "ActivitySleepViewController.h"
#import "ActivitySleepDayChartTableViewCell.h"
#import "ActivitySleepWeekChartTableViewCell.h"
#import "ActivitySleepMonthChartTableViewCell.h"
#import "ActivitySleepGuideTableViewCell.h"
#import "ChartAxisValueFormatter.h"
#import "ActivityTableViewCell.h"
#import "PhotoViewController.h"

@interface ActivitySleepViewController ()
{
    NSUserDefaults *shareObject;
    AppDelegate *appDelegate;
    NSDateFormatter *formatter;
    NSMutableArray *activityList;
    NSMutableArray *weekList;
    NSMutableArray *weekValueList;
    NSMutableArray *monthList;
    NSMutableArray *monthValueList;
    NSMutableDictionary *unitInfo;
    NSMutableDictionary *chartInfo;
    NSString *selectedHeader;
    NSDate *selectedDay;
    NSInteger selectedWeek;
    NSInteger selectedMonth;
    BOOL saveCheck;
    BOOL isLoading;
    int babyMonthsold;
    int selectedIndex;
}
@end

@implementation ActivitySleepViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    shareObject = [NSUserDefaults standardUserDefaults];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    formatter = [[NSDateFormatter alloc] init];
    [formatter setTimeZone:[NSTimeZone localTimeZone]];
    [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
    
    activityList = [[NSMutableArray alloc] init];
    unitInfo = (NSMutableDictionary *)[NSKeyedUnarchiver unarchiveObjectWithData:[shareObject objectForKey:@"unit_info"]];
    selectedIndex = [[self.data objectForKey:@"selected_index"] intValue];
    selectedDay = [NSDate date];
    babyMonthsold = [[self.data objectForKey:@"monthsold"] intValue];
    
    weekList = [[NSMutableArray alloc] init];
    weekValueList = [[NSMutableArray alloc] init];
    NSDate *firstDayOfWeek = [self getFirstDayOfTheWeekFromDate:[NSDate date]];
    for(int i=0; i<=100; i++)
    {
        NSDate *newDate = [firstDayOfWeek dateByAddingTimeInterval:-i*7*24*60*60];
        int diff = [newDate timeIntervalSinceDate:[NSDate date]]/60;
        if(diff <= 0)
        {
            [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
            formatter.dateFormat = @"yyyyww";
            NSString *week = [formatter stringFromDate:newDate];
            [weekValueList addObject:week];
            
            if([[shareObject objectForKey:@"language"] hasPrefix:@"zh"])
            {
                [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"zh_HK"]];
                [formatter setDateStyle:NSDateFormatterMediumStyle];
                [formatter setTimeStyle:NSDateFormatterShortStyle];
                
                NSString *weekString = [NSString stringWithFormat:@"%@, %@", [NSString stringWithFormat:LocalizedString(@"txt_week_no", nil), [[week substringWithRange:NSMakeRange(4,2)] intValue]], [[formatter stringFromDate:newDate] componentsSeparatedByString:@" "][0]];
                
                [weekList addObject:weekString];
            }
            else
            {
                [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
                formatter.dateFormat = @"ww, dd MMM yyyy";
                [weekList addObject:[NSString stringWithFormat:LocalizedString(@"txt_week_no", nil), [formatter stringFromDate:newDate]]];
            }
            
            
            
            if([[self.data objectForKey:@"selected_week"] isEqualToString:week])
            {
                selectedWeek = i;
            }
        }
    }
    
    monthList = [[NSMutableArray alloc] init];
    monthValueList = [[NSMutableArray alloc] init];
    formatter.dateFormat = @"yyyy";
    int year = [[formatter stringFromDate:[NSDate date]] intValue];
    formatter.dateFormat = @"M";
    int month = [[formatter stringFromDate:[NSDate date]] intValue];
    for(int i=year; i>=year-3; i--)
    {
        for(int j=month; j>=1; j--)
        {
            formatter.dateFormat = @"yyyy-MM-dd";
            NSDate *newDate = [formatter dateFromString:[NSString stringWithFormat:@"%d-%02d-01", i, j]];
            
            if([[shareObject objectForKey:@"language"] hasPrefix:@"zh"])
            {
                [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"zh_HK"]];
                [formatter setDateStyle:NSDateFormatterMediumStyle];
                [formatter setTimeStyle:NSDateFormatterShortStyle];
                
                [monthList addObject:[NSString stringWithFormat:@"%@月", [[formatter stringFromDate:newDate] componentsSeparatedByString:@"月"][0]]];
            }
            else
            {
                formatter.dateFormat = @"MMMM yyyy";
                [monthList addObject:[formatter stringFromDate:newDate]];
            }
            
            formatter.dateFormat = @"yyyyMM";
            NSString *month = [formatter stringFromDate:newDate];
            [monthValueList addObject:month];
            
            if([[self.data objectForKey:@"selected_month"] isEqualToString:month])
            {
                selectedMonth = monthValueList.count-1;
            }
        }
        month = 12;
    }
    
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:UIColorFromRGB(0x8D8082), NSForegroundColorAttributeName, [UIFont fontWithName:@"SFUIDisplay-Medium" size:18.0f], NSFontAttributeName, nil]];
    [self.navigationController.navigationBar setBarTintColor:UIColorFromRGB(0xC5D7E1)];
    [self.navigationController.navigationBar setTranslucent:NO];
    [self.navigationItem setTitle:LocalizedString(@"title_sleep", nil)];
    [self customBackButton];
    
    self.btnAdd.title = LocalizedString(@"btn_add", nil);
    
    self.btnDay.layer.cornerRadius = 3.f;
    self.btnWeek.layer.cornerRadius = 3.f;
    self.btnMonth.layer.cornerRadius = 3.f;
    [self.btnDay setTitle:LocalizedString(@"btn_day", nil) forState:UIControlStateNormal];
    [self.btnWeek setTitle:LocalizedString(@"btn_week", nil) forState:UIControlStateNormal];
    [self.btnMonth setTitle:LocalizedString(@"btn_month", nil) forState:UIControlStateNormal];
    
    if(selectedIndex == 0)
    {
        [self.btnDay setBackgroundColor:UIColorFromRGB(0x8D8082)];
        [self.btnWeek setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
        [self.btnMonth setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    }
    else if(selectedIndex == 1)
    {
        [self.btnDay setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
        [self.btnWeek setBackgroundColor:UIColorFromRGB(0x8D8082)];
        [self.btnMonth setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    }
    else if(selectedIndex == 2)
    {
        [self.btnDay setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
        [self.btnWeek setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
        [self.btnMonth setBackgroundColor:UIColorFromRGB(0x8D8082)];
    }
    
    __weak __typeof(self)weakSelf = self;
    [self.tableView addPullToRefreshWithActionHandler:^{
        [weakSelf refreshAction];
    }];
    [self.tableView addInfiniteScrollingWithActionHandler:^{
        [weakSelf getActivityList];
    }];
    
    [self refreshAction];
    
    if (IS_IPAD) {
        [_btnDay.titleLabel setFont:[UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f]];
        [_btnWeek.titleLabel setFont:[UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f]];
        [_btnMonth.titleLabel setFont:[UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f]];
    }

}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:(BOOL)animated];
    
    NSLog(@"here=%@", self.data);
    NSLog(@"updateActivityCheck=%@", appDelegate.updateActivityCheck?@"YES":@"NO");
    if([[self.data objectForKey:@"save_check"] boolValue] || appDelegate.updateActivityCheck)
    {
        [self.data removeAllObjects];
        [self refreshAction];
    }
}

- (void)viewDidLayoutSubviews
{
    /*if(self.scrollView.contentSize.width == 0)
     {
     self.scrollView.contentSize = CGSizeMake(self.view.frame.size.width*3, 1);
     self.scrollView.contentOffset = CGPointMake(selectedIndex*self.view.frame.size.width, self.scrollView.contentOffset.y);
     }*/
}

- (void)customBackButton
{
    if([self.navigationController.viewControllers count] > 1)
    {
        UIImage *imgBack = [UIImage imageNamed:@"nav_btn_back"];
        UIButton *btnBack = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnBack setImage:imgBack forState:UIControlStateNormal];
        btnBack.frame = CGRectMake(0, 0, imgBack.size.width, imgBack.size.height);
        [btnBack addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
        [btnBack setImageEdgeInsets:UIEdgeInsetsMake(0, -16, 0, 16)];
        UIBarButtonItem *backBarItem = [[UIBarButtonItem alloc] initWithCustomView:btnBack];
        self.navigationItem.leftBarButtonItem = backBarItem;
    }
}

- (void)backAction
{
    UIViewController *viewController = [self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count-2];
    viewController.data = [[NSMutableDictionary alloc] initWithDictionary:@{@"save_check":@(saveCheck)}];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)refreshAction
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    [COMMON_HELPER hideSpinner];
    isLoading = NO;
    self.tableView.showsInfiniteScrolling = NO;
    [self getActivityList];
}

- (void)getActivityList
{
    [COMMON_HELPER showSpinner];
    if(isLoading) return;
    isLoading = YES;
    
    if(selectedIndex == 0)
    {
        formatter.dateFormat = @"yyyy-MM-dd";
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setValue:@([shareObject integerForKey:@"user_id"]) forKey:@"user_id"];
        [parameters setValue:@([shareObject integerForKey:@"baby_id"]) forKey:@"baby_id"];
        [parameters setValue:@(babyMonthsold) forKey:@"monthsold"];
        [parameters setValue:[formatter stringFromDate:selectedDay] forKey:@"day"];
        
        [[WebServiceManager sharedManager] initWithBaseURL:@"api/getSleepActivityListByDay" parameters:parameters success:^(id result) {
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                NSIndexPath *top = [NSIndexPath indexPathForRow:0 inSection:0];
                [self.tableView scrollToRowAtIndexPath:top atScrollPosition:UITableViewScrollPositionTop animated:YES];
            });
            
            NSString *dateString = [result objectForKey:@"header2"];
            [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
            formatter.dateFormat = @"yyyy-MM-dd";
            NSDate *date = [formatter dateFromString:dateString];
            
            if([[shareObject objectForKey:@"language"] hasPrefix:@"zh"])
            {
                [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"zh_HK"]];
                [formatter setDateStyle:NSDateFormatterMediumStyle];
                [formatter setTimeStyle:NSDateFormatterShortStyle];
                
                selectedHeader = [[NSString stringWithFormat:@"%@", [formatter stringFromDate:date]] componentsSeparatedByString:@" "][0];
            }
            else
            {
                [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
                formatter.dateFormat = @"EE d MMM, yyyy";
                selectedHeader = [NSString stringWithFormat:@"%@", [formatter stringFromDate:date]];
            }
            //selectedHeader = [result objectForKey:@"header"];
            activityList = (NSMutableArray *)[result objectForKey:@"list"];
            chartInfo = (NSMutableDictionary *)[result objectForKey:@"info"];
            NSLog(@"chartInfo=%@", chartInfo);
            
            isLoading = NO;
            self.tableView.hidden = NO;
            [self.tableView reloadData];
            [self.tableView.pullToRefreshView stopAnimating];
            result = nil;
            [COMMON_HELPER hideSpinner];
        } failure:^(NSError *error) {
            isLoading = NO;
            [self.tableView.pullToRefreshView stopAnimating];
            [COMMON_HELPER hideSpinner];
        }];
    }
    else if(selectedIndex == 1)
    {
        formatter.dateFormat = @"yyyy-MM-dd";
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setValue:@([shareObject integerForKey:@"user_id"]) forKey:@"user_id"];
        [parameters setValue:@([shareObject integerForKey:@"baby_id"]) forKey:@"baby_id"];
        [parameters setValue:@(babyMonthsold) forKey:@"monthsold"];
        [parameters setValue:[weekValueList objectAtIndex:selectedWeek] forKey:@"week"];
        
        [[WebServiceManager sharedManager] initWithBaseURL:@"api/getSleepActivityListByWeek" parameters:parameters success:^(id result) {
            
            NSString *dateString = [result objectForKey:@"header2"];
            [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
            formatter.dateFormat = @"yyyy-MM-dd";
            NSDate *date = [formatter dateFromString:dateString];
            
            NSDate *firstDayOfWeek = [self getFirstDayOfTheWeekFromDate:date];
            NSDate *endDayOfWeek = [firstDayOfWeek dateByAddingTimeInterval:6*24*60*60];
            
            NSDateComponents *components1 = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:firstDayOfWeek];
            NSDateComponents *components2 = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:endDayOfWeek];
            
            if([[shareObject objectForKey:@"language"] hasPrefix:@"zh"])
            {
                [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"zh_HK"]];
                [formatter setDateStyle:NSDateFormatterMediumStyle];
                [formatter setTimeStyle:NSDateFormatterShortStyle];
                
                NSString *start = [[formatter stringFromDate:firstDayOfWeek]  componentsSeparatedByString:@" "][0];
                NSString *end = [[formatter stringFromDate:endDayOfWeek] componentsSeparatedByString:@" "][0];
                selectedHeader = [NSString stringWithFormat:@"%@ - %@", start, end];
            }
            else
            {
                if([components1 year] == [components2 year])
                {
                    if([components1 month] == [components2 month])
                    {
                        [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
                        formatter.dateFormat = @"ww, dd";
                        NSString *start = [NSString stringWithFormat:LocalizedString(@"txt_week_no", nil), [formatter stringFromDate:firstDayOfWeek]];
                        formatter.dateFormat = @"-dd MMMM yyyy";
                        NSString *end = [formatter stringFromDate:endDayOfWeek];
                        selectedHeader = [NSString stringWithFormat:@"%@%@", start, end];
                    }
                    else
                    {
                        [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
                        formatter.dateFormat = @"ww, dd MMMM";
                        NSString *start = [NSString stringWithFormat:LocalizedString(@"txt_week_no", nil), [formatter stringFromDate:firstDayOfWeek]];
                        formatter.dateFormat = @" - dd MMMM yyyy";
                        NSString *end = [formatter stringFromDate:endDayOfWeek];
                        selectedHeader = [NSString stringWithFormat:@"%@%@", start, end];
                    }
                }
                else
                {
                    [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
                    formatter.dateFormat = @"ww, dd MMMM yyyy";
                    NSString *start = [NSString stringWithFormat:LocalizedString(@"txt_week_no", nil), [formatter stringFromDate:firstDayOfWeek]];
                    formatter.dateFormat = @" - dd MMMM yyyy";
                    NSString *end = [formatter stringFromDate:endDayOfWeek];
                    selectedHeader = [NSString stringWithFormat:@"%@%@", start, end];
                }
            }
            
            //selectedHeader = [result objectForKey:@"header"];
            activityList = (NSMutableArray *)[result objectForKey:@"list"];
            chartInfo = (NSMutableDictionary *)[result objectForKey:@"info"];
            NSLog(@"chartInfo=%@", chartInfo);
            
            isLoading = NO;
            self.tableView.hidden = NO;
            [self.tableView reloadData];
            [self.tableView.pullToRefreshView stopAnimating];
            result = nil;
            [COMMON_HELPER hideSpinner];
        } failure:^(NSError *error) {
            isLoading = NO;
            [self.tableView.pullToRefreshView stopAnimating];
            [COMMON_HELPER hideSpinner];
        }];
    }
    else
    {
        formatter.dateFormat = @"yyyy-MM-dd";
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setValue:@([shareObject integerForKey:@"user_id"]) forKey:@"user_id"];
        [parameters setValue:@([shareObject integerForKey:@"baby_id"]) forKey:@"baby_id"];
        [parameters setValue:@(babyMonthsold) forKey:@"monthsold"];
        [parameters setValue:[monthValueList objectAtIndex:selectedMonth] forKey:@"month"];
        
        [[WebServiceManager sharedManager] initWithBaseURL:@"api/getSleepActivityListByMonth" parameters:parameters success:^(id result) {
            
            NSString *dateString = [result objectForKey:@"header2"];
            [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
            formatter.dateFormat = @"yyyy-MM";
            NSDate *date = [formatter dateFromString:dateString];
            
            if([[shareObject objectForKey:@"language"] hasPrefix:@"zh"])
            {
                [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"zh_HK"]];
                [formatter setDateStyle:NSDateFormatterMediumStyle];
                [formatter setTimeStyle:NSDateFormatterShortStyle];
                
                selectedHeader = [NSString stringWithFormat:@"%@月", [[formatter stringFromDate:date] componentsSeparatedByString:@"月"][0]];
            }
            else
            {
                [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
                formatter.dateFormat = @"MMMM yyyy";
                selectedHeader = [NSString stringWithFormat:@"%@", [formatter stringFromDate:date]];
            }
            //selectedHeader = [result objectForKey:@"header"];
            activityList = (NSMutableArray *)[result objectForKey:@"list"];
            chartInfo = (NSMutableDictionary *)[result objectForKey:@"info"];
            NSLog(@"chartInfo=%@", chartInfo);
            
            isLoading = NO;
            self.tableView.hidden = NO;
            [self.tableView reloadData];
            [self.tableView.pullToRefreshView stopAnimating];
            result = nil;
            [COMMON_HELPER hideSpinner];
        } failure:^(NSError *error) {
            isLoading = NO;
            [self.tableView.pullToRefreshView stopAnimating];
            [COMMON_HELPER hideSpinner];
        }];
    }
}

- (void)getDatePickerAction
{
    NSDate *minimumDate = [[NSDate date] dateByAddingTimeInterval:-365*24*60*60];
    NSDate *maximumDate = [NSDate date];
    [ActionSheetDatePicker showPickerWithTitle:@"" datePickerMode:UIDatePickerModeDate selectedDate:selectedDay minimumDate:minimumDate maximumDate:maximumDate doneBlock:^(ActionSheetDatePicker *picker, id selectedDate, id origin) {
        
        selectedDay = selectedDate;
        formatter.dateFormat = @"d MMM yyyy";
        selectedHeader = [formatter stringFromDate:selectedDay];
        
        [self refreshAction];
        
    } cancelBlock:^(ActionSheetDatePicker *picker) {
        
    } origin:self.view];
}

- (void)getWeekPickerAction
{
    [ActionSheetStringPicker showPickerWithTitle:@"" rows:weekList initialSelection:selectedWeek doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex2, id selectedValue) {
        
        selectedWeek = selectedIndex2;
        
        formatter.dateFormat = @"d MMM yyyy";
        selectedHeader = [formatter stringFromDate:selectedDay];
        
        [self refreshAction];
        
    } cancelBlock:^(ActionSheetStringPicker *picker) {
        
    } origin:self.view];
}

- (void)getMonthPickerAction
{
    [ActionSheetStringPicker showPickerWithTitle:@"" rows:monthList initialSelection:selectedMonth doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex2, id selectedValue) {
        
        selectedMonth = selectedIndex2;
        formatter.dateFormat = @"d MMM yyyy";
        selectedHeader = [formatter stringFromDate:selectedDay];
        
        [self refreshAction];
        
    } cancelBlock:^(ActionSheetStringPicker *picker) {
        
    } origin:self.view];
}

- (void)showPhotoAction:(UIButton *)button
{
    CGPoint buttonPosition = [button convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
    
    NSString *videoUrl = [[[[activityList objectAtIndex:indexPath.section-2] objectForKey:@"list"] objectAtIndex:indexPath.row] objectForKey:@"video_url"];
    NSString *videoUrl2 = [[[[activityList objectAtIndex:indexPath.section-2] objectForKey:@"list"] objectAtIndex:indexPath.row] objectForKey:@"video_url2"];
    NSString *videoUrl3 = [[[[activityList objectAtIndex:indexPath.section-2] objectForKey:@"list"] objectAtIndex:indexPath.row] objectForKey:@"video_url3"];
    NSString *photoUrl = [[[[activityList objectAtIndex:indexPath.section-2] objectForKey:@"list"] objectAtIndex:indexPath.row] objectForKey:@"photo_url"];
    NSString *photoUrl2 = [[[[activityList objectAtIndex:indexPath.section-2] objectForKey:@"list"] objectAtIndex:indexPath.row] objectForKey:@"photo_url2"];
    NSString *photoUrl3 = [[[[activityList objectAtIndex:indexPath.section-2] objectForKey:@"list"] objectAtIndex:indexPath.row] objectForKey:@"photo_url3"];
    
    NSMutableArray *mUrls = [[NSMutableArray alloc] init];
    
    if (photoUrl && ![photoUrl isKindOfClass:[NSNull class]] && photoUrl.length>0) {
        [mUrls addObject:photoUrl];
    }
    if (photoUrl2 && ![photoUrl2 isKindOfClass:[NSNull class]] && photoUrl2.length>0) {
        [mUrls addObject:photoUrl2];
    }
    if (photoUrl3 && ![photoUrl3 isKindOfClass:[NSNull class]]  && photoUrl3.length>0) {
        [mUrls addObject:photoUrl3];
    }
    if (videoUrl && ![videoUrl isKindOfClass:[NSNull class]]  && videoUrl.length>0) {
        [mUrls addObject:videoUrl];
    }
    if (videoUrl2 && ![videoUrl2 isKindOfClass:[NSNull class]]   && videoUrl2.length>0) {
        [mUrls addObject:videoUrl2];
    }
    if (videoUrl3 && ![videoUrl3 isKindOfClass:[NSNull class]]   && videoUrl3.length>0) {
        [mUrls addObject:videoUrl3];
    }
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Settings" bundle:nil];
    UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"MediaViewerViewController"];
    viewController.data = [[NSMutableDictionary alloc] initWithDictionary:@{@"viewController":self, @"urls":mUrls,@"view_check":@(YES)}];
    [self presentViewController:viewController animated:YES completion:nil];
    
    //    if(![videoUrl isEqual:[NSNull null]] && videoUrl.length>0){
    //
    //        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    //        UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"VideoViewController"];
    //        viewController.data = [[NSMutableDictionary alloc] initWithDictionary:@{@"viewController":self, @"video":videoUrl,@"view_check":@(YES)}];
    //        [self presentViewController:viewController animated:YES completion:nil];
    //    }
    //    else {
    //        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    //        PhotoViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"PhotoViewController"];
    //        viewController.data = [[NSMutableDictionary alloc] initWithDictionary:@{@"viewController":self, @"image":button.imageView.image, @"view_check":@(YES)}];
    //
    //        NSMutableArray *ogImgArray = [NSMutableArray new];
    //        CGPoint buttonPosition = [button convertPoint:CGPointZero toView:self.tableView];
    //        NSIndexPath *selectedIndexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
    //        NSDictionary *selectedRecordDict = [[[activityList objectAtIndex:selectedIndexPath.section-2] objectForKey:@"list"] objectAtIndex:selectedIndexPath.row];
    //
    //        NSString *photoUrl = @"";
    //        photoUrl = [selectedRecordDict objectForKey:@"photo_url"];
    //        if (![[selectedRecordDict objectForKey:@"photo_url"] isKindOfClass:[NSNull class]] && photoUrl.length>0) {
    //            [ogImgArray addObject:photoUrl];
    //        }
    //
    //        photoUrl = @"";
    //        photoUrl = [selectedRecordDict objectForKey:@"photo_url2"];
    //        if (![[selectedRecordDict objectForKey:@"photo_url2"] isKindOfClass:[NSNull class]] && photoUrl.length>0) {
    //            [ogImgArray addObject:photoUrl];
    //        }
    //
    //        photoUrl = @"";
    //        photoUrl = [selectedRecordDict objectForKey:@"photo_url3"];
    //        if (![[selectedRecordDict objectForKey:@"photo_url3"] isKindOfClass:[NSNull class]] && photoUrl.length>0) {
    //            [ogImgArray addObject:photoUrl];
    //        }
    //
    //        if (ogImgArray.count>0) {
    //            viewController.originalImgArray = ogImgArray;
    //            viewController.index = 0;
    //            viewController.hidesBottomBarWhenPushed = YES;
    //            viewController.isMultiple = YES;
    //        }
    //
    //        [self presentViewController:viewController animated:YES completion:nil];
    //    }
}

- (IBAction)getDayListAction:(id)sender
{
    selectedIndex = 0;
    selectedDay = [NSDate date];
    
    [self.btnDay setBackgroundColor:UIColorFromRGB(0x8D8082)];
    [self.btnWeek setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    
    [self refreshAction];
}

- (IBAction)getWeekListAction:(id)sender
{
    selectedIndex = 1;
    selectedWeek = 0;
    
    [self.btnDay setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnWeek setBackgroundColor:UIColorFromRGB(0x8D8082)];
    [self.btnMonth setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    
    [self refreshAction];
}

- (IBAction)getMonthListAction:(id)sender
{
    selectedIndex = 2;
    selectedMonth = 0;
    
    [self.btnDay setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnWeek setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnMonth setBackgroundColor:UIColorFromRGB(0x8D8082)];
    
    [self refreshAction];
}

- (IBAction)addAction:(id)sender
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Activities" bundle:nil];
    UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"ActivitySleepAddViewController"];
    [self.navigationController pushViewController:viewController animated:YES];
}


#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if(isLoading) return 0;
    return activityList.count+2;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if(section > 1)
    {
        NSString *dateString = [[activityList objectAtIndex:section-2] objectForKey:@"header"];
        NSLog(@"dateString=%@", dateString);
        
        [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
        formatter.dateFormat = @"yyyy-MM-dd";
        NSDate *date = [formatter dateFromString:dateString];
        
        if([[shareObject objectForKey:@"language"] hasPrefix:@"zh"])
        {
            [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"zh_HK"]];
            [formatter setDateStyle:NSDateFormatterMediumStyle];
            [formatter setTimeStyle:NSDateFormatterShortStyle];
            
            return [[NSString stringWithFormat:@"%@", [formatter stringFromDate:date]] componentsSeparatedByString:@" "][0];
        }
        else
        {
            [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
            formatter.dateFormat = @"EE d MMM, yyyy";
            return [NSString stringWithFormat:@"%@", [formatter stringFromDate:date]];
        }
    }
    return nil;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if(section == 0)
    {
        CGFloat headerHeight = 40;
        if (IS_IPAD) {
            headerHeight = 48;
        }

        UIButton *btnHeader = [[UIButton alloc] initWithFrame:CGRectMake(10, 0, self.tableView.bounds.size.width-20, headerHeight)];
        btnHeader.backgroundColor = [UIColorFromRGB(0xC5D7E1) colorWithAlphaComponent:0.8f];
        btnHeader.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        btnHeader.layer.cornerRadius = 3.f;
        [btnHeader setTitleEdgeInsets:UIEdgeInsetsMake(0, 10, 0, 0)];
        [btnHeader setTitle:selectedHeader forState:UIControlStateNormal];
        [btnHeader setTitleColor:UIColorFromRGB(0x8D8082) forState:UIControlStateNormal];
        [btnHeader.titleLabel setFont:[UIFont fontWithName:@"SFUIDisplay-Medium" size:15.0f]];
        if (IS_IPAD) {
            [btnHeader.titleLabel setFont:[UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f]];
        }
        
        if(selectedIndex == 0)
        {
            [btnHeader addTarget:self action:@selector(getDatePickerAction) forControlEvents:UIControlEventTouchUpInside];
        }
        else if(selectedIndex == 1)
        {
            [btnHeader addTarget:self action:@selector(getWeekPickerAction) forControlEvents:UIControlEventTouchUpInside];
        }
        else
        {
            [btnHeader addTarget:self action:@selector(getMonthPickerAction) forControlEvents:UIControlEventTouchUpInside];
        }
        
        UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, headerHeight)];
        headerView.backgroundColor = [UIColor whiteColor];
        [headerView addSubview:btnHeader];
        return headerView;
    }
    else if(section > 1)
    {
        CGFloat headerHeight = 30;
        if (IS_IPAD) {
            headerHeight = 40;
        }

        NSString *header = [[activityList objectAtIndex:section-2] objectForKey:@"date"];
        if(header.length > 0)
        {
            UILabel *txtHeader = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, tableView.bounds.size.width-20, headerHeight)];
            txtHeader.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:15.0f];
            if (IS_IPAD) {
                txtHeader.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f];
            }
            txtHeader.textColor = UIColorFromRGB(0x8D8082);
            txtHeader.text = [self tableView:tableView titleForHeaderInSection:section];
            
            UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, headerHeight)];
            headerView.backgroundColor = UIColorFromRGB(0xEEE8E4);
            [headerView addSubview:txtHeader];
            return headerView;
        }
    }
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (IS_IPAD) {
        if(section == 0) return 48;
        else if(section == 1) return 0;
        else
        {
            NSString *header = [[activityList objectAtIndex:section-2] objectForKey:@"date"];
            if(header.length == 0) return 0;
        }
        return 40;
    }
    else {
        if(section == 0) return 40;
        else if(section == 1) return 0;
        else
        {
            NSString *header = [[activityList objectAtIndex:section-2] objectForKey:@"date"];
            if(header.length == 0) return 0;
        }
        return 30;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(section == 0) return 1;
    else if(section == 1) return 1;
    else return [[[activityList objectAtIndex:section-2] objectForKey:@"list"] count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section > 1)
    {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Activities" bundle:nil];
        
        NSMutableDictionary *data = [[NSMutableDictionary alloc] initWithDictionary:@{@"activity_info":[[[activityList objectAtIndex:indexPath.section-2] objectForKey:@"list"] objectAtIndex:indexPath.row]}];
        
        UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"ActivitySleepEditViewController"];
        viewController.data = data;
        [self.navigationController pushViewController:viewController animated:YES];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 0)
    {
        if(selectedIndex == 0)
        {
            ActivitySleepDayChartTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ActivitySleepDayChartTableViewCell"];
            if(!cell)
            {
                [tableView registerNib:[UINib nibWithNibName:@"ActivitySleepDayChartTableViewCell" bundle:nil] forCellReuseIdentifier:@"ActivitySleepDayChartTableViewCell"];
                cell = [tableView dequeueReusableCellWithIdentifier:@"ActivitySleepDayChartTableViewCell"];
            }
            
            cell.txtNo.text = LocalizedString(@"txt_hrs_of_sleep", nil);
            cell.txtAxisTop.text = LocalizedString(@"txt_awake", nil);
            cell.txtAxisBottom.text = LocalizedString(@"txt_asleep", nil);
            
            NSMutableArray *yVals1 = [[NSMutableArray alloc] init];
            [yVals1 addObject:[[BarChartDataEntry alloc] initWithX:0 yValues:[chartInfo objectForKey:@"day_list"]]];
            
            NSMutableArray *yVals2 = [[NSMutableArray alloc] init];
            [yVals2 addObject:[[BarChartDataEntry alloc] initWithX:1 yValues:[chartInfo objectForKey:@"day_list"]]];
            
            BarChartDataSet *set1 = [[BarChartDataSet alloc] initWithValues:yVals1];
            set1.drawValuesEnabled = NO;
            
            BarChartDataSet *set2 = [[BarChartDataSet alloc] initWithValues:yVals2];
            set2.drawValuesEnabled = NO;
            
            NSMutableArray *colors1 = [[NSMutableArray alloc] init];
            NSMutableArray *colors2 = [[NSMutableArray alloc] init];
            for(int i=0; i<[[chartInfo objectForKey:@"night_list"] count]; i++)
            {
                NSString *type = [[chartInfo objectForKey:@"night_list"] objectAtIndex:i] ;
                
                if ([[[chartInfo objectForKey:@"night_list"] objectAtIndex:i] isKindOfClass:[NSNumber class]]) {
                    type = @"";
                }
                
                if(type.length == 0)
                {
                    [colors1 addObject:[UIColor clearColor]];
                    [colors2 addObject:UIColorFromRGB(0xD1C6BE)];
                }
                else
                {
                    if([type isEqualToString:@"day"])
                    {
                        [colors1 addObject:UIColorFromRGB(0xF5EEB3)];
                    }
                    else
                    {
                        [colors1 addObject:UIColorFromRGB(0xCAD7E0)];
                    }
                    [colors2 addObject:[UIColor clearColor]];
                }
            }
            if(colors1.count > 0)set1.colors = colors1;
            if(colors2.count > 0)set2.colors = colors2;
            
            NSMutableArray *dataSets = [[NSMutableArray alloc] init];
            [dataSets addObject:set1];
            [dataSets addObject:set2];
            
            BarChartData *data = [[BarChartData alloc] initWithDataSets:dataSets];
            [data groupBarsFromX:0 groupSpace:0 barSpace:0];
            cell.chartView.data = data;
            
            NSMutableArray *values = [[NSMutableArray alloc] init];
            NSMutableArray *colors = [[NSMutableArray alloc] init];
            
            double total = 0;
            total += [[chartInfo objectForKey:@"day_count"] doubleValue];
            total += [[chartInfo objectForKey:@"night_count"] doubleValue];
            
            if([[chartInfo objectForKey:@"day_count"] doubleValue] > 0)
            {
                [values addObject:[[PieChartDataEntry alloc] initWithValue:([[chartInfo objectForKey:@"day_count"] doubleValue]/total)*100]];
                [colors addObject:UIColorFromRGB(0xF5EEB3)];
            }
            if([[chartInfo objectForKey:@"night_count"] doubleValue] > 0)
            {
                [values addObject:[[PieChartDataEntry alloc] initWithValue:([[chartInfo objectForKey:@"night_count"] doubleValue]/total)*100]];
                [colors addObject:UIColorFromRGB(0xCAD7E0)];
            }
            if(total == 0)
            {
                [values addObject:[[PieChartDataEntry alloc] initWithValue:100]];
                [colors addObject:UIColorFromRGB(0xF7F6F5)];
            }
            NSLog(@"values=%@",values);
            
            PieChartDataSet *dataSet = [[PieChartDataSet alloc] initWithValues:values];
            dataSet.sliceSpace = 0;
            dataSet.selectionShift = 0;
            dataSet.colors = colors;
            
            PieChartData *pieChartData = [[PieChartData alloc] initWithDataSet:dataSet];
            [pieChartData setDrawValues:NO];
            cell.pieChartView.data = pieChartData;
            cell.pieChartView.centerAttributedText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%d", [[chartInfo objectForKey:@"count"] intValue]] attributes:@{NSFontAttributeName:[UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f], NSForegroundColorAttributeName:UIColorFromRGB(0x8D8082)}];
            //cell.pieChartView.centerAttributedText = [[NSMutableAttributedString alloc] initWithString:[[self convertUnit:[NSString stringWithFormat:@"%.1f hr", [[chartInfo objectForKey:@"count"] floatValue]] unit:@"hr"] stringByReplacingOccurrencesOfString:@" hr" withString:@""] attributes:@{NSFontAttributeName:[UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f], NSForegroundColorAttributeName:UIColorFromRGB(0x8D8082)}];
            
            [chartInfo setObject:[self convertDuration:[[chartInfo objectForKey:@"day_total"] intValue] fullFormatCheck:NO] forKey:@"day_total"];
            [chartInfo setObject:[self convertDuration:[[chartInfo objectForKey:@"night_total"] intValue] fullFormatCheck:NO] forKey:@"night_total"];

            cell.txtDayValue.text = [chartInfo objectForKey:@"day_total"];
            cell.txtNightValue.text = [chartInfo objectForKey:@"night_total"];
            
            UISwipeGestureRecognizer *gestureRecognizer1 = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(leftSwipeHandler:)];
            gestureRecognizer1.direction = UISwipeGestureRecognizerDirectionLeft;
            [cell addGestureRecognizer:gestureRecognizer1];
            
            UISwipeGestureRecognizer *gestureRecognizer2 = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(rightSwipeHandler:)];
            gestureRecognizer2.direction = UISwipeGestureRecognizerDirectionRight;
            [cell addGestureRecognizer:gestureRecognizer2];

            return cell;
        }
        else
        {
            NSMutableArray *yVals = [[NSMutableArray alloc] init];
            for(int i=0; i<[[chartInfo objectForKey:@"day_list"] count]; i++)
            {
                [yVals addObject:[[BarChartDataEntry alloc] initWithX:i yValues:@[@([[[chartInfo objectForKey:@"day_list"] objectAtIndex:i] doubleValue]/60), @([[[chartInfo objectForKey:@"night_list"] objectAtIndex:i] doubleValue]/60)]]];
            }
            
            BarChartDataSet *set = [[BarChartDataSet alloc] initWithValues:yVals];
            set.drawValuesEnabled = NO;
            set.colors = @[UIColorFromRGB(0xF5EEB3), UIColorFromRGB(0xCAD7E0)];
            
            NSMutableArray *values = [[NSMutableArray alloc] init];
            NSMutableArray *colors = [[NSMutableArray alloc] init];
            
            double total = 0;
            total += [[chartInfo objectForKey:@"day_total"] doubleValue];
            total += [[chartInfo objectForKey:@"night_total"] doubleValue];
            
            if([[chartInfo objectForKey:@"day_total"] doubleValue] > 0)
            {
                [values addObject:[[PieChartDataEntry alloc] initWithValue:([[chartInfo objectForKey:@"day_total"] doubleValue]/total)*100]];
                [colors addObject:UIColorFromRGB(0xF5EEB3)];
            }
            if([[chartInfo objectForKey:@"night_total"] doubleValue] > 0)
            {
                [values addObject:[[PieChartDataEntry alloc] initWithValue:([[chartInfo objectForKey:@"night_total"] doubleValue]/total)*100]];
                [colors addObject:UIColorFromRGB(0xCAD7E0)];
            }
            if(total == 0)
            {
                [values addObject:[[PieChartDataEntry alloc] initWithValue:100]];
                [colors addObject:UIColorFromRGB(0xF7F6F5)];
            }
            NSLog(@"values=%@",values);
            
            PieChartDataSet *dataSet = [[PieChartDataSet alloc] initWithValues:values];
            dataSet.sliceSpace = 0;
            dataSet.selectionShift = 0;
            dataSet.colors = colors;
            
            PieChartData *pieChartData = [[PieChartData alloc] initWithDataSet:dataSet];
            [pieChartData setDrawValues:NO];
            
            [chartInfo setObject:[self convertDuration:[[chartInfo objectForKey:@"day_total"] intValue] fullFormatCheck:NO] forKey:@"day_total"];
            [chartInfo setObject:[self convertDuration:[[chartInfo objectForKey:@"night_total"] intValue] fullFormatCheck:NO] forKey:@"night_total"];
            
            if(selectedIndex == 1)
            {
                ActivitySleepWeekChartTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ActivitySleepWeekChartTableViewCell"];
                if(!cell)
                {
                    [tableView registerNib:[UINib nibWithNibName:@"ActivitySleepWeekChartTableViewCell" bundle:nil] forCellReuseIdentifier:@"ActivitySleepWeekChartTableViewCell"];
                    cell = [tableView dequeueReusableCellWithIdentifier:@"ActivitySleepWeekChartTableViewCell"];
                }
                
                
                NSMutableArray *dataSets = [[NSMutableArray alloc] initWithObjects:set, nil];
                BarChartData *data = [[BarChartData alloc] initWithDataSets:dataSets];
                cell.chartView.data = data;
                
                cell.pieChartView.data = pieChartData;
                cell.pieChartView.centerAttributedText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%d", [[chartInfo objectForKey:@"count"] intValue]] attributes:@{NSFontAttributeName:[UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f], NSForegroundColorAttributeName:UIColorFromRGB(0x8D8082)}];
                
                
                
                //cell.pieChartView.centerAttributedText = [[NSMutableAttributedString alloc] initWithString:[[self convertUnit:[NSString stringWithFormat:@"%.1f hr", [[chartInfo objectForKey:@"count"] floatValue]] unit:@"hr"] stringByReplacingOccurrencesOfString:@" hr" withString:@""] attributes:@{NSFontAttributeName:[UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f], NSForegroundColorAttributeName:UIColorFromRGB(0x8D8082)}];
                
                cell.txtDayValue.text = [chartInfo objectForKey:@"day_total"];
                cell.txtNightValue.text = [chartInfo objectForKey:@"night_total"];
                
                UISwipeGestureRecognizer *gestureRecognizer1 = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(leftSwipeHandler:)];
                gestureRecognizer1.direction = UISwipeGestureRecognizerDirectionLeft;
                [cell addGestureRecognizer:gestureRecognizer1];
                
                UISwipeGestureRecognizer *gestureRecognizer2 = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(rightSwipeHandler:)];
                gestureRecognizer2.direction = UISwipeGestureRecognizerDirectionRight;
                [cell addGestureRecognizer:gestureRecognizer2];

                return cell;
            }
            else
            {
                ActivitySleepMonthChartTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ActivitySleepMonthChartTableViewCell"];
                if(!cell)
                {
                    [tableView registerNib:[UINib nibWithNibName:@"ActivitySleepMonthChartTableViewCell" bundle:nil] forCellReuseIdentifier:@"ActivitySleepMonthChartTableViewCell"];
                    cell = [tableView dequeueReusableCellWithIdentifier:@"ActivitySleepMonthChartTableViewCell"];
                }
                
                NSMutableArray *dataSets = [[NSMutableArray alloc] initWithObjects:set, nil];
                BarChartData *data = [[BarChartData alloc] initWithDataSets:dataSets];
                cell.chartView.data = data;
                
                cell.pieChartView.data = pieChartData;
                cell.pieChartView.centerAttributedText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%d", [[chartInfo objectForKey:@"count"] intValue]] attributes:@{NSFontAttributeName:[UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f], NSForegroundColorAttributeName:UIColorFromRGB(0x8D8082)}];
                //cell.pieChartView.centerAttributedText = [[NSMutableAttributedString alloc] initWithString:[[self convertUnit:[NSString stringWithFormat:@"%.1f hr", [[chartInfo objectForKey:@"count"] floatValue]] unit:@"hr"] stringByReplacingOccurrencesOfString:@" hr" withString:@""] attributes:@{NSFontAttributeName:[UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f], NSForegroundColorAttributeName:UIColorFromRGB(0x8D8082)}];
                
                cell.txtDayValue.text = [chartInfo objectForKey:@"day_total"];
                cell.txtNightValue.text = [chartInfo objectForKey:@"night_total"];
                
                UISwipeGestureRecognizer *gestureRecognizer1 = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(leftSwipeHandler:)];
                gestureRecognizer1.direction = UISwipeGestureRecognizerDirectionLeft;
                [cell addGestureRecognizer:gestureRecognizer1];
                
                UISwipeGestureRecognizer *gestureRecognizer2 = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(rightSwipeHandler:)];
                gestureRecognizer2.direction = UISwipeGestureRecognizerDirectionRight;
                [cell addGestureRecognizer:gestureRecognizer2];

                return cell;
            }

            /*ChartXAxis *xAxis = cell.chartView.xAxis;
            
            if(selectedIndex == 1)
            {
                xAxis.labelCount = 7;
                xAxis.valueFormatter = [[ChartAxisValueFormatter alloc] initForChart:cell.chartView type:ChartAxisTypeForWeek];
                cell.txtNo.text = LocalizedString(@"txt_avg_daily_sleep", nil);
                cell.txtSummary.text = LocalizedString(@"txt_avg_daily_sleep", nil);
                cell.txtAxisTop.text = LocalizedString(@"txt_hours", nil);
                cell.txtAxisBottom.text = @"";
                
            }
            else
            {
                xAxis.labelCount = 6;
                xAxis.valueFormatter = [[ChartAxisValueFormatter alloc] initForChart:cell.chartView type:ChartAxisTypeForMonth];
                cell.txtNo.text = LocalizedString(@"txt_avg_daily_sleep", nil);
                cell.txtSummary.text = LocalizedString(@"txt_avg_daily_sleep", nil);
                cell.txtAxisTop.text = LocalizedString(@"txt_hours", nil);
                cell.txtAxisBottom.text = @"";
                
            }
            
            NSMutableArray *yVals = [[NSMutableArray alloc] init];
            for(int i=0; i<[[chartInfo objectForKey:@"day_list"] count]; i++)
            {
                [yVals addObject:[[BarChartDataEntry alloc] initWithX:i yValues:@[@([[[chartInfo objectForKey:@"day_list"] objectAtIndex:i] doubleValue]/60), @([[[chartInfo objectForKey:@"night_list"] objectAtIndex:i] doubleValue]/60)]]];
            }
            
            BarChartDataSet *set1 = [[BarChartDataSet alloc] initWithValues:yVals];
            set1.drawValuesEnabled = NO;
            set1.colors = @[UIColorFromRGB(0xF5EEB3), UIColorFromRGB(0xCAD7E0)];
            
            NSMutableArray *dataSets = [[NSMutableArray alloc] initWithObjects:set1, nil];
            BarChartData *data = [[BarChartData alloc] initWithDataSets:dataSets];
            cell.chartView.data = data;
            
            NSMutableArray *values = [[NSMutableArray alloc] init];
            [values addObject:[[PieChartDataEntry alloc] initWithValue:[[chartInfo objectForKey:@"day_count"] intValue]]];
            [values addObject:[[PieChartDataEntry alloc] initWithValue:[[chartInfo objectForKey:@"night_count"] intValue]]];
            
            if([[chartInfo objectForKey:@"day_count"] intValue] == 0 && [[chartInfo objectForKey:@"night_count"] intValue] == 0)
            {
                [values addObject:[[PieChartDataEntry alloc] initWithValue:100]];
            }
            
            PieChartDataSet *dataSet = [[PieChartDataSet alloc] initWithValues:values];
            dataSet.sliceSpace = 0;
            dataSet.selectionShift = 0;
            
            NSMutableArray *colors = [[NSMutableArray alloc] init];
            [colors addObject:UIColorFromRGB(0xF5EEB3)];
            [colors addObject:UIColorFromRGB(0xCAD7E0)];
            [colors addObject:UIColorFromRGB(0xF7F6F5)];
            dataSet.colors = colors;
            
            PieChartData *pieChartData = [[PieChartData alloc] initWithDataSet:dataSet];
            [pieChartData setDrawValues:NO];
            cell.pieChartView.data = pieChartData;
            cell.pieChartView.centerAttributedText = [[NSMutableAttributedString alloc] initWithString:[[self convertUnit:[NSString stringWithFormat:@"%.1f hr", [[chartInfo objectForKey:@"count"] floatValue]] unit:@"hr"] stringByReplacingOccurrencesOfString:@" hr" withString:@""] attributes:@{NSFontAttributeName:[UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f], NSForegroundColorAttributeName:UIColorFromRGB(0x8D8082)}];
            
            [chartInfo setObject:[self convertDuration:[[chartInfo objectForKey:@"day_total"] intValue] fullFormatCheck:NO] forKey:@"day_total"];
            [chartInfo setObject:[self convertDuration:[[chartInfo objectForKey:@"night_total"] intValue] fullFormatCheck:NO] forKey:@"night_total"];
            
            
            cell.txtDayValue.text = [chartInfo objectForKey:@"day_total"];
            cell.txtNightValue.text = [chartInfo objectForKey:@"night_total"];
            
            return cell;*/
        }
    }
    else if(indexPath.section == 1)
    {
        ActivitySleepGuideTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ActivitySleepGuideTableViewCell"];
        if(!cell)
        {
            [tableView registerNib:[UINib nibWithNibName:@"ActivitySleepGuideTableViewCell" bundle:nil] forCellReuseIdentifier:@"ActivitySleepGuideTableViewCell"];
            cell = [tableView dequeueReusableCellWithIdentifier:@"ActivitySleepGuideTableViewCell"];
        }
        
        int minValue = (int)ceil([[chartInfo objectForKey:@"min_sleep"] floatValue]/60);
        int maxValue = (int)ceil([[chartInfo objectForKey:@"max_sleep"] floatValue]/60);
        int total = [[chartInfo objectForKey:@"count"] intValue];
        
        cell.lblNo1.text = @"";
        cell.lblNo2.text = [NSString stringWithFormat:@"%d", minValue];
        cell.lblNo3.text = [NSString stringWithFormat:@"%d", maxValue];
        cell.lblNo4.text = @"";
        cell.txtNo.text = [NSString stringWithFormat:@"%d", total];
        
        float leftX = cell.highlightView.frame.origin.x-cell.guideView.frame.origin.x;
        float rightX = leftX+cell.highlightView.frame.size.width;
        float widthValue = ((cell.guideView.frame.size.width-cell.highlightView.frame.size.width)/2)/minValue;
        float widthHighlightValue = cell.highlightView.frame.size.width/(maxValue-minValue);
        float offset = 5;
        
        if(total < minValue)
        {
            cell.starViewLeadingConstraint.constant = widthValue*total-offset;
        }
        else if(total > maxValue)
        {
            cell.starViewLeadingConstraint.constant = rightX+widthValue*(total-maxValue)-offset;
        }
        else
        {
            cell.starViewLeadingConstraint.constant = leftX+widthHighlightValue*(total-minValue)-offset;
        }
        [cell.starView layoutIfNeeded];
        
        return cell;
    }
    else
    {
        ActivityTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ActivityTableViewCell"];
        if(!cell)
        {
            [tableView registerNib:[UINib nibWithNibName:@"ActivityTableViewCell" bundle:nil] forCellReuseIdentifier:@"ActivityTableViewCell"];
            cell = [tableView dequeueReusableCellWithIdentifier:@"ActivityTableViewCell"];
        }
        
        if (IS_IPAD) {
            cell.txtTime.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:19.0f];
            cell.txtValue.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:19.0f];
        }
        [cell layoutSubviews];

        cell.topSeperator.hidden = YES;
        if (selectedIndex==0 && indexPath.row==0) {
            cell.topSeperator.hidden = NO;
        }
        
        NSString *time = [[[[activityList objectAtIndex:indexPath.section-2] objectForKey:@"list"] objectAtIndex:indexPath.row] objectForKey:@"time"];
        if([[shareObject objectForKey:@"language"] hasPrefix:@"zh"])
        {
            NSArray *timeArray = [time componentsSeparatedByString:@" "];
            time = [NSString stringWithFormat:@"%@ %@", LocalizedString(timeArray[1], nil), timeArray[0]];
        }
        
        cell.txtTime.text = time;
        
        if([[[[[activityList objectAtIndex:indexPath.section-2] objectForKey:@"list"] objectAtIndex:indexPath.row] objectForKey:@"type"] isEqualToString:@"sleep_cycle"])
        {
            cell.txtValue.text = [self convertDuration:[[[[[activityList objectAtIndex:indexPath.section-2] objectForKey:@"list"] objectAtIndex:indexPath.row] objectForKey:@"attribute_value"] intValue] fullFormatCheck:NO];
        }
        else
        {
            cell.txtValue.text = [[[[activityList objectAtIndex:indexPath.section-2] objectForKey:@"list"] objectAtIndex:indexPath.row] objectForKey:@"attribute_value"];
        }
        
        cell.txtNotes.text = @"";
        if (![[[[[activityList objectAtIndex:indexPath.section-2] objectForKey:@"list"] objectAtIndex:indexPath.row] objectForKey:@"notes"] isKindOfClass:[NSNull class]]) {
            cell.txtNotes.text =  [[[[activityList objectAtIndex:indexPath.section-2] objectForKey:@"list"] objectAtIndex:indexPath.row] objectForKey:@"notes"];
            
            if ([cell.txtNotes.text length]>0) {
                cell.txtNotes.text = [NSString stringWithFormat:@"Notes : %@", [[[[activityList objectAtIndex:indexPath.section-2] objectForKey:@"list"] objectAtIndex:indexPath.row] objectForKey:@"notes"]];
            }
            
        }

        NSString *photoUrl = [[[[activityList objectAtIndex:indexPath.section-2] objectForKey:@"list"] objectAtIndex:indexPath.row] objectForKey:@"photo_url"];
        
        if([photoUrl isEqual:[NSNull null]] || !photoUrl.length){
            photoUrl = [[[[activityList objectAtIndex:indexPath.section-2] objectForKey:@"list"] objectAtIndex:indexPath.row] objectForKey:@"photo_url2"];
            if([photoUrl isEqual:[NSNull null]] || !photoUrl.length){
                photoUrl = [[[[activityList objectAtIndex:indexPath.section-2] objectForKey:@"list"] objectAtIndex:indexPath.row] objectForKey:@"photo_url3"];
            }
        }
        
        if((![photoUrl isEqual:[NSNull null]])&&(photoUrl.length>0))
        {
            cell.btnPhoto.hidden = NO;
            cell.btnPhoto.userInteractionEnabled = YES;
            [cell.btnPhoto sd_setImageWithURL:[NSURL URLWithString:photoUrl] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"dummy.png"]];
            [cell.btnPhoto addTarget:self action:@selector(showPhotoAction:) forControlEvents:UIControlEventTouchUpInside];
        }
        else {
            
            NSString *videoUrl = [[[[activityList objectAtIndex:indexPath.section-2] objectForKey:@"list"] objectAtIndex:indexPath.row] objectForKey:@"video_url"];
            
            if([videoUrl isEqual:[NSNull null]] || !videoUrl.length){
                videoUrl = [[[[activityList objectAtIndex:indexPath.section-2] objectForKey:@"list"] objectAtIndex:indexPath.row] objectForKey:@"video_url2"];
                if([videoUrl isEqual:[NSNull null]] || !videoUrl.length){
                    videoUrl = [[[[activityList objectAtIndex:indexPath.section-2] objectForKey:@"list"] objectAtIndex:indexPath.row] objectForKey:@"video_url3"];
                }
            }
            
            if ((![videoUrl isEqual:[NSNull null]])&&(videoUrl.length>0)) {
                cell.btnPhoto.hidden = NO;
                cell.btnPhoto.userInteractionEnabled = YES;
                // Generate Thumbnail Image.
                //UIImage *tImg = [COMMON_HELPER generateThumbnailImageFromVideoWithUrl:[NSURL URLWithString:videoUrl]];
                //[cell.btnPhoto setImage:tImg forState:UIControlStateNormal];
                //[cell.btnPhoto sd_setImageWithURL:[NSURL URLWithString:videoUrl] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"dummy.png"]];
                [cell.btnPhoto setImage:[UIImage imageNamed:@"video_player_icon"] forState:UIControlStateNormal];
                cell.btnPhoto.backgroundColor = [UIColor darkTextColor];
                [cell.btnPhoto addTarget:self action:@selector(showPhotoAction:) forControlEvents:UIControlEventTouchUpInside];
            }
            else {
                cell.btnPhoto.hidden = YES;
                cell.btnPhoto.userInteractionEnabled = NO;
            }
        }

        return cell;
    }
}

#pragma mark - SwipeGesture methods RS
-(void)rightSwipeHandler: (UISwipeGestureRecognizer *)gesture
{
    NSLog(@"%lu" , (unsigned long)gesture.direction);
    
    if (selectedIndex == 0) {
        
        NSDateComponents *dayComponent = [[NSDateComponents alloc] init];
        dayComponent.day = -1;
        NSCalendar *theCalendar = [NSCalendar currentCalendar];
        NSDate *nextDate = [theCalendar dateByAddingComponents:dayComponent toDate:selectedDay options:0];
        selectedDay = nextDate;
        [self refreshAction];
        
        //        NSLog(@"nextDate: %@ ...", nextDate);
        //        //selectedDay = selectedDate;
        //        formatter.dateFormat = @"d MMM yyyy";
        //        selectedHeader = [formatter stringFromDate:selectedDay];
        //        [self refreshAction];
        
    }else if (selectedIndex == 1){
        
        if ((selectedWeek>=0)&&(selectedWeek<weekList.count-1)) {
            selectedWeek += 1;
            [self refreshAction];
        }
        
        //        //selectedWeek = selectedIndex2;
        //        //NSLog(@"selectedWeek=%d", selectedWeek);
        //        formatter.dateFormat = @"d MMM yyyy";
        //        selectedHeader = [formatter stringFromDate:selectedDay];
        //        NSLog(@"selectedWeek=%@", selectedHeader);
        
    }else if (selectedIndex == 2){
        
        if ((selectedMonth>=0)&&(selectedMonth<monthList.count-1)) {
            selectedMonth += 1;
            [self refreshAction];
        }
        //        // selectedMonth = selectedIndex2;
        //        formatter.dateFormat = @"d MMM yyyy";
        //        selectedHeader = [formatter stringFromDate:selectedDay];
        //        [self refreshAction];
        
    }
    
}

-(void)leftSwipeHandler: (UISwipeGestureRecognizer *)gesture
{
    NSLog(@"%lu" , (unsigned long)gesture.direction);
    
    if (selectedIndex == 0) {
        
        BOOL isToday = [[NSCalendar currentCalendar] isDateInToday:selectedDay];
        if (!isToday) {
            NSDateComponents *dayComponent = [[NSDateComponents alloc] init];
            dayComponent.day = 1;
            NSCalendar *theCalendar = [NSCalendar currentCalendar];
            NSDate *nextDate = [theCalendar dateByAddingComponents:dayComponent toDate:selectedDay options:0];
            selectedDay = nextDate;
            [self refreshAction];
        }
        
        //        NSLog(@"nextDate: %@ ...", nextDate);
        //        //selectedDay = selectedDate;
        //        formatter.dateFormat = @"d MMM yyyy";
        //        selectedHeader = [formatter stringFromDate:selectedDay];
        //        [self refreshAction];
        
    }else if (selectedIndex == 1){
        
        if (selectedWeek > 0) {
            selectedWeek -= 1;
            [self refreshAction];
        }
        
        //        //selectedWeek = selectedIndex2;
        //        //NSLog(@"selectedWeek=%d", selectedWeek);
        //        formatter.dateFormat = @"d MMM yyyy";
        //        selectedHeader = [formatter stringFromDate:selectedDay];
        //        NSLog(@"selectedWeek=%@", selectedHeader);
        //        [self refreshAction];
        
    }else if (selectedIndex == 2){
        
        if (selectedMonth > 0) {
            selectedMonth -= 1;
            [self refreshAction];
        }
        //       // selectedMonth = selectedIndex2;
        //        formatter.dateFormat = @"d MMM yyyy";
        //        selectedHeader = [formatter stringFromDate:selectedDay];
        //        [self refreshAction];
        
    }
    
}

@end
