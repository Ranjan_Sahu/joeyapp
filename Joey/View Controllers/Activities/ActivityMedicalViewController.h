//
//  ActivityMedicalViewController.h
//  Joey
//
//  Created by csl on 4/1/2017.
//  Copyright © 2017 Auxilia. All rights reserved.
//

#import "BaseViewController.h"

@interface ActivityMedicalViewController : BaseViewController <UIScrollViewDelegate, UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, weak) IBOutlet UIBarButtonItem *btnAdd;
@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, weak) IBOutlet UIButton *btnDay;
@property (nonatomic, weak) IBOutlet UIButton *btnWeek;
@property (nonatomic, weak) IBOutlet UIButton *btnMonth;

@end
