//
//  ActivitiesViewController.h
//  Joey
//
//  Created by csl on 21/12/2016.
//  Copyright © 2016 Auxilia. All rights reserved.
//

#import "BaseViewController.h"

@interface ActivitiesViewController : BaseViewController <UIScrollViewDelegate, UICollectionViewDelegate, UICollectionViewDataSource>

@property (nonatomic, weak) IBOutlet UIScrollView *babyScrollView;
@property (nonatomic, weak) IBOutlet UICollectionView *collectionView;

@property (nonatomic, weak) IBOutlet UIImageView *imgBabyPic1;
@property (nonatomic, weak) IBOutlet UIImageView *imgBabyPic2;
@property (nonatomic, weak) IBOutlet UIImageView *imgBabyPic3;
@property (nonatomic, weak) IBOutlet UILabel *txtName1;
@property (nonatomic, weak) IBOutlet UILabel *txtInfo1;
@property (nonatomic, weak) IBOutlet UILabel *txtName2;
@property (nonatomic, weak) IBOutlet UILabel *txtInfo2;
@property (nonatomic, weak) IBOutlet UILabel *txtName3;
@property (nonatomic, weak) IBOutlet UILabel *txtInfo3;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomViewBottomSpace;

- (void)viewWillAppear:(BOOL)animated;

@end

