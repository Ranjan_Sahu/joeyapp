//
//  ActivitiesViewController.m
//  Joey
//
//  Created by csl on 21/12/2016.
//  Copyright © 2016 Auxilia. All rights reserved.
//

#import "AppDelegate.h"
#import "ActivitiesViewController.h"
#import "ActivitiesCollectionViewCell.h"

@interface ActivitiesViewController ()
{
    NSUserDefaults *shareObject;
    AppDelegate *appDelegate;
    NSDateFormatter *formatter;
    NSMutableArray *babyList;
    NSMutableArray *activityList;
    NSMutableDictionary *unitInfo;
    NSTimer *timer;
    BOOL isInit;
    BOOL isLoading;
    NSString *isSubscribed;
    BOOL feedReminderCheck;
    BOOL pumpReminderCheck;
    BOOL sleepReminderCheck;
    BOOL pooReminderCheck;
    BOOL weeReminderCheck;
    BOOL medicalReminderCheck;
    int backupPageNo;
    int selectedBabyIndex;
    
    UIView *animationView;
    
}
@end

@implementation ActivitiesViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    shareObject = [NSUserDefaults standardUserDefaults];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    formatter = [[NSDateFormatter alloc] init];
    [formatter setTimeZone:[NSTimeZone localTimeZone]];
    [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
    
    if (IS_IPHONE_X) {
        self.bottomViewBottomSpace.constant = 84.0f;
    }
    activityList = [[NSMutableArray alloc] init];
    unitInfo = (NSMutableDictionary *)[NSKeyedUnarchiver unarchiveObjectWithData:[shareObject objectForKey:@"unit_info"]];
    isSubscribed = [shareObject objectForKey:@"subscription_status"];
    NSLog(@"%@",isSubscribed);
    //free_trial
    //subscription_status
    backupPageNo = 1;
    
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:UIColorFromRGB(0x8D8082), NSForegroundColorAttributeName, [UIFont fontWithName:@"SFUIDisplay-Medium" size:18.0f], NSFontAttributeName, nil]];
    [self.navigationController.navigationBar setBarTintColor:UIColorFromRGB(0xEEE8E4)];
    [self.navigationController.navigationBar setTranslucent:NO];
    [self customSettingsButton];
    
    UIButton *btnLogo = [[UIButton alloc] initWithFrame:CGRectZero];
    [btnLogo setImage:[UIImage imageNamed:@"nav_logo.png"] forState:UIControlStateNormal];
    [btnLogo sizeToFit];
    self.navigationItem.titleView = btnLogo;
    
    self.collectionView.alwaysBounceVertical = YES;
    
    __weak __typeof(self)weakSelf = self;
    [self.collectionView addPullToRefreshWithActionHandler:^{
        [weakSelf refreshAction];
    }];
    
    [SVProgressHUD show];
    [self setupBabyList];
    [self refreshAction];
    
    if (IS_IPAD) {
        self.txtName1.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.txtInfo1.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:22.0f];
        self.txtName2.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.txtInfo2.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:22.0f];
        self.txtName3.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.txtInfo3.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:22.0f];
    }
    
    if(timer == nil) timer = [NSTimer scheduledTimerWithTimeInterval:60 target:self selector:@selector(refreshAction) userInfo:nil repeats:YES];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:(BOOL)animated];
    
    [appDelegate.tracker send:[[[GAIDictionaryBuilder createScreenView] set:@"Activities" forKey:kGAIScreenName] build]];
    
    [self.navigationController.navigationBar setBarTintColor:UIColorFromRGB(0xEEE8E4)];
    
    unitInfo = (NSMutableDictionary *)[NSKeyedUnarchiver unarchiveObjectWithData:[shareObject objectForKey:@"unit_info"]];
    NSLog(@"unitInfo=%@", unitInfo);
    
    NSLog(@"here=%@", self.data);
    NSLog(@"appDelegate.updateActivitiesCheck=%@", appDelegate.updateActivitiesCheck?@"YES":@"NO");
    if([[self.data objectForKey:@"save_check"] boolValue] || appDelegate.updateActivitiesCheck || selectedBabyIndex != (int)[shareObject integerForKey:@"baby_index"])
    {
        appDelegate.updateActivitiesCheck = NO;
        [self.data removeAllObjects];
        
        [SVProgressHUD show];
        [self setupBabyList];
        [self refreshAction];
    }
    
    NSLog(@"selectedBabyIndex 5=%d", selectedBabyIndex);
    NSLog(@"selectedBabyIndex 6=%d", (int)[shareObject integerForKey:@"baby_index"]);
    
    UITabBarItem *tabbarItem = (UITabBarItem *)[appDelegate.tabbar.tabBar.items objectAtIndex:1];
    NSLog(@"tabbarItem=%d", [tabbarItem.badgeValue intValue]);
    
    if(selectedBabyIndex != (int)[shareObject integerForKey:@"baby_index"] || [tabbarItem.badgeValue intValue] > 0)
    {
        NSLog(@"hey");
        tabbarItem.badgeValue = nil;
        [self setupBabyList];
        [self refreshAction];
    }
    
    /*if(!appDelegate.cbCentralManager.delegate)
    {
        appDelegate.cbCentralManager.delegate = appDelegate;
        [appDelegate scanForPeripherals:YES];
    }
    NSLog(@"ble2=%@", appDelegate.bluetoothManager);*/
}

- (void)viewDidDisappear:(BOOL)animated
{
    NSLog(@"ActivitiesViewController viewDidDisappear");
    [super viewDidDisappear:animated];
    [timer invalidate];
}

- (void)viewDidLayoutSubviews
{
    if(self.babyScrollView.contentSize.width == 0)
    {
        self.babyScrollView.contentSize = CGSizeMake(self.view.frame.size.width*3, 1);
        self.babyScrollView.contentOffset = CGPointMake(self.view.frame.size.width, self.babyScrollView.contentOffset.y);
    }
    
    self.imgBabyPic1.clipsToBounds = self.imgBabyPic2.clipsToBounds = self.imgBabyPic3.clipsToBounds = YES;
    self.imgBabyPic1.layer.cornerRadius = self.imgBabyPic1.frame.size.width/2;
    self.imgBabyPic2.layer.cornerRadius = self.imgBabyPic2.frame.size.width/2;
    self.imgBabyPic3.layer.cornerRadius = self.imgBabyPic3.frame.size.width/2;
}

#pragma mark - custom methods
- (void)customSettingsButton
{
    UIImage *imgSettings = [UIImage imageNamed:@"nav_btn_settings"];
    UIButton *btnSettings = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnSettings setImage:imgSettings forState:UIControlStateNormal];
    btnSettings.frame = CGRectMake(52, 0, imgSettings.size.width, imgSettings.size.height);
    [btnSettings addTarget:self action:@selector(getSettingsAction:) forControlEvents:UIControlEventTouchUpInside];
    
    UIView *buttonView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 88, 44)];
    [buttonView addSubview:btnSettings];
    
    UIBarButtonItem *backBarItem = [[UIBarButtonItem alloc] initWithCustomView:buttonView];
    self.navigationItem.rightBarButtonItem = backBarItem;
}

- (void)setupBabyList
{
    babyList = (NSMutableArray *)[NSKeyedUnarchiver unarchiveObjectWithData:[shareObject objectForKey:@"baby_list"]];
    
    selectedBabyIndex = (int)[shareObject integerForKey:@"baby_index"];
    selectedBabyIndex = (selectedBabyIndex >= babyList.count)?0:selectedBabyIndex;
    NSLog(@"selectedBabyIndex1=%d", selectedBabyIndex);
    
    /*if([shareObject integerForKey:@"wearable_baby_id"] != [shareObject integerForKey:@"baby_id"])
    {
        for(int i=0; i<babyList.count; i++)
        {
            if([[[babyList objectAtIndex:i] objectForKey:@"id"] intValue] == [shareObject integerForKey:@"wearable_baby_id"])
            {
                selectedBabyIndex = i;
                NSLog(@"selectedBabyIndex2=%d", selectedBabyIndex);
                break;
            }
        }
        
        [shareObject setInteger:selectedBabyIndex forKey:@"baby_index"];
        [shareObject setInteger:[shareObject integerForKey:@"wearable_baby_id"] forKey:@"baby_id"];
        [shareObject synchronize];
    }*/
    
    for(int i=0; i<babyList.count; i++)
    {
        NSLog(@"weight=%@", [[babyList objectAtIndex:i] objectForKey:@"weight"]);
        NSLog(@"unit=%@", [unitInfo objectForKey:@"weight"]);
        [[babyList objectAtIndex:i] setObject:[self convertUnit:[[babyList objectAtIndex:i] objectForKey:@"height"] unit:[unitInfo objectForKey:@"height"] spacing:YES] forKey:@"height"];
        
        [[babyList objectAtIndex:i] setObject:[self convertUnit:[[babyList objectAtIndex:i] objectForKey:@"weight"] unit:[unitInfo objectForKey:@"weight"] spacing:YES] forKey:@"weight"];
    }
    
    // Check for Subscription of selected baby
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"is_Subscription_Enabled"];
    if (![[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"subscription"] isKindOfClass:[NSNull class]]) {
        BOOL isSubscribed = [[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"subscription"] boolValue];
        [[NSUserDefaults standardUserDefaults]setBool:isSubscribed forKey:@"is_Subscription_Enabled"];
    }
    
    NSLog(@"babyList2=%@", babyList);
    if(babyList.count == 1)
    {
        self.babyScrollView.scrollEnabled = NO;
        [self.imgBabyPic2 sd_setImageWithURL:[NSURL URLWithString:[[babyList objectAtIndex:0] objectForKey:@"photo_url"]] placeholderImage:[UIImage imageNamed:@"dummy.png"]];
        self.txtName2.text = [[babyList objectAtIndex:0] objectForKey:@"name"];
        self.txtInfo2.text = [NSString stringWithFormat:LocalizedString(@"txt_baby_info", nil), [[babyList objectAtIndex:0] objectForKey:@"age"], [[babyList objectAtIndex:0] objectForKey:@"height"], [[babyList objectAtIndex:0] objectForKey:@"weight"], [[babyList objectAtIndex:0] objectForKey:@"blood_type"]];
    }
    else
    {
        int prevIndex = selectedBabyIndex-1;
        prevIndex = (prevIndex < 0)?(int)(babyList.count-1):prevIndex;
        int nextIndex = selectedBabyIndex+1;
        nextIndex = (nextIndex >= babyList.count)?0:nextIndex;
        
        self.babyScrollView.scrollEnabled = YES;
        [self.imgBabyPic1 sd_setImageWithURL:[NSURL URLWithString:[[babyList objectAtIndex:prevIndex] objectForKey:@"photo_url"]] placeholderImage:[UIImage imageNamed:@"dummy.png"]];
        self.txtName1.text = [[babyList objectAtIndex:prevIndex] objectForKey:@"name"];
        NSString *age = [[babyList objectAtIndex:prevIndex] objectForKey:@"age"];
        NSLog(@"%@",age);
        self.txtInfo1.text = [NSString stringWithFormat:LocalizedString(@"txt_baby_info", nil), [[babyList objectAtIndex:prevIndex] objectForKey:@"age"], [[babyList objectAtIndex:prevIndex] objectForKey:@"height"], [[babyList objectAtIndex:prevIndex] objectForKey:@"weight"], [[babyList objectAtIndex:prevIndex] objectForKey:@"blood_type"]];
        
        [self.imgBabyPic2 sd_setImageWithURL:[NSURL URLWithString:[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"photo_url"]] placeholderImage:[UIImage imageNamed:@"dummy.png"]];
        self.txtName2.text = [[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"name"];
        self.txtInfo2.text = [NSString stringWithFormat:LocalizedString(@"txt_baby_info", nil), [[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"age"], [[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"height"], [[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"weight"], [[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"blood_type"]];
        
        [self.imgBabyPic3 sd_setImageWithURL:[NSURL URLWithString:[[babyList objectAtIndex:nextIndex] objectForKey:@"photo_url"]] placeholderImage:[UIImage imageNamed:@"dummy.png"]];
        self.txtName3.text = [[babyList objectAtIndex:nextIndex] objectForKey:@"name"];
        self.txtInfo3.text = [NSString stringWithFormat:LocalizedString(@"txt_baby_info", nil), [[babyList objectAtIndex:nextIndex] objectForKey:@"age"], [[babyList objectAtIndex:nextIndex] objectForKey:@"height"], [[babyList objectAtIndex:nextIndex] objectForKey:@"weight"], [[babyList objectAtIndex:nextIndex] objectForKey:@"blood_type"]];
    }
    
    [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"feed_info"]] forKey:@"feed_info"];
    [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"pump_info"]] forKey:@"pump_info"];
    [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"sleep_info"]] forKey:@"sleep_info"];
    [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"poo_info"]] forKey:@"poo_info"];
    [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"wee_info"]] forKey:@"wee_info"];
    [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"medical_info"]] forKey:@"medical_info"];
    [shareObject synchronize];
}

- (void)refreshAction
{
    NSLog(@"refreshAction");
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    [shareObject setInteger:[[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"id"] intValue] forKey:@"baby_id"];
    [shareObject setInteger:selectedBabyIndex forKey:@"baby_index"];
    [shareObject synchronize];
    
    isLoading = NO;
    self.collectionView.showsInfiniteScrolling = NO;
    
    feedReminderCheck = pumpReminderCheck = sleepReminderCheck = pooReminderCheck = weeReminderCheck = medicalReminderCheck = NO;
    for(UILocalNotification *aNotif in [[UIApplication sharedApplication] scheduledLocalNotifications])
    {
        NSLog(@"aNotif.userInfo=%@", aNotif.userInfo);
        NSString *type = [aNotif.userInfo objectForKey:@"type"];
        int babyId = [[aNotif.userInfo objectForKey:@"baby_id"] intValue];
        NSLog(@"notification=%@, %d", type, babyId);
        
        if(babyId == [shareObject integerForKey:@"baby_id"])
        {
            if([type isEqualToString:@"feed"])
            {
                feedReminderCheck = YES;
            }
            if([type isEqualToString:@"pump"])
            {
                pumpReminderCheck = YES;
            }
            if([type isEqualToString:@"sleep"])
            {
                sleepReminderCheck = YES;
            }
            if([type isEqualToString:@"poo"])
            {
                pooReminderCheck = YES;
            }
            if([type isEqualToString:@"wee"])
            {
                weeReminderCheck = YES;
            }
            if([type isEqualToString:@"medical"])
            {
                medicalReminderCheck = YES;
            }
        }
    }
    
    [self getActivityList];
}

- (void)getActivityList
{
    if(isLoading) return;
    isLoading = YES;
    [SVProgressHUD show];
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setValue:@([shareObject integerForKey:@"user_id"]) forKey:@"user_id"];
    [parameters setValue:@([shareObject integerForKey:@"baby_id"]) forKey:@"baby_id"];
    [parameters setValue:@(0) forKey:@"offset"];
    [parameters setValue:@(30) forKey:@"limit"];
    
    [[WebServiceManager sharedManager] initWithBaseURL:@"api/getActivityList" parameters:parameters success:^(id result) {
        
        activityList = (NSMutableArray *)[result valueForKey:@"list"];
        NSLog(@"activityList=%@", activityList);

        self.collectionView.showsInfiniteScrolling = NO;
        isLoading = NO;
        [self.collectionView reloadData];
        
        [self.collectionView.pullToRefreshView stopAnimating];
        result = nil;
        
    } failure:^(NSError *error) {
        isLoading = NO;
        [self.collectionView reloadData];
        [self.collectionView.pullToRefreshView stopAnimating];
        [self.collectionView.infiniteScrollingView stopAnimating];
    }];
}

#pragma mark : Add Activity
- (void)addFeedAction
{
    [appDelegate.tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"ui_action" action:@"button_press" label:@"View Profile" value:nil] build]];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Activities" bundle:nil];
    UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"ActivityFeedAddViewController"];
    viewController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:viewController animated:YES];
}

- (void)addPumpAction
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Activities" bundle:nil];
    UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"ActivityPumpAddViewController"];
    viewController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:viewController animated:YES];
}

- (void)addSleepAction
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Activities" bundle:nil];
    UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"ActivitySleepAddViewController"];
    viewController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:viewController animated:YES];
}

- (void)addPooAction
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PooWeeMedStoryboard" bundle:nil];
    UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"ActivityPooAddViewController"];
    viewController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:viewController animated:YES];
}

- (void)addWeeAction
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PooWeeMedStoryboard" bundle:nil];
    UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"ActivityWeeAddViewController"];
    viewController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:viewController animated:YES];
}

- (void)addMedicalAction
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PooWeeMedStoryboard" bundle:nil];
    UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"ActivityMedicalAddViewController"];
    viewController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:viewController animated:YES];
}

- (void)getFeedReminderAction
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Activities" bundle:nil];
    UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"ActivityReminderViewController"];
    viewController.data = [[NSMutableDictionary alloc] initWithDictionary:@{@"name":[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"name"], @"type":@"feed"}];
    viewController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:viewController animated:YES];
}

- (void)getPumpReminderAction
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Activities" bundle:nil];
    UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"ActivityReminderViewController"];
    viewController.data = [[NSMutableDictionary alloc] initWithDictionary:@{@"name":[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"name"], @"type":@"pump"}];
    viewController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:viewController animated:YES];
}

- (void)getSleepReminderAction
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Activities" bundle:nil];
    UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"ActivityReminderViewController"];
    viewController.data = [[NSMutableDictionary alloc] initWithDictionary:@{@"name":[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"name"], @"type":@"sleep"}];
    viewController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:viewController animated:YES];
}

- (void)getPooReminderAction
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Activities" bundle:nil];
    UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"ActivityReminderViewController"];
    viewController.data = [[NSMutableDictionary alloc] initWithDictionary:@{@"name":[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"name"], @"type":@"poo"}];
    viewController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:viewController animated:YES];
}

- (void)getWeeReminderAction
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Activities" bundle:nil];
    UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"ActivityReminderViewController"];
    viewController.data = [[NSMutableDictionary alloc] initWithDictionary:@{@"name":[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"name"], @"type":@"wee"}];
    viewController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:viewController animated:YES];
}

- (void)getMedicalReminderAction
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Activities" bundle:nil];
    UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"ActivityReminderViewController"];
    viewController.data = [[NSMutableDictionary alloc] initWithDictionary:@{@"name":[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"name"], @"type":@"medical"}];
    viewController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:viewController animated:YES];
}

- (IBAction)getBabyInfoAction:(id)sender
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Settings" bundle:nil];
    UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"SettingsBabyViewController"];
    viewController.data = [[babyList objectAtIndex:selectedBabyIndex] mutableCopy];
    viewController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:viewController animated:YES];
}

- (IBAction)getSettingsAction:(id)sender
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Settings" bundle:nil];
    UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"SettingsViewController"];
    viewController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:viewController animated:YES];
}


#pragma mark - Collection view data source
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return activityList.count;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static ActivitiesCollectionViewCell *sizingCell = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sizingCell = [[NSBundle mainBundle] loadNibNamed:@"ActivitiesCollectionViewCell" owner:self options:nil][0];
    });
    
    int w = (self.collectionView.bounds.size.width-4) / 2.0f;
    return CGSizeMake(w, self.collectionView.frame.size.height/3);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(2, 2, 2, 2);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *name = [[activityList objectAtIndex:indexPath.row] objectForKey:@"name"];
    
    if([name isEqualToString:@"feed"])
    {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Activities" bundle:nil];
        UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"ActivityFeedViewController"];
        viewController.data = [[NSMutableDictionary alloc] initWithDictionary:@{@"monthsold":[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"monthsold"]}];
        viewController.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:viewController animated:YES];
    }
    if([name isEqualToString:@"wee"])
    {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PooWeeMedStoryboard" bundle:nil];
        UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"ActivityWeeViewController"];
        viewController.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:viewController animated:YES];
    }
    if([name isEqualToString:@"poo"])
    {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PooWeeMedStoryboard" bundle:nil];
        UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"ActivityPooViewController"];
        viewController.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:viewController animated:YES];
    }
    if([name isEqualToString:@"pump"])
    {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Activities" bundle:nil];
        UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"ActivityPumpViewController"];
        viewController.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:viewController animated:YES];
    }
    if([name isEqualToString:@"sleep"])
    {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Activities" bundle:nil];
        UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"ActivitySleepViewController"];
        viewController.data = [[NSMutableDictionary alloc] initWithDictionary:@{@"monthsold":[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"monthsold"]}];
        viewController.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:viewController animated:YES];
    }
    if([name isEqualToString:@"medical"])
    {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"PooWeeMedStoryboard" bundle:nil];
        UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"ActivityMedicalViewController"];
        viewController.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:viewController animated:YES];
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    [collectionView registerNib:[UINib nibWithNibName:@"ActivitiesCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"ActivitiesCollectionViewCell"];
    ActivitiesCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ActivitiesCollectionViewCell" forIndexPath:indexPath];
    
    CGFloat fontSizeMax = 20.0f;
    CGFloat fontSizeMin = 14.0f;

    if (IS_IPAD) {
        cell.txtName.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        cell.txtTime.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:28.0f];
        cell.txtDiff.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f];
        cell.txtValue.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:23.0f];
        cell.txtValue2.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:20.0f];
        fontSizeMax = 28.0f;
        fontSizeMin = 22.0f;
    }
    else if(IS_IPHONE_5) {
        cell.txtName.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:14.0f];
        cell.txtTime.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:18.0f];
        cell.txtDiff.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:11.0f];
        cell.txtValue.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:13.0f];
        cell.txtValue2.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:10.0f];
        fontSizeMax = 18.0f;
        fontSizeMin = 12.0f;
    }
    else {
        cell.txtName.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:16.0f];
        cell.txtTime.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:20.0f];
        cell.txtDiff.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:13.0f];
        cell.txtValue.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:15.0f];
        cell.txtValue2.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:12.0f];
        fontSizeMax = 20.0f;
        fontSizeMin = 14.0f;
    }
    [cell layoutSubviews];
    
    if(!isLoading)
    {
        NSString *name = [[activityList objectAtIndex:indexPath.row] objectForKey:@"name"];
        NSString *type = [[activityList objectAtIndex:indexPath.row] objectForKey:@"type"];
        NSLog(@"name=%@", name);
        
        [cell.btnAlarm removeTarget:self action:nil forControlEvents:UIControlEventTouchUpInside];
        [cell.btnAdd removeTarget:self action:nil forControlEvents:UIControlEventTouchUpInside];
        
        if([name isEqualToString:@"feed"])
        {
            if([type isEqualToString:@"solids"])
            {
                [[activityList objectAtIndex:indexPath.row] setObject:[self convertUnit:[[activityList objectAtIndex:indexPath.row] objectForKey:@"attribute_value"] unit:[unitInfo objectForKey:@"serving"] spacing:NO] forKey:@"attribute_value"];
            }
            else if([type isEqualToString:@"lboob"] || [type isEqualToString:@"rboob"])
            {
                [[activityList objectAtIndex:indexPath.row] setObject:[[[activityList objectAtIndex:indexPath.row] objectForKey:@"attribute_value"] stringByReplacingOccurrencesOfString:@" min" withString:@"m"] forKey:@"attribute_value"];
            }
            else if([type isEqualToString:@"lboob+rboob"])
            {
                [[activityList objectAtIndex:indexPath.row] setObject:[[[activityList objectAtIndex:indexPath.row] objectForKey:@"attribute_value"] stringByReplacingOccurrencesOfString:@" min" withString:@"m"] forKey:@"attribute_value"];
            }
            else
            {
                [[activityList objectAtIndex:indexPath.row] setObject:[self convertUnit:[[activityList objectAtIndex:indexPath.row] objectForKey:@"attribute_value"] unit:[unitInfo objectForKey:@"volume"] spacing:NO] forKey:@"attribute_value"];
            }
            
            if(feedReminderCheck)
            {
                cell.imgIconAlarm.tintColor = UIColorFromRGB(0xF4EA9E);
            }
            else
            {
                cell.imgIconAlarm.tintColor = UIColorFromRGB(0xEDE8E4);
            }
            cell.imgIcon.backgroundColor = UIColorFromRGB(0xF4EA9E);
            cell.imgIconAdd.tintColor = UIColorFromRGB(0xF4EA9E);
            cell.viewBg.layer.shadowColor = UIColorFromRGB(0xF4EA9E).CGColor;
            
            [cell.btnAlarm addTarget:self action:@selector(getFeedReminderAction) forControlEvents:UIControlEventTouchUpInside];
            [cell.btnAdd addTarget:self action:@selector(addFeedAction) forControlEvents:UIControlEventTouchUpInside];
            
            cell.imgQuickAdd.tintColor = UIColorFromRGB(0xF4EA9E);
        }
        else if([name isEqualToString:@"pump"])
        {
            [[activityList objectAtIndex:indexPath.row] setObject:[self convertUnit:[[activityList objectAtIndex:indexPath.row] objectForKey:@"attribute_value"] unit:[unitInfo objectForKey:@"volume"] spacing:NO] forKey:@"attribute_value"];
            
            if(pumpReminderCheck)
            {
                cell.imgIconAlarm.tintColor = UIColorFromRGB(0xF9C99B);
            }
            else
            {
                cell.imgIconAlarm.tintColor = UIColorFromRGB(0xEDE8E4);
            }
            cell.imgIcon.backgroundColor = UIColorFromRGB(0xF9C99B);
            cell.imgIconAdd.tintColor = UIColorFromRGB(0xF9C99B);
            cell.viewBg.layer.shadowColor = UIColorFromRGB(0xF9C99B).CGColor;
            
            [cell.btnAlarm addTarget:self action:@selector(getPumpReminderAction) forControlEvents:UIControlEventTouchUpInside];
            [cell.btnAdd addTarget:self action:@selector(addPumpAction) forControlEvents:UIControlEventTouchUpInside];
            
            cell.imgQuickAdd.tintColor = UIColorFromRGB(0xF9C99B);
        }
        else if([name isEqualToString:@"sleep"])
        {
            [[activityList objectAtIndex:indexPath.row] setObject:[self convertDuration:[[[activityList objectAtIndex:indexPath.row] objectForKey:@"value"] intValue] fullFormatCheck:NO] forKey:@"attribute_value"];
            
            if(sleepReminderCheck)
            {
                cell.imgIconAlarm.tintColor = UIColorFromRGB(0xC5D7E1);
            }
            else
            {
                cell.imgIconAlarm.tintColor = UIColorFromRGB(0xEDE8E4);
            }
            cell.imgIcon.backgroundColor = UIColorFromRGB(0xC5D7E1);
            cell.imgIconAdd.tintColor = UIColorFromRGB(0xC5D7E1);
            cell.viewBg.layer.shadowColor = UIColorFromRGB(0xC5D7E1).CGColor;
            
            [cell.btnAlarm addTarget:self action:@selector(getSleepReminderAction) forControlEvents:UIControlEventTouchUpInside];
            [cell.btnAdd addTarget:self action:@selector(addSleepAction) forControlEvents:UIControlEventTouchUpInside];
            
            cell.imgQuickAdd.tintColor = UIColorFromRGB(0xC5D7E1);
        }
        else if([name isEqualToString:@"poo"])
        {
            if(pooReminderCheck)
            {
                cell.imgIconAlarm.tintColor = UIColorFromRGB(0xE0C5B8);
            }
            else
            {
                cell.imgIconAlarm.tintColor = UIColorFromRGB(0xEDE8E4);
            }
            cell.imgIcon.backgroundColor = UIColorFromRGB(0xE0C5B8);
            cell.imgIconAdd.tintColor = UIColorFromRGB(0xE0C5B8);
            cell.viewBg.layer.shadowColor = UIColorFromRGB(0xE0C5B8).CGColor;
            
            [cell.btnAlarm addTarget:self action:@selector(getPooReminderAction) forControlEvents:UIControlEventTouchUpInside];
            [cell.btnAdd addTarget:self action:@selector(addPooAction) forControlEvents:UIControlEventTouchUpInside];
            
            cell.imgQuickAdd.tintColor = UIColorFromRGB(0xE0C5B8);
        }
        else if([name isEqualToString:@"wee"])
        {
            if(weeReminderCheck)
            {
                cell.imgIconAlarm.tintColor = UIColorFromRGB(0xCADAA9);
            }
            else
            {
                cell.imgIconAlarm.tintColor = UIColorFromRGB(0xEDE8E4);
            }
            cell.imgIcon.backgroundColor = UIColorFromRGB(0xCADAA9);
            cell.imgIconAdd.tintColor = UIColorFromRGB(0xCADAA9);
            cell.viewBg.layer.shadowColor = UIColorFromRGB(0xCADAA9).CGColor;
            
            [cell.btnAlarm addTarget:self action:@selector(getWeeReminderAction) forControlEvents:UIControlEventTouchUpInside];
            [cell.btnAdd addTarget:self action:@selector(addWeeAction) forControlEvents:UIControlEventTouchUpInside];
            
            cell.imgQuickAdd.tintColor = UIColorFromRGB(0xCADAA9);
        }
        else if([name isEqualToString:@"medical"])
        {
            if([type isEqualToString:@"temperature"])
            {
                [[activityList objectAtIndex:indexPath.row] setObject:[self convertUnit:[[activityList objectAtIndex:indexPath.row] objectForKey:@"attribute_value"] unit:[unitInfo objectForKey:@"temperature"] spacing:NO] forKey:@"attribute_value"];
            }
            else if([type containsString:@"med"])
            {
                //[[activityList objectAtIndex:indexPath.row] setObject:[self convertUnit:[[activityList objectAtIndex:indexPath.row] objectForKey:@"attribute_value"] unit:[unitInfo objectForKey:@"medicine"] spacing:NO] forKey:@"attribute_value"];
            }
            
            if(medicalReminderCheck)
            {
                cell.imgIconAlarm.tintColor = UIColorFromRGB(0xF4D2D5);
            }
            else
            {
                cell.imgIconAlarm.tintColor = UIColorFromRGB(0xEDE8E4);
            }
            cell.imgIcon.backgroundColor = UIColorFromRGB(0xF4D2D5);
            cell.imgIconAdd.tintColor = UIColorFromRGB(0xF4D2D5);
            cell.viewBg.layer.shadowColor = UIColorFromRGB(0xF4D2D5).CGColor;
            
            [cell.btnAlarm addTarget:self action:@selector(getMedicalReminderAction) forControlEvents:UIControlEventTouchUpInside];
            [cell.btnAdd addTarget:self action:@selector(addMedicalAction) forControlEvents:UIControlEventTouchUpInside];
            
            cell.imgQuickAdd.tintColor = UIColorFromRGB(0xF4D2D5);
        }
        
        UIImage *imgIcon = [UIImage imageNamed:[NSString stringWithFormat:@"icon_%@.png", name]];
        cell.imgIcon.image = imgIcon;
        cell.txtValue.text = [[activityList objectAtIndex:indexPath.row] objectForKey:@"attribute_value"];
        cell.txtName.text = [[activityList objectAtIndex:indexPath.row] objectForKey:@"attribute_name"];
        
        cell.txtDiff.text = [[activityList objectAtIndex:indexPath.row] objectForKey:@"diff"];
        NSString *str = [NSString stringWithFormat:@"%@",[[activityList objectAtIndex:indexPath.row] objectForKey:@"diff"]];
        if (str.length>0 ) {
            cell.txtDiff.text = [NSString stringWithFormat:@"%@ ago",[[activityList objectAtIndex:indexPath.row] objectForKey:@"diff"]];
        }
        
        cell.txtValue2.text = @"";
        
        if([name isEqualToString:@"sleep"])
        {
            if(cell.txtDiff.text.length == 0)
            {
                cell.txtValue.text = @"";
                cell.txtValue2.text = @"";
            }
            else
            {
                if([type isEqualToString:@"sleep_cycle"])
                {
                    cell.txtValue.text = LocalizedString(@"txt_awake", nil);
                    cell.txtValue2.text = [NSString stringWithFormat:@"Last Sleep Cycle %@",[[activityList objectAtIndex:indexPath.row] objectForKey:@"attribute_value"]];
                    
                    [shareObject removeObjectForKey:[NSString stringWithFormat:@"sleep_id_%d", (int)[shareObject integerForKey:@"baby_id"]]];
                    [shareObject removeObjectForKey:[NSString stringWithFormat:@"sleep_time_%d", (int)[shareObject integerForKey:@"baby_id"]]];
                }
                else
                {
                    cell.txtValue.text = LocalizedString(@"txt_sleeping", nil);
                    cell.txtValue2.text = @"";
                    
                    formatter.dateFormat = @"yyyy-MM-dd hh:mm a";
                    NSDate *time = [formatter dateFromString:[NSString stringWithFormat:@"%@ %@", [[activityList objectAtIndex:indexPath.row] objectForKey:@"date"], [[activityList objectAtIndex:indexPath.row] objectForKey:@"time"]]];
                    
                    [shareObject setObject:@([[[activityList objectAtIndex:indexPath.row] objectForKey:@"id"] intValue]) forKey:[NSString stringWithFormat:@"sleep_id_%d", (int)[shareObject integerForKey:@"baby_id"]]];
                    [shareObject setObject:time forKey:[NSString stringWithFormat:@"sleep_time_%d", (int)[shareObject integerForKey:@"baby_id"]]];
                }
                [shareObject synchronize];
            }
        }
        
        NSString *time = [[activityList objectAtIndex:indexPath.row] objectForKey:@"time"];
        NSMutableAttributedString *timeString;
        
        if ([time isKindOfClass:[NSNull class]]) {
            NSLog(@"Error !");
        }
        else {
        timeString = [[NSMutableAttributedString alloc] initWithString:time attributes:@{NSFontAttributeName:[UIFont fontWithName:@"SFUIDisplay-Medium" size:fontSizeMax]}]; //22
        }
        
        if([[shareObject objectForKey:@"language"] hasPrefix:@"zh"])
        {
            NSArray *timeArray = [time componentsSeparatedByString:@" "];
            NSLog(@"timeArray=%@", timeArray);
            if([timeArray[1] length] > 0)
            {
                time = [NSString stringWithFormat:@"%@ %@", LocalizedString(timeArray[1], nil), timeArray[0]];
                timeString = [[NSMutableAttributedString alloc] initWithString:time attributes:@{NSFontAttributeName:[UIFont fontWithName:@"SFUIDisplay-Medium" size:fontSizeMax]}]; //22
                [timeString setAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"SFUIDisplay-Medium" size:fontSizeMin], NSBaselineOffsetAttributeName:@(4)} range:NSMakeRange(0,2)];
            }
        }
        else
        {
            [timeString setAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"SFUIDisplay-Medium" size:fontSizeMin]} range:NSMakeRange(6,2)];
        }
        
        [cell.txtTime setAttributedText:timeString];
        
        cell.btnQuickAdd.tag = indexPath.row;
        UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPress:)];
        longPress.minimumPressDuration = 1.0;
        [cell.btnQuickAdd addGestureRecognizer:longPress];
        
        [cell.btnQuickAdd addTarget:self action:@selector(showAnimation:) forControlEvents:UIControlEventTouchDown];
        [cell.btnQuickAdd addTarget:self action:@selector(removeAnimation:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    return cell;
}


#pragma mark -
#pragma mark UIScrollViewDelegate Methods
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if([scrollView isEqual:self.babyScrollView])
    {
        CGFloat pageWidth = self.view.bounds.size.width;
        int pageNo = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
        NSLog(@"pageNo=%d", pageNo);
        NSLog(@"backupPageNo=%d", backupPageNo);
        
        if(pageNo == 0)
        {
            NSLog(@"selectedBabyIndex=%d", selectedBabyIndex);
            if(backupPageNo != pageNo)
            {
                int nextIndex = selectedBabyIndex;
                nextIndex = (nextIndex >= babyList.count)?0:nextIndex;
                selectedBabyIndex--;
                selectedBabyIndex = (selectedBabyIndex < 0)?(int)(babyList.count-1):selectedBabyIndex;
                int prevIndex = selectedBabyIndex-1;
                prevIndex = (prevIndex < 0)?(int)(babyList.count-1):prevIndex;
                
                NSLog(@"selectedBabyIndex=%d", selectedBabyIndex);
                NSLog(@"prevIndex=%d", prevIndex);
                NSLog(@"nextIndex=%d", nextIndex);
                
                [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"feed_info"]] forKey:@"feed_info"];
                [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"pump_info"]] forKey:@"pump_info"];
                [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"sleep_info"]] forKey:@"sleep_info"];
                [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"poo_info"]] forKey:@"poo_info"];
                [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"wee_info"]] forKey:@"wee_info"];
                [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"medical_info"]] forKey:@"medical_info"];
                [shareObject synchronize];
                
                [self.imgBabyPic1 sd_setImageWithURL:[NSURL URLWithString:[[babyList objectAtIndex:prevIndex] objectForKey:@"photo_url"]] placeholderImage:[UIImage imageNamed:@"dummy.png"]];
                self.txtName1.text = [[babyList objectAtIndex:prevIndex] objectForKey:@"name"];
                self.txtInfo1.text = [NSString stringWithFormat:LocalizedString(@"txt_baby_info", nil), [[babyList objectAtIndex:prevIndex] objectForKey:@"age"], [[babyList objectAtIndex:prevIndex] objectForKey:@"height"], [[babyList objectAtIndex:prevIndex] objectForKey:@"weight"], [[babyList objectAtIndex:prevIndex] objectForKey:@"blood_type"]];
                
                [self.imgBabyPic2 sd_setImageWithURL:[NSURL URLWithString:[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"photo_url"]] placeholderImage:[UIImage imageNamed:@"dummy.png"]];
                self.txtName2.text = [[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"name"];
                self.txtInfo2.text = [NSString stringWithFormat:LocalizedString(@"txt_baby_info", nil), [[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"age"], [[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"height"], [[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"weight"], [[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"blood_type"]];
                
                [self.imgBabyPic3 sd_setImageWithURL:[NSURL URLWithString:[[babyList objectAtIndex:nextIndex] objectForKey:@"photo_url"]] placeholderImage:[UIImage imageNamed:@"dummy.png"]];
                self.txtName3.text = [[babyList objectAtIndex:nextIndex] objectForKey:@"name"];
                self.txtInfo3.text = [NSString stringWithFormat:LocalizedString(@"txt_baby_info", nil), [[babyList objectAtIndex:nextIndex] objectForKey:@"age"], [[babyList objectAtIndex:nextIndex] objectForKey:@"height"], [[babyList objectAtIndex:nextIndex] objectForKey:@"weight"], [[babyList objectAtIndex:nextIndex] objectForKey:@"blood_type"]];
                
                self.babyScrollView.contentOffset = CGPointMake(self.view.frame.size.width, self.babyScrollView.contentOffset.y);
                backupPageNo = 1;
                
                [self refreshAction];
            }
        }
        else if(pageNo == 2)
        {
            if(backupPageNo != pageNo)
            {
                int prevIndex = selectedBabyIndex;
                prevIndex = (prevIndex < 0)?(int)(babyList.count-1):prevIndex;
                selectedBabyIndex++;
                selectedBabyIndex = (selectedBabyIndex >= babyList.count)?0:selectedBabyIndex;
                int nextIndex = selectedBabyIndex+1;
                nextIndex = (nextIndex >= babyList.count)?0:nextIndex;
                
                NSLog(@"selectedBabyIndex=%d", selectedBabyIndex);
                NSLog(@"prevIndex=%d", prevIndex);
                NSLog(@"nextIndex=%d", nextIndex);
                
                [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"feed_info"]] forKey:@"feed_info"];
                [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"pump_info"]] forKey:@"pump_info"];
                [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"sleep_info"]] forKey:@"sleep_info"];
                [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"poo_info"]] forKey:@"poo_info"];
                [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"wee_info"]] forKey:@"wee_info"];
                [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"medical_info"]] forKey:@"medical_info"];
                [shareObject synchronize];
                
                [self.imgBabyPic1 sd_setImageWithURL:[NSURL URLWithString:[[babyList objectAtIndex:prevIndex] objectForKey:@"photo_url"]] placeholderImage:[UIImage imageNamed:@"dummy.png"]];
                self.txtName1.text = [[babyList objectAtIndex:prevIndex] objectForKey:@"name"];
                self.txtInfo1.text = [NSString stringWithFormat:LocalizedString(@"txt_baby_info", nil), [[babyList objectAtIndex:prevIndex] objectForKey:@"age"], [[babyList objectAtIndex:prevIndex] objectForKey:@"height"], [[babyList objectAtIndex:prevIndex] objectForKey:@"weight"], [[babyList objectAtIndex:prevIndex] objectForKey:@"blood_type"]];
                
                [self.imgBabyPic2 sd_setImageWithURL:[NSURL URLWithString:[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"photo_url"]] placeholderImage:[UIImage imageNamed:@"dummy.png"]];
                self.txtName2.text = [[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"name"];
                self.txtInfo2.text = [NSString stringWithFormat:LocalizedString(@"txt_baby_info", nil), [[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"age"], [[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"height"], [[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"weight"], [[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"blood_type"]];
                
                [self.imgBabyPic3 sd_setImageWithURL:[NSURL URLWithString:[[babyList objectAtIndex:nextIndex] objectForKey:@"photo_url"]] placeholderImage:[UIImage imageNamed:@"dummy.png"]];
                self.txtName3.text = [[babyList objectAtIndex:nextIndex] objectForKey:@"name"];
                self.txtInfo3.text = [NSString stringWithFormat:LocalizedString(@"txt_baby_info", nil), [[babyList objectAtIndex:nextIndex] objectForKey:@"age"], [[babyList objectAtIndex:nextIndex] objectForKey:@"height"], [[babyList objectAtIndex:nextIndex] objectForKey:@"weight"], [[babyList objectAtIndex:nextIndex] objectForKey:@"blood_type"]];
                
                self.babyScrollView.contentOffset = CGPointMake(self.view.frame.size.width, self.babyScrollView.contentOffset.y);
                backupPageNo = 1;
                
                [self refreshAction];
            }
        }
        else backupPageNo = pageNo;
        
        // Check for Subscription of selected baby
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"is_Subscription_Enabled"];
        if (![[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"subscription"] isKindOfClass:[NSNull class]]) {
            BOOL isSubscribed = [[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"subscription"] boolValue];
            [[NSUserDefaults standardUserDefaults]setBool:isSubscribed forKey:@"is_Subscription_Enabled"];
        }
    }
    
}

#pragma mark - Custom Methods

-(void)showAnimation:(UIButton *)sender {
    
    dispatch_async(dispatch_get_main_queue()   , ^{
        
        ActivitiesCollectionViewCell *cell;
        CGPoint location = [sender convertPoint:CGPointZero toView:self.collectionView];
        NSIndexPath *indexPath = [self.collectionView indexPathForItemAtPoint:location];
        cell = (ActivitiesCollectionViewCell *)[self.collectionView cellForItemAtIndexPath:indexPath];
        
        animationView = [cell.imgIcon snapshotViewAfterScreenUpdates:YES];
        animationView.frame = cell.imgIcon.frame;
        [UIView animateWithDuration:0.8 delay:0 usingSpringWithDamping:0.4 initialSpringVelocity:0 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
            animationView.transform = CGAffineTransformMakeScale(1.2, 1.2);
            cell.alpha = 0.8;
        } completion:^(BOOL finished) {
        }];
        [cell addSubview:animationView];
        
    });
    
}

-(void)removeAnimation:(UIButton *)sender {
    
    dispatch_async(dispatch_get_main_queue()   , ^{
        
        ActivitiesCollectionViewCell *cell;
        CGPoint location = [sender convertPoint:CGPointZero toView:self.collectionView];
        NSIndexPath *indexPath = [self.collectionView indexPathForItemAtPoint:location];
        cell = (ActivitiesCollectionViewCell *)[self.collectionView cellForItemAtIndexPath:indexPath];
        animationView.transform = CGAffineTransformMakeScale(1, 1);
        cell.alpha = 1.0;
        [animationView removeFromSuperview];
        return;
        
    });
    
}

- (void)longPress:(UILongPressGestureRecognizer*)gesture {
    
    BOOL isFeedTimerStart = [shareObject objectForKey:[NSString stringWithFormat:@"feed_start_time_%d", (int)[shareObject integerForKey:@"baby_id"]]]?YES:NO;
    
    BOOL isPumpTimerStart = [shareObject objectForKey:[NSString stringWithFormat:@"pump_start_time_%d", (int)[shareObject integerForKey:@"baby_id"]]]?YES:NO;
    
    if (isFeedTimerStart || isPumpTimerStart) {
        
        dispatch_async(dispatch_get_main_queue()   , ^{
            ActivitiesCollectionViewCell *cell;
            CGPoint location = [gesture locationInView:self.collectionView];
            NSIndexPath *indexPath = [self.collectionView indexPathForItemAtPoint:location];
            cell = (ActivitiesCollectionViewCell *)[self.collectionView cellForItemAtIndexPath:indexPath];
            animationView.transform = CGAffineTransformMakeScale(1, 1);
            cell.alpha = 1.0;
            [animationView removeFromSuperview];
            return;
            
        });
        [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:@"Active Timer Running" input:nil];
    }

    ActivitiesCollectionViewCell *cell;
    
    if ( gesture.state == UIGestureRecognizerStateBegan ) {
        
        [COMMON_HELPER vibrateDevice];
//        CGPoint location = [gesture locationInView:self.collectionView];
//        NSIndexPath *indexPath = [self.collectionView indexPathForItemAtPoint:location];
//        cell = (ActivitiesCollectionViewCell *)[self.collectionView cellForItemAtIndexPath:indexPath];
//
//        animationView = [cell.imgIcon snapshotViewAfterScreenUpdates:YES];
//        animationView.frame = cell.imgIcon.frame;
//        [UIView animateWithDuration:0.4 delay:0 usingSpringWithDamping:0.4 initialSpringVelocity:0 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
//            animationView.transform = CGAffineTransformMakeScale(1.2, 1.2);
//            cell.alpha = 0.8;
//        } completion:^(BOOL finished) {
//        }];
//        [cell addSubview:animationView];
        
    }
    else if ( gesture.state == UIGestureRecognizerStateEnded ) {
    
        NSString *name = [[activityList objectAtIndex:gesture.view.tag] objectForKey:@"name"];
        NSMutableDictionary *paramDict =[[NSMutableDictionary alloc] init];
        NSString *type = [[activityList objectAtIndex:gesture.view.tag] objectForKey:@"type"];
        
        if ([name isEqualToString:@"sleep"] && [type length]==0) {
            
            animationView.transform = CGAffineTransformMakeScale(1, 1);
            cell.alpha = 1.0;
            [animationView removeFromSuperview];
            [self showAlertWithTitle:@"Quick Add" message:@"Can not add record !" input:nil];
            return;
            
        }
        
        if ([[[activityList objectAtIndex:gesture.view.tag] objectForKey:@"attribute_value"] isKindOfClass:[NSNull class]] || [[[activityList objectAtIndex:gesture.view.tag] objectForKey:@"attribute_value"] length] == 0) {
            
            animationView.transform = CGAffineTransformMakeScale(1, 1);
            cell.alpha = 1.0;
            [animationView removeFromSuperview];
            [self showAlertWithTitle:@"Quick Add" message:@"Can not add record !" input:nil];
            return;
            
        }
        
        [SVProgressHUD show];

        if ([name isEqualToString:@"feed"]) {
            
            NSString *currentDateTime = [self getCurrentDateTime];
            [paramDict setValue:@([shareObject integerForKey:@"user_id"]) forKey:@"user_id"];
            [paramDict setValue:@([shareObject integerForKey:@"baby_id"]) forKey:@"baby_id"];
            [paramDict setValue:@"feed" forKey:@"name"];
            [paramDict setValue:[[activityList objectAtIndex:gesture.view.tag] valueForKey:@"notes"] forKey:@"notes"];

            if ([type isEqualToString:@"lboob+rboob"]) {
                
                NSMutableDictionary *feedInfo = (NSMutableDictionary *)[NSKeyedUnarchiver unarchiveObjectWithData:[shareObject objectForKey:@"feed_info"]];

                NSDictionary *lDict = [feedInfo objectForKey:@"lboob"];
                NSDictionary *rDict = [feedInfo objectForKey:@"rboob"];
                
                NSMutableDictionary *dictL = [NSMutableDictionary new];
                [dictL setValue:[lDict valueForKey:@"value"] forKey:@"value"];
                [dictL setValue:[lDict valueForKey:@"duration"] forKey:@"duration"];
                [dictL setValue:@"min" forKey:@"unit"];
                [dictL setValue:currentDateTime forKey:@"time"];

                NSMutableDictionary *dictR = [NSMutableDictionary new];
                [dictR setValue:[rDict valueForKey:@"value"] forKey:@"value"];
                [dictR setValue:[rDict valueForKey:@"duration"] forKey:@"duration"];
                [dictR setValue:@"min" forKey:@"unit"];
                [dictR setValue:currentDateTime forKey:@"time"];

                NSMutableDictionary *dict = [NSMutableDictionary new];
                [dict setObject:dictL forKey:@"lboob"];
                [dict setObject:dictR forKey:@"rboob"];

                NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict
                                                                   options:NSJSONWritingPrettyPrinted
                                                                     error:nil];
                NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
                jsonString = [jsonString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                jsonString = [jsonString stringByReplacingOccurrencesOfString:@"\n" withString:@""];
                jsonString = [jsonString stringByReplacingOccurrencesOfString:@"\\\"" withString:@"\""];
                [paramDict setValue:@"lboob+rboob" forKey:@"type"];
                [paramDict setValue:jsonString forKey:@"nursingData"];
                
            }
            else {
                [paramDict setValue:[[activityList objectAtIndex:gesture.view.tag] valueForKey:@"type"] forKey:@"type"];
                [paramDict setValue:[[activityList objectAtIndex:gesture.view.tag] valueForKey:@"value"] forKey:@"value"];
                [paramDict setValue:[[activityList objectAtIndex:gesture.view.tag] valueForKey:@"unit"] forKey:@"unit"];
                [paramDict setValue:[[activityList objectAtIndex:gesture.view.tag] valueForKey:@"duration"] forKey:@"duration"];
                [paramDict setValue:currentDateTime forKey:@"time"];
            }
            
//            if ((![[[activityList objectAtIndex:gesture.view.tag] valueForKey:@"photo_url"] isKindOfClass:[NSNull class]]) && ([[[activityList objectAtIndex:gesture.view.tag] valueForKey:@"photo_url"] length]>0)) {
//                [paramDict setValue:[[activityList objectAtIndex:gesture.view.tag] valueForKey:@"photo_url"] forKey:@"photo_url"];
//            }
            
            [[WebServiceManager sharedManager] initWithBaseURL:@"api/addBabyActivity" parameters:paramDict success:^(id result)
             {
                 if([[result objectForKey:@"success"] boolValue]){
                     NSMutableArray *babyList = (NSMutableArray *)[NSKeyedUnarchiver unarchiveObjectWithData:[shareObject objectForKey:@"baby_list"]];
                     [[babyList objectAtIndex:[shareObject integerForKey:@"baby_index"]] setObject:[result valueForKey:@"info"] forKey:@"feed_info"];
                     [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:babyList] forKey:@"baby_list"];
                     [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[result valueForKey:@"info"]] forKey:@"feed_info"];
                     [shareObject synchronize];
                     
                     appDelegate.updateTimelineCheck = appDelegate.updateActivitiesCheck = appDelegate.updateActivityCheck = YES;

                     [self refreshAction];
                     
                 }else{
                     [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:[result objectForKey:@"message"] input:nil];
                 }
                 
                 [self showLongPressSuccess:cell];
                 
             } failure:^(NSError *error) {
                 [self showLongPressFailed:cell];
             }];
            
        }
        else if ([name isEqualToString:@"pump"]) {
            
            NSString *currentDateTime = [self getCurrentDateTime];
            [paramDict setValue:@([shareObject integerForKey:@"user_id"]) forKey:@"user_id"];
            [paramDict setValue:@([shareObject integerForKey:@"baby_id"]) forKey:@"baby_id"];
            [paramDict setValue:@"pump" forKey:@"name"];
            [paramDict setValue:[[activityList objectAtIndex:gesture.view.tag] valueForKey:@"notes"] forKey:@"notes"];

            if ([type isEqualToString:@"left+right"]) {
                
                NSMutableDictionary *pumpInfo = (NSMutableDictionary *)[NSKeyedUnarchiver unarchiveObjectWithData:[shareObject objectForKey:@"pump_info"]];
                
                NSDictionary *lDict = [pumpInfo objectForKey:@"left"];
                NSDictionary *rDict = [pumpInfo objectForKey:@"right"];

                NSMutableDictionary *dictL = [NSMutableDictionary new];
                [dictL setValue:[lDict valueForKey:@"value"] forKey:@"value"];
                [dictL setValue:[lDict valueForKey:@"duration"] forKey:@"duration"];
                [dictL setValue:@"ml" forKey:@"unit"];
                [dictL setValue:currentDateTime forKey:@"time"];
                
                NSMutableDictionary *dictR = [NSMutableDictionary new];
                [dictR setValue:[rDict valueForKey:@"value"] forKey:@"value"];
                [dictR setValue:[rDict valueForKey:@"duration"] forKey:@"duration"];
                [dictR setValue:@"ml" forKey:@"unit"];
                [dictR setValue:currentDateTime forKey:@"time"];
                
                NSMutableDictionary *dict = [NSMutableDictionary new];
                [dict setObject:dictL forKey:@"left"];
                [dict setObject:dictR forKey:@"right"];
                
                NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict
                                                                   options:NSJSONWritingPrettyPrinted
                                                                     error:nil];
                NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
                jsonString = [jsonString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                jsonString = [jsonString stringByReplacingOccurrencesOfString:@"\n" withString:@""];
                jsonString = [jsonString stringByReplacingOccurrencesOfString:@"\\\"" withString:@"\""];
                [paramDict setValue:@"left+right" forKey:@"type"];
                [paramDict setValue:jsonString forKey:@"nursingData"];

            }
            else {
                [paramDict setValue:[[activityList objectAtIndex:gesture.view.tag] valueForKey:@"type"] forKey:@"type"];
                [paramDict setValue:[[activityList objectAtIndex:gesture.view.tag] valueForKey:@"value"] forKey:@"value"];
                [paramDict setValue:[[activityList objectAtIndex:gesture.view.tag] valueForKey:@"unit"] forKey:@"unit"];
                [paramDict setValue:[[activityList objectAtIndex:gesture.view.tag] valueForKey:@"duration"] forKey:@"duration"];
                [paramDict setValue:currentDateTime forKey:@"time"];
            }
            
//            if ((![[[activityList objectAtIndex:gesture.view.tag] valueForKey:@"photo_url"] isKindOfClass:[NSNull class]]) && ([[[activityList objectAtIndex:gesture.view.tag] valueForKey:@"photo_url"] length]>0)) {
//                [paramDict setValue:[[activityList objectAtIndex:gesture.view.tag] valueForKey:@"photo_url"] forKey:@"photo_url"];
//            }
            
            [[WebServiceManager sharedManager] initWithBaseURL:@"api/addBabyActivity" parameters:paramDict success:^(id result)
             {
                 if([[result objectForKey:@"success"] boolValue]){
                     NSMutableArray *babyList = (NSMutableArray *)[NSKeyedUnarchiver unarchiveObjectWithData:[shareObject objectForKey:@"baby_list"]];
                     [[babyList objectAtIndex:[shareObject integerForKey:@"baby_index"]] setObject:[result valueForKey:@"info"] forKey:@"pump_info"];
                     [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:babyList] forKey:@"baby_list"];
                     [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[result valueForKey:@"info"]] forKey:@"pump_info"];
                     [shareObject synchronize];

                     appDelegate.updateTimelineCheck = appDelegate.updateActivitiesCheck = appDelegate.updateActivityCheck = YES;

                     [self refreshAction];
                     
                 }else{
                     [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:[result objectForKey:@"message"] input:nil];
                 }
                 
                 [self showLongPressSuccess:cell];
                 
             } failure:^(NSError *error) {
                 [self showLongPressFailed:cell];
             }];
        }
        else if ([name isEqualToString:@"sleep"]) {
            
            NSString *currentDateTime = [self getCurrentDateTime];
            [paramDict setValue:@([shareObject integerForKey:@"user_id"]) forKey:@"user_id"];
            [paramDict setValue:@([shareObject integerForKey:@"baby_id"]) forKey:@"baby_id"];
            [paramDict setValue:@"sleep" forKey:@"name"];
            [paramDict setValue:@"min" forKey:@"unit"];
            [paramDict setValue:currentDateTime forKey:@"time"];
            [paramDict setValue:[[activityList objectAtIndex:gesture.view.tag] valueForKey:@"notes"] forKey:@"notes"];
            
//            if ((![[[activityList objectAtIndex:gesture.view.tag] valueForKey:@"photo_url"] isKindOfClass:[NSNull class]]) && ([[[activityList objectAtIndex:gesture.view.tag] valueForKey:@"photo_url"] length]>0)) {
//                [paramDict setValue:[[activityList objectAtIndex:gesture.view.tag] valueForKey:@"photo_url"] forKey:@"photo_url"];
//            }
            
            if([shareObject integerForKey:[NSString stringWithFormat:@"sleep_id_%d", (int)[shareObject integerForKey:@"baby_id"]]] == 0)
            {
                //isSleeping = NO
                if ([type isEqualToString:@"sleep_cycle"]) {
//                    NSMutableDictionary *sleepInfo = (NSMutableDictionary *)[NSKeyedUnarchiver unarchiveObjectWithData:[shareObject objectForKey:@"sleep_info"]];
//
//                    NSDictionary *sleepInfoDict = [sleepInfo objectForKey:@"sleep_cycle"];
//                    CGFloat mins = [[sleepInfoDict valueForKey:@"duration"] doubleValue];
//                    NSDate *cycleStartDate = [[NSDate date] dateByAddingTimeInterval:-mins*60 ];
//                    NSString *cycleStartDateStr = [self formatDateTime:cycleStartDate];
//
//                    [paramDict setValue:cycleStartDateStr forKey:@"time"];
//                    [paramDict setValue:@"sleep_cycle" forKey:@"type"];
//                    [paramDict setValue:[sleepInfoDict valueForKey:@"duration"] forKey:@"duration"];
//                    [paramDict setValue:[sleepInfoDict valueForKey:@"value"] forKey:@"value"];
//                }
//                else {
                    [paramDict setValue:@"sleeping" forKey:@"type"];
                    [paramDict setValue:@(0) forKey:@"value"];
                    type = @"sleeping";
                }
                
                [[WebServiceManager sharedManager] initWithBaseURL:@"api/addBabyActivity" parameters:paramDict success:^(id result) {
                    
                    NSLog(@"result=%@", result);
                    if([[result objectForKey:@"success"] boolValue])
                    {
                        
                        if ([type isEqualToString:@"sleeping"]) {
                            
                            [shareObject setObject:[result valueForKey:@"id"] forKey:[NSString stringWithFormat:@"sleep_id_%d", (int)[shareObject integerForKey:@"baby_id"]]];
                            [shareObject setObject:currentDateTime forKey:[NSString stringWithFormat:@"sleep_time_%d", (int)[shareObject integerForKey:@"baby_id"]]];
                            
                            if(![[[[result valueForKey:@"info"] objectForKey:@"sleep"] objectForKey:@"photo_url"] isEqual:[NSNull null]])
                            {
                                [shareObject setObject:[[[result valueForKey:@"info"] objectForKey:@"sleep"] objectForKey:@"photo_url"] forKey:[NSString stringWithFormat:@"sleep_photo_url_%d", (int)[shareObject integerForKey:@"baby_id"]]];
                            }
                            if(![[[[result valueForKey:@"info"] objectForKey:@"sleep"] objectForKey:@"notes"] isEqual:[NSNull null]])
                            {
                                [shareObject setObject:[[[result valueForKey:@"info"] objectForKey:@"sleep"] objectForKey:@"notes"] forKey:[NSString stringWithFormat:@"sleep_notes_%d", (int)[shareObject integerForKey:@"baby_id"]]];
                            }
                            
                        }
                        
                        NSMutableArray *babyList = (NSMutableArray *)[NSKeyedUnarchiver unarchiveObjectWithData:[shareObject objectForKey:@"baby_list"]];
                        [[babyList objectAtIndex:[shareObject integerForKey:@"baby_index"]] setObject:[result valueForKey:@"info"] forKey:@"sleep_info"];
                        [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:babyList] forKey:@"baby_list"];
                        [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[result valueForKey:@"info"]] forKey:@"sleep_info"];
                        [shareObject synchronize];
                        
                        appDelegate.updateTimelineCheck = appDelegate.updateActivitiesCheck = appDelegate.updateActivityCheck = YES;
                        [self refreshAction];
                    }
                    else{
                        [self showAlertWithTitleCallback:LocalizedString(@"title_error", nil) message:LocalizedString(@"error_baby_sleeping", nil) callback:^(UIAlertAction *action) {
                            
                            appDelegate.updateTimelineCheck = appDelegate.updateActivitiesCheck = appDelegate.updateActivityCheck = YES;
                            [self refreshAction];
                        }];
                    }
                    
                    [self showLongPressSuccess:cell];
                    
                } failure:^(NSError *error) {
                    //[self showAlertWithTitle:LocalizedString(@"title_error", nil) message:LocalizedString(@"error_unknown", nil) input:nil];
                    [self showLongPressFailed:cell];
                }];
                
            }
            else
            {
                //isSleeping = YES;
                [paramDict setValue:@([shareObject integerForKey:[NSString stringWithFormat:@"sleep_id_%d", (int)[shareObject integerForKey:@"baby_id"]]]) forKey:@"id"];
                [paramDict setValue:@"sleep_cycle" forKey:@"type"];
                
                NSDate *selectedStart = [shareObject objectForKey:[NSString stringWithFormat:@"sleep_time_%d", (int)[shareObject integerForKey:@"baby_id"]]];
                NSDate *selectedEnd = [NSDate date];
                int diff = [selectedEnd timeIntervalSinceDate:selectedStart]/60;
                int selectedDuration = abs(diff);
                
                [paramDict setValue:[NSString stringWithFormat:@"%d", selectedDuration] forKey:@"duration"];
                [paramDict setValue:[NSString stringWithFormat:@"%d", selectedDuration] forKey:@"value"];
                [paramDict setValue:[self formatDateTime:selectedStart] forKey:@"time"];
                //[paramDict setValue:[[NSString stringWithFormat:@"%@", selectedStart] substringWithRange:NSMakeRange(0, 19)] forKey:@"time"];
                
                [[WebServiceManager sharedManager] initWithBaseURL:@"api/editBabyActivity" parameters:paramDict success:^(id result) {
                    
                    NSLog(@"result=%@", result);
                    if([[result objectForKey:@"success"] boolValue])
                    {
                        [shareObject removeObjectForKey:[NSString stringWithFormat:@"sleep_id_%d", (int)[shareObject integerForKey:@"baby_id"]]];
                        [shareObject removeObjectForKey:[NSString stringWithFormat:@"sleep_time_%d", (int)[shareObject integerForKey:@"baby_id"]]];
                        [shareObject removeObjectForKey:[NSString stringWithFormat:@"sleep_photo_url_%d", (int)[shareObject integerForKey:@"baby_id"]]];
                        [shareObject removeObjectForKey:[NSString stringWithFormat:@"sleep_notes_%d", (int)[shareObject integerForKey:@"baby_id"]]];
                        
                        NSMutableArray *babyList = (NSMutableArray *)[NSKeyedUnarchiver unarchiveObjectWithData:[shareObject objectForKey:@"baby_list"]];
                        [[babyList objectAtIndex:[shareObject integerForKey:@"baby_index"]] setObject:[result valueForKey:@"info"] forKey:@"sleep_info"];
                        [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:babyList] forKey:@"baby_list"];
                        [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[result valueForKey:@"info"]] forKey:@"sleep_info"];
                        [shareObject synchronize];
                        
                        appDelegate.updateTimelineCheck = appDelegate.updateActivitiesCheck = appDelegate.updateActivityCheck = YES;
                        [self refreshAction];
                    }
                    else{
                        [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:[result objectForKey:@"message"] input:nil];
                    }
                    
                    [self showLongPressSuccess:cell];
                    
                } failure:^(NSError *error) {
                    [self showLongPressFailed:cell];
                }];
            }
            
        }
        else if ([name isEqualToString:@"poo"]) {
            
            NSString *currentDateTime = [self getCurrentDateTime];
            
            [paramDict setValue:@([shareObject integerForKey:@"user_id"]) forKey:@"user_id"];
            [paramDict setValue:@([shareObject integerForKey:@"baby_id"]) forKey:@"baby_id"];
            [paramDict setValue:@"poo" forKey:@"name"];
            [paramDict setValue:@"poo" forKey:@"type"];
            [paramDict setValue:[[activityList objectAtIndex:gesture.view.tag] valueForKey:@"color"] forKey:@"color"];
            [paramDict setValue:[[activityList objectAtIndex:gesture.view.tag] valueForKey:@"texture"] forKey:@"texture"];
            [paramDict setValue:[[activityList objectAtIndex:gesture.view.tag] valueForKey:@"volume"] forKey:@"volume"];
            [paramDict setValue:@(1)forKey:@"value"];
            [paramDict setValue:@"time" forKey:@"unit"];
            [paramDict setValue:currentDateTime forKey:@"time"];
            [paramDict setValue:[[activityList objectAtIndex:gesture.view.tag] valueForKey:@"notes"] forKey:@"notes"];

//            if ((![[[activityList objectAtIndex:gesture.view.tag] valueForKey:@"photo_url"] isKindOfClass:[NSNull class]]) && ([[[activityList objectAtIndex:gesture.view.tag] valueForKey:@"photo_url"] length]>0)) {
//                [paramDict setValue:[[activityList objectAtIndex:gesture.view.tag] valueForKey:@"photo_url"] forKey:@"photo_url"];
//            }
            
            [[WebServiceManager sharedManager] initWithBaseURL:@"api/addBabyActivity" parameters:paramDict success:^(id result) {
                
                NSLog(@"result=%@", result);
                if([[result objectForKey:@"success"] boolValue])
                {
                    NSMutableArray *babyList = (NSMutableArray *)[NSKeyedUnarchiver unarchiveObjectWithData:[shareObject objectForKey:@"baby_list"]];
                    [[babyList objectAtIndex:[shareObject integerForKey:@"baby_index"]] setObject:[result valueForKey:@"info"] forKey:@"poo_info"];
                    [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:babyList] forKey:@"baby_list"];
                    [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[result valueForKey:@"info"]] forKey:@"poo_info"];
                    [shareObject synchronize];
                    
                    appDelegate.updateTimelineCheck = appDelegate.updateActivitiesCheck = appDelegate.updateActivityCheck = YES;
                    [self refreshAction];
                }
                else{
                    [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:[result objectForKey:@"message"] input:nil];
                }
                
                [self showLongPressSuccess:cell];
                
            } failure:^(NSError *error) {
                [self showLongPressFailed:cell];
            }];
            
        }
        else if ([name isEqualToString:@"wee"]) {
            
            NSString *currentDateTime = [self getCurrentDateTime];

            [paramDict setValue:@([shareObject integerForKey:@"user_id"]) forKey:@"user_id"];
            [paramDict setValue:@([shareObject integerForKey:@"baby_id"]) forKey:@"baby_id"];
            [paramDict setValue:@"wee" forKey:@"name"];
            [paramDict setValue:@"wee" forKey:@"type"];
            [paramDict setValue:[[activityList objectAtIndex:gesture.view.tag] valueForKey:@"color"] forKey:@"color"];
            [paramDict setValue:[[activityList objectAtIndex:gesture.view.tag] valueForKey:@"volume"] forKey:@"volume"];
            [paramDict setValue:@(1)forKey:@"value"];
            [paramDict setValue:@"time" forKey:@"unit"];
            [paramDict setValue:currentDateTime forKey:@"time"];
            [paramDict setValue:[[activityList objectAtIndex:gesture.view.tag] valueForKey:@"notes"] forKey:@"notes"];

//            if ((![[[activityList objectAtIndex:gesture.view.tag] valueForKey:@"photo_url"] isKindOfClass:[NSNull class]]) && ([[[activityList objectAtIndex:gesture.view.tag] valueForKey:@"photo_url"] length]>0)) {
//                [paramDict setValue:[[activityList objectAtIndex:gesture.view.tag] valueForKey:@"photo_url"] forKey:@"photo_url"];
//            }
            
            [[WebServiceManager sharedManager] initWithBaseURL:@"api/addBabyActivity" parameters:paramDict success:^(id result) {
                
                NSLog(@"result=%@", result);
                if([[result objectForKey:@"success"] boolValue])
                {
                    NSMutableArray *babyList = (NSMutableArray *)[NSKeyedUnarchiver unarchiveObjectWithData:[shareObject objectForKey:@"baby_list"]];
                    [[babyList objectAtIndex:[shareObject integerForKey:@"baby_index"]] setObject:[result valueForKey:@"info"] forKey:@"wee_info"];
                    [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:babyList] forKey:@"baby_list"];
                    [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[result valueForKey:@"info"]] forKey:@"wee_info"];
                    [shareObject synchronize];
                    
                    appDelegate.updateTimelineCheck = appDelegate.updateActivitiesCheck = appDelegate.updateActivityCheck = YES;
                    [self refreshAction];
                }
                else{
                    [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:[result objectForKey:@"message"] input:nil];
                }
                
                [self showLongPressSuccess:cell];
                
            } failure:^(NSError *error) {
                [self showLongPressFailed:cell];
            }];
            
        }
        else if ([name isEqualToString:@"medical"]) {
            
            NSString *currentDateTime = [self getCurrentDateTime];
            
            [paramDict setValue:@([shareObject integerForKey:@"user_id"]) forKey:@"user_id"];
            [paramDict setValue:@([shareObject integerForKey:@"baby_id"]) forKey:@"baby_id"];
            [paramDict setValue:@"medical" forKey:@"name"];
            [paramDict setValue:[[activityList objectAtIndex:gesture.view.tag] valueForKey:@"type"] forKey:@"type"];
            [paramDict setValue:[[activityList objectAtIndex:gesture.view.tag] valueForKey:@"value"] forKey:@"value"];
            [paramDict setValue:[[activityList objectAtIndex:gesture.view.tag] valueForKey:@"unit"] forKey:@"unit"];
            [paramDict setValue:[[activityList objectAtIndex:gesture.view.tag] valueForKey:@"medicine"] forKey:@"medicine"];

            [paramDict setValue:currentDateTime forKey:@"time"];
            [paramDict setValue:[[activityList objectAtIndex:gesture.view.tag] valueForKey:@"notes"] forKey:@"notes"];
            
//            if ((![[[activityList objectAtIndex:gesture.view.tag] valueForKey:@"photo_url"] isKindOfClass:[NSNull class]]) && ([[[activityList objectAtIndex:gesture.view.tag] valueForKey:@"photo_url"] length]>0)) {
//                [paramDict setValue:[[activityList objectAtIndex:gesture.view.tag] valueForKey:@"photo_url"] forKey:@"photo_url"];
//            }
            
            [[WebServiceManager sharedManager] initWithBaseURL:@"api/addBabyActivity" parameters:paramDict success:^(id result) {
                
                NSLog(@"result=%@", result);
                if([[result objectForKey:@"success"] boolValue])
                {
                    NSMutableArray *babyList = (NSMutableArray *)[NSKeyedUnarchiver unarchiveObjectWithData:[shareObject objectForKey:@"baby_list"]];
                    [[babyList objectAtIndex:[shareObject integerForKey:@"baby_index"]] setObject:[result valueForKey:@"info"] forKey:@"medical_info"];
                    [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:babyList] forKey:@"baby_list"];
                    [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[result valueForKey:@"info"]] forKey:@"medical_info"];
                    [shareObject synchronize];
                    
                    appDelegate.updateTimelineCheck = appDelegate.updateActivitiesCheck = appDelegate.updateActivityCheck = YES;
                    [self refreshAction];
                }
                else
                {
                    [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:[result objectForKey:@"message"] input:nil];
                }
                
                [self showLongPressSuccess:cell];
                
            } failure:^(NSError *error) {
                [self showLongPressFailed:cell];
            }];

        }
        
    }
}

-(void)showLongPressSuccess:(ActivitiesCollectionViewCell *)animCell
{
    [UIView animateWithDuration:0.4 delay:0 usingSpringWithDamping:0.4 initialSpringVelocity:0 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
        animationView.transform = CGAffineTransformMakeScale(1, 1);
        animCell.alpha = 1.0;
    } completion:^(BOOL finished) {
    }];
    [animationView removeFromSuperview];
    animationView = nil;
    [SVProgressHUD showSuccessWithStatus:@"Success"];
    [SVProgressHUD dismissWithDelay:2.0];
}

-(void)showLongPressFailed:(ActivitiesCollectionViewCell *)animCell
{
    [UIView animateWithDuration:0.4 delay:0 usingSpringWithDamping:0.4 initialSpringVelocity:0 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
        animationView.transform = CGAffineTransformMakeScale(1, 1);
        animCell.alpha = 1.0;
    } completion:^(BOOL finished) {
    }];
    [animationView removeFromSuperview];
    animationView = nil;
    [SVProgressHUD showErrorWithStatus:@"Failed"];
    [SVProgressHUD dismissWithDelay:2.0];
}

-(NSString *)getCurrentDateTime
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    //[dateFormatter setDateFormat:@"E d MMM, yyyy hh:mm a"];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    [dateFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
    NSString *dateStr = [dateFormatter stringFromDate:[NSDate date]];
    NSLog(@"%@",dateStr);
    return dateStr;

}

-(NSString *)formatDateTime:(NSDate *)sDate
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    //[dateFormatter setDateFormat:@"E d MMM, yyyy hh:mm a"];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    [dateFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
    NSString *dateStr = [dateFormatter stringFromDate:sDate];
    NSLog(@"%@",dateStr);
    return dateStr;
    
}

/*#pragma mark Central Manager delegate methods
- (void)centralManagerDidUpdateState:(CBCentralManager *)central
{
    if([central state] == CBCentralManagerStatePoweredOff)
    {
        NSLog(@"CoreBluetooth BLE hardware is powered off");
    }
    else if([central state] == CBCentralManagerStatePoweredOn)
    {
        NSLog(@"CoreBluetooth BLE hardware is powered on and ready");
    }
    else if([central state] == CBCentralManagerStateUnauthorized)
    {
        NSLog(@"CoreBluetooth BLE state is unauthorized");
    }
    else if([central state] == CBCentralManagerStateUnknown)
    {
        NSLog(@"CoreBluetooth BLE state is unknown");
    }
    else if([central state] == CBCentralManagerStateUnsupported)
    {
        NSLog(@"CoreBluetooth BLE hardware is unsupported on this platform");
    }
}*/

/*!
 * @brief Starts scanning for peripherals with rscServiceUUID
 * @param enable If YES, this method will enable scanning for bridge devices, if NO it will stop scanning
 * @return 0 if success, -1 if Bluetooth Manager is not in CBCentralManagerStatePoweredOn state.
 */
/*- (int)scanForPeripherals:(BOOL)enable
{
    if(appDelegate.cbCentralManager.state != CBCentralManagerStatePoweredOn)
    {
        return -1;
    }
    
    // Scanner uses other queue to send events. We must edit UI in the main queue
    dispatch_async(dispatch_get_main_queue(), ^{
        if(enable)
        {
            NSLog(@"appDelegate here");
            NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:YES], CBCentralManagerScanOptionAllowDuplicatesKey, nil];
            [appDelegate.cbCentralManager scanForPeripheralsWithServices:nil options:options];
        }
        else
        {
            [appDelegate.cbCentralManager stopScan];
        }
    });
    
    return 0;
}

- (void)centralManager:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)peripheral advertisementData:(NSDictionary *)advertisementData RSSI:(NSNumber *)RSSI
{
    // Scanner uses other queue to send events. We must edit UI in the main queue
    if ([[advertisementData objectForKey:CBAdvertisementDataIsConnectable] boolValue])
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            // Add the sensor to the list and reload deta set
            ScannedPeripheral *sensor = [ScannedPeripheral initWithPeripheral:peripheral rssi:RSSI.intValue isPeripheralConnected:NO];
            //NSLog(@"sensor=%@", sensor.name);
            
            NSMutableArray *deviceList = (NSMutableArray *)[NSKeyedUnarchiver unarchiveObjectWithData:[shareObject objectForKey:@"device_list"]];
            NSLog(@"deviceList=%@", deviceList);
            
            for(int i=0; i<deviceList.count; i++)
            {
                if([[[deviceList objectAtIndex:i] objectForKey:@"name"] isEqualToString:sensor.name] && [[[deviceList objectAtIndex:i] objectForKey:@"wearable_check"] boolValue])
                {
                    appDelegate.peripheral = [sensor peripheral];
                    [self scanForPeripherals:NO];
                    [self centralManager:appDelegate.cbCentralManager didPeripheralSelected:appDelegate.peripheral];
                    
                    NSLog(@"Connecting to %@...", appDelegate.peripheral.name);
                    [appDelegate.cbCentralManager connectPeripheral:appDelegate.peripheral options:nil];
                    break;
                }
            }
        });
    }
}


#pragma mark - Scanner Delegate methods
- (void)centralManager:(CBCentralManager *)manager didPeripheralSelected:(CBPeripheral *)peripheral
{
    NSLog(@"connectPeripheral2");
    // We may not use more than one Central Manager instance. Let's just take the one returned from Scanner View Controller
    appDelegate.bluetoothManager = [[BluetoothManager alloc] initWithManager:manager];
    appDelegate.bluetoothManager.delegate = self;
    [appDelegate.bluetoothManager connectDevice:peripheral];
}


#pragma mark - BluetoothManager delegate methods
- (void)didDeviceConnected:(NSString *)peripheralName
{
    NSLog(@"didDeviceConnected %@", peripheralName);
}
- (void)didDeviceDisconnected
{
    NSLog(@"didDeviceDisconnected");
    appDelegate.bluetoothManager = nil;
}
- (void)isDeviceReady
{
    NSLog(@"Device is ready");
}
- (void)deviceNotSupported
{
    NSLog(@"Device not supported");
}
- (void)didCharacteristicsFound
{
    NSLog(@"didCharacteristicsFound");
}
- (void)didUpdateValueForCharacteristic:(NSString *)message
{
    NSLog(@"didUpdateValueForCharacteristic %@", message);
    
    NSArray *hexData = [message componentsSeparatedByString:@"-"];
    if(hexData.count >= 20)
    {
        [appDelegate addActivity:hexData];
    }
}*/

@end
