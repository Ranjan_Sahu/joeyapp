//
//  ActivityFeedAddViewController.h
//  Joey
//
//  Created by csl on 5/1/2017.
//  Copyright © 2017 Auxilia. All rights reserved.
//

#import "BaseTableViewController.h"
#import <Photos/Photos.h>
#import <AVFoundation/AVFoundation.h>

@interface ActivityFeedAddViewController : BaseTableViewController <UINavigationControllerDelegate, UIImagePickerControllerDelegate, UICollectionViewDelegate, UICollectionViewDataSource>

@property (nonatomic, weak) IBOutlet UIBarButtonItem *btnSave;
@property (nonatomic, weak) IBOutlet UILabel *lblVolume;

@property (nonatomic, weak) IBOutlet UILabel *lblDate;
@property (nonatomic, weak) IBOutlet UILabel *txtDate;
@property (nonatomic, weak) IBOutlet UILabel *lblDuration;
@property (nonatomic, weak) IBOutlet UILabel *lblPhoto;

@property (nonatomic, weak) IBOutlet UIButton *btnPhoto1;
@property (nonatomic, weak) IBOutlet UIButton *btnPhoto2;
@property (nonatomic, weak) IBOutlet UIButton *btnPhoto3;

@property (nonatomic, weak) IBOutlet UILabel *lblNotes;
@property (nonatomic, weak) IBOutlet UITextView *txtNotes;

@property (nonatomic, weak) IBOutlet UILabel *lblSide;
@property (nonatomic, weak) IBOutlet UILabel *lblTimer;
@property (nonatomic, weak) IBOutlet UIButton *btnLeft;
@property (nonatomic, weak) IBOutlet UIButton *btnRight;
@property (nonatomic, weak) IBOutlet UIButton *btnStartLeft;
@property (nonatomic, weak) IBOutlet UIButton *btnStartRight;

@property (nonatomic, weak) IBOutlet UILabel *lblLeftDurationValue;
@property (nonatomic, weak) IBOutlet UILabel *lblRightDurationValue;
@property (nonatomic, weak) IBOutlet UIPickerView *durationPickerLeft;
@property (nonatomic, weak) IBOutlet UIPickerView *durationPickerRight;

@property (nonatomic, weak) IBOutlet UIPickerView *volumeValuePicker;
@property (nonatomic, weak) IBOutlet UIPickerView *volumeUnitPicker;

@property (nonatomic, weak) IBOutlet UIView *durationLeftView;
@property (nonatomic, weak) IBOutlet UIView *durationRightView;

@property (nonatomic, weak) IBOutlet UILabel *lblDurationUnitLeft;
@property (nonatomic, weak) IBOutlet UILabel *lblDurationUnitRight;

@property (nonatomic, weak) IBOutlet UICollectionView *feedSelectionCollectionView;

// Keywords View
@property (nonatomic, weak) IBOutlet UIButton *btnaddKeyword;

@property (weak, nonatomic) IBOutlet UIView *keywordView;
@property (nonatomic, weak) IBOutlet UILabel *keyword1;
@property (nonatomic, weak) IBOutlet UILabel *keyword2;
@property (nonatomic, weak) IBOutlet UILabel *keyword3;
@property (nonatomic, weak) IBOutlet UILabel *keyword4;
@property (nonatomic, weak) IBOutlet UILabel *keyword5;
@property (nonatomic, weak) IBOutlet UILabel *keyword6;
@property (nonatomic, weak) IBOutlet UILabel *keyword7;
@property (nonatomic, weak) IBOutlet UILabel *keyword8;
@property (nonatomic, weak) IBOutlet UILabel *keyword9;

@property (nonatomic, weak) IBOutlet UIImageView *upArrow2;
@property (nonatomic, weak) IBOutlet UIImageView *downArrow2;
@end
