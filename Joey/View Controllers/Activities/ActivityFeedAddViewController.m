//
//  ActivityFeedAddViewController.m
//  Joey
//
//  Created by csl on 5/1/2017.
//  Copyright © 2017 Auxilia. All rights reserved.
//

#import "AppDelegate.h"
#import "ActivityFeedAddViewController.h"
#import <AudioToolbox/AudioToolbox.h>
#import "SettingsFamilyBabyCollectionViewCell.h"
#import "ActivitySelectCollectionViewCell.h"
#import "VideoViewController.h"

@interface ActivityFeedAddViewController () <UIPickerViewDelegate, UIPickerViewDataSource>
{
    NSUserDefaults *shareObject;
    AppDelegate *appDelegate;
    NSMutableDictionary *unitInfo;
    NSMutableDictionary *feedInfo;
    NSDateFormatter *formatter;
    NSMutableArray *typeList;
    NSMutableArray *typeValueList;
    NSMutableArray *typeActiveImageList;
    NSMutableArray *typeInactiveImageList;
    NSMutableArray *no100List;
    NSMutableArray *no1000List;
    NSMutableArray *no24List;
    NSMutableArray *no60List;
    NSMutableArray *digitList;
    NSMutableArray *volumeUnitList;
    NSArray *selectedVolumeIndexes;
    NSArray *selectedDurationIndexes;
    NSDate *selectedDateTime;
    NSDate *selectedStart;
    NSTimer *timer;
    
    BOOL saveCheck;
    
    BOOL editAmountCheck;
    BOOL editAmountUnitCheck;
    BOOL editStartLeftTimerCheck;
    BOOL editStartRightTimerCheck;
    BOOL editEndLeftTimerCheck;
    BOOL editEndRightTimerCheck;
    BOOL editLeftValueCheck;
    BOOL editRightValueCheck;
    BOOL editDateCheck;
    BOOL editDurationCheck;
    BOOL isTimerStart;
    
    NSInteger selectedTypeIndex;
    int selectedDuration;
    int feedSeconds;
    float selectedVolume;
    NSString *selectedType;
    NSString *selectedSide;
    NSString *selectedUnit;
    int selectedDurationValueLeft;
    int selectedDurationValueRight;
    NSMutableArray *keywordsArray;
    UIColor *keywordsColor;
    NSDictionary *keywordslistDict;
    NSString *videoUrl;
    NSString *videoUrl2;
    NSString *videoUrl3;

}
@end

@implementation ActivityFeedAddViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    shareObject = [NSUserDefaults standardUserDefaults];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    formatter = [[NSDateFormatter alloc] init];
    [formatter setTimeZone:[NSTimeZone localTimeZone]];
    [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
    
    unitInfo = (NSMutableDictionary *)[NSKeyedUnarchiver unarchiveObjectWithData:[shareObject objectForKey:@"unit_info"]];
    feedInfo = (NSMutableDictionary *)[NSKeyedUnarchiver unarchiveObjectWithData:[shareObject objectForKey:@"feed_info"]];
    NSLog(@"feedInfo=%@", feedInfo);
    
    typeValueList = [[NSMutableArray alloc] initWithObjects: @"bbottle", @"formula", @"nursing", @"solids", @"water", @"juice", nil];
    typeActiveImageList = [[NSMutableArray alloc]initWithObjects: @"icon_activity_feed_bottle_breastmilk_active",@"icon_activity_feed_bottle_formula_active",@"icon_activity_feed_nursing_active",@"icon_activity_feed_solids_active",@"icon_activity_feed_bottle_water_active",@"icon_activity_feed_bottle_juice_active", nil];
    typeInactiveImageList = [[NSMutableArray alloc]initWithObjects:@"icon_activity_feed_bottle_breastmilk_inactive",@"icon_activity_feed_bottle_formula_inactive",@"icon_activity_feed_nursing_inactive",@"icon_activity_feed_solids_inactive",@"icon_activity_feed_bottle_water_inactive",@"icon_activity_feed_bottle_juice_inactive", nil];
    
    [self.feedSelectionCollectionView registerNib:[UINib nibWithNibName:@"ActivitySelectCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"ActivitySelectCollectionViewCell"];
    self.feedSelectionCollectionView.delegate = self;
    self.feedSelectionCollectionView.dataSource = self;
    [self.feedSelectionCollectionView setAllowsMultipleSelection:NO];
    [self.feedSelectionCollectionView reloadData];

    self.durationPickerLeft.dataSource = self;
    self.durationPickerRight.dataSource = self;
    self.durationPickerLeft.delegate = self;
    self.durationPickerRight.delegate = self;
    self.volumeValuePicker.dataSource = self;
    self.volumeUnitPicker.dataSource = self;
    self.volumeValuePicker.delegate = self;
    self.volumeUnitPicker.delegate = self;
    
    typeList = [[NSMutableArray alloc] init];
    for(int i=0; i<typeValueList.count; i++) [typeList addObject:LocalizedString([typeValueList objectAtIndex:i], nil)];
    
    no100List = [[NSMutableArray alloc] init];
    for(int i=0; i<=100; i++) [no100List addObject:[NSString stringWithFormat:@"%d", i]];
    
    no1000List = [[NSMutableArray alloc] init];
    for(int i=0; i<=1000; i++) [no1000List addObject:[NSString stringWithFormat:@"%d", i]];
    
    no24List = [[NSMutableArray alloc] init];
    for(int i=0; i<24; i++) [no24List addObject:[NSString stringWithFormat:@"%d", i]];
    
    no60List = [[NSMutableArray alloc] init];
    for(int i=0; i<60; i++) [no60List addObject:[NSString stringWithFormat:@"%d", i]];
    
    digitList = [[NSMutableArray alloc] init];
    for(int i=0; i<10; i++) [digitList addObject:[NSString stringWithFormat:@"%d", i]];
    
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:UIColorFromRGB(0x8D8082), NSForegroundColorAttributeName, [UIFont fontWithName:@"SFUIDisplay-Medium" size:18.0f], NSFontAttributeName, nil]];
    [self.navigationController.navigationBar setBarTintColor:UIColorFromRGB(0xF4EA9E)];
    [self.navigationController.navigationBar setTranslucent:NO];
    [self.navigationItem setTitle:LocalizedString(@"title_new_feed", nil)];
    [self customBackButton];
    
    self.btnSave.title = LocalizedString(@"btn_save", nil);
    
    self.lblVolume.text = LocalizedString(@"txt_volume", nil);
    self.lblDate.text = LocalizedString(@"txt_date", nil);
    self.lblDuration.text = LocalizedString(@"txt_duration", nil);
    self.lblPhoto.text = LocalizedString(@"txt_photo", nil);
    self.lblNotes.text = LocalizedString(@"txt_notes", nil);
    self.lblSide.text = LocalizedString(@"txt_side", nil);
    self.lblTimer.text = LocalizedString(@"txt_timer", nil);
    
    self.btnStartLeft.layer.cornerRadius = 3.f;
    self.btnStartRight.layer.cornerRadius = 3.f;
    self.btnLeft.layer.cornerRadius = 3.f;
    self.btnRight.layer.cornerRadius = 3.f;
    [self.btnStartLeft setTitle:LocalizedString(@"btn_start", nil) forState:UIControlStateNormal];
    [self.btnStartRight setTitle:LocalizedString(@"btn_start", nil) forState:UIControlStateNormal];
    [self.btnLeft setTitle:LocalizedString(@"btn_left", nil) forState:UIControlStateNormal];
    [self.btnRight setTitle:LocalizedString(@"btn_right", nil) forState:UIControlStateNormal];
    
    self.lblDurationUnitLeft.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:18.0f];
    self.lblDurationUnitRight.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:18.0f];
    
    if(feedInfo.count > 0)
    {
        if([[feedInfo objectForKey:@"last_type"] isEqualToString:@"bbottle"]) {
            selectedTypeIndex = 0;
            keywordsColor = UIColorFromRGB(0xF4EA9E);
        }
        if([[feedInfo objectForKey:@"last_type"] isEqualToString:@"formula"]) {
            selectedTypeIndex = 1;
            keywordsColor = UIColorFromRGB(0xDACEDB);
        }
        if([[feedInfo objectForKey:@"last_type"] isEqualToString:@"lboob"]) {
            selectedTypeIndex = 2;
            keywordsColor = UIColorFromRGB(0xBDB5AF);
        }
        if([[feedInfo objectForKey:@"last_type"] isEqualToString:@"rboob"]) {
            selectedTypeIndex = 2;
            keywordsColor = UIColorFromRGB(0xBDB5AF);
        }
        if([[feedInfo objectForKey:@"last_type"] isEqualToString:@"solids"]) {
            selectedTypeIndex = 3;
            keywordsColor = UIColorFromRGB(0xC9DAAB);
        }
        if([[feedInfo objectForKey:@"last_type"] isEqualToString:@"water"]) {
            selectedTypeIndex = 4;
            keywordsColor = UIColorFromRGB(0xC5D5DF);
        }
        if([[feedInfo objectForKey:@"last_type"] isEqualToString:@"juice"]) {
            selectedTypeIndex = 5;
            keywordsColor = UIColorFromRGB(0xFFCA9B);
        }

    }
    
    if (IS_IPAD) {
        self.lblVolume.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:22.0f];
        self.lblDuration.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:22.0f];
        self.lblDate.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:22.0f];
        self.txtDate.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:22.0f];
        self.lblNotes.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:22.0f];
        self.txtNotes.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:22.0f];

        self.lblSide.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:22.0f];
        self.lblTimer.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:22.0f];
        self.lblLeftDurationValue.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:22.0f];
        self.lblRightDurationValue.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:22.0f];
        
        self.lblDurationUnitLeft.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.lblDurationUnitRight.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];

        self.lblPhoto.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:22.0f];
        self.btnStartLeft.titleLabel.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f];
        self.btnStartRight.titleLabel.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f];
        self.btnLeft.titleLabel.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f];
        self.btnRight.titleLabel.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f];

    }
    
    self.lblLeftDurationValue.hidden = YES;
    self.lblRightDurationValue.hidden = YES;
    self.btnStartLeft.alpha = 0.7;
    self.btnStartRight.alpha = 0.7;
    
    selectedSide = @"";
    selectedType = [[typeValueList objectAtIndex:selectedTypeIndex] lowercaseString];
    if ([selectedType isEqualToString:@"nursing"]) {
        
        if ([[feedInfo objectForKey:@"last_type"] isEqualToString:@"lboob"])
        {
            selectedType = @"lboob";
            selectedSide = @"Left";
            self.btnLeft.enabled = YES;
            self.btnStartLeft.enabled = YES;
            self.btnStartRight.enabled = NO;
            self.btnLeft.backgroundColor = UIColorFromRGB(0x827577);
            self.btnRight.backgroundColor = UIColorFromRGB(0xE0D3CB);
            
            self.btnStartLeft.alpha = 1.0;

        }
        else if ([[feedInfo objectForKey:@"last_type"] isEqualToString:@"rboob"])
        {
            selectedType = @"rboob";
            selectedSide = @"Right";
            self.btnRight.enabled = YES;
            self.btnStartRight.enabled = YES;
            self.btnStartLeft.enabled = NO;
            self.btnRight.backgroundColor = UIColorFromRGB(0x827577);
            self.btnLeft.backgroundColor = UIColorFromRGB(0xE0D3CB);

            self.btnStartRight.alpha = 1.0;
        }
        
    }
    
    self.btnPhoto1.imageView.contentMode = UIViewContentModeScaleAspectFill;
    self.btnPhoto2.imageView.contentMode = UIViewContentModeScaleAspectFill;
    self.btnPhoto3.imageView.contentMode = UIViewContentModeScaleAspectFill;
    self.btnPhoto1.hidden = YES;
    self.btnPhoto2.hidden = YES;
    self.btnPhoto3.hidden = YES;
    self.btnPhoto1.tag = 1;
    self.btnPhoto2.tag = 2;
    self.btnPhoto3.tag = 3;
    [self.btnPhoto1 addTarget:self action:@selector(showPhotoAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.btnPhoto2 addTarget:self action:@selector(showPhotoAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.btnPhoto3 addTarget:self action:@selector(showPhotoAction:) forControlEvents:UIControlEventTouchUpInside];

    [self doSetupForKeywordView];
    [self changeAction];
    [self getKeywordsList];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationWillEnterInBackGround) name:UIApplicationWillResignActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationWillEnterInForeground) name:UIApplicationWillEnterForegroundNotification object:nil];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:(BOOL)animated];
    [APP_DELEGATE removeFeedTimerAlertForActiveState];
    
    [appDelegate.tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"ui_action" action:@"button_press" label:@"Add New Feed" value:nil] build]];
    
    NSLog(@"self.data=%@", self.data);
    if([[self.data objectForKey:@"deleteCheck"] boolValue])
    {
        if ([[self.data objectForKey:@"imageTag"] integerValue] == 1) {
            self.btnPhoto1.hidden = YES;
            [self.data removeObjectForKey:@"imageTag"];
            videoUrl = nil;
        }else if ([[self.data objectForKey:@"imageTag"] integerValue] == 2) {
            self.btnPhoto2.hidden = YES;
            [self.data removeObjectForKey:@"imageTag"];
            videoUrl2 = nil;
        }else if ([[self.data objectForKey:@"imageTag"] integerValue] == 3) {
            self.btnPhoto3.hidden = YES;
            [self.data removeObjectForKey:@"imageTag"];
            videoUrl3 = nil;
        }
        
        [self.data removeObjectForKey:@"deleteCheck"];
        
    }
    
    isTimerStart = [shareObject objectForKey:[NSString stringWithFormat:@"feed_start_time_%d", (int)[shareObject integerForKey:@"baby_id"]]]?YES:NO;
    
    if(isTimerStart)
    {
        feedSeconds = [[NSDate date] timeIntervalSinceDate:(NSDate *)[shareObject objectForKey:[NSString stringWithFormat:@"feed_start_time_%d", (int)[shareObject integerForKey:@"baby_id"]]]];
        selectedDuration = (int)feedSeconds;
        
        if([[shareObject objectForKey:[NSString stringWithFormat:@"feed_type_%d", (int)[shareObject integerForKey:@"baby_id"]]] isEqualToString:@"rboob"])
        {
            selectedTypeIndex = 2;
            selectedType = @"rboob";
            selectedSide = @"Right";
            self.btnRight.backgroundColor = UIColorFromRGB(0x827577);
            self.btnLeft.backgroundColor = UIColorFromRGB(0xE0D3CB);
            self.lblRightDurationValue.hidden = NO;
            self.durationRightView.hidden = YES;
            
            self.btnStartRight.enabled = YES;
            self.btnStartLeft.enabled = NO;
            self.btnStartRight.backgroundColor = UIColorFromRGB(0x827577);
            self.btnStartLeft.backgroundColor = UIColorFromRGB(0xE0D3CB);
            [self.btnStartRight setTitle:LocalizedString(@"btn_pause", nil) forState:UIControlStateNormal];
            //self.lblRightDurationValue.text = [self convertSeconds:selectedDuration];
            self.lblRightDurationValue.text = [self getMinuteAndSecound:selectedDuration];

        }
        if([[shareObject objectForKey:[NSString stringWithFormat:@"feed_type_%d", (int)[shareObject integerForKey:@"baby_id"]]] isEqualToString:@"lboob"])
        {
            selectedTypeIndex = 2;
            selectedType = @"lboob";
            selectedSide = @"Left";
            self.btnLeft.backgroundColor = UIColorFromRGB(0x827577);
            self.btnRight.backgroundColor = UIColorFromRGB(0xE0D3CB);
            self.lblLeftDurationValue.hidden = NO;
            self.durationLeftView.hidden = YES;
            
            self.btnStartLeft.enabled = YES;
            self.btnStartRight.enabled = NO;
            self.btnStartLeft.backgroundColor = UIColorFromRGB(0x827577);
            self.btnStartRight.backgroundColor = UIColorFromRGB(0xE0D3CB);
            [self.btnStartLeft setTitle:LocalizedString(@"btn_pause", nil) forState:UIControlStateNormal];
            //self.lblLeftDurationValue.text = [self convertSeconds:selectedDuration];
            self.lblLeftDurationValue.text = [self getMinuteAndSecound:selectedDuration];

        }
        
        [self changeAction];
        [self.feedSelectionCollectionView reloadData];
        
        self.btnSave.enabled = NO;
        timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(timerActionFeed) userInfo:nil repeats:YES];
    }
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    if(timer) [timer invalidate];
    
    BOOL chkTimer = [shareObject boolForKey:@"isFeedTimerOn"];
    if (chkTimer) {
        [APP_DELEGATE setFeedTimerAlertForActiveState];
    }
}

- (void)applicationWillEnterInForeground
{
    isTimerStart = [shareObject objectForKey:[NSString stringWithFormat:@"feed_start_time_%d", (int)[shareObject integerForKey:@"baby_id"]]]?YES:NO;
    
    if(isTimerStart)
    {
        feedSeconds = [[NSDate date] timeIntervalSinceDate:(NSDate *)[shareObject objectForKey:[NSString stringWithFormat:@"feed_start_time_%d", (int)[shareObject integerForKey:@"baby_id"]]]];
        selectedDuration = (int)feedSeconds;
        
        if([[shareObject objectForKey:[NSString stringWithFormat:@"feed_type_%d", (int)[shareObject integerForKey:@"baby_id"]]] isEqualToString:@"rboob"])
        {
            [self.btnStartRight setTitle:LocalizedString(@"btn_pause", nil) forState:UIControlStateNormal];
            self.lblRightDurationValue.text = [self convertSeconds:selectedDuration];
        }
        if([[shareObject objectForKey:[NSString stringWithFormat:@"feed_type_%d", (int)[shareObject integerForKey:@"baby_id"]]] isEqualToString:@"lboob"])
        {
            [self.btnStartLeft setTitle:LocalizedString(@"btn_pause", nil) forState:UIControlStateNormal];
            self.lblLeftDurationValue.text = [self convertSeconds:selectedDuration];
        }
        
        self.btnSave.enabled = NO;
        timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(timerActionFeed) userInfo:nil repeats:YES];
    }
}

- (void)applicationWillEnterInBackGround
{
    NSLog(@"applicationWillEnterInBackGround");
    if(timer) [timer invalidate];
}

- (void)didReceiveMemoryWarning 
{
    [super didReceiveMemoryWarning];
}

- (BOOL)prefersStatusBarHidden
{
    return NO;
}

#pragma mark - Button Action
- (void)customBackButton
{
    if([self.navigationController.viewControllers count] > 1)
    {
        UIImage *imgBack = [UIImage imageNamed:@"nav_btn_back"];
        UIButton *btnBack = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnBack setImage:imgBack forState:UIControlStateNormal];
        btnBack.frame = CGRectMake(0, 0, imgBack.size.width, imgBack.size.height);
        [btnBack addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
        [btnBack setImageEdgeInsets:UIEdgeInsetsMake(0, -16, 0, 16)];
        UIBarButtonItem *backBarItem = [[UIBarButtonItem alloc] initWithCustomView:btnBack];
        self.navigationItem.leftBarButtonItem = backBarItem;
    }
}

- (void)backAction
{
    if(timer) [timer invalidate];
    [COMMON_HELPER clearTmpDirectory];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationWillEnterInBackGround) name:UIApplicationWillResignActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationWillEnterInForeground) name:UIApplicationWillEnterForegroundNotification object:nil];
    
    UIViewController *viewController = [self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count-2];
    viewController.data = [[NSMutableDictionary alloc] initWithDictionary:@{@"save_check":@(saveCheck)}];
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (void)changeAction
{
    if (selectedTypeIndex == 3) {
        volumeUnitList  = [[NSMutableArray alloc] initWithObjects: @"gm", @"oz", @"tbsp", @"tspn", @"cups", nil];
    }else {
        volumeUnitList  = [[NSMutableArray alloc] initWithObjects: @"ml", nil];
    }

    if(!editAmountCheck)
    {
        float value = 0;
        NSString *unit = [unitInfo objectForKey:@"volume"] ? [unitInfo objectForKey:@"volume"] : @"";
        if ([unit isEqualToString:@"time"]) {
            unit = @"ml";
        }
        
        if ([selectedType isEqualToString:@"solids"]) {
            unit = [unitInfo objectForKey:@"serving"];
            if ([unit isEqualToString:@"time"]) {
                unit = @"gm";
            }
        }
        
        if(feedInfo.count > 0 && [feedInfo objectForKey:selectedType])
        {
            value = [[[feedInfo objectForKey:selectedType] objectForKey:@"value"] floatValue];
            unit = [[feedInfo objectForKey:selectedType] objectForKey:@"unit"];
            if ([unit isEqualToString:@"time"]) {
                if ([selectedType isEqualToString:@"solids"]) {
                unit = @"gm";
                }else {
                    unit = @"ml";
                }
            }
        }
        
        if(selectedTypeIndex == 3)
        {
            selectedVolume = [self convertUnit:value unit:unit newUnit:[unitInfo objectForKey:@"serving"]];
            selectedUnit = unit;
            selectedVolumeIndexes = [[NSString stringWithFormat:@"%.1f", selectedVolume] componentsSeparatedByString:@"."];
            [self.volumeValuePicker selectRow:value inComponent:0 animated:YES];
            if ([volumeUnitList containsObject:unit]) {
                [self.volumeUnitPicker selectRow:[volumeUnitList indexOfObject:unit] inComponent:0 animated:YES];
            }
            //self.txtVolume.text = [self convertUnit:[NSString stringWithFormat:@"%f %@", selectedVolume, [unitInfo objectForKey:@"serving"]] unit:[unitInfo objectForKey:@"serving"] spacing:YES];
        }
        else if (selectedTypeIndex != 2)
        {
            selectedVolume = [self convertUnit:value unit:unit newUnit:[unitInfo objectForKey:@"volume"]];
            selectedVolumeIndexes = [[NSString stringWithFormat:@"%.1f", selectedVolume] componentsSeparatedByString:@"."];
            [self.volumeValuePicker selectRow:value inComponent:0 animated:YES];
            if ([volumeUnitList containsObject:unit]) {
                [self.volumeUnitPicker selectRow:[volumeUnitList indexOfObject:unit] inComponent:0 animated:YES];
            }
            //self.txtVolume.text = [self convertUnit:[NSString stringWithFormat:@"%f %@", selectedVolume, [unitInfo objectForKey:@"volume"]] unit:[unitInfo objectForKey:@"volume"] spacing:YES];
        }
    
        NSLog(@"selectedVolume=%f", selectedVolume);
        NSLog(@"selectedVolumeIndexes=%@", selectedVolumeIndexes);
    }
    
    if(!editDateCheck)
    {
        selectedDateTime = [NSDate date];
        if([[shareObject objectForKey:@"language"] hasPrefix:@"zh"])
        {
            [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"zh_HK"]];
            [formatter setDateStyle:NSDateFormatterMediumStyle];
            [formatter setTimeStyle:NSDateFormatterShortStyle];
        }
        else
        {
            [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
            formatter.dateFormat = @"EE d MMM, yyyy hh:mm a";
        }
        self.txtDate.text = [NSString stringWithFormat:@"%@", [formatter stringFromDate:selectedDateTime]];
        
        NSLog(@"selectedDateTime=%@", selectedDateTime);
    }
    
    if((!editStartLeftTimerCheck && !editEndLeftTimerCheck) && [selectedType isEqualToString:@"lboob"])
    {
//        int value = 0;
//
//        if([feedInfo objectForKey:selectedType])
//        {
//            value = [[[feedInfo objectForKey:selectedType] objectForKey:@"duration"] intValue];
//            selectedDuration = value;
//            if ([[feedInfo valueForKey:@"unit"] isEqualToString:@"min"]) {
//                selectedDuration = (value*60);
//            }
//        }
//
//        selectedDurationIndexes = [NSArray arrayWithObjects:@(floor(selectedDuration/60)), @(selectedDuration%60), nil];
//        self.lblLeftDurationValue.text = [self convertDurationWithFullFormat:selectedDuration];
//        selectedStart = [[NSDate date] dateByAddingTimeInterval:-selectedDuration];
//
//        if([[shareObject objectForKey:@"language"] hasPrefix:@"zh"])
//        {
//            [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"zh_HK"]];
//            [formatter setDateStyle:NSDateFormatterMediumStyle];
//            [formatter setTimeStyle:NSDateFormatterShortStyle];
//        }
//        else
//        {
//            [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
//            formatter.dateFormat = @"EE d MMM, yyyy hh:mm a";
//        }
    }
    
    if((!editStartRightTimerCheck && !editEndRightTimerCheck) && [selectedType isEqualToString:@"rboob"])
    {
//        int value = 0;
//
//        if([feedInfo objectForKey:selectedType])
//        {
//            value = [[[feedInfo objectForKey:selectedType] objectForKey:@"duration"] intValue];
//            selectedDuration = value;
//            if ([[feedInfo valueForKey:@"unit"] isEqualToString:@"min"]) {
//                selectedDuration = (value*60);
//            }
//        }
//
//        selectedDurationIndexes = [NSArray arrayWithObjects:@(floor(selectedDuration/60)), @(selectedDuration%60), nil];
//        self.lblRightDurationValue.text = [self convertDurationWithFullFormat:selectedDuration];
//        selectedStart = [[NSDate date] dateByAddingTimeInterval:-selectedDuration];
//
//        if([[shareObject objectForKey:@"language"] hasPrefix:@"zh"])
//        {
//            [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"zh_HK"]];
//            [formatter setDateStyle:NSDateFormatterMediumStyle];
//            [formatter setTimeStyle:NSDateFormatterShortStyle];
//        }
//        else
//        {
//            [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
//            formatter.dateFormat = @"EE d MMM, yyyy hh:mm a";
//        }
    }
    
    if (!editLeftValueCheck && [selectedType isEqualToString:@"lboob"])
    {
        int value = 0;
        if(feedInfo.count > 0 && [feedInfo objectForKey:selectedType])
        {
            value = [[[feedInfo objectForKey:selectedType] objectForKey:@"duration"] intValue];
            [self.durationPickerLeft selectRow:value inComponent:0 animated:YES];
            selectedDurationValueLeft = value;
        }
    }
    
    if (!editRightValueCheck && [selectedType isEqualToString:@"rboob"])
    {
        int value = 0;
        if(feedInfo.count > 0 && [feedInfo objectForKey:selectedType])
        {
            value = [[[feedInfo objectForKey:selectedType] objectForKey:@"duration"] intValue];
            [self.durationPickerRight selectRow:value inComponent:0 animated:YES];
            selectedDurationValueRight = value;
        }
    }
    
}

- (void)showCameraAction
{
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    if ([[NSUserDefaults standardUserDefaults]boolForKey:@"is_Subscription_Enabled"]) {
        imagePicker.mediaTypes = [[NSArray alloc] initWithObjects: (NSString *) kUTTypeMovie, (NSString *) kUTTypeVideo, (NSString *) kUTTypeImage, nil];
        imagePicker.videoQuality = UIImagePickerControllerQualityTypeLow;
        imagePicker.videoMaximumDuration = 120;
    }else {
        imagePicker.mediaTypes = [[NSArray alloc] initWithObjects: (NSString *) kUTTypeImage, nil];
    }
    
    [self presentViewController:imagePicker animated:NO completion:nil];
}

- (void)showPhotoAlbumAction
{
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    if ([[NSUserDefaults standardUserDefaults]boolForKey:@"is_Subscription_Enabled"]) {
        imagePicker.mediaTypes = [[NSArray alloc] initWithObjects: (NSString *) kUTTypeMovie, (NSString *) kUTTypeVideo, (NSString *) kUTTypeImage, nil];
    }else {
        imagePicker.mediaTypes = [[NSArray alloc] initWithObjects: (NSString *) kUTTypeImage, nil];
    }

    [self presentViewController:imagePicker animated:NO completion:nil];
}

- (void)timerActionFeed
{
    feedSeconds++;
    
    if ([selectedType isEqualToString:@"lboob"])
    {
        self.lblLeftDurationValue.text = [self getMinuteAndSecound:feedSeconds];
    }
    else if ([selectedType isEqualToString:@"rboob"])
    {
        self.lblRightDurationValue.text = [self getMinuteAndSecound:feedSeconds];
    }
    
    if (feedSeconds%300 == 0) {
        [self vibrateDevice];
    }
    
}

- (void)showPhotoAction:(UIButton *)sender
{
    if (sender.hidden)
        return;
    
    UIImage *img;
    NSString *vStr;
    if (sender.tag == 1) {
        if (videoUrl && videoUrl.length>0) {
            vStr = videoUrl;
        }else {
            img = self.btnPhoto1.imageView.image;
        }
    }else if (sender.tag == 2) {
        if (videoUrl2 && videoUrl2.length>0) {
            vStr = videoUrl2;
        }else {
            img = self.btnPhoto2.imageView.image;
        }
    }else if (sender.tag == 3) {
        if (videoUrl3 && videoUrl3.length>0) {
            vStr = videoUrl3;
        }else {
            img = self.btnPhoto3.imageView.image;
        }
    }
    
    if (vStr && vStr.length>0) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"VideoViewController"];
        viewController.data = [[NSMutableDictionary alloc] initWithDictionary:@{@"viewController":self, @"video":vStr,@"imageTag":@(sender.tag)}];
        [self presentViewController:viewController animated:YES completion:nil];
    }
    else
    {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"PhotoViewController"];
        viewController.data = [[NSMutableDictionary alloc] initWithDictionary:@{@"viewController":self, @"image":img,@"imageTag":@(sender.tag)}];
        [self presentViewController:viewController animated:YES completion:nil];
    }
    
}

- (IBAction)takePhotoAction
{
    AVAuthorizationStatus status = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    if(status == AVAuthorizationStatusDenied || status == AVAuthorizationStatusRestricted)
    {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:LocalizedString(@"error_camera", nil) message:@"" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *btnOk = [UIAlertAction actionWithTitle:LocalizedString(@"btn_ok", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [alert dismissViewControllerAnimated:YES completion:nil];
        }];
        [alert addAction:btnOk];
        [self presentViewController:alert animated:YES completion:nil];
    }
    else
    {
        [self showCameraAction];
    }
}

- (IBAction)getPhotoAlbumAction
{
    PHAuthorizationStatus status = [PHPhotoLibrary authorizationStatus];
    if(status == PHAuthorizationStatusDenied || status == PHAuthorizationStatusRestricted)
    {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:LocalizedString(@"error_photo", nil) message:@"" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *btnOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            
            [alert dismissViewControllerAnimated:YES completion:nil];
        }];
        
        [alert addAction:btnOk];
        
        [self presentViewController:alert animated:YES completion:nil];
        
    }
    else
    {
        [self showPhotoAlbumAction];
    }
}

- (IBAction)selectSideAction:(UIButton *)sender
{
    if (sender.tag == 1) {
        
        if (editStartRightTimerCheck && isTimerStart) {
            [self showAlertWithTitle:@"" message:@"Please stop active timer first !" input:nil];
            return;
        }
        
        selectedSide = @"Left";
        selectedType = @"lboob";
        self.btnStartLeft.enabled = YES;
        self.btnStartRight.enabled = NO;
        self.btnLeft.backgroundColor = UIColorFromRGB(0x827577);
        self.btnRight.backgroundColor = UIColorFromRGB(0xE0D3CB);

        self.btnStartLeft.alpha = 1.0;
        self.btnStartRight.alpha = 0.7;
        
    }
    else if (sender.tag == 2) {
        
        if (editStartLeftTimerCheck || isTimerStart) {
            [self showAlertWithTitle:@"" message:@"Please stop active timer first !" input:nil];
            return;
        }
        
        selectedSide = @"Right";
        selectedType = @"rboob";
        self.btnStartRight.enabled = YES;
        self.btnStartLeft.enabled = NO;
        self.btnRight.backgroundColor = UIColorFromRGB(0x827577);
        self.btnLeft.backgroundColor = UIColorFromRGB(0xE0D3CB);

        self.btnStartRight.alpha = 1.0;
        self.btnStartLeft.alpha = 0.7;
    }
    
    [self changeAction];
}

- (IBAction)selectTimerStartAction:(UIButton *)sender
{
    if (!([selectedSide isEqualToString:@"Left"] || [selectedSide isEqualToString:@"Right"])) {
        NSLog(@"Please selecte side first");
        return;
    }
    
    if (sender.tag == 1)
    {
        selectedType = @"lboob";
        self.btnStartLeft.backgroundColor = UIColorFromRGB(0x827577);
        self.btnStartRight.backgroundColor = UIColorFromRGB(0xE0D3CB);
        
         if([self.btnStartLeft.titleLabel.text isEqualToString:LocalizedString(@"btn_pause", nil)]) //Pause
        {
            int diff = [[NSDate date] timeIntervalSinceDate:(NSDate *)[shareObject objectForKey:[NSString stringWithFormat:@"feed_start_time_%d", (int)[shareObject integerForKey:@"baby_id"]]]];

            NSDate *startTime = [shareObject objectForKey:[NSString stringWithFormat:@"feed_start_time_%d", (int)[shareObject integerForKey:@"baby_id"]]];
            startTime = [startTime dateByAddingTimeInterval:diff];
            
            [shareObject removeObjectForKey:[NSString stringWithFormat:@"feed_start_time_%d", (int)[shareObject integerForKey:@"baby_id"]]];
            [shareObject removeObjectForKey:[NSString stringWithFormat:@"feed_type_%d", (int)[shareObject integerForKey:@"baby_id"]]];
            [shareObject removeObjectForKey:@"isFeedTimerOn"];
            
            selectedDuration = ceil(diff/60);
            feedSeconds = diff;
            selectedDuration = (selectedDuration == 0)?1:selectedDuration;
            self.lblLeftDurationValue.text = [self convertDurationWithFullFormat:diff];//[self convertDuration:selectedDuration fullFormatCheck:NO];
            selectedDurationIndexes = [self convertDuration:selectedDuration];
            
            selectedDurationValueLeft = diff/60;
            if (diff%60 > 0) {
                selectedDurationValueLeft += 1;
            }
            
            self.lblLeftDurationValue.hidden = NO;
            self.durationLeftView.hidden = YES;
            //feedSeconds = 0;
            isTimerStart = NO;
            editStartLeftTimerCheck = NO;
            editEndLeftTimerCheck = YES;
            self.btnSave.enabled = YES;
            [timer invalidate];
            [self.btnStartLeft setTitle:LocalizedString(@"btn_start", nil) forState:UIControlStateNormal];
            
        }
        else //start
        {
            feedSeconds = 0;
            
            if (self.lblLeftDurationValue.text.length>0) {
                NSString *val = self.lblLeftDurationValue.text;
                if ([self.lblLeftDurationValue.text containsString:@"seconds"]) {
                    val = [val stringByReplacingOccurrencesOfString:@"seconds" withString:@""];
                    feedSeconds = [val intValue];
                }
                else if([self.lblLeftDurationValue.text containsString:@"min"] || [self.lblLeftDurationValue.text containsString:@"mins"])
                {
                    val = [val stringByReplacingOccurrencesOfString:@"mins" withString:@""];
                    val = [val stringByReplacingOccurrencesOfString:@"min" withString:@""];
                    feedSeconds = ([val intValue]*60);
                }
            }
            
            editStartLeftTimerCheck = YES;
            editEndLeftTimerCheck = NO;
            isTimerStart = YES;
            self.btnSave.enabled = NO;
            
            if (feedSeconds>0) {
                NSDate *startTime = [NSDate date];
                startTime = [startTime dateByAddingTimeInterval:-feedSeconds];
                [shareObject setObject:startTime forKey:[NSString stringWithFormat:@"feed_start_time_%d", (int)[shareObject integerForKey:@"baby_id"]]];
            }
            else {
                [shareObject setObject:[NSDate date] forKey:[NSString stringWithFormat:@"feed_start_time_%d", (int)[shareObject integerForKey:@"baby_id"]]];
            }
            
            [shareObject setObject:selectedType forKey:[NSString stringWithFormat:@"feed_type_%d", (int)[shareObject integerForKey:@"baby_id"]]];
            [shareObject setBool:true forKey:@"isFeedTimerOn"];
            
            self.lblLeftDurationValue.text = [self convertSeconds:feedSeconds];
            selectedDuration = feedSeconds;
            self.lblLeftDurationValue.hidden = NO;
            self.durationLeftView.hidden = YES;
            
            [self.btnStartLeft setTitle:LocalizedString(@"btn_pause", nil) forState:UIControlStateNormal];
            timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(timerActionFeed) userInfo:nil repeats:YES];
        }

    }
    else if (sender.tag == 2)
    {
        selectedType = @"rboob";
        self.btnStartRight.backgroundColor = UIColorFromRGB(0x827577);
        self.btnStartLeft.backgroundColor = UIColorFromRGB(0xE0D3CB);

        if([self.btnStartRight.titleLabel.text isEqualToString:LocalizedString(@"btn_pause", nil)]) //resume
        {
            int diff = [[NSDate date] timeIntervalSinceDate:(NSDate *)[shareObject objectForKey:[NSString stringWithFormat:@"feed_start_time_%d", (int)[shareObject integerForKey:@"baby_id"]]]];

            NSDate *startTime = [shareObject objectForKey:[NSString stringWithFormat:@"feed_start_time_%d", (int)[shareObject integerForKey:@"baby_id"]]];
            startTime = [startTime dateByAddingTimeInterval:diff];
            
            [shareObject removeObjectForKey:[NSString stringWithFormat:@"feed_start_time_%d", (int)[shareObject integerForKey:@"baby_id"]]];
            [shareObject removeObjectForKey:[NSString stringWithFormat:@"feed_type_%d", (int)[shareObject integerForKey:@"baby_id"]]];
            [shareObject removeObjectForKey:@"isFeedTimerOn"];
            
            selectedDuration = ceil(diff/60);
            selectedDuration = (selectedDuration == 0)?1:selectedDuration;
            self.lblRightDurationValue.text = [self convertDurationWithFullFormat:diff];//[self convertDuration:selectedDuration fullFormatCheck:NO];
            selectedDurationIndexes = [self convertDuration:selectedDuration];
            
            selectedDurationValueRight = diff/60;
            if (diff%60 > 0) {
                selectedDurationValueRight += 1;
            }
            
            self.lblRightDurationValue.hidden = NO;
            self.durationRightView.hidden = YES;
            feedSeconds = 0;
            isTimerStart = NO;
            editStartRightTimerCheck = NO;
            editEndRightTimerCheck = YES;
            self.btnSave.enabled = YES;
            [timer invalidate];
            [self.btnStartRight setTitle:LocalizedString(@"btn_start", nil) forState:UIControlStateNormal];

        }
        else
        {
            feedSeconds = 0;
            
            if (self.lblRightDurationValue.text.length>0) {
                NSString *val = self.lblRightDurationValue.text;
                if ([val containsString:@"seconds"])
                {
                    val = [val stringByReplacingOccurrencesOfString:@"seconds" withString:@""];
                    feedSeconds = [val intValue];
                }
                else if([val containsString:@"min"] || [val containsString:@"mins"])
                {
                    val = [val stringByReplacingOccurrencesOfString:@"mins" withString:@""];
                    val = [val stringByReplacingOccurrencesOfString:@"min" withString:@""];
                    feedSeconds = ([val intValue]*60);
                }
            }

            editStartRightTimerCheck = YES;
            editEndRightTimerCheck = NO;
            isTimerStart = YES;
            self.btnSave.enabled = NO;
            
            if (feedSeconds>0) {
                NSDate *startTime = [NSDate date];
                startTime = [startTime dateByAddingTimeInterval:-feedSeconds];
                [shareObject setObject:startTime forKey:[NSString stringWithFormat:@"feed_start_time_%d", (int)[shareObject integerForKey:@"baby_id"]]];
            }
            else {
                [shareObject setObject:[NSDate date] forKey:[NSString stringWithFormat:@"feed_start_time_%d", (int)[shareObject integerForKey:@"baby_id"]]];
            }
            
            [shareObject setObject:selectedType forKey:[NSString stringWithFormat:@"feed_type_%d", (int)[shareObject integerForKey:@"baby_id"]]];
            [shareObject setBool:true forKey:@"isFeedTimerOn"];
            
            self.lblRightDurationValue.text = [self convertSeconds:feedSeconds];
            selectedDuration = feedSeconds;
            self.lblRightDurationValue.hidden = NO;
            self.durationRightView.hidden = YES;
            
            [self.btnStartRight setTitle:LocalizedString(@"btn_pause", nil) forState:UIControlStateNormal];
            timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(timerActionFeed) userInfo:nil repeats:YES];
            
        }
    }
    
    [shareObject synchronize];
    [self.tableView reloadData];
    
}

//- (IBAction)selectTimerStopAction:(id)sender
//{
//    editEndDateCheck = YES;
//
//    self.txtDurationLeft.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:15.0f];
//    int diff = [[NSDate date] timeIntervalSinceDate:(NSDate *)[shareObject objectForKey:[NSString stringWithFormat:@"feed_start_time_%d", (int)[shareObject integerForKey:@"baby_id"]]]];
//    NSLog(@"diff=%d", diff);
//    selectedDuration = ceil(diff/60);
//    NSLog(@"selectedDuration=%d", selectedDuration);
//    selectedDuration = (selectedDuration == 0)?1:selectedDuration;
//    self.txtDurationLeft.text = [self convertDuration:selectedDuration fullFormatCheck:YES];
//    selectedDurationIndexes = [self convertDuration:selectedDuration];
//
//    selectedStart = [[NSDate date] dateByAddingTimeInterval:-selectedDuration*60];
//    if([[shareObject objectForKey:@"language"] hasPrefix:@"zh"])
//    {
//        [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"zh_HK"]];
//        [formatter setDateStyle:NSDateFormatterMediumStyle];
//        [formatter setTimeStyle:NSDateFormatterShortStyle];
//    }
//    else
//    {
//        [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
//        formatter.dateFormat = @"EE d MMM, yyyy hh:mm a";
//    }
//
//    isTimerStart = NO;
//    [[NSUserDefaults standardUserDefaults] setBool:false forKey:@"isFeedTimerOn"];
//
//    self.btnSave.enabled = YES;
//    [self.btnStartLeft setTitle:LocalizedString(@"btn_start", nil) forState:UIControlStateNormal];
//    [timer invalidate];
//
//    [shareObject removeObjectForKey:[NSString stringWithFormat:@"feed_start_time_%d", (int)[shareObject integerForKey:@"baby_id"]]];
//    [shareObject removeObjectForKey:[NSString stringWithFormat:@"feed_pause_time_%d", (int)[shareObject integerForKey:@"baby_id"]]];
//    [shareObject removeObjectForKey:[NSString stringWithFormat:@"feed_type_%d", (int)[shareObject integerForKey:@"baby_id"]]];
//    [shareObject synchronize];
//
//    [self.tableView reloadData];
//}

- (IBAction)nextAction:(id)sender
{
    if (selectedTypeIndex == 2) {
        
        if (![selectedSide isEqualToString:@"Left"] && ![selectedSide isEqualToString:@"Right"]) {
            [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:@"Please select side !" input:nil];
            return;
        }
        
        if (!selectedDurationValueLeft && !selectedDurationValueRight ) {
            [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:@"No data has been entered !" input:nil];
            return;
        }
        
    }
    else if (!selectedVolume) {
        [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:@"No data has been entered !" input:nil];
        return;
    }
    
    [SVProgressHUD show];
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setValue:@([shareObject integerForKey:@"user_id"]) forKey:@"user_id"];
    [parameters setValue:@([shareObject integerForKey:@"baby_id"]) forKey:@"baby_id"];
    [parameters setValue:@"feed" forKey:@"name"];
    
    if (selectedTypeIndex == 2) {
        
        if (selectedDurationValueLeft && !selectedDurationValueRight) {
            
            [parameters setValue:@"lboob" forKey:@"type"];
            [parameters setValue:[NSString stringWithFormat:@"%d", selectedDurationValueLeft] forKey:@"value"];
            [parameters setValue:@"min" forKey:@"unit"];
            [parameters setValue:[NSString stringWithFormat:@"%d", selectedDurationValueLeft] forKey:@"duration"];
            [parameters setValue:[self formatDateTime:selectedDateTime] forKey:@"time"];
            //[parameters setValue:[[NSString stringWithFormat:@"%@", selectedDateTime] substringWithRange:NSMakeRange(0, 19)] forKey:@"time"];
            
        }
        else if (selectedDurationValueRight && !selectedDurationValueLeft) {
            
            [parameters setValue:@"rboob" forKey:@"type"];
            [parameters setValue:[NSString stringWithFormat:@"%d", selectedDurationValueRight] forKey:@"value"];
            [parameters setValue:@"min" forKey:@"unit"];
            [parameters setValue:[NSString stringWithFormat:@"%d", selectedDurationValueRight] forKey:@"duration"];
            [parameters setValue:[self formatDateTime:selectedDateTime] forKey:@"time"];
            //[parameters setValue:[[NSString stringWithFormat:@"%@", selectedDateTime] substringWithRange:NSMakeRange(0, 19)] forKey:@"time"];

        }
        else if (selectedDurationValueRight && selectedDurationValueLeft) {
            
            NSMutableDictionary *dictL = [NSMutableDictionary new];
            [dictL setValue:[NSString stringWithFormat:@"%d", selectedDurationValueLeft] forKey:@"value"];
            [dictL setValue:[NSString stringWithFormat:@"%d", selectedDurationValueLeft] forKey:@"duration"];
            [dictL setValue:@"min" forKey:@"unit"];
            [dictL setValue:[self formatDateTime:selectedDateTime] forKey:@"time"];
            //[dictL setValue:[[NSString stringWithFormat:@"%@", selectedDateTime] substringWithRange:NSMakeRange(0, 19)] forKey:@"time"];
            
            NSMutableDictionary *dictR = [NSMutableDictionary new];
            [dictR setValue:[NSString stringWithFormat:@"%d", selectedDurationValueRight] forKey:@"value"];
            [dictR setValue:[NSString stringWithFormat:@"%d", selectedDurationValueRight] forKey:@"duration"];
            [dictR setValue:@"min" forKey:@"unit"];
            [dictR setValue:[self formatDateTime:selectedDateTime] forKey:@"time"];
            //[dictR setValue:[[NSString stringWithFormat:@"%@", selectedDateTime] substringWithRange:NSMakeRange(0, 19)] forKey:@"time"];
            
            NSMutableDictionary *dict = [NSMutableDictionary new];
            [dict setObject:dictL forKey:@"lboob"];
            [dict setObject:dictR forKey:@"rboob"];
            
            NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict
                                                               options:NSJSONWritingPrettyPrinted
                                                                 error:nil];
            NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
            jsonString = [jsonString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            jsonString = [jsonString stringByReplacingOccurrencesOfString:@"\n" withString:@""];
            jsonString = [jsonString stringByReplacingOccurrencesOfString:@"\\\"" withString:@"\""];
            [parameters setValue:@"lboob+rboob" forKey:@"type"];
            [parameters setValue:jsonString forKey:@"nursingData"];

        }
    }
    else if (selectedTypeIndex == 3){
        [parameters setValue:selectedType forKey:@"type"];
        //[parameters setValue:[NSString stringWithFormat:@"%f", [self convertUnit:selectedVolume unit:[unitInfo objectForKey:@"serving"] newUnit:@"gm"]] forKey:@"value"];
        //[parameters setValue:@"gm" forKey:@"unit"];
        
        [parameters setValue:@(selectedVolume) forKey:@"value"];
        [parameters setValue:selectedUnit forKey:@"unit"];
        [parameters setValue:[NSString stringWithFormat:@"%d", selectedDuration] forKey:@"duration"];
        [parameters setValue:[self formatDateTime:selectedDateTime] forKey:@"time"];
        //[parameters setValue:[[NSString stringWithFormat:@"%@", selectedDateTime] substringWithRange:NSMakeRange(0, 19)] forKey:@"time"];
    }
    else {
        [parameters setValue:selectedType forKey:@"type"];
        [parameters setValue:[NSString stringWithFormat:@"%f", [self convertUnit:selectedVolume unit:[unitInfo objectForKey:@"volume"] newUnit:@"ml"]] forKey:@"value"];
        [parameters setValue:@"ml" forKey:@"unit"];
        [parameters setValue:[NSString stringWithFormat:@"%d", selectedDuration] forKey:@"duration"];
        [parameters setValue:[self formatDateTime:selectedDateTime] forKey:@"time"];
        //[parameters setValue:[[NSString stringWithFormat:@"%@", selectedDateTime] substringWithRange:NSMakeRange(0, 19)] forKey:@"time"];
    }
    
    [parameters setValue:self.txtNotes.text forKey:@"notes"];
    
    if(!self.btnPhoto1.hidden || !self.btnPhoto2.hidden || !self.btnPhoto3.hidden)
    {
        [[WebServiceManager sharedManager] initWithBaseURL:@"api/addBabyActivity" parameters:parameters formData:^(id<AFMultipartFormData> formData) {
            
            if (!self.btnPhoto1.hidden) {
                if (videoUrl && videoUrl.length>0) {
                    [formData appendPartWithFileURL:[NSURL URLWithString:videoUrl] name:@"video" fileName:@"video.mp4" mimeType:@"video" error: nil];
                }else {
                    [formData appendPartWithFileData:UIImageJPEGRepresentation((UIImage*)self.btnPhoto1.imageView.image, 0.8f) name:@"image" fileName:@"image.jpg" mimeType:@"image/jpeg"];
                }
            }
            
            if (!self.btnPhoto2.hidden) {
                if (videoUrl2 && videoUrl2.length>0) {
                    [formData appendPartWithFileURL:[NSURL URLWithString:videoUrl2] name:@"video2" fileName:@"video2.mp4" mimeType:@"video" error: nil];
                }else {
                    [formData appendPartWithFileData:UIImageJPEGRepresentation((UIImage*)self.btnPhoto2.imageView.image, 0.8f) name:@"image2" fileName:@"image2.jpg" mimeType:@"image/jpeg"];
                }
            }
            
            if (!self.btnPhoto3.hidden) {
                if (videoUrl3 && videoUrl3.length>0) {
                    [formData appendPartWithFileURL:[NSURL URLWithString:videoUrl3] name:@"video3" fileName:@"video3.mp4" mimeType:@"video" error: nil];
                }else {
                    [formData appendPartWithFileData:UIImageJPEGRepresentation((UIImage*)self.btnPhoto3.imageView.image, 0.8f) name:@"image3" fileName:@"image3.jpg" mimeType:@"image/jpeg"];
                }
            }
            
        } success:^(id result) {
            
            NSLog(@"result=%@", result);
            if([[result objectForKey:@"success"] boolValue])
            {
                NSMutableArray *babyList = (NSMutableArray *)[NSKeyedUnarchiver unarchiveObjectWithData:[shareObject objectForKey:@"baby_list"]];
                [[babyList objectAtIndex:[shareObject integerForKey:@"baby_index"]] setObject:[result valueForKey:@"info"] forKey:@"feed_info"];
                [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:babyList] forKey:@"baby_list"];
                [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[result valueForKey:@"info"]] forKey:@"feed_info"];
                [shareObject synchronize];
                
                saveCheck = YES;
                appDelegate.updateTimelineCheck = appDelegate.updateActivitiesCheck = appDelegate.updateActivityCheck = YES;
                [self backAction];
            }
            else
            {
                [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:[result objectForKey:@"message"] input:nil];
            }
            
        } failure:^(NSError *error) {
            [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:LocalizedString(@"error_unknown", nil) input:nil];
        }];
    }
    else
    {
        [[WebServiceManager sharedManager] initWithBaseURL:@"api/addBabyActivity" parameters:parameters success:^(id result) {
            
            NSLog(@"result=%@", result);
            if([[result objectForKey:@"success"] boolValue])
            {
                NSMutableArray *babyList = (NSMutableArray *)[NSKeyedUnarchiver unarchiveObjectWithData:[shareObject objectForKey:@"baby_list"]];
                [[babyList objectAtIndex:[shareObject integerForKey:@"baby_index"]] setObject:[result valueForKey:@"info"] forKey:@"feed_info"];
                [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:babyList] forKey:@"baby_list"];
                [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[result valueForKey:@"info"]] forKey:@"feed_info"];
                [shareObject synchronize];
                
                saveCheck = YES;
                appDelegate.updateTimelineCheck = appDelegate.updateActivitiesCheck = appDelegate.updateActivityCheck = YES;
                [self backAction];
            }
            else
            {
                [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:[result objectForKey:@"message"] input:nil];
            }
            
        } failure:^(NSError *error) {
            [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:LocalizedString(@"error_unknown", nil) input:nil];
        }];
    }
}

-(IBAction)didSelectAddPhoto:(UIButton *)sender
{
    if (self.btnPhoto1.hidden || self.btnPhoto2.hidden || self.btnPhoto3.hidden)
    {
        UIAlertController *alertController=[UIAlertController alertControllerWithTitle:LocalizedString(@"title_selectImage", nil) message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        
        UIAlertAction *takePhoto=[UIAlertAction actionWithTitle:LocalizedString(@"title_camera", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self takePhotoAction];
        }];
        UIAlertAction *choosePhoto=[UIAlertAction actionWithTitle:LocalizedString(@"title_camera_roll", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self getPhotoAlbumAction];
        }];
        UIAlertAction *actionCancel=[UIAlertAction actionWithTitle:LocalizedString(@"title_cancel", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            [alertController dismissViewControllerAnimated:YES completion:nil];
        }];

        [alertController addAction:takePhoto];
        [alertController addAction:choosePhoto];
        [alertController addAction:actionCancel];
        
        if (IS_IPAD) {
            UIPopoverPresentationController *popPresenter = [alertController popoverPresentationController];
            popPresenter.sourceView = sender;
            popPresenter.sourceRect = CGRectMake(sender.bounds.origin.x, sender.bounds.size.height/2, 0, 0);
            //popPresenter.presentedViewController.preferredContentSize =  CGSizeMake(520, 400);
        }
        [self presentViewController:alertController animated:YES completion:nil];
    }
    else
    {
        [self showAlertWithTitle:@"" message:@"You can not add more than 3 photos. " input:nil];
    }
    
}

#pragma mark - Table View Methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)aTableView
{
    return 2;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if(section == 0) return LocalizedString(@"header_type", nil);
    if(section == 1) return LocalizedString(@"header_details", nil);
    
    return nil;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UILabel *txtHeader;
    UIView *headerView;
    if (IS_IPAD) {
        txtHeader = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, tableView.bounds.size.width-20, 40)];
        txtHeader.font = [UIFont fontWithName:@"SFUIDisplay-Bold" size:18.0f];
        headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 40)];
    }
    else{
        txtHeader = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, tableView.bounds.size.width-20, 30)];
        txtHeader.font = [UIFont fontWithName:@"SFUIDisplay-Bold" size:12.0f];
        headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 30)];
    }
    
    txtHeader.textColor = UIColorFromRGB(0x8D8082);
    txtHeader.text = [self tableView:tableView titleForHeaderInSection:section];
    headerView.backgroundColor = UIColorFromRGB(0xEEE8E4);
    [headerView addSubview:txtHeader];
    return headerView;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (IS_IPAD) {
        return 40;
    }
    return 30;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        if (IS_IPAD) {
            return 104;
        }
        return 84;
    }
    if(selectedTypeIndex == 2 )
    {
        if(indexPath.section == 1 && (indexPath.row == 3)) return 0;
    }
    else
    {
        if(indexPath.section == 1 && (indexPath.row == 1 || indexPath.row == 2)) return 0;
    }
    
    return UITableViewAutomaticDimension;
}
- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        if (IS_IPAD) {
            return 104;
        }
        return 84;
    }
    
    if(selectedTypeIndex == 2 )
    {
        if(indexPath.section == 1 && (indexPath.row == 3)) return 0;
    }
    else
    {
        if(indexPath.section == 1 && (indexPath.row == 1 || indexPath.row == 2)) return 0;
    }
    
    return UITableViewAutomaticDimension;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.section == 1) {
        
        //ActionSheetMultipleStringPicker *picker;
        //NSArray *stringList = [[NSArray alloc] init];
        //NSArray *initialList = [[NSArray alloc] init];
        
        if (indexPath.row == 0)
        {
            NSDate *minimumDate = [[NSDate date] dateByAddingTimeInterval:-365*24*60*60];
            NSDate *maximumDate = [NSDate date];
            [ActionSheetDatePicker showPickerWithTitle:@"" datePickerMode:UIDatePickerModeDateAndTime selectedDate:selectedDateTime minimumDate:minimumDate maximumDate:maximumDate doneBlock:^(ActionSheetDatePicker *picker, id selectedDate, id origin) {
                
                editDateCheck = YES;
                selectedDateTime = selectedDate;
                NSLog(@"selectedDateTime=%@", selectedDateTime);
                if([[shareObject objectForKey:@"language"] hasPrefix:@"zh"])
                {
                    [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"zh_HK"]];
                    [formatter setDateStyle:NSDateFormatterMediumStyle];
                    [formatter setTimeStyle:NSDateFormatterShortStyle];
                }
                else
                {
                    [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
                    formatter.dateFormat = @"EE d MMM, yyyy hh:mm a";
                }
                self.txtDate.text = [formatter stringFromDate:selectedDateTime];
                [self.tableView reloadData];
                
            } cancelBlock:^(ActionSheetDatePicker *picker) {
                
            } origin:self.view];
        }
//        else
//        {
//            if (selectedTypeIndex == 4) {
//
//            }
//            else if (indexPath.row == 3){
//
//                if(selectedTypeIndex == 5)
//                {
//                    if([[unitInfo objectForKey:@"serving"] isEqualToString:@"gm"])
//                    {
//                        stringList = [NSArray arrayWithObjects:no1000List, nil];
//                        initialList = [NSArray arrayWithObjects:@([selectedVolumeIndexes[0] intValue]), nil];
//                    }
//                    else
//                    {
//                        stringList = [NSArray arrayWithObjects:no100List, digitList, nil];
//                        initialList = [NSArray arrayWithObjects:@([selectedVolumeIndexes[0] intValue]), @([selectedVolumeIndexes[1] intValue]), nil];
//                    }
//
//                    picker = [ActionSheetMultipleStringPicker showPickerWithTitle:@"" rows:stringList initialSelection:initialList doneBlock:^(ActionSheetMultipleStringPicker *picker, NSArray *selectedIndexes, id selectedValues) {
//
//                        editAmountCheck = YES;
//                        selectedVolume = [[selectedValues componentsJoinedByString:@"."] floatValue];
//                        selectedVolumeIndexes = selectedIndexes;
//                        self.txtVolume.text = [self convertUnit:[NSString stringWithFormat:@"%f %@", selectedVolume, [unitInfo objectForKey:@"serving"]] unit:[unitInfo objectForKey:@"serving"] spacing:YES];
//
//                        /*// convert to oz
//                         selectedTotal = [self convertUnit: [[selectedValues componentsJoinedByString:@"."] floatValue] unit:[unitInfo objectForKey:@"volume"] newUnit:@"oz"];
//                         selectedStartAt = selectedTotal+selectedEndAt;*/
//                        NSLog(@"selectedVolume=%f", selectedVolume);
//                        [self.tableView reloadData];
//
//                    } cancelBlock:^(ActionSheetMultipleStringPicker *picker) {
//
//                    } origin:self.view];
//
////                    if([[unitInfo objectForKey:@"serving"] isEqualToString:@"gm"])
////                    {
////                        UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(picker.viewSize.width/2, 0, picker.viewSize.width/2, 36)];
////                        label1.text = @"       gm";
////                        label1.textAlignment = NSTextAlignmentLeft;
////                        label1.font = [UIFont systemFontOfSize:20.0f];
////                        label1.backgroundColor = [[UIColor redColor] colorWithAlphaComponent:0.f];
////                        [[picker.pickerView.subviews objectAtIndex:1] addSubview:label1];
////                    }
////                    if([[unitInfo objectForKey:@"serving"] isEqualToString:@"oz"])
////                    {
////                        UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, (picker.viewSize.width/2), 36)];
////                        label1.text = @".";
////                        label1.textAlignment = NSTextAlignmentRight;
////                        label1.font = [UIFont systemFontOfSize:20.0f];
////                        label1.backgroundColor = [[UIColor redColor] colorWithAlphaComponent:0.f];
////                        [[picker.pickerView.subviews objectAtIndex:1] addSubview:label1];
////
////                        UILabel *label2 = [[UILabel alloc] initWithFrame:CGRectMake(picker.viewSize.width-(picker.viewSize.width/2), 0, (picker.viewSize.width/2)-20, 36)];
////                        label2.text = @"oz";
////                        label2.textAlignment = NSTextAlignmentRight;
////                        label2.font = [UIFont systemFontOfSize:20.0f];
////                        label2.backgroundColor = [[UIColor greenColor] colorWithAlphaComponent:0.f];
////                        [[picker.pickerView.subviews objectAtIndex:1] addSubview:label2];
////                    }
//                }
//                else
//                {
//                    if([[unitInfo objectForKey:@"volume"] isEqualToString:@"ml"])
//                    {
//                        stringList = [NSArray arrayWithObjects:no1000List, nil];
//                        initialList = [NSArray arrayWithObjects:@([selectedVolumeIndexes[0] intValue]), nil];
//                    }
//                    else
//                    {
//                        stringList = [NSArray arrayWithObjects:no100List, digitList, nil];
//                        initialList = [NSArray arrayWithObjects:@([selectedVolumeIndexes[0] intValue]), @([selectedVolumeIndexes[1] intValue]), nil];
//                    }
//
//                    picker = [ActionSheetMultipleStringPicker showPickerWithTitle:@"" rows:stringList initialSelection:initialList doneBlock:^(ActionSheetMultipleStringPicker *picker, NSArray *selectedIndexes, id selectedValues) {
//
//                        editAmountCheck = YES;
//                        selectedVolume = [[selectedValues componentsJoinedByString:@"."] floatValue];
//                        NSLog(@"selectedVolume=%f", selectedVolume);
//                        selectedVolumeIndexes = selectedIndexes;
//                        self.txtVolume.text = [self convertUnit:[NSString stringWithFormat:@"%f %@", selectedVolume, [unitInfo objectForKey:@"volume"]] unit:[unitInfo objectForKey:@"volume"] spacing:YES];
//
//                        /*// convert to oz
//                         selectedTotal = [self convertUnit: [[selectedValues componentsJoinedByString:@"."] floatValue] unit:[unitInfo objectForKey:@"volume"] newUnit:@"oz"];
//                         selectedStartAt = selectedTotal+selectedEndAt;*/
//                        NSLog(@"selectedVolume=%f", selectedVolume);
//
//                        [self.tableView reloadData];
//
//                    } cancelBlock:^(ActionSheetMultipleStringPicker *picker) {
//
//                    } origin:self.view];
//
////                    if([[unitInfo objectForKey:@"volume"] isEqualToString:@"ml"])
////                    {
////                        UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(picker.viewSize.width/2, 0, picker.viewSize.width/2, 36)];
////                        label1.text = @"       ml";
////                        label1.textAlignment = NSTextAlignmentLeft;
////                        label1.font = [UIFont systemFontOfSize:20.0f];
////                        label1.backgroundColor = [[UIColor redColor] colorWithAlphaComponent:0.f];
////                        [[picker.pickerView.subviews objectAtIndex:1] addSubview:label1];
////                    }
////                    if([[unitInfo objectForKey:@"volume"] isEqualToString:@"oz"])
////                    {
////                        UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, (picker.viewSize.width/2), 36)];
////                        label1.text = @".";
////                        label1.textAlignment = NSTextAlignmentRight;
////                        label1.font = [UIFont systemFontOfSize:20.0f];
////                        label1.backgroundColor = [[UIColor redColor] colorWithAlphaComponent:0.f];
////                        [[picker.pickerView.subviews objectAtIndex:1] addSubview:label1];
////
////                        UILabel *label2 = [[UILabel alloc] initWithFrame:CGRectMake(picker.viewSize.width-(picker.viewSize.width/2), 0, (picker.viewSize.width/2)-20, 36)];
////                        label2.text = @"oz";
////                        label2.textAlignment = NSTextAlignmentRight;
////                        label2.font = [UIFont systemFontOfSize:20.0f];
////                        label2.backgroundColor = [[UIColor greenColor] colorWithAlphaComponent:0.f];
////                        [[picker.pickerView.subviews objectAtIndex:1] addSubview:label2];
////                    }
//
//                }
//
//            }
//        }
        
    }
    
}


#pragma mark - Collection view Datasource and Delegate methods

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    ActivitySelectCollectionViewCell  *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ActivitySelectCollectionViewCell" forIndexPath:indexPath];
    
    [cell.imgActivityType setImage:[UIImage imageNamed:[typeInactiveImageList objectAtIndex:indexPath.row]]];
    
    cell.txtActivityType.text = [typeList objectAtIndex:indexPath.row];
    if (indexPath.row == selectedTypeIndex) {
        
        selectedType = [[typeValueList objectAtIndex:selectedTypeIndex] lowercaseString];
        if ([selectedType isEqualToString:@"nursing"] || [selectedType isEqualToString:@"lboob"] || [selectedType isEqualToString:@"rboob"]) {
            
            if ([[shareObject objectForKey:[NSString stringWithFormat:@"feed_type_%d", (int)[shareObject integerForKey:@"baby_id"]]] isEqualToString:@"lboob"])
            {
                selectedType = @"lboob";
                selectedSide = @"Left";
                self.btnLeft.enabled = YES;
                self.btnStartLeft.enabled = YES;
                self.btnStartRight.enabled = NO;
                
                self.btnLeft.backgroundColor = UIColorFromRGB(0x827577);
                self.btnRight.backgroundColor = UIColorFromRGB(0xE0D3CB);
                self.btnStartLeft.backgroundColor = UIColorFromRGB(0x827577);
                self.btnStartRight.backgroundColor = UIColorFromRGB(0xE0D3CB);
                

            }
            else if ([[shareObject objectForKey:[NSString stringWithFormat:@"feed_type_%d", (int)[shareObject integerForKey:@"baby_id"]]] isEqualToString:@"rboob"])
            {
                selectedType = @"rboob";
                selectedSide = @"Right";
                self.btnRight.enabled = YES;
                self.btnStartRight.enabled = YES;
                self.btnStartLeft.enabled = NO;
                
                self.btnRight.backgroundColor = UIColorFromRGB(0x827577);
                self.btnLeft.backgroundColor = UIColorFromRGB(0xE0D3CB);
                self.btnStartRight.backgroundColor = UIColorFromRGB(0x827577);
                self.btnStartLeft.backgroundColor = UIColorFromRGB(0xE0D3CB);
                
            }
            else if ([[feedInfo objectForKey:@"last_type"] isEqualToString:@"lboob"])
            {
                selectedType = @"lboob";
                selectedSide = @"Left";
                self.btnLeft.enabled = YES;
                self.btnStartLeft.enabled = YES;
                self.btnStartRight.enabled = NO;
                
                self.btnLeft.backgroundColor = UIColorFromRGB(0x827577);
                self.btnRight.backgroundColor = UIColorFromRGB(0xE0D3CB);
                self.btnStartLeft.backgroundColor = UIColorFromRGB(0x827577);
                self.btnStartRight.backgroundColor = UIColorFromRGB(0xE0D3CB);
                
            }
            else if ([[feedInfo objectForKey:@"last_type"] isEqualToString:@"rboob"])
            {
                selectedType = @"rboob";
                selectedSide = @"Right";
                self.btnRight.enabled = YES;
                self.btnStartRight.enabled = YES;
                self.btnStartLeft.enabled = NO;
                
                self.btnRight.backgroundColor = UIColorFromRGB(0x827577);
                self.btnLeft.backgroundColor = UIColorFromRGB(0xE0D3CB);
                self.btnStartRight.backgroundColor = UIColorFromRGB(0x827577);
                self.btnStartLeft.backgroundColor = UIColorFromRGB(0xE0D3CB);

                
            }
        }
        
        [cell.imgActivityType setImage:[UIImage imageNamed:[typeActiveImageList objectAtIndex:indexPath.row]]];
        [collectionView selectItemAtIndexPath:indexPath animated:YES scrollPosition:(UICollectionViewScrollPositionCenteredHorizontally)];
        
    }
    
    [cell layoutSubviews];
    return cell;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return typeValueList.count;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if (selectedTypeIndex == indexPath.row) {
        return;
    }
    
    ActivitySelectCollectionViewCell  *cell;

    if (isTimerStart) {
        cell = (ActivitySelectCollectionViewCell *)[collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:selectedTypeIndex inSection:0]];
        [cell.imgActivityType setImage:[UIImage imageNamed:[typeActiveImageList objectAtIndex:selectedTypeIndex]]];

        [cell.imgActivityType setImage:[UIImage imageNamed:[typeActiveImageList objectAtIndex:selectedTypeIndex]]];
        [collectionView selectItemAtIndexPath:[NSIndexPath indexPathForRow:selectedTypeIndex inSection:0] animated:YES scrollPosition:(UICollectionViewScrollPositionCenteredHorizontally)];
        [self showAlertWithTitle:@"" message:@"Please stop active timer first !" input:nil];
        return;
    }
    
    cell = (ActivitySelectCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
    [cell.imgActivityType setImage:[UIImage imageNamed:[typeActiveImageList objectAtIndex:indexPath.row]]];
    
    selectedTypeIndex = indexPath.row;
    selectedType = [[typeValueList objectAtIndex:indexPath.row] lowercaseString];
    
    if ([selectedType isEqualToString:@"nursing"] || [selectedType isEqualToString:@"lboob"] || [selectedType isEqualToString:@"rboob"]) {
        
        if ([[shareObject objectForKey:[NSString stringWithFormat:@"feed_type_%d", (int)[shareObject integerForKey:@"baby_id"]]] isEqualToString:@"lboob"])
        {
            selectedType = @"lboob";
            selectedSide = @"Left";
            self.btnLeft.enabled = YES;
            self.btnStartLeft.enabled = YES;
            self.btnStartRight.enabled = NO;
            
            self.btnLeft.backgroundColor = UIColorFromRGB(0x827577);
            self.btnRight.backgroundColor = UIColorFromRGB(0xE0D3CB);
            self.btnStartLeft.backgroundColor = UIColorFromRGB(0x827577);
            self.btnStartRight.backgroundColor = UIColorFromRGB(0xE0D3CB);

        }
        else if ([[shareObject objectForKey:[NSString stringWithFormat:@"feed_type_%d", (int)[shareObject integerForKey:@"baby_id"]]] isEqualToString:@"rboob"])
        {
            selectedType = @"rboob";
            selectedSide = @"Right";
            self.btnRight.enabled = YES;
            self.btnStartRight.enabled = YES;
            self.btnStartLeft.enabled = NO;

            self.btnRight.backgroundColor = UIColorFromRGB(0x827577);
            self.btnLeft.backgroundColor = UIColorFromRGB(0xE0D3CB);
            self.btnStartRight.backgroundColor = UIColorFromRGB(0x827577);
            self.btnStartLeft.backgroundColor = UIColorFromRGB(0xE0D3CB);

        }
        else if ([[feedInfo objectForKey:@"last_type"] isEqualToString:@"lboob"])
        {
            selectedType = @"lboob";
            selectedSide = @"Left";
            self.btnLeft.enabled = YES;
            self.btnStartLeft.enabled = YES;
            self.btnStartRight.enabled = NO;

            self.btnLeft.backgroundColor = UIColorFromRGB(0x827577);
            self.btnRight.backgroundColor = UIColorFromRGB(0xE0D3CB);
            self.btnStartLeft.backgroundColor = UIColorFromRGB(0x827577);
            self.btnStartRight.backgroundColor = UIColorFromRGB(0xE0D3CB);

        }
        else if ([[feedInfo objectForKey:@"last_type"] isEqualToString:@"rboob"])
        {
            selectedType = @"rboob";
            selectedSide = @"Right";
            self.btnRight.enabled = YES;
            self.btnStartRight.enabled = YES;
            self.btnStartLeft.enabled = NO;

            self.btnRight.backgroundColor = UIColorFromRGB(0x827577);
            self.btnLeft.backgroundColor = UIColorFromRGB(0xE0D3CB);
            self.btnStartRight.backgroundColor = UIColorFromRGB(0x827577);
            self.btnStartLeft.backgroundColor = UIColorFromRGB(0xE0D3CB);

        }
    }
    
    //remove up and down arrow
    if ([selectedType isEqualToString:@"solids"]){
        self.upArrow2.hidden = false;
        self.downArrow2.hidden = false;
    }
    else{
        self.upArrow2.hidden = true;
        self.downArrow2.hidden = true;
    }
    if(selectedTypeIndex == 0) {
        keywordsColor = UIColorFromRGB(0xF4EA9E);
    }
    if(selectedTypeIndex == 1) {
        keywordsColor = UIColorFromRGB(0xDACEDB);
    }
    if(selectedTypeIndex == 2) {
        keywordsColor = UIColorFromRGB(0xBDB5AF);
    }
    if(selectedTypeIndex == 3) {
        keywordsColor = UIColorFromRGB(0xC9DAAB);
    }
    if(selectedTypeIndex == 4) {
        keywordsColor = UIColorFromRGB(0xC5D5DF);
    }
    if(selectedTypeIndex == 5) {
        keywordsColor = UIColorFromRGB(0xFFCA9B);
    }

    if (selectedTypeIndex == 2) {
        [self.durationPickerLeft reloadAllComponents];
        [self.durationPickerRight reloadAllComponents];
    }else {
        [self.volumeValuePicker reloadAllComponents];
        [self.volumeUnitPicker reloadAllComponents];
    }
    
    NSString *str;
    if ([selectedType isEqualToString:@"lboob"] || [selectedType isEqualToString:@"rboob"]) {
        str = [keywordslistDict objectForKey:@"nursing"];
    }else {
        str = [keywordslistDict objectForKey:selectedType];
    }
    if (str.length>0) {
        keywordsArray = [[NSMutableArray alloc] initWithArray:[str componentsSeparatedByString:@","]];
    }else {
        keywordsArray = [[NSMutableArray alloc] init];
    }

    [self addKeywordLabel];
    [self changeAction];
    [self.tableView reloadData];
    
}

-(void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    ActivitySelectCollectionViewCell  *cell = (ActivitySelectCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
    [cell.imgActivityType setImage:[UIImage imageNamed:[typeInactiveImageList objectAtIndex:indexPath.row]]];
    
    [self.tableView reloadData];
    
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat w = self.feedSelectionCollectionView.frame.size.width/6;
    if (IS_IPAD) {
        return CGSizeMake(w, 104);
    }
    return CGSizeMake(w, 84);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

#pragma mark -
#pragma mark UIScrollViewDelegate Methods
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if([self.navigationController.visibleViewController isKindOfClass:[ActivityFeedAddViewController class]])
    {
        CGPoint translation = [scrollView.panGestureRecognizer translationInView:scrollView.superview];
        if(translation.y > 0)
        {
            [self.view endEditing:YES];
        }
    }
}

#pragma mark - UIImagePickerController Delegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    NSString *mediaType = info[UIImagePickerControllerMediaType];
    if ([mediaType isEqualToString:(NSString *)kUTTypeImage])
    {
        NSLog(@"Media is an image");
        // Handle selected image
        UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
        NSLog(@"image=%@", image);
        
        if (picker.sourceType == UIImagePickerControllerSourceTypeCamera) {
            UIImageWriteToSavedPhotosAlbum(image, self, @selector(savedPhotoImage:didFinishSavingWithError:contextInfo:), nil);
        }
        
        float scale = self.view.bounds.size.width*2/image.size.width;
        CGSize newSize = CGSizeMake(self.view.bounds.size.width*2, image.size.height*scale);
        if(image.size.width > image.size.height)
        {
            scale = self.view.bounds.size.width*2/image.size.height;
            newSize = CGSizeMake(image.size.width*scale, self.view.bounds.size.width*2);
        }
        UIGraphicsBeginImageContext(newSize);
        [image drawInRect:(CGRect){0, 0, newSize}];
        image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        if (self.btnPhoto1.hidden) {
            self.btnPhoto1.hidden = false;
            [self.btnPhoto1 setImage:image forState:UIControlStateNormal];
        }else if (self.btnPhoto2.hidden) {
            self.btnPhoto2.hidden = false;
            [self.btnPhoto2 setImage:image forState:UIControlStateNormal];
        }else if (self.btnPhoto3.hidden) {
            self.btnPhoto3.hidden = false;
            [self.btnPhoto3 setImage:image forState:UIControlStateNormal];
        }
        
        [self dismissViewControllerAnimated:NO completion:^{
        }];
    }
    else if ([mediaType isEqualToString:(NSString *)kUTTypeMovie])
    {
        BOOL ntwrkFlagStatus = [[NSUserDefaults standardUserDefaults] boolForKey:@"can_Upload_Using_Mobile_Network"];
        BOOL checkWifiStatus = [COMMON_HELPER checkforWifiNetwork];
        
        if ((checkWifiStatus==NO) && (ntwrkFlagStatus==NO)) {
            [self dismissViewControllerAnimated:NO completion:^{
            }];
            [self showAlertWithTitle:LocalizedString(@"mobile_data_usages_title", nil) message:LocalizedString(@"mobile_data_usages_message", nil) input:nil];
            return;
        }else {
            
            NSLog(@"Media is a video");
            NSURL *vUrl= (NSURL*)[info objectForKey:UIImagePickerControllerMediaURL];
            NSString *moviePath = [vUrl path];
            
            NSURL* uploadURL;
            if (self.btnPhoto1.hidden) {
                [[NSFileManager defaultManager] removeItemAtPath:[NSString stringWithFormat:@"%@video1.mov", NSTemporaryDirectory()] error:NULL];
                uploadURL = [NSURL fileURLWithPath:
                             [NSTemporaryDirectory() stringByAppendingPathComponent:@"video1.mov"]];
            }else if (self.btnPhoto2.hidden) {
                [[NSFileManager defaultManager] removeItemAtPath:[NSString stringWithFormat:@"%@video2.mov", NSTemporaryDirectory()] error:NULL];
                uploadURL = [NSURL fileURLWithPath:
                             [NSTemporaryDirectory() stringByAppendingPathComponent:@"video2.mov"]];
            }else if (self.btnPhoto3.hidden) {
                [[NSFileManager defaultManager] removeItemAtPath:[NSString stringWithFormat:@"%@video3.mov", NSTemporaryDirectory()] error:NULL];
                uploadURL = [NSURL fileURLWithPath:
                             [NSTemporaryDirectory() stringByAppendingPathComponent:@"video3.mov"]];
            }
            
            // Generate Thumbnail Image.
            UIImage *tImg = [COMMON_HELPER generateThumbnailImageFromVideoWithUrl:vUrl];
            
            [SVProgressHUD showWithStatus:LocalizedString(@"processing_video", nil)];
            [COMMON_HELPER compressVideo:vUrl outputURL:uploadURL handler:^(AVAssetExportSession *completion) {
                
                if (completion.status == AVAssetExportSessionStatusCompleted) {
                    NSData *newDataForUpload = [NSData dataWithContentsOfURL:uploadURL];
                    NSLog(@"Size of new Video after compression is (bytes):%lu",(unsigned long)[newDataForUpload length]);
                    
                    CGFloat sizeInMB = ([newDataForUpload length]/1000)/1000;
                    if (sizeInMB>15) {
                        [self showAlertWithTitleCallback:@"Large video size" message:@"Can not add this video." callback:^(UIAlertAction *action) {
                            [[NSFileManager defaultManager] removeItemAtPath:[NSString stringWithFormat:@"%@", uploadURL] error:NULL];
                        }];
                    }
                    else {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            if (self.btnPhoto1.hidden) {
                                self.btnPhoto1.hidden = false;
                                videoUrl = [uploadURL absoluteString];
                                [self.btnPhoto1 setImage:tImg forState:UIControlStateNormal];
                            }else if (self.btnPhoto2.hidden) {
                                self.btnPhoto2.hidden = false;
                                videoUrl2 = [uploadURL absoluteString];
                                [self.btnPhoto2 setImage:tImg forState:UIControlStateNormal];
                            }else if (self.btnPhoto3.hidden) {
                                self.btnPhoto3.hidden = false;
                                videoUrl3 = [uploadURL absoluteString];
                                [self.btnPhoto3 setImage:tImg forState:UIControlStateNormal];
                            }
                        });
                        
                    }
                }
                [SVProgressHUD dismiss];
                
            }];
            
            if (picker.sourceType == UIImagePickerControllerSourceTypeCamera) {
                if (UIVideoAtPathIsCompatibleWithSavedPhotosAlbum (moviePath)) {
                    UISaveVideoAtPathToSavedPhotosAlbum (moviePath, nil, nil, nil);
                }
            }
            
            [self dismissViewControllerAnimated:NO completion:^{
            }];
        }
    }
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Custom Methods RS
-(void)vibrateDevice {
    [COMMON_HELPER vibrateDevice];
}

- (void) savedPhotoImage:(UIImage *)image
  didFinishSavingWithError:(NSError *)error
               contextInfo:(void *)contextInfo
{
    NSLog(@"This image has been saved to your camera roll");
}

- (NSString *)convertDurationWithFullFormat:(int)diff
{
    BOOL fullFormatCheck = YES;
    NSString *duration = @"";
    
    int sec = ceil(diff % 60);
    int min = ceil((diff / 60) % 60);
    int hour = ceil(diff / 3600);
    if(hour > 24) hour = ceil(diff % (60*24) / 60);
    int day = ceil(diff / (60*24));
    
    if(day > 0) {
        duration = [NSString stringWithFormat:@"%@%d%@%@ ", duration, day, fullFormatCheck?@" ":@"", (day > 1)?LocalizedString(fullFormatCheck?@"days":@"d", nil):LocalizedString(fullFormatCheck?@"day":@"d", nil)];
    }
    if(hour > 0) {
        duration = [NSString stringWithFormat:@"%@%d%@%@ ", duration, hour, fullFormatCheck?@" ":@"", (hour > 1)?LocalizedString(fullFormatCheck?@"hrs":@"h", nil):LocalizedString(fullFormatCheck?@"hr":@"h", nil)];
    }
    if(day == 0 && min > 0) {
        duration = [NSString stringWithFormat:@"%@%d%@%@ ", duration, min, fullFormatCheck?@" ":@"", (min > 1)?LocalizedString(fullFormatCheck?@"mins":@"m", nil):LocalizedString(fullFormatCheck?@"min":@"m", nil)];
    }
    
    if (min == 0 && sec>0) {
        duration = [NSString stringWithFormat:@"%@%d%@%@ ", duration, sec, fullFormatCheck?@" ":@"", (sec > 1)?LocalizedString(fullFormatCheck?@"seconds":@"s", nil):LocalizedString(fullFormatCheck?@"sec":@"s", nil)];
    }
    
    if(diff == 0) {
        duration = [NSString stringWithFormat:@"0 %@ ", LocalizedString(fullFormatCheck?@"mins":@"m", nil)];
    }

    return duration;
}

-(NSString *)formatDateTime:(NSDate *)sDate
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    //[dateFormatter setDateFormat:@"E d MMM, yyyy hh:mm a"];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    [dateFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
    NSString *dateStr = [dateFormatter stringFromDate:sDate];
    NSLog(@"%@",dateStr);
    return dateStr;
    
}

#pragma mark - Keywords Implementations
-(void)doSetupForKeywordView
{
    [self.btnaddKeyword addTarget:self action:@selector(didSelectAddKeyword:) forControlEvents:UIControlEventTouchUpInside];
    
    if (IS_IPAD) {
        self.keyword1.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.keyword2.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.keyword3.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.keyword4.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.keyword5.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.keyword6.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.keyword7.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.keyword8.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.keyword9.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
    }
    
    // keyword view
    keywordsArray = [[NSMutableArray alloc] init];
    self.keywordView.backgroundColor = [UIColor clearColor];
    self.keyword1.backgroundColor = [UIColor clearColor];
    self.keyword2.backgroundColor = [UIColor clearColor];
    self.keyword3.backgroundColor = [UIColor clearColor];
    self.keyword4.backgroundColor = [UIColor clearColor];
    self.keyword5.backgroundColor = [UIColor clearColor];
    self.keyword6.backgroundColor = [UIColor clearColor];
    self.keyword7.backgroundColor = [UIColor clearColor];
    self.keyword8.backgroundColor = [UIColor clearColor];
    self.keyword9.backgroundColor = [UIColor clearColor];
    
    self.keyword1.text = @"";
    self.keyword2.text = @"";
    self.keyword3.text = @"";
    self.keyword4.text = @"";
    self.keyword5.text = @"";
    self.keyword6.text = @"";
    self.keyword7.text = @"";
    self.keyword8.text = @"";
    self.keyword9.text = @"";
    
    self.keyword1.layer.cornerRadius = 4.0f;
    self.keyword1.layer.masksToBounds = YES;
    self.keyword2.layer.cornerRadius = 4.0f;
    self.keyword2.layer.masksToBounds = YES;
    self.keyword3.layer.cornerRadius = 4.0f;
    self.keyword3.layer.masksToBounds = YES;
    self.keyword4.layer.cornerRadius = 4.0f;
    self.keyword4.layer.masksToBounds = YES;
    self.keyword5.layer.cornerRadius = 4.0f;
    self.keyword5.layer.masksToBounds = YES;
    self.keyword6.layer.cornerRadius = 4.0f;
    self.keyword5.layer.masksToBounds = YES;
    self.keyword7.layer.cornerRadius = 4.0f;
    self.keyword7.layer.masksToBounds = YES;
    self.keyword8.layer.cornerRadius = 4.0f;
    self.keyword8.layer.masksToBounds = YES;
    self.keyword9.layer.cornerRadius = 4.0f;
    self.keyword9.layer.masksToBounds = YES;
    
    self.keyword1.userInteractionEnabled = YES;
    self.keyword2.userInteractionEnabled = YES;
    self.keyword3.userInteractionEnabled = YES;
    self.keyword4.userInteractionEnabled = YES;
    self.keyword5.userInteractionEnabled = YES;
    self.keyword6.userInteractionEnabled = YES;
    self.keyword7.userInteractionEnabled = YES;
    self.keyword8.userInteractionEnabled = YES;
    self.keyword9.userInteractionEnabled = YES;
    
    self.keyword1.tag = 1;
    self.keyword2.tag = 2;
    self.keyword3.tag = 3;
    self.keyword4.tag = 4;
    self.keyword5.tag = 5;
    self.keyword6.tag = 6;
    self.keyword7.tag = 7;
    self.keyword8.tag = 8;
    self.keyword9.tag = 9;
    
    UITapGestureRecognizer *gesture1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)] ;
    UITapGestureRecognizer *gesture2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)] ;
    UITapGestureRecognizer *gesture3 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)] ;
    UITapGestureRecognizer *gesture4 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)] ;
    UITapGestureRecognizer *gesture5 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)] ;
    UITapGestureRecognizer *gesture6 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)] ;
    UITapGestureRecognizer *gesture7 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)] ;
    UITapGestureRecognizer *gesture8 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)] ;
    UITapGestureRecognizer *gesture9 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)] ;
    
    [self.keyword1 addGestureRecognizer:gesture1];
    [self.keyword2 addGestureRecognizer:gesture2];
    [self.keyword3 addGestureRecognizer:gesture3];
    [self.keyword4 addGestureRecognizer:gesture4];
    [self.keyword5 addGestureRecognizer:gesture5];
    [self.keyword6 addGestureRecognizer:gesture6];
    [self.keyword7 addGestureRecognizer:gesture7];
    [self.keyword8 addGestureRecognizer:gesture8];
    [self.keyword9 addGestureRecognizer:gesture9];
    
    UILongPressGestureRecognizer *longPressGesture1 = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongGesture:)];
    longPressGesture1.minimumPressDuration = 1.5;
    UILongPressGestureRecognizer *longPressGesture2 = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongGesture:)];
    longPressGesture2.minimumPressDuration = 1.5;
    UILongPressGestureRecognizer *longPressGesture3 = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongGesture:)];
    longPressGesture3.minimumPressDuration = 1.5;
    UILongPressGestureRecognizer *longPressGesture4 = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongGesture:)];
    longPressGesture4.minimumPressDuration = 1.5;
    UILongPressGestureRecognizer *longPressGesture5 = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongGesture:)];
    longPressGesture5.minimumPressDuration = 1.5;
    UILongPressGestureRecognizer *longPressGesture6 = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongGesture:)];
    longPressGesture6.minimumPressDuration = 1.5;
    UILongPressGestureRecognizer *longPressGesture7 = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongGesture:)];
    longPressGesture7.minimumPressDuration = 1.5;
    UILongPressGestureRecognizer *longPressGesture8 = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongGesture:)];
    longPressGesture8.minimumPressDuration = 1.5;
    UILongPressGestureRecognizer *longPressGesture9 = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongGesture:)];
    longPressGesture9.minimumPressDuration = 1.5;
    
    [self.keyword1 addGestureRecognizer:longPressGesture1];
    [self.keyword2 addGestureRecognizer:longPressGesture2];
    [self.keyword3 addGestureRecognizer:longPressGesture3];
    [self.keyword4 addGestureRecognizer:longPressGesture4];
    [self.keyword5 addGestureRecognizer:longPressGesture5];
    [self.keyword6 addGestureRecognizer:longPressGesture6];
    [self.keyword7 addGestureRecognizer:longPressGesture7];
    [self.keyword8 addGestureRecognizer:longPressGesture8];
    [self.keyword9 addGestureRecognizer:longPressGesture9];
    
}

-(void)handleTap:(UITapGestureRecognizer *)sender
{
    NSString * seletedKey = @"";
    
    if (sender.view.tag == 1) {
        seletedKey = self.keyword1.text;
    }else if (sender.view.tag == 2) {
        seletedKey = self.keyword2.text;
    }else if (sender.view.tag == 3) {
        seletedKey = self.keyword3.text;
    }else if (sender.view.tag == 4) {
        seletedKey = self.keyword4.text;
    }else if (sender.view.tag == 5) {
        seletedKey = self.keyword5.text;
    }else if (sender.view.tag == 6) {
        seletedKey = self.keyword6.text;
    }else if (sender.view.tag == 7) {
        seletedKey = self.keyword7.text;
    }else if (sender.view.tag == 8) {
        seletedKey = self.keyword8.text;
    }else if (sender.view.tag == 9) {
        seletedKey = self.keyword9.text;
    }
    
    self.txtNotes.text = [NSString stringWithFormat:@"%@ %@",self.txtNotes.text, seletedKey];
    
}

-(void)handleLongGesture:(UILongPressGestureRecognizer *)sender
{
    int tag = (int)sender.view.tag;
    CGPoint buttonPosition = [sender.view convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *indexPathToReload = [self.tableView indexPathForRowAtPoint:buttonPosition];
    
    //    if ( sender.state == UIGestureRecognizerStateEnded ) {
    
    if (keywordsArray.count>0) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Remove Keyword" message:@"Are you sure you want to remove this Keyword !" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *btnOk = [UIAlertAction actionWithTitle:LocalizedString(@"btn_ok", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            
            [keywordsArray removeObjectAtIndex:tag-1];
            [self addKeywordLabel];
            [self addKeywordsList];
            //[self.tableView reloadRowsAtIndexPaths:@[indexPathToReload] withRowAnimation:UITableViewRowAnimationBottom];
            [self.tableView reloadData];
            
        }];
        
        UIAlertAction *btnCancel = [UIAlertAction actionWithTitle:LocalizedString(@"btn_cancel", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [alert dismissViewControllerAnimated:YES completion:nil];
        }];
        [alert addAction:btnCancel];
        [alert addAction:btnOk];
        [self presentViewController:alert animated:YES completion:nil];
    }
    //    }
}

-(void)didSelectAddKeyword:(UIButton *)sender
{
    if (keywordsArray.count<9) {
        
        CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
        NSIndexPath *indexPathToReload = [self.tableView indexPathForRowAtPoint:buttonPosition];
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:@"Add Keyword" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *btnOk = [UIAlertAction actionWithTitle:LocalizedString(@"btn_ok", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            if(alert.textFields[0].text.length > 0)
            {
                [alert dismissViewControllerAnimated:YES completion:nil];
                
                NSString *text = alert.textFields[0].text;
                [keywordsArray addObject:text];
                [self addKeywordLabel];
                [self addKeywordsList];
                //[self.tableView reloadRowsAtIndexPaths:@[indexPathToReload] withRowAnimation:UITableViewRowAnimationBottom];
                [self.tableView reloadData];
            }
            else
            {
                [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:LocalizedString(@"error_family_name", nil) input:nil];
            }
        }];
        
        UIAlertAction *btnCancel = [UIAlertAction actionWithTitle:LocalizedString(@"btn_cancel", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            
            [alert dismissViewControllerAnimated:YES completion:nil];
        }];
        
        [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
            textField.autocapitalizationType = UITextAutocapitalizationTypeWords;
        }];
        
        [alert addAction:btnCancel];
        [alert addAction:btnOk];
        
        [self presentViewController:alert animated:YES completion:nil];
        
    }
    else {
        [self showAlertWithTitle:LocalizedString(@"Keywords", nil) message:@"You can not add more than 9 keywords !" input:nil];
    }
    
}

-(void)addKeywordLabel
{
    self.keyword1.text = @"";
    self.keyword2.text = @"";
    self.keyword3.text = @"";
    self.keyword4.text = @"";
    self.keyword5.text = @"";
    self.keyword6.text = @"";
    self.keyword7.text = @"";
    self.keyword8.text = @"";
    self.keyword9.text = @"";
    
    self.keyword1.backgroundColor = [UIColor clearColor];
    self.keyword2.backgroundColor = [UIColor clearColor];
    self.keyword3.backgroundColor = [UIColor clearColor];
    self.keyword4.backgroundColor = [UIColor clearColor];
    self.keyword5.backgroundColor = [UIColor clearColor];
    self.keyword6.backgroundColor = [UIColor clearColor];
    self.keyword7.backgroundColor = [UIColor clearColor];
    self.keyword8.backgroundColor = [UIColor clearColor];
    self.keyword9.backgroundColor = [UIColor clearColor];
    
    if (keywordsArray.count>0) {
        
        for (int i=0;i<keywordsArray.count;i++) {
            if (i==0) {
                self.keyword1.text =[NSString stringWithFormat:@"%@",[keywordsArray objectAtIndex:i]];
                self.keyword1.backgroundColor = keywordsColor;
            }
            else if (i==1) {
                self.keyword2.text =[NSString stringWithFormat:@"%@",[keywordsArray objectAtIndex:i]];
                self.keyword2.backgroundColor = keywordsColor;
            }
            else if (i==2) {
                self.keyword3.text =[NSString stringWithFormat:@"%@",[keywordsArray objectAtIndex:i]];
                self.keyword3.backgroundColor = keywordsColor;
            }
            else if (i==3) {
                self.keyword4.text =[NSString stringWithFormat:@"%@",[keywordsArray objectAtIndex:i]];
                self.keyword4.backgroundColor = keywordsColor;
            }
            else if (i==4) {
                self.keyword5.text =[NSString stringWithFormat:@"%@",[keywordsArray objectAtIndex:i]];
                self.keyword5.backgroundColor = keywordsColor;
            }
            else if (i==5) {
                self.keyword6.text =[NSString stringWithFormat:@"%@",[keywordsArray objectAtIndex:i]];
                self.keyword6.backgroundColor = keywordsColor;
            }
            else if (i==6) {
                self.keyword7.text =[NSString stringWithFormat:@"%@",[keywordsArray objectAtIndex:i]];
                self.keyword7.backgroundColor = keywordsColor;
            }
            else if (i==7) {
                self.keyword8.text =[NSString stringWithFormat:@"%@",[keywordsArray objectAtIndex:i]];
                self.keyword8.backgroundColor = keywordsColor;
            }
            else if (i==8) {
                self.keyword9.text =[NSString stringWithFormat:@"%@",[keywordsArray objectAtIndex:i]];
                self.keyword9.backgroundColor = keywordsColor;
            }
        }
        
    }
}

-(void)getKeywordsList
{
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setValue:@([shareObject integerForKey:@"user_id"]) forKey:@"user_id"];
    [parameters setValue:@([shareObject integerForKey:@"baby_id"]) forKey:@"baby_id"];
    [parameters setValue:@"activities" forKey:@"type"];
    [parameters setValue:@"feed" forKey:@"category"];
    
    [[WebServiceManager sharedManager] initWithBaseURL:@"api/getKeywords" parameters:parameters success:^(id result) {
        
        if ([[result valueForKey:@"success"] boolValue]) {
            NSMutableArray *list = (NSMutableArray *)[result valueForKey:@"list"];
            if(list.count > 0){
                
                keywordslistDict = [[NSDictionary alloc] initWithDictionary:[result valueForKey:@"list"]];
                NSString *str;
                if ([selectedType isEqualToString:@"lboob"] || [selectedType isEqualToString:@"rboob"]) {
                    str = [keywordslistDict objectForKey:@"nursing"];
                }else {
                    str = [keywordslistDict objectForKey:selectedType];
                }
                if (str.length>0) {
                    keywordsArray = [[NSMutableArray alloc] initWithArray:[str componentsSeparatedByString:@","]];
                }else {
                    keywordsArray = [[NSMutableArray alloc] init];
                }
            }
            [self addKeywordLabel];
            [self.tableView reloadData];
        }
        else {
            NSLog(@"error");
        }
        
    } failure:^(NSError *error) {
        
    }];
}

-(void)addKeywordsList
{
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setValue:@([shareObject integerForKey:@"user_id"]) forKey:@"user_id"];
    [parameters setValue:@([shareObject integerForKey:@"baby_id"]) forKey:@"baby_id"];
    [parameters setValue:@"activities" forKey:@"type"];
    [parameters setValue:@"feed" forKey:@"category"];
    [parameters setValue:selectedType forKey:@"sub_category"];
    if (selectedTypeIndex == 2) {
        [parameters setValue:@"nursing" forKey:@"sub_category"];
    }
    [parameters setValue:@1 forKey:@"notes"];
    
    NSLog(@"%@",parameters);
    if (keywordsArray.count>0) {
        for(int i=0; i<keywordsArray.count; i++){
            [parameters setValue:[keywordsArray componentsJoinedByString:@","] forKey:@"keywords"];
        }
    }else{
        [parameters setValue:@"" forKey:@"keywords"];
    }

    [[WebServiceManager sharedManager] initWithBaseURL:@"api/setKeywords" parameters:parameters success:^(id result) {

        if ([[result valueForKey:@"success"] boolValue]) {
            NSMutableArray *list = (NSMutableArray *)[result valueForKey:@"list"];
            if(list.count > 0){
                
                keywordslistDict = [[NSDictionary alloc] initWithDictionary:[result valueForKey:@"list"]];
                NSString *str;
                if ([selectedType isEqualToString:@"lboob"] || [selectedType isEqualToString:@"rboob"]) {
                    str = [keywordslistDict objectForKey:@"nursing"];
                }else {
                    str = [keywordslistDict objectForKey:selectedType];
                }
                if (str.length>0) {
                    keywordsArray = [[NSMutableArray alloc] initWithArray:[str componentsSeparatedByString:@","]];
                }else {
                    keywordsArray = [[NSMutableArray alloc] init];
                }

                //keywordsArray = list;
                //[list removeAllObjects];
            }
        }
        else {
            NSLog(@"error");
        }

    } failure:^(NSError *error) {

    }];
    
}

#pragma mark - Pickerview delegate
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if ([pickerView isEqual:self.volumeUnitPicker])
    {
        if (selectedTypeIndex == 3) {
            volumeUnitList  = [[NSMutableArray alloc] initWithObjects: @"gm", @"oz", @"tbsp", @"tspn", @"cups", nil];
        }else {
            volumeUnitList  = [[NSMutableArray alloc] initWithObjects: @"ml", nil];
        }
        return volumeUnitList.count;
    }
    else if ([pickerView isEqual:self.volumeValuePicker])
    {
        return  no1000List.count;
    }
    else {
        return  no100List.count;
    }
}
-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if ([pickerView isEqual:self.volumeUnitPicker])
    {
        return [volumeUnitList objectAtIndex:row];
    }
    else if ([pickerView isEqual:self.volumeValuePicker])
    {
        return [no1000List objectAtIndex:row];
    }
    else
    {
        return [no100List objectAtIndex:row];
    }

}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view{
    
    UILabel* tView = (UILabel*)view;
    if (!tView){
        tView = [[UILabel alloc] init];
    }
    
    tView.frame = pickerView.frame;
    tView.backgroundColor = [UIColor clearColor];
    tView.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:16.0f];
    if (IS_IPAD) {
        tView.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:22.0f];
    }
    tView.textColor = UIColorFromRGB(0x786E6F);
    tView.textAlignment = NSTextAlignmentCenter;
    
    NSString *txt  = @"";
    if ([pickerView isEqual:self.volumeUnitPicker])
    {
        txt = [volumeUnitList objectAtIndex:row];
    }
    else if ([pickerView isEqual:self.volumeValuePicker])
    {
        txt = [no1000List objectAtIndex:row];
    }
    else
    {
        txt = [no100List objectAtIndex:row];
    }

    tView.text = txt;
    
    [[pickerView.subviews objectAtIndex:1] setBackgroundColor:[UIColor clearColor]];
    [[pickerView.subviews objectAtIndex:2] setBackgroundColor:[UIColor clearColor]];
    
    return tView;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    return pickerView.frame.size.height;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if ([pickerView isEqual:self.volumeValuePicker]) {
        editAmountCheck = YES;
        selectedVolume = [[no1000List objectAtIndex:[pickerView selectedRowInComponent:component]] floatValue];
    }
    else if ([pickerView isEqual:self.durationPickerLeft]) {
         editLeftValueCheck = YES;
        selectedDurationValueLeft = [[no100List objectAtIndex:[pickerView selectedRowInComponent:component]] intValue];
    }
    else if ([pickerView isEqual:self.durationPickerRight]) {
         editRightValueCheck = YES;
        selectedDurationValueRight = [[no100List objectAtIndex:[pickerView selectedRowInComponent:component]] intValue];
    }
    else if ([pickerView isEqual:self.volumeUnitPicker]) {
        editAmountUnitCheck = YES;
        selectedUnit = [volumeUnitList objectAtIndex:[pickerView selectedRowInComponent:component]];
    }
}

@end
