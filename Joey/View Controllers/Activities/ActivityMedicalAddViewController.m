//
//  ActivityMedicalAddViewController.m
//  Joey
//
//  Created by csl on 5/1/2017.
//  Copyright © 2017 Auxilia. All rights reserved.
//

#import "AppDelegate.h"
#import "ActivityMedicalAddViewController.h"

@interface ActivityMedicalAddViewController ()
{
    NSUserDefaults *shareObject;
    AppDelegate *appDelegate;
    NSMutableDictionary *unitInfo;
    NSMutableDictionary *medicalInfo;
    NSDateFormatter *formatter;
    NSMutableArray *typeList;
    NSMutableArray *typeValueList;
    NSMutableArray *no10List;
    NSMutableArray *no100List;
    NSMutableArray *no1000List;
    NSMutableArray *no24List;
    NSMutableArray *no60List;
    NSMutableArray *digitList;
    NSMutableArray *fractionList;
    NSMutableArray *fractionValueList;
    NSMutableArray *temperatureCelsiusList;
    NSMutableArray *temperatureFahrenheitList;
    NSArray *selectedTemperatureIndexes;
    NSArray *selectedTotalIndexes;
    NSDate *selectedDateTime;

    NSArray *doseUnitList;
    
    BOOL saveCheck;
    
    BOOL editTemperatureCheck;
    BOOL editAmountCheck;
    BOOL editDateCheck;
    BOOL editMedSymNameCheck;
    
    int selectedTypeIndex;
    float selectedTemperature;
    float selectedTotal;
    NSInteger checkTagButton;
    NSString *selectedType;
   
    int selectedTemprature1;
    int selectedTemprature2;
    int selectedVolume;
    NSString *selectedVolumeUnit;
    
    NSMutableArray *keywordsArray;
    NSMutableArray *medSymNameArray;
    UIColor *keywordColor;
    NSDictionary *keywordslistDict;
    NSString *currentKeyAttribute;
    NSString *videoUrl;
    NSString *videoUrl2;
    NSString *videoUrl3;
}
@end

@implementation ActivityMedicalAddViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    shareObject = [NSUserDefaults standardUserDefaults];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    formatter = [[NSDateFormatter alloc] init];
    [formatter setTimeZone:[NSTimeZone localTimeZone]];
    [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
    
    unitInfo = (NSMutableDictionary *)[NSKeyedUnarchiver unarchiveObjectWithData:[shareObject objectForKey:@"unit_info"]];
    medicalInfo = (NSMutableDictionary *)[NSKeyedUnarchiver unarchiveObjectWithData:[shareObject objectForKey:@"medical_info"]];
    NSLog(@"medicalInfo=%@", medicalInfo);
    
    no100List = [[NSMutableArray alloc] init];
    for(int i=0; i<=100; i++) [no100List addObject:[NSString stringWithFormat:@"%d", i]];
    no1000List = [[NSMutableArray alloc] init];
    for(int i=0; i<=1000; i++) [no1000List addObject:[NSString stringWithFormat:@"%d", i]];
    digitList = [[NSMutableArray alloc] init];
    for(int i=0; i<10; i++) [digitList addObject:[NSString stringWithFormat:@"%d", i]];
    doseUnitList = [[NSArray alloc]initWithObjects:@"ml",@"oz",@"tbsp",@"tspn",@"sachet",@"tablet",@"droplet",@"puff",@"dose",@"cream", nil];
    
    no10List = [[NSMutableArray alloc] init];
    for(int i=0; i<=10; i++) [no10List addObject:[NSString stringWithFormat:@"%d", i]];
    
    no24List = [[NSMutableArray alloc] init];
    for(int i=0; i<24; i++) [no24List addObject:[NSString stringWithFormat:@"%d", i]];
    
    no60List = [[NSMutableArray alloc] init];
    for(int i=0; i<60; i++) [no60List addObject:[NSString stringWithFormat:@"%d", i]];
    
    fractionValueList = [[NSMutableArray alloc] initWithObjects: @".00", @".50", @".33", @".67", @".25", @".75", nil];
    fractionList = [[NSMutableArray alloc] initWithObjects: @"0", @"½", @"⅓", @"⅔", @"¼", @"¾", nil];
    
    temperatureCelsiusList = [[NSMutableArray alloc] init];
    for(int i=35; i<=41; i++) [temperatureCelsiusList addObject:[NSString stringWithFormat:@"%d", i]];
    
    temperatureFahrenheitList = [[NSMutableArray alloc] init];
    for(int i=95; i<=110; i++) [temperatureFahrenheitList addObject:[NSString stringWithFormat:@"%d", i]];
    
    self.numberMLPicvker.delegate = self;
    self.numberMLPicvker.dataSource = self;
    self.mlPicvker.delegate = self;
    self.mlPicvker.dataSource = self;
    self.temp1Picvker.delegate = self;
    self.temp1Picvker.dataSource = self;
    self.temp2Picvker.dataSource = self;
    self.temp2Picvker.delegate = self;

    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:UIColorFromRGB(0x8D8082), NSForegroundColorAttributeName, [UIFont fontWithName:@"SFUIDisplay-Medium" size:18.0f], NSFontAttributeName, nil]];
    [self.navigationController.navigationBar setBarTintColor:UIColorFromRGB(0xF4D2D5)];
    [self.navigationController.navigationBar setTranslucent:NO];
    
    [self.navigationItem setTitle:LocalizedString(@"title_new_medical", nil)];
    [self customBackButton];
    
    self.btnSave.title = LocalizedString(@"btn_save", nil);
    
    self.lblDateTime.text = LocalizedString(@"txt_date", nil);
    self.lblMedSymp.text = LocalizedString(@"txt_medicine", nil);
    self.lblDose.text = LocalizedString(@"txt_dose", nil);
    self.lblTemperature.text = LocalizedString(@"txt_temp", nil);
    self.lblPhoto.text = LocalizedString(@"txt_photo", nil);
    self.lblNotes.text = LocalizedString(@"txt_notes", nil);

    self.titleMedicine.text = LocalizedString(@"txt_medicine", nil);
    self.titltSymptoms.text = LocalizedString(@"txt_symptom", nil);
    self.titleTemparature.text = LocalizedString(@"txt_temperature", nil);
    self.titleMovement.text = LocalizedString(@"txt_movement", nil);

    self.titleMedicine.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:11.0f];
    self.titltSymptoms.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:11.0f];
    self.titleTemparature.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:11.0f];
    self.titleMovement.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:11.0f];

    self.btnMedicineWidth.constant = 36.0f;
    self.btnMovementWidth.constant = 36.0f;
    self.btnSymptompsWidth.constant = 36.0f;
    self.btnTemparatureWidth.constant = 36.0f;
    
    if (IS_IPAD) {
        [self.view setNeedsLayout];
        [self.view updateConstraintsIfNeeded];
        
        self.lblDateTime.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:22.0f];
        self.lblMedSymp.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:22.0f];
        self.lblDose.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:22.0f];
        self.lblTemperature.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:22.0f];
        self.lblPhoto.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:22.0f];
        self.lblNotes.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:22.0f];
        
        self.celciusLBL.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:22.0f];
        
        self.txtDateTime.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:22.0f];
         self.txtNotes.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:22.0f];
        self.txtMedSymp.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:22.0f];

        self.titleMedicine.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:22.0f];
        self.titltSymptoms.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:22.0f];
        self.titleTemparature.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:22.0f];
        self.titleMovement.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:22.0f];

        self.btnMedicineWidth.constant = 44.0f;
        self.btnMovementWidth.constant = 44.0f;
        self.btnSymptompsWidth.constant = 44.0f;
        self.btnTemparatureWidth.constant = 44.0f;
    }
    

    NSString *lastType = [medicalInfo objectForKey:@"last_type"];
    [self selectMedicalTypeAction:self.btnMedicine];
    
    if ([lastType isEqualToString:@"symptom"]) {
        [self selectMedicalTypeAction:self.btnSymptoms];
    }
    else if ([lastType isEqualToString:@"temperature"]) {
        [self selectMedicalTypeAction:self.btnTemperature];
    }
    else if ([lastType isEqualToString:@"movement"]) {
        [self selectMedicalTypeAction:self.btnMovement];
    }
    
    self.btnPhoto1.imageView.contentMode = UIViewContentModeScaleAspectFill;
    self.btnPhoto2.imageView.contentMode = UIViewContentModeScaleAspectFill;
    self.btnPhoto3.imageView.contentMode = UIViewContentModeScaleAspectFill;
    self.btnPhoto1.hidden = YES;
    self.btnPhoto2.hidden = YES;
    self.btnPhoto3.hidden = YES;
    self.btnPhoto1.tag = 1;
    self.btnPhoto2.tag = 2;
    self.btnPhoto3.tag = 3;
    [self.btnPhoto1 addTarget:self action:@selector(showPhotoAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.btnPhoto2 addTarget:self action:@selector(showPhotoAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.btnPhoto3 addTarget:self action:@selector(showPhotoAction:) forControlEvents:UIControlEventTouchUpInside];

    [self changeAction];
    [self getKeywordsList];
    [self doSetupForKeywordView];
    [self doSetupForMedKeywordView];
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:(BOOL)animated];
    
    [appDelegate.tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"ui_action" action:@"button_press" label:@"Add New Medical" value:nil] build]];
    
    NSLog(@"self.data=%@", self.data);
    if([[self.data objectForKey:@"deleteCheck"] boolValue])
    {
        if ([[self.data objectForKey:@"imageTag"] integerValue] == 1) {
            self.btnPhoto1.hidden = YES;
            [self.data removeObjectForKey:@"imageTag"];
            videoUrl = nil;
        }else if ([[self.data objectForKey:@"imageTag"] integerValue] == 2) {
            self.btnPhoto2.hidden = YES;
            [self.data removeObjectForKey:@"imageTag"];
            videoUrl2 = nil;
        }else if ([[self.data objectForKey:@"imageTag"] integerValue] == 3) {
            self.btnPhoto3.hidden = YES;
            [self.data removeObjectForKey:@"imageTag"];
            videoUrl3 = nil;
        }
        
        [self.data removeObjectForKey:@"deleteCheck"];
        
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.txtMedSymp.userInteractionEnabled = NO;
}

#pragma mark - Button Action Methods
- (void)customBackButton
{
    if([self.navigationController.viewControllers count] > 1)
    {
        UIImage *imgBack = [UIImage imageNamed:@"nav_btn_back"];
        UIButton *btnBack = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnBack setImage:imgBack forState:UIControlStateNormal];
        btnBack.frame = CGRectMake(0, 0, imgBack.size.width, imgBack.size.height);
        [btnBack addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
        [btnBack setImageEdgeInsets:UIEdgeInsetsMake(0, -16, 0, 16)];
        UIBarButtonItem *backBarItem = [[UIBarButtonItem alloc] initWithCustomView:btnBack];
        self.navigationItem.leftBarButtonItem = backBarItem;
    }
}

- (void)backAction
{
    [COMMON_HELPER clearTmpDirectory];
    UIViewController *viewController = [self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count-2];
    viewController.data = [[NSMutableDictionary alloc] initWithDictionary:@{@"save_check":@(saveCheck)}];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)changeAction
{
    if (!editTemperatureCheck && [selectedType isEqualToString:@"temperature"]) {
        float value = 0;
        NSString *unit = @"C";
        
        if(medicalInfo.count > 0 && [medicalInfo objectForKey:selectedType])
        {
            value = [[[medicalInfo objectForKey:selectedType] objectForKey:@"value"] floatValue];
            unit = [[medicalInfo objectForKey:selectedType] objectForKey:@"unit"];
        }
        
        selectedTemperature = [self convertUnit:value unit:unit newUnit:[unitInfo objectForKey:@"temperature"]];
        selectedTemperatureIndexes = [[NSString stringWithFormat:@"%.1f", selectedTemperature] componentsSeparatedByString:@"."];
        
        if([[unitInfo objectForKey:@"temperature"] isEqualToString:@"°C"])
        {
            self.celciusLBL.text = @"C";
            selectedTemperatureIndexes = [NSArray arrayWithObjects:@([temperatureCelsiusList indexOfObject:selectedTemperatureIndexes[0]]), @([selectedTemperatureIndexes[1] intValue]), nil];
            
            int temp1Index = [[selectedTemperatureIndexes objectAtIndex:0] intValue];
            int temp2Index = [[selectedTemperatureIndexes objectAtIndex:1] intValue];

            if (temp1Index<0 || temp1Index>temperatureCelsiusList.count-1) {
                temp1Index = 0;
                temp2Index = 0;
            }
            
            [self.temp1Picvker selectRow:temp1Index inComponent:0 animated:YES];
            [self.temp2Picvker selectRow:temp2Index inComponent:0 animated:YES];
            selectedTemprature1 = [[temperatureCelsiusList objectAtIndex:temp1Index] intValue];
            selectedTemprature2 = [[digitList objectAtIndex:temp2Index] intValue];

        }
        else
        {
            self.celciusLBL.text = @"F";
            selectedTemperatureIndexes = [NSArray arrayWithObjects:@([temperatureFahrenheitList indexOfObject:selectedTemperatureIndexes[0]]), @([selectedTemperatureIndexes[1] intValue]), nil];
            
            int temp1Index = [[selectedTemperatureIndexes objectAtIndex:0] intValue];
            int temp2Index = [[selectedTemperatureIndexes objectAtIndex:1] intValue];
            
            if (temp1Index<0 || temp1Index>temperatureFahrenheitList.count-1) {
                temp1Index = 0;
                temp2Index = 0;
            }
            
            [self.temp1Picvker selectRow:temp1Index inComponent:0 animated:YES];
            [self.temp2Picvker selectRow:temp2Index inComponent:0 animated:YES];
            selectedTemprature1 = [[temperatureFahrenheitList objectAtIndex:temp1Index] intValue];
            selectedTemprature2 = [[digitList objectAtIndex:temp2Index] intValue];

        }
        
    }
    
    if (!editAmountCheck && [selectedType isEqualToString:@"medicine"]) {
        float value = 0;
        NSString *unit = [unitInfo objectForKey:@"medicine"];
        NSString *medName = @"";
        
        if(medicalInfo.count > 0 && [medicalInfo objectForKey:selectedType])
        {
            value = [[[medicalInfo objectForKey:selectedType] objectForKey:@"value"] floatValue];
            unit = [[medicalInfo objectForKey:selectedType] objectForKey:@"unit"];
            medName = [[medicalInfo objectForKey:selectedType]objectForKey:@"medicine"];
        }
        
        selectedTotal = [self convertUnit:value unit:unit newUnit:[unitInfo objectForKey:@"medicine"]];
        if([[unitInfo objectForKey:@"medicine"] isEqualToString:@"ml"])
        {
            selectedTotalIndexes = [NSArray arrayWithObjects:@([selectedTotalIndexes[0] intValue]), @([selectedTotalIndexes[1] intValue]), nil];
        }
        else
        {
            selectedTotalIndexes = [NSArray arrayWithObjects:@([selectedTotalIndexes[0] intValue]), @([fractionValueList indexOfObject:selectedTotalIndexes[1]]), nil];
        }
        
        selectedVolume = value;
        selectedVolumeUnit = unit;
        
        self.txtMedSymp.text = medName;
        [self.numberMLPicvker selectRow:(int)value inComponent:0 animated:YES];
        NSUInteger unitIndex = [doseUnitList indexOfObject:unit];
        [self.mlPicvker selectRow:(int)unitIndex inComponent:0 animated:YES];
        
    }
    
    if(!editDateCheck)
    {
        selectedDateTime = [NSDate date];
        if([[shareObject objectForKey:@"language"] hasPrefix:@"zh"])
        {
            [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"zh_HK"]];
            [formatter setDateStyle:NSDateFormatterMediumStyle];
            [formatter setTimeStyle:NSDateFormatterShortStyle];
        }
        else
        {
            [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
            formatter.dateFormat = @"EE d MMM, yyyy hh:mm a";
        }
        self.txtDateTime.text = [NSString stringWithFormat:@"%@", [formatter stringFromDate:selectedDateTime]];
        
        NSLog(@"selectedDateTime=%@", selectedDateTime);
    }
    
    if (!editMedSymNameCheck && [selectedType isEqualToString:@"symptom"]) {
        
        if(medicalInfo.count > 0 && [medicalInfo objectForKey:selectedType])
        {
            self.txtMedSymp.text = [[medicalInfo objectForKey:selectedType]objectForKey:@"symptom"];
        }
        
    }
}

- (void)showCameraAction
{
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    if ([[NSUserDefaults standardUserDefaults]boolForKey:@"is_Subscription_Enabled"]) {
        imagePicker.mediaTypes = [[NSArray alloc] initWithObjects: (NSString *) kUTTypeMovie, (NSString *) kUTTypeVideo, (NSString *) kUTTypeImage, nil];
        imagePicker.videoQuality = UIImagePickerControllerQualityTypeLow;
        imagePicker.videoMaximumDuration = 120;
    }else {
        imagePicker.mediaTypes = [[NSArray alloc] initWithObjects: (NSString *) kUTTypeImage, nil];
    }
    
    [self presentViewController:imagePicker animated:NO completion:nil];
}

- (void)showPhotoAlbumAction
{
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    if ([[NSUserDefaults standardUserDefaults]boolForKey:@"is_Subscription_Enabled"]) {
        imagePicker.mediaTypes = [[NSArray alloc] initWithObjects: (NSString *) kUTTypeMovie, (NSString *) kUTTypeVideo, (NSString *) kUTTypeImage, nil];
    }else {
        imagePicker.mediaTypes = [[NSArray alloc] initWithObjects: (NSString *) kUTTypeImage, nil];
    }
    
    [self presentViewController:imagePicker animated:NO completion:nil];
}

- (void)showPhotoAction:(UIButton *)sender
{
    if (sender.hidden)
        return;
    
    UIImage *img;
    NSString *vStr;
    if (sender.tag == 1) {
        if (videoUrl && videoUrl.length>0) {
            vStr = videoUrl;
        }else {
            img = self.btnPhoto1.imageView.image;
        }
    }else if (sender.tag == 2) {
        if (videoUrl2 && videoUrl2.length>0) {
            vStr = videoUrl2;
        }else {
            img = self.btnPhoto2.imageView.image;
        }
    }else if (sender.tag == 3) {
        if (videoUrl3 && videoUrl3.length>0) {
            vStr = videoUrl3;
        }else {
            img = self.btnPhoto3.imageView.image;
        }
    }
    
    if (vStr && vStr.length>0) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"VideoViewController"];
        viewController.data = [[NSMutableDictionary alloc] initWithDictionary:@{@"viewController":self, @"video":vStr,@"imageTag":@(sender.tag)}];
        [self presentViewController:viewController animated:YES completion:nil];
    }
    else
    {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"PhotoViewController"];
        viewController.data = [[NSMutableDictionary alloc] initWithDictionary:@{@"viewController":self, @"image":img,@"imageTag":@(sender.tag)}];
        [self presentViewController:viewController animated:YES completion:nil];
    }
    
}

- (void)takePhotoAction
{
    AVAuthorizationStatus status = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    if(status == AVAuthorizationStatusDenied || status == AVAuthorizationStatusRestricted)
    {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:LocalizedString(@"error_camera", nil) message:@"" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *btnOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            
            [alert dismissViewControllerAnimated:YES completion:nil];
        }];
        
        [alert addAction:btnOk];
        
        [self presentViewController:alert animated:YES completion:nil];
        
    }
    else
    {
        [self showCameraAction];
    }
}

- (void)getPhotoAlbumAction
{
    PHAuthorizationStatus status = [PHPhotoLibrary authorizationStatus];
    if(status == PHAuthorizationStatusDenied || status == PHAuthorizationStatusRestricted)
    {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:LocalizedString(@"error_photo", nil) message:@"" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *btnOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            
            [alert dismissViewControllerAnimated:YES completion:nil];
        }];
        
        [alert addAction:btnOk];
        
        [self presentViewController:alert animated:YES completion:nil];
        
    }
    else
    {
        [self showPhotoAlbumAction];
    }
}

- (IBAction)nextAction:(id)sender{
    [SVProgressHUD show];
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setValue:@([shareObject integerForKey:@"user_id"]) forKey:@"user_id"];
    [parameters setValue:@([shareObject integerForKey:@"baby_id"]) forKey:@"baby_id"];
    [parameters setValue:selectedType forKey:@"type"];
    [parameters setValue:@"medical" forKey:@"name"];
    [parameters setValue:@(0) forKey:@"value"];
    [parameters setValue:@"" forKey:@"unit"];
    
    if([selectedType isEqualToString:@"temperature"]){
        
        NSString *tempValue = [NSString stringWithFormat:@"%d.%d",selectedTemprature1,selectedTemprature2];
        selectedTemperature = [tempValue floatValue];
        
        tempValue = [NSString stringWithFormat:@"%f", [self convertUnit:selectedTemperature unit:[unitInfo objectForKey:@"temperature"] newUnit:@"°C"]];
        
        [parameters setValue:tempValue forKey:@"value"];
        [parameters setValue:@"°C" forKey:@"unit"];
        
    }
    else if([selectedType isEqualToString:@"medicine"] ){
        
        [parameters setValue:@(selectedVolume) forKey:@"value"];
        [parameters setValue:selectedVolumeUnit forKey:@"unit"];
        [parameters setValue:self.txtMedSymp.text forKey:@"medicine"];
        
    } else if([selectedType isEqualToString:@"symptom"]) {
        
        [parameters setValue:self.txtMedSymp.text forKey:@"symptom"];
        
    }
    
    [parameters setValue:self.txtNotes.text forKey:@"notes"];
    [parameters setValue:[self formatDateTime:selectedDateTime] forKey:@"time"];
    //[parameters setValue:[[NSString stringWithFormat:@"%@", selectedDateTime] substringWithRange:NSMakeRange(0, 19)] forKey:@"time"];
    
    if(!self.btnPhoto1.hidden || !self.btnPhoto2.hidden || !self.btnPhoto3.hidden) {
        [[WebServiceManager sharedManager] initWithBaseURL:@"api/addBabyActivity" parameters:parameters formData:^(id<AFMultipartFormData> formData) {
            
            if (!self.btnPhoto1.hidden) {
                if (videoUrl && videoUrl.length>0) {
                    [formData appendPartWithFileURL:[NSURL URLWithString:videoUrl] name:@"video" fileName:@"video.mp4" mimeType:@"video" error: nil];
                }else {
                    [formData appendPartWithFileData:UIImageJPEGRepresentation((UIImage*)self.btnPhoto1.imageView.image, 0.8f) name:@"image" fileName:@"image.jpg" mimeType:@"image/jpeg"];
                }
            }
            
            if (!self.btnPhoto2.hidden) {
                if (videoUrl2 && videoUrl2.length>0) {
                    [formData appendPartWithFileURL:[NSURL URLWithString:videoUrl2] name:@"video2" fileName:@"video2.mp4" mimeType:@"video" error: nil];
                }else {
                    [formData appendPartWithFileData:UIImageJPEGRepresentation((UIImage*)self.btnPhoto2.imageView.image, 0.8f) name:@"image2" fileName:@"image2.jpg" mimeType:@"image/jpeg"];
                }
            }
            
            if (!self.btnPhoto3.hidden) {
                if (videoUrl3 && videoUrl3.length>0) {
                    [formData appendPartWithFileURL:[NSURL URLWithString:videoUrl3] name:@"video3" fileName:@"video3.mp4" mimeType:@"video" error: nil];
                }else {
                    [formData appendPartWithFileData:UIImageJPEGRepresentation((UIImage*)self.btnPhoto3.imageView.image, 0.8f) name:@"image3" fileName:@"image3.jpg" mimeType:@"image/jpeg"];
                }
            }

        } success:^(id result) {
            
            NSLog(@"result=%@", result);
            if([[result objectForKey:@"success"] boolValue])
            {
                NSMutableArray *babyList = (NSMutableArray *)[NSKeyedUnarchiver unarchiveObjectWithData:[shareObject objectForKey:@"baby_list"]];
                [[babyList objectAtIndex:[shareObject integerForKey:@"baby_index"]] setObject:[result valueForKey:@"info"] forKey:@"medical_info"];
                [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:babyList] forKey:@"baby_list"];
                [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[result valueForKey:@"info"]] forKey:@"medical_info"];
                [shareObject synchronize];
                
                saveCheck = YES;
                appDelegate.updateTimelineCheck = appDelegate.updateActivitiesCheck = appDelegate.updateActivityCheck = YES;
                [self backAction];
            }
            else
            {
                [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:[result objectForKey:@"message"] input:nil];
            }
            
        } failure:^(NSError *error) {
            [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:LocalizedString(@"error_unknown", nil) input:nil];
        }];
    }
    else
    {
        [[WebServiceManager sharedManager] initWithBaseURL:@"api/addBabyActivity" parameters:parameters success:^(id result) {
            
            NSLog(@"result=%@", result);
            if([[result objectForKey:@"success"] boolValue])
            {
                NSMutableArray *babyList = (NSMutableArray *)[NSKeyedUnarchiver unarchiveObjectWithData:[shareObject objectForKey:@"baby_list"]];
                [[babyList objectAtIndex:[shareObject integerForKey:@"baby_index"]] setObject:[result valueForKey:@"info"] forKey:@"medical_info"];
                [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:babyList] forKey:@"baby_list"];
                [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[result valueForKey:@"info"]] forKey:@"medical_info"];
                [shareObject synchronize];
                
                saveCheck = YES;
                appDelegate.updateTimelineCheck = appDelegate.updateActivitiesCheck = appDelegate.updateActivityCheck = YES;
                [self backAction];
            }
            else
            {
                [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:[result objectForKey:@"message"] input:nil];
            }
            
        } failure:^(NSError *error) {
            [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:LocalizedString(@"error_unknown", nil) input:nil];
        }];
    }
}

#pragma mark - Table view methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)aTableView
{
    return 2;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if(section == 0) return LocalizedString(@"header_type", nil);
    if(section == 1) return LocalizedString(@"header_details", nil);
    
    return nil;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UILabel *txtHeader;
    UIView *headerView;
    if (IS_IPAD) {
        txtHeader = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, tableView.bounds.size.width-20, 40)];
        txtHeader.font = [UIFont fontWithName:@"SFUIDisplay-Bold" size:18.0f];
        headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 40)];
    }
    else{
        txtHeader = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, tableView.bounds.size.width-20, 30)];
        txtHeader.font = [UIFont fontWithName:@"SFUIDisplay-Bold" size:12.0f];
        headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 30)];
    }
    
    txtHeader.textColor = UIColorFromRGB(0x8D8082);
    txtHeader.text = [self tableView:tableView titleForHeaderInSection:section];
    headerView.backgroundColor = UIColorFromRGB(0xEEE8E4);
    [headerView addSubview:txtHeader];
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (IS_IPAD) {
        return 40;
    }
    return 30;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
//        if (IS_IPAD) {
//            return 150;
//        }
//        return 120;
    }
    
    if(indexPath.section == 1)
    {
        if (selectedTypeIndex == 4) {
            if(indexPath.row == 1 ||indexPath.row==2 || indexPath.row==3) return 0;
        }
        if (selectedTypeIndex == 3) {
            if(indexPath.row == 1 || indexPath.row==3) return 0;
        }
        if (selectedTypeIndex == 2) {
            if(indexPath.row == 2 || indexPath.row==3) return 0;
        }
        if (selectedTypeIndex == 1) {
            if(indexPath.row == 2) return 0;
        }
    }
    
    return UITableViewAutomaticDimension;
}
- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
//        if (IS_IPAD) {
//            return 150;
//        }
//        return 120;
    }
    
    if(indexPath.section == 1)
    {
        if (selectedTypeIndex == 4) {
            if(indexPath.row == 1 ||indexPath.row==2 || indexPath.row==3) return 0;
        }
        if (selectedTypeIndex == 3) {
            if(indexPath.row == 1 || indexPath.row==3) return 0;
        }
        if (selectedTypeIndex == 2) {
            if(indexPath.row == 2 || indexPath.row==3) return 0;
        }
        if (selectedTypeIndex == 1) {
            if(indexPath.row == 2) return 0;
        }
    }

    return UITableViewAutomaticDimension;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

    if(indexPath.section == 1)  {
        if(indexPath.row == 0){
            // Date & Time
            NSDate *minimumDate = [[NSDate date] dateByAddingTimeInterval:-365*24*60*60];
            NSDate *maximumDate = [NSDate date];
            [ActionSheetDatePicker showPickerWithTitle:@"" datePickerMode:UIDatePickerModeDateAndTime selectedDate:selectedDateTime minimumDate:minimumDate maximumDate:maximumDate doneBlock:^(ActionSheetDatePicker *picker, id selectedDate, id origin) {
                
                editDateCheck = YES;
                selectedDateTime = selectedDate;
                NSLog(@"selectedDateTime=%@", selectedDateTime);
                if([[shareObject objectForKey:@"language"] hasPrefix:@"zh"])
                {
                    [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"zh_HK"]];
                    [formatter setDateStyle:NSDateFormatterMediumStyle];
                    [formatter setTimeStyle:NSDateFormatterShortStyle];
                }
                else
                {
                    [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
                    formatter.dateFormat = @"EE d MMM, yyyy hh:mm a";
                }
                self.txtDateTime.text = [formatter stringFromDate:selectedDateTime];
                
            } cancelBlock:^(ActionSheetDatePicker *picker) {
                
            } origin:self.view];
        }
    }
   
}

#pragma mark -
#pragma mark UIScrollViewDelegate Methods
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if([self.navigationController.visibleViewController isKindOfClass:[ActivityMedicalAddViewController class]])
    {
        CGPoint translation = [scrollView.panGestureRecognizer translationInView:scrollView.superview];
        if(translation.y > 0)
        {
            [self.view endEditing:YES];
        }
    }
}

#pragma mark - Custom Actions
-(NSString *)formatDateTime:(NSDate *)sDate
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"E d MMM, yyyy hh:mm a"];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    [dateFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
    NSString *dateStr = [dateFormatter stringFromDate:sDate];
    NSLog(@"%@",dateStr);
    return dateStr;
}

-(IBAction)didSelectAddPhoto:(UIButton *)sender
{
    if (self.btnPhoto1.hidden || self.btnPhoto2.hidden || self.btnPhoto3.hidden)
    {
        UIAlertController *alertController=[UIAlertController alertControllerWithTitle:LocalizedString(@"title_selectImage", nil) message:nil preferredStyle:UIAlertControllerStyleActionSheet];

        UIAlertAction *takePhoto=[UIAlertAction actionWithTitle:LocalizedString(@"title_camera", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self takePhotoAction];
        }];
        UIAlertAction *choosePhoto=[UIAlertAction actionWithTitle:LocalizedString(@"title_camera_roll", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self getPhotoAlbumAction];
        }];
        UIAlertAction *actionCancel=[UIAlertAction actionWithTitle:LocalizedString(@"title_cancel", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            [alertController dismissViewControllerAnimated:YES completion:nil];
        }];

        [alertController addAction:takePhoto];
        [alertController addAction:choosePhoto];
        [alertController addAction:actionCancel];
        
        if (IS_IPAD) {
            UIPopoverPresentationController *popPresenter = [alertController popoverPresentationController];
            popPresenter.sourceView = sender;
            popPresenter.sourceRect = CGRectMake(sender.bounds.origin.x, sender.bounds.size.height/2, 0, 0);
            //popPresenter.presentedViewController.preferredContentSize =  CGSizeMake(520, 400);
        }
        [self presentViewController:alertController animated:YES completion:nil];
    }
    else
    {
        [self showAlertWithTitle:@"" message:@"You can not add more than 3 photos. " input:nil];
    }
    
}

#pragma mark - UIImagePickerController Delegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    NSString *mediaType = info[UIImagePickerControllerMediaType];
    if ([mediaType isEqualToString:(NSString *)kUTTypeImage])
    {
        NSLog(@"Media is an image");
        // Handle selected image
        UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
        NSLog(@"image=%@", image);
        
        if (picker.sourceType == UIImagePickerControllerSourceTypeCamera) {
            UIImageWriteToSavedPhotosAlbum(image, self, @selector(savedPhotoImage:didFinishSavingWithError:contextInfo:), nil);
        }
        
        float scale = self.view.bounds.size.width*2/image.size.width;
        CGSize newSize = CGSizeMake(self.view.bounds.size.width*2, image.size.height*scale);
        if(image.size.width > image.size.height)
        {
            scale = self.view.bounds.size.width*2/image.size.height;
            newSize = CGSizeMake(image.size.width*scale, self.view.bounds.size.width*2);
        }
        UIGraphicsBeginImageContext(newSize);
        [image drawInRect:(CGRect){0, 0, newSize}];
        image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        if (self.btnPhoto1.hidden) {
            self.btnPhoto1.hidden = false;
            [self.btnPhoto1 setImage:image forState:UIControlStateNormal];
        }else if (self.btnPhoto2.hidden) {
            self.btnPhoto2.hidden = false;
            [self.btnPhoto2 setImage:image forState:UIControlStateNormal];
        }else if (self.btnPhoto3.hidden) {
            self.btnPhoto3.hidden = false;
            [self.btnPhoto3 setImage:image forState:UIControlStateNormal];
        }
        
        [self dismissViewControllerAnimated:NO completion:^{
        }];
    }
    else if ([mediaType isEqualToString:(NSString *)kUTTypeMovie])
    {
        BOOL ntwrkFlagStatus = [[NSUserDefaults standardUserDefaults] boolForKey:@"can_Upload_Using_Mobile_Network"];
        BOOL checkWifiStatus = [COMMON_HELPER checkforWifiNetwork];
        
        if ((checkWifiStatus==NO) && (ntwrkFlagStatus==NO)) {
            [self dismissViewControllerAnimated:NO completion:^{
            }];
            [self showAlertWithTitle:LocalizedString(@"mobile_data_usages_title", nil) message:LocalizedString(@"mobile_data_usages_message", nil) input:nil];
            return;
        }else {
            
            NSLog(@"Media is a video");
            NSURL *vUrl= (NSURL*)[info objectForKey:UIImagePickerControllerMediaURL];
            NSString *moviePath = [vUrl path];
            
            NSURL* uploadURL;
            if (self.btnPhoto1.hidden) {
                [[NSFileManager defaultManager] removeItemAtPath:[NSString stringWithFormat:@"%@video1.mov", NSTemporaryDirectory()] error:NULL];
                uploadURL = [NSURL fileURLWithPath:
                             [NSTemporaryDirectory() stringByAppendingPathComponent:@"video1.mov"]];
            }else if (self.btnPhoto2.hidden) {
                [[NSFileManager defaultManager] removeItemAtPath:[NSString stringWithFormat:@"%@video2.mov", NSTemporaryDirectory()] error:NULL];
                uploadURL = [NSURL fileURLWithPath:
                             [NSTemporaryDirectory() stringByAppendingPathComponent:@"video2.mov"]];
            }else if (self.btnPhoto3.hidden) {
                [[NSFileManager defaultManager] removeItemAtPath:[NSString stringWithFormat:@"%@video3.mov", NSTemporaryDirectory()] error:NULL];
                uploadURL = [NSURL fileURLWithPath:
                             [NSTemporaryDirectory() stringByAppendingPathComponent:@"video3.mov"]];
            }
            
            // Generate Thumbnail Image.
            UIImage *tImg = [COMMON_HELPER generateThumbnailImageFromVideoWithUrl:vUrl];
            
            [SVProgressHUD showWithStatus:LocalizedString(@"processing_video", nil)];
            [COMMON_HELPER compressVideo:vUrl outputURL:uploadURL handler:^(AVAssetExportSession *completion) {
                
                if (completion.status == AVAssetExportSessionStatusCompleted) {
                    NSData *newDataForUpload = [NSData dataWithContentsOfURL:uploadURL];
                    NSLog(@"Size of new Video after compression is (bytes):%lu",(unsigned long)[newDataForUpload length]);
                    
                    CGFloat sizeInMB = ([newDataForUpload length]/1000)/1000;
                    if (sizeInMB>15) {
                        [self showAlertWithTitleCallback:@"Large video size" message:@"Can not add this video." callback:^(UIAlertAction *action) {
                            [[NSFileManager defaultManager] removeItemAtPath:[NSString stringWithFormat:@"%@", uploadURL] error:NULL];
                        }];
                    }
                    else {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            if (self.btnPhoto1.hidden) {
                                self.btnPhoto1.hidden = false;
                                videoUrl = [uploadURL absoluteString];
                                [self.btnPhoto1 setImage:tImg forState:UIControlStateNormal];
                            }else if (self.btnPhoto2.hidden) {
                                self.btnPhoto2.hidden = false;
                                videoUrl2 = [uploadURL absoluteString];
                                [self.btnPhoto2 setImage:tImg forState:UIControlStateNormal];
                            }else if (self.btnPhoto3.hidden) {
                                self.btnPhoto3.hidden = false;
                                videoUrl3 = [uploadURL absoluteString];
                                [self.btnPhoto3 setImage:tImg forState:UIControlStateNormal];
                            }
                        });
                        
                    }
                }
                [SVProgressHUD dismiss];
                
            }];
            
            if (picker.sourceType == UIImagePickerControllerSourceTypeCamera) {
                if (UIVideoAtPathIsCompatibleWithSavedPhotosAlbum (moviePath)) {
                    UISaveVideoAtPathToSavedPhotosAlbum (moviePath, nil, nil, nil);
                }
            }
            
            [self dismissViewControllerAnimated:NO completion:^{
            }];
        }
    }
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void) savedPhotoImage:(UIImage *)image
didFinishSavingWithError:(NSError *)error
             contextInfo:(void *)contextInfo
{
    NSLog(@"This image has been saved to your camera roll");
}

- (IBAction)selectMedicalTypeAction:(UIButton*)sender {
    
    if (sender.tag == 1) {
        selectedTypeIndex = 1;
        selectedType = @"medicine";
        self.lblMedSymp.text = LocalizedString(@"txt_medicine", nil);
        self.txtMedSymp.text = @"";
        keywordColor = UIColorFromRGB(0xB6CFD1);
        
        [self.btnMedicine setSelected:YES];
        [self.btnSymptoms setSelected:NO];
        [self.btnTemperature setSelected:NO];
        [self.btnMovement setSelected:NO];
        
    }else if (sender.tag == 2){
        selectedTypeIndex = 2;
        selectedType = @"symptom";
        self.lblMedSymp.text = LocalizedString(@"txt_symptom", nil);
        self.txtMedSymp.text = @"";
        keywordColor = UIColorFromRGB(0xC4D5A1);
        
        [self.btnMedicine setSelected:NO];
        [self.btnSymptoms setSelected:YES];
        [self.btnTemperature setSelected:NO];
        [self.btnMovement setSelected:NO];

    }else if (sender.tag == 3){
        selectedTypeIndex = 3;
        selectedType = @"temperature";
        keywordColor = UIColorFromRGB(0xF9BEAF);
        
        [self.btnMedicine setSelected:NO];
        [self.btnSymptoms setSelected:NO];
        [self.btnTemperature setSelected:YES];
        [self.btnMovement setSelected:NO];

    }else if (sender.tag == 4){
        selectedTypeIndex = 4;
        selectedType = @"movement";
        keywordColor = UIColorFromRGB(0xE4C0E8);

        [self.btnMedicine setSelected:NO];
        [self.btnSymptoms setSelected:NO];
        [self.btnTemperature setSelected:NO];
        [self.btnMovement setSelected:YES];

    }
    
    NSString *str;
    if ([selectedType isEqualToString:@"medicine"]) {
        str = [keywordslistDict objectForKey:@"medicine_names"];
    }else if ([selectedType isEqualToString:@"symptom"]){
        str = [keywordslistDict objectForKey:@"symptoms_name"];
    }
    if (str.length>0) {
        medSymNameArray = [[NSMutableArray alloc] initWithArray:[str componentsSeparatedByString:@","]];
    }else {
        medSymNameArray = [[NSMutableArray alloc] init];
    }
    
    str = [keywordslistDict objectForKey:selectedType];
    if (str.length>0) {
        keywordsArray = [[NSMutableArray alloc] initWithArray:[str componentsSeparatedByString:@","]];
    }else {
        keywordsArray = [[NSMutableArray alloc] init];
    }

    [self addKeywordLabel];
    [self addMedKeywordLabel];
    [self changeAction];
    [self.tableView reloadData];
 
}

#pragma mark - Pickerview delegate
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if ([pickerView isEqual:self.numberMLPicvker])
    {
        return no1000List.count;
    }
    else if ([pickerView isEqual:self.mlPicvker])
    {
        return  doseUnitList.count;
    }
    else if ([pickerView isEqual:self.temp1Picvker])
    {
        // Temperature
        if([[unitInfo objectForKey:@"temperature"] isEqualToString:@"°C"]){
            return  temperatureCelsiusList.count;
        }else{
            return  temperatureFahrenheitList.count;
        }
        //return  no100List.count;
    }
    else {
        return  digitList.count;
    }
    
}
-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    
    if ([pickerView isEqual:self.numberMLPicvker])
    {
        return [no1000List objectAtIndex:row];
    }
    else if ([pickerView isEqual:self.mlPicvker])
    {
        return [doseUnitList objectAtIndex:row];
    }
    else if ([pickerView isEqual:self.temp1Picvker])
    {
        if([[unitInfo objectForKey:@"temperature"] isEqualToString:@"°C"]){
            return [temperatureCelsiusList objectAtIndex:row];
        }else{
            return [temperatureFahrenheitList objectAtIndex:row];
        }
        //return [no100List objectAtIndex:row];
    }
    else {
        return [digitList objectAtIndex:row];
    }
    
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view{
    
    UILabel* tView = (UILabel*)view;
    if (!tView){
        tView = [[UILabel alloc] init];
    }
    
    tView.frame = pickerView.frame;
    tView.backgroundColor = [UIColor clearColor];
    tView.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:16.0f];
    if (IS_IPAD) {
        tView.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:22.0f];
    }
    tView.textColor = UIColorFromRGB(0x786E6F);
    tView.textAlignment = NSTextAlignmentCenter;
    
    NSString *txt  = @"";
    if ([pickerView isEqual:self.numberMLPicvker])
    {
        txt = [no1000List objectAtIndex:row];
    }
    else if ([pickerView isEqual:self.mlPicvker])
    {
        txt = [doseUnitList objectAtIndex:row];
    }
    else if ([pickerView isEqual:self.temp1Picvker])
    {
        // Temperature
        if([[unitInfo objectForKey:@"temperature"] isEqualToString:@"°C"]){
            txt = [temperatureCelsiusList objectAtIndex:row];
        }else{
            txt = [temperatureFahrenheitList objectAtIndex:row];
        }
        //txt = [no100List objectAtIndex:row];
    }
    else {
        txt = [digitList objectAtIndex:row];
    }

    tView.text = txt;
    
    [[pickerView.subviews objectAtIndex:1] setBackgroundColor:[UIColor clearColor]];
    [[pickerView.subviews objectAtIndex:2] setBackgroundColor:[UIColor clearColor]];
    
    return tView;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    return pickerView.frame.size.height;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if ([pickerView isEqual:self.numberMLPicvker]) {
        editAmountCheck = YES;
        selectedVolume = [[no1000List objectAtIndex:[pickerView selectedRowInComponent:component]] floatValue];
    }
    else if ([pickerView isEqual:self.mlPicvker]) {
        editAmountCheck = YES;
        selectedVolumeUnit = [doseUnitList objectAtIndex:[pickerView selectedRowInComponent:component]];
    }
    else if ([pickerView isEqual:self.temp1Picvker]) {
        editTemperatureCheck = YES;
        
        if([[unitInfo objectForKey:@"temperature"] isEqualToString:@"°C"]){
            selectedTemprature1 = [[temperatureCelsiusList objectAtIndex:[pickerView selectedRowInComponent:component]] intValue];
        }else{
            selectedTemprature1 = [[temperatureFahrenheitList objectAtIndex:[pickerView selectedRowInComponent:component]] intValue];
        }
        //selectedTemprature1 = [[no100List objectAtIndex:[pickerView selectedRowInComponent:component]] intValue];
    }
    else if ([pickerView isEqual:self.temp2Picvker]) {
        editTemperatureCheck = YES;
        selectedTemprature2 = [[digitList objectAtIndex:[pickerView selectedRowInComponent:component]] intValue];
    }
}

#pragma mark - Keywords Implementations
-(void)doSetupForKeywordView
{
    [self.btnaddKeyword addTarget:self action:@selector(didSelectAddKeyword:) forControlEvents:UIControlEventTouchUpInside];
    
    if (IS_IPAD) {
        self.keyword1.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.keyword2.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.keyword3.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.keyword4.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.keyword5.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.keyword6.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.keyword7.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.keyword8.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.keyword9.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
    }
    
    // keyword view
    keywordsArray = [[NSMutableArray alloc] init];
    self.keywordView.backgroundColor = [UIColor clearColor];
    self.keyword1.backgroundColor = [UIColor clearColor];
    self.keyword2.backgroundColor = [UIColor clearColor];
    self.keyword3.backgroundColor = [UIColor clearColor];
    self.keyword4.backgroundColor = [UIColor clearColor];
    self.keyword5.backgroundColor = [UIColor clearColor];
    self.keyword6.backgroundColor = [UIColor clearColor];
    self.keyword7.backgroundColor = [UIColor clearColor];
    self.keyword8.backgroundColor = [UIColor clearColor];
    self.keyword9.backgroundColor = [UIColor clearColor];
    
    self.keyword1.text = @"";
    self.keyword2.text = @"";
    self.keyword3.text = @"";
    self.keyword4.text = @"";
    self.keyword5.text = @"";
    self.keyword6.text = @"";
    self.keyword7.text = @"";
    self.keyword8.text = @"";
    self.keyword9.text = @"";
    
    self.keyword1.layer.cornerRadius = 4.0f;
    self.keyword1.layer.masksToBounds = YES;
    self.keyword2.layer.cornerRadius = 4.0f;
    self.keyword2.layer.masksToBounds = YES;
    self.keyword3.layer.cornerRadius = 4.0f;
    self.keyword3.layer.masksToBounds = YES;
    self.keyword4.layer.cornerRadius = 4.0f;
    self.keyword4.layer.masksToBounds = YES;
    self.keyword5.layer.cornerRadius = 4.0f;
    self.keyword5.layer.masksToBounds = YES;
    self.keyword6.layer.cornerRadius = 4.0f;
    self.keyword5.layer.masksToBounds = YES;
    self.keyword7.layer.cornerRadius = 4.0f;
    self.keyword7.layer.masksToBounds = YES;
    self.keyword8.layer.cornerRadius = 4.0f;
    self.keyword8.layer.masksToBounds = YES;
    self.keyword9.layer.cornerRadius = 4.0f;
    self.keyword9.layer.masksToBounds = YES;
    
    self.keyword1.userInteractionEnabled = YES;
    self.keyword2.userInteractionEnabled = YES;
    self.keyword3.userInteractionEnabled = YES;
    self.keyword4.userInteractionEnabled = YES;
    self.keyword5.userInteractionEnabled = YES;
    self.keyword6.userInteractionEnabled = YES;
    self.keyword7.userInteractionEnabled = YES;
    self.keyword8.userInteractionEnabled = YES;
    self.keyword9.userInteractionEnabled = YES;
    
    self.keyword1.tag = 1;
    self.keyword2.tag = 2;
    self.keyword3.tag = 3;
    self.keyword4.tag = 4;
    self.keyword5.tag = 5;
    self.keyword6.tag = 6;
    self.keyword7.tag = 7;
    self.keyword8.tag = 8;
    self.keyword9.tag = 9;
    
    UITapGestureRecognizer *gesture1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)] ;
    UITapGestureRecognizer *gesture2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)] ;
    UITapGestureRecognizer *gesture3 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)] ;
    UITapGestureRecognizer *gesture4 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)] ;
    UITapGestureRecognizer *gesture5 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)] ;
    UITapGestureRecognizer *gesture6 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)] ;
    UITapGestureRecognizer *gesture7 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)] ;
    UITapGestureRecognizer *gesture8 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)] ;
    UITapGestureRecognizer *gesture9 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)] ;
    
    [self.keyword1 addGestureRecognizer:gesture1];
    [self.keyword2 addGestureRecognizer:gesture2];
    [self.keyword3 addGestureRecognizer:gesture3];
    [self.keyword4 addGestureRecognizer:gesture4];
    [self.keyword5 addGestureRecognizer:gesture5];
    [self.keyword6 addGestureRecognizer:gesture6];
    [self.keyword7 addGestureRecognizer:gesture7];
    [self.keyword8 addGestureRecognizer:gesture8];
    [self.keyword9 addGestureRecognizer:gesture9];
    
    UILongPressGestureRecognizer *longPressGesture1 = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongGesture:)];
    longPressGesture1.minimumPressDuration = 1.5;
    UILongPressGestureRecognizer *longPressGesture2 = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongGesture:)];
    longPressGesture2.minimumPressDuration = 1.5;
    UILongPressGestureRecognizer *longPressGesture3 = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongGesture:)];
    longPressGesture3.minimumPressDuration = 1.5;
    UILongPressGestureRecognizer *longPressGesture4 = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongGesture:)];
    longPressGesture4.minimumPressDuration = 1.5;
    UILongPressGestureRecognizer *longPressGesture5 = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongGesture:)];
    longPressGesture5.minimumPressDuration = 1.5;
    UILongPressGestureRecognizer *longPressGesture6 = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongGesture:)];
    longPressGesture6.minimumPressDuration = 1.5;
    UILongPressGestureRecognizer *longPressGesture7 = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongGesture:)];
    longPressGesture7.minimumPressDuration = 1.5;
    UILongPressGestureRecognizer *longPressGesture8 = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongGesture:)];
    longPressGesture8.minimumPressDuration = 1.5;
    UILongPressGestureRecognizer *longPressGesture9 = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongGesture:)];
    longPressGesture9.minimumPressDuration = 1.5;
    
    [self.keyword1 addGestureRecognizer:longPressGesture1];
    [self.keyword2 addGestureRecognizer:longPressGesture2];
    [self.keyword3 addGestureRecognizer:longPressGesture3];
    [self.keyword4 addGestureRecognizer:longPressGesture4];
    [self.keyword5 addGestureRecognizer:longPressGesture5];
    [self.keyword6 addGestureRecognizer:longPressGesture6];
    [self.keyword7 addGestureRecognizer:longPressGesture7];
    [self.keyword8 addGestureRecognizer:longPressGesture8];
    [self.keyword9 addGestureRecognizer:longPressGesture9];
    
}

-(void)handleTap:(UITapGestureRecognizer *)sender
{
    NSString * seletedKey = @"";
    
    if (sender.view.tag == 1) {
        seletedKey = self.keyword1.text;
    }else if (sender.view.tag == 2) {
        seletedKey = self.keyword2.text;
    }else if (sender.view.tag == 3) {
        seletedKey = self.keyword3.text;
    }else if (sender.view.tag == 4) {
        seletedKey = self.keyword4.text;
    }else if (sender.view.tag == 5) {
        seletedKey = self.keyword5.text;
    }else if (sender.view.tag == 6) {
        seletedKey = self.keyword6.text;
    }else if (sender.view.tag == 7) {
        seletedKey = self.keyword7.text;
    }else if (sender.view.tag == 8) {
        seletedKey = self.keyword8.text;
    }else if (sender.view.tag == 9) {
        seletedKey = self.keyword9.text;
    }
    
    self.txtNotes.text = [NSString stringWithFormat:@"%@ %@",self.txtNotes.text, seletedKey];
    
}

-(void)handleLongGesture:(UILongPressGestureRecognizer *)sender
{
    int tag = (int)sender.view.tag;
    CGPoint buttonPosition = [sender.view convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *indexPathToReload = [self.tableView indexPathForRowAtPoint:buttonPosition];
    
    //    if ( sender.state == UIGestureRecognizerStateEnded ) {
    
    if (keywordsArray.count>0) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Remove Keyword" message:@"Are you sure you want to remove this Keyword !" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *btnOk = [UIAlertAction actionWithTitle:LocalizedString(@"btn_ok", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            
            [keywordsArray removeObjectAtIndex:tag-1];
            [self addKeywordLabel];
            [self addKeywordsList];
            //[self.tableView reloadRowsAtIndexPaths:@[indexPathToReload] withRowAnimation:UITableViewRowAnimationBottom];
            [self.tableView reloadData];
            
        }];
        
        UIAlertAction *btnCancel = [UIAlertAction actionWithTitle:LocalizedString(@"btn_cancel", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [alert dismissViewControllerAnimated:YES completion:nil];
        }];
        [alert addAction:btnCancel];
        [alert addAction:btnOk];
        [self presentViewController:alert animated:YES completion:nil];
    }
    //    }
}

-(void)didSelectAddKeyword:(UIButton *)sender
{
    if (keywordsArray.count<9) {
        
        CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
        NSIndexPath *indexPathToReload = [self.tableView indexPathForRowAtPoint:buttonPosition];
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:@"Add Keyword" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *btnOk = [UIAlertAction actionWithTitle:LocalizedString(@"btn_ok", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            if(alert.textFields[0].text.length > 0)
            {
                [alert dismissViewControllerAnimated:YES completion:nil];
                
                currentKeyAttribute = @"notes";
                NSString *text = alert.textFields[0].text;
                [keywordsArray addObject:text];
                [self addKeywordLabel];
                [self addKeywordsList];
                //[self.tableView reloadRowsAtIndexPaths:@[indexPathToReload] withRowAnimation:UITableViewRowAnimationBottom];
                [self.tableView reloadData];
            }
            else
            {
                [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:LocalizedString(@"error_family_name", nil) input:nil];
            }
        }];
        
        UIAlertAction *btnCancel = [UIAlertAction actionWithTitle:LocalizedString(@"btn_cancel", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            
            [alert dismissViewControllerAnimated:YES completion:nil];
        }];
        
        [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
            textField.autocapitalizationType = UITextAutocapitalizationTypeWords;
        }];
        
        [alert addAction:btnCancel];
        [alert addAction:btnOk];
        
        [self presentViewController:alert animated:YES completion:nil];
        
    }
    else {
        [self showAlertWithTitle:LocalizedString(@"Keywords", nil) message:@"You can not add more than 9 keywords !" input:nil];
    }
    
}

-(void)addKeywordLabel
{
    self.keyword1.text = @"";
    self.keyword2.text = @"";
    self.keyword3.text = @"";
    self.keyword4.text = @"";
    self.keyword5.text = @"";
    self.keyword6.text = @"";
    self.keyword7.text = @"";
    self.keyword8.text = @"";
    self.keyword9.text = @"";
    
    self.keyword1.backgroundColor = [UIColor clearColor];
    self.keyword2.backgroundColor = [UIColor clearColor];
    self.keyword3.backgroundColor = [UIColor clearColor];
    self.keyword4.backgroundColor = [UIColor clearColor];
    self.keyword5.backgroundColor = [UIColor clearColor];
    self.keyword6.backgroundColor = [UIColor clearColor];
    self.keyword7.backgroundColor = [UIColor clearColor];
    self.keyword8.backgroundColor = [UIColor clearColor];
    self.keyword9.backgroundColor = [UIColor clearColor];
    
    if (keywordsArray.count>0) {
        
        for (int i=0;i<keywordsArray.count;i++) {
            if (i==0) {
                self.keyword1.text =[NSString stringWithFormat:@"%@",[keywordsArray objectAtIndex:i]];
                self.keyword1.backgroundColor = keywordColor;
            }
            else if (i==1) {
                self.keyword2.text =[NSString stringWithFormat:@"%@",[keywordsArray objectAtIndex:i]];
                self.keyword2.backgroundColor = keywordColor;
            }
            else if (i==2) {
                self.keyword3.text =[NSString stringWithFormat:@"%@",[keywordsArray objectAtIndex:i]];
                self.keyword3.backgroundColor = keywordColor;
            }
            else if (i==3) {
                self.keyword4.text =[NSString stringWithFormat:@"%@",[keywordsArray objectAtIndex:i]];
                self.keyword4.backgroundColor = keywordColor;
            }
            else if (i==4) {
                self.keyword5.text =[NSString stringWithFormat:@"%@",[keywordsArray objectAtIndex:i]];
                self.keyword5.backgroundColor = keywordColor;
            }
            else if (i==5) {
                self.keyword6.text =[NSString stringWithFormat:@"%@",[keywordsArray objectAtIndex:i]];
                self.keyword6.backgroundColor = keywordColor;
            }
            else if (i==6) {
                self.keyword7.text =[NSString stringWithFormat:@"%@",[keywordsArray objectAtIndex:i]];
                self.keyword7.backgroundColor = keywordColor;
            }
            else if (i==7) {
                self.keyword8.text =[NSString stringWithFormat:@"%@",[keywordsArray objectAtIndex:i]];
                self.keyword8.backgroundColor = keywordColor;
            }
            else if (i==8) {
                self.keyword9.text =[NSString stringWithFormat:@"%@",[keywordsArray objectAtIndex:i]];
                self.keyword9.backgroundColor = keywordColor;
            }
        }
        
    }
}

-(void)getKeywordsList
{
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setValue:@([shareObject integerForKey:@"user_id"]) forKey:@"user_id"];
    [parameters setValue:@([shareObject integerForKey:@"baby_id"]) forKey:@"baby_id"];
    [parameters setValue:@"activities" forKey:@"type"];
    [parameters setValue:@"medical" forKey:@"category"];
    
    [[WebServiceManager sharedManager] initWithBaseURL:@"api/getKeywords" parameters:parameters success:^(id result) {
        
        if ([[result valueForKey:@"success"] boolValue]) {
            NSMutableArray *list = (NSMutableArray *)[result valueForKey:@"list"];
            if(list.count > 0){
                
                keywordslistDict = [[NSDictionary alloc] initWithDictionary:[result valueForKey:@"list"]];
                NSString *str;
                if ([selectedType isEqualToString:@"medicine"]) {
                    str = [keywordslistDict objectForKey:@"medicine_names"];
                }else if ([selectedType isEqualToString:@"symptom"]){
                    str = [keywordslistDict objectForKey:@"symptoms_name"];
                }
                if (str.length>0) {
                    medSymNameArray = [[NSMutableArray alloc] initWithArray:[str componentsSeparatedByString:@","]];
                }else {
                    medSymNameArray = [[NSMutableArray alloc] init];
                }

                str = [keywordslistDict objectForKey:selectedType];
                if (str.length>0) {
                    keywordsArray = [[NSMutableArray alloc] initWithArray:[str componentsSeparatedByString:@","]];
                }else {
                    keywordsArray = [[NSMutableArray alloc] init];
                }
            }
            [self addKeywordLabel];
            [self addMedKeywordLabel];
            [self.tableView reloadData];
        }
        else {
            NSLog(@"error");
        }
        
    } failure:^(NSError *error) {
        
    }];
}

-(void)addKeywordsList
{
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setValue:@([shareObject integerForKey:@"user_id"]) forKey:@"user_id"];
    [parameters setValue:@([shareObject integerForKey:@"baby_id"]) forKey:@"baby_id"];
    [parameters setValue:@"activities" forKey:@"type"];
    [parameters setValue:@"medical" forKey:@"category"];
    [parameters setValue:selectedType forKey:@"sub_category"];
    [parameters setValue:@1 forKey:@"notes"];
    
    if (keywordsArray.count>0) {
        for(int i=0; i<keywordsArray.count; i++){
            [parameters setValue:[keywordsArray componentsJoinedByString:@","] forKey:@"keywords"];
        }
    }else{
        [parameters setValue:@"" forKey:@"keywords"];
    }

    if ([currentKeyAttribute isEqualToString:@"medicine_names"] || [currentKeyAttribute isEqualToString:@"symptoms_name"]) {
        [parameters setValue:@0 forKey:@"notes"];
        if (medSymNameArray.count>0) {
            for(int i=0; i<medSymNameArray.count; i++){
                [parameters setValue:[medSymNameArray componentsJoinedByString:@","] forKey:@"keywords"];
            }
        }else{
            [parameters setValue:@"" forKey:@"keywords"];
        }
        
    }

    [[WebServiceManager sharedManager] initWithBaseURL:@"api/setKeywords" parameters:parameters success:^(id result) {
        
        if ([[result valueForKey:@"success"] boolValue]) {
            NSMutableArray *list = (NSMutableArray *)[result valueForKey:@"list"];
            if(list.count > 0){
                
                keywordslistDict = [[NSDictionary alloc] initWithDictionary:[result valueForKey:@"list"]];
                NSString *str;
                if ([selectedType isEqualToString:@"medicine"]) {
                    str = [keywordslistDict objectForKey:@"medicine_names"];
                }else if ([selectedType isEqualToString:@"symptom"]){
                    str = [keywordslistDict objectForKey:@"symptoms_name"];
                }
                if (str.length>0) {
                    medSymNameArray = [[NSMutableArray alloc] initWithArray:[str componentsSeparatedByString:@","]];
                }else {
                    medSymNameArray = [[NSMutableArray alloc] init];
                }
                
                str = [keywordslistDict objectForKey:selectedType];
                if (str.length>0) {
                    keywordsArray = [[NSMutableArray alloc] initWithArray:[str componentsSeparatedByString:@","]];
                }else {
                    keywordsArray = [[NSMutableArray alloc] init];
                }

                //keywordsArray = list;
                //[list removeAllObjects];
            }
        }
        else {
            NSLog(@"error");
        }
        
    } failure:^(NSError *error) {
        
    }];
    
}

#pragma mark - Medical Keywords Implementations
-(void)doSetupForMedKeywordView
{
    [self.btnaddKeywordMed addTarget:self action:@selector(didSelectAddMedKeyword:) forControlEvents:UIControlEventTouchUpInside];
    
    if (IS_IPAD) {
        self.medKeyword1.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.medKeyword2.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.medKeyword3.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.medKeyword4.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.medKeyword5.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.medKeyword6.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.medKeyword7.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.medKeyword8.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.medKeyword9.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
    }
    
    // keyword view
    medSymNameArray = [[NSMutableArray alloc] init];
    self.medKeywordView.backgroundColor = [UIColor clearColor];
    self.medKeyword1.backgroundColor = [UIColor clearColor];
    self.medKeyword2.backgroundColor = [UIColor clearColor];
    self.medKeyword3.backgroundColor = [UIColor clearColor];
    self.medKeyword4.backgroundColor = [UIColor clearColor];
    self.medKeyword5.backgroundColor = [UIColor clearColor];
    self.medKeyword6.backgroundColor = [UIColor clearColor];
    self.medKeyword7.backgroundColor = [UIColor clearColor];
    self.medKeyword8.backgroundColor = [UIColor clearColor];
    self.medKeyword9.backgroundColor = [UIColor clearColor];
    
    self.medKeyword1.text = @"";
    self.medKeyword2.text = @"";
    self.medKeyword3.text = @"";
    self.medKeyword4.text = @"";
    self.medKeyword5.text = @"";
    self.medKeyword6.text = @"";
    self.medKeyword7.text = @"";
    self.medKeyword8.text = @"";
    self.medKeyword9.text = @"";
    
    self.medKeyword1.layer.cornerRadius = 4.0f;
    self.medKeyword1.layer.masksToBounds = YES;
    self.medKeyword2.layer.cornerRadius = 4.0f;
    self.medKeyword2.layer.masksToBounds = YES;
    self.medKeyword3.layer.cornerRadius = 4.0f;
    self.medKeyword3.layer.masksToBounds = YES;
    self.medKeyword4.layer.cornerRadius = 4.0f;
    self.medKeyword4.layer.masksToBounds = YES;
    self.medKeyword5.layer.cornerRadius = 4.0f;
    self.medKeyword5.layer.masksToBounds = YES;
    self.medKeyword6.layer.cornerRadius = 4.0f;
    self.medKeyword5.layer.masksToBounds = YES;
    self.medKeyword7.layer.cornerRadius = 4.0f;
    self.medKeyword7.layer.masksToBounds = YES;
    self.medKeyword8.layer.cornerRadius = 4.0f;
    self.medKeyword8.layer.masksToBounds = YES;
    self.medKeyword9.layer.cornerRadius = 4.0f;
    self.medKeyword9.layer.masksToBounds = YES;
    
    self.medKeyword1.userInteractionEnabled = YES;
    self.medKeyword2.userInteractionEnabled = YES;
    self.medKeyword3.userInteractionEnabled = YES;
    self.medKeyword4.userInteractionEnabled = YES;
    self.medKeyword5.userInteractionEnabled = YES;
    self.medKeyword6.userInteractionEnabled = YES;
    self.medKeyword7.userInteractionEnabled = YES;
    self.medKeyword8.userInteractionEnabled = YES;
    self.medKeyword9.userInteractionEnabled = YES;
    
    self.medKeyword1.tag = 1;
    self.medKeyword2.tag = 2;
    self.medKeyword3.tag = 3;
    self.medKeyword4.tag = 4;
    self.medKeyword5.tag = 5;
    self.medKeyword6.tag = 6;
    self.medKeyword7.tag = 7;
    self.medKeyword8.tag = 8;
    self.medKeyword9.tag = 9;
    
    UITapGestureRecognizer *gesture11 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleMedTap:)] ;
    UITapGestureRecognizer *gesture22 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleMedTap:)] ;
    UITapGestureRecognizer *gesture33 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleMedTap:)] ;
    UITapGestureRecognizer *gesture44 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleMedTap:)] ;
    UITapGestureRecognizer *gesture55 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleMedTap:)] ;
    UITapGestureRecognizer *gesture66 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleMedTap:)] ;
    UITapGestureRecognizer *gesture77 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleMedTap:)] ;
    UITapGestureRecognizer *gesture88 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleMedTap:)] ;
    UITapGestureRecognizer *gesture99 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleMedTap:)] ;
    
    [self.medKeyword1 addGestureRecognizer:gesture11];
    [self.medKeyword2 addGestureRecognizer:gesture22];
    [self.medKeyword3 addGestureRecognizer:gesture33];
    [self.medKeyword4 addGestureRecognizer:gesture44];
    [self.medKeyword5 addGestureRecognizer:gesture55];
    [self.medKeyword6 addGestureRecognizer:gesture66];
    [self.medKeyword7 addGestureRecognizer:gesture77];
    [self.medKeyword8 addGestureRecognizer:gesture88];
    [self.medKeyword9 addGestureRecognizer:gesture99];
    
    UILongPressGestureRecognizer *longPressGesture11 = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleMedLongGesture:)];
    longPressGesture11.minimumPressDuration = 1.5;
    UILongPressGestureRecognizer *longPressGesture22 = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleMedLongGesture:)];
    longPressGesture22.minimumPressDuration = 1.5;
    UILongPressGestureRecognizer *longPressGesture33 = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleMedLongGesture:)];
    longPressGesture33.minimumPressDuration = 1.5;
    UILongPressGestureRecognizer *longPressGesture44 = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleMedLongGesture:)];
    longPressGesture44.minimumPressDuration = 1.5;
    UILongPressGestureRecognizer *longPressGesture55 = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleMedLongGesture:)];
    longPressGesture55.minimumPressDuration = 1.5;
    UILongPressGestureRecognizer *longPressGesture66 = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleMedLongGesture:)];
    longPressGesture66.minimumPressDuration = 1.5;
    UILongPressGestureRecognizer *longPressGesture77 = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleMedLongGesture:)];
    longPressGesture77.minimumPressDuration = 1.5;
    UILongPressGestureRecognizer *longPressGesture88 = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleMedLongGesture:)];
    longPressGesture88.minimumPressDuration = 1.5;
    UILongPressGestureRecognizer *longPressGesture99 = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleMedLongGesture:)];
    longPressGesture99.minimumPressDuration = 1.5;
    
    [self.medKeyword1 addGestureRecognizer:longPressGesture11];
    [self.medKeyword2 addGestureRecognizer:longPressGesture22];
    [self.medKeyword3 addGestureRecognizer:longPressGesture33];
    [self.medKeyword4 addGestureRecognizer:longPressGesture44];
    [self.medKeyword5 addGestureRecognizer:longPressGesture55];
    [self.medKeyword6 addGestureRecognizer:longPressGesture66];
    [self.medKeyword7 addGestureRecognizer:longPressGesture77];
    [self.medKeyword8 addGestureRecognizer:longPressGesture88];
    [self.medKeyword9 addGestureRecognizer:longPressGesture99];
    
}

-(void)handleMedTap:(UITapGestureRecognizer *)sender
{
    NSString * seletedKey = @"";
    
    if (sender.view.tag == 1) {
        seletedKey = self.medKeyword1.text;
    }else if (sender.view.tag == 2) {
        seletedKey = self.medKeyword2.text;
    }else if (sender.view.tag == 3) {
        seletedKey = self.medKeyword3.text;
    }else if (sender.view.tag == 4) {
        seletedKey = self.medKeyword4.text;
    }else if (sender.view.tag == 5) {
        seletedKey = self.medKeyword5.text;
    }else if (sender.view.tag == 6) {
        seletedKey = self.medKeyword6.text;
    }else if (sender.view.tag == 7) {
        seletedKey = self.medKeyword7.text;
    }else if (sender.view.tag == 8) {
        seletedKey = self.medKeyword8.text;
    }else if (sender.view.tag == 9) {
        seletedKey = self.medKeyword9.text;
    }
    self.txtMedSymp.text = [NSString stringWithFormat:@"%@", seletedKey];
    //self.txtMedSymp.text = [NSString stringWithFormat:@"%@ %@",self.txtMedSymp.text, seletedKey];
    
}

-(void)handleMedLongGesture:(UILongPressGestureRecognizer *)sender
{
    int tag = (int)sender.view.tag;
    CGPoint buttonPosition = [sender.view convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *indexPathToReload = [self.tableView indexPathForRowAtPoint:buttonPosition];
    
    //    if ( sender.state == UIGestureRecognizerStateEnded ) {
    
    if (medSymNameArray.count>0) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Remove Keyword" message:@"Are you sure you want to remove this Keyword !" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *btnOk = [UIAlertAction actionWithTitle:LocalizedString(@"btn_ok", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            
            [medSymNameArray removeObjectAtIndex:tag-1];
            [self addMedKeywordLabel];
            [self addKeywordsList];
            //[self.tableView reloadRowsAtIndexPaths:@[indexPathToReload] withRowAnimation:UITableViewRowAnimationBottom];
            [self.tableView reloadData];
            
        }];
        
        UIAlertAction *btnCancel = [UIAlertAction actionWithTitle:LocalizedString(@"btn_cancel", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [alert dismissViewControllerAnimated:YES completion:nil];
        }];
        [alert addAction:btnCancel];
        [alert addAction:btnOk];
        [self presentViewController:alert animated:YES completion:nil];
    }
    //    }
}

-(void)didSelectAddMedKeyword:(UIButton *)sender
{
    if (medSymNameArray.count<9) {
        
        CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
        NSIndexPath *indexPathToReload = [self.tableView indexPathForRowAtPoint:buttonPosition];
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:@"Add Keyword" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *btnOk = [UIAlertAction actionWithTitle:LocalizedString(@"btn_ok", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            if(alert.textFields[0].text.length > 0)
            {
                [alert dismissViewControllerAnimated:YES completion:nil];
                
                if ([selectedType isEqualToString:@"medicine"]) {
                    currentKeyAttribute = @"medicine_names";
                }else if ([selectedType isEqualToString:@"symptom"]) {
                    currentKeyAttribute = @"symptoms_name";
                }
                NSString *text = alert.textFields[0].text;
                [medSymNameArray addObject:text];
                [self addMedKeywordLabel];
                [self addKeywordsList];
                //[self.tableView reloadRowsAtIndexPaths:@[indexPathToReload] withRowAnimation:UITableViewRowAnimationBottom];
                [self.tableView reloadData];
            }
            else
            {
                [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:LocalizedString(@"error_family_name", nil) input:nil];
            }
        }];
        
        UIAlertAction *btnCancel = [UIAlertAction actionWithTitle:LocalizedString(@"btn_cancel", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            
            [alert dismissViewControllerAnimated:YES completion:nil];
        }];
        
        [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
            textField.autocapitalizationType = UITextAutocapitalizationTypeWords;
        }];
        
        [alert addAction:btnCancel];
        [alert addAction:btnOk];
        
        [self presentViewController:alert animated:YES completion:nil];
        
    }
    else {
        [self showAlertWithTitle:LocalizedString(@"Keywords", nil) message:@"You can not add more than 9 keywords !" input:nil];
    }
    
}

-(void)addMedKeywordLabel
{
    self.medKeyword1.text = @"";
    self.medKeyword2.text = @"";
    self.medKeyword3.text = @"";
    self.medKeyword4.text = @"";
    self.medKeyword5.text = @"";
    self.medKeyword6.text = @"";
    self.medKeyword7.text = @"";
    self.medKeyword8.text = @"";
    self.medKeyword9.text = @"";
    
    self.medKeyword1.backgroundColor = [UIColor clearColor];
    self.medKeyword2.backgroundColor = [UIColor clearColor];
    self.medKeyword3.backgroundColor = [UIColor clearColor];
    self.medKeyword4.backgroundColor = [UIColor clearColor];
    self.medKeyword5.backgroundColor = [UIColor clearColor];
    self.medKeyword6.backgroundColor = [UIColor clearColor];
    self.medKeyword7.backgroundColor = [UIColor clearColor];
    self.medKeyword8.backgroundColor = [UIColor clearColor];
    self.medKeyword9.backgroundColor = [UIColor clearColor];
    
    if (medSymNameArray.count>0) {
        
        for (int i=0;i<medSymNameArray.count;i++) {
            if (i==0) {
                self.medKeyword1.text =[NSString stringWithFormat:@"%@",[medSymNameArray objectAtIndex:i]];
                self.medKeyword1.backgroundColor = keywordColor;
            }
            else if (i==1) {
                self.medKeyword2.text =[NSString stringWithFormat:@"%@",[medSymNameArray objectAtIndex:i]];
                self.medKeyword2.backgroundColor = keywordColor;
            }
            else if (i==2) {
                self.medKeyword3.text =[NSString stringWithFormat:@"%@",[medSymNameArray objectAtIndex:i]];
                self.medKeyword3.backgroundColor = keywordColor;
            }
            else if (i==3) {
                self.medKeyword4.text =[NSString stringWithFormat:@"%@",[medSymNameArray objectAtIndex:i]];
                self.medKeyword4.backgroundColor = keywordColor;
            }
            else if (i==4) {
                self.medKeyword5.text =[NSString stringWithFormat:@"%@",[medSymNameArray objectAtIndex:i]];
                self.medKeyword5.backgroundColor = keywordColor;
            }
            else if (i==5) {
                self.medKeyword6.text =[NSString stringWithFormat:@"%@",[medSymNameArray objectAtIndex:i]];
                self.medKeyword6.backgroundColor = keywordColor;
            }
            else if (i==6) {
                self.medKeyword7.text =[NSString stringWithFormat:@"%@",[medSymNameArray objectAtIndex:i]];
                self.medKeyword7.backgroundColor = keywordColor;
            }
            else if (i==7) {
                self.medKeyword8.text =[NSString stringWithFormat:@"%@",[medSymNameArray objectAtIndex:i]];
                self.medKeyword8.backgroundColor = keywordColor;
            }
            else if (i==8) {
                self.medKeyword9.text =[NSString stringWithFormat:@"%@",[medSymNameArray objectAtIndex:i]];
                self.medKeyword9.backgroundColor = keywordColor;
            }
        }
        
    }
}

@end
