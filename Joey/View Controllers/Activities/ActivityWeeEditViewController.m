//
//  ActivityWeeEditViewController.m
//  Joey
//
//  Created by csl on 5/1/2017.
//  Copyright © 2017 Auxilia. All rights reserved.
//

#import "AppDelegate.h"
#import "ActivityWeeEditViewController.h"

@interface ActivityWeeEditViewController ()
{
    NSUserDefaults *shareObject;
    AppDelegate *appDelegate;
    NSMutableDictionary *unitInfo;
    NSMutableDictionary *weeInfo;
    NSMutableDictionary *activityInfo;
    NSDateFormatter *formatter;
    NSMutableArray *no100List;
    NSMutableArray *no1000List;
    NSMutableArray *no24List;
    NSMutableArray *no60List;
    NSMutableArray *digitList;
    NSString *selectedColor;
    NSString *selectedSize;
    NSDate *selectedDateTime;
    UIImage *imgUploadPhoto;
    
    BOOL saveCheck;
    NSMutableArray *keywordsArray;
    NSDictionary *keywordslistDict;
    NSString *videoUrl;
    NSString *videoUrl2;
    NSString *videoUrl3;
    BOOL havePhotoUrl;
    BOOL havePhotoUrl2;
    BOOL havePhotoUrl3;
}
@end

@implementation ActivityWeeEditViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    shareObject = [NSUserDefaults standardUserDefaults];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    formatter = [[NSDateFormatter alloc] init];
    [formatter setTimeZone:[NSTimeZone localTimeZone]];
    [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
    
    unitInfo = (NSMutableDictionary *)[NSKeyedUnarchiver unarchiveObjectWithData:[shareObject objectForKey:@"unit_info"]];
    weeInfo = (NSMutableDictionary *)[NSKeyedUnarchiver unarchiveObjectWithData:[shareObject objectForKey:@"wee_info"]];
    activityInfo = [self.data objectForKey:@"activity_info"];
    NSLog(@"activityInfo=%@", activityInfo);
    
    no100List = [[NSMutableArray alloc] init];
    for(int i=0; i<=100; i++) [no100List addObject:[NSString stringWithFormat:@"%d", i]];
    
    no1000List = [[NSMutableArray alloc] init];
    for(int i=0; i<=1000; i++) [no1000List addObject:[NSString stringWithFormat:@"%d", i]];
    
    no24List = [[NSMutableArray alloc] init];
    for(int i=0; i<24; i++) [no24List addObject:[NSString stringWithFormat:@"%d", i]];
    
    no60List = [[NSMutableArray alloc] init];
    for(int i=0; i<60; i++) [no60List addObject:[NSString stringWithFormat:@"%d", i]];
    
    digitList = [[NSMutableArray alloc] init];
    for(int i=0; i<10; i++) [digitList addObject:[NSString stringWithFormat:@"%d", i]];
    
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:UIColorFromRGB(0x8D8082), NSForegroundColorAttributeName, [UIFont fontWithName:@"SFUIDisplay-Medium" size:18.0f], NSFontAttributeName, nil]];
    [self.navigationController.navigationBar setBarTintColor:UIColorFromRGB(0xCADAA9)];
    [self.navigationController.navigationBar setTranslucent:NO];
    
    [self.navigationItem setTitle:LocalizedString(@"title_edit_wee", nil)];
    [self customBackButton];
    
    self.btnSave.title = LocalizedString(@"btn_save", nil);
    
    [self.btnDelete setTitle:LocalizedString(@"btn_delete", nil) forState:UIControlStateNormal];
    
    // Photo Picking & Display
    self.btnPhoto1.hidden = YES;
    self.btnPhoto2.hidden = YES;
    self.btnPhoto3.hidden = YES;
    self.btnPhoto1.tag = 1;
    self.btnPhoto2.tag = 2;
    self.btnPhoto3.tag = 3;
    [self.btnPhoto1 addTarget:self action:@selector(showPhotoAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.btnPhoto2 addTarget:self action:@selector(showPhotoAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.btnPhoto3 addTarget:self action:@selector(showPhotoAction:) forControlEvents:UIControlEventTouchUpInside];
    
    NSString *photoUrl = [activityInfo objectForKey:@"photo_url"];
    videoUrl = [activityInfo objectForKey:@"video_url"];
    if((photoUrl != nil)&&(![photoUrl isEqual:[NSNull null]])&&(photoUrl.length>0))
    {
        self.btnPhoto1.hidden = NO;
        havePhotoUrl = YES;
        [self.btnPhoto1 sd_setImageWithURL:[NSURL URLWithString:photoUrl] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"dummy.png"]];
    }
    else if ((videoUrl != nil)&&(![videoUrl isEqual:[NSNull null]])&&(videoUrl.length>0))
    {
        //        [self.btnPhoto1 setImage:[UIImage imageNamed:@"video_player_icon"] forState:UIControlStateNormal];
        //        self.btnPhoto1.backgroundColor = [UIColor darkTextColor];
        //        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        //            UIImage *thImg = [COMMON_HELPER generateThumbnailImageFromVideoWithUrl:[NSURL URLWithString:videoUrl]];
        //            dispatch_async(dispatch_get_main_queue(), ^(void){
        //                [self.btnPhoto1 setImage:thImg forState:UIControlStateNormal];
        //            });
        //        });
        [COMMON_HELPER generateThumbnailImageFromVideoWithUrl:[NSURL URLWithString:videoUrl] inView:self.btnPhoto1];
        self.btnPhoto1.hidden = NO;
    }
    
    NSString *photoUrl2 = [activityInfo objectForKey:@"photo_url2"];
    videoUrl2 = [activityInfo objectForKey:@"video_url2"];
    if((photoUrl2 != nil)&&(![photoUrl2 isEqual:[NSNull null]])&&(photoUrl2.length>0))
    {
        self.btnPhoto2.hidden = NO;
        havePhotoUrl2 = YES;
        [self.btnPhoto2 sd_setImageWithURL:[NSURL URLWithString:photoUrl2] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"dummy.png"]];
    }
    else if ((videoUrl2 != nil)&&(![videoUrl2 isEqual:[NSNull null]])&&(videoUrl2.length>0))
    {
        //        [self.btnPhoto2 setImage:[UIImage imageNamed:@"video_player_icon"] forState:UIControlStateNormal];
        //        self.btnPhoto2.backgroundColor = [UIColor darkTextColor];
        //        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        //            UIImage *thImg = [COMMON_HELPER generateThumbnailImageFromVideoWithUrl:[NSURL URLWithString:videoUrl2]];
        //            dispatch_async(dispatch_get_main_queue(), ^(void){
        //                [self.btnPhoto2 setImage:thImg forState:UIControlStateNormal];
        //            });
        //        });
        [COMMON_HELPER generateThumbnailImageFromVideoWithUrl:[NSURL URLWithString:videoUrl2] inView:self.btnPhoto2];
        self.btnPhoto2.hidden = NO;
    }
    
    NSString *photoUrl3 = [activityInfo objectForKey:@"photo_url3"];
    videoUrl3 = [activityInfo objectForKey:@"video_url3"];
    if((photoUrl3 != nil)&&(![photoUrl3 isEqual:[NSNull null]])&&(photoUrl3.length>0))
    {
        self.btnPhoto3.hidden = NO;
        havePhotoUrl3 = YES;
        [self.btnPhoto3 sd_setImageWithURL:[NSURL URLWithString:photoUrl3] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"dummy.png"]];
    }
    else if ((videoUrl3 != nil)&&(![videoUrl3 isEqual:[NSNull null]])&&(videoUrl3.length>0))
    {
        //        [self.btnPhoto3 setImage:[UIImage imageNamed:@"video_player_icon"] forState:UIControlStateNormal];
        //        self.btnPhoto3.backgroundColor = [UIColor darkTextColor];
        //        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        //            UIImage *thImg = [COMMON_HELPER generateThumbnailImageFromVideoWithUrl:[NSURL URLWithString:videoUrl3]];
        //            dispatch_async(dispatch_get_main_queue(), ^(void){
        //                [self.btnPhoto3 setImage:thImg forState:UIControlStateNormal];
        //            });
        //        });
        [COMMON_HELPER generateThumbnailImageFromVideoWithUrl:[NSURL URLWithString:videoUrl3] inView:self.btnPhoto3];
        self.btnPhoto3.hidden = NO;
    }

    NSString *notes = [activityInfo objectForKey:@"notes"];
    if(![notes isEqual:[NSNull null]]) self.txtNotes.text = notes;
    
    self.lblSize.text = LocalizedString(@"txt_size", nil);
    self.lblColor.text = LocalizedString(@"txt_color", nil);
    self.lblDateTime.text = LocalizedString(@"txt_datetime", nil);
    self.lblPhoto.text = LocalizedString(@"txt_photo", nil);
    self.lblNotes.text = LocalizedString(@"txt_notes", nil);
    
    if (IS_IPAD) {
        self.lblSize.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:22.0f];
        self.lblColor.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:22.0f];
        self.lblDateTime.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:22.0f];
        self.lblPhoto.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:22.0f];
        self.lblNotes.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:22.0f];
        
        self.txtDateTime.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:22.0f];
        self.txtNotes.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:22.0f];
        
        self.btnDelete.titleLabel.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:25.0f];
        
        self.btnSizeSmall.titleLabel.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f];
        self.btnSizeMedium.titleLabel.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f];
        self.btnSizeLarge.titleLabel.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f];
    }
    
    self.btnSizeSmall.layer.cornerRadius = 3.f;
    self.btnSizeMedium.layer.cornerRadius = 3.f;
    self.btnSizeLarge.layer.cornerRadius = 3.f;
    [self.btnSizeSmall setTitle:LocalizedString(@"small", nil) forState:UIControlStateNormal];
    [self.btnSizeMedium setTitle:LocalizedString(@"medium", nil) forState:UIControlStateNormal];
    [self.btnSizeLarge setTitle:LocalizedString(@"large", nil) forState:UIControlStateNormal];
    
    self.btnColorClear.layer.cornerRadius = self.btnColorClear.frame.size.width/2.f;
    self.btnColorClear.layer.borderColor = UIColorFromRGB(0xD3D3D3).CGColor;
    self.btnColorClear.layer.borderWidth = 0.5f;
    self.btnColorClear.backgroundColor = UIColorFromRGB(0xFFFFFF);
    self.btnColorlightYellow.layer.cornerRadius = self.btnColorlightYellow.frame.size.width/2.f;
    self.btnColorlightYellow.backgroundColor =   UIColorFromRGB(0xF5EBB8);
    self.btnColorYellow.layer.cornerRadius = self.btnColorYellow.frame.size.width/2.f;
    self.btnColorYellow.backgroundColor = UIColorFromRGB(0xFDE086);
    self.btnColorOrange.layer.cornerRadius = self.btnColorOrange.frame.size.width/2.f;
    self.btnColorOrange.backgroundColor = UIColorFromRGB(0xF49B5C);
    self.btnColorAlert.layer.cornerRadius = self.btnColorAlert.frame.size.width/2.f;
    self.btnColorAlert.backgroundColor = UIColorFromRGB(0xF07861);
    
    selectedSize = [activityInfo objectForKey:@"volume"];
    if([selectedSize isEqualToString:@"small"]) [self.btnSizeSmall sendActionsForControlEvents:UIControlEventTouchUpInside];
    if([selectedSize isEqualToString:@"medium"]) [self.btnSizeMedium sendActionsForControlEvents:UIControlEventTouchUpInside];
    if([selectedSize isEqualToString:@"large"]) [self.btnSizeLarge sendActionsForControlEvents:UIControlEventTouchUpInside];
    
    selectedColor = [activityInfo objectForKey:@"color"];
    if([selectedColor isEqualToString:@"white"])
        [self.btnColorClear sendActionsForControlEvents:UIControlEventTouchUpInside];
    if([selectedColor isEqualToString:@"yellow"]) 
        [self.btnColorYellow sendActionsForControlEvents:UIControlEventTouchUpInside];
    if([selectedColor isEqualToString:@"lightyellow"])
        [self.btnColorlightYellow  sendActionsForControlEvents:UIControlEventTouchUpInside];
    if([selectedColor isEqualToString:@"orange"])
        [self.btnColorOrange sendActionsForControlEvents:UIControlEventTouchUpInside];
    if([selectedColor isEqualToString:@"alert"])
        [self.btnColorAlert sendActionsForControlEvents:UIControlEventTouchUpInside];
    
    formatter.dateFormat = @"yyyy-MM-dd hh:mm a";
    selectedDateTime = [formatter dateFromString:[NSString stringWithFormat:@"%@ %@", [activityInfo objectForKey:@"date"], [activityInfo objectForKey:@"time"]]];
    
    if([[shareObject objectForKey:@"language"] hasPrefix:@"zh"])
    {
        [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"zh_HK"]];
        [formatter setDateStyle:NSDateFormatterMediumStyle];
        [formatter setTimeStyle:NSDateFormatterShortStyle];
    }
    else
    {
        [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
        formatter.dateFormat = @"EE d MMM, yyyy hh:mm a";
    }
    self.txtDateTime.text = [NSString stringWithFormat:@"%@", [formatter stringFromDate:selectedDateTime]];
    
    NSLog(@"selectedDateTime=%@", selectedDateTime);
    
    [self doSetupForKeywordView];
    [self getKeywordsList];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:(BOOL)animated];
    
    NSLog(@"self.data=%@", self.data);
    if([[self.data objectForKey:@"deleteCheck"] boolValue])
    {
        if ([[self.data objectForKey:@"imageTag"] integerValue] == 1) {
            self.btnPhoto1.hidden = YES;
            [self.data removeObjectForKey:@"imageTag"];
            videoUrl = nil;
            havePhotoUrl = NO;
        }else if ([[self.data objectForKey:@"imageTag"] integerValue] == 2) {
            self.btnPhoto2.hidden = YES;
            [self.data removeObjectForKey:@"imageTag"];
            videoUrl2 = nil;
            havePhotoUrl2 = NO;
        }else if ([[self.data objectForKey:@"imageTag"] integerValue] == 3) {
            self.btnPhoto3.hidden = YES;
            [self.data removeObjectForKey:@"imageTag"];
            videoUrl3 = nil;
            havePhotoUrl3 = NO;
        }
        
        [self.data removeObjectForKey:@"deleteCheck"];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (BOOL)prefersStatusBarHidden
{
    return NO;
}

- (void)customBackButton
{
    if([self.navigationController.viewControllers count] > 1)
    {
        UIImage *imgBack = [UIImage imageNamed:@"nav_btn_back"];
        UIButton *btnBack = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnBack setImage:imgBack forState:UIControlStateNormal];
        btnBack.frame = CGRectMake(0, 0, imgBack.size.width, imgBack.size.height);
        [btnBack addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
        [btnBack setImageEdgeInsets:UIEdgeInsetsMake(0, -16, 0, 16)];
        UIBarButtonItem *backBarItem = [[UIBarButtonItem alloc] initWithCustomView:btnBack];
        self.navigationItem.leftBarButtonItem = backBarItem;
    }
}

- (void)backAction
{
    [COMMON_HELPER clearTmpDirectory];
    UIViewController *viewController = [self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count-2];
    viewController.data = [[NSMutableDictionary alloc] initWithDictionary:@{@"save_check":@(saveCheck)}];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)showCameraAction
{
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    if ([[NSUserDefaults standardUserDefaults]boolForKey:@"is_Subscription_Enabled"]) {
        imagePicker.mediaTypes = [[NSArray alloc] initWithObjects: (NSString *) kUTTypeMovie, (NSString *) kUTTypeVideo, (NSString *) kUTTypeImage, nil];
        imagePicker.videoQuality = UIImagePickerControllerQualityTypeLow;
        imagePicker.videoMaximumDuration = 120;
    }else {
        imagePicker.mediaTypes = [[NSArray alloc] initWithObjects: (NSString *) kUTTypeImage, nil];
    }
    
    [self presentViewController:imagePicker animated:NO completion:nil];
}

- (void)showPhotoAlbumAction
{
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    if ([[NSUserDefaults standardUserDefaults]boolForKey:@"is_Subscription_Enabled"]) {
        imagePicker.mediaTypes = [[NSArray alloc] initWithObjects: (NSString *) kUTTypeMovie, (NSString *) kUTTypeVideo, (NSString *) kUTTypeImage, nil];
    }else {
        imagePicker.mediaTypes = [[NSArray alloc] initWithObjects: (NSString *) kUTTypeImage, nil];
    }
    
    [self presentViewController:imagePicker animated:NO completion:nil];
}

- (IBAction)showPhotoAction:(UIButton *)sender
{
    if (sender.hidden)
        return;
    
    UIImage *img;
    NSString *vStr;
    if (sender.tag == 1) {
        if (videoUrl && videoUrl.length>0) {
            vStr = videoUrl;
        }else {
            img = self.btnPhoto1.imageView.image;
        }
    }else if (sender.tag == 2) {
        if (videoUrl2 && videoUrl2.length>0) {
            vStr = videoUrl2;
        }else {
            img = self.btnPhoto2.imageView.image;
        }
    }else if (sender.tag == 3) {
        if (videoUrl3 && videoUrl3.length>0) {
            vStr = videoUrl3;
        }else {
            img = self.btnPhoto3.imageView.image;
        }
    }
    
    if (vStr && vStr.length>0) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"VideoViewController"];
        viewController.data = [[NSMutableDictionary alloc] initWithDictionary:@{@"viewController":self, @"video":vStr,@"imageTag":@(sender.tag)}];
        [self presentViewController:viewController animated:YES completion:nil];
    }
    else
    {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"PhotoViewController"];
        viewController.data = [[NSMutableDictionary alloc] initWithDictionary:@{@"viewController":self, @"image":img,@"imageTag":@(sender.tag)}];
        [self presentViewController:viewController animated:YES completion:nil];
    }
    
}

- (void)takePhotoAction
{
    AVAuthorizationStatus status = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    if(status == AVAuthorizationStatusDenied || status == AVAuthorizationStatusRestricted)
    {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:LocalizedString(@"error_camera", nil) message:@"" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *btnOk = [UIAlertAction actionWithTitle:LocalizedString(@"btn_ok", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            
            [alert dismissViewControllerAnimated:YES completion:nil];
        }];
        
        [alert addAction:btnOk];
        
        [self presentViewController:alert animated:YES completion:nil];
        
    }
    else
    {
        [self showCameraAction];
    }
}

- (void)getPhotoAlbumAction
{
    PHAuthorizationStatus status = [PHPhotoLibrary authorizationStatus];
    if(status == PHAuthorizationStatusDenied || status == PHAuthorizationStatusRestricted)
    {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:LocalizedString(@"error_photo", nil) message:@"" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *btnOk = [UIAlertAction actionWithTitle:LocalizedString(@"btn_ok", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            
            [alert dismissViewControllerAnimated:YES completion:nil];
        }];
        
        [alert addAction:btnOk];
        
        [self presentViewController:alert animated:YES completion:nil];
        
    }
    else
    {
        [self showPhotoAlbumAction];
    }
}

- (IBAction)selectSmallAction:(id)sender
{
    selectedSize = @"small";
    [self.btnSizeSmall setBackgroundColor:UIColorFromRGB(0x8D8082)];
    [self.btnSizeMedium setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnSizeLarge setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
}

- (IBAction)selectMediumAction:(id)sender
{
    selectedSize = @"medium";
    [self.btnSizeSmall setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnSizeMedium setBackgroundColor:UIColorFromRGB(0x8D8082)];
    [self.btnSizeLarge setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
}

- (IBAction)selectLargeAction:(id)sender
{
    selectedSize = @"large";
    [self.btnSizeSmall setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnSizeMedium setBackgroundColor:UIColorFromRGB(0xE2D3CA)];
    [self.btnSizeLarge setBackgroundColor:UIColorFromRGB(0x8D8082)];
}

- (IBAction)selectClearAction:(id)sender
{
    selectedColor = @"white";
    self.btnColorlightYellow.transform = self.btnColorClear.transform = self.btnColorYellow.transform = self.btnColorOrange.transform = self.btnColorAlert.transform = CGAffineTransformMakeScale(1, 1);
    self.btnColorClear.transform = CGAffineTransformMakeScale(1.5, 1.5);
}

- (IBAction)selectlightYellowAction:(id)sender
{
    selectedColor = @"lightyellow";
    self.btnColorClear.transform = self.btnColorYellow.transform = self.btnColorOrange.transform = self.btnColorAlert.transform = CGAffineTransformMakeScale(1, 1);
    self.btnColorlightYellow.transform = CGAffineTransformMakeScale(1.5, 1.5);
}

- (IBAction)selectYellowAction:(id)sender
{
     selectedColor = @"yellow";
     self.btnColorlightYellow.transform=self.btnColorClear.transform = self.btnColorYellow.transform = self.btnColorOrange.transform = self.btnColorAlert.transform = CGAffineTransformMakeScale(1, 1);
    self.btnColorYellow.transform = CGAffineTransformMakeScale(1.5, 1.5);
}

- (IBAction)selectOrangeAction:(id)sender
{
    selectedColor = @"orange";
    self.btnColorlightYellow.transform = self.btnColorClear.transform = self.btnColorYellow.transform = self.btnColorOrange.transform = self.btnColorAlert.transform = CGAffineTransformMakeScale(1, 1);
    self.btnColorOrange.transform = CGAffineTransformMakeScale(1.5, 1.5);
}

- (IBAction)selectRedAction:(id)sender
{
    selectedColor = @"alert";
    self.btnColorlightYellow.transform = self.btnColorClear.transform = self.btnColorYellow.transform = self.btnColorOrange.transform = self.btnColorAlert.transform = CGAffineTransformMakeScale(1, 1);
    self.btnColorAlert.transform = CGAffineTransformMakeScale(1.5, 1.5);
}

- (IBAction)deleteAction:(id)sender
{
    [self showConfirmAlert:LocalizedString(@"alert_are_you_sure", nil) message:@"" callback:^(UIAlertAction *action) {
        
        [SVProgressHUD show];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setValue:@([[activityInfo objectForKey:@"id"] intValue]) forKey:@"id"];
        [parameters setValue:@([shareObject integerForKey:@"user_id"]) forKey:@"user_id"];
        [parameters setValue:@([shareObject integerForKey:@"baby_id"]) forKey:@"baby_id"];
        [parameters setValue:[activityInfo objectForKey:@"photo_url"] forKey:@"photo_url"];
        [parameters setValue:@"wee" forKey:@"name"];
        
        [[WebServiceManager sharedManager] initWithBaseURL:@"api/deleteBabyActivity" parameters:parameters success:^(id result) {
            
            if([[result objectForKey:@"success"] boolValue])
            {
                NSMutableArray *babyList = (NSMutableArray *)[NSKeyedUnarchiver unarchiveObjectWithData:[shareObject objectForKey:@"baby_list"]];
                [[babyList objectAtIndex:[shareObject integerForKey:@"baby_index"]] setObject:[result valueForKey:@"info"] forKey:@"wee_info"];
                [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:babyList] forKey:@"baby_list"];
                [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[result valueForKey:@"info"]] forKey:@"wee_info"];
                [shareObject synchronize];
                
                saveCheck = YES;
                appDelegate.updateTimelineCheck = appDelegate.updateActivitiesCheck = appDelegate.updateActivityCheck = YES;
                [self backAction];
            }
            else
            {
                
            }
            
        } failure:^(NSError *error) {
            
        }];
    }];
}

- (IBAction)nextAction:(id)sender
{
    [SVProgressHUD show];
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setValue:@([[activityInfo objectForKey:@"id"] intValue]) forKey:@"id"];
    [parameters setValue:@([shareObject integerForKey:@"user_id"]) forKey:@"user_id"];
    [parameters setValue:@([shareObject integerForKey:@"baby_id"]) forKey:@"baby_id"];
    [parameters setValue:@"wee" forKey:@"name"];
    [parameters setValue:@"wee" forKey:@"type"];
    [parameters setValue:selectedColor forKey:@"color"];
    [parameters setValue:selectedSize forKey:@"volume"];
    [parameters setValue:@(1)forKey:@"value"];
    [parameters setValue:@"time" forKey:@"unit"];
    [parameters setValue:self.txtNotes.text forKey:@"notes"];
    [parameters setValue:[self formatDateTime:selectedDateTime] forKey:@"time"];
    //[parameters setValue:[[NSString stringWithFormat:@"%@", selectedDateTime] substringWithRange:NSMakeRange(0, 19)] forKey:@"time"];
    
    //    if (self.btnPhoto1.hidden) {
    //        NSString *vUrlStr = [activityInfo objectForKey:@"video_url"];
    //        if (vUrlStr && vUrlStr.length>0) {
    //            [parameters setValue:[activityInfo objectForKey:@"video_url"] forKey:@"video_url"];
    //        }else {
    //            [parameters setValue:[activityInfo objectForKey:@"photo_url"] forKey:@"photo_url"];
    //        }
    //
    //    }
    //    if (self.btnPhoto2.hidden) {
    //        NSString *vUrlStr = [activityInfo objectForKey:@"video_url2"];
    //        if (vUrlStr && vUrlStr.length>0) {
    //            [parameters setValue:[activityInfo objectForKey:@"video_url2"] forKey:@"video_url2"];
    //        }else {
    //            [parameters setValue:[activityInfo objectForKey:@"photo_url2"] forKey:@"photo_url2"];
    //        }
    //    }
    //    if (self.btnPhoto3.hidden) {
    //        NSString *vUrlStr = [activityInfo objectForKey:@"video_url3"];
    //        if (vUrlStr && vUrlStr.length>0) {
    //            [parameters setValue:[activityInfo objectForKey:@"video_url3"] forKey:@"video_url3"];
    //        }else {
    //            [parameters setValue:[activityInfo objectForKey:@"photo_url3"] forKey:@"photo_url3"];
    //        }
    //    }
    
    // To delete existing media file fro server
    NSString *vUrlStr = [activityInfo objectForKey:@"video_url"];
    if ((!videoUrl || videoUrl.length==0) && (![vUrlStr isKindOfClass:[NSNull class]] && vUrlStr.length>0)) {
        [parameters setValue:[activityInfo objectForKey:@"video_url"] forKey:@"video_url"];
    }
    NSString *vUrlStr2 = [activityInfo objectForKey:@"video_url2"];
    if ((!videoUrl2 || videoUrl2.length==0) && (![vUrlStr2 isKindOfClass:[NSNull class]] && vUrlStr2.length>0)) {
        [parameters setValue:[activityInfo objectForKey:@"video_url2"] forKey:@"video_url2"];
    }
    NSString *vUrlStr3 = [activityInfo objectForKey:@"video_url3"];
    if ((!videoUrl3 || videoUrl3.length==0) && (![vUrlStr3 isKindOfClass:[NSNull class]] && vUrlStr3.length>0)) {
        [parameters setValue:[activityInfo objectForKey:@"video_url3"] forKey:@"video_url3"];
    }
    
    NSString *pUrlStr = [activityInfo objectForKey:@"photo_url"];
    if (!havePhotoUrl && (![pUrlStr isKindOfClass:[NSNull class]] && pUrlStr.length>0)) {
        [parameters setValue:[activityInfo objectForKey:@"photo_url"] forKey:@"photo_url"];
    }
    NSString *pUrlStr2 = [activityInfo objectForKey:@"photo_url2"];
    if (!havePhotoUrl2 && (![pUrlStr2 isKindOfClass:[NSNull class]] && pUrlStr2.length>0)) {
        [parameters setValue:[activityInfo objectForKey:@"photo_url2"] forKey:@"photo_url2"];
    }
    NSString *pUrlStr3 = [activityInfo objectForKey:@"photo_url3"];
    if (!havePhotoUrl3 && (![pUrlStr3 isKindOfClass:[NSNull class]] && pUrlStr3.length>0)) {
        [parameters setValue:[activityInfo objectForKey:@"photo_url3"] forKey:@"photo_url3"];
    }

    if(!self.btnPhoto1.hidden || !self.btnPhoto2.hidden || !self.btnPhoto3.hidden)
    {        
        [[WebServiceManager sharedManager] initWithBaseURL:@"api/editBabyActivity" parameters:parameters formData:^(id<AFMultipartFormData> formData) {
            
            if (!self.btnPhoto1.hidden) {
                if (videoUrl && videoUrl.length>0) {
                    [formData appendPartWithFileURL:[NSURL URLWithString:videoUrl] name:@"video" fileName:@"video.mp4" mimeType:@"video" error: nil];
                }else {
                    [formData appendPartWithFileData:UIImageJPEGRepresentation((UIImage*)self.btnPhoto1.imageView.image, 0.8f) name:@"image" fileName:@"image.jpg" mimeType:@"image/jpeg"];
                }
            }
            
            if (!self.btnPhoto2.hidden) {
                if (videoUrl2 && videoUrl2.length>0) {
                    [formData appendPartWithFileURL:[NSURL URLWithString:videoUrl2] name:@"video2" fileName:@"video2.mp4" mimeType:@"video" error: nil];
                }else {
                    [formData appendPartWithFileData:UIImageJPEGRepresentation((UIImage*)self.btnPhoto2.imageView.image, 0.8f) name:@"image2" fileName:@"image2.jpg" mimeType:@"image/jpeg"];
                }
            }
            
            if (!self.btnPhoto3.hidden) {
                if (videoUrl3 && videoUrl3.length>0) {
                    [formData appendPartWithFileURL:[NSURL URLWithString:videoUrl3] name:@"video3" fileName:@"video3.mp4" mimeType:@"video" error: nil];
                }else {
                    [formData appendPartWithFileData:UIImageJPEGRepresentation((UIImage*)self.btnPhoto3.imageView.image, 0.8f) name:@"image3" fileName:@"image3.jpg" mimeType:@"image/jpeg"];
                }
            }

        } success:^(id result) {
            
            if([[result objectForKey:@"success"] boolValue])
            {
                NSMutableArray *babyList = (NSMutableArray *)[NSKeyedUnarchiver unarchiveObjectWithData:[shareObject objectForKey:@"baby_list"]];
                [[babyList objectAtIndex:[shareObject integerForKey:@"baby_index"]] setObject:[result valueForKey:@"info"] forKey:@"wee_info"];
                [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:babyList] forKey:@"baby_list"];
                [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[result valueForKey:@"info"]] forKey:@"wee_info"];
                [shareObject synchronize];
                
                saveCheck = YES;
                appDelegate.updateTimelineCheck = appDelegate.updateActivitiesCheck = appDelegate.updateActivityCheck = YES;
                [self backAction];
            }
            else
            {
                [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:[result objectForKey:@"message"] input:nil];
            }
            
        } failure:^(NSError *error) {
            [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:LocalizedString(@"error_unknown", nil) input:nil];
        }];
    }
    else
    {
        [[WebServiceManager sharedManager] initWithBaseURL:@"api/editBabyActivity" parameters:parameters success:^(id result) {
            
            if([[result objectForKey:@"success"] boolValue])
            {
                NSMutableArray *babyList = (NSMutableArray *)[NSKeyedUnarchiver unarchiveObjectWithData:[shareObject objectForKey:@"baby_list"]];
                [[babyList objectAtIndex:[shareObject integerForKey:@"baby_index"]] setObject:[result valueForKey:@"info"] forKey:@"wee_info"];
                [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:babyList] forKey:@"baby_list"];
                [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[result valueForKey:@"info"]] forKey:@"wee_info"];
                [shareObject synchronize];
                
                saveCheck = YES;
                appDelegate.updateTimelineCheck = appDelegate.updateActivitiesCheck = appDelegate.updateActivityCheck = YES;
                [self backAction];
            }
            else
            {
                [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:[result objectForKey:@"message"] input:nil];
            }
            
        } failure:^(NSError *error) {
            [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:LocalizedString(@"error_unknown", nil) input:nil];
        }];
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)aTableView
{
    return 1;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if(section == 0) return LocalizedString(@"header_details", nil);
    
    return nil;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UILabel *txtHeader;
    UIView *headerView;
    if (IS_IPAD) {
        txtHeader = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, tableView.bounds.size.width-20, 40)];
        txtHeader.font = [UIFont fontWithName:@"SFUIDisplay-Bold" size:18.0f];
        headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 40)];
    }
    else{
        txtHeader = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, tableView.bounds.size.width-20, 30)];
        txtHeader.font = [UIFont fontWithName:@"SFUIDisplay-Bold" size:12.0f];
        headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 30)];
    }
    
    txtHeader.textColor = UIColorFromRGB(0x8D8082);
    txtHeader.text = [self tableView:tableView titleForHeaderInSection:section];
    headerView.backgroundColor = UIColorFromRGB(0xEEE8E4);
    [headerView addSubview:txtHeader];
    return headerView;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (IS_IPAD) {
        return 40;
    }
    return 30;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}
- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 0)
    {
        if(indexPath.row == 0)
        {
            NSDate *minimumDate = [[NSDate date] dateByAddingTimeInterval:-365*24*60*60];
            NSDate *maximumDate = [NSDate date];
            [ActionSheetDatePicker showPickerWithTitle:@"" datePickerMode:UIDatePickerModeDateAndTime selectedDate:selectedDateTime minimumDate:minimumDate maximumDate:maximumDate doneBlock:^(ActionSheetDatePicker *picker, id selectedDate, id origin) {
                
                selectedDateTime = selectedDate;
                NSLog(@"selectedDateTime=%@", selectedDateTime);
                if([[shareObject objectForKey:@"language"] hasPrefix:@"zh"])
                {
                    [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"zh_HK"]];
                    [formatter setDateStyle:NSDateFormatterMediumStyle];
                    [formatter setTimeStyle:NSDateFormatterShortStyle];
                }
                else
                {
                    [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
                    formatter.dateFormat = @"EE d MMM, yyyy hh:mm a";
                }
                self.txtDateTime.text = [formatter stringFromDate:selectedDateTime];
                
            } cancelBlock:^(ActionSheetDatePicker *picker) {
                
            } origin:self.view];
        }
    }
}

#pragma mark -
#pragma mark UIScrollViewDelegate Methods
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if([self.navigationController.visibleViewController isKindOfClass:[ActivityWeeEditViewController class]])
    {
        CGPoint translation = [scrollView.panGestureRecognizer translationInView:scrollView.superview];
        if(translation.y > 0)
        {
            [self.view endEditing:YES];
        }
    }
}


#pragma mark - UIImagePickerController Delegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    NSString *mediaType = info[UIImagePickerControllerMediaType];
    if ([mediaType isEqualToString:(NSString *)kUTTypeImage])
    {
        NSLog(@"Media is an image");
        // Handle selected image
        UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
        NSLog(@"image=%@", image);
        
        if (picker.sourceType == UIImagePickerControllerSourceTypeCamera) {
            UIImageWriteToSavedPhotosAlbum(image, self, @selector(savedPhotoImage:didFinishSavingWithError:contextInfo:), nil);
        }
        
        float scale = self.view.bounds.size.width*2/image.size.width;
        CGSize newSize = CGSizeMake(self.view.bounds.size.width*2, image.size.height*scale);
        if(image.size.width > image.size.height)
        {
            scale = self.view.bounds.size.width*2/image.size.height;
            newSize = CGSizeMake(image.size.width*scale, self.view.bounds.size.width*2);
        }
        UIGraphicsBeginImageContext(newSize);
        [image drawInRect:(CGRect){0, 0, newSize}];
        image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        if (self.btnPhoto1.hidden) {
            self.btnPhoto1.hidden = false;
            [self.btnPhoto1 setImage:image forState:UIControlStateNormal];
        }else if (self.btnPhoto2.hidden) {
            self.btnPhoto2.hidden = false;
            [self.btnPhoto2 setImage:image forState:UIControlStateNormal];
        }else if (self.btnPhoto3.hidden) {
            self.btnPhoto3.hidden = false;
            [self.btnPhoto3 setImage:image forState:UIControlStateNormal];
        }
        
        [self dismissViewControllerAnimated:NO completion:^{
        }];
    }
    else if ([mediaType isEqualToString:(NSString *)kUTTypeMovie])
    {
        BOOL ntwrkFlagStatus = [[NSUserDefaults standardUserDefaults] boolForKey:@"can_Upload_Using_Mobile_Network"];
        BOOL checkWifiStatus = [COMMON_HELPER checkforWifiNetwork];
        
        if ((checkWifiStatus==NO) && (ntwrkFlagStatus==NO)) {
            [self dismissViewControllerAnimated:NO completion:^{
            }];
            [self showAlertWithTitle:LocalizedString(@"mobile_data_usages_title", nil) message:LocalizedString(@"mobile_data_usages_message", nil) input:nil];
            return;
        }else {
            
            NSLog(@"Media is a video");
            NSURL *vUrl= (NSURL*)[info objectForKey:UIImagePickerControllerMediaURL];
            NSString *moviePath = [vUrl path];
            
            NSURL* uploadURL;
            if (self.btnPhoto1.hidden) {
                [[NSFileManager defaultManager] removeItemAtPath:[NSString stringWithFormat:@"%@video1.mov", NSTemporaryDirectory()] error:NULL];
                uploadURL = [NSURL fileURLWithPath:
                             [NSTemporaryDirectory() stringByAppendingPathComponent:@"video1.mov"]];
            }else if (self.btnPhoto2.hidden) {
                [[NSFileManager defaultManager] removeItemAtPath:[NSString stringWithFormat:@"%@video2.mov", NSTemporaryDirectory()] error:NULL];
                uploadURL = [NSURL fileURLWithPath:
                             [NSTemporaryDirectory() stringByAppendingPathComponent:@"video2.mov"]];
            }else if (self.btnPhoto3.hidden) {
                [[NSFileManager defaultManager] removeItemAtPath:[NSString stringWithFormat:@"%@video3.mov", NSTemporaryDirectory()] error:NULL];
                uploadURL = [NSURL fileURLWithPath:
                             [NSTemporaryDirectory() stringByAppendingPathComponent:@"video3.mov"]];
            }
            
            // Generate Thumbnail Image.
            UIImage *tImg = [COMMON_HELPER generateThumbnailImageFromVideoWithUrl:vUrl];
            
            [SVProgressHUD showWithStatus:LocalizedString(@"processing_video", nil)];
            [COMMON_HELPER compressVideo:vUrl outputURL:uploadURL handler:^(AVAssetExportSession *completion) {
                
                if (completion.status == AVAssetExportSessionStatusCompleted) {
                    NSData *newDataForUpload = [NSData dataWithContentsOfURL:uploadURL];
                    NSLog(@"Size of new Video after compression is (bytes):%lu",(unsigned long)[newDataForUpload length]);
                    
                    CGFloat sizeInMB = ([newDataForUpload length]/1000)/1000;
                    if (sizeInMB>15) {
                        [self showAlertWithTitleCallback:@"Large video size" message:@"Can not add this video." callback:^(UIAlertAction *action) {
                            [[NSFileManager defaultManager] removeItemAtPath:[NSString stringWithFormat:@"%@", uploadURL] error:NULL];
                        }];
                    }
                    else {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            if (self.btnPhoto1.hidden) {
                                self.btnPhoto1.hidden = false;
                                videoUrl = [uploadURL absoluteString];
                                [self.btnPhoto1 setImage:tImg forState:UIControlStateNormal];
                            }else if (self.btnPhoto2.hidden) {
                                self.btnPhoto2.hidden = false;
                                videoUrl2 = [uploadURL absoluteString];
                                [self.btnPhoto2 setImage:tImg forState:UIControlStateNormal];
                            }else if (self.btnPhoto3.hidden) {
                                self.btnPhoto3.hidden = false;
                                videoUrl3 = [uploadURL absoluteString];
                                [self.btnPhoto3 setImage:tImg forState:UIControlStateNormal];
                            }
                        });
                        
                    }
                }
                [SVProgressHUD dismiss];
                
            }];
            
            if (picker.sourceType == UIImagePickerControllerSourceTypeCamera) {
                if (UIVideoAtPathIsCompatibleWithSavedPhotosAlbum (moviePath)) {
                    UISaveVideoAtPathToSavedPhotosAlbum (moviePath, nil, nil, nil);
                }
            }
            
            [self dismissViewControllerAnimated:NO completion:^{
            }];
        }
    }
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void) savedPhotoImage:(UIImage *)image
didFinishSavingWithError:(NSError *)error
             contextInfo:(void *)contextInfo
{
    NSLog(@"This image has been saved to your camera roll");
}

#pragma mark - Custom Actions
-(NSString *)formatDateTime:(NSDate *)sDate
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"E d MMM, yyyy hh:mm a"];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    [dateFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
    NSString *dateStr = [dateFormatter stringFromDate:sDate];
    NSLog(@"%@",dateStr);
    return dateStr;
}

-(IBAction)didSelectAddPhoto:(UIButton *)sender
{
    if (self.btnPhoto1.hidden || self.btnPhoto2.hidden || self.btnPhoto3.hidden)
    {
        UIAlertController *alertController=[UIAlertController alertControllerWithTitle:LocalizedString(@"title_selectImage", nil) message:nil preferredStyle:UIAlertControllerStyleActionSheet];

        UIAlertAction *takePhoto=[UIAlertAction actionWithTitle:LocalizedString(@"title_camera", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self takePhotoAction];
        }];
        UIAlertAction *choosePhoto=[UIAlertAction actionWithTitle:LocalizedString(@"title_camera_roll", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self getPhotoAlbumAction];
        }];
        UIAlertAction *actionCancel=[UIAlertAction actionWithTitle:LocalizedString(@"title_cancel", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            [alertController dismissViewControllerAnimated:YES completion:nil];
        }];

        [alertController addAction:takePhoto];
        [alertController addAction:choosePhoto];
        [alertController addAction:actionCancel];
        
        if (IS_IPAD) {
            UIPopoverPresentationController *popPresenter = [alertController popoverPresentationController];
            popPresenter.sourceView = sender;
            popPresenter.sourceRect = CGRectMake(sender.bounds.origin.x, sender.bounds.size.height/2, 0, 0);
            //popPresenter.presentedViewController.preferredContentSize =  CGSizeMake(520, 400);
        }
        [self presentViewController:alertController animated:YES completion:nil];
    }
    else
    {
        [self showAlertWithTitle:@"" message:@"You can not add more than 3 photos. " input:nil];
    }
    
}

#pragma mark - Keywords Implementations
-(void)doSetupForKeywordView
{
    [self.btnaddKeyword addTarget:self action:@selector(didSelectAddKeyword:) forControlEvents:UIControlEventTouchUpInside];
    
    if (IS_IPAD) {
        self.keyword1.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.keyword2.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.keyword3.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.keyword4.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.keyword5.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.keyword6.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.keyword7.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.keyword8.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.keyword9.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
    }
    
    // keyword view
    keywordsArray = [[NSMutableArray alloc] init];
    self.keywordView.backgroundColor = [UIColor clearColor];
    self.keyword1.backgroundColor = [UIColor clearColor];
    self.keyword2.backgroundColor = [UIColor clearColor];
    self.keyword3.backgroundColor = [UIColor clearColor];
    self.keyword4.backgroundColor = [UIColor clearColor];
    self.keyword5.backgroundColor = [UIColor clearColor];
    self.keyword6.backgroundColor = [UIColor clearColor];
    self.keyword7.backgroundColor = [UIColor clearColor];
    self.keyword8.backgroundColor = [UIColor clearColor];
    self.keyword9.backgroundColor = [UIColor clearColor];
    
    self.keyword1.text = @"";
    self.keyword2.text = @"";
    self.keyword3.text = @"";
    self.keyword4.text = @"";
    self.keyword5.text = @"";
    self.keyword6.text = @"";
    self.keyword7.text = @"";
    self.keyword8.text = @"";
    self.keyword9.text = @"";
    
    self.keyword1.layer.cornerRadius = 4.0f;
    self.keyword1.layer.masksToBounds = YES;
    self.keyword2.layer.cornerRadius = 4.0f;
    self.keyword2.layer.masksToBounds = YES;
    self.keyword3.layer.cornerRadius = 4.0f;
    self.keyword3.layer.masksToBounds = YES;
    self.keyword4.layer.cornerRadius = 4.0f;
    self.keyword4.layer.masksToBounds = YES;
    self.keyword5.layer.cornerRadius = 4.0f;
    self.keyword5.layer.masksToBounds = YES;
    self.keyword6.layer.cornerRadius = 4.0f;
    self.keyword5.layer.masksToBounds = YES;
    self.keyword7.layer.cornerRadius = 4.0f;
    self.keyword7.layer.masksToBounds = YES;
    self.keyword8.layer.cornerRadius = 4.0f;
    self.keyword8.layer.masksToBounds = YES;
    self.keyword9.layer.cornerRadius = 4.0f;
    self.keyword9.layer.masksToBounds = YES;
    
    self.keyword1.userInteractionEnabled = YES;
    self.keyword2.userInteractionEnabled = YES;
    self.keyword3.userInteractionEnabled = YES;
    self.keyword4.userInteractionEnabled = YES;
    self.keyword5.userInteractionEnabled = YES;
    self.keyword6.userInteractionEnabled = YES;
    self.keyword7.userInteractionEnabled = YES;
    self.keyword8.userInteractionEnabled = YES;
    self.keyword9.userInteractionEnabled = YES;
    
    self.keyword1.tag = 1;
    self.keyword2.tag = 2;
    self.keyword3.tag = 3;
    self.keyword4.tag = 4;
    self.keyword5.tag = 5;
    self.keyword6.tag = 6;
    self.keyword7.tag = 7;
    self.keyword8.tag = 8;
    self.keyword9.tag = 9;
    
    UITapGestureRecognizer *gesture1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)] ;
    UITapGestureRecognizer *gesture2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)] ;
    UITapGestureRecognizer *gesture3 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)] ;
    UITapGestureRecognizer *gesture4 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)] ;
    UITapGestureRecognizer *gesture5 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)] ;
    UITapGestureRecognizer *gesture6 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)] ;
    UITapGestureRecognizer *gesture7 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)] ;
    UITapGestureRecognizer *gesture8 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)] ;
    UITapGestureRecognizer *gesture9 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)] ;
    
    [self.keyword1 addGestureRecognizer:gesture1];
    [self.keyword2 addGestureRecognizer:gesture2];
    [self.keyword3 addGestureRecognizer:gesture3];
    [self.keyword4 addGestureRecognizer:gesture4];
    [self.keyword5 addGestureRecognizer:gesture5];
    [self.keyword6 addGestureRecognizer:gesture6];
    [self.keyword7 addGestureRecognizer:gesture7];
    [self.keyword8 addGestureRecognizer:gesture8];
    [self.keyword9 addGestureRecognizer:gesture9];
    
    UILongPressGestureRecognizer *longPressGesture1 = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongGesture:)];
    longPressGesture1.minimumPressDuration = 1.5;
    UILongPressGestureRecognizer *longPressGesture2 = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongGesture:)];
    longPressGesture2.minimumPressDuration = 1.5;
    UILongPressGestureRecognizer *longPressGesture3 = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongGesture:)];
    longPressGesture3.minimumPressDuration = 1.5;
    UILongPressGestureRecognizer *longPressGesture4 = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongGesture:)];
    longPressGesture4.minimumPressDuration = 1.5;
    UILongPressGestureRecognizer *longPressGesture5 = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongGesture:)];
    longPressGesture5.minimumPressDuration = 1.5;
    UILongPressGestureRecognizer *longPressGesture6 = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongGesture:)];
    longPressGesture6.minimumPressDuration = 1.5;
    UILongPressGestureRecognizer *longPressGesture7 = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongGesture:)];
    longPressGesture7.minimumPressDuration = 1.5;
    UILongPressGestureRecognizer *longPressGesture8 = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongGesture:)];
    longPressGesture8.minimumPressDuration = 1.5;
    UILongPressGestureRecognizer *longPressGesture9 = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongGesture:)];
    longPressGesture9.minimumPressDuration = 1.5;
    
    [self.keyword1 addGestureRecognizer:longPressGesture1];
    [self.keyword2 addGestureRecognizer:longPressGesture2];
    [self.keyword3 addGestureRecognizer:longPressGesture3];
    [self.keyword4 addGestureRecognizer:longPressGesture4];
    [self.keyword5 addGestureRecognizer:longPressGesture5];
    [self.keyword6 addGestureRecognizer:longPressGesture6];
    [self.keyword7 addGestureRecognizer:longPressGesture7];
    [self.keyword8 addGestureRecognizer:longPressGesture8];
    [self.keyword9 addGestureRecognizer:longPressGesture9];
    
}

-(void)handleTap:(UITapGestureRecognizer *)sender
{
    NSString * seletedKey = @"";
    
    if (sender.view.tag == 1) {
        seletedKey = self.keyword1.text;
    }else if (sender.view.tag == 2) {
        seletedKey = self.keyword2.text;
    }else if (sender.view.tag == 3) {
        seletedKey = self.keyword3.text;
    }else if (sender.view.tag == 4) {
        seletedKey = self.keyword4.text;
    }else if (sender.view.tag == 5) {
        seletedKey = self.keyword5.text;
    }else if (sender.view.tag == 6) {
        seletedKey = self.keyword6.text;
    }else if (sender.view.tag == 7) {
        seletedKey = self.keyword7.text;
    }else if (sender.view.tag == 8) {
        seletedKey = self.keyword8.text;
    }else if (sender.view.tag == 9) {
        seletedKey = self.keyword9.text;
    }
    
    self.txtNotes.text = [NSString stringWithFormat:@"%@ %@",self.txtNotes.text, seletedKey];
    
}

-(void)handleLongGesture:(UILongPressGestureRecognizer *)sender
{
    int tag = (int)sender.view.tag;
    CGPoint buttonPosition = [sender.view convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *indexPathToReload = [self.tableView indexPathForRowAtPoint:buttonPosition];
    
    //    if ( sender.state == UIGestureRecognizerStateEnded ) {
    
    if (keywordsArray.count>0) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Remove Keyword" message:@"Are you sure you want to remove this Keyword !" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *btnOk = [UIAlertAction actionWithTitle:LocalizedString(@"btn_ok", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            
            [keywordsArray removeObjectAtIndex:tag-1];
            [self addKeywordLabel];
            [self addKeywordsList];
            //[self.tableView reloadRowsAtIndexPaths:@[indexPathToReload] withRowAnimation:UITableViewRowAnimationBottom];
            [self.tableView reloadData];
            
        }];
        
        UIAlertAction *btnCancel = [UIAlertAction actionWithTitle:LocalizedString(@"btn_cancel", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [alert dismissViewControllerAnimated:YES completion:nil];
        }];
        [alert addAction:btnCancel];
        [alert addAction:btnOk];
        [self presentViewController:alert animated:YES completion:nil];
    }
    //    }
}

-(void)didSelectAddKeyword:(UIButton *)sender
{
    if (keywordsArray.count<9) {
        
        CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
        NSIndexPath *indexPathToReload = [self.tableView indexPathForRowAtPoint:buttonPosition];
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:@"Add Keyword" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *btnOk = [UIAlertAction actionWithTitle:LocalizedString(@"btn_ok", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            if(alert.textFields[0].text.length > 0)
            {
                [alert dismissViewControllerAnimated:YES completion:nil];
                
                NSString *text = alert.textFields[0].text;
                [keywordsArray addObject:text];
                [self addKeywordLabel];
                [self addKeywordsList];
                //[self.tableView reloadRowsAtIndexPaths:@[indexPathToReload] withRowAnimation:UITableViewRowAnimationBottom];
                [self.tableView reloadData];
            }
            else
            {
                [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:LocalizedString(@"error_family_name", nil) input:nil];
            }
        }];
        
        UIAlertAction *btnCancel = [UIAlertAction actionWithTitle:LocalizedString(@"btn_cancel", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            
            [alert dismissViewControllerAnimated:YES completion:nil];
        }];
        
        [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
            textField.autocapitalizationType = UITextAutocapitalizationTypeWords;
        }];
        
        [alert addAction:btnCancel];
        [alert addAction:btnOk];
        
        [self presentViewController:alert animated:YES completion:nil];
        
    }
    else {
        [self showAlertWithTitle:LocalizedString(@"Keywords", nil) message:@"You can not add more than 9 keywords !" input:nil];
    }
    
}

-(void)addKeywordLabel
{
    self.keyword1.text = @"";
    self.keyword2.text = @"";
    self.keyword3.text = @"";
    self.keyword4.text = @"";
    self.keyword5.text = @"";
    self.keyword6.text = @"";
    self.keyword7.text = @"";
    self.keyword8.text = @"";
    self.keyword9.text = @"";
    
    self.keyword1.backgroundColor = [UIColor clearColor];
    self.keyword2.backgroundColor = [UIColor clearColor];
    self.keyword3.backgroundColor = [UIColor clearColor];
    self.keyword4.backgroundColor = [UIColor clearColor];
    self.keyword5.backgroundColor = [UIColor clearColor];
    self.keyword6.backgroundColor = [UIColor clearColor];
    self.keyword7.backgroundColor = [UIColor clearColor];
    self.keyword8.backgroundColor = [UIColor clearColor];
    self.keyword9.backgroundColor = [UIColor clearColor];
    
    if (keywordsArray.count>0) {
        
        for (int i=0;i<keywordsArray.count;i++) {
            if (i==0) {
                self.keyword1.text =[NSString stringWithFormat:@"%@",[keywordsArray objectAtIndex:i]];
                self.keyword1.backgroundColor = UIColorFromRGB(0xB7ADA6);
            }
            else if (i==1) {
                self.keyword2.text =[NSString stringWithFormat:@"%@",[keywordsArray objectAtIndex:i]];
                self.keyword2.backgroundColor = UIColorFromRGB(0xB7ADA6);
            }
            else if (i==2) {
                self.keyword3.text =[NSString stringWithFormat:@"%@",[keywordsArray objectAtIndex:i]];
                self.keyword3.backgroundColor = UIColorFromRGB(0xB7ADA6);
            }
            else if (i==3) {
                self.keyword4.text =[NSString stringWithFormat:@"%@",[keywordsArray objectAtIndex:i]];
                self.keyword4.backgroundColor = UIColorFromRGB(0xB7ADA6);
            }
            else if (i==4) {
                self.keyword5.text =[NSString stringWithFormat:@"%@",[keywordsArray objectAtIndex:i]];
                self.keyword5.backgroundColor = UIColorFromRGB(0xB7ADA6);
            }
            else if (i==5) {
                self.keyword6.text =[NSString stringWithFormat:@"%@",[keywordsArray objectAtIndex:i]];
                self.keyword6.backgroundColor = UIColorFromRGB(0xB7ADA6);
            }
            else if (i==6) {
                self.keyword7.text =[NSString stringWithFormat:@"%@",[keywordsArray objectAtIndex:i]];
                self.keyword7.backgroundColor = UIColorFromRGB(0xB7ADA6);
            }
            else if (i==7) {
                self.keyword8.text =[NSString stringWithFormat:@"%@",[keywordsArray objectAtIndex:i]];
                self.keyword8.backgroundColor = UIColorFromRGB(0xB7ADA6);
            }
            else if (i==8) {
                self.keyword9.text =[NSString stringWithFormat:@"%@",[keywordsArray objectAtIndex:i]];
                self.keyword9.backgroundColor = UIColorFromRGB(0xB7ADA6);
            }
        }
        
    }
}

-(void)getKeywordsList
{
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setValue:@([shareObject integerForKey:@"user_id"]) forKey:@"user_id"];
    [parameters setValue:@([shareObject integerForKey:@"baby_id"]) forKey:@"baby_id"];
    [parameters setValue:@"activities" forKey:@"type"];
    [parameters setValue:@"wee" forKey:@"category"];
    
    [[WebServiceManager sharedManager] initWithBaseURL:@"api/getKeywords" parameters:parameters success:^(id result) {
        
        if ([[result valueForKey:@"success"] boolValue]) {
            NSMutableArray *list = (NSMutableArray *)[result valueForKey:@"list"];
            if(list.count > 0){
                
                keywordslistDict = [[NSDictionary alloc] initWithDictionary:[result valueForKey:@"list"]];
                NSString *str = [keywordslistDict objectForKey:@"wee"];
                if (str.length>0) {
                    keywordsArray = [[NSMutableArray alloc] initWithArray:[str componentsSeparatedByString:@","]];
                }else {
                    keywordsArray = [[NSMutableArray alloc] init];
                }
            }
            [self addKeywordLabel];
            [self.tableView reloadData];
        }
        else {
            NSLog(@"error");
        }
        
    } failure:^(NSError *error) {
        
    }];
}

-(void)addKeywordsList
{
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setValue:@([shareObject integerForKey:@"user_id"]) forKey:@"user_id"];
    [parameters setValue:@([shareObject integerForKey:@"baby_id"]) forKey:@"baby_id"];
    [parameters setValue:@"activities" forKey:@"type"];
    [parameters setValue:@"wee" forKey:@"category"];
    [parameters setValue:@1 forKey:@"notes"];
    
    if (keywordsArray.count>0) {
        for(int i=0; i<keywordsArray.count; i++){
            [parameters setValue:[keywordsArray componentsJoinedByString:@","] forKey:@"keywords"];
        }
    }else{
        [parameters setValue:@"" forKey:@"keywords"];
    }
    
    [[WebServiceManager sharedManager] initWithBaseURL:@"api/setKeywords" parameters:parameters success:^(id result) {
        
        if ([[result valueForKey:@"success"] boolValue]) {
            NSMutableArray *list = (NSMutableArray *)[result valueForKey:@"list"];
            if(list.count > 0){
                
                keywordslistDict = [[NSDictionary alloc] initWithDictionary:[result valueForKey:@"list"]];
                NSString *str = [keywordslistDict objectForKey:@"wee"];
                if (str.length>0) {
                    keywordsArray = [[NSMutableArray alloc] initWithArray:[str componentsSeparatedByString:@","]];
                }else {
                    keywordsArray = [[NSMutableArray alloc] init];
                }

                //keywordsArray = list;
                //[list removeAllObjects];
            }
        }
        else {
            NSLog(@"error");
        }
        
    } failure:^(NSError *error) {
        
    }];
    
}

@end
