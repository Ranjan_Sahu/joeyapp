//
//  ActivityReminderViewController.h
//  Joey
//
//  Created by csl on 26/5/2017.
//  Copyright © 2017 Auxilia. All rights reserved.
//

#import "BaseViewController.h"

@interface ActivityReminderViewController : BaseViewController <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, weak) IBOutlet UIButton *btnAddReminder;
@property (nonatomic, weak) IBOutlet UILabel *lblTitle;
@property (nonatomic, weak) IBOutlet UILabel *lblTime;
@property (nonatomic, weak) IBOutlet UILabel *lblRecurrence;
@property (nonatomic, weak) IBOutlet UITextField *txtTitle;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *titleLblHgtConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *timelblHgtConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *recurrrenceLblHgtConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *btnAddRemainderHgtConstraint;

@property (nonatomic, weak) IBOutlet UIButton *btnMonday;
@property (nonatomic, weak) IBOutlet UIButton *btnTuesday;
@property (nonatomic, weak) IBOutlet UIButton *btnWednesday;
@property (nonatomic, weak) IBOutlet UIButton *btnThursday;
@property (nonatomic, weak) IBOutlet UIButton *btnFriday;
@property (nonatomic, weak) IBOutlet UIButton *btnSaturday;
@property (nonatomic, weak) IBOutlet UIButton *btnSunday;

@property (nonatomic, weak) IBOutlet UIPickerView *hourPicker;
@property (nonatomic, weak) IBOutlet UIPickerView *minutePicker;
@property (nonatomic, weak) IBOutlet UIPickerView *amPmPicker;

@property (nonatomic, weak) IBOutlet UILabel *colonLabel;

@end
