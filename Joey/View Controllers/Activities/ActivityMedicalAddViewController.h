//
//  ActivityMedicalAddViewController.h
//  Joey
//
//  Created by csl on 5/1/2017.
//  Copyright © 2017 Auxilia. All rights reserved.
//

#import "BaseTableViewController.h"
#import <Photos/Photos.h>
#import <AVFoundation/AVFoundation.h>

@interface ActivityMedicalAddViewController : BaseTableViewController <UINavigationControllerDelegate, UIImagePickerControllerDelegate,UIPickerViewDelegate,UIPickerViewDataSource>

@property (nonatomic, weak) IBOutlet UIBarButtonItem *btnSave;
@property (nonatomic, weak) IBOutlet UILabel *lblTemperature;
@property (nonatomic, weak) IBOutlet UILabel *lblDateTime;
@property (nonatomic, weak) IBOutlet UILabel *txtDateTime;
@property (nonatomic, weak) IBOutlet UILabel *lblPhoto;
@property (nonatomic, weak) IBOutlet UILabel *lblNotes;
@property (nonatomic, weak) IBOutlet UITextView *txtNotes;

@property (nonatomic, weak) IBOutlet UIButton *btnPhoto1;
@property (nonatomic, weak) IBOutlet UIButton *btnPhoto2;
@property (nonatomic, weak) IBOutlet UIButton *btnPhoto3;

@property (nonatomic, weak) IBOutlet UIButton *btnMedicine;
@property (nonatomic, weak) IBOutlet UIButton *btnSymptoms;
@property (nonatomic, weak) IBOutlet UIButton *btnTemperature;
@property (nonatomic, weak) IBOutlet UIButton *btnMovement;

@property (nonatomic, weak) IBOutlet UIPickerView *numberMLPicvker;
@property (nonatomic, weak) IBOutlet UIPickerView *mlPicvker;

@property (nonatomic, weak) IBOutlet UILabel *titleMedicine;
@property (nonatomic, weak) IBOutlet UILabel *titltSymptoms;
@property (nonatomic, weak) IBOutlet UILabel *titleTemparature;
@property (nonatomic, weak) IBOutlet UILabel *titleMovement;

@property (nonatomic, weak) IBOutlet UILabel *lblDose;
@property (nonatomic, weak) IBOutlet UILabel *lblMedSymp;
@property (nonatomic, weak) IBOutlet UITextField *txtMedSymp;

@property (nonatomic, weak) IBOutlet UIPickerView *temp1Picvker;
@property (nonatomic, weak) IBOutlet UIPickerView *temp2Picvker;
@property (nonatomic, weak) IBOutlet UILabel *celciusLBL;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *btnMedicineWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *btnSymptompsWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *btnTemparatureWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *btnMovementWidth;

- (IBAction)selectMedicalTypeAction:(id)sender;

// Keywords View
@property (nonatomic, weak) IBOutlet UIButton *btnaddKeyword;

@property (weak, nonatomic) IBOutlet UIView *keywordView;
@property (nonatomic, weak) IBOutlet UILabel *keyword1;
@property (nonatomic, weak) IBOutlet UILabel *keyword2;
@property (nonatomic, weak) IBOutlet UILabel *keyword3;
@property (nonatomic, weak) IBOutlet UILabel *keyword4;
@property (nonatomic, weak) IBOutlet UILabel *keyword5;
@property (nonatomic, weak) IBOutlet UILabel *keyword6;
@property (nonatomic, weak) IBOutlet UILabel *keyword7;
@property (nonatomic, weak) IBOutlet UILabel *keyword8;
@property (nonatomic, weak) IBOutlet UILabel *keyword9;

// Medical/Symptoms Keywords View
@property (nonatomic, weak) IBOutlet UIButton *btnaddKeywordMed;

@property (weak, nonatomic) IBOutlet UIView *medKeywordView;
@property (nonatomic, weak) IBOutlet UILabel *medKeyword1;
@property (nonatomic, weak) IBOutlet UILabel *medKeyword2;
@property (nonatomic, weak) IBOutlet UILabel *medKeyword3;
@property (nonatomic, weak) IBOutlet UILabel *medKeyword4;
@property (nonatomic, weak) IBOutlet UILabel *medKeyword5;
@property (nonatomic, weak) IBOutlet UILabel *medKeyword6;
@property (nonatomic, weak) IBOutlet UILabel *medKeyword7;
@property (nonatomic, weak) IBOutlet UILabel *medKeyword8;
@property (nonatomic, weak) IBOutlet UILabel *medKeyword9;

@end
