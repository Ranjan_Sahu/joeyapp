//
//  SigninViewController.h
//  Joey
//
//  Created by csl on 23/1/2017.
//  Copyright © 2017 Auxilia. All rights reserved.
//

#import "BaseViewController.h"
#import <WatchConnectivity/WatchConnectivity.h>

@interface SigninViewController : BaseViewController<WCSessionDelegate>

@property (nonatomic, weak) IBOutlet UILabel *lblTitle;
@property (nonatomic, weak) IBOutlet UITextField *txtEmail;
@property (nonatomic, weak) IBOutlet UITextField *txtPassword;
@property (nonatomic, weak) IBOutlet UIView *viewEmail;
@property (nonatomic, weak) IBOutlet UIView *viewPassword;
@property (nonatomic, weak) IBOutlet UIButton *btnSignin;
@property (nonatomic, weak) IBOutlet UIButton *btnForgotPassword;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *emailHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *passWordHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *logoHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *logoWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *logInHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *forgotPassHeight;

@end
