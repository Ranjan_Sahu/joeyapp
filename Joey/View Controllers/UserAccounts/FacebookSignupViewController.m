//
//  FacebookSignupViewController.m
//  Joey
//
//  Created by csl on 23/1/2017.
//  Copyright © 2017 Auxilia. All rights reserved.
//

#import "AppDelegate.h"
#import "FacebookSignupViewController.h"
#import <ChimpKit/ChimpKit.h>

@interface FacebookSignupViewController ()<ChimpKitRequestDelegate>
{
    NSUserDefaults *shareObject;
    AppDelegate *appDelegate;
    NSMutableDictionary *parameters;
    BOOL termsCheck;
}
@end

@implementation FacebookSignupViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    shareObject = [NSUserDefaults standardUserDefaults];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    parameters = [NSMutableDictionary dictionary];
    if(self.data) parameters = [self.data objectForKey:@"parameters"];
    
    [self.navigationController setNavigationBarHidden:NO];
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:UIColorFromRGB(0x8D8082), NSForegroundColorAttributeName, [UIFont fontWithName:@"SFUIDisplay-Medium" size:18.0f], NSFontAttributeName, nil]];
    [self.navigationController.navigationBar setBarTintColor:UIColorFromRGB(0xEEE8E4)];
    [self.navigationController.navigationBar setTranslucent:NO];
    [self.navigationItem setTitle:LocalizedString(@"title_signup", nil)];
    [self customBackButton];
    
    self.lblTitle.text = LocalizedString(@"txt_signup_title", nil);
    self.txtFullname.placeholder = LocalizedString(@"txt_fullname", nil);
    self.txtEmail.placeholder = LocalizedString(@"txt_email", nil);
    
    NSLog(@"self.data=%@", self.data);
    if(self.data)
    {
        self.txtFullname.text = [self.data objectForKey:@"name"];
        self.txtEmail.text = [self.data objectForKey:@"email"];
        self.txtEmail.enabled = NO;
    }
    
    self.viewFullname.layer.borderWidth = 1.f;
    self.viewFullname.layer.borderColor = UIColorFromRGB(0xF2EEEB).CGColor;
    self.viewEmail.layer.borderWidth = 1.f;
    self.viewEmail.layer.borderColor = UIColorFromRGB(0xF2EEEB).CGColor;
    self.btnSignup.layer.cornerRadius = 5.f;
    [self.btnSignup setTitle:LocalizedString(@"btn_signup", nil) forState:UIControlStateNormal];
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
    
    if (IS_IPAD) {
        self.fullNameHeight.constant = 54.0;
        self.emailHeight.constant = 54.0;
        self.btnSignUpHeight.constant = 54.0;
        self.logoWidth.constant = 117.0;
        self.logoHeight.constant = 96.0;
        self.lblTitle.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:22.0f];
        [self.btnSignup.titleLabel setFont:[UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f]];
        self.txtFullname.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f];
        self.txtEmail.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f];
    }

}

- (void)customBackButton
{
    if([self.navigationController.viewControllers count] > 1)
    {
        UIImage *imgBack = [UIImage imageNamed:@"nav_btn_back"];
        UIButton *btnBack = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnBack setImage:imgBack forState:UIControlStateNormal];
        btnBack.frame = CGRectMake(0, 0, imgBack.size.width, imgBack.size.height);
        [btnBack addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
        [btnBack setImageEdgeInsets:UIEdgeInsetsMake(0, -16, 0, 16)];
        UIBarButtonItem *backBarItem = [[UIBarButtonItem alloc] initWithCustomView:btnBack];
        self.navigationItem.leftBarButtonItem = backBarItem;
    }
}

- (void)backAction
{
    [self.navigationController setNavigationBarHidden:YES];
    [self.navigationController popViewControllerAnimated:YES];
}

- (BOOL)validateInputs
{
    ALPValidator *validator = nil;
    
    validator = [ALPValidator validatorWithType:ALPValidatorTypeString];
    [validator addValidationToEnsurePresenceWithInvalidMessage:LocalizedString(@"error_full_name", nil)];
    [validator validate:self.txtFullname.text];
    if(![validator isValid])
    {
        [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:[[validator errorMessages] objectAtIndex:0] input:self.txtFullname];
        return NO;
    }
    
    validator = [ALPValidator validatorWithType:ALPValidatorTypeString];
    [validator addValidationToEnsureRegularExpressionIsMetWithPattern:@"^[_A-Za-z0-9-+]+(\\.[_A-Za-z0-9-+]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z‌​]{2,10})$" invalidMessage:LocalizedString(@"error_email", nil)];
    [validator validate:self.txtEmail.text];
    if(![validator isValid])
    {
        [self.txtEmail becomeFirstResponder];
        [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:[[validator errorMessages] objectAtIndex:0] input:self.txtEmail];
        return NO;
    }
    
    return YES;
}

- (IBAction)nextAction:(id)sender
{
    if([self validateInputs])
    {
        [SVProgressHUD show];
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setValue:@"facebook" forKey:@"login_type"];
        [parameters setValue:self.txtFullname.text forKey:@"facebook_id"];
        [parameters setValue:[self.data objectForKey:@"facebook_id"] forKey:@"facebook_id"];
        [parameters setValue:[self.data objectForKey:@"gender"] forKey:@"gender"];
        [parameters setValue:self.txtFullname.text forKey:@"name"];
        [parameters setValue:self.txtEmail.text forKey:@"email"];
        [parameters setValue:appDelegate.deviceToken forKey:@"device_token"];
        
        [[WebServiceManager sharedManager] initWithBaseURL:@"api/signup" parameters:parameters success:^(id result) {
            
            if([[result objectForKey:@"success"] boolValue])
            {
                [shareObject setObject:[result objectForKey:@"facebook_id"] forKey:@"facebook_id"];
                [shareObject setInteger:[[result objectForKey:@"id"] integerValue] forKey:@"user_id"];
                [shareObject setObject:[result objectForKey:@"name"] forKey:@"user_name"];
                [shareObject setObject:[result objectForKey:@"email"] forKey:@"user_email"];
                [shareObject setBool:[[result objectForKey:@"email_confirm_check"] integerValue] forKey:@"email_confirm_check"];
                [shareObject setObject:[result objectForKey:@"access_token"] forKey:@"access_token"];
                [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[result valueForKey:@"unit_info"]] forKey:@"unit_info"];
                [shareObject synchronize];
                
                // MailChimp
                NSInteger userId = [[result objectForKey:@"id"] integerValue];
                NSNumber *uId = [NSNumber numberWithInteger:userId];
                NSString *uEmial = [result objectForKey:@"email"];
                NSString *uName = [result objectForKey:@"name"];
                NSDictionary *paramDict = @{@"id": uId, @"email": @{@"email": uEmial}, @"merge_vars": @{@"Name": uName}};
                
                [[ChimpKit sharedKit] setShouldUseBackgroundThread:YES];
                [[ChimpKit sharedKit] setTimeoutInterval:30.0f];
                [[ChimpKit sharedKit] callApiMethod:@"lists/subscribe" withParams:paramDict andDelegate:self];

                [self sendUserToNextScreen];
            }
            else
            {
                [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:LocalizedString([result objectForKey:@"message"], nil) input:nil];
            }
            
        } failure:^(NSError *error) {
            [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:LocalizedString(@"error_unknown", nil) input:nil];
        }];
    }
}


#pragma mark UITextFieldDelegate Methods
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if([textField isEqual:self.txtFullname])
    {
        [self.view endEditing:YES];
    }
    return YES;
}

#pragma mark - ChimpKit Delegate
-(void)ckRequestIdentifier:(NSUInteger)requestIdentifier didSucceedWithResponse:(NSURLResponse *)response andData:(NSData *)data
{
    //NSLog(@"HTTP Status Code: %d", response.statusCode);
    NSLog(@"Response String: %lul", requestIdentifier);
    
    NSError *parseError = nil;
    id responseData = [NSJSONSerialization JSONObjectWithData:data options:0 error:&parseError];
    if ([responseData isKindOfClass:[NSDictionary class]]) {
        id email = [responseData objectForKey:@"email"];
        if ([email isKindOfClass:[NSString class]]) {
            //Successfully subscribed email address
            dispatch_async(dispatch_get_main_queue(), ^{
                //Update UI here
                //[self sendUserToNextScreen];
            });
        }
    }
}

-(void)ckRequestFailedWithIdentifier:(NSUInteger)requestIdentifier andError:(NSError *)anError
{
    //Handle connection error
    NSLog(@"Error, %@", anError);
    dispatch_async(dispatch_get_main_queue(), ^{
        //Update UI here
        //[self sendUserToNextScreen];
    });
}

#pragma mark - Custom Methods
-(void)sendUserToNextScreen
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"InvitationViewController"];
    viewController.data = [[NSMutableDictionary alloc] initWithDictionary:@{@"splash_page_check":@(YES)}];
    [self.navigationController pushViewController:viewController animated:YES];
}

@end
