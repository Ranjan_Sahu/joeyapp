//
//  AccountVerificationViewController.m
//  Joey
//
//  Created by csl on 21/3/2017.
//  Copyright © 2017 Auxilia. All rights reserved.
//

#import "AppDelegate.h"
#import "AccountVerificationViewController.h"

@interface AccountVerificationViewController ()
{
    NSUserDefaults *shareObject;
    AppDelegate *appDelegate;
}
@end

@implementation AccountVerificationViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    shareObject = [NSUserDefaults standardUserDefaults];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    [self.navigationController setNavigationBarHidden:NO];
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:UIColorFromRGB(0x8D8082), NSForegroundColorAttributeName, [UIFont fontWithName:@"SFUIDisplay-Medium" size:18.0f], NSFontAttributeName, nil]];
    [self.navigationController.navigationBar setBarTintColor:UIColorFromRGB(0xEEE8E4)];
    [self.navigationController.navigationBar setTranslucent:NO];
    [self.navigationItem setTitle:LocalizedString(@"title_signup", nil)];
    [self customBackButton];
    
    self.lblTitle.text = LocalizedString(@"txt_account_verification_title", nil);
    self.lblMessage.text = LocalizedString(@"txt_account_verification_message", nil);
    self.lblOr.text = LocalizedString(@"txt_or", nil);
    
    self.txtConfirmationCode.placeholder = LocalizedString(@"txt_confirmation_code", nil);
    
    self.viewConfirmationCode.layer.borderWidth = 1.f;
    self.viewConfirmationCode.layer.borderColor = UIColorFromRGB(0xF2EEEB).CGColor;
    
    self.btnVerify.layer.cornerRadius = 5.f;
    [self.btnVerify setTitle:LocalizedString(@"btn_verify", nil) forState:UIControlStateNormal];
    
    self.btnResend.layer.cornerRadius = 5.f;
    [self.btnResend setTitle:LocalizedString(@"btn_resend_confirmation_code", nil) forState:UIControlStateNormal];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (IS_IPAD) {
        self.confirmationHeight.constant = 54.0;
        self.btnResendHeight.constant = 54.0;
        self.btnVerifyHeight.constant = 54.0;
        self.logoWidth.constant = 117.0;
        self.logoHeight.constant = 96.0;
        self.lblTitle.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:22.0f];
        self.lblMessage.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:20.0f];
        self.lblOr.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:20.0f];
        self.txtConfirmationCode.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f];
        [self.btnResend.titleLabel setFont:[UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f]];
        [self.btnVerify.titleLabel setFont:[UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f]];
    }
}

- (void)customBackButton
{
    if([self.navigationController.viewControllers count] > 1)
    {
        UIImage *imgBack = [UIImage imageNamed:@"nav_btn_back"];
        UIButton *btnBack = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnBack setImage:imgBack forState:UIControlStateNormal];
        btnBack.frame = CGRectMake(0, 0, imgBack.size.width, imgBack.size.height);
        [btnBack addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
        [btnBack setImageEdgeInsets:UIEdgeInsetsMake(0, -16, 0, 16)];
        UIBarButtonItem *backBarItem = [[UIBarButtonItem alloc] initWithCustomView:btnBack];
        self.navigationItem.leftBarButtonItem = backBarItem;
    }
}

- (void)backAction
{
    [self.navigationController setNavigationBarHidden:YES];
    [self.navigationController popViewControllerAnimated:YES];
}

- (BOOL)validateInputs
{
    ALPValidator *validator = nil;
    
    validator = [ALPValidator validatorWithType:ALPValidatorTypeString];
    [validator addValidationToEnsurePresenceWithInvalidMessage:LocalizedString(@"error_confirmation_code", nil)];
    [validator validate:self.txtConfirmationCode.text];
    if(![validator isValid])
    {
        [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:[[validator errorMessages] objectAtIndex:0] input:self.txtConfirmationCode];
        return NO;
    }
    
    return YES;
}

- (IBAction)resendAction:(id)sender
{
    [SVProgressHUD show];
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setValue:@([shareObject integerForKey:@"user_id"]) forKey:@"user_id"];
    [[WebServiceManager sharedManager] initWithBaseURL:@"api/resendEmailConfirmation" parameters:parameters success:^(id result) {
        
        if([[result objectForKey:@"success"] boolValue])
        {
            [self showAlertWithTitle:LocalizedString(@"title_success", nil) message:[result objectForKey:@"message"] input:nil];
        }
        else
        {
            [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:[result objectForKey:@"message"] input:nil];
        }
        
    } failure:^(NSError *error) {
    }];
}

- (IBAction)nextAction:(id)sender
{
    if([self validateInputs])
    {
        [SVProgressHUD show];
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setValue:@([shareObject integerForKey:@"user_id"]) forKey:@"user_id"];
        [parameters setValue:self.txtConfirmationCode.text forKey:@"code"];
        [[WebServiceManager sharedManager] initWithBaseURL:@"api/confirmEmail" parameters:parameters success:^(id result) {
            
            if([[result objectForKey:@"success"] boolValue])
            {
                [shareObject setBool:[[result objectForKey:@"email_confirm_check"] boolValue] forKey:@"email_confirm_check"];
                [shareObject synchronize];
                
                NSMutableArray *familyList = (NSMutableArray *)[NSKeyedUnarchiver unarchiveObjectWithData:[shareObject objectForKey:@"family_list"]];
                
                if(familyList.count > 0)
                {
                    NSMutableArray *babyList = (NSMutableArray *)[NSKeyedUnarchiver unarchiveObjectWithData:[shareObject objectForKey:@"baby_list"]];
                    
                    if(babyList.count > 0)
                    {
                        [self.navigationController setNavigationBarHidden:YES];
                        [self.view removeFromSuperview];
                        
                        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                        UIViewController *tabBarController = [storyboard instantiateViewControllerWithIdentifier:@"TabBarController"];
                        appDelegate.window.rootViewController = tabBarController;
                    }
                    else
                    {
                        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                        UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"NewBabyViewController"];
                        [self.navigationController pushViewController:viewController animated:YES];
                    }
                }
                else
                {
                    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                    UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"InvitationViewController"];
                    viewController.data = [[NSMutableDictionary alloc] initWithDictionary:@{@"splash_page_check":@(YES)}];
                    [self.navigationController pushViewController:viewController animated:YES];
                }
            }
            else
            {
                [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:[result objectForKey:@"message"] input:nil];
            }
            
        } failure:^(NSError *error) {
            [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:LocalizedString(@"error_unknown", nil) input:nil];
        }];
    }
}


#pragma mark UITextFieldDelegate Methods
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if([textField isEqual:self.txtConfirmationCode])
    {
        [self.btnVerify sendActionsForControlEvents:UIControlEventTouchUpInside];
    }
    return YES;
}

@end
