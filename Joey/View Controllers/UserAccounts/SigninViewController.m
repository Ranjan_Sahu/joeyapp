//
//  SigninViewController.m
//  Joey
//
//  Created by csl on 23/1/2017.
//  Copyright © 2017 Auxilia. All rights reserved.
//

#import "AppDelegate.h"
#import "SigninViewController.h"

@interface SigninViewController ()
{
    NSUserDefaults *shareObject;
    AppDelegate *appDelegate;
    WCSession *wkSession;
}
@end

@implementation SigninViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    shareObject = [NSUserDefaults standardUserDefaults];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    [self.navigationController setNavigationBarHidden:NO];
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:UIColorFromRGB(0x8D8082), NSForegroundColorAttributeName, [UIFont fontWithName:@"SFUIDisplay-Medium" size:18.0f], NSFontAttributeName, nil]];
    [self.navigationController.navigationBar setBarTintColor:UIColorFromRGB(0xEEE8E4)];
    [self.navigationController.navigationBar setTranslucent:NO];
    [self.navigationItem setTitle:LocalizedString(@"title_signin", nil)];
    [self customBackButton];
    
    self.lblTitle.text = LocalizedString(@"txt_signin_title", nil);
    
    self.txtEmail.placeholder = LocalizedString(@"txt_email", nil);
    self.txtPassword.placeholder = LocalizedString(@"txt_password", nil);
    
    self.viewEmail.layer.borderWidth = 1.f;
    self.viewEmail.layer.borderColor = UIColorFromRGB(0xF2EEEB).CGColor;
    self.viewPassword.layer.borderWidth = 1.f;
    self.viewPassword.layer.borderColor = UIColorFromRGB(0xF2EEEB).CGColor;
    
    self.btnSignin.layer.cornerRadius = 5.f;
    [self.btnSignin setTitle:LocalizedString(@"btn_signin", nil) forState:UIControlStateNormal];
    
    [self.btnForgotPassword setTitle:LocalizedString(@"btn_forgot_password", nil) forState:UIControlStateNormal];
    
//    self.txtEmail.text = @"eyecee@gmail.com";
//    self.txtPassword.text = @"joey123!!";
//    self.txtEmail.text = @"amits.neosoft@gmail.com";
//    self.txtPassword.text = @"secret";

    // For Watch App
    [self checkStatus];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (IS_IPAD) {
        self.lblTitle.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:22.0f];
        self.txtEmail.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f];
        self.txtPassword.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f];
        [self.btnSignin.titleLabel setFont:[UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f]];
        [self.btnForgotPassword.titleLabel setFont:[UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f]];
        self.emailHeight.constant = 54.0;
        self.passWordHeight.constant = 54.0;
        self.logInHeight.constant = 54.0;
        self.forgotPassHeight.constant = 54.0;
        self.logoWidth.constant = 117.0;
        self.logoHeight.constant = 96.0;
    }
    
}
- (void)customBackButton
{
    if([self.navigationController.viewControllers count] > 1)
    {
        UIImage *imgBack = [UIImage imageNamed:@"nav_btn_back"];
        UIButton *btnBack = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnBack setImage:imgBack forState:UIControlStateNormal];
        btnBack.frame = CGRectMake(0, 0, imgBack.size.width, imgBack.size.height);
        [btnBack addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
        [btnBack setImageEdgeInsets:UIEdgeInsetsMake(0, -16, 0, 16)];
        UIBarButtonItem *backBarItem = [[UIBarButtonItem alloc] initWithCustomView:btnBack];
        self.navigationItem.leftBarButtonItem = backBarItem;
    }
}

- (void)backAction
{
    [self.navigationController setNavigationBarHidden:YES];
    [self.navigationController popViewControllerAnimated:YES];
}

- (BOOL)validateInputs
{
    ALPValidator *validator = nil;
    
    validator = [ALPValidator validatorWithType:ALPValidatorTypeString];
    [validator addValidationToEnsureRegularExpressionIsMetWithPattern:@"^[_A-Za-z0-9-+]+(\\.[_A-Za-z0-9-+]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z‌​]{2,10})$" invalidMessage:LocalizedString(@"error_email", nil)];
    [validator validate:self.txtEmail.text];
    if(![validator isValid])
    {
        [self.txtEmail becomeFirstResponder];
        [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:[[validator errorMessages] objectAtIndex:0] input:self.txtEmail];
        return NO;
    }
    
    validator = [ALPValidator validatorWithType:ALPValidatorTypeString];
    [validator addValidationToEnsurePresenceWithInvalidMessage:LocalizedString(@"error_password", nil)];
    [validator validate:self.txtPassword.text];
    if(![validator isValid])
    {
        [self.txtPassword becomeFirstResponder];
        [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:[[validator errorMessages] objectAtIndex:0] input:self.txtPassword];
        return NO;
    }
    
    return YES;
}

- (IBAction)getForgotPasswordAction:(id)sender
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"ForgotPasswordViewController"];
    [self.navigationController pushViewController:viewController animated:YES];
}

- (IBAction)nextAction:(id)sender
{
    if([self validateInputs])
    {
        [SVProgressHUD show];
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setValue:self.txtEmail.text forKey:@"email"];
        [parameters setValue:self.txtPassword.text forKey:@"password"];
        [parameters setValue:appDelegate.deviceToken forKey:@"device_token"];
        
        // For Watch App
        [self transferUserInfoToWatchWithData:parameters];
        
        [[WebServiceManager sharedManager] initWithBaseURL:@"api/login" parameters:parameters success:^(id result) {
            
            if([[result objectForKey:@"success"] boolValue])
            {
                [shareObject setInteger:[[result objectForKey:@"id"] integerValue] forKey:@"user_id"];
                [shareObject setObject:[result objectForKey:@"name"] forKey:@"user_name"];
                [shareObject setObject:[result objectForKey:@"email"] forKey:@"user_email"];
                [shareObject setBool:[[result objectForKey:@"email_confirm_check"] boolValue] forKey:@"email_confirm_check"];
                [shareObject setObject:[result objectForKey:@"access_token"] forKey:@"access_token"];
                [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[result valueForKey:@"unit_info"]] forKey:@"unit_info"];
                //[shareObject setObject:[result objectForKey:@"subscription_details"] forKey:@"subscription_details"];
                [shareObject setObject:[result objectForKey:@"free_trial"] forKey:@"free_trial"];
                [shareObject setObject:[result objectForKey:@"subscription_status"] forKey:@"subscription_status"];
                if([[result valueForKey:@"family_list"] count] > 0)
                {
                    [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableArray *)[result valueForKey:@"family_list"]] forKey:@"family_list"];
                }
                if([[result valueForKey:@"baby_list"] count] > 0)
                {
                    [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableArray *)[result valueForKey:@"baby_list"]] forKey:@"baby_list"];
                }
                [shareObject synchronize];
                
                // Set flag for mobile network use restriction for video upload
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"can_Upload_Using_Mobile_Network"];
                [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"can_Upload_Using_Mobile_Network"];
                
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                if(![shareObject boolForKey:@"email_confirm_check"])
                {
                    UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"AccountVerificationViewController"];
                    [self.navigationController pushViewController:viewController animated:NO];
                }
                else
                {
                    if([[result valueForKey:@"device_list"] count] > 0)
                    {
                        [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableArray *)[result valueForKey:@"device_list"]] forKey:@"device_list"];
                    }
                    if([[result valueForKey:@"family_list"] count] > 0)
                    {
                        if([[result valueForKey:@"baby_list"] count] > 0)
                        {
                            [self.navigationController setNavigationBarHidden:YES];
                            [self.view removeFromSuperview];
                            
                            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                            UIViewController *tabBarController = [storyboard instantiateViewControllerWithIdentifier:@"TabBarController"];
                            appDelegate.window.rootViewController = tabBarController;
                        }
                        else
                        {
                            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                            UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"NewBabyViewController"];
                            [self.navigationController pushViewController:viewController animated:YES];
                        }
                    }
                    else
                    {
                        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                        UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"InvitationViewController"];
                        [self.navigationController pushViewController:viewController animated:YES];
                    }
                }
            }
            else
            {
                [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:[result objectForKey:@"message"] input:nil];
            }
            
        } failure:^(NSError *error) {
            NSLog(@"failed");
            //[self showAlertWithTitle:LocalizedString(@"title_error", nil) message:LocalizedString(@"error_unknown", nil) input:nil];
            NSString *errDesc = [NSString stringWithFormat:@"Error Description : %@", error.localizedDescription];
            [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:errDesc input:nil];
        }];
    }
}


#pragma mark UITextFieldDelegate Methods
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if([textField isEqual:self.txtEmail])
    {
        [self.txtPassword becomeFirstResponder];
    }
    if([textField isEqual:self.txtPassword])
    {
        [self.btnSignin sendActionsForControlEvents:UIControlEventTouchUpInside];
    }
    return YES;
}

#pragma mark - WatchKit Connectivity Handlers
- (void)checkStatus{
    if ([WCSession isSupported]) {
        [WCSession defaultSession].delegate = self;
        wkSession = [WCSession defaultSession];
        wkSession.delegate = self;
        NSLog(@"Session on phone starting");
        [[WCSession defaultSession] activateSession];
    }
    NSLog(@"iphone app starting");
}

- (void) transferUserInfoToWatchWithData : (NSDictionary<NSString*,id>*)userData{
    if (wkSession.reachable) {
        WCSessionUserInfoTransfer *info = [wkSession transferUserInfo:userData];
        NSLog(@"%@",info);
    }
}

- (void)SendMessageToWatch {
    
    NSDictionary *dict = @{
                           @"firstName":@"Hi",
                           @"lastName":@"Joey"
                           };
    if(wkSession.reachable) {
        [wkSession sendMessage:dict replyHandler: ^(NSDictionary<NSString *,id> * replyMessage) {
            dispatch_async(dispatch_get_main_queue(), ^{
                NSLog(@".....replyHandler called --- %@",replyMessage);
            });
        }errorHandler:^(NSError * error) {
                      dispatch_async(dispatch_get_main_queue(), ^{
                          NSLog(@"Error = %@",error.localizedDescription);
                      });
                  }
         ];
    }
}

- (void)session:(WCSession *)session didReceiveMessage:(NSDictionary<NSString *,id> *)message replyHandler:(void (^)(NSDictionary<NSString *,id> *))replyHandler {
    dispatch_async(dispatch_get_main_queue(), ^{
        NSString* message1 = [message objectForKey:@"firstName"];
        NSString* message2 = [message objectForKey:@"lastName"];
        NSLog(@"%@",message1);
        NSLog(@"%@",message2);
    });
    NSLog(@"Reached IOS APP");
}

-(void)session:(WCSession *)session didReceiveMessage:(NSDictionary<NSString *,id> *)message{
    NSLog(@"didReceiveMessage : Reached IOS APP");
}

- (void) session:(WCSession *)session didReceiveUserInfo:(NSDictionary<NSString *,id> *)userInfo{
    NSLog(@"didReceiveUserInfo");
}

- (void) session:(WCSession *)session didFinishUserInfoTransfer:(WCSessionUserInfoTransfer *)userInfoTransfer error:(NSError *)error{
    NSLog(@"didFinishUserInfoTransfer");
}

- (void)session:(nonnull WCSession *)session activationDidCompleteWithState:(WCSessionActivationState)activationState error:(nullable NSError *)error {
    NSLog(@"activationDidCompleteWithState");
}

- (void)sessionDidBecomeInactive:(nonnull WCSession *)session {
    NSLog(@"sessionDidBecomeInactive");
}

- (void)sessionDidDeactivate:(nonnull WCSession *)session {
    NSLog(@"sessionDidDeactivate");
}

@end
