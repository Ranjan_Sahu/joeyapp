//
//  ForgotPasswordViewController.m
//  Joey
//
//  Created by csl on 23/1/2017.
//  Copyright © 2017 Auxilia. All rights reserved.
//

#import "AppDelegate.h"
#import "ForgotPasswordViewController.h"

@interface ForgotPasswordViewController ()
{
    NSUserDefaults *shareObject;
}
@end

@implementation ForgotPasswordViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    shareObject = [NSUserDefaults standardUserDefaults];
    
    [self.navigationController setNavigationBarHidden:NO];
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:UIColorFromRGB(0x8D8082), NSForegroundColorAttributeName, [UIFont fontWithName:@"SFUIDisplay-Medium" size:18.0f], NSFontAttributeName, nil]];
    [self.navigationController.navigationBar setBarTintColor:UIColorFromRGB(0xEEE8E4)];
    [self.navigationController.navigationBar setTranslucent:NO];
    [self.navigationItem setTitle:LocalizedString(@"title_forgot_password", nil)];
    [self customBackButton];
    
    self.lblMessage.text = LocalizedString(@"txt_forgot_password_message", nil);
    self.txtEmail.placeholder = LocalizedString(@"txt_email", nil);
    [self.txtEmail becomeFirstResponder];
    
    self.viewEmail.layer.borderWidth = 1.f;
    self.viewEmail.layer.borderColor = UIColorFromRGB(0xF2EEEB).CGColor;
    
    self.btnResendPassword.layer.cornerRadius = 5.f;
    [self.btnResendPassword setTitle:LocalizedString(@"btn_resend_password", nil) forState:UIControlStateNormal];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (IS_IPAD) {
        self.txtEmail.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f];
        [self.btnResendPassword.titleLabel setFont:[UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f]];
        self.emailHeight.constant = 54.0;
        self.resendPassHeight.constant = 54.0;
    }
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (BOOL)prefersStatusBarHidden
{
    return NO;
}

- (void)customBackButton
{
    if([self.navigationController.viewControllers count] > 1)
    {
        UIImage *imgBack = [UIImage imageNamed:@"nav_btn_back"];
        UIButton *btnBack = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnBack setImage:imgBack forState:UIControlStateNormal];
        btnBack.frame = CGRectMake(0, 0, imgBack.size.width, imgBack.size.height);
        [btnBack addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
        [btnBack setImageEdgeInsets:UIEdgeInsetsMake(0, -16, 0, 16)];
        UIBarButtonItem *backBarItem = [[UIBarButtonItem alloc] initWithCustomView:btnBack];
        self.navigationItem.leftBarButtonItem = backBarItem;
    }
}

- (void)backAction
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (BOOL)validateInputs
{
    ALPValidator *validator = nil;
    
    validator = [ALPValidator validatorWithType:ALPValidatorTypeString];
    [validator addValidationToEnsureRegularExpressionIsMetWithPattern:@"^[_A-Za-z0-9-+]+(\\.[_A-Za-z0-9-+]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z‌​]{2,10})$" invalidMessage:LocalizedString(@"error_email", nil)];
    [validator validate:self.txtEmail.text];
    if(![validator isValid])
    {
        [self.txtEmail becomeFirstResponder];
        [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:[[validator errorMessages] objectAtIndex:0] input:self.txtEmail];
        return NO;
    }
    
    return YES;
}

- (IBAction)nextAction:(id)sender
{
    if([self validateInputs])
    {
        [SVProgressHUD show];
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setValue:self.txtEmail.text forKey:@"email"];
        [[WebServiceManager sharedManager] initWithBaseURL:@"api/forgotPassword" parameters:parameters success:^(id result) {
            
            self.txtEmail.text = @"";
            
            if([[result objectForKey:@"success"] boolValue])
            {
                [self showAlertWithTitle:LocalizedString(@"title_success", nil) message:[result objectForKey:@"message"] input:nil];
                [self backAction];
            }
            else
            {
                [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:[result objectForKey:@"message"] input:nil];
            }
            
        } failure:^(NSError *error) {
            [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:LocalizedString(@"error_unknown", nil) input:nil];
        }];
    }
}


#pragma mark UITextFieldDelegate Methods
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if([textField isEqual:self.txtEmail])
    {
        [self.btnResendPassword sendActionsForControlEvents:UIControlEventTouchUpInside];
    }
    return YES;
}

@end
