//
//  SignupViewController.m
//  Joey
//
//  Created by csl on 23/1/2017.
//  Copyright © 2017 Auxilia. All rights reserved.
//

#import "AppDelegate.h"
#import "SignupViewController.h"
#import <ChimpKit/ChimpKit.h>

@interface SignupViewController ()<ChimpKitRequestDelegate>
{
    NSUserDefaults *shareObject;
    AppDelegate *appDelegate;
    NSMutableDictionary *parameters;
    BOOL termsCheck;
}
@end

@implementation SignupViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    shareObject = [NSUserDefaults standardUserDefaults];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    parameters = [NSMutableDictionary dictionary];
    if(self.data) parameters = [self.data objectForKey:@"parameters"];
    
    [self.navigationController setNavigationBarHidden:NO];
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:UIColorFromRGB(0x8D8082), NSForegroundColorAttributeName, [UIFont fontWithName:@"SFUIDisplay-Medium" size:18.0f], NSFontAttributeName, nil]];
    [self.navigationController.navigationBar setBarTintColor:UIColorFromRGB(0xEEE8E4)];
    [self.navigationController.navigationBar setTranslucent:NO];
    [self.navigationItem setTitle:LocalizedString(@"title_signup", nil)];
    [self customBackButton];
    
    self.lblTitle.text = LocalizedString(@"txt_signup_title", nil);
    self.txtFullname.placeholder = LocalizedString(@"txt_fullname", nil);
    self.txtEmail.placeholder = LocalizedString(@"txt_email", nil);
    self.txtPassword.placeholder = LocalizedString(@"txt_password", nil);
    self.txtRetypePassword.placeholder = LocalizedString(@"txt_reenter_password", nil);
    
    if(self.data)
    {
        self.txtFullname.text = [parameters objectForKey:@"name"];
        self.txtEmail.text = [parameters objectForKey:@"email"];
        self.txtEmail.enabled = NO;
    }
    
    self.viewFullname.layer.borderWidth = 1.f;
    self.viewFullname.layer.borderColor = UIColorFromRGB(0xF2EEEB).CGColor;
    self.viewEmail.layer.borderWidth = 1.f;
    self.viewEmail.layer.borderColor = UIColorFromRGB(0xF2EEEB).CGColor;
    self.viewPassword.layer.borderWidth = 1.f;
    self.viewPassword.layer.borderColor = UIColorFromRGB(0xF2EEEB).CGColor;
    self.viewRetypePassword.layer.borderWidth = 1.f;
    self.viewRetypePassword.layer.borderColor = UIColorFromRGB(0xF2EEEB).CGColor;
    self.btnSignup.layer.cornerRadius = 5.f;
    [self.btnSignup setTitle:LocalizedString(@"btn_signup", nil) forState:UIControlStateNormal];
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
    
    if (IS_IPAD) {
        self.fullNameHeight.constant = 54.0;
        self.emailHeight.constant = 54.0;
        self.passHeight.constant = 54.0;
        self.reTypePassHeight.constant = 54.0;
        self.signUpHeight.constant = 54.0;
        self.logoWidth.constant = 117.0;
        self.logoHeight.constant = 96.0;
        self.lblTitle.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:22.0f];
        [self.btnSignup.titleLabel setFont:[UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f]];
        self.txtFullname.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f];
        self.txtEmail.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:       21.0f];
        self.txtPassword.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f];
        self.txtRetypePassword.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f];
    }
    
}

- (void)customBackButton
{
    if([self.navigationController.viewControllers count] > 1)
    {
        UIImage *imgBack = [UIImage imageNamed:@"nav_btn_back"];
        UIButton *btnBack = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnBack setImage:imgBack forState:UIControlStateNormal];
        btnBack.frame = CGRectMake(0, 0, imgBack.size.width, imgBack.size.height);
        [btnBack addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
        [btnBack setImageEdgeInsets:UIEdgeInsetsMake(0, -16, 0, 16)];
        UIBarButtonItem *backBarItem = [[UIBarButtonItem alloc] initWithCustomView:btnBack];
        self.navigationItem.leftBarButtonItem = backBarItem;
    }
}

- (void)backAction
{
    [self.navigationController setNavigationBarHidden:YES];
    [self.navigationController popViewControllerAnimated:YES];
}

- (BOOL)validateInputs
{
    ALPValidator *validator = nil;
    
    validator = [ALPValidator validatorWithType:ALPValidatorTypeString];
    [validator addValidationToEnsurePresenceWithInvalidMessage:LocalizedString(@"error_full_name", nil)];
    [validator validate:self.txtFullname.text];
    if(![validator isValid])
    {
        [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:[[validator errorMessages] objectAtIndex:0] input:self.txtFullname];
        return NO;
    }
    
    validator = [ALPValidator validatorWithType:ALPValidatorTypeString];
    [validator addValidationToEnsureRegularExpressionIsMetWithPattern:@"^[_A-Za-z0-9-+]+(\\.[_A-Za-z0-9-+]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z‌​]{2,10})$" invalidMessage:LocalizedString(@"error_email", nil)];
    [validator validate:self.txtEmail.text];
    if(![validator isValid])
    {
        [self.txtEmail becomeFirstResponder];
        [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:[[validator errorMessages] objectAtIndex:0] input:self.txtEmail];
        return NO;
    }
    
    validator = [ALPValidator validatorWithType:ALPValidatorTypeString];
    [validator addValidationToEnsurePresenceWithInvalidMessage:LocalizedString(@"error_password", nil)];
    [validator addValidationToEnsureMinimumLength:6 invalidMessage:LocalizedString(@"error_password_min", nil)];
    [validator addValidationToEnsureMaximumLength:50 invalidMessage:LocalizedString(@"error_password_max", nil)];
    [validator validate:self.txtPassword.text];
    if(![validator isValid])
    {
        [self.txtPassword becomeFirstResponder];
        [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:[[validator errorMessages] objectAtIndex:0] input:self.txtPassword];
        return NO;
    }
    
    validator = [ALPValidator validatorWithType:ALPValidatorTypeString];
    [validator addValidationToEnsureInstanceIsTheSameAs:self.txtPassword.text invalidMessage:LocalizedString(@"error_retype_not_match_password", nil)];
    [validator validate:self.txtRetypePassword.text];
    if(![validator isValid])
    {
        [self.txtPassword becomeFirstResponder];
        [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:[[validator errorMessages] objectAtIndex:0] input:self.txtRetypePassword];
        return NO;
    }
    
    return YES;
}


- (IBAction)nextAction:(id)sender
{
    if([self validateInputs])
    {
        [SVProgressHUD show];
        [parameters setValue:self.txtFullname.text forKey:@"name"];
        [parameters setValue:self.txtEmail.text forKey:@"email"];
        [parameters setValue:self.txtPassword.text forKey:@"password"];
        [parameters setValue:appDelegate.deviceToken forKey:@"device_token"];
        [[WebServiceManager sharedManager] initWithBaseURL:@"api/signup" parameters:parameters success:^(id result) {

            if([[result objectForKey:@"success"] boolValue]){
                [shareObject setInteger:[[result objectForKey:@"id"] integerValue] forKey:@"user_id"];
                [shareObject setObject:[result objectForKey:@"name"] forKey:@"user_name"];
                [shareObject setObject:[result objectForKey:@"email"] forKey:@"user_email"];
                [shareObject setBool:[[result objectForKey:@"email_confirm_check"] integerValue] forKey:@"email_confirm_check"];
                [shareObject setObject:[result objectForKey:@"access_token"] forKey:@"access_token"];
                [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[result valueForKey:@"unit_info"]] forKey:@"unit_info"];

                [shareObject synchronize];

                // MailChimp
                NSString *listId = @"32964100cf";
                NSString *uEmial = self.txtEmail.text;
                NSString *uName = self.txtFullname.text;
                NSString *lName = @"";
                NSDictionary *paramDict = @{@"id": listId, @"email": @{@"email": uEmial}, @"merge_vars": @{@"FNAME": uName, @"LName":lName}};
                
                [[ChimpKit sharedKit] setShouldUseBackgroundThread:YES];
                [[ChimpKit sharedKit] setTimeoutInterval:30.0f];
                [[ChimpKit sharedKit] callApiMethod:@"lists/subscribe" withParams:paramDict andDelegate:self];
                
                [self sendUserToNextScreen];
            }
            else{
                [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:LocalizedString([result objectForKey:@"message"], nil) input:nil];
            }

        } failure:^(NSError *error) {
            [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:LocalizedString(@"error_unknown", nil) input:nil];
        }];
    }
}

#pragma mark UITextFieldDelegate Methods
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if([textField isEqual:self.txtFullname])
    {
        [self.txtEmail becomeFirstResponder];
    }
    if([textField isEqual:self.txtEmail])
    {
        [self.txtPassword becomeFirstResponder];
    }
    if([textField isEqual:self.txtPassword])
    {
        [self.txtRetypePassword becomeFirstResponder];
    }
    if([textField isEqual:self.txtRetypePassword])
    {
        [self.view endEditing:YES];
    }
    return YES;
}

#pragma mark - ChimpKit Delegate
-(void)ckRequestIdentifier:(NSUInteger)requestIdentifier didSucceedWithResponse:(NSURLResponse *)response andData:(NSData *)data
{
    //NSLog(@"HTTP Status Code: %d", response.statusCode);
    NSLog(@"Response String: %lu", (unsigned long)requestIdentifier);
    
    NSError *parseError = nil;
    id responseData = [NSJSONSerialization JSONObjectWithData:data options:0 error:&parseError];
    if ([responseData isKindOfClass:[NSDictionary class]]) {
        id email = [responseData objectForKey:@"email"];
        if ([email isKindOfClass:[NSString class]]) {
            //Successfully subscribed email address
            dispatch_async(dispatch_get_main_queue(), ^{
                //Update UI here
                //[self sendUserToNextScreen];
            });
        }
    }
}

-(void)ckRequestFailedWithIdentifier:(NSUInteger)requestIdentifier andError:(NSError *)anError
{
    //Handle connection error
    NSLog(@"Error, %@", anError);
    dispatch_async(dispatch_get_main_queue(), ^{
        //Update UI here
        //[self sendUserToNextScreen];
    });
}

#pragma mark - Custom Methods
-(void)sendUserToNextScreen
{
    [self.navigationController setNavigationBarHidden:YES];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"AccountVerificationViewController"];
    [self.navigationController pushViewController:viewController animated:YES];
    
}

@end
