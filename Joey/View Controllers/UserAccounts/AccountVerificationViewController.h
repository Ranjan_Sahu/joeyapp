//
//  AccountVerificationViewController.h
//  Joey
//
//  Created by csl on 21/3/2017.
//  Copyright © 2017 Auxilia. All rights reserved.
//

#import "BaseViewController.h"

@interface AccountVerificationViewController : BaseViewController

@property (nonatomic, weak) IBOutlet UILabel *lblTitle;
@property (nonatomic, weak) IBOutlet UILabel *lblMessage;
@property (nonatomic, weak) IBOutlet UILabel *lblOr;
@property (nonatomic, weak) IBOutlet UITextField *txtConfirmationCode;
@property (nonatomic, weak) IBOutlet UIView *viewConfirmationCode;
@property (nonatomic, weak) IBOutlet UIButton *btnVerify;
@property (nonatomic, weak) IBOutlet UIButton *btnResend;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *confirmationHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *btnVerifyHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *btnResendHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *logoWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *logoHeight;

@end
