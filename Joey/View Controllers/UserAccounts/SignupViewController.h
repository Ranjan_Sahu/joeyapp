//
//  SignupViewController.h
//  Joey
//
//  Created by csl on 23/1/2017.
//  Copyright © 2017 Auxilia. All rights reserved.
//

#import "BaseViewController.h"
#import <FRHyperLabel/FRHyperLabel.h>

@interface SignupViewController : BaseViewController

@property (nonatomic, weak) IBOutlet UILabel *lblTitle;
@property (nonatomic, weak) IBOutlet UITextField *txtFullname;
@property (nonatomic, weak) IBOutlet UITextField *txtEmail;
@property (nonatomic, weak) IBOutlet UITextField *txtPassword;
@property (nonatomic, weak) IBOutlet UITextField *txtRetypePassword;

@property (nonatomic, weak) IBOutlet UIView *viewFullname;
@property (nonatomic, weak) IBOutlet UIView *viewEmail;
@property (nonatomic, weak) IBOutlet UIView *viewPassword;
@property (nonatomic, weak) IBOutlet UIView *viewRetypePassword;
@property (nonatomic, weak) IBOutlet UIButton *btnSignup;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *fullNameHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *emailHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *passHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *reTypePassHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *signUpHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *logoHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *logoWidth;

@end
