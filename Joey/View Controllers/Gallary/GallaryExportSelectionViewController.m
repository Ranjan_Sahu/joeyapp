//
//  GallaryExportSelectionViewController.m
//  Joey
//
//  Created by webwerks on 2/9/18.
//  Copyright © 2018 Auxilia. All rights reserved.
//

#import "GallaryExportSelectionViewController.h"
#import "AppDelegate.h"
#import "gallaryImageCollectionViewCell.h"
#import "GallaryExportViewController.h"

@interface GallaryExportSelectionViewController ()
{
    NSUserDefaults *shareObject;
    AppDelegate *appDelegate;
    NSMutableArray *arrSelectedImages;
    NSMutableArray *arrSelectedDataBlocks;
    NSDateFormatter *formatter;
    NSCalendar *calendar;
    NSDate *selectedStartDate;
    NSDate *selectedEndDate;
    NSMutableArray *arrGallaryList;
    NSMutableArray *searchResult;
    NSMutableDictionary *searchParam;
    
    
}
@end

@implementation GallaryExportSelectionViewController

#pragma mark - View Controller Methods
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    arrSelectedDataBlocks = [[NSMutableArray alloc] init];
    shareObject = [NSUserDefaults standardUserDefaults];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:UIColorFromRGB(0x8D8082), NSForegroundColorAttributeName, [UIFont fontWithName:@"SFUIDisplay-Medium" size:18.0f], NSFontAttributeName, nil]];
    [self.navigationController.navigationBar setBarTintColor:UIColorFromRGB(0xEEE8E4)];
    [self.navigationController.navigationBar setTranslucent:NO];
    
    [self.navigationItem setTitle:LocalizedString(@"Gallery Export", nil)];
    self.btnSave.title = @"Next";
    [self customBackButton];
    
    formatter = [[NSDateFormatter alloc] init];
    [formatter setTimeZone:[NSTimeZone localTimeZone]];
    [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
    //calendar = [[NSCalendar alloc] initWithCalendarIdentifier: NSCalendarIdentifierGregorian];
    calendar = [NSCalendar currentCalendar];
    
    NSDateComponents *components = [calendar components:NSUIntegerMax fromDate:[NSDate date]];
    [components setHour:0];
    [components setMinute:0];
    [components setSecond:0];
    selectedEndDate = [calendar dateFromComponents:components];
    NSLog(@"selectedStartDate=%@", selectedEndDate);
    
    NSDate *oneMonthBackDate = [calendar dateByAddingUnit:NSCalendarUnitMonth value:-5 toDate:[NSDate date] options:0];
    components = [calendar components:NSUIntegerMax fromDate:oneMonthBackDate];
    selectedStartDate = [calendar dateFromComponents:components];
    NSLog(@"selectedEndDate=%@", selectedStartDate);
    
    if([[shareObject objectForKey:@"language"] hasPrefix:@"zh"])
    {
        [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"zh_HK"]];
        [formatter setDateStyle:NSDateFormatterMediumStyle];
        [formatter setTimeStyle:NSDateFormatterNoStyle];
    }
    else
    {
        [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
        formatter.dateFormat = @"EE d MMM, yyyy";
    }
    self.lblStartTxt.text = [NSString stringWithFormat:@"%@", [formatter stringFromDate:selectedStartDate]];
    self.lblEndTxt.text = [NSString stringWithFormat:@"%@", [formatter stringFromDate:selectedEndDate]];

    UITapGestureRecognizer *tapRecognizer1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTapOnTxtStart)];
    tapRecognizer1.numberOfTapsRequired = 1;
    self.lblStartTxt.userInteractionEnabled = YES;
    [self.lblStartTxt addGestureRecognizer:tapRecognizer1];
    
    UITapGestureRecognizer *tapRecognizer2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTapOnTxtEnd)];
    tapRecognizer2.numberOfTapsRequired = 1;
    self.lblEndTxt.userInteractionEnabled = YES;
    [self.lblEndTxt addGestureRecognizer:tapRecognizer2];

    arrGallaryList = [[NSMutableArray alloc] init];
    arrGallaryList = [self.data objectForKey:@"gallary_List"];
    searchResult = [[NSMutableArray alloc] initWithArray:arrGallaryList];
    
    [self.collectionView registerNib:[UINib nibWithNibName:@"gallaryImageCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"gallaryImageCollectionViewCell"];
    self.collectionView.dataSource = self;
    self.collectionView.delegate = self;
    self.collectionView.alwaysBounceVertical = YES;
    
    __weak __typeof(self)weakSelf = self;
    [self.collectionView addPullToRefreshWithActionHandler:^{
        [weakSelf refreshAction];
    }];
    
    arrSelectedImages = [[NSMutableArray alloc] initWithCapacity:50];
    [self setupBabyList];
    [self refreshAction];
    
    if (IS_IPAD) {
        self.lblStart.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.lblEnd.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.lblStartTxt.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:22.0f];
        self.lblEndTxt.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:22.0f];
        
        self.lblHeight.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.lblWeight.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.lblAge.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.lblDevCaledar.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];

    }
    
    self.filterViewHeight.constant = (self.view.bounds.size.width / 4.0f);
    self.btnHeightCheck.hidden = YES;
    self.btnWeightCheck.hidden = YES;
    self.btnAgeCheck.hidden = YES;
    self.btnDevCalCheck.hidden = YES;
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:(BOOL)animated];
    [self.navigationController.navigationBar setBarTintColor:UIColorFromRGB(0xEEE8E4)];
    NSLog(@"here=%@", self.data);
        
    self.btnHeightCheck.layer.cornerRadius = 12;
    self.btnWeightCheck.layer.cornerRadius = 12;
    self.btnAgeCheck.layer.cornerRadius = 12;
    self.btnDevCalCheck.layer.cornerRadius = 12;
    
    self.lblHeight.layer.cornerRadius = 4;
    self.lblHeight.layer.borderWidth = 1.0;
    self.lblHeight.layer.borderColor = UIColorFromRGB(0x8D8082).CGColor;
    
    self.lblWeight.layer.cornerRadius = 4;
    self.lblWeight.layer.borderWidth = 1.0;
    self.lblWeight.layer.borderColor = UIColorFromRGB(0x8D8082).CGColor;

    self.lblAge.layer.cornerRadius = 4;
    self.lblAge.layer.borderWidth = 1.0;
    self.lblAge.layer.borderColor = UIColorFromRGB(0x8D8082).CGColor;

    self.lblDevCaledar.layer.cornerRadius = 4;
    self.lblDevCaledar.layer.borderWidth = 1.0;
    self.lblDevCaledar.layer.borderColor = UIColorFromRGB(0x8D8082).CGColor;

}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

#pragma mark - UI Customization & Layout Hadling Methods
- (void)customBackButton
{
    if([self.navigationController.viewControllers count] > 1)
    {
        UIImage *imgBack = [UIImage imageNamed:@"nav_btn_back"];
        UIButton *btnBack = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnBack setImage:imgBack forState:UIControlStateNormal];
        btnBack.frame = CGRectMake(0, 0, imgBack.size.width, imgBack.size.height);
        [btnBack addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
        [btnBack setImageEdgeInsets:UIEdgeInsetsMake(0, -16, 0, 16)];
        UIBarButtonItem *backBarItem = [[UIBarButtonItem alloc] initWithCustomView:btnBack];
        self.navigationItem.leftBarButtonItem = backBarItem;
    }
}

#pragma mark - Custom Actions
- (void)backAction
{
    [self.navigationController popViewControllerAnimated:YES];
}

//- (void)didSelectHeight
//{
//    self.btnHeightCheck.hidden = !self.btnHeightCheck.hidden;
//    [self filterGallaryContentWith];
//}
//- (void)didSelectWeight
//{
//    self.btnWeightCheck.hidden = !self.btnWeightCheck.hidden;
//    [self filterGallaryContentWith];
//}
//- (void)didSelectAge
//{
//    self.btnAgeCheck.hidden = !self.btnAgeCheck.hidden;
//    [self filterGallaryContentWith];
//}
//- (void)didSelectDevCalendar
//{
//    self.btnDevCalCheck.hidden = !self.btnDevCalCheck.hidden;
//    [self filterGallaryContentWith];
//}

-(void)didTapOnTxtStart
{
    NSDate *minimumDate = nil;
    NSDate *maximumDate = [NSDate date];
    [ActionSheetDatePicker showPickerWithTitle:@"" datePickerMode:UIDatePickerModeDate selectedDate:selectedStartDate minimumDate:minimumDate maximumDate:maximumDate doneBlock:^(ActionSheetDatePicker *picker, id selectedDate, id origin) {
        
        selectedStartDate = selectedDate;
        
        NSDateComponents *components = [calendar components:NSUIntegerMax fromDate:selectedStartDate];
        [components setHour:0];
        [components setMinute:0];
        [components setSecond:0];
        selectedStartDate = [calendar dateFromComponents:components];
        NSLog(@"selectedStartDate=%@", selectedStartDate);
        
        if([[shareObject objectForKey:@"language"] hasPrefix:@"zh"])
        {
            [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"zh_HK"]];
            [formatter setDateStyle:NSDateFormatterMediumStyle];
            [formatter setTimeStyle:NSDateFormatterNoStyle];
        }
        else
        {
            [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
            formatter.dateFormat = @"EE d MMM, yyyy";
        }
        self.lblStartTxt.text = [formatter stringFromDate:selectedStartDate];
        [self filterGallarybyDate];
        
    } cancelBlock:^(ActionSheetDatePicker *picker) {
        
    } origin:self.view];
}

-(void)didTapOnTxtEnd
{
    NSDate *minimumDate = selectedStartDate;
    NSDate *maximumDate = [NSDate date];
    [ActionSheetDatePicker showPickerWithTitle:@"" datePickerMode:UIDatePickerModeDate selectedDate:selectedEndDate minimumDate:minimumDate maximumDate:maximumDate doneBlock:^(ActionSheetDatePicker *picker, id selectedDate, id origin) {
        
        selectedEndDate = selectedDate;
        NSLog(@"selectedEndDate=%@", selectedEndDate);
        NSDateComponents *components = [calendar components:NSUIntegerMax fromDate:selectedEndDate];
        [components setHour:23];
        [components setMinute:59];
        [components setSecond:59];
        selectedEndDate = [calendar dateFromComponents:components];
        NSLog(@"selectedEndDate=%@", selectedEndDate);
        
        if([[shareObject objectForKey:@"language"] hasPrefix:@"zh"])
        {
            [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"zh_HK"]];
            [formatter setDateStyle:NSDateFormatterMediumStyle];
            [formatter setTimeStyle:NSDateFormatterNoStyle];
        }
        else
        {
            [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
            formatter.dateFormat = @"EE d MMM, yyyy";
        }
        self.lblEndTxt.text = [formatter stringFromDate:selectedEndDate];
        [self filterGallarybyDate];
        
    } cancelBlock:^(ActionSheetDatePicker *picker) {
        
    } origin:self.view];
}

#pragma mark - IBAction Methods
- (IBAction)nextAction:(id)sender
{
    
    //NSMutableArray *arrSelectedDataBlocks = [[NSMutableArray alloc] init];
    NSLog(@"arrSelectedImages %lu",(unsigned long)arrSelectedImages.count);
    [arrSelectedDataBlocks removeAllObjects];
    NSMutableArray *selectedImagesArray = [[NSMutableArray alloc] init];
    
    if (!self.btnHeightCheck.hidden) {
        [arrSelectedDataBlocks addObject:@"height"];
        //UIImage *postImage = [UIImage imageNamed:@"gallery_data_height_bg.png"];
        //[arrSelectedImages addObject:postImage];
        
        
    }
    if (!self.btnWeightCheck.hidden) {
        [arrSelectedDataBlocks addObject:@"weight"];
         //UIImage *postImage = [UIImage imageNamed:@"gallery_data_weight_bg"];
        //[arrSelectedImages addObject:postImage];
        
    }
    if (!self.btnAgeCheck.hidden) {
        [arrSelectedDataBlocks addObject:@"age"];
        //UIImage *postImage = [UIImage imageNamed:@"gallery_data_age_bg"];
        //[arrSelectedImages addObject:postImage];
        
    }
    if (!self.btnDevCalCheck.hidden) {
        [arrSelectedDataBlocks addObject:@"achievement"];
        //UIImage *postImage = [UIImage imageNamed:@"bg_welcome"];
        //[arrSelectedImages addObject:postImage];
        
    }
    
    NSString *selectedBlocks = @"";
    selectedBlocks = [arrSelectedDataBlocks componentsJoinedByString:@","];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"GallaryExportViewController"];
    viewController.hidesBottomBarWhenPushed = YES;
    
    //NSMutableArray *selectedImagesArray = [[NSMutableArray alloc] init];
    for (int i = 0; i<arrSelectedImages.count; i++) {
        NSIndexPath *indexPath = (NSIndexPath *)[arrSelectedImages objectAtIndex:i];
        [selectedImagesArray addObject:[searchResult objectAtIndex:indexPath.row]];
    }
    
    NSLog(@"selectedImagesArray %@",selectedImagesArray);
    
    
    
    NSLog(@"selectedImagesArray %@",selectedImagesArray);
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:selectedImagesArray forKey:@"selected_Images_Data"];
    [dict setObject:selectedBlocks forKey:@"selected_Data_Blocks"];
    [dict setObject:selectedStartDate forKey:@"selected_Start_Date"];
    [dict setObject:selectedEndDate forKey:@"selected_End_Date"];
    [dict setObject:arrSelectedDataBlocks forKey:@"arr_selected_Data_Blocks"]; //Anil
    
    viewController.data = dict;
    
    [self.navigationController pushViewController:viewController animated:YES];
}

// Anil
-(NSURL *)getUrlFromData :(NSString *)imageName{
    
    //UIImage *postImage = [UIImage imageNamed:@"gallery_data_height_bg.png"];
    UIImage *postImage = [UIImage imageNamed:imageName];
    NSData *data = UIImageJPEGRepresentation(postImage, 1.0);
    
    NSString *yourstring = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSURL *url = [[NSURL alloc] initWithString:yourstring];
    NSLog(@"Print URL %@",url);
    
    return url;
    
}
- (IBAction)didSelectFilter:(UIButton *)sender
{
    
    if (sender.tag == 1) {
        //self.btnHeightCheck.hidden = !self.btnHeightCheck.hidden;
        
        if (self.btnHeightCheck.hidden == YES) {
            
            if (arrSelectedImages.count + arrSelectedDataBlocks.count <9) {
                
                self.btnHeightCheck.hidden = NO;
                
                [arrSelectedDataBlocks addObject:sender];
            }
            else {
                NSLog(@"maximum");
            }
            
        }else {
            
            self.btnHeightCheck.hidden = YES;
            
            [arrSelectedDataBlocks removeObject:sender];
        }
        
        
    }else if (sender.tag == 2) {
        //self.btnWeightCheck.hidden = !self.btnWeightCheck.hidden;
        
        if (self.btnWeightCheck.hidden == YES) {
            
            if (arrSelectedImages.count + arrSelectedDataBlocks.count<9) {
                
                self.btnWeightCheck.hidden = NO;
                
                [arrSelectedDataBlocks addObject:sender];
            }
            else {
                NSLog(@"maximum");
            }
            
        }else {
            
            self.btnWeightCheck.hidden = YES;
            
            [arrSelectedDataBlocks removeObject:sender];
        }
    }else if (sender.tag == 3) {
        //self.btnAgeCheck.hidden = !self.btnAgeCheck.hidden;
        if (self.btnAgeCheck.hidden == YES) {
            
            if (arrSelectedImages.count + arrSelectedDataBlocks.count <9) {
                
                self.btnAgeCheck.hidden = NO;
                
                [arrSelectedDataBlocks addObject:sender];
            }
            else {
                NSLog(@"maximum");
            }
            
        }else {
            
            self.btnAgeCheck.hidden = YES;
            
            [arrSelectedDataBlocks removeObject:sender];
        }
    }else if (sender.tag == 4) {
        //self.btnDevCalCheck.hidden = !self.btnDevCalCheck.hidden;
        if (self.btnDevCalCheck.hidden == YES) {
            
            if (arrSelectedImages.count + arrSelectedDataBlocks.count <9) {
                
                self.btnDevCalCheck.hidden = NO;
                
                [arrSelectedDataBlocks addObject:sender];
            }
            else {
                NSLog(@"maximum");
            }
            
        }else {
            
            self.btnDevCalCheck.hidden = YES;
            
            [arrSelectedDataBlocks removeObject:sender];
        }
    }
    
//    if (sender.tag == 1) {
//        if (arrSelectedImages.count<9) {
//
//            //self.btnHeightCheck.hidden = !self.btnHeightCheck.hidden;
//            //Anil
//            if(!self.btnHeightCheck.hidden){
//                NSLog(@"arrSelectedImages count %lu",(unsigned long)arrSelectedImages.count);
//                [arrSelectedImages addObject:sender];
//                self.btnHeightCheck.hidden = !self.btnHeightCheck.hidden;
//
//            }
//
//            else if(self.btnHeightCheck.hidden){
//                [arrSelectedImages removeLastObject];
//                self.btnHeightCheck.hidden = !self.btnHeightCheck.hidden;
//            }
//        }
//
//
//    }else if (sender.tag == 2) {
//        if (arrSelectedImages.count<9) {
//        //self.btnWeightCheck.hidden = !self.btnWeightCheck.hidden;
//
//        if(!self.btnHeightCheck.hidden){
//            if (arrSelectedImages.count<9) {
//                NSLog(@"arrSelectedImages count %lu",(unsigned long)arrSelectedImages.count);
//            [arrSelectedImages addObject:sender];
//            self.btnWeightCheck.hidden = !self.btnWeightCheck.hidden;
//            }
//        }
//
//        else if(self.btnHeightCheck.hidden){
//            [arrSelectedImages removeLastObject];
//            self.btnWeightCheck.hidden = !self.btnWeightCheck.hidden;
//        }
//    }
//    }else if (sender.tag == 3) {
//        if (arrSelectedImages.count<9) {
//        //self.btnAgeCheck.hidden = !self.btnAgeCheck.hidden;
//
//        if(!self.btnHeightCheck.hidden){
//            if (arrSelectedImages.count<9) {
//                NSLog(@"arrSelectedImages count %lu",(unsigned long)arrSelectedImages.count);
//            [arrSelectedImages addObject:sender];
//                self.btnAgeCheck.hidden = !self.btnAgeCheck.hidden;
//            }
//        }
//
//        else if(self.btnHeightCheck.hidden){
//            [arrSelectedImages removeLastObject];
//            self.btnAgeCheck.hidden = !self.btnAgeCheck.hidden;
//        }
//    }
//    }else if (sender.tag == 4) {
//        if (arrSelectedImages.count<9) {
//       // self.btnDevCalCheck.hidden = !self.btnDevCalCheck.hidden;
//
//        if(!self.btnHeightCheck.hidden){
//            if (arrSelectedImages.count<9) {
//                NSLog(@"arrSelectedImages count %lu",(unsigned long)arrSelectedImages.count);
//            [arrSelectedImages addObject:sender];
//            self.btnDevCalCheck.hidden = !self.btnDevCalCheck.hidden;
//
//            }
//        }
//
//        else if(self.btnHeightCheck.hidden){
//            [arrSelectedImages removeLastObject];
//            self.btnDevCalCheck.hidden = !self.btnDevCalCheck.hidden;
//
//        }
//        // Anil
//    }
//    }
    //[self filterGallaryContentWith];
}

#pragma mark - Collection view Data Source & Delegates
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (searchResult.count>0) {
        return searchResult.count;
    }else {
        return 0;
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    gallaryImageCollectionViewCell *cell;
    if (cell) {
        cell = nil;
    }
    cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"gallaryImageCollectionViewCell" forIndexPath:indexPath];
    
    NSDictionary *imgDict = [searchResult  objectAtIndex:indexPath.row];
    NSString *imgUrl =  [imgDict valueForKey:@"photo_url"];
    
    cell.imgView.image = nil;
    cell.imgView.backgroundColor = UIColorFromRGB(0xD9CEC4);
    if ([[imgDict valueForKey:@"photo_url"] isKindOfClass:[NSNull class]]) {
        cell.imgView.image = [UIImage imageNamed:@"dummy.png"];
    }else {
        [cell.imgView sd_setImageWithURL:[NSURL URLWithString:imgUrl] placeholderImage:[UIImage imageNamed:@"dummy.png"]];
    }
    
    if ([arrSelectedImages containsObject:indexPath])
    {
        cell.btnSelect.hidden = NO;
        cell.imgView.alpha = 0.7;
        cell.imgView.transform = CGAffineTransformMakeScale(0.9, 0.9);
    }
    else
    {
        cell.btnSelect.hidden = YES;
        cell.imgView.alpha = 1.0;
        cell.imgView.transform = CGAffineTransformMakeScale(1.0, 1.0);
    }
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static gallaryImageCollectionViewCell *sizingCell = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sizingCell = [[NSBundle mainBundle] loadNibNamed:@"gallaryImageCollectionViewCell" owner:self options:nil][0];
    });
    
    int w = (self.collectionView.bounds.size.width-4) / 4.0f;
    return CGSizeMake(w, w);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(2, 2, 2, 2);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSLog(@"arrSelectedImages count--- %lu",(unsigned long)arrSelectedImages.count);
    gallaryImageCollectionViewCell *cell = (gallaryImageCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
    
    if (cell.btnSelect.hidden == YES) {
        
        if (arrSelectedImages.count + arrSelectedDataBlocks.count <9) {
            cell.imgView.alpha = 0.7;
            cell.btnSelect.hidden = NO;
            cell.imgView.transform = CGAffineTransformMakeScale(0.9, 0.9);
            [arrSelectedImages addObject:indexPath];
        }
        else {
            NSLog(@"maximum");
        }
        
    }else {
        
        cell.btnSelect.hidden = YES;
        cell.imgView.alpha = 1.0;
        cell.imgView.transform = CGAffineTransformMakeScale(1.0, 1.0);
        [arrSelectedImages removeObject:indexPath];
    }

}

#pragma mark -  View's Data Configuration methods
-(void) refreshAction
{
    [SVProgressHUD show];
    [SVProgressHUD dismiss];
    [self.collectionView.pullToRefreshView stopAnimating];
}

- (void)setupBabyList
{
    
}

#pragma mark -  Custom methods
-(void)filterGallarybyDate
{
        if ((selectedStartDate != nil) && (selectedEndDate != nil)) {

            [searchResult removeAllObjects];

            for (NSDictionary *dict in arrGallaryList) {
                NSString *dateStr = [dict valueForKey:@"created_at"];
                NSDateFormatter *frmtr = [NSDateFormatter new];
                [frmtr setTimeZone:[NSTimeZone localTimeZone]];
                [frmtr setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
                [frmtr setDateFormat:@"yyyy-MM-dd hh:mm:ss "];

               NSDate *date = [frmtr dateFromString:dateStr];

                if (date != nil) {
                    if (([date compare:selectedStartDate] == NSOrderedDescending) || ([date compare:selectedStartDate] == NSOrderedSame)) {

                        if (([date compare:selectedEndDate] == NSOrderedAscending) || ([date compare:selectedEndDate] == NSOrderedSame)) {
                            [searchResult addObject:dict];
                        }

                    }
                }
            }
            [self.collectionView reloadData];
        }
}

- (void)filterGallaryContentWith
{
//    [arrSelectedImages removeAllObjects];
//    NSMutableArray *searchPredicates = [[NSMutableArray alloc] init];
//    BOOL isFilterSelected = NO;
    
//    if ((selectedStartDate != nil) && (selectedEndDate != nil)) {
//
//        NSPredicate *startDatePredicate = [NSPredicate predicateWithFormat:@"((created_at >= %@) AND (created_at <= %@))",selectedStartDate, selectedEndDate];
//        [searchPredicates addObject:startDatePredicate];
//        isFilterSelected = YES;
//    }
    
//    if (self.btnHeightCheck.hidden == false) {
//        NSString *heightStr = @"height";
//        NSPredicate *heightPredicate = [NSPredicate predicateWithFormat:@"(type == %@)",heightStr];
//         [searchPredicates addObject:heightPredicate];
//        isFilterSelected = YES;
//    }
    
//    if (self.btnWeightCheck.hidden == false) {
//        NSString *weightStr = @"weight";
//        NSPredicate *weightPredicate = [NSPredicate predicateWithFormat:@"(type == %@)",weightStr];
//        [searchPredicates addObject:weightPredicate];
//        isFilterSelected = YES;
//    }
    
//    if (self.btnAgeCheck.hidden == false) {
//        NSPredicate *agePredicate = [NSPredicate predicateWithFormat:@"(type contains[c] age)"];
//        [searchPredicates addObject:agePredicate];
//    }
    
//    if (self.btnDevCalCheck.hidden == false) {
//        NSPredicate *devAchPredicate = [NSPredicate predicateWithFormat:@"ANY lstKeyword.keyword CONTAINS[cd] age"];
//        [searchPredicates addObject:devAchPredicate];
//    }
    
//    if (isFilterSelected == YES)
//    {
//        NSPredicate *finalPredicate = [NSCompoundPredicate orPredicateWithSubpredicates:searchPredicates];
//        NSLog(@"%@",finalPredicate);
//        [searchResult removeAllObjects];
//        searchResult = [[NSMutableArray alloc] initWithArray:[arrGallaryList filteredArrayUsingPredicate:finalPredicate]];
//    }
//    else
//    {
//        searchResult = [[NSMutableArray alloc] initWithArray:arrGallaryList];
//    }
//    [self.collectionView reloadData];
    
}

@end
