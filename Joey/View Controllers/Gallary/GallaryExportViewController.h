//
//  GallaryExportViewController.h
//  Joey
//
//  Created by webwerks on 2/9/18.
//  Copyright © 2018 Auxilia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface GallaryExportViewController : BaseViewController < UICollectionViewDelegate, UICollectionViewDataSource>

@property (weak, nonatomic) IBOutlet UICollectionView *templateTypeCollectionView;
@property (weak, nonatomic) IBOutlet UICollectionView *selectedImagesCollectionView;
@property (weak, nonatomic) IBOutlet UIView *middleView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *widthConstraintLayout; // Anil
@property (weak, nonatomic) IBOutlet UIImageView *imgViewLeftArrow;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewRightArrow;

// labels-- Anil
@property (weak, nonatomic) IBOutlet UILabel *lblBabyName;
@property (weak, nonatomic) IBOutlet UILabel *lblBabyAge;
@property (weak, nonatomic) IBOutlet UILabel *lblBabyHeight;
@property (weak, nonatomic) IBOutlet UILabel *lblBabyWeight;

// ImageViews -- Anil
@property (weak, nonatomic) IBOutlet UIImageView *imgViewAge;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewHeight;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewWeight;
@property (strong, nonatomic) IBOutlet UIView *viewCollectionSecondary;
@property (strong, nonatomic) IBOutlet UICollectionView *collectionVSecondary;


@end
