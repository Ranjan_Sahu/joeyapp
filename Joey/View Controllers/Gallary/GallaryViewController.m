//
//  GallaryViewController.m
//  Joey
//
//  Created by webwerks on 2/8/18.
//  Copyright © 2018 Auxilia. All rights reserved.
//

#import "GallaryViewController.h"
#import "AppDelegate.h"
#import "gallaryImageCollectionViewCell.h"
#import "GallaryExportSelectionViewController.h"
#import "GallaryHeaderReusableView.h"

@interface GallaryViewController ()
{
    NSUserDefaults *shareObject;
    AppDelegate *appDelegate;
    NSMutableArray *babyList;
    NSMutableDictionary *unitInfo;
    NSMutableArray *gallaryList;
    
    BOOL isInit;
    BOOL isLoading;
    BOOL isBabySubscribed;
    int backupPageNo;
    int selectedBabyIndex;
    int limit;
    int offset;
    
}

@end

@implementation GallaryViewController

#pragma mark - View Controller Methods
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    shareObject = [NSUserDefaults standardUserDefaults];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
//    if (IS_IPHONE_X) {
//        self.bottomViewBottomSpace.constant = 84.0f;
//    }
    unitInfo = (NSMutableDictionary *)[NSKeyedUnarchiver unarchiveObjectWithData:[shareObject objectForKey:@"unit_info"]];
    backupPageNo = 1;
    gallaryList = [[NSMutableArray alloc] init];

    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:UIColorFromRGB(0x8D8082), NSForegroundColorAttributeName, [UIFont fontWithName:@"SFUIDisplay-Medium" size:18.0f], NSFontAttributeName, nil]];
    [self.navigationController.navigationBar setBarTintColor:UIColorFromRGB(0xEEE8E4)];
    [self.navigationController.navigationBar setTranslucent:NO];
    
    UIButton *btnLogo = [[UIButton alloc] initWithFrame:CGRectZero];
    [btnLogo setImage:[UIImage imageNamed:@"nav_logo.png"] forState:UIControlStateNormal];
    [btnLogo sizeToFit];
    self.navigationItem.titleView = btnLogo;
    [self customSettingsButton];
    
    [self.collectionView registerNib:[UINib nibWithNibName:@"gallaryImageCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"gallaryImageCollectionViewCell"];
    self.collectionView.dataSource = self;
    self.collectionView.delegate = self;
    self.babyScrollView.delegate = self;
    self.collectionView.alwaysBounceVertical = YES;
    
    __weak __typeof(self)weakSelf = self;
    [self.collectionView addPullToRefreshWithActionHandler:^{
        [weakSelf refreshAction];
    }];
    [self.collectionView addInfiniteScrollingWithActionHandler:^{
        [weakSelf getGallaryData];
    }];
    [self setupBabyList];
    [self refreshAction];

    if (IS_IPAD) {
        self.txtName1.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.txtInfo1.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:22.0f];
        self.txtName2.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.txtInfo2.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:22.0f];
        self.txtName3.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:24.0f];
        self.txtInfo3.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:22.0f];
    }

}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:(BOOL)animated];
    [self.navigationController.navigationBar setBarTintColor:UIColorFromRGB(0xEEE8E4)];
    
    unitInfo = (NSMutableDictionary *)[NSKeyedUnarchiver unarchiveObjectWithData:[shareObject objectForKey:@"unit_info"]];
    if(appDelegate.updateGalleryCheck){
        appDelegate.updateGalleryCheck = NO;
        [self setupBabyList];
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

#pragma mark - UI Customization & Layout Hadling Methods
- (void)customSettingsButton
{
    CGFloat fWidth = self.view.frame.size.width;
    
    UIImage *imgExport = [UIImage imageNamed:@"nav_btn_export"];
    UIButton *btnExport = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnExport setImage:imgExport forState:UIControlStateNormal];
    btnExport.frame = CGRectMake(0, 0, imgExport.size.width, imgExport.size.height);
    [btnExport addTarget:self action:@selector(getExportAction:) forControlEvents:UIControlEventTouchUpInside];
    
    UIImage *imgSettings = [UIImage imageNamed:@"nav_btn_settings"];
    UIButton *btnSettings = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnSettings setImage:imgSettings forState:UIControlStateNormal];
    btnSettings.frame = CGRectMake(((fWidth/4)+15 - imgSettings.size.width), 0, imgSettings.size.width, imgSettings.size.height);
    [btnSettings addTarget:self action:@selector(getSettingsAction:) forControlEvents:UIControlEventTouchUpInside];
    
    UIView *rightButtonView = [[UIView alloc] initWithFrame:CGRectMake((fWidth*3/4)-15, 0, (fWidth/4)+15, 44)];
    [rightButtonView addSubview:btnExport];
    [rightButtonView addSubview:btnSettings];
    
    UIBarButtonItem *rightBarItem = [[UIBarButtonItem alloc] initWithCustomView:rightButtonView];
    self.navigationItem.rightBarButtonItem = rightBarItem;
}

- (void)viewDidLayoutSubviews
{
    if(self.babyScrollView.contentSize.width == 0)
    {
        self.babyScrollView.contentSize = CGSizeMake(self.view.frame.size.width*3, 1);
        self.babyScrollView.contentOffset = CGPointMake(self.view.frame.size.width, self.babyScrollView.contentOffset.y);
    }
    
    self.imgBabyPic1.clipsToBounds = self.imgBabyPic2.clipsToBounds = self.imgBabyPic3.clipsToBounds = YES;
    self.imgBabyPic1.layer.cornerRadius = self.imgBabyPic1.frame.size.width/2;
    self.imgBabyPic2.layer.cornerRadius = self.imgBabyPic2.frame.size.width/2;
    self.imgBabyPic3.layer.cornerRadius = self.imgBabyPic3.frame.size.width/2;
}

#pragma mark - Custom Actions
-(void)getGallaryData
{
    [SVProgressHUD show];
    
    if(isLoading) return;
    isLoading = YES;
    limit = 10;
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setValue:@([shareObject integerForKey:@"user_id"]) forKey:@"user_id"];
    [parameters setValue:@([shareObject integerForKey:@"baby_id"]) forKey:@"baby_id"];
    [parameters setValue:@(offset) forKey:@"offset"];
    [parameters setValue:@(limit) forKey:@"limit"];
    
    [[WebServiceManager sharedManager] initWithBaseURL:@"api/getBabyImageGallery" parameters:parameters success:^(id result) {
        
        
        
        if ([[result valueForKey:@"success"] boolValue] == false) {
            [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:[result objectForKey:@"message"] input:nil];
        }
        else{
            
            if(offset == 0) [gallaryList removeAllObjects];
            NSLog(@"%@",parameters);
//            isBabySubscribed = [[result valueForKey:@"is_baby_subscribe"] boolValue];
            NSMutableArray *list = (NSMutableArray *)[result valueForKey:@"list"];
            if(list.count > 0)
            {
                if(offset == 0)
                {
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        [self.collectionView.pullToRefreshView stopAnimating];
                        [self.collectionView.infiniteScrollingView stopAnimating];
                    });
                }
                
                self.collectionView.showsInfiniteScrolling =YES;
                [gallaryList addObjectsFromArray:list];
                offset += limit;
            }
            else {
                self.collectionView.showsInfiniteScrolling = NO;
            }
            
            [self.collectionView reloadData];
            [self.collectionView.pullToRefreshView stopAnimating];
            [self.collectionView.infiniteScrollingView stopAnimating];
            [list removeAllObjects];
            list = result = nil;
        }
        
        isLoading = NO;
        [SVProgressHUD dismiss];
        
    } failure:^(NSError *error) {
        
        [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:LocalizedString(@"error_unknown", nil) input:nil];
        
        isLoading = NO;
        [self.collectionView.pullToRefreshView stopAnimating];
        [self.collectionView.infiniteScrollingView stopAnimating];
        
        [SVProgressHUD dismiss];
    }];
}

#pragma mark - IBAction Methods
- (IBAction)getSettingsAction:(id)sender
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Settings" bundle:nil];
    UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"SettingsViewController"];
    viewController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:viewController animated:YES];
}

- (IBAction)getExportAction:(id)sender
{
    
    //Alert to 
    if (isBabySubscribed) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"GallaryExportSelectionViewController"];
        viewController.hidesBottomBarWhenPushed = YES;
        
        // NSMutableArray *arr = [[NSMutableArray alloc] init];
        //    for (NSDictionary *dictObj in gallaryList) {
        //        [arr addObjectsFromArray:[dictObj objectForKey:@"list"]];
        //    }
        
        
        
        NSMutableDictionary *dataDict = [[NSMutableDictionary alloc] init];
        [dataDict setObject:gallaryList forKey:@"gallary_List"];
        viewController.data = dataDict;
        [self.navigationController pushViewController:viewController animated:YES];
    }
    else {
        NSString *errMessage = LocalizedString(@"alert_user_unsubscribed", nil);
        [self showAlertWithTitle:LocalizedString(@"txt_about_title", nil) message:errMessage input:nil];
    }
    
}

#pragma mark - Collection view Data Source & Delegates
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

//-(UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
//
//    GallaryHeaderReusableView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"GallaryHeaderReusableView" forIndexPath:indexPath];
//
//    headerView.backgroundColor = UIColorFromRGB(0x8D8082);
//    NSString *title = @"";
//    if (gallaryList.count>0) {
//        title = [NSString stringWithFormat:@"%@",[[gallaryList objectAtIndex:indexPath.section] valueForKey:@"name"]];
//    }
//    headerView.lblTitle.text = title;
//    headerView.lblTitle.textColor = [UIColor whiteColor];
//    headerView.lblTitle.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:15.0f];
//    if (IS_IPAD) {
//        headerView.lblTitle.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:21.0f];
//    }
//    return headerView;
//}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
{
    if (IS_IPAD) {
        //return CGSizeMake(self.collectionView.bounds.size.width, 40);
        return CGSizeZero;
    }
    else {
        //return CGSizeMake(self.collectionView.bounds.size.width, 30);
        return CGSizeZero;
    }
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (gallaryList.count>0) {
        return gallaryList.count;
    }else {
        return 0;
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    gallaryImageCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"gallaryImageCollectionViewCell" forIndexPath:indexPath];
    
    NSDictionary *imgDict = [gallaryList objectAtIndex:indexPath.row];
   NSString *imgUrl =  [imgDict valueForKey:@"photo_url"];
    NSLog(@"%@",imgUrl);
    cell.imgView.image = nil;
    cell.imgView.backgroundColor = UIColorFromRGB(0xD9CEC4);
    if ([[imgDict valueForKey:@"photo_url"] isKindOfClass:[NSNull class]]) {
        cell.imgView.image = [UIImage imageNamed:@"dummy.png"];
    }else {
        [cell.imgView sd_setImageWithURL:[NSURL URLWithString:imgUrl] placeholderImage:[UIImage imageNamed:@"dummy.png"]];
    }
    
    cell.btnSelect.hidden = YES;
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static gallaryImageCollectionViewCell *sizingCell = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sizingCell = [[NSBundle mainBundle] loadNibNamed:@"gallaryImageCollectionViewCell" owner:self options:nil][0];
    });
    
    int w = (self.collectionView.bounds.size.width-4) / 4.0f;
    return CGSizeMake(w, w);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(2, 2, 2, 2);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    // Showing full screen on the tap
    
    gallaryImageCollectionViewCell *cell = [collectionView cellForItemAtIndexPath:indexPath];
    
    [self showPhotoAction:cell.imgView.image];
    
    //    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Activities" bundle:nil];
    //    UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"ActivityFeedViewController"];
    //    viewController.data = [[NSMutableDictionary alloc] initWithDictionary:@{@"monthsold":[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"monthsold"]}];
    //    viewController.hidesBottomBarWhenPushed = YES;
    //    [self.navigationController pushViewController:viewController animated:YES];
}

- (void)showPhotoAction:(UIImage *)img
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"PhotoViewController"];
    viewController.data = [[NSMutableDictionary alloc] initWithDictionary:@{@"viewController":self, @"image":img}];
    [self presentViewController:viewController animated:YES completion:nil];
    
}

#pragma mark UIScrollViewDelegate Methods
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if([scrollView isEqual:self.babyScrollView])
    {
        CGFloat pageWidth = self.view.bounds.size.width;
        int pageNo = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
        NSLog(@"pageNo=%d", pageNo);
        NSLog(@"backupPageNo=%d", backupPageNo);
        
        if(pageNo == 0)
        {
            NSLog(@"selectedBabyIndex=%d", selectedBabyIndex);
            if(backupPageNo != pageNo)
            {
                int nextIndex = selectedBabyIndex;
                nextIndex = (nextIndex >= babyList.count)?0:nextIndex;
                selectedBabyIndex--;
                selectedBabyIndex = (selectedBabyIndex < 0)?(int)(babyList.count-1):selectedBabyIndex;
                int prevIndex = selectedBabyIndex-1;
                prevIndex = (prevIndex < 0)?(int)(babyList.count-1):prevIndex;
                
                NSLog(@"selectedBabyIndex=%d", selectedBabyIndex);
                NSLog(@"prevIndex=%d", prevIndex);
                NSLog(@"nextIndex=%d", nextIndex);
                
                [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"feed_info"]] forKey:@"feed_info"];
                [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"pump_info"]] forKey:@"pump_info"];
                [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"sleep_info"]] forKey:@"sleep_info"];
                [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"poo_info"]] forKey:@"poo_info"];
                [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"wee_info"]] forKey:@"wee_info"];
                [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"medical_info"]] forKey:@"medical_info"];
                [shareObject synchronize];
                
                [self.imgBabyPic1 sd_setImageWithURL:[NSURL URLWithString:[[babyList objectAtIndex:prevIndex] objectForKey:@"photo_url"]] placeholderImage:[UIImage imageNamed:@"dummy.png"]];
                self.txtName1.text = [[babyList objectAtIndex:prevIndex] objectForKey:@"name"];
                self.txtInfo1.text = [NSString stringWithFormat:LocalizedString(@"txt_baby_info", nil), [[babyList objectAtIndex:prevIndex] objectForKey:@"age"], [[babyList objectAtIndex:prevIndex] objectForKey:@"height"], [[babyList objectAtIndex:prevIndex] objectForKey:@"weight"], [[babyList objectAtIndex:prevIndex] objectForKey:@"blood_type"]];
                
                [self.imgBabyPic2 sd_setImageWithURL:[NSURL URLWithString:[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"photo_url"]] placeholderImage:[UIImage imageNamed:@"dummy.png"]];
                self.txtName2.text = [[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"name"];
                self.txtInfo2.text = [NSString stringWithFormat:LocalizedString(@"txt_baby_info", nil), [[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"age"], [[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"height"], [[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"weight"], [[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"blood_type"]];
                
                [self.imgBabyPic3 sd_setImageWithURL:[NSURL URLWithString:[[babyList objectAtIndex:nextIndex] objectForKey:@"photo_url"]] placeholderImage:[UIImage imageNamed:@"dummy.png"]];
                self.txtName3.text = [[babyList objectAtIndex:nextIndex] objectForKey:@"name"];
                self.txtInfo3.text = [NSString stringWithFormat:LocalizedString(@"txt_baby_info", nil), [[babyList objectAtIndex:nextIndex] objectForKey:@"age"], [[babyList objectAtIndex:nextIndex] objectForKey:@"height"], [[babyList objectAtIndex:nextIndex] objectForKey:@"weight"], [[babyList objectAtIndex:nextIndex] objectForKey:@"blood_type"]];
                
                self.babyScrollView.contentOffset = CGPointMake(self.view.frame.size.width, self.babyScrollView.contentOffset.y);
                backupPageNo = 1;
                
                [self refreshAction];
            }
        }
        else if(pageNo == 2)
        {
            if(backupPageNo != pageNo)
            {
                int prevIndex = selectedBabyIndex;
                prevIndex = (prevIndex < 0)?(int)(babyList.count-1):prevIndex;
                selectedBabyIndex++;
                selectedBabyIndex = (selectedBabyIndex >= babyList.count)?0:selectedBabyIndex;
                int nextIndex = selectedBabyIndex+1;
                nextIndex = (nextIndex >= babyList.count)?0:nextIndex;
                
                NSLog(@"selectedBabyIndex=%d", selectedBabyIndex);
                NSLog(@"prevIndex=%d", prevIndex);
                NSLog(@"nextIndex=%d", nextIndex);
                
                [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"feed_info"]] forKey:@"feed_info"];
                [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"pump_info"]] forKey:@"pump_info"];
                [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"sleep_info"]] forKey:@"sleep_info"];
                [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"poo_info"]] forKey:@"poo_info"];
                [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"wee_info"]] forKey:@"wee_info"];
                [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"medical_info"]] forKey:@"medical_info"];
                [shareObject synchronize];
                
                [self.imgBabyPic1 sd_setImageWithURL:[NSURL URLWithString:[[babyList objectAtIndex:prevIndex] objectForKey:@"photo_url"]] placeholderImage:[UIImage imageNamed:@"dummy.png"]];
                self.txtName1.text = [[babyList objectAtIndex:prevIndex] objectForKey:@"name"];
                self.txtInfo1.text = [NSString stringWithFormat:LocalizedString(@"txt_baby_info", nil), [[babyList objectAtIndex:prevIndex] objectForKey:@"age"], [[babyList objectAtIndex:prevIndex] objectForKey:@"height"], [[babyList objectAtIndex:prevIndex] objectForKey:@"weight"], [[babyList objectAtIndex:prevIndex] objectForKey:@"blood_type"]];
                
                [self.imgBabyPic2 sd_setImageWithURL:[NSURL URLWithString:[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"photo_url"]] placeholderImage:[UIImage imageNamed:@"dummy.png"]];
                self.txtName2.text = [[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"name"];
                self.txtInfo2.text = [NSString stringWithFormat:LocalizedString(@"txt_baby_info", nil), [[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"age"], [[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"height"], [[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"weight"], [[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"blood_type"]];
                
                [self.imgBabyPic3 sd_setImageWithURL:[NSURL URLWithString:[[babyList objectAtIndex:nextIndex] objectForKey:@"photo_url"]] placeholderImage:[UIImage imageNamed:@"dummy.png"]];
                self.txtName3.text = [[babyList objectAtIndex:nextIndex] objectForKey:@"name"];
                self.txtInfo3.text = [NSString stringWithFormat:LocalizedString(@"txt_baby_info", nil), [[babyList objectAtIndex:nextIndex] objectForKey:@"age"], [[babyList objectAtIndex:nextIndex] objectForKey:@"height"], [[babyList objectAtIndex:nextIndex] objectForKey:@"weight"], [[babyList objectAtIndex:nextIndex] objectForKey:@"blood_type"]];
                
                self.babyScrollView.contentOffset = CGPointMake(self.view.frame.size.width, self.babyScrollView.contentOffset.y);
                backupPageNo = 1;
                
                [self refreshAction];
            }
        }
        else backupPageNo = pageNo;
        
        // Check for Subscription of selected baby
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"is_Subscription_Enabled"];
        if (![[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"subscription"] isKindOfClass:[NSNull class]]) {
            BOOL isSubscribed = [[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"subscription"] boolValue];
            [[NSUserDefaults standardUserDefaults]setBool:isSubscribed forKey:@"is_Subscription_Enabled"];
        }
    }
}

#pragma mark -  View's Data Configuration methods
-(void) refreshAction
{
    NSLog(@"selectedBabyIndex=%d", selectedBabyIndex);
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    [shareObject setInteger:[[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"id"] intValue] forKey:@"baby_id"];
    [shareObject setInteger:selectedBabyIndex forKey:@"baby_index"];
    [shareObject synchronize];
    
    offset = 0;
    
    [self.collectionView.pullToRefreshView stopAnimating];
    [self getGallaryData];
    
}

- (void)setupBabyList
{
    babyList = (NSMutableArray *)[NSKeyedUnarchiver unarchiveObjectWithData:[shareObject objectForKey:@"baby_list"]];
    
    selectedBabyIndex = (int)[shareObject integerForKey:@"baby_index"];
    selectedBabyIndex = (selectedBabyIndex >= babyList.count)?0:selectedBabyIndex;
    NSLog(@"selectedBabyIndex1=%d", selectedBabyIndex);
    
    for(int i=0; i<babyList.count; i++){
        
        NSLog(@"weight=%@", [[babyList objectAtIndex:i] objectForKey:@"weight"]);
        NSLog(@"unit=%@", [unitInfo objectForKey:@"weight"]);
        [[babyList objectAtIndex:i] setObject:[self convertUnit:[[babyList objectAtIndex:i] objectForKey:@"height"] unit:[unitInfo objectForKey:@"height"] spacing:YES] forKey:@"height"];
        [[babyList objectAtIndex:i] setObject:[self convertUnit:[[babyList objectAtIndex:i] objectForKey:@"weight"] unit:[unitInfo objectForKey:@"weight"] spacing:YES] forKey:@"weight"];
    }
    
    // Check for Subscription of selected baby
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"is_Subscription_Enabled"];
    if (![[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"subscription"] isKindOfClass:[NSNull class]]) {
        BOOL isSubscribed = [[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"subscription"] boolValue];
        isBabySubscribed = isSubscribed;
        [[NSUserDefaults standardUserDefaults]setBool:isSubscribed forKey:@"is_Subscription_Enabled"];
    }
    
    NSLog(@"babyList2=%@", babyList);
    if(babyList.count == 1)
    {
        self.babyScrollView.scrollEnabled = NO;
        [self.imgBabyPic2 sd_setImageWithURL:[NSURL URLWithString:[[babyList objectAtIndex:0] objectForKey:@"photo_url"]] placeholderImage:[UIImage imageNamed:@"dummy.png"]];
        self.txtName2.text = [[babyList objectAtIndex:0] objectForKey:@"name"];
        self.txtInfo2.text = [NSString stringWithFormat:LocalizedString(@"txt_baby_info", nil), [[babyList objectAtIndex:0] objectForKey:@"age"], [[babyList objectAtIndex:0] objectForKey:@"height"], [[babyList objectAtIndex:0] objectForKey:@"weight"], [[babyList objectAtIndex:0] objectForKey:@"blood_type"]];
    }
    else
    {
        int prevIndex = selectedBabyIndex-1;
        prevIndex = (prevIndex < 0)?(int)(babyList.count-1):prevIndex;
        int nextIndex = selectedBabyIndex+1;
        nextIndex = (nextIndex >= babyList.count)?0:nextIndex;
        
        self.babyScrollView.scrollEnabled = YES;
        [self.imgBabyPic1 sd_setImageWithURL:[NSURL URLWithString:[[babyList objectAtIndex:prevIndex] objectForKey:@"photo_url"]] placeholderImage:[UIImage imageNamed:@"dummy.png"]];
        self.txtName1.text = [[babyList objectAtIndex:prevIndex] objectForKey:@"name"];
        self.txtInfo1.text = [NSString stringWithFormat:LocalizedString(@"txt_baby_info", nil), [[babyList objectAtIndex:prevIndex] objectForKey:@"age"], [[babyList objectAtIndex:prevIndex] objectForKey:@"height"], [[babyList objectAtIndex:prevIndex] objectForKey:@"weight"], [[babyList objectAtIndex:prevIndex] objectForKey:@"blood_type"]];
        
        [self.imgBabyPic2 sd_setImageWithURL:[NSURL URLWithString:[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"photo_url"]] placeholderImage:[UIImage imageNamed:@"dummy.png"]];
        self.txtName2.text = [[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"name"];
        self.txtInfo2.text = [NSString stringWithFormat:LocalizedString(@"txt_baby_info", nil), [[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"age"], [[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"height"], [[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"weight"], [[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"blood_type"]];
        
        [self.imgBabyPic3 sd_setImageWithURL:[NSURL URLWithString:[[babyList objectAtIndex:nextIndex] objectForKey:@"photo_url"]] placeholderImage:[UIImage imageNamed:@"dummy.png"]];
        self.txtName3.text = [[babyList objectAtIndex:nextIndex] objectForKey:@"name"];
        self.txtInfo3.text = [NSString stringWithFormat:LocalizedString(@"txt_baby_info", nil), [[babyList objectAtIndex:nextIndex] objectForKey:@"age"], [[babyList objectAtIndex:nextIndex] objectForKey:@"height"], [[babyList objectAtIndex:nextIndex] objectForKey:@"weight"], [[babyList objectAtIndex:nextIndex] objectForKey:@"blood_type"]];
    }
    
    [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"feed_info"]] forKey:@"feed_info"];
    [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"pump_info"]] forKey:@"pump_info"];
    [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"sleep_info"]] forKey:@"sleep_info"];
    [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"poo_info"]] forKey:@"poo_info"];
    [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"wee_info"]] forKey:@"wee_info"];
    [shareObject setObject:[NSKeyedArchiver archivedDataWithRootObject:(NSMutableDictionary *)[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"medical_info"]] forKey:@"medical_info"];
    [shareObject synchronize];
}

@end
