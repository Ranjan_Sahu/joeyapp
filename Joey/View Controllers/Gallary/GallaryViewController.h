//
//  GallaryViewController.h
//  Joey
//
//  Created by webwerks on 2/8/18.
//  Copyright © 2018 Auxilia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface GallaryViewController : BaseViewController <UIScrollViewDelegate, UICollectionViewDelegate, UICollectionViewDataSource>

@property (nonatomic, weak) IBOutlet UICollectionView *collectionView;

@property (nonatomic, weak) IBOutlet UIScrollView *babyScrollView;
@property (nonatomic, weak) IBOutlet UIImageView *imgBabyPic1;
@property (nonatomic, weak) IBOutlet UIImageView *imgBabyPic2;
@property (nonatomic, weak) IBOutlet UIImageView *imgBabyPic3;
@property (nonatomic, weak) IBOutlet UILabel *txtName1;
@property (nonatomic, weak) IBOutlet UILabel *txtInfo1;
@property (nonatomic, weak) IBOutlet UILabel *txtName2;
@property (nonatomic, weak) IBOutlet UILabel *txtInfo2;
@property (nonatomic, weak) IBOutlet UILabel *txtName3;
@property (nonatomic, weak) IBOutlet UILabel *txtInfo3;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomViewBottomSpace;

@end
