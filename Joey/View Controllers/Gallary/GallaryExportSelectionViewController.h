//
//  GallaryExportSelectionViewController.h
//  Joey
//
//  Created by webwerks on 2/9/18.
//  Copyright © 2018 Auxilia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface GallaryExportSelectionViewController : BaseViewController<UIScrollViewDelegate, UICollectionViewDelegate, UICollectionViewDataSource>

@property (weak, nonatomic) IBOutlet UIBarButtonItem *btnSave;
@property (nonatomic, weak) IBOutlet UICollectionView *collectionView;
@property (nonatomic, weak) IBOutlet UILabel *lblStart;
@property (nonatomic, weak) IBOutlet UILabel *lblEnd;
@property (nonatomic, weak) IBOutlet UILabel *lblStartTxt;
@property (nonatomic, weak) IBOutlet UILabel *lblEndTxt;

// Filter
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *filterViewHeight;
@property (weak, nonatomic) IBOutlet UIButton *btnHeightCheck;
@property (weak, nonatomic) IBOutlet UIButton *btnWeightCheck;
@property (weak, nonatomic) IBOutlet UIButton *btnAgeCheck;
@property (weak, nonatomic) IBOutlet UIButton *btnDevCalCheck;
@property (weak, nonatomic) IBOutlet UILabel *lblHeight;
@property (weak, nonatomic) IBOutlet UILabel *lblWeight;
@property (weak, nonatomic) IBOutlet UILabel *lblAge;
@property (weak, nonatomic) IBOutlet UILabel *lblDevCaledar;

@property (weak,nonatomic) NSString *strName;

@end
