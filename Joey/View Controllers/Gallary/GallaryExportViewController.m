//
//  GallaryExportViewController.m
//  Joey
//
//  Created by webwerks on 2/9/18.
//  Copyright © 2018 Auxilia. All rights reserved.
//

#import "GallaryExportViewController.h"
#import "AppDelegate.h"
#import "gallaryImageCollectionViewCell.h"
#import "templateCollectionViewCell.h"
#import "GallaryViewController.h"
#import "collectionCellGallaryLayout.h"
#import "collectionCellSixthLayout.h"

@interface GallaryExportViewController ()
{

    NSUserDefaults *shareObject;
    AppDelegate *appDelegate;
    NSMutableArray *arrSelectedImages;
    NSString *selectedDataBlocks; // Anil
    NSMutableArray *arrSelectedDataBlocks; // Anil
    NSMutableArray *arrNew; // New Anil
    NSMutableDictionary *dictList; // New Anil
    CGPoint previousLocation;
    UIPanGestureRecognizer *previousGesture;
    NSIndexPath *selectedTemplate;
    NSIndexPath *originalIndexPath;
    NSIndexPath *draggingIndexPath;
    NSIndexPath *newIndexPath;
    UIView *draggingView;
    CGPoint dragOffset;
    NSMutableArray *arrLayout;
    BOOL isLoading; // Anil
    NSUInteger indexVisibleCell; // Anil
    UICollectionViewFlowLayout *layout;
    NSString *selectedTemplateType;
    NSMutableArray *babyList;//Anil
    int selectedBabyIndex;// Anil
    NSMutableDictionary *unitInfo;// Anil
}
@end

@implementation GallaryExportViewController

#pragma mark - View Controller Methods
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //self.templateTypeCollectionView.hidden = YES;
    self.imgViewLeftArrow.hidden = YES;
    self.imgViewRightArrow.hidden = YES;
    
    shareObject = [NSUserDefaults standardUserDefaults];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:UIColorFromRGB(0x8D8082), NSForegroundColorAttributeName, [UIFont fontWithName:@"SFUIDisplay-Medium" size:18.0f], NSFontAttributeName, nil]];
    [self.navigationController.navigationBar setBarTintColor:UIColorFromRGB(0xEEE8E4)];
    [self.navigationController.navigationBar setTranslucent:NO];
    
    [self.navigationItem setTitle:LocalizedString(@"Gallery Export", nil)];
    [self customBackButton];
    [self customSettingsButton];
    
    selectedTemplateType = @"first";
    
    NSLog(@"arrNew is--- %@",arrNew);
    
    babyList = (NSMutableArray *)[NSKeyedUnarchiver unarchiveObjectWithData:[shareObject objectForKey:@"baby_list"]];
    NSLog(@"count is %ld",babyList.count);
    NSLog(@"babyList --- %@",babyList);

    selectedBabyIndex = (int)[shareObject integerForKey:@"baby_index"];
    selectedBabyIndex = (selectedBabyIndex >= babyList.count)?0:selectedBabyIndex;
    NSLog(@"selectedBabyIndex1=%d", selectedBabyIndex);
    
     NSLog(@"selected index baby list is %@",[babyList objectAtIndex:selectedBabyIndex]);
    
    self.lblBabyName.text = @"";
    self.lblBabyAge.text = @"";
    self.lblBabyWeight.text = @"";
    self.lblBabyHeight.text = @"";
    
    unitInfo = (NSMutableDictionary *)[NSKeyedUnarchiver unarchiveObjectWithData:[shareObject objectForKey:@"unit_info"]];
    
    NSLog(@"height=%@", [[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"height"]);
    NSLog(@"unit=%@", [unitInfo objectForKey:@"height"]);
    
    NSLog(@"weight=%@", [[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"weight"]);
    NSLog(@"unit=%@", [unitInfo objectForKey:@"weight"]);
    [[babyList objectAtIndex:selectedBabyIndex] setObject:[self convertUnit:[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"height"] unit:[unitInfo objectForKey:@"height"] spacing:YES] forKey:@"height"];
    [[babyList objectAtIndex:selectedBabyIndex] setObject:[self convertUnit:[[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"weight"] unit:[unitInfo objectForKey:@"weight"] spacing:YES] forKey:@"weight"];
    
    self.lblBabyName.text = [[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"name"];
    self.lblBabyAge.text = [[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"age"];
    self.lblBabyHeight.text = [[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"height"];
    self.lblBabyWeight.text = [[babyList objectAtIndex:selectedBabyIndex] objectForKey:@"weight"];
    
    //[self.templateTypeCollectionView registerNib:[UINib nibWithNibName:@"templateCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"templateCollectionViewCell"];
    
    //[self.templateTypeCollectionView registerNib:[UINib nibWithNibName:@"gallaryImageCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"gallaryImageCollectionViewCell"];
    
    self.templateTypeCollectionView.dataSource = self;
    self.templateTypeCollectionView.delegate = self;
    self.selectedImagesCollectionView.dataSource = self;
    self.selectedImagesCollectionView.delegate = self;
    self.templateTypeCollectionView.alwaysBounceHorizontal = YES;
    self.selectedImagesCollectionView.alwaysBounceVertical = YES;
    
    // Anil
    selectedDataBlocks = [[NSString alloc]init];
    selectedDataBlocks = [self.data objectForKey:@"selected_Data_Blocks"];
    NSLog(@"selected Data Blocks %@",selectedDataBlocks);
    
    arrSelectedDataBlocks = [[NSMutableArray alloc]init];
    arrSelectedDataBlocks = [self.data objectForKey:@"arr_selected_Data_Blocks"];
    NSLog(@"arrSelectedDataBlocks %@",arrSelectedDataBlocks);
    
    arrNew = [[NSMutableArray alloc]init];
    arrNew = [self.data objectForKey:@"selected_Images_Data"];
    NSLog(@"arrNew %@",arrNew);
    
    arrLayout = [[NSMutableArray alloc]init]; // Anil
    
    dictList = [[NSMutableDictionary alloc]init];
    //
    
    arrSelectedImages = [[NSMutableArray alloc]init];
    //arrSelectedImages = [self.data objectForKey:@"selected_Images_Data"];
    
    UILongPressGestureRecognizer *longPressGesture = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongGesture:)];
    longPressGesture.minimumPressDuration = 0.2;
    [self.selectedImagesCollectionView setUserInteractionEnabled:YES];
    [self.selectedImagesCollectionView addGestureRecognizer:longPressGesture];
    
    UILongPressGestureRecognizer *longPressGestureNew = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongGestureNew:)];
    longPressGestureNew.minimumPressDuration = 0.2;
    
    
    [self.collectionVSecondary setUserInteractionEnabled:YES];
    [self.collectionVSecondary addGestureRecognizer:longPressGestureNew];
    
    [self getDataForDataBlocks];
    
        // For temporary testing only
//        [arrSelectedImages removeAllObjects];
//        UIImage *img = [[UIImage alloc]init];
//        NSString *str = [[NSString alloc]init];
//        for(int i = 0 ; i < 6 ; i++){
//            str = [NSString stringWithFormat:@"btn_teeth%d",i];
//
//            [arrSelectedImages addObject:str];
//        }
    
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:(BOOL)animated];
    [self.navigationController.navigationBar setBarTintColor:UIColorFromRGB(0xEEE8E4)];
    NSLog(@"here=%@", self.data);
    
    if(IS_IPAD){
        self.topViewHeight.constant = 128;
    }
    else {
        self.topViewHeight.constant = 84;
    }

    self.middleView.layer.borderWidth = 1.0;
    self.middleView.layer.cornerRadius = 1.0;
    self.middleView.layer.borderColor = UIColorFromRGB(0x8D8082).CGColor;
    
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    //int cellNumbers = [self setLayoutWidth];
    //NSLog(@"value is %d",cellNumbers);
}

-(int)setLayoutWidth {
    
    self.templateTypeCollectionView.hidden = NO;
    NSLog(@"arrLayout is %@",arrLayout);
    NSString *str = [arrLayout objectAtIndex:0];
    NSLog(@"str is %@",str);
    int value = [str intValue];
    NSLog(@"value is %d",value);
    _widthConstraintLayout.constant = (value * 50);
    
    
    return value;
    
}

#pragma mark - UI Customization & Layout Hadling Methods
- (void)customBackButton
{
    if([self.navigationController.viewControllers count] > 1)
    {
        UIImage *imgBack = [UIImage imageNamed:@"nav_btn_back"];
        UIButton *btnBack = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnBack setImage:imgBack forState:UIControlStateNormal];
        btnBack.frame = CGRectMake(0, 0, imgBack.size.width, imgBack.size.height);
        [btnBack addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
        [btnBack setImageEdgeInsets:UIEdgeInsetsMake(0, -16, 0, 16)];
        UIBarButtonItem *backBarItem = [[UIBarButtonItem alloc] initWithCustomView:btnBack];
        self.navigationItem.leftBarButtonItem = backBarItem;
    }
}

- (void)customSettingsButton
{
    CGFloat fWidth = self.view.frame.size.width;
    
    UIImage *imgExport = [UIImage imageNamed:@"nav_btn_export"];
    UIButton *btnExport = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnExport setImage:imgExport forState:UIControlStateNormal];
    btnExport.frame = CGRectMake(15, 0, imgExport.size.width, imgExport.size.height);
    [btnExport addTarget:self action:@selector(exportAction:) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *btnSettings = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnSettings setTitle:@"Save" forState:UIControlStateNormal];
    [btnSettings setTitleColor:UIColorFromRGB(0x786E6F) forState:UIControlStateNormal];
    btnSettings.titleLabel.textAlignment = NSTextAlignmentRight;
    btnSettings.frame = CGRectMake(((fWidth/4)+20 - 42), 0, 52, imgExport.size.height);
    [btnSettings addTarget:self action:@selector(nextAction:) forControlEvents:UIControlEventTouchUpInside];
    
    UIView *rightButtonView = [[UIView alloc] initWithFrame:CGRectMake((fWidth*3/4)-20, 0, (fWidth/4)+20, 44)];
    [rightButtonView addSubview:btnExport];
    [rightButtonView addSubview:btnSettings];
    
    if (![[NSUserDefaults standardUserDefaults]boolForKey:@"is_Subscription_Enabled"]) {
        btnExport.hidden = YES;
    }
    
    UIBarButtonItem *rightBarItem = [[UIBarButtonItem alloc] initWithCustomView:rightButtonView];
    self.navigationItem.rightBarButtonItem = rightBarItem;
}

//Anil
#pragma mark - Collection view Data Source & Delegates


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (collectionView == self.templateTypeCollectionView) {
        
        NSLog(@"data blocks count is %ld",arrSelectedDataBlocks.count);
        NSLog(@"selected images count is %ld",arrSelectedImages.count);
        NSLog(@"Total count is %ld",arrSelectedDataBlocks.count + arrSelectedImages.count);
        
        if(arrSelectedImages.count == 1 || arrSelectedImages.count == 7 || arrSelectedImages.count == 8 || arrSelectedImages.count == 9){
            [arrLayout removeAllObjects];
            [arrLayout addObject:@"1"];
            return 1;
            
        }
        
        else if(arrSelectedImages.count == 2 || arrSelectedImages.count == 3 || arrSelectedImages.count == 5){
            [arrLayout removeAllObjects];
            [arrLayout addObject:@"2"];
            return 2;
        }
        
        else if(arrSelectedImages.count == 4 || arrSelectedImages.count == 6){
            [arrLayout removeAllObjects];
            [arrLayout addObject:@"3"];
            return 3;
        }
        
        else{
            return 0;
        }
        
    }else if (collectionView == self.selectedImagesCollectionView){
        
        return arrSelectedImages.count;
        
        
    }else if(collectionView == self.collectionVSecondary){
        return 6;
    }
    
    else{
        return 0;
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if (collectionView == self.templateTypeCollectionView) {
        [collectionView registerNib:[UINib nibWithNibName:@"templateCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"templateCollectionViewCell"];
        templateCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"templateCollectionViewCell" forIndexPath:indexPath];
        
        //Anil
        
        NSLog(@"arrNew count %lu",(unsigned long)arrNew.count);
        NSLog(@"arrSelectedDataBlocks count %lu",(unsigned long)arrSelectedDataBlocks.count);
        NSLog(@"arrSelectedImages count %lu",(unsigned long)arrSelectedImages.count);
        
        cell.imgView_inactive.hidden = NO;
        cell.imgView.hidden = NO;
        
        
        // New one Start
        
        if ([selectedTemplateType isEqualToString:@"first"]){

            if (arrSelectedImages.count == 1) {
                
                if (indexPath.row == 0) {
                    cell.imgView.image = [UIImage imageNamed:@"icon_gallery_export_1_1_active_96"];
                    cell.imgView_inactive.image = [UIImage imageNamed:@"icon_gallery_export_1_1_inactive_96"];
                    cell.imgView_inactive.hidden = YES;
                }
//                else if(indexPath.row == 1){
//
//                    cell.imgView.image = [UIImage imageNamed:@"icon_gallery_export_1_2_active_192"];
//                    cell.imgView_inactive.image = [UIImage imageNamed:@"icon_gallery_export_1_2_inactive_192"];
//                    cell.imgView.hidden = YES;
//
//                }
//
//                else if(indexPath.row == 2){
//
//                    cell.imgView.image = [UIImage imageNamed:@"icon_gallery_export_1_3_active_192"];
//                    cell.imgView_inactive.image = [UIImage imageNamed:@"icon_gallery_export_1_3_inactive_192"];
//                    cell.imgView.hidden = YES;
//                }
//
//                else if(indexPath.row == 3){
//
//                    cell.imgView.image = [UIImage imageNamed:@"icon_gallery_export_1_4_active_192"];
//                    cell.imgView_inactive.image = [UIImage imageNamed:@"icon_gallery_export_1_4_inactive_192"];
//                    cell.imgView.hidden = YES;
//                }

            }

            // for two images

            if (arrSelectedImages.count == 2) {

                if (indexPath.row == 0) {
                    
                    cell.imgView.image = [UIImage imageNamed:@"icon_gallery_export_2_1_active_96.png"];
                    
                    cell.imgView_inactive.image = [UIImage imageNamed:@"icon_gallery_export_2_1_inactive_96.png"];
                    
                    cell.imgView_inactive.hidden = YES;
                }
                else if(indexPath.row == 1){

                    cell.imgView.image = [UIImage imageNamed:@"icon_gallery_export_2_2_active_96.png"];
                    cell.imgView_inactive.image = [UIImage imageNamed:@"icon_gallery_export_2_2_inactive_96.png"];
                    
                    cell.imgView.hidden = YES;

                }


            }

            // for three

            if (arrSelectedImages.count == 3) {

                if (indexPath.row == 0){
                    cell.imgView.image = [UIImage imageNamed:@"icon_gallery_export_3_1_active_96"];
                    cell.imgView_inactive.image = [UIImage imageNamed:@"icon_gallery_export_3_1_inative_96"];
                    cell.imgView_inactive.hidden = YES;
                }
                else if(indexPath.row == 1){

                    cell.imgView.image = [UIImage imageNamed:@"icon_gallery_export_3_2_active_96"];
                    cell.imgView_inactive.image = [UIImage imageNamed:@"icon_gallery_export_3_2_inactive_96"];
                    cell.imgView.hidden = YES;

                }

            }

            // For forth

            if (arrSelectedImages.count == 4) {

                if (indexPath.row == 0) {
                    cell.imgView.image = [UIImage imageNamed:@"icon_gallery_export_4_1_active_96"];
                    cell.imgView_inactive.image = [UIImage imageNamed:@"icon_gallery_export_4_1_inative_96"];
                    cell.imgView_inactive.hidden = YES;
                }
                else if(indexPath.row == 1){

                    cell.imgView.image = [UIImage imageNamed:@"icon_gallery_export_4_2_active_96"];
                    cell.imgView_inactive.image = [UIImage imageNamed:@"icon_gallery_export_4_2_inactive_96"];
                    cell.imgView.hidden = YES;

                }

                else if(indexPath.row == 2){

                    cell.imgView.image = [UIImage imageNamed:@"icon_gallery_export_4_3_active_96"];
                    cell.imgView_inactive.image = [UIImage imageNamed:@"icon_gallery_export_4_3_inactive_96"];
                    cell.imgView.hidden = YES;
                }

            }
            
            // For fifth
            
            if (arrSelectedImages.count == 5) {
                
                if (indexPath.row == 0) {
                    cell.imgView.image = [UIImage imageNamed:@"icon_gallery_export_5_1_active_96"];
                    cell.imgView_inactive.image = [UIImage imageNamed:@"icon_gallery_export_5_1_inactive_96"];
                    cell.imgView_inactive.hidden = YES;
                }
                
                else if(indexPath.row == 1){
                    
                    cell.imgView.image = [UIImage imageNamed:@"icon_gallery_export_5_2_active_96"];
                    cell.imgView_inactive.image = [UIImage imageNamed:@"icon_gallery_export_5_2_inactive_96"];
                    cell.imgView.hidden = YES;
                    
                }
                
            }
            
            // For Sixth
            
            if (arrSelectedImages.count == 6) {
                
                if (indexPath.row == 0) {
                    cell.imgView.image = [UIImage imageNamed:@"icon_gallery_export_6_1_active_96"];
                    cell.imgView_inactive.image = [UIImage imageNamed:@"icon_gallery_export_6_1_inactive_96"];
                    cell.imgView_inactive.hidden = YES;
                }
                
                else if(indexPath.row == 1){
                    
                    cell.imgView.image = [UIImage imageNamed:@"icon_gallery_export_6_2_active_96"];
                    cell.imgView_inactive.image = [UIImage imageNamed:@"icon_gallery_export_6_2_inactive_96"];
                    cell.imgView.hidden = YES;
                    
                }
                
                else if(indexPath.row == 2){
                    
                    cell.imgView.image = [UIImage imageNamed:@"icon_gallery_export_6_3_active_96"];
                    cell.imgView_inactive.image = [UIImage imageNamed:@"icon_gallery_export_6_3_inactive_96"];
                    cell.imgView.hidden = YES;
                    
                }
                
            }
            
            // For Seven
            if(arrSelectedImages.count == 7){
                if (indexPath.row == 0) {
                    cell.imgView.image = [UIImage imageNamed:@"icon_gallery_export_7_1_active_96"];
                    cell.imgView_inactive.image = [UIImage imageNamed:@"icon_gallery_export_7_1_inactive_96"];
                    cell.imgView_inactive.hidden = YES;
                }
            }
            
            
            // For Eight
            if(arrSelectedImages.count == 8){
                if (indexPath.row == 0) {
                    cell.imgView.image = [UIImage imageNamed:@"icon_gallery_export_8_1_active_96"];
                    cell.imgView_inactive.image = [UIImage imageNamed:@"icon_gallery_export_8_1_inactive_96"];
                    cell.imgView_inactive.hidden = YES;
                }
            }
            
            
            // For Nine
            if(arrSelectedImages.count == 9){
                
                cell.imgView.image = [UIImage imageNamed:@"icon_gallery_export_9_1_active_96"];
                cell.imgView_inactive.image = [UIImage imageNamed:@"icon_gallery_export_9_1_inactive_96"];
                cell.imgView_inactive.hidden = YES;
                
            }

        }
        
        if ([selectedTemplateType isEqualToString:@"second"]){


            // for two images

            if (arrSelectedImages.count == 2) {

                if (indexPath.row == 0) {
                    cell.imgView.image = [UIImage imageNamed:@"icon_gallery_export_2_1_active_96"];
                    cell.imgView_inactive.image = [UIImage imageNamed:@"icon_gallery_export_2_1_inactive_96"];
                    cell.imgView.hidden = YES;
                }
                else if(indexPath.row == 1){

                    cell.imgView.image = [UIImage imageNamed:@"icon_gallery_export_2_2_active_96"];
                    cell.imgView_inactive.image = [UIImage imageNamed:@"icon_gallery_export_2_2_inactive_96"];
                    cell.imgView_inactive.hidden = YES;

                }

            }

            // for three

            if (arrSelectedImages.count == 3) {

                if (indexPath.row == 0) {
                    cell.imgView.image = [UIImage imageNamed:@"icon_gallery_export_3_1_active_96"];
                    cell.imgView_inactive.image = [UIImage imageNamed:@"icon_gallery_export_3_1_inactive_96"];
                    cell.imgView.hidden = YES;
                }
                else if(indexPath.row == 1){

                    cell.imgView.image = [UIImage imageNamed:@"icon_gallery_export_3_2_active_96"];
                    cell.imgView_inactive.image = [UIImage imageNamed:@"icon_gallery_export_3_2_inactive_96"];
                    cell.imgView_inactive.hidden = YES;

                }

            }

            // For forth

            if (arrSelectedImages.count == 4) {

                if (indexPath.row == 0) {
                    cell.imgView.image = [UIImage imageNamed:@"icon_gallery_export_4_1_active_96"];
                    cell.imgView_inactive.image = [UIImage imageNamed:@"icon_gallery_export_4_1_inactive_96"];
                    cell.imgView.hidden = YES;
                }
                else if(indexPath.row == 1){

                    cell.imgView.image = [UIImage imageNamed:@"icon_gallery_export_4_2_active_96"];
                    cell.imgView_inactive.image = [UIImage imageNamed:@"icon_gallery_export_4_2_inactive_96"];
                    cell.imgView_inactive.hidden = YES;

                }

                else if(indexPath.row == 2){

                    cell.imgView.image = [UIImage imageNamed:@"icon_gallery_export_4_3_active_96"];
                    cell.imgView_inactive.image = [UIImage imageNamed:@"icon_gallery_export_4_3_inactive_96"];
                    cell.imgView.hidden = YES;
                }

            }
            
            // For fifth
            
            if (arrSelectedImages.count == 5) {
                
                if (indexPath.row == 0) {
                    cell.imgView.image = [UIImage imageNamed:@"icon_gallery_export_5_1_active_96"];
                    cell.imgView_inactive.image = [UIImage imageNamed:@"icon_gallery_export_5_1_inactive_96"];
                    cell.imgView.hidden = YES;
                }
                
                else if(indexPath.row == 1){
                    
                    cell.imgView.image = [UIImage imageNamed:@"icon_gallery_export_5_2_active_96"];
                    cell.imgView_inactive.image = [UIImage imageNamed:@"icon_gallery_export_5_2_inactive_96"];
                    cell.imgView_inactive.hidden = YES;
                    
                }
                
            }
            
            // For Sixth
            
            if (arrSelectedImages.count == 6) {
                
                if (indexPath.row == 0) {
                    cell.imgView.image = [UIImage imageNamed:@"icon_gallery_export_6_1_active_96"];
                    cell.imgView_inactive.image = [UIImage imageNamed:@"icon_gallery_export_6_1_inactive_96"];
                    cell.imgView.hidden = YES;
                }
                
                else if(indexPath.row == 1){
                    
                    cell.imgView.image = [UIImage imageNamed:@"icon_gallery_export_6_2_active_96"];
                    cell.imgView_inactive.image = [UIImage imageNamed:@"icon_gallery_export_6_2_inactive_96"];
                    cell.imgView_inactive.hidden = YES;
                    
                }
                
                else if(indexPath.row == 2){
                    
                    cell.imgView.image = [UIImage imageNamed:@"icon_gallery_export_6_3_active_96"];
                    cell.imgView_inactive.image = [UIImage imageNamed:@"icon_gallery_export_6_3_inactive_96"];
                    cell.imgView.hidden = YES;
                    
                }
                
            }
       

        }
        
        if ([selectedTemplateType isEqualToString:@"third"]){

            // for two images

            if (arrSelectedImages.count == 2) {

                if (indexPath.row == 0) {
                    cell.imgView.image = [UIImage imageNamed:@"icon_gallery_export_2_1_active_96"];
                    cell.imgView_inactive.image = [UIImage imageNamed:@"icon_gallery_export_2_1_inactive_96"];
                    cell.imgView.hidden = YES;
                }
                else if(indexPath.row == 1){

                    cell.imgView.image = [UIImage imageNamed:@"icon_gallery_export_2_2_active_96"];
                    cell.imgView_inactive.image = [UIImage imageNamed:@"icon_gallery_export_2_2_inactive_96"];
                    cell.imgView.hidden = YES;

                }

            }

            // for three

            if (arrSelectedImages.count == 3) {

                if (indexPath.row == 0) {
                    cell.imgView.image = [UIImage imageNamed:@"icon_gallery_export_3_1_active_96"];
                    cell.imgView_inactive.image = [UIImage imageNamed:@"icon_gallery_export_3_1_inactive_96"];
                    cell.imgView.hidden = YES;
                }

                else if(indexPath.row == 1){

                    cell.imgView.image = [UIImage imageNamed:@"icon_gallery_export_3_3_active_96"];
                    cell.imgView_inactive.image = [UIImage imageNamed:@"icon_gallery_export_3_3_inactive_96"];
                    cell.imgView_inactive.hidden = YES;
                }


            }

            // For forth

            if (arrSelectedImages.count == 4) {

                if (indexPath.row == 0) {
                    cell.imgView.image = [UIImage imageNamed:@"icon_gallery_export_4_1_active_96"];
                    cell.imgView_inactive.image = [UIImage imageNamed:@"icon_gallery_export_4_1_inactive_96"];
                    cell.imgView.hidden = YES;
                }
                else if(indexPath.row == 1){

                    cell.imgView.image = [UIImage imageNamed:@"icon_gallery_export_4_2_active_96"];
                    cell.imgView_inactive.image = [UIImage imageNamed:@"icon_gallery_export_4_2_inactive_96"];
                    cell.imgView.hidden = YES;

                }

                else if(indexPath.row == 2){

                    cell.imgView.image = [UIImage imageNamed:@"icon_gallery_export_4_3_active_96"];
                    cell.imgView_inactive.image = [UIImage imageNamed:@"icon_gallery_export_4_3_inactive_96"];
                    cell.imgView_inactive.hidden = YES;
                }

            }
            
            // For fifth
            
            if (arrSelectedImages.count == 5) {
                
                if (indexPath.row == 0) {
                    cell.imgView.image = [UIImage imageNamed:@"icon_gallery_export_5_1_active_96"];
                    cell.imgView_inactive.image = [UIImage imageNamed:@"icon_gallery_export_5_1_inactive_96"];
                    cell.imgView.hidden = YES;
                }
                
                else if(indexPath.row == 1){
                    
                    cell.imgView.image = [UIImage imageNamed:@"icon_gallery_export_53_active_96"];
                    cell.imgView_inactive.image = [UIImage imageNamed:@"icon_gallery_export_5_3_inactive_96"];
                    cell.imgView_inactive.hidden = YES;
                    
                }
                
            }
            
            // For Sixth
            
            if (arrSelectedImages.count == 6) {
                
                if (indexPath.row == 0) {
                    cell.imgView.image = [UIImage imageNamed:@"icon_gallery_export_6_1_active_96"];
                    cell.imgView_inactive.image = [UIImage imageNamed:@"icon_gallery_export_6_1_inactive_96"];
                    cell.imgView.hidden = YES;
                }
                
                else if(indexPath.row == 1){
                    
                    cell.imgView.image = [UIImage imageNamed:@"icon_gallery_export_6_2_active_96"];
                    cell.imgView_inactive.image = [UIImage imageNamed:@"icon_gallery_export_6_2_inactive_96"];
                    cell.imgView.hidden = YES;
                    
                }
                
                else if(indexPath.row == 2){
                    
                    cell.imgView.image = [UIImage imageNamed:@"icon_gallery_export_6_3_active_96"];
                    cell.imgView_inactive.image = [UIImage imageNamed:@"icon_gallery_export_6_3_inactive_96"];
                    cell.imgView_inactive.hidden = YES;
                    
                }
                
            }
            
          
            
        }
        
//        if ([selectedTemplateType isEqualToString:@"fourth"]){
//
//            if (arrSelectedImages.count == 1) {
//
//                if (indexPath.row == 0) {
//                    cell.imgView.image = [UIImage imageNamed:@"icon_gallery_export_1_1_active_192"];
//                    cell.imgView_inactive.image = [UIImage imageNamed:@"icon_gallery_export_1_1_inactive_192"];
//                    cell.imgView.hidden = YES;
//                }
//                else if(indexPath.row == 1){
//
//                    cell.imgView.image = [UIImage imageNamed:@"icon_gallery_export_1_2_active_192"];
//                    cell.imgView_inactive.image = [UIImage imageNamed:@"icon_gallery_export_1_2_inactive_192"];
//                    cell.imgView.hidden = YES;
//
//                }
//
//                else if(indexPath.row == 2){
//
//                    cell.imgView.image = [UIImage imageNamed:@"icon_gallery_export_1_3_active_192"];
//                    cell.imgView_inactive.image = [UIImage imageNamed:@"icon_gallery_export_1_3_inactive_192"];
//                    cell.imgView.hidden = YES;
//                }
//
//                else if(indexPath.row == 3){
//
//                    cell.imgView.image = [UIImage imageNamed:@"icon_gallery_export_1_4_active_192"];
//                    cell.imgView_inactive.image = [UIImage imageNamed:@"icon_gallery_export_1_4_inactive_192"];
//                    cell.imgView_inactive.hidden = YES;
//                }
//
//            }
//
//
//            // for two images
//
//            if (arrSelectedImages.count == 2) {
//
//                if (indexPath.row == 0) {
//                    cell.imgView.image = [UIImage imageNamed:@"icon_gallery_export_2_1_active_192"];
//                    cell.imgView_inactive.image = [UIImage imageNamed:@"icon_gallery_export_2_1_inactive_192"];
//                    cell.imgView.hidden = YES;
//                }
//                else if(indexPath.row == 1){
//
//                    cell.imgView.image = [UIImage imageNamed:@"icon_gallery_export_2_2_active_192"];
//                    cell.imgView_inactive.image = [UIImage imageNamed:@"icon_gallery_export_2_2_inactive_192"];
//                    cell.imgView.hidden = YES;
//
//                }
//
//                else if(indexPath.row == 2){
//
//                    cell.imgView.image = [UIImage imageNamed:@"icon_gallery_export_2_3_active_192"];
//                    cell.imgView_inactive.image = [UIImage imageNamed:@"icon_gallery_export_2_3_inactive_192"];
//                    cell.imgView.hidden = YES;
//                }
//
//                else if(indexPath.row == 3){
//
//                    cell.imgView.image = [UIImage imageNamed:@"icon_gallery_export_2_4_active_192"];
//                    cell.imgView_inactive.image = [UIImage imageNamed:@"icon_gallery_export_2_4_inactive_192"];
//                    cell.imgView_inactive.hidden = YES;
//                }
//
//
//            }
//
//            // for three
//
//            if (arrSelectedImages.count == 3) {
//
//                if (indexPath.row == 0) {
//                    cell.imgView.image = [UIImage imageNamed:@"icon_gallery_export_3_1_active_192"];
//                    cell.imgView_inactive.image = [UIImage imageNamed:@"icon_gallery_export_3_1_inactive_192"];
//                    cell.imgView.hidden = YES;
//                }
//                else if(indexPath.row == 1){
//
//                    cell.imgView.image = [UIImage imageNamed:@"icon_gallery_export_3_2_active_192"];
//                    cell.imgView_inactive.image = [UIImage imageNamed:@"icon_gallery_export_3_2_inactive_192"];
//                    cell.imgView.hidden = YES;
//
//                }
//
//                else if(indexPath.row == 2){
//
//                    cell.imgView.image = [UIImage imageNamed:@"icon_gallery_export_3_3_active_192"];
//                    cell.imgView_inactive.image = [UIImage imageNamed:@"icon_gallery_export_3_3_inactive_192"];
//                    cell.imgView.hidden = YES;
//                }
//
//                else if(indexPath.row == 3){
//
//                    cell.imgView.image = [UIImage imageNamed:@"icon_gallery_export_3_4_active_192"];
//                    cell.imgView_inactive.image = [UIImage imageNamed:@"icon_gallery_export_3_4_inactive_192"];
//                    cell.imgView_inactive.hidden = YES;
//                }
//
//            }
//
//            // For forth
//
//            if (arrSelectedImages.count == 4) {
//
//                if (indexPath.row == 0) {
//                    cell.imgView.image = [UIImage imageNamed:@"icon_gallery_export_4_1_active_192"];
//                    cell.imgView_inactive.image = [UIImage imageNamed:@"icon_gallery_export_4_1_inactive_192"];
//                    cell.imgView.hidden = YES;
//                }
//                else if(indexPath.row == 1){
//
//                    cell.imgView.image = [UIImage imageNamed:@"icon_gallery_export_4_2_active_192"];
//                    cell.imgView_inactive.image = [UIImage imageNamed:@"icon_gallery_export_4_2_inactive_192"];
//                    cell.imgView.hidden = YES;
//
//                }
//
//                else if(indexPath.row == 2){
//
//                    cell.imgView.image = [UIImage imageNamed:@"icon_gallery_export_4_3_active_192"];
//                    cell.imgView_inactive.image = [UIImage imageNamed:@"icon_gallery_export_4_3_inactive_192"];
//                    cell.imgView.hidden = YES;
//                }
//
//                else if(indexPath.row == 3){
//
//                    cell.imgView.image = [UIImage imageNamed:@"icon_gallery_export_4_4_active_192"];
//                    cell.imgView_inactive.image = [UIImage imageNamed:@"icon_gallery_export_4_4_inactive_192"];
//                    cell.imgView_inactive.hidden = YES;
//                }
//            }
//        }
        
        
     // New one End
        
    // OLD ONE
     /*   if (indexPath.row == 0) {
            
            selectedTemplateType = @"first";
            
            if (arrSelectedImages.count == 1) {
                
                cell.imgView.image = [UIImage imageNamed:@"icon_gallery_export_1_1_active_192.png"];
                
            }
            else if (arrSelectedImages.count == 2) {
                cell.imgView.image = [UIImage imageNamed:@"icon_gallery_export_2_1_active_192.png"];
                
                
                
                
                //
            }
            else if (arrSelectedImages.count >= 3) {
                cell.imgView.image = [UIImage imageNamed:@""];
                //
            }
            
            
        }else if (indexPath.row == 1) {
            selectedTemplateType = @"second";
            
            if (arrSelectedImages.count == 1) {
                
                
                
                cell.imgView.image = [UIImage imageNamed:@"icon_gallery_export_1_2_active_192.png"];
                
                
                //
            }
            else if (arrSelectedImages.count == 2) {
                
                
                cell.imgView.image = [UIImage imageNamed:@"icon_gallery_export_2_2_active_192.png"];
                
                
                //
            }
            else if (arrSelectedImages.count >= 3) {
                cell.imgView.image = [UIImage imageNamed:@""];
                
            }
            
        }else if (indexPath.row == 2) {
            selectedTemplateType = @"third";
            
            if (arrSelectedImages.count == 1) {
                
                cell.imgView.image = [UIImage imageNamed:@"icon_gallery_export_1_3_active_192.png"];
                
                
                //
            }
            else if (arrSelectedImages.count == 2) {
                
                
                cell.imgView.image = [UIImage imageNamed:@"icon_gallery_export_2_3_active_192.png"];
                
                //
            }
            else if (arrSelectedImages.count >= 3) {
                cell.imgView.image = [UIImage imageNamed:@""];
                
            }
            
        }else if (indexPath.row == 3) {
            selectedTemplateType = @"fourth";
            
            if (arrSelectedImages.count == 1) {
                
                cell.imgView.image = [UIImage imageNamed:@"icon_gallery_export_1_4_active_192.png"];
                //
            }
            else if (arrSelectedImages.count == 2) {
                
                cell.imgView.image = [UIImage imageNamed:@"icon_gallery_export_2_4_active_192.png"];
                //
            }
            else if (arrSelectedImages.count >= 3) {
                cell.imgView.image = [UIImage imageNamed:@""];
                
            }
        }
*/
        
        return cell;

    }
    
    else {
        [collectionView registerNib:[UINib nibWithNibName:@"collectionCellGallaryLayout" bundle:nil] forCellWithReuseIdentifier:@"collectionCellGallaryLayout"];
        
        //gallaryImageCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"collectionCellGallaryLayout" forIndexPath:indexPath];
        
        collectionCellGallaryLayout *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"collectionCellGallaryLayout" forIndexPath:indexPath];
        
        // Anil
        
        layout = (UICollectionViewFlowLayout *)[self.selectedImagesCollectionView collectionViewLayout];
        
        if ([selectedTemplateType isEqualToString:@"first"]){
            
            if(arrSelectedImages.count == 3){
                layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
            }
            if(arrSelectedImages.count == 5 || arrSelectedImages.count == 7 || arrSelectedImages.count == 8){
                layout.scrollDirection = UICollectionViewScrollDirectionVertical;
            }
            
            if(arrSelectedImages.count == 6){
                
                _viewCollectionSecondary.hidden = YES;
                
            }
            
        }
        
        if ([selectedTemplateType isEqualToString:@"second"]){
            
            if(arrSelectedImages.count == 3){
                layout.scrollDirection = UICollectionViewScrollDirectionVertical;
            }
            if(arrSelectedImages.count == 4){
                layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
            }
            
            if(arrSelectedImages.count == 6){

            _viewCollectionSecondary.hidden = NO;
              
            }
        }
        
        if ([selectedTemplateType isEqualToString:@"third"]){
            
            if(arrSelectedImages.count == 4){
                layout.scrollDirection = UICollectionViewScrollDirectionVertical;
            }
            
            if(arrSelectedImages.count == 6){
                
                _viewCollectionSecondary.hidden = YES;
            }
        }
        
        if(arrSelectedImages.count >3){
            
            cell.heightCBottomView.constant = 0;
            cell.viewBottom.hidden = YES;
            cell.imgViewIcon.hidden = NO;
        }
        
        cell.imgViewLayout.backgroundColor = UIColorFromRGB(0xD9CEC4);
        cell.imgViewLayout.image = [UIImage imageNamed:@""];
        
        cell.imgViewIcon.layer.cornerRadius = cell.imgViewIcon.bounds.size.width/2;
        cell.imgViewIcon.clipsToBounds = YES;
        
        cell.imgViewIconLabel.layer.cornerRadius = cell.imgViewIconLabel.bounds.size.width/2;
        cell.imgViewIconLabel.clipsToBounds = YES;
        
        //cell.imgViewIcon.backgroundColor = [UIColor yellowColor];//UIColorFromRGB(0xD9CEC4);
        
        
        
        
        
        
        
        
        
        
        

        cell.lblBlocksValue.text = @"";
        if (indexPath.row<arrSelectedImages.count) {
            if  ([[arrSelectedImages objectAtIndex:indexPath.row] isKindOfClass:[NSNull class]]) {
                cell.imgViewLayout.image = [UIImage imageNamed:@"dummy.png"];
               

            }
            else {

                NSLog(@"%lu", (unsigned long)[arrSelectedImages count]);
                id imgUrl = [arrSelectedImages objectAtIndex:indexPath.row];

                if([imgUrl isKindOfClass:[UIImage class]]){
                 
                    cell.imgViewIcon.image = [UIImage imageNamed:@""];
                    cell.imgViewIconLabel.image = [UIImage imageNamed:@""];
                    cell.lblActivityName.text = @"";
                    cell.imgViewIcon.backgroundColor = [UIColor clearColor];
                    cell.imgViewIconLabel.backgroundColor = [UIColor clearColor];
                    
        cell.imgViewLayout.contentMode = UIViewContentModeScaleToFill;
        cell.imgViewLayout.image = [UIImage imageNamed:@""];
                    cell.imgViewLayout.image = imgUrl;
                    cell.imgViewLayout.backgroundColor = [UIColor whiteColor];

            cell.lblBlocksValue.hidden = NO;
            cell.lblBlocksValue.text = @"";
            cell.lblBlocksValue.textColor = UIColorFromRGB(0x8D8082);

                    if (IS_IPAD) {
                        cell.lblBlocksValue.font = [UIFont fontWithName:@"SFUIDisplay-Medium" size:30.0f];
                    }

                    NSLog(@"dict is %@",dictList);
                    //for (int i=0; i<arrSelectedDataBlocks.count; i++) {
                        if ([[arrSelectedDataBlocks objectAtIndex:indexPath.row] isEqualToString:@"height"]){

                            cell.lblBlocksValue.textAlignment = NSTextAlignmentCenter;

                            if(dictList.count>0){
                            NSString *strHeight = [NSString stringWithFormat:@"%@",[dictList valueForKey:@"height"]];

                            cell.lblBlocksValue.text = strHeight;
                            
                            }


                        }
                        if([[arrSelectedDataBlocks objectAtIndex:indexPath.row] isEqualToString:@"weight"]){

                            cell.lblBlocksValue.textAlignment = NSTextAlignmentCenter;
                            if(dictList.count>0){

                        NSString *strWeight = [NSString stringWithFormat:@"%@",[dictList valueForKey:@"weight"]];

                        cell.lblBlocksValue.text = strWeight;
                            }

                        }
                        if([[arrSelectedDataBlocks objectAtIndex:indexPath.row] isEqualToString:@"age"]){

                            cell.lblBlocksValue.textAlignment = NSTextAlignmentCenter;
                            if(dictList.count>0){
                            NSString *strYear = [NSString stringWithFormat:@"%@",[dictList valueForKey:@"birth_year"]];

                                NSString *strMonth = [NSString stringWithFormat:@"%@",[dictList valueForKey:@"birth_month"]];

                                if([strYear isEqualToString:@"0"]) {
                                NSString *strBirth = [NSString stringWithFormat:@"%@",strMonth];

                                    cell.lblBlocksValue.text = strBirth;
                                }
                                else{
                                 NSString *strBirth = [NSString stringWithFormat:@"%@ year %@",strYear,strMonth];

                                cell.lblBlocksValue.text = strBirth;
                            }
                            }

                        }
                        if([[arrSelectedDataBlocks objectAtIndex:indexPath.row] isEqualToString:@"achievement"]){

                            cell.lblBlocksValue.textAlignment = NSTextAlignmentCenter;

                            if(dictList.count>0){
    NSString *strAchievement = [NSString stringWithFormat:@"I achieved %@ development goals",[dictList valueForKey:@"achievement"]];

    cell.lblBlocksValue.text = strAchievement;
    cell.imgViewLayout.image = [UIImage imageNamed:@""];
    

                            }

                        }
                    //}
}


                else{
                    
                    NSArray *arr = [imgUrl componentsSeparatedByString:@","];
                    NSString *strImgURL = [arr objectAtIndex:0];
                    NSString *strName = [arr objectAtIndex:1];
                    NSString *strNotes = [arr objectAtIndex:2];
                    
                    cell.imgViewLayout.contentMode = UIViewContentModeScaleAspectFill;
                    [cell.imgViewLayout sd_setImageWithURL:[NSURL URLWithString:strImgURL] placeholderImage:[UIImage imageNamed:@"dummy.png"]];
                    cell.imgViewLayout.backgroundColor = UIColorFromRGB(0xD9CEC4);
                    
                    NSString *name = strName;
                    if ([name isKindOfClass:[NSNull class]]) {
                        name = @"others";
                    }
                    
                    UIImage *imgIcon = [UIImage imageNamed:[NSString stringWithFormat:@"icon_%@.png", name]];
                    
                    if ([name isEqualToString:@"birthday"] || [name isEqualToString:@"reminder"]) {
                        imgIcon = [UIImage imageNamed:[NSString stringWithFormat:@"icon_%@.png", name]];
                    }
                    else if ([name isEqualToString:@"grow"] || [name isEqualToString:@"move"] || [name isEqualToString:@"iq"] || [name isEqualToString:@"health"] || [name isEqualToString:@"others"] || [name isEqualToString:@"pregnancy"]){
                        imgIcon = [UIImage imageNamed:[NSString stringWithFormat:@"icon_%@_Selected.png", name]];
                    }
                    
                    // Activities
                    if([name isEqualToString:@"feed"]){
                        cell.imgViewIcon.backgroundColor = UIColorFromRGB(0xF4EA9E);
                        cell.imgViewIconLabel.backgroundColor = UIColorFromRGB(0xF4EA9E);
                    }if([name isEqualToString:@"pump"]){
                        cell.imgViewIcon.backgroundColor = UIColorFromRGB(0xF9C99B);
                        cell.imgViewIconLabel.backgroundColor = UIColorFromRGB(0xF9C99B);
                    }if([name isEqualToString:@"sleep"]){
                        cell.imgViewIcon.backgroundColor = UIColorFromRGB(0xC5D7E1);
                        cell.imgViewIconLabel.backgroundColor = UIColorFromRGB(0xC5D7E1);
                    }if([name isEqualToString:@"poo"]){
                        cell.imgViewIcon.backgroundColor = UIColorFromRGB(0xE0C5B8);
                        cell.imgViewIconLabel.backgroundColor = UIColorFromRGB(0xE0C5B8);
                    }if([name isEqualToString:@"wee"]){
                        cell.imgViewIcon.backgroundColor = UIColorFromRGB(0xCADAA9);
                        cell.imgViewIconLabel.backgroundColor = UIColorFromRGB(0xCADAA9);
                    }if([name isEqualToString:@"medical"]){
                        cell.imgViewIcon.backgroundColor = UIColorFromRGB(0xF4D2D5);
                        cell.imgViewIconLabel.backgroundColor = UIColorFromRGB(0xF4D2D5);
                    }
                    
                    // Milestones
                    if([name isEqualToString:@"birthday"]){
                        cell.imgViewIcon.backgroundColor = UIColorFromRGB(0xB9CDDA);
                        cell.imgViewIconLabel.backgroundColor = UIColorFromRGB(0xB9CDDA);
                    }if([name isEqualToString:@"grow"]){
                        cell.imgViewIcon.backgroundColor = UIColorFromRGB(0xF3E99F);
                        cell.imgViewIconLabel.backgroundColor = UIColorFromRGB(0xF3E99F);
                    }if([name isEqualToString:@"move"]){
                        cell.imgViewIcon.backgroundColor = UIColorFromRGB(0xFFC896);
                        cell.imgViewIconLabel.backgroundColor = UIColorFromRGB(0xFFC896);
                    }if([name isEqualToString:@"iq"]){
                        cell.imgViewIcon.backgroundColor = UIColorFromRGB(0xC5DEAA);
                        cell.imgViewIconLabel.backgroundColor = UIColorFromRGB(0xC5DEAA);
                    }if([name isEqualToString:@"health"]){
                        cell.imgViewIcon.backgroundColor = UIColorFromRGB(0xFACFD4);
                        cell.imgViewIconLabel.backgroundColor = UIColorFromRGB(0xFACFD4);
                    }if([name isEqualToString:@"others"]){
                        cell.imgViewIcon.backgroundColor = UIColorFromRGB(0xDFCCDC);
                       cell.imgViewIconLabel.backgroundColor = UIColorFromRGB(0xDFCCDC);
                    }if ([name isEqualToString:@"pregnancy"]) {
                        cell.imgViewIcon.backgroundColor = UIColorFromRGB(0xDCC4E8);
                        cell.imgViewIconLabel.backgroundColor = UIColorFromRGB(0xDCC4E8);
                    }
                    
                    
                    cell.imgViewIcon.image = imgIcon;
                    cell.imgViewIconLabel.image  = imgIcon;
                    cell.lblActivityName.text = [NSString stringWithFormat:@"[%@] - [%@]",strName,strNotes];
                    
                    
                }

            }
        }
        else
        {
            cell.imgViewLayout.image = [UIImage imageNamed:@"dummy.png"];
        }
        

       
        return cell;
    }
    
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    // Anil
    CGFloat w = 0; // Anil
    CGFloat h = 0; // Anil
    
    if (collectionView == self.selectedImagesCollectionView){
        
        //UICollectionViewFlowLayout *layout = (UICollectionViewFlowLayout *)[self.selectedImagesCollectionView collectionViewLayout];
    
        if(arrSelectedImages.count == 1){
            if ([selectedTemplateType isEqualToString:@"first"]){
            w = (self.selectedImagesCollectionView.bounds.size.width);
            h = (self.selectedImagesCollectionView.bounds.size.height);
            return CGSizeMake(w, h);
        }
    }
        
        if(arrSelectedImages.count == 2){
            if ([selectedTemplateType isEqualToString:@"first"]){
            
                w = (self.selectedImagesCollectionView.bounds.size.width-8)/1.0f;
                h = (self.selectedImagesCollectionView.bounds.size.height-8)/2.0f;
                return CGSizeMake(w, h);
            }
            
            else if([selectedTemplateType isEqualToString:@"second"]){
                
                w = (self.selectedImagesCollectionView.bounds.size.width-8)/2.0f;
                h = (self.selectedImagesCollectionView.bounds.size.height-8)/1.0f;
                return CGSizeMake(w, h);

            }
            
        }
        
        if(arrSelectedImages.count == 3){
            
            if ([selectedTemplateType isEqualToString:@"first"]){
                
    
                //layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;

                if(indexPath.row == 0){
                w = (self.selectedImagesCollectionView.bounds.size.width-8)/2.0f;
                h = (self.selectedImagesCollectionView.bounds.size.height-8)/1.0f;
                return CGSizeMake(w, h);
                }
                
                else{
                    w = (self.selectedImagesCollectionView.bounds.size.width-8)/2.0f;
                    h = (self.selectedImagesCollectionView.bounds.size.height-8)/2.0f;
                    return CGSizeMake(w, h);
                }
            }
            
            
           else if ([selectedTemplateType isEqualToString:@"second"]){
              
               //layout.scrollDirection = UICollectionViewScrollDirectionVertical;
               
               if(indexPath.row == 0){
                   w = (self.selectedImagesCollectionView.bounds.size.width-8)/1.0f;
                   h = (self.selectedImagesCollectionView.bounds.size.height-8)/2.0f;
                   return CGSizeMake(w, h);
               }
               
               
               else{
                   w = (self.selectedImagesCollectionView.bounds.size.width-8)/2.0f;
                   h = (self.selectedImagesCollectionView.bounds.size.height-8)/2.0f;
                   return CGSizeMake(w, h);
               }
                
            }
        }
            
        if(arrSelectedImages.count == 4){
            
            if ([selectedTemplateType isEqualToString:@"first"]){
                
                    w = (self.selectedImagesCollectionView.bounds.size.width-8)/2.0f;
                    h = (self.selectedImagesCollectionView.bounds.size.height-8)/2.0f;
                    return CGSizeMake(w, h);
            }
            
            if ([selectedTemplateType isEqualToString:@"second"]){
                
                
                if(indexPath.row == 0 || indexPath.row == 3){
                    w = (self.selectedImagesCollectionView.bounds.size.width-8)/2.0f;
                    h = (self.selectedImagesCollectionView.bounds.size.height-8) *0.599;
                    return CGSizeMake(w, h);
                }
                
                else if(indexPath.row == 1 || indexPath.row == 2){
                    w = (self.selectedImagesCollectionView.bounds.size.width-8)/2.0f;
                    h = (self.selectedImagesCollectionView.bounds.size.height-8) *0.40;
                    return CGSizeMake(w, h);
                }
            }
            
            if ([selectedTemplateType isEqualToString:@"third"]){
                
                if(indexPath.row == 0){
                    w = (self.selectedImagesCollectionView.bounds.size.width-8);
                    h = (self.selectedImagesCollectionView.bounds.size.height-8) * 0.67;
                    return CGSizeMake(w, h);
                }
                
                else{
                    w = (self.selectedImagesCollectionView.bounds.size.width-10)/3;
                    h = (self.selectedImagesCollectionView.bounds.size.height-8) * 0.33;
                    return CGSizeMake(w, h);
                }
            }
            
        }
        
        
        
        if(arrSelectedImages.count == 5){
            
            if ([selectedTemplateType isEqualToString:@"first"]){
                if(indexPath.row == 0 || indexPath.row == 1){
                
                    w = (self.selectedImagesCollectionView.bounds.size.width-8)/2.0f;
                    h = (self.selectedImagesCollectionView.bounds.size.height-8) * 0.67;
                    return CGSizeMake(w, h);
                }
                else{
                    w = (self.selectedImagesCollectionView.bounds.size.width-10)/3;
                    h = (self.selectedImagesCollectionView.bounds.size.height-8) * 0.33;
                    return CGSizeMake(w, h);
                }
                
            }
            
            if ([selectedTemplateType isEqualToString:@"second"]){
                
                
                if(indexPath.row == 0 || indexPath.row == 1 || indexPath.row == 3 || indexPath.row == 4){
                    w = (self.selectedImagesCollectionView.bounds.size.width-8)/2.0f;
                    h = (self.selectedImagesCollectionView.bounds.size.height-8)/3.0f;
                    return CGSizeMake(w, h);
                }

                else {
                    w = (self.selectedImagesCollectionView.bounds.size.width-8);
                    h = (self.selectedImagesCollectionView.bounds.size.height-8)/3.0f;
                    return CGSizeMake(w, h);
                }
            }
            
        }
        
        
        if(arrSelectedImages.count == 6){
            
            if ([selectedTemplateType isEqualToString:@"first"]){
               
                    w = (self.selectedImagesCollectionView.bounds.size.width-8)/3.0f;
                    h = (self.selectedImagesCollectionView.bounds.size.height-8)/2.0f;
                    return CGSizeMake(w, h);
                
            }
            
            if ([selectedTemplateType isEqualToString:@"second"]){
                
                if(indexPath.row == 3){

                w = (self.selectedImagesCollectionView.bounds.size.width-8) * 0.65;
                h = (self.selectedImagesCollectionView.bounds.size.height-8) * 0.65;
                return CGSizeMake(w, h);
            }

              else if(indexPath.row == 4 || indexPath.row == 5){

                    w = ((self.selectedImagesCollectionView.bounds.size.width-8) * 0.65)/2;
                    h = (self.selectedImagesCollectionView.bounds.size.height-8) * 0.33;
                    return CGSizeMake(w, h);
            }


                else{
                    w = (self.selectedImagesCollectionView.bounds.size.width-8) * 0.33;
                    h = (self.selectedImagesCollectionView.bounds.size.height-8) * 0.33;
                    return CGSizeMake(w, h);
                }
                
            }
            
            
            if ([selectedTemplateType isEqualToString:@"third"]){
                
                w = (self.selectedImagesCollectionView.bounds.size.width-8)/2.0f;
                h = (self.selectedImagesCollectionView.bounds.size.height-8)/3.0f;
                return CGSizeMake(w, h);
                
            }
            
        }
        
        
        if(arrSelectedImages.count == 7){
            
            if ([selectedTemplateType isEqualToString:@"first"]){
                
                if(indexPath.row == 3){
                    w = (self.selectedImagesCollectionView.bounds.size.width-8);
                    h = (self.selectedImagesCollectionView.bounds.size.height-8)/3.0f;
                    return CGSizeMake(w, h);
                }
                else{
                    w = (self.selectedImagesCollectionView.bounds.size.width-10)/3.0f;
                    h = (self.selectedImagesCollectionView.bounds.size.height-10)/3.0f;
                    return CGSizeMake(w, h);
                }
                
               
                
            }
        }
        
        if(arrSelectedImages.count == 8){
            
            if ([selectedTemplateType isEqualToString:@"first"]){
                
                if(indexPath.row == 3 || indexPath.row == 4){
                    w = (self.selectedImagesCollectionView.bounds.size.width-8)/2.0f;
                    h = (self.selectedImagesCollectionView.bounds.size.height-8)/3.0f;
                    return CGSizeMake(w, h);
                }
                else{
                    w = (self.selectedImagesCollectionView.bounds.size.width-10)/3.0f;
                    h = (self.selectedImagesCollectionView.bounds.size.height-10)/3.0f;
                    return CGSizeMake(w, h);
                }
            }
        }
        
        if(arrSelectedImages.count == 9){
            
            if ([selectedTemplateType isEqualToString:@"first"]){
                
                    w = (self.selectedImagesCollectionView.bounds.size.width-8)/3.0f;
                    h = (self.selectedImagesCollectionView.bounds.size.height-8)/3.0f;
                    return CGSizeMake(w, h);
            }
        }

        
        
    // New one start
        
//        if (arrSelectedImages.count == 1) {
//            if ([selectedTemplateType isEqualToString:@"first"])
//            {
//
//            w = (self.selectedImagesCollectionView.bounds.size.width);
//            h = (self.selectedImagesCollectionView.bounds.size.height)*0.80;
//                return CGSizeMake(w, h);
//            }
//            else if ([selectedTemplateType isEqualToString:@"second"])
//            {
//
//            w = (self.selectedImagesCollectionView.bounds.size.width-8);
//            h = (self.selectedImagesCollectionView.bounds.size.height)*0.60;
//
//                return CGSizeMake(w, h);
//            }
//            else if ([selectedTemplateType isEqualToString:@"third"])
//            {
//            w = self.selectedImagesCollectionView.bounds.size.width-16;
//                h = (self.selectedImagesCollectionView.bounds.size.height) *0.80;
//
//                return CGSizeMake(w, h);
//
//            }
//            else if ([selectedTemplateType isEqualToString:@"fourth"])
//            {
//                w = (self.selectedImagesCollectionView.bounds.size.width)*0.80;
//                h = (self.selectedImagesCollectionView.bounds.size.height-16);
//
//                return CGSizeMake(w, h);
//
//            }
//
//        }
        
//        if (arrSelectedImages.count == 2) {
//            if ([selectedTemplateType isEqualToString:@"first"])
//            {
//        w = (self.selectedImagesCollectionView.bounds.size.width-8)/2.0f;
//        h = (self.selectedImagesCollectionView.bounds.size.height-8)/1.0f;
//                return CGSizeMake(w, h);
//
//            }
//            else if ([selectedTemplateType isEqualToString:@"second"])
//            {
//                if (indexPath.row == 0) {
//        w = (self.selectedImagesCollectionView.bounds.size.width-8) *0.60;
//        h = (self.selectedImagesCollectionView.bounds.size.height-16);
//
//                    return CGSizeMake(w, h);
//                }
//
//                else {
//        w = (self.selectedImagesCollectionView.bounds.size.width-8 ) *0.40;
//        h = (self.selectedImagesCollectionView.bounds.size.height-16)/1.0f;
//
//                    return CGSizeMake(w, h);
//                }
//
//            }
//            else if ([selectedTemplateType isEqualToString:@"third"])
//            {
//        w = (self.selectedImagesCollectionView.bounds.size.width-16)/1.0f;
//        h = (self.selectedImagesCollectionView.bounds.size.height-8)/2;
//
//                    return CGSizeMake(w, h);
//            }
//
//
//            else if ([selectedTemplateType isEqualToString:@"fourth"])
//            {
//        w = (self.selectedImagesCollectionView.bounds.size.width-8)/2.0f;
//        h = (self.selectedImagesCollectionView.bounds.size.height-32)/1.0f;
//
//                return CGSizeMake(w, h);
//
//            }
//
//        }
        
       
//        if (arrSelectedImages.count >= 3) {
//            int w = 0;
//            int h = 0;
//            if (collectionView == self.selectedImagesCollectionView){
//
//                if ([selectedTemplateType isEqualToString:@"first"])
//                {
//                    w = (self.selectedImagesCollectionView.bounds.size.width-8)/3.0f;
//                    return CGSizeMake(w, w);
//                }
//                else if ([selectedTemplateType isEqualToString:@"second"])
//                {
//                    w = (self.selectedImagesCollectionView.bounds.size.width-8)/3.0f;
//                    h = (self.selectedImagesCollectionView.bounds.size.height-8)/4.0f;
//
//                    if (indexPath.row == 0 || indexPath.row == 1 || indexPath.row == 2)
//                    {
//                        return CGSizeMake(w, h);
//                    }
//                    else if (indexPath.row == 3 || indexPath.row == 4 || indexPath.row == 5)
//                    {
//                        h = h*2;
//                        return CGSizeMake(w, h);
//                    }
//                    else
//                    {
//                        return CGSizeMake(w, h);
//                    }
//                }
//                else if ([selectedTemplateType isEqualToString:@"third"])
//                {
//                    w = self.selectedImagesCollectionView.bounds.size.width-4;
//                    h = w/2;
//                    if (indexPath.row == 0)
//                    {
//                        return CGSizeMake(w, h);
//                    }
//                    else
//                    {
//                        w = (self.selectedImagesCollectionView.bounds.size.width-8)/4.0f;
//                        return CGSizeMake(w, w);
//                    }
//                }
//                else if ([selectedTemplateType isEqualToString:@"fourth"])
//                {
//                    w = self.selectedImagesCollectionView.bounds.size.width-4;
//                    h = w/2;
//                    if (indexPath.row == 4)
//                    {
//                        return CGSizeMake(w, h);
//                    }
//                    else
//                    {
//                        w = (self.selectedImagesCollectionView.bounds.size.width-8)/4.0f;
//                        return CGSizeMake(w, w);
//                    }
//                }
//            }
//            else if (collectionView == self.templateTypeCollectionView){
//                w = (self.templateTypeCollectionView.bounds.size.width)/4.0f;
//            }
//            return CGSizeMake(w, w);
//        }
        
        // New one end
        
//        if ([selectedTemplateType isEqualToString:@"first"])
//        {
//            w = (self.selectedImagesCollectionView.bounds.size.width-8)/3.0f;
//            return CGSizeMake(w, w);
//        }
//        else if ([selectedTemplateType isEqualToString:@"second"])
//        {
//            w = (self.selectedImagesCollectionView.bounds.size.width-8)/3.0f;
//            h = (self.selectedImagesCollectionView.bounds.size.height-8)/4.0f;
//
//            if (indexPath.row == 0 || indexPath.row == 1 || indexPath.row == 2)
//            {
//                return CGSizeMake(w, h);
//            }
//            else if (indexPath.row == 3 || indexPath.row == 4 || indexPath.row == 5)
//            {
//               h = h*2;
//                return CGSizeMake(w, h);
//            }
//            else
//            {
//                return CGSizeMake(w, h);
//            }
//        }
//        else if ([selectedTemplateType isEqualToString:@"third"])
//        {
//            w = self.selectedImagesCollectionView.bounds.size.width-4;
//            h = w/2;
//            if (indexPath.row == 0)
//            {
//                return CGSizeMake(w, h);
//            }
//            else
//            {
//                w = (self.selectedImagesCollectionView.bounds.size.width-8)/4.0f;
//                return CGSizeMake(w, w);
//            }
//        }
//        else if ([selectedTemplateType isEqualToString:@"fourth"])
//        {
//            w = self.selectedImagesCollectionView.bounds.size.width-4;
//            h = w/2;
//            if (indexPath.row == 4)
//            {
//                return CGSizeMake(w, h);
//            }
//            else
//            {
//                w = (self.selectedImagesCollectionView.bounds.size.width-8)/4.0f;
//                return CGSizeMake(w, w);
//            }
//        }
    }
    
    
    else if (collectionView == self.templateTypeCollectionView){
      
//      int cellNumbers = [self setLayoutWidth];
//      NSLog(@"cell numbers %d",cellNumbers);
//      _widthConstraintLayout.constant = (cellNumbers * 50);
//
//        w = (cellNumbers * 50)/cellNumbers;//(self.templateTypeCollectionView.bounds.size.width)/4.0f;
//        h = (cellNumbers * 50)/cellNumbers;//(self.templateTypeCollectionView.bounds.size.width)/4.0f;
        
        w = (self.templateTypeCollectionView.bounds.size.width)/4.0f;
        h = (self.templateTypeCollectionView.bounds.size.width)/4.0f;
    }
    
    return CGSizeMake(w, h);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    if (collectionView == self.selectedImagesCollectionView || self.collectionVSecondary) {
        
        return UIEdgeInsetsMake(4, 4, 4, 4);
    }
    else {
        return UIEdgeInsetsMake(0, 0, 0, 0);
    }

}


/// Anil

//- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath {
//    indexVisibleCell = [indexPath indexAtPosition:[indexPath length] - 1];
//}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:
(NSIndexPath *)indexPath
{
    if (collectionView == self.templateTypeCollectionView) {
        
        templateCollectionViewCell *cell = [collectionView cellForItemAtIndexPath:indexPath];
        
        
        switch (indexPath.row) {
            case 0:
                selectedTemplateType = @"first";
                break;
            case 1:
                selectedTemplateType = @"second";
                break;
            case 2:
                selectedTemplateType = @"third";
                break;
            default:
                break;
        }
        
        [self refreshAction];
        
    }else if (collectionView == self.selectedImagesCollectionView) {
        NSLog(@"indexpath is %ld", (long)indexPath.row);
        
    }
}

#pragma mark - Custom Actions
- (void)backAction
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)exportAction:(UIButton *)sender
{
    UIView *subView = self.middleView;
    UIGraphicsBeginImageContextWithOptions(subView.bounds.size, YES, 0.0f);
    CGContextRef context = UIGraphicsGetCurrentContext();
    [subView.layer renderInContext:context];
    UIImage *snapshotImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    UIActivityViewController *activityController = [[UIActivityViewController alloc] initWithActivityItems:@[snapshotImage] applicationActivities:nil];
    
    activityController.excludedActivityTypes = @[UIActivityTypeAssignToContact, UIActivityTypePrint, UIActivityTypePostToFacebook, UIActivityTypePostToTwitter, UIActivityTypePostToWeibo, UIActivityTypeMail, UIActivityTypeCopyToPasteboard, UIActivityTypeAssignToContact,UIActivityTypeSaveToCameraRoll, UIActivityTypeAddToReadingList,UIActivityTypePostToFlickr,UIActivityTypePostToVimeo, UIActivityTypePostToTencentWeibo, UIActivityTypeAirDrop, UIActivityTypeOpenInIBooks];
    
    if (IS_IPAD) {
        activityController.popoverPresentationController.sourceView = self.view;
        activityController.popoverPresentationController.sourceRect = CGRectMake(self.view.bounds.origin.x+self.view.bounds.size.width/4, self.view.bounds.origin.y+self.view.bounds.size.height, 400, 400);
    }
    [self presentViewController:activityController animated:YES completion:nil];
    
}

-(void) getDataForDataBlocks
{
    [SVProgressHUD show];
    NSLog(@"loading is %hhd",isLoading);
    if(isLoading) return;
    isLoading = YES;
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setValue:@([shareObject integerForKey:@"user_id"]) forKey:@"user_id"];
    [parameters setValue:@([shareObject integerForKey:@"baby_id"]) forKey:@"baby_id"];
    
    NSString *startDate = [self formatDateTime:[self.data objectForKey:@"selected_Start_Date"]];
    NSString *endDate = [self formatDateTime:[self.data objectForKey:@"selected_End_Date"]];

    [parameters setValue:[self.data objectForKey:@"selected_Data_Blocks"] forKey:@"type"];
    [parameters setValue:startDate forKey:@"start_date"];
    [parameters setValue:endDate forKey:@"end_date"];
    
    NSLog(@"Parameters %@",parameters);
    
    [[WebServiceManager sharedManager] initWithBaseURL:@"api/getBabyAchievementGallery" parameters:parameters success:^(id result) {

        if ([[result valueForKey:@"success"] boolValue] == false) {
//            [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:[result objectForKey:@"message"] input:nil];
            
        }else{
            
            NSLog(@"result %@",result);
            dictList = (NSMutableDictionary *)[result valueForKey:@"list"];
            NSLog(@"Block details list is %@",dictList);
            
        }

        isLoading = NO;
        [SVProgressHUD dismiss];
        
        [self addingBlocks];
        [self refreshAction];
        

    } failure:^(NSError *error) {
        [SVProgressHUD dismiss];
        isLoading = NO;
        [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:LocalizedString(@"error_unknown", nil) input:nil];
    }];
    
}

// testing Anil

-(void)addingBlocks {
    
    NSLog(@"arrNew.count -- %ld",(unsigned long)arrNew.count);
    
    //[arrSelectedDataBlocks removeAllObjects];
    [arrSelectedImages removeAllObjects];
    
    NSLog(@"arrSelectedImages -- %@",arrSelectedImages);
    NSLog(@"arrNew -- %@",arrNew);
    
    for (int i = 0; i<arrSelectedDataBlocks.count; i++) {
    
        if ([[arrSelectedDataBlocks objectAtIndex:i] isEqualToString:@"height"]) {
            
            UIImage *postImage = [UIImage imageNamed:@"gallery_data_height_bg.png"];
            //[arrSelectedImages insertObject:postImage atIndex:i];
            [arrSelectedImages addObject:postImage];
            NSLog(@"arrSelectedImages -- %@",arrSelectedImages);
            
        }
        
        if ([[arrSelectedDataBlocks objectAtIndex:i] isEqualToString:@"weight"]) {
            
            UIImage *postImage = [UIImage imageNamed:@"gallery_data_weight_bg"];
            //[arrSelectedImages insertObject:postImage atIndex:i];
            [arrSelectedImages addObject:postImage];
            NSLog(@"arrSelectedImages -- %@",arrSelectedImages);
            
        }
        
        if ([[arrSelectedDataBlocks objectAtIndex:i] isEqualToString:@"age"]) {
            
            UIImage *postImage = [UIImage imageNamed:@"gallery_data_age_bg"];
            //[arrSelectedImages insertObject:postImage atIndex:i];
            [arrSelectedImages addObject:postImage];
            NSLog(@"arrSelectedImages -- %@",arrSelectedImages);
            
        }
        
        if ([[arrSelectedDataBlocks objectAtIndex:i] isEqualToString:@"achievement"]) {
            UIImage *postImage = [UIImage imageNamed:@"bg_welcome"];
            //[arrSelectedImages insertObject:postImage atIndex:i];
            [arrSelectedImages addObject:postImage];
            NSLog(@"arrSelectedImages -- %@",arrSelectedImages);
            
        }
    }
    
    
    for (int i = 0; i<arrNew.count; i++) {
        NSLog(@"arrNew is %@",arrNew);
        NSString *photoUrl = [[arrNew objectAtIndex:i] valueForKey:@"photo_url"];
        NSString *strName = [[arrNew objectAtIndex:i] valueForKey:@"name"];
        NSString *strNotes = [[arrNew objectAtIndex:i] valueForKey:@"notes"];
        
        if ([photoUrl isKindOfClass:[NSNull class]]) {
            photoUrl = @"others";
        }
        if ([strName isKindOfClass:[NSNull class]]) {
            strName = @"";
        }
        if ([strNotes isKindOfClass:[NSNull class]]) {
            strNotes = @"";
        }
        
        NSString *strNew = [NSString stringWithFormat:@"%@,%@,%@",photoUrl,strName,strNotes];
        [arrSelectedImages addObject:strNew];
        NSLog(@"arrSelectedImages count is %lu",(unsigned long)arrSelectedImages.count);
//        if (arrSelectedImages.count>8) {
//            break;
//            
//        }
    }
    
    
    NSLog(@"arrSelectedImages -- %@",arrSelectedImages);
//     UIImage *dummyImage = [UIImage imageNamed:@"dummy.png"];
//    for (unsigned long int i = arrNew.count; i < 9; i++){
//        [arrSelectedImages addObject:dummyImage];
//    }
    NSLog(@"arrSelectedImages -- %@",arrSelectedImages);
    
//    // For temporary testing only
//    [arrSelectedImages removeAllObjects];
//    UIImage *img = [[UIImage alloc]init];
//    NSString *str = [[NSString alloc]init];
//    for(int i = 0 ; i < 1 ; i++){
//        str = [NSString stringWithFormat:@"btn_teeth%d",i];
//        [arrSelectedImages addObject:img];
//    }
}

// Anil
-(NSURL *)getUrlFromData :(NSString *)imageName{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *imageFilePath;
    
    imageFilePath = [documentsDirectory stringByAppendingPathComponent:imageName];
    NSURL *url = [NSURL fileURLWithPath:imageFilePath];
    
    return url;
    
}

#pragma mark - IBAction Methods

- (IBAction)nextAction:(UIButton *)sender
{
    // Save export data.
    [SVProgressHUD show];
    UIView *subView = self.middleView;
    UIGraphicsBeginImageContextWithOptions(subView.bounds.size, YES, 0.0f);
    CGContextRef context = UIGraphicsGetCurrentContext();
    [subView.layer renderInContext:context];
    UIImage *snapshotImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    if (snapshotImage) {
        [SVProgressHUD dismiss];
        UIImageWriteToSavedPhotosAlbum(snapshotImage, self, @selector(savedPhotoImage:didFinishSavingWithError:contextInfo:), nil);
    }
    else {
        [SVProgressHUD dismiss];
        [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:LocalizedString(@"error_unknown", nil) input:nil];
    }
    
}

#pragma mark -  View's Data Configuration methods
-(void) refreshAction
{
    [SVProgressHUD show];
    
    [self.selectedImagesCollectionView reloadData];
    [self.templateTypeCollectionView reloadData];
    [self.collectionVSecondary reloadData];
    [SVProgressHUD dismiss];
    
}

#pragma mark - Pangesture Method
-(void)handleLongGesture:(UILongPressGestureRecognizer *)sender
{
    CGPoint location = [sender locationInView:self.selectedImagesCollectionView];
    
    switch (sender.state) {
        case UIGestureRecognizerStateBegan:
            [self startDragAtLocation:location];
            break;
        case UIGestureRecognizerStateChanged:
            [self updateDragAtLocation:location];
            break;
        case UIGestureRecognizerStateEnded:
            [self endDragAtLocation:location];
            break;
        default:
            break;
    }
}

-(void)handleLongGestureNew:(UILongPressGestureRecognizer *)sender
{
    CGPoint location = [sender locationInView:self.collectionVSecondary];
    
    switch (sender.state) {
        case UIGestureRecognizerStateBegan:
            [self startDragAtLocationNew:location];
            break;
        case UIGestureRecognizerStateChanged:
            [self updateDragAtLocationNew:location];
            break;
        case UIGestureRecognizerStateEnded:
            [self endDragAtLocationNew:location];
            break;
        default:
            break;
    }
}

-(void)startDragAtLocation:(CGPoint)location
{
    NSLog(@"startDragAtLocation");
    
    NSIndexPath *indexPath = [self.selectedImagesCollectionView indexPathForItemAtPoint:location];
    
    UICollectionViewCell *cell = [self.selectedImagesCollectionView cellForItemAtIndexPath:indexPath];
    
    cell = [self.selectedImagesCollectionView cellForItemAtIndexPath:indexPath];
    if (cell == nil) {
        return;
    }
    
    originalIndexPath = indexPath;
    draggingIndexPath = indexPath;
    draggingView = [cell snapshotViewAfterScreenUpdates:YES];
    draggingView.frame = cell.frame;
    [self.selectedImagesCollectionView addSubview:draggingView];
    

    dragOffset = CGPointMake(draggingView.center.x - location.x, draggingView.center.y - location.y);
    draggingView.layer.shadowPath = [UIBezierPath bezierPathWithRect:draggingView.bounds].CGPath;
    draggingView.layer.shadowColor = [UIColor blackColor].CGColor;
    draggingView.layer.shadowOpacity = 0.8;
    draggingView.layer.shadowRadius = 10;
    
    [UIView animateWithDuration:0.4 delay:0 usingSpringWithDamping:0.4 initialSpringVelocity:0 options:nil animations:^{
        draggingView.alpha = 0.9;
        draggingView.transform = CGAffineTransformMakeScale(1.2, 1.2);
    } completion:^(BOOL finished) {
        
    }];

}

-(void)startDragAtLocationNew:(CGPoint)location
{
    NSLog(@"startDragAtLocation");
    
    NSIndexPath *indexPath = [self.collectionVSecondary indexPathForItemAtPoint:location];
    
    UICollectionViewCell *cell = [self.collectionVSecondary cellForItemAtIndexPath:indexPath];
    
    cell = [self.collectionVSecondary cellForItemAtIndexPath:indexPath];
    if (cell == nil) {
        return;
    }
    
    originalIndexPath = indexPath;
    draggingIndexPath = indexPath;
    draggingView = [cell snapshotViewAfterScreenUpdates:YES];
    draggingView.frame = cell.frame;
    [self.collectionVSecondary addSubview:draggingView];
    
    
    dragOffset = CGPointMake(draggingView.center.x - location.x, draggingView.center.y - location.y);
    draggingView.layer.shadowPath = [UIBezierPath bezierPathWithRect:draggingView.bounds].CGPath;
    draggingView.layer.shadowColor = [UIColor blackColor].CGColor;
    draggingView.layer.shadowOpacity = 0.8;
    draggingView.layer.shadowRadius = 10;
    
    [UIView animateWithDuration:0.4 delay:0 usingSpringWithDamping:0.4 initialSpringVelocity:0 options:nil animations:^{
        draggingView.alpha = 0.9;
        draggingView.transform = CGAffineTransformMakeScale(1.2, 1.2);
    } completion:^(BOOL finished) {
        
    }];
    
}


-(void)updateDragAtLocation:(CGPoint)location
{
    NSLog(@"updateDragAtLocation");
    
    if (draggingView == nil) {
        return;
    }

    draggingView.center = CGPointMake(location.x + dragOffset.x, location.y + dragOffset.y);
    newIndexPath = [self.selectedImagesCollectionView indexPathForItemAtPoint:location];
    if (newIndexPath != nil) {
        [self.selectedImagesCollectionView moveItemAtIndexPath:draggingIndexPath toIndexPath:newIndexPath];
        draggingIndexPath = newIndexPath;
    }
    
}

-(void)updateDragAtLocationNew:(CGPoint)location
{
    NSLog(@"updateDragAtLocation");
    
    if (draggingView == nil) {
        return;
    }
    
    draggingView.center = CGPointMake(location.x + dragOffset.x, location.y + dragOffset.y);
    newIndexPath = [self.collectionVSecondary indexPathForItemAtPoint:location];
    if (newIndexPath != nil) {
        [self.collectionVSecondary moveItemAtIndexPath:draggingIndexPath toIndexPath:newIndexPath];
        draggingIndexPath = newIndexPath;
    }
    
}


-(void)endDragAtLocation:(CGPoint)location
{
    if (draggingView == nil) {
        return;
    }
    
    CGPoint targetCentre = [self.selectedImagesCollectionView.dataSource collectionView:self.selectedImagesCollectionView cellForItemAtIndexPath:draggingIndexPath].center;
    
    CABasicAnimation *shadowFade = [CABasicAnimation animationWithKeyPath:@"shadowOpacity"];
    shadowFade.fromValue = [NSNumber numberWithDouble:0.8];
    shadowFade.toValue = 0;
    shadowFade.duration = 0.4;
    [draggingView.layer addAnimation:shadowFade forKey:@"shadowFade"];
    
    [UIView animateWithDuration:0.4 delay:0 usingSpringWithDamping:0.4 initialSpringVelocity:0 options:nil animations:^{
        draggingView.center = targetCentre;
        draggingView.transform = CGAffineTransformIdentity;
        
    } completion:^(BOOL finished) {
        
        if (draggingIndexPath == originalIndexPath) {
            [self.selectedImagesCollectionView moveItemAtIndexPath:originalIndexPath toIndexPath:draggingIndexPath];
        }
        [draggingView removeFromSuperview];
        draggingIndexPath = nil;
        draggingView = nil;
        
    }];

}


-(void)endDragAtLocationNew:(CGPoint)location
{
    if (draggingView == nil) {
        return;
    }
    
    CGPoint targetCentre = [self.collectionVSecondary.dataSource collectionView:self.collectionVSecondary cellForItemAtIndexPath:draggingIndexPath].center;
    
    CABasicAnimation *shadowFade = [CABasicAnimation animationWithKeyPath:@"shadowOpacity"];
    shadowFade.fromValue = [NSNumber numberWithDouble:0.8];
    shadowFade.toValue = 0;
    shadowFade.duration = 0.4;
    [draggingView.layer addAnimation:shadowFade forKey:@"shadowFade"];
    
    [UIView animateWithDuration:0.4 delay:0 usingSpringWithDamping:0.4 initialSpringVelocity:0 options:nil animations:^{
        draggingView.center = targetCentre;
        draggingView.transform = CGAffineTransformIdentity;
        
    } completion:^(BOOL finished) {
        
        if (draggingIndexPath == originalIndexPath) {
            [self.collectionVSecondary moveItemAtIndexPath:originalIndexPath toIndexPath:draggingIndexPath];
        }
        [draggingView removeFromSuperview];
        draggingIndexPath = nil;
        draggingView = nil;
        
    }];
    
}


#pragma mark - Other Methods
- (void) savedPhotoImage:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo
{
    if (!error) {
        
        [self showAlertWithTitleCallback:LocalizedString(@"title_saved", nil) message:LocalizedString(@"txt_imageSave_success", nil) callback:^(UIAlertAction *action) {
            [self.navigationController popToRootViewControllerAnimated:YES];
        }];
        
    }
    else {
        [self showAlertWithTitle:LocalizedString(@"title_error", nil) message:LocalizedString(@"error_unknown", nil) input:nil];
    }
}

-(NSString *)formatDateTime:(NSDate *)sDate
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    //[dateFormatter setDateFormat:@"E d MMM, yyyy hh:mm a"];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    [dateFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
    NSString *dateStr = [dateFormatter stringFromDate:sDate];
    NSLog(@"%@",dateStr);
    return dateStr;
    
}

- (void)showPhotoAction:(UIImage *)img
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"PhotoViewController"];
    viewController.data = [[NSMutableDictionary alloc] initWithDictionary:@{@"viewController":self, @"image":img}];
    [self presentViewController:viewController animated:YES completion:nil];
    
}



@end
